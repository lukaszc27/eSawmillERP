add_library(widgets-lib)
target_sources(widgets-lib PRIVATE
    "include/colorselector.hpp"
    "include/configurationwidget.h"
    "include/contactwidget.hpp"
    "include/eventswidget.hpp"
    "include/invoicenumberspinbox.hpp"
    "include/messagebox.h"
    "include/pushbutton.h"
    "include/roleeditwidget.hpp"
    "include/tableview.h"
    "include/widgets_global.h"
    #
    "src/colorselector.cpp"
    "src/configurationwidget.cpp"
    "src/contactwidget.cpp"
    "src/eventswidget.cpp"
    "src/invoicenumberspinbox.cpp"
    "src/messagebox.cpp"
    "src/pushbutton.cpp"
    "src/roleeditwidget.cpp"
    "src/tableview.cpp"

    # dialogs directory
    "include/dialogs/citydictionarydialog.hpp"
    "include/dialogs/orders/createelement.h"
    "src/dialogs/citydictionarydialog.cpp"
    "src/dialogs/orders/createelement.cpp"

    # filters directory
    "include/filters/eventsproxymodel.hpp"
    "src/filters/eventsproxymodel.cpp"

    # models directory
    "include/models/citydictionarymodel.hpp"
    "include/models/eventsmodel.hpp"
    "include/models/rolemodel.hpp"
    "include/models/orders/elementnamemodel.hpp"
    "src/models/citydictionarymodel.cpp"
    "src/models/rolemodel.cpp"
    "src/models/eventsmodel.cpp"
    "src/models/orders/elementnamemodel.cpp"
)
target_compile_definitions(widgets-lib PRIVATE WIDGETS_LIBRARY)
target_include_directories(widgets-lib
    PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        core-lib
)
target_link_libraries(widgets-lib
    core-lib
    Qt::Widgets
    Qt::Gui
    Qt::Network
    Qt::Xml
)
install(TARGETS widgets-lib)