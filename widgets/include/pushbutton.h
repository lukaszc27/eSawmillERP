#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include "widgets_global.h"
#include <QPushButton>
#include <QLabel>
#include <QWidget>

namespace widgets {

class WIDGETS_EXPORT PushButton : public QWidget {
	Q_OBJECT

public:
	PushButton(QWidget* parent = nullptr);
	PushButton(const QString &label,
			   QKeySequence sequence = QKeySequence(),
			   QWidget* parent = nullptr,
			   const QIcon &icon = QIcon());

	void setPlaceholderText(const QString& text);
	inline void setText(const QString &text) { mPushButton->setText(text); }
	inline void setMenu(QMenu* menu) { mPushButton->setMenu(menu); }
	inline QKeySequence shortcut() const { return mPushButton->shortcut(); }

private:
	QPushButton* mPushButton;
	QLabel* mLabel;

signals:
	void clicked();	// emitowany w przypadku kliknięcia w przycisk
};

} // namespace widgets

#endif // PUSHBUTTON_H
