#ifndef CONFIGURATIONWIDGET_H
#define CONFIGURATIONWIDGET_H

#include "widgets_global.h"
#include <QWidget>
#include <eventagentclient.hpp>


namespace eSawmill {
namespace widgets {

/**
 * @brief The ConfigurationWidget class
 * klasa z której dziedziczą wszystkie okna konfiguracyjne
 * oprócz konfiguracji wprowadzanej w QWizard
 */
class WIDGETS_EXPORT ConfigurationWidget : public QWidget {
	Q_OBJECT

public:
	ConfigurationWidget(QWidget* parent = 0);

	/**
	 * @brief initialize - uruchamiana w momęcie tworzenia okna
	 * służy glównie do wczytania obecnej konfiguracji
	 */
	virtual void initialize();

	/**
	 * @brief save - zapisuje wprowadzoną konfigurację
	 */
	virtual void save();

protected:
	QLayout* createRow(QWidget *field, const QString& unit);

Q_SIGNALS:
	void repositoryChanged(const core::providers::agent::RepositoryEvent& event);
};

} // namespace widget
} // namespace eSawmill

#endif // CONFIGURATIONWIDGET_H
