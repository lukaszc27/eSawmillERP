#ifndef CONTACTWIDGET_HPP
#define CONTACTWIDGET_HPP

#include "widgets_global.h"
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <dbo/contact.h>


namespace widgets {

///
/// \brief The ContactWidget class
/// widget to display contact data from core::Contact structure
///
class WIDGETS_EXPORT ContactWidget : public QWidget {
	Q_OBJECT

public:
	explicit ContactWidget(
		core::Contact& contact,		///< reference to editing contact object
		QWidget* parent = nullptr	///< pointer to parent widget
	);
	virtual ~ContactWidget() = default;

	///
	/// \brief load
	/// load data from contact object into GUI fields
	/// \param contact
	///
	void load(const core::Contact& contact);
	core::Contact& contact();

private:
	core::Contact& _contact;	///< reference to editing contact object

	void createAllWidgets();
	void createConnections();

	/// controls widget
	QLineEdit* mCountry;
	QLineEdit* mCountryIso;
	QLineEdit* mCity;
	QLineEdit* mPostOffice;
	QLineEdit* mStreet;
	QLineEdit* mPhone;
	QComboBox* mState;
	QLineEdit* mPostCode;
	QLineEdit* mHomeNumber;
	QLineEdit* mLocalNumber;
	QLineEdit* mMail;
	QLineEdit* mUrl;
	QPushButton* mCityButton;
	QPushButton* mPostOfficeButton;


	/// \brief showDictionaryDialog
	/// show dialog where user can select city name from system dictionary
	/// \param lineEdit
	void showDictionaryDialog(QLineEdit* lineEdit);

	/// \brief prepareContactObject
	/// load data from fields into contact object before returned from widget
	void prepareContactObject();

protected:
	void closeEvent(QCloseEvent* event) override;
};

}

#endif // CONTACTWIDGET_HPP
