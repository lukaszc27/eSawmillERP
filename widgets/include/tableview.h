#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include "widgets_global.h"
#include <QTableView>
#include <QKeyEvent>
#include <QMenu>


namespace widgets {

class WIDGETS_EXPORT TableView : public QTableView {
	Q_OBJECT

public:
	TableView(QWidget* parent = nullptr);

	///
	/// \brief setContextMenu set context menu for QTableView
	/// \param menu - pointer to QMenu
	///
	void setContextMenu(QMenu* menu);

protected:
	virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void dragEnterEvent(QDragEnterEvent* event) override;
    virtual void dragMoveEvent(QDragMoveEvent* event) override;
	virtual void dropEvent(QDropEvent* event) override;

	///
	/// \brief paintEvent repaint widget
	/// and add information when model return 0 rows
	/// \param event
	///
	virtual void paintEvent(QPaintEvent* event) override;

Q_SIGNALS:

	/**
	 * emitowany po kliknięci klawisza enter
	 * wysyła indeks na której pozycji znajduje się wskaźnik
	 */
	void returnPressed(const QModelIndex &index);

	///
	/// \brief fileDropped emited when user drop file on tableview (on widget)
	/// \param filename - file name to load data (importing)
	///
    void filesDropped(const QStringList& files);

	///
	/// \brief contextMenuShowed - signal emited before showed context menu
	///
	void contextMenuShowed();

private:
	QMenu* mContextMenu;
};

} // namespace widgets

#endif // TABLEVIEW_H
