#ifndef COLORSELECTOR_HPP
#define COLORSELECTOR_HPP

#include <QWidget>
#include <QPushButton>
#include "widgets_global.h"


namespace widgets {

class WIDGETS_EXPORT ColorSelector : public QWidget {
	Q_OBJECT

public:
	ColorSelector(QWidget* parent = nullptr, QColor initial = QColor(Qt::black));

	QColor color() const { return mCurrentColor; }
	void setCurrentColor(QColor color) { currentColorChanged(color); }

private:
	void createWidgets();
	void createConnections();

	//// widgety
	QWidget* mColorView;
	QPushButton* mButtonChange;

	QColor mInitialColor;
	QColor mCurrentColor;

private Q_SLOTS:
	void changeButton_clicked();

signals:
	void currentColorChanged(const QColor color);
};

}

#endif // COLORSELECTOR_HPP
