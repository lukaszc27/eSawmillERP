#ifndef CITYDICTIONARYMODEL_HPP
#define CITYDICTIONARYMODEL_HPP

#include "../widgets_global.h"
#include <QAbstractListModel>
#include <QList>
#include <dbo/city.hpp>


namespace widgets {
namespace models {

class WIDGETS_EXPORT CityDictionaryModel : public QAbstractListModel {
	Q_OBJECT

public:
	explicit CityDictionaryModel(QObject* parent = nullptr);
	~CityDictionaryModel() {}

	int rowCount(const QModelIndex &index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	/**
	 * @brief get pobiera dane z bazy i umieszcza je w modelu
	 */
	void get();

	/**
	 * @brief create dodaje do bazy oraz do modelu nowe miasto
	 * wprowadzone przez użytkownika
	 * @param name - nazwa miasta do wprowadzania
	 */
	void create(const QString &name);

private:
	QList<core::City> mList;
};

} // namespace models
} // namespace widgets

#endif // CITYDICTIONARYMODEL_HPP
