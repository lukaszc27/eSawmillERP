#ifndef ROLEMODEL_HPP
#define ROLEMODEL_HPP

#include "../widgets_global.h"
#include <QAbstractListModel>
#include <dbo/role.hpp>


namespace widgets::models {

///
/// \brief The RoleModel class
/// basic model to display all roles existing in system
/// uses by role editor
///
class WIDGETS_EXPORT RoleModel : public QAbstractListModel {
	Q_OBJECT

public:
	explicit RoleModel(QObject* parent = nullptr);
	virtual ~RoleModel() = default;

	core::RoleCollection roles() const
		{ return _roles; }

	int rowCount(const QModelIndex& index) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

	void removeRoles(const core::RoleCollection& roles);
	void setRoles(const core::RoleCollection& roles);
	void addRole(const core::Role& role);
	void removeRole(const core::eloquent::DBIDKey& roleId);
	void removeRole(const core::Role& role) { removeRole(role.id()); }

	///
	/// \brief The Roles enum
	/// additional roles uses by model
	///
	enum Roles {
		Id = Qt::UserRole + 1
	};

private:
	/// roles to display in model
	core::RoleCollection _roles;
};

} // namespace widgets::models

#endif // ROLEMODEL_HPP
