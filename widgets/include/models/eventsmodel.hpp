#ifndef EVENTSMODEL_HPP
#define EVENTSMODEL_HPP

#include "widgets_global.h"
#include <QAbstractTableModel>
#include <eventagentclient.hpp>
#include <tuple>


namespace widgets::models {

///
/// \brief The EventsModel class
/// model to display all events received by eventagent
///
class WIDGETS_EXPORT EventsModel : public QAbstractTableModel {
	Q_OBJECT

public:
	explicit EventsModel(QObject* parent = nullptr);
	virtual ~EventsModel() = default;

	///
	/// \brief The Columns enum
	/// columns uses by model to present data
	///
	enum Columns {
		User,
		Time,
		Description
	};

	///
	/// \brief addEvent
	/// add new event to model
	/// \param event
	///
	void addEvent(const core::providers::agent::RepositoryEvent& event);

	///@{
	/// abstract table model interface implementation
	int rowCount(const QModelIndex& index) const override;
	int columnCount(const QModelIndex& index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

private:
	QList<std::tuple<QString, QTime, QString>> mEvents;
};

} // namespace widgets::models

#endif // EVENTSMODEL_HPP
