#ifndef ELEMENTNAMEMODEL_H
#define ELEMENTNAMEMODEL_H

#include "../../widgets_global.h"
#include <QStringListModel>


namespace widgets::models::orders {

class WIDGETS_EXPORT ElementName : public QStringListModel {
	Q_OBJECT

public:
	ElementName(QObject* parent = nullptr);
	~ElementName() {}
};

} // namespace widgets::models::orders

#endif // ELEMENTNAMEMODEL_H
