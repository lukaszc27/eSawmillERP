#ifndef CITYDICTIONARYDIALOG_HPP
#define CITYDICTIONARYDIALOG_HPP

#include "../widgets_global.h"
#include <QDialog>
#include <QListView>

#include "../pushbutton.h"
#include "../models/citydictionarymodel.hpp"


namespace widgets::dialogs {

class WIDGETS_EXPORT CityDictionaryDialog : public QDialog {
	Q_OBJECT

public:
	CityDictionaryDialog(QWidget* parent = nullptr);
	~CityDictionaryDialog();

	void accept() override;

	/**
	 * @brief city zwraca nazwę aktualnie wybranego miasta
	 * przez użytkownika
	 * @return
	 */
	QString city() const { return mCurrentSelectedCity; }

private:
	void createAllWidgets();
	void createConnections();

	QString mCurrentSelectedCity;	// nazwa aktualnie wybranego miasta

	QListView* mListView;
	PushButton* mAddButton;
	PushButton* mRejectButton;
	PushButton* mAcceptButton;

	models::CityDictionaryModel* mModel;


private Q_SLOTS:
	void addButtonHandle();
};

} // namesapce widgets

#endif // CITYDICTIONARYDIALOG_HPP
