#ifndef CREATEELEMENT_H
#define CREATEELEMENT_H

#include "../../widgets_global.h"
#include <QDialog>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>

#include <pushbutton.h>		// moduł widgets
#include <dbo/orderElements.h>	// moduł core


namespace widgets::dialogs::orders {

/**
 * @brief The CreateElement class
 * Dialog umożliwiający wprowadzenie nowego elementu konstrukcyjengo
 * listy zamówienia
 */
class WIDGETS_EXPORT CreateElement : public QDialog {
	Q_OBJECT

public:
	CreateElement(QWidget* parent = nullptr, unsigned int id = 0);

	core::order::Element element();

private:
	//// funkcje
	void createWidgets();
	void createConnections();

	//// kontrolki
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
	QDoubleSpinBox* mWidth;
	QDoubleSpinBox* mHeight;
	QDoubleSpinBox* mLength;
	QSpinBox* mCount;
	QCheckBox* mPlanned;
	QComboBox* mName;
	QComboBox* mWoodType;
};

} // namespace widgets::dialogs::orders

#endif // CREATEELEMENT_H
