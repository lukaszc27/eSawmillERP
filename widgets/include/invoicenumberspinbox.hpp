#ifndef INVOICENUMBERSPINBOX_HPP
#define INVOICENUMBERSPINBOX_HPP

#include <QSpinBox>
#include <QLineEdit>
#include "widgets_global.h"

namespace widgets {

class WIDGETS_EXPORT CustomLineEdit : public QLineEdit {
	Q_OBJECT

public:
	CustomLineEdit(QWidget* parent = nullptr);
	virtual ~CustomLineEdit() = default;

protected:
	virtual void paintEvent(QPaintEvent* event) override;
};

class WIDGETS_EXPORT InvoiceNumberSpinBox : public QSpinBox {
	Q_OBJECT

public:
	InvoiceNumberSpinBox(QWidget* parent = nullptr);
	virtual ~InvoiceNumberSpinBox() = default;
};

} // namespace widgets

#endif // INVOICENUMBERSPINBOX_HPP
