#ifndef EVENTSWIDGET_H
#define EVENTSWIDGET_H

#include "widgets_global.h"
#include <QTreeView>
#include "models/eventsmodel.hpp"
#include "filters/eventsproxymodel.hpp"
#include <providers/protocol.hpp>
#include <eventagentclient.hpp>

namespace agent = core::providers::agent;

namespace widgets {

///
/// \brief The EventsWidget class
/// widget to display historical infomrmation about events
/// existing in current session (from last login time)
///
class WIDGETS_EXPORT EventsWidget : public QWidget {
	Q_OBJECT

public:
	explicit EventsWidget(QWidget* parent = nullptr);
	virtual ~EventsWidget() = default;

public Q_SLOTS:
	void notifyEventReceived(const agent::RepositoryEvent& event);

private:
	void createWidgets();
	void createModels();

	/// widgets
	QTreeView* mEventsView;

	/// models
	models::EventsModel* mEventsModel;
	filters::EventsProxyModel* mEventsProxyModel;
};

} // namespace widgets

#endif // EVENTSWIDGET_H
