#ifndef ROLEEDITWIDGET_HPP
#define ROLEEDITWIDGET_HPP

#include "widgets_global.h"
#include <QWidget>
#include <QPushButton>
#include <QListView>
#include "models/rolemodel.hpp"


namespace widgets {

class RoleView;
///
/// \brief The RoleEditWidget class
/// editor for user roles
///
class WIDGETS_EXPORT RoleEditWidget : public QWidget {
	Q_OBJECT

public:
	explicit RoleEditWidget(QWidget* parent = nullptr);
	virtual ~RoleEditWidget() = default;

	///
	/// \brief assignRoles
	/// add roles for user
	/// \param roles
	///
	void assignRoles(const core::RoleCollection& roles);

	///
	/// \brief assignedRoles
	/// get edited role list
	/// \return
	///
	core::RoleCollection assignedRoles() const
		{ return mRoles->roles(); }

private:
	void createAllWidgets();
	void createConnections();
	void createModels();

	// controls
	RoleView* mRolesView;
	RoleView* mAllRolesView;
	QPushButton* mAddRole;
	QPushButton* mRemoveRole;

	// models
	models::RoleModel* mRoles;
	models::RoleModel* mAllRoles;

private Q_SLOTS:
	///
	/// \brief addRole
	/// assign new role for user
	///
	void addRole();

	///
	/// \brief removeRole
	/// remove role from user
	///
	void removeRole();
};

///////////////////////////////////////////////////////////////////
class RoleView : public QListView {
	Q_OBJECT

public:
	explicit RoleView(QWidget* parent = nullptr);
	virtual ~RoleView() = default;

protected:
	void dragEnterEvent(QDragEnterEvent* event) override;
	void dragMoveEvent(QDragMoveEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void dropEvent(QDropEvent* event) override;
};

} // namespace widgets


#endif // ROLEEDITWIDGET_HPP
