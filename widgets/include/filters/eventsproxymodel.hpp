#ifndef EVENTSPROXYMODEL_H
#define EVENTSPROXYMODEL_H

#include "../widgets_global.h"
#include <QSortFilterProxyModel>


namespace widgets::filters {

class WIDGETS_EXPORT EventsProxyModel : public QSortFilterProxyModel {
	Q_OBJECT

public:
	explicit EventsProxyModel(QObject* parent = nullptr);
	virtual ~EventsProxyModel() = default;

protected:
	virtual bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
};

}

#endif // EVENTSPROXYMODEL_H
