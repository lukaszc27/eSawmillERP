#ifndef MESSAGEBOX_WIDGETS_H
#define MESSAGEBOX_WIDGETS_H

#include "widgets_global.h"
#include <QMessageBox>


namespace widgets {

/**
 * Modalne okna informacyjne dostosowane do różnych platform systemowych
 */
class WIDGETS_EXPORT MessageBox
{
public:
	/**
	 * @brief information - spercionalizowany dialog inforamcyjny
	 * @param parent - okno rodzic
	 * @param title - tytuł okna informacyjnego
	 * @param text - komunikat do przekazania
	 * @param buttons - przyciski jakie mają pokazać się na dialogu
	 * @return indeks klikniętego przycisku
	 */
	static int information(QWidget* parent,
						const QString &title,
						const QString &text,
						const QString &detailedText = "",
						QMessageBox::StandardButtons buttons = QMessageBox::StandardButton::Ok);

	/**
	 * @brief question - dialog pytający użytkownika o podjęcie określonego działania
	 * @param parent - okno rodzica
	 * @param title - tytuł okna informacyjnego
	 * @param text - komunikat do przekazania
	 * @param detailedText - spersolizowane inforacje dla użytkownika
	 * @param buttons - przyciski w dialogu
	 * @return - indeks klikniętego przycisku
	 */
	static int question(QWidget* parent,
						const QString &title,
						const QString &text,
						const QString& detailedText = "",
						QMessageBox::StandardButtons buttons = QMessageBox::StandardButton::Ok);

	/**
	 * @brief warning - dialog ostzregający użytkownika
	 * @param parent - okno rodzica
	 * @param title - tytuł dialogu
	 * @param text - komunikat do przekazania
	 * @param detailedText - spersionalizowane informacje dla użytkownika
	 * @param buttons - przyciski w dialogu
	 * @return
	 */
	static int warning(QWidget* parent,
					   const QString &title,
					   const QString &text,
					   const QString &detailedText = "",
					   QMessageBox::StandardButtons buttons = QMessageBox::StandardButton::Ok);

	/**
	 * @brief critical - dialog informujący użytkowników o krytycznych błędach/ informacjach
	 * @param parent - okno rodzic
	 * @param title - tytuł
	 * @param text - komunikat informacyjny
	 * @param detailedText - didatkowe informacje do przekazania
	 * @param buttons - dostępne przyciski w dialogu
	 * @return
	 */
	static int critical(QWidget* parent,
						const QString &title,
						const QString &text,
						const QString &detailedText = "",
						QMessageBox::StandardButtons buttons = QMessageBox::StandardButton::Ok);
};

} // namespace widgets

#endif // MESSAGEBOX_WIDGETS_H
