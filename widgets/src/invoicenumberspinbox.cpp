#include "invoicenumberspinbox.hpp"
#include <QPainter>
#include <QPaintEvent>
#include <QDebug>


namespace widgets {

InvoiceNumberSpinBox::InvoiceNumberSpinBox(QWidget* parent)
	: QSpinBox {parent}
{
	setLineEdit(new CustomLineEdit(this));
}

CustomLineEdit::CustomLineEdit(QWidget *parent)
	: QLineEdit(parent)
{
}

void CustomLineEdit::paintEvent(QPaintEvent *event)
{
	QLineEdit::paintEvent(event);

	if (this->text().toInt() <= 0) {
		QPainter painter(this);
		painter.fillRect(event->rect(), Qt::white);
		painter.drawText(event->rect(), "AUTO");
	}
}

} // namespace
