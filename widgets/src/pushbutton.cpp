#include "pushbutton.h"
#include <QVBoxLayout>


namespace widgets {

PushButton::PushButton(QWidget* parent)
	: QWidget(parent)
{
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mPushButton = new QPushButton(this);
	mLabel = new QLabel(this);

	QVBoxLayout* layout = new QVBoxLayout(this);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins(0, 0, 0, 0));
	layout->addWidget(mLabel, 0, Qt::AlignCenter | Qt::AlignBottom);
	layout->addWidget(mPushButton);
}

PushButton::PushButton(const QString &label,
					   QKeySequence sequence,
					   QWidget* parent,
					   const QIcon &icon)
	: QWidget(parent)
{
	const auto getSequenceText = [&sequence]() -> QString {
		const auto text = sequence.toString();
		if (text.toUpper() == "RETURN")
			return "Enter";

		return text;
	};

	mPushButton = new QPushButton(icon, label, this);
	mLabel = new QLabel(getSequenceText(), this);

	mPushButton->setShortcut(sequence);

	mLabel->setAlignment(Qt::AlignCenter);
	mLabel->setStyleSheet("text-align: center; color: blue;");

	QVBoxLayout* layout = new QVBoxLayout(this);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins(0, 0, 0, 0));
	layout->addWidget(mLabel, 0, Qt::AlignCenter | Qt::AlignBottom);
	layout->addWidget(mPushButton);

	connect(mPushButton, &QPushButton::clicked, this, [&] {
		Q_EMIT clicked();
	});
}

void PushButton::setPlaceholderText(const QString& text)
{
	if (!mLabel->text().isEmpty())
		return;

	mLabel->setText(text);
}

} // namespace widgets
