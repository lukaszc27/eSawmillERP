#include "tableview.h"
#include <QMessageBox>
#include <QDebug>
#include <QDrag>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QHeaderView>
#include <QPaintEvent>
#include <QPainter>


namespace widgets {

TableView::TableView(QWidget* parent)
	: QTableView(parent)
	, mContextMenu {nullptr}
{
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setCornerButtonEnabled(false);
	setSortingEnabled(true);
//	horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);

	// context menu settigns
	this->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTableView::customContextMenuRequested, this, [this](const QPoint& pos){
		if (mContextMenu != nullptr) {
			emit contextMenuShowed();
			mContextMenu->popup(viewport()->mapToGlobal(pos));
		}
	});

	setAcceptDrops(true);
	setDropIndicatorShown(true);
}

void TableView::setContextMenu(QMenu *menu) {
	 mContextMenu = menu;
}

void TableView::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Return) {
		emit returnPressed(this->currentIndex());
	}
	QTableView::keyPressEvent(event);
}

void TableView::dragEnterEvent(QDragEnterEvent *event) {
    event->acceptProposedAction();
}

void TableView::dragMoveEvent(QDragMoveEvent *event) {
    event->acceptProposedAction();
}

void TableView::dropEvent(QDropEvent *event) {
    event->acceptProposedAction();

    if (const QMimeData* mime = event->mimeData(); mime->hasUrls()) {
        QStringList files;
		for (auto& url : mime->urls())
            files.append(url.toLocalFile());

        emit filesDropped(files);
	}
}

void TableView::paintEvent(QPaintEvent *event)
{
	QTableView::paintEvent(event);

	if (model() != nullptr && model()->rowCount() <= 0) {
		QFont font;
		font.setPixelSize(24);

		QPainter painter(viewport());
		painter.setFont(font);
		painter.setPen(Qt::gray);
		painter.drawText(event->rect(), "Brak danych", Qt::AlignCenter | Qt::AlignVCenter);
	}
}

} // namespace widgets
