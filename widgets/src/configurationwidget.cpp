#include "configurationwidget.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QIcon>

namespace eSawmill {
namespace widgets {

ConfigurationWidget::ConfigurationWidget(QWidget* parent)
	: QWidget(parent)
{
	setWindowIcon(QIcon(":/icons/settings"));
}

void ConfigurationWidget::initialize()
{
}

void ConfigurationWidget::save()
{
}

QLayout* ConfigurationWidget::createRow(QWidget *field, const QString &unit)
{
	QLabel* unitLabel = new QLabel(unit);
	unitLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->addWidget(field);
	layout->addWidget(unitLabel);

	return layout;
}

} // namespace widget
} // namespace eSawmill
