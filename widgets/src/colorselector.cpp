#include "colorselector.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QColorDialog>
#include <QScopedPointer>


namespace widgets {

ColorSelector::ColorSelector(QWidget* parent, QColor initial)
	: QWidget (parent)
	, mInitialColor(initial)
{
	createWidgets();
	createConnections();

	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	currentColorChanged(initial);
}

void ColorSelector::createWidgets()
{
	mColorView = new QWidget(this);
	mButtonChange = new QPushButton(tr("Wybierz..."), this);

	mColorView->setMinimumSize(48, 32);
	mColorView->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mButtonChange->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* mainLayout = new QHBoxLayout(this);
	mainLayout->addWidget(mColorView);
	mainLayout->addWidget(mButtonChange);
}

void ColorSelector::createConnections()
{
	connect(mButtonChange, &QPushButton::clicked, this, &ColorSelector::changeButton_clicked);

	// ustawienie aktualnie wybranego koloru w podglądzie
	connect(this, &ColorSelector::currentColorChanged, this, [&](const QColor color){
		mCurrentColor = color;
		QPalette palette = mColorView->palette();
		palette.setColor(QPalette::ColorRole::Window, color);
		mColorView->setAutoFillBackground(true);
		mColorView->setPalette(palette);
	});
}

void ColorSelector::changeButton_clicked()
{
	QColor color = QColorDialog::getColor(mInitialColor, this, tr("Wybierz własny kolor..."));
	currentColorChanged(color);
}

}
