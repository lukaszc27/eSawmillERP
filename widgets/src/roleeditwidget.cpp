#include "roleeditwidget.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QMouseEvent>
#include <QDrag>
#include <globaldata.hpp>


namespace widgets {

RoleEditWidget::RoleEditWidget(QWidget* parent)
	: QWidget {parent}
{
	createAllWidgets();
	createModels();
	createConnections();
}

void RoleEditWidget::assignRoles(const core::RoleCollection& roles) {
	mRoles->setRoles(roles);
	mAllRoles->removeRoles(roles);
}

void RoleEditWidget::createAllWidgets() {
	mRolesView		= new RoleView(this);
	mAllRolesView	= new RoleView(this);
	mAddRole		= new QPushButton(this);
	mRemoveRole		= new QPushButton(this);

	mAddRole->setIcon(QIcon(":/icons/back"));
	mAddRole->setToolTip(tr("Dodaj rolę"));
	mRemoveRole->setIcon(QIcon(":/icons/next"));
	mRemoveRole->setToolTip(tr("Usuń rolę"));

	auto btnLayout = new QVBoxLayout{};
	btnLayout->addStretch(1);
	btnLayout->addWidget(mAddRole);
	btnLayout->addWidget(mRemoveRole);
	btnLayout->addStretch(1);

	auto leftLayout = new QVBoxLayout{};
	leftLayout->addWidget(new QLabel(tr("Role użytkownika"), this), 0, Qt::AlignLeft);
	leftLayout->addWidget(mRolesView);

	auto rightLayout = new QVBoxLayout{};
	rightLayout->addWidget(new QLabel(tr("Wszystkie role"), this), 0, Qt::AlignLeft);
	rightLayout->addWidget(mAllRolesView);

	auto mainLayout = new QHBoxLayout{this};
	mainLayout->addLayout(leftLayout);
	mainLayout->addSpacing(3);
	mainLayout->addLayout(btnLayout);
	mainLayout->addSpacing(3);
	mainLayout->addLayout(rightLayout);
}

void RoleEditWidget::createConnections() {
	connect(mAddRole, &QPushButton::clicked, this, &RoleEditWidget::addRole);
	connect(mRemoveRole, &QPushButton::clicked, this, &RoleEditWidget::removeRole);
	// add role by double click
	connect(mAllRolesView, &RoleView::activated, this, [this](const QModelIndex& index){
		const core::eloquent::DBIDKey& roleId = index.data(models::RoleModel::Roles::Id).toInt();
		Q_ASSERT(roleId);
		if (roleId > 0) {
			auto& userRepository = core::GlobalData::instance().repository().userRepository();
			mAllRoles->removeRole(roleId);
			mRoles->addRole(userRepository.getRole(roleId));
		}
	});
	// remove role by double click
	connect(mRolesView, &RoleView::activated, this, [this](const QModelIndex& index){
		const core::eloquent::DBIDKey& roleId = index.data(models::RoleModel::Roles::Id).toInt();
		Q_ASSERT(roleId > 0);
		if (roleId > 0) {
			auto& userRepository = core::GlobalData::instance().repository().userRepository();
			mRoles->removeRole(roleId);
			mAllRoles->addRole(userRepository.getRole(roleId));
		}
	});
}

void RoleEditWidget::createModels() {
	mRoles		= new models::RoleModel(this);
	mAllRoles	= new models::RoleModel(this);

	mRolesView->setModel(mRoles);
	mAllRolesView->setModel(mAllRoles);

	// load all roles from repository
	mAllRoles->setRoles(core::GlobalData::instance().repository().userRepository().getAllRoles());
}

void RoleEditWidget::addRole() {
	const auto& indexes = mAllRolesView->selectionModel()->selectedIndexes();
	if (indexes.empty())
		return;

	auto& userRepository = core::GlobalData::instance().repository().userRepository();
	for (const auto& index : indexes) {
		const core::eloquent::DBIDKey& roleId = mAllRolesView->model()->data(index, models::RoleModel::Roles::Id).toInt();
		if (roleId <= 0)
			continue;

		mAllRoles->removeRole(roleId);
		mRoles->addRole(userRepository.getRole(roleId));
	}
}

void RoleEditWidget::removeRole() {
	const auto& indexes = mRolesView->selectionModel()->selectedIndexes();
	if (indexes.empty())
		return;

	auto& userRepository = core::GlobalData::instance().repository().userRepository();
	for (const auto& index : indexes) {
		const core::eloquent::DBIDKey& roleId = mRolesView->model()->data(index, models::RoleModel::Roles::Id).toInt();
		if (roleId <= 0)
			continue;

		mRoles->removeRole(roleId);
		mAllRoles->addRole(userRepository.getRole(roleId));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RoleView::RoleView(QWidget* parent)
	: QListView {parent}
{
	setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);
	setAcceptDrops(true);
	setDragEnabled(true);
	setDragDropMode(QListView::DragDropMode::DragDrop);
	setDefaultDropAction(Qt::DropAction::MoveAction);
}

void RoleView::dragEnterEvent(QDragEnterEvent* event) {
	event->acceptProposedAction();
}

void RoleView::dragMoveEvent(QDragMoveEvent* event) {
	event->acceptProposedAction();
}

void RoleView::mouseMoveEvent(QMouseEvent* event) {
	if (event->buttons() & Qt::LeftButton) {
		const core::eloquent::DBIDKey& roleId = currentIndex().data(models::RoleModel::Roles::Id).toInt();
		if (roleId > 0) {
			// first try remove item from model
			models::RoleModel* model = dynamic_cast<models::RoleModel*>(this->model());
			if (!model)
				return;

			model->removeRole(roleId);

			// and now create drag object
			QDrag* drag = new QDrag(this);
			QMimeData* mimeData = new QMimeData();
			mimeData->setProperty("roleId", roleId);
			drag->setMimeData(mimeData);
			drag->exec();
		}
	}
}

void RoleView::dropEvent(QDropEvent* event) {
	const QMimeData* mimeData = event->mimeData();
	const core::eloquent::DBIDKey& roleId = mimeData->property("roleId").toInt();
	if (roleId > 0) {
		models::RoleModel* model = dynamic_cast<models::RoleModel*>(this->model());
		if (model) {
			auto& userRepository = core::GlobalData::instance().repository().userRepository();
			model->addRole(userRepository.getRole(roleId));
		}
	}
}


} // namespace widgets
