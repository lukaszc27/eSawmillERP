#include "eventswidget.hpp"
#include <QVBoxLayout>

namespace widgets {

EventsWidget::EventsWidget(QWidget* parent)
	: QWidget {parent}
{
	setWindowTitle(tr("Zdarzenia"));
	createWidgets();
	createModels();
}

void EventsWidget::notifyEventReceived(const agent::RepositoryEvent& event) {
	mEventsModel->addEvent(event);
}

void EventsWidget::createWidgets() {
	mEventsView = new QTreeView(this);
	mEventsView->setSortingEnabled(true);
	mEventsView->sortByColumn(models::EventsModel::Columns::Time, Qt::SortOrder::DescendingOrder);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mEventsView, 1);
}

void EventsWidget::createModels() {
	mEventsModel = new models::EventsModel(this);
	mEventsProxyModel = new filters::EventsProxyModel(this);
	mEventsProxyModel->setSourceModel(mEventsModel);
	mEventsView->setModel(mEventsProxyModel);
}

} // namespace widgets

