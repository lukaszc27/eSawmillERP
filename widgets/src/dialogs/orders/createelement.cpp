#include "dialogs/orders/createelement.h"
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <models/woodtypelistmodel.hpp>
#include "models/orders/elementnamemodel.hpp"
#include <settings/appsettings.hpp>

namespace widgets::dialogs::orders {

CreateElement::CreateElement(QWidget* parent, unsigned int id)
	: QDialog(parent)
{
	Q_UNUSED(id);

	setWindowTitle(tr("Dodawanie elementu konstrukcyjnego"));

	createWidgets();
	createConnections();

	// assign model to view
	mWoodType->setModel(new core::models::WoodTypeListModel(this));

	// set default values in widget
	mName->setCurrentText(core::settings::AppSettings::instance().order().defaultItemName());
	mCount->setValue(core::settings::AppSettings::instance().order().defaultItemQuantity());
}

core::order::Element CreateElement::element()
{
	core::order::Element element;
	element.setWidth(mWidth->value());
	element.setHeight(mHeight->value());
	element.setLength(mLength->value());
	element.setQuantity(mCount->value());
	element.setPlanned(mPlanned->isChecked());
	element.setName(mName->currentText());
	element.setWoodType(mWoodType->currentData(core::models::WoodTypeListModel::Roles::Id).toInt());
	return element;
}

void CreateElement::createWidgets()
{
	mAcceptButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
	mWidth = new QDoubleSpinBox(this);
	mHeight = new QDoubleSpinBox(this);
	mLength = new QDoubleSpinBox(this);
	mCount = new QSpinBox(this);
	mName = new QComboBox(this);
	mWoodType = new QComboBox(this);
	mPlanned = new QCheckBox(tr("&Struganie"), this);

	QList<QDoubleSpinBox*> doubleSpinsBox = findChildren<QDoubleSpinBox*>();
	for (auto doubleSpinBox : doubleSpinsBox)
		doubleSpinBox->setMaximum(10000);
	mCount->setMaximum(10000);

	mName->setEditable(true);
	mName->setModel(new ::widgets::models::orders::ElementName(this));

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QGroupBox* sizeGroup = new QGroupBox(tr("Wymiary"), this);
	QGroupBox* detailsGroup = new QGroupBox(tr("Właściwości"), this);

	sizeGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	auto createLayoutWithLabel = [this](QWidget* field, const QString& label) -> QLayout* {
		QHBoxLayout* layout = new QHBoxLayout;
		layout->addWidget(field);
		layout->addWidget(new QLabel(label, this));
		return layout;
	};

	QList<QLabel*> labels = findChildren<QLabel*>();
	for (auto label : labels) {
		label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	}

	QFormLayout* formLayout = new QFormLayout(sizeGroup);
	formLayout->addRow(tr("Szerokość"), createLayoutWithLabel(mWidth, "cm"));
	formLayout->addRow(tr("Wysokość"), createLayoutWithLabel(mHeight, "cm"));
	formLayout->addRow(tr("Długość"), createLayoutWithLabel(mLength, "m"));
	formLayout->addRow(tr("Ilość"), createLayoutWithLabel(mCount, "szt"));

	QFormLayout* detailsLayout = new QFormLayout(detailsGroup);
	detailsLayout->addRow(tr("Nazwa"), mName);
	detailsLayout->addRow(tr("Rodzaj"), mWoodType);
	detailsLayout->addRow("", mPlanned);

	QHBoxLayout* groupLayout = new QHBoxLayout;
	groupLayout->addWidget(sizeGroup);
	groupLayout->addWidget(detailsGroup);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(groupLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);

	mWidth->selectAll();
	mWidth->setFocus();
}

void CreateElement::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateElement::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateElement::reject);
}

} // namespace widgets::dialogs::orders
