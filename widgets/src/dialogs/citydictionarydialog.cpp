#include "dialogs/citydictionarydialog.hpp"
#include <QLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QInputDialog>
#include "messagebox.h"
#include <exceptions/notimplementexception.hpp>
#include <exceptions/repositoryexception.hpp>
#include <globaldata.hpp>

namespace widgets::dialogs {

CityDictionaryDialog::CityDictionaryDialog(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle(tr("Słownik miast"));

	createAllWidgets();
	createConnections();

	mModel = new models::CityDictionaryModel(this);
	mListView->setModel(mModel);
}

CityDictionaryDialog::~CityDictionaryDialog() {}

void CityDictionaryDialog::accept()
{
	const QString currentCity = mListView->currentIndex().data().toString();
	if (currentCity.isEmpty()) {
		MessageBox::warning(this, tr("Słownik miast"), tr("Nie wybrałeś miasta!"));
		return;
	}

	mCurrentSelectedCity = currentCity;
	QDialog::accept();
}

void CityDictionaryDialog::createAllWidgets()
{
	mListView = new QListView(this);
	mAddButton = new PushButton(tr("Dodaj"), QKeySequence("Ins"), this, QIcon(":/icons/add"));
	mAcceptButton = new PushButton(tr("Wybierz"), QKeySequence("F10"), this);
	mRejectButton = new PushButton(tr("Anuluj"), QKeySequence("ESC"), this);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mListView);
	mainLayout->addSpacing(24);
	mainLayout->addLayout(buttonsLayout);
}

void CityDictionaryDialog::createConnections()
{
	connect(mAcceptButton, &PushButton::clicked, this, &CityDictionaryDialog::accept);
	connect(mRejectButton, &PushButton::clicked, this, &CityDictionaryDialog::reject);
	connect(mAddButton, &PushButton::clicked, this, &CityDictionaryDialog::addButtonHandle);

	connect(mListView, &QListView::doubleClicked, this, [&](const QModelIndex &index){
		Q_UNUSED(index);
		accept();
	});
}

void CityDictionaryDialog::addButtonHandle()
{
	try {
		const QString cityName = QInputDialog::getText(this, tr("Nowe miasto"), tr("Nazwa"));
		if (cityName.isEmpty())
			return;

		mModel->create(cityName);
	} catch (core::exceptions::RepositoryException& ex) {
		::widgets::MessageBox::critical(this, tr("Błąd dostawcy danych"),
			tr("Podczas tworzenia nowego miasta w słowniku wystąpił błąd podczas zapisu w repozytorium!\n"
			   "Spróbuj ponownie, w przypadku kolejnego wystąpienia błędu skontaktuj się z dostawcą oprogramowania"));

		// log exception into log file
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Wyjątek core::exceptions::RepositoryException podczas w %s (what: %s)", Q_FUNC_INFO, ex.what()));
	} catch (core::exceptions::NotImplementException& ex) {
		::widgets::MessageBox::critical(this, tr("Brak implementacji funkcjonalności"),
			tr("Brak zaimplementowanej funkcjonalności u dostawcy danych!\n"
			   "Błąd może być spowodowany niezgodnością wersji klienta oraz serwera"));

		// log exception into log file
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Wyjątek core::exceptions::NotImplementException w %s (what: %s)", Q_FUNC_INFO, ex.what()));
	}
}

} // namespace widgets
