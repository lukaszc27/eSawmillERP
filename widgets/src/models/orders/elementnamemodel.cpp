#include "models/orders/elementnamemodel.hpp"

namespace widgets::models::orders {

ElementName::ElementName(QObject* parent)
	: QStringListModel(parent)
{
	setStringList(QStringList()
		<< "Dźwigar"
		<< "Kratownica"
		<< "Gąsior"
		<< "Jętka"
		<< "Kalenica"
		<< "Kleszcze"
		<< "Kontrłata"
		<< "Krokiew"
		<< "Krawężnica"
		<< "Kulawka"
		<< "Łata"
		<< "Miecz"
		<< "Murłata"
		<< "Płatew"
		<< "Wiartownica"
		<< "Słup"
		<< "Belka"
		<< "Belka podwalinowa"
		<< "Zastrzał");
}

} // namespace widgets::models::orders
