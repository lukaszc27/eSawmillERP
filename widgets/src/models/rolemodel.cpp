#include "models/rolemodel.hpp"

namespace widgets::models {

RoleModel::RoleModel(QObject* parent)
	: QAbstractListModel {parent}
{
}

int RoleModel::rowCount(const QModelIndex& index) const {
	Q_UNUSED(index);
	return _roles.size();
}

QVariant RoleModel::data(const QModelIndex& index, int role) const {
	if (!index.isValid())
		return QVariant{};

	if (index.column() == 0 && index.row() >= 0 && index.row() < _roles.size()) {
		const auto& current = _roles.at(index.row());
		if (role == Qt::DisplayRole) {
			return current.name();
		}

		if (role == Roles::Id)
			return current.id();
	}
	return QVariant{};
}

void RoleModel::removeRoles(const core::RoleCollection& roles) {
	if (roles.size() > 0) {
		beginResetModel();
		for (const auto& role : roles) {
			auto itr = std::find(_roles.begin(), _roles.end(), role);
			if (itr != _roles.end())
				_roles.erase(itr);
		}
		endResetModel();
	}
}

void RoleModel::setRoles(const core::RoleCollection& roles) {
	beginResetModel();
	_roles = roles;
	endResetModel();
}

void RoleModel::addRole(const core::Role& role) {
	Q_ASSERT(role);
	bool exist = std::any_of(_roles.begin(), _roles.end(),
		[&role](const core::Role& item)->bool {
			return item.id() == role.id();
	});
	if (exist)
		return;	// this role exist in model

	beginResetModel();
	_roles.append(role);
	endResetModel();
}

void RoleModel::removeRole(const core::eloquent::DBIDKey& roleId) {
	Q_ASSERT(roleId > 0);
	auto itr = std::find_if(_roles.begin(), _roles.end(),
		[&roleId](const core::Role& item)->bool {
			return item.id() == roleId;
	});
	if (itr != _roles.end()) {
		beginResetModel();
		_roles.erase(itr);
		endResetModel();
	}
}



} // namespace widgets::models
