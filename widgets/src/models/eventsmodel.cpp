#include "models/eventsmodel.hpp"
#include <globaldata.hpp>
#include <unordered_map>

namespace agent = core::providers::agent;

namespace widgets::models {

EventsModel::EventsModel(QObject* parent)
	: QAbstractTableModel {parent}
{
}

void EventsModel::addEvent(const core::providers::agent::RepositoryEvent& event) {
	beginResetModel();
	std::unordered_map<agent::ResourceOperation, QString> operationsName;
	operationsName[agent::ResourceOperation::Logged] = tr("Zalogowano");
	operationsName[agent::ResourceOperation::Logout] = tr("Wylogowano");
	operationsName[agent::ResourceOperation::Created] = tr("Utworzono");
	operationsName[agent::ResourceOperation::Updated] = tr("Zaaktualizowano");
	operationsName[agent::ResourceOperation::Removed] = tr("Usunięto");

	std::unordered_map<agent::Resource, QString> resourcesName;
	resourcesName[agent::Resource::Article] = tr("artykuł");
	resourcesName[agent::Resource::Company] = tr("firmę");
	resourcesName[agent::Resource::Contractor] = tr("kontrahenta");
	resourcesName[agent::Resource::Order] = tr("zamówienie");
	resourcesName[agent::Resource::PurchaseInvoice] = tr("fakturę zakupu");
	resourcesName[agent::Resource::SaleInvoice] = tr("fakturę sprzedaży");
	resourcesName[agent::Resource::Service] = tr("uslugę");
	resourcesName[agent::Resource::User] = tr("użytkownika");
	resourcesName[agent::Resource::Warehouse] = tr("magazyn");
	resourcesName[agent::Resource::WoodType] = tr("Rodzaj drewna");

	const auto& user = core::GlobalData::instance()
		.repository()
		.userRepository()
		.getUser(event.userId);

	const QString userName = user.firstName() + " " + user.surName();
	QString operation = operationsName[event.operation] + " " + resourcesName[event.resourceType];

	QString what;
	if (event.operation != agent::ResourceOperation::Removed) {
		if (event.resourceType == agent::Resource::Article) {
			const auto& article = core::GlobalData::instance()
				.repository()
				.articleRepository()
				.getArticle(event.resourceId);
			what = article.name();
		} else if (event.resourceType == agent::Resource::Contractor) {
			const auto& contractor = core::GlobalData::instance()
				.repository()
				.contractorRepository()
				.get(event.resourceId);

			if (!contractor.name().isEmpty())
				what = contractor.name();
			else what = contractor.personName() + " " + contractor.personSurname();
		} else if (event.resourceType == agent::Resource::Warehouse) {
			const auto& warehouse = core::GlobalData::instance()
				.repository()
				.warehousesRepository()
				.getWarehouse(event.resourceId);

			what = warehouse.name();
		} else if (event.resourceType == agent::Resource::PurchaseInvoice) {
			const auto& purchaseInvoice = core::GlobalData::instance()
				.repository()
				.invoiceRepository()
				.getPurchaseInvoice(event.resourceId);

			what = purchaseInvoice.number();
		} else if (event.resourceType == agent::Resource::SaleInvoice) {
			const auto& saleInvoice = core::GlobalData::instance()
				.repository()
				.invoiceRepository()
				.getSaleInvoice(event.resourceId);

			what = saleInvoice.number();
		} else if (event.resourceType == agent::Resource::Order) {
			const auto& order = core::GlobalData::instance()
				.repository()
				.orderRepository()
				.getOrder(event.resourceId);

			what += QString("%1 (%2)").arg(order.number(), order.name());
		} else if (event.resourceType == agent::Resource::Service) {
			const auto& service = core::GlobalData::instance()
				.repository()
				.serviceRepository()
				.get(event.resourceId);

			what += QString("%1 (%2)").arg(service.number(), service.name());
		} else if (event.resourceType == agent::Resource::User) {
			const auto& user = core::GlobalData::instance()
				.repository()
				.userRepository()
				.getUser(event.resourceId);

			what += QString("%1 %2").arg(user.firstName(), user.surName());
		} else if (event.resourceType == agent::Resource::WoodType) {
			const auto& wood = core::GlobalData::instance()
				.repository()
				.woodTypeRepository()
				.get(event.resourceId);

			what += " " + wood.name();
		}
	}

	if (!what.isEmpty())
		operation.append(QString(" %1").arg(what));

	// add event to list
	mEvents.append(std::make_tuple(
		userName,
		QTime::currentTime(),
		operation
	));

	endResetModel();
}

int EventsModel::rowCount(const QModelIndex& index) const {
	Q_UNUSED(index);
	return mEvents.size();
}

int EventsModel::columnCount(const QModelIndex& index) const {
	Q_UNUSED(index);
	return 3;
}

QVariant EventsModel::headerData(int section, Qt::Orientation orientation, int role) const {
	const QStringList titles = QStringList()
		<< tr("Użytkownik")
		<< tr("Czas")
		<< tr("Zdarzenie");

	if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
		return titles.at(section);
	}
	return QVariant {};
}

QVariant EventsModel::data(const QModelIndex& index, int role) const {
	Q_UNUSED(index);
	Q_UNUSED(role);
	int col = index.column();
	int row = index.row();

	const auto& [user, time, description] = mEvents.at(row);
	if (role == Qt::DisplayRole) {
		switch (col) {
		case Columns::User:
			return user;
			break;
		case Columns::Time:
			return time.toString();
			break;
		case Columns::Description:
			return description;
			break;
		}
	}

	return QVariant{};
}

} // namespace widgets::models
