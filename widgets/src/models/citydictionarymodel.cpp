#include "models/citydictionarymodel.hpp"
#include <globaldata.hpp>
#include "messagebox.h"

namespace widgets {
namespace models {

CityDictionaryModel::CityDictionaryModel(QObject* parent)
	: QAbstractListModel(parent)
{
	get();
}

int CityDictionaryModel::rowCount(const QModelIndex &index) const
{
	Q_UNUSED(index);
	return mList.size();
}

QVariant CityDictionaryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && section == 0) {
		switch (role) {
		case Qt::DisplayRole:
			return QString("Miasto");
		}
	}
	return QVariant();
}

QVariant CityDictionaryModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	const auto& current = mList.at(row);

	switch (role) {
	case Qt::DisplayRole:
		return current.name();
	}
	return QVariant();
}

void CityDictionaryModel::get()
{
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.cityRepository()
			.getAll();
		endResetModel();
	} catch (...) {
		throw;
	}
}

void CityDictionaryModel::create(const QString &name) {
	try {
		QString cityName = name.trimmed();
		auto& repository = core::GlobalData::instance().repository().cityRepository();
		if (!repository.hasCity(cityName)) {
			core::City city {};
			city.setName(cityName);
			repository.create(city);

			// after added new city, refresh model list
			get();
		} else {
			::widgets::MessageBox::information(static_cast<QWidget*>(parent()),
				tr("Inforamcja"),
				tr("Podane miasto już istnieje w słowniku"));
		}
	} catch(...) {
		throw;
	}
}

} // namespace models
} // namespace widgets
