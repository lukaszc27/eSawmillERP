#include "messagebox.h"
#include <QScopedPointer>
#include <QApplication>


namespace widgets {

int MessageBox::information(QWidget *parent,
							const QString &title,
							const QString &text,
							const QString &detailedText,
							QMessageBox::StandardButtons buttons)
{
	QScopedPointer<QMessageBox> messageBox(new QMessageBox(parent));
	if (parent)
		messageBox->setWindowModality(Qt::WindowModal);

	messageBox->setWindowTitle(title);
	messageBox->setText(text);
	if (!detailedText.isEmpty())
		messageBox->setDetailedText(detailedText);
	messageBox->setIcon(QMessageBox::Information);

	if (buttons.testFlag(QMessageBox::Ok)) messageBox->addButton(QMessageBox::Ok);
	if (buttons.testFlag(QMessageBox::Yes)) messageBox->addButton(QMessageBox::Yes);
	if (buttons.testFlag(QMessageBox::No)) messageBox->addButton(QMessageBox::No);

	return messageBox->exec();
}

int MessageBox::question(QWidget *parent,
						 const QString &title,
						 const QString &text,
						 const QString &detailedText,
						 QMessageBox::StandardButtons buttons)
{
	QScopedPointer<QMessageBox> messageBox(new QMessageBox(parent));
	if (parent)
		messageBox->setWindowModality(Qt::WindowModal);

	messageBox->setWindowTitle(title);
	messageBox->setText(text);
	if (!detailedText.isEmpty())
		messageBox->setDetailedText(detailedText);
	messageBox->setIcon(QMessageBox::Question);

	if (buttons.testFlag(QMessageBox::Ok)) messageBox->addButton(QMessageBox::Ok);
	if (buttons.testFlag(QMessageBox::Yes)) messageBox->addButton(QMessageBox::Yes);
	if (buttons.testFlag(QMessageBox::No)) messageBox->addButton(QMessageBox::No);

	return messageBox->exec();
}

int MessageBox::warning(QWidget *parent,
						const QString &title,
						const QString &text,
						const QString &detailedText,
						QMessageBox::StandardButtons buttons)
{
	QScopedPointer<QMessageBox> messageBox(new QMessageBox(parent));
	if (parent)
		messageBox->setWindowModality(Qt::WindowModal);

	messageBox->setWindowTitle(title);
	messageBox->setText(text);
	if (!detailedText.isEmpty())
		messageBox->setDetailedText(detailedText);
	messageBox->setIcon(QMessageBox::Warning);

	if (buttons.testFlag(QMessageBox::Ok)) messageBox->addButton(QMessageBox::Ok);
	if (buttons.testFlag(QMessageBox::Yes)) messageBox->addButton(QMessageBox::Yes);
	if (buttons.testFlag(QMessageBox::No)) messageBox->addButton(QMessageBox::No);

	return messageBox->exec();
}

int MessageBox::critical(QWidget *parent,
						 const QString &title,
						 const QString &text,
						 const QString &detailedText,
						 QMessageBox::StandardButtons buttons)
{
	QScopedPointer<QMessageBox> messageBox(new QMessageBox(parent));
	if (parent)
		messageBox->setWindowModality(Qt::WindowModal);

	messageBox->setWindowTitle(title);
	messageBox->setText(text);
	if (!detailedText.isEmpty()) {
		messageBox->setDetailedText(detailedText);
	}
	messageBox->setIcon(QMessageBox::Critical);

	if (buttons.testFlag(QMessageBox::Ok)) messageBox->addButton(QMessageBox::Ok);
	if (buttons.testFlag(QMessageBox::Yes)) messageBox->addButton(QMessageBox::Yes);
	if (buttons.testFlag(QMessageBox::No)) messageBox->addButton(QMessageBox::No);

	return messageBox->exec();
}

} // namespace widgets
