#include "contactwidget.hpp"
#include <QLabel>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QCompleter>
#include <QCloseEvent>
#include <models/stateslistmodel.hpp>
#include "models/citydictionarymodel.hpp"
#include "dialogs/citydictionarydialog.hpp"
#include <memory>

namespace widgets {

ContactWidget::ContactWidget(core::Contact& contact, QWidget* parent)
	: QWidget(parent)
	, _contact {contact}
{
	createAllWidgets();
	createConnections();

	// set startup values
	mCountryIso->setInputMask(">AA;_");
	mPostCode->setInputMask("99-999;_");
	mPhone->setInputMask("+99 999999999;_");
	mPhone->setText(tr("+48"));

	// load contact object
	load(contact);
}

void ContactWidget::load(const core::Contact& contact) {
	if (!contact)
		return;

	_contact = contact;
	mCountry->setText(contact.country());
	mCountryIso->setText(contact.IsoCountry());
	mCity->setText(contact.city());
	mStreet->setText(contact.street());
	mPostOffice->setText(contact.postOffice());
	mPostCode->setText(contact.postCode());
	mHomeNumber->setText(contact.homeNumber());
	mLocalNumber->setText(contact.localNumber());
	mMail->setText(contact.email());
	mUrl->setText(contact.url());
	mState->setCurrentText(contact.state());
	mPhone->setText(contact.phone());
}

core::Contact& ContactWidget::contact() {
	prepareContactObject();
	return _contact;
}

void ContactWidget::createAllWidgets() {
	mCountry		= new QLineEdit(this);
	mCity			= new QLineEdit(this);
	mPostOffice		= new QLineEdit(this);
	mStreet			= new QLineEdit(this);
	mPhone			= new QLineEdit(this);
	mCountryIso		= new QLineEdit(this);
	mState			= new QComboBox(this);
	mPostCode		= new QLineEdit(this);
	mHomeNumber		= new QLineEdit(this);
	mLocalNumber	= new QLineEdit(this);
	mMail			= new QLineEdit(this);
	mUrl			= new QLineEdit(this);
	mCityButton		= new QPushButton(tr("Miasto"), this);
	mPostOfficeButton = new QPushButton(tr("Poczta"), this);

	mCountryIso->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountryIso->setMaximumWidth(48);
	mCountryIso->setText(tr("PL"));	// def value

	mState->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mState->setEditable(true);
	mState->setModel(new core::models::StatesListModel(this));

	// create completer object for cities
	auto cityCompleter = new QCompleter(this);
	cityCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	cityCompleter->setModel(new ::widgets::models::CityDictionaryModel(this));
	cityCompleter->setCompletionRole(Qt::DisplayRole);
	cityCompleter->setCaseSensitivity(Qt::CaseInsensitive);

	// apply completer for city and post office fields
	mCity->setCompleter(cityCompleter);
	mPostOffice->setCompleter(cityCompleter);

	// set maximum size for home and local number fields
	mHomeNumber->setMaximumWidth(65);
	mHomeNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mLocalNumber->setMaximumWidth(65);
	mLocalNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// create left form layout
	auto left = new QFormLayout {};
	left->addRow(tr("Kraj"), mCountry);
	left->addRow(mCityButton, mCity);
	left->addRow(mPostOfficeButton, mPostOffice);
	left->addRow(tr("Ulica"), mStreet);
	left->addRow(tr("Telefon"), mPhone);

	auto streetNumberLayout = new QHBoxLayout{};
	streetNumberLayout->addWidget(mHomeNumber);
	streetNumberLayout->addWidget(new QLabel("/", this));
	streetNumberLayout->addWidget(mLocalNumber);
	streetNumberLayout->addStretch(1);

	auto right = new QFormLayout {};
	right->addRow(tr("Kraj ISO"), mCountryIso);
	right->addRow(tr("Województwo"), mState);
	right->addRow(tr("Kod pocztowy"), mPostCode);
	right->addRow(tr(""), streetNumberLayout);
	right->addRow(tr("E-Mail"), mMail);
	right->addRow(tr("URL"), mUrl);

	auto mainLayout = new QHBoxLayout(this);
	mainLayout->addLayout(left);
	mainLayout->addSpacing(24);
	mainLayout->addLayout(right);
}

void ContactWidget::createConnections() {
	connect(mCityButton, &QPushButton::clicked, this, [this]()->void {
		showDictionaryDialog(mCity);
	});
	connect(mPostOfficeButton, &QPushButton::clicked, this, [this]()->void {
		showDictionaryDialog(mPostOffice);
	});
}

void ContactWidget::showDictionaryDialog(QLineEdit* lineEdit) {
	std::unique_ptr<::widgets::dialogs::CityDictionaryDialog> dialog
		= std::make_unique<::widgets::dialogs::CityDictionaryDialog>(this);
	if (dialog->exec() == QDialog::Accepted) {
		if (lineEdit != nullptr)
			lineEdit->setText(dialog->city());
	}
}

void ContactWidget::prepareContactObject() {
	_contact.setCountry(mCountry->text());
	_contact.setIsoCountry(mCountryIso->text());
	_contact.setCity(mCity->text());
	_contact.setStreet(mStreet->text());
	_contact.setPostCode(mPostCode->text());
	_contact.setPostOffice(mPostOffice->text());
	_contact.setHomeNumber(mHomeNumber->text());
	_contact.setLocalNumber(mLocalNumber->text());
	_contact.setEmail(mMail->text());
	_contact.setUrl(mUrl->text());
	_contact.setState(mState->currentText());
	_contact.setPhone(mPhone->text());
}

void ContactWidget::closeEvent(QCloseEvent* event) {
	prepareContactObject();
	event->accept();
}

} // namespace widgets
