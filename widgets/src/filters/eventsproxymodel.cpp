#include "filters/eventsproxymodel.hpp"
#include "models/eventsmodel.hpp"
#include <QTime>

namespace widgets::filters {

EventsProxyModel::EventsProxyModel(QObject* parent)
	: QSortFilterProxyModel {parent}
{
}

bool EventsProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const {
	QVariant leftData = sourceModel()->data(left);
	QVariant rightData = sourceModel()->data(right);

	if (left.column() == ::widgets::models::EventsModel::Columns::Time &&
		right.column() == ::widgets::models::EventsModel::Columns::Time)
	{
		QTime leftTime (leftData.toTime());
		QTime rightTime(rightData.toTime());
		return rightTime > leftTime;
	}
	return leftData.toString().compare(rightData.toString());
}

}
