#ifndef UNREALISEDORDERSVERIFICATION_HPP
#define UNREALISEDORDERSVERIFICATION_HPP

#include "../orders_global.h"
#include <verifications/abstractverification.hpp>


namespace eSawmill::orders::verifications {

class ORDERS_EXPORT UnrealisedOrdersVerification : public core::verifications::AbstractVerification {
public:
	explicit UnrealisedOrdersVerification();
	virtual ~UnrealisedOrdersVerification() = default;

	virtual void execute() override final;
};

} // namespace eSawmill::orders::verifications

#endif // UNREALISEDORDERSVERIFICATION_HPP
