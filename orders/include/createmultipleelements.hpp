#ifndef CREATEMULTIPLEELEMENTS_HPP
#define CREATEMULTIPLEELEMENTS_HPP

#include "orders_global.h"
#include <tableview.h>
#include <pushbutton.h>

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include "models/elements.h"


namespace eSawmill::orders {

/**
 * @brief The CreateMultipleElements class
 * dialog tworzenia grupy elementów kontrukcyjnych
 * (podawane raz są tylko szerokość oraz wysokość)
 * a długość oraz ilość zmieniana jest po dodaniu
 * elementu do listy tymczasowej
 */
class ORDERS_EXPORT CreateMultipleElements : public QDialog {
	Q_OBJECT

public:
	explicit CreateMultipleElements(QWidget* parent = nullptr);

	/**
	 * @brief items - zwraca listę utwożonych elementów
	 * @return QList
	 */
	QList<core::order::Element> items();

private:
	void createAllWidgets();
	void createConnections();
	void createModels();

	QDoubleSpinBox* mWidth;
	QDoubleSpinBox* mHeight;
	QDoubleSpinBox* mLength;
	QDoubleSpinBox* mCount;
	QCheckBox* mPlanned;
	QComboBox* mWoodType;
	QComboBox* mName;
	QCheckBox* mShowPlannedColumn;

	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;

	orders::models::Elements* mElements;

private Q_SLOTS:
	/**
	 * @brief returnHandle
	 * slot obsługujący kliknięcie przycisku Dodaj
	 */
	void returnHandle();

	/**
	 * @brief removeButtonHandle
	 * remove selected items from list
	 */
	void removeButtonHandle();
};

} // namespace eSawmill

#endif // CREATEMULTIPLEELEMENTS_HPP
