#ifndef ORDER_ARTICLES_HPP
#define ORDER_ARTICLES_HPP

#include "orders_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/article.h>
#include <dbo/orderarticle.h>

namespace eSawmill::orders::models {

class ORDERS_EXPORT Articles : public core::models::AbstractModel<core::Article> {
	Q_OBJECT

public:
	///@{
	explicit Articles(QObject* parent = nullptr);
	explicit Articles(int id, QObject* parent = nullptr);
	virtual ~Articles() = default;
	///@}

	///
	/// \brief setOrder
	/// assign order to model
	/// \param orderId
	///
	void setOrder(core::eloquent::DBIDKey orderId);

	///@{
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	QStringList headerData() const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	///@}

	void insert(core::Article element) override;
	void insert(QList<core::Article> elements) override;

	///
	/// \brief totalPrice
	/// override function from AbstractModel to return sum of all elements prices
	/// \return
	///
	double totalPrice() override;

	///
	/// \brief The Columns enum
	/// column indexes uses in model
	///
	enum Columns {
		Name,
		EAN,
		Quantity,
		Unit,
		Price,
		Value
	};

	///@{
	/// THIS METHODS IS MARKED AS DEPRECATED
	/// IN FUTURE CAN BE REMOVED IN NEXT CODE REFACTORING
	void get() override;
	void createOrUpdate(core::Article element) override;
	void destroy(core::Article element) override;
	///@}

protected:
	core::eloquent::DBIDKey mId;

Q_SIGNALS:
	///
	/// \brief articlesChanged
	/// emitted when user add new article to model
	/// used in widget (CreateOrder or CreateService widget)
	///
	void articlesChanged();
};

} // namespace eSawmill

#endif // ORDER_ARTICLES_HPP
