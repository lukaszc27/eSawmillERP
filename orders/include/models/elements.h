#ifndef ELEMENTS_H
#define ELEMENTS_H

#include <QIODevice>
#include <QDomDocument>

#include <abstracts/abstractelementsmodel.hpp>
#include <dbo/orderElements.h>

namespace eSawmill::orders::models {

///
/// \brief The Elements class
/// basic model to display list of assigned elements to order
///
class Elements : public core::models::AbstractElementsModel<core::order::Element> {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit Elements(core::Order& order, QObject* parent = nullptr);
	virtual ~Elements() = default;
	///@}

	///
	/// \brief totalCubature
	/// sum all elements cubature
	/// \return
	///
	double totalCubature() const final override;
	double totalPlannedCubature() const final override;

	///
	/// \brief importFromXML
	/// import order elements from XML document
	/// \param doc
	/// \return
	///
	bool importFromXML(const QDomDocument* doc) override;
	QDomDocument exportToXML() override;
	QDomElement toDomElement() const override;
	void fromDomElement(QDomElement& element) override;

	///@{
	/// standard methods to cooperate with view to display data
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	///@}

	///@{
	/// THIS METHOS ARE MARK AS DEPRECATED
	/// CAN BE REMOVED IN NEXT REFACTORING
	void createOrUpdate(core::order::Element element) override;
	void destroy(core::order::Element element) override;
	///@}
	void get() override;

	///
	/// \brief The Columns enum
	/// column indexes
	///
	enum Columns {
		Name,
		WoodType,
		Width,
		Height,
		Length,
		Quantity,
		IsPlanned,
		Cubature
	};

	///
	/// \brief The Roles enum
	/// additional roles uses in model
	///
	enum Roles {
		Id = Qt::UserRole + 1,	///< element ID from database
		Planned					///< information about planing
	};

public Q_SLOTS:
	///
	/// \brief totalPrice
	/// total cost of elements added to the order (including planing and discount)
	/// \return
	///
	double totalPrice() override;

protected:
	void notifyDataChanged() override;
	void notifySpareLengthChanged(double spareLength) override;

private:
	core::Order& _order;	///< reference to current editing order

Q_SIGNALS:
	void dataChanged();	///< emitted by notifyDataChanged
};

} // namespace eSawmill

#endif // ELEMENTS_H
