#ifndef ORDERS_MODEL_H
#define ORDERS_MODEL_H

#include <abstracts/abstractmodel.h>
#include <dbo/orderElements.h>
#include <dbo/order.h>
#include <commandexecutor.hpp>

namespace eSawmill::orders::models {
///
/// \brief The Orders class
/// basic model to operate on all orders
///
class Orders
		: public core::models::AbstractModel<core::Order>
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	explicit Orders(QObject* parent = nullptr);
	virtual ~Orders();

	///@{
	/// the methods uses by Qt framework to cooperate with views to present data
	int rowCount(const QModelIndex &index) const override;
	int columnCount(const QModelIndex &index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	///@}

	///
	/// \brief The Columns enum
	/// column indexes uses by model
	///
	enum Columns {
		Name,
		Number,
		ContractorName,
		ContractorPhone,
		Priority,
		AddDate,
		EndDate,
	};

	///
	/// \brief The Roles enum
	/// additional roles used by model
	///
	enum Roles {
		Id = Qt::UserRole + 1,
		IsRealised,
		WasGeneratedInvoice
	};

	///@{
	/// THIS METHODS ARE MARKED AS DEPRECATED
	/// YOU'T SCHOULDN'T USE IT BECAUSE CAN BE REMOVED IN NEXT REFACTORING TIME :)
	void createOrUpdate(core::Order element) override;
	void destroy(core::Order element) override;
	void get() override;
	///@}

private:
	// CommandExecutor::Listener implementation
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;

private:
	QStringList mHeaders;
//	QList<core::Order> m_orders;
};

} // namespace eSawmill


#endif // ORDERS_MODEL_H
