#ifndef ORDERSFILTER_HPP
#define ORDERSFILTER_HPP

#include "../orders_global.h"
#include <QSortFilterProxyModel>


namespace eSawmill::orders::filters {

class ORDERS_EXPORT OrdersFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	OrdersFilter(QObject* parent = nullptr);

public slots:
	void setActive(bool active);
	void setPriorityEnable(bool enable);
	void setName(const QString &name);
	void setNumber(const QString &number);
	void setPriority(const QString &priority);
	void setContractor(const QString &contractor);
	void setPhone(const QString &phone);

	/**
	 * @brief clearAllFilter
	 */
	void clearAllFilter();

private:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

protected:
	bool filterByName(const QString& value) const;
	bool filterByNumber(const QString &value) const;
	bool filterByPriority(const QString &value) const;
	bool filterByContractor(const QString &value) const;
	bool filterByPhone(const QString &value) const;

	bool mIsActive;
	bool mPriorityEnable;
	QString mName;
	QString mNumber;
	QString mPriority;
	QString mContractor;
	QString mPhone;
};

} // namespace eSawmill

#endif // ORDERSFILTER_HPP
