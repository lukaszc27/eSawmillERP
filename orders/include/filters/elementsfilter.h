#ifndef ELEMENTSFILTER_H
#define ELEMENTSFILTER_H

#include <QSortFilterProxyModel>


namespace eSawmill::orders::filters {

/**
 * filtr elementów konstrukcyjnych
 * umożliwia filtrowanie elementów
 * z długości oraz z przekroju
 */
class ElementsFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	ElementsFilter(QObject* parent = nullptr);

	/**
	 * uruchamia filtrowanie przez przekrój
	 * elementu (szer x wys)
	 */
	void enableFetchWithSection(bool enable);

	/**
	 * uruchamia filtrowanie elementów z długości
	 */
	void enableFetchWithLength(bool enable);

	/**
	 * @brief setFilterLength - ustawia parametry do filtra długości
	 * @param min - minimalna szukana długość
	 * @param max - maksymalna szukana długość
	 */
	void setFilterLength(double min, double max);

	/**
	 * @brief setFilterSection - ustawia parametry do filtra przekroju
	 * @param width - szerokość elementu
	 * @param height - wysokość elementu
	 */
	void setFilterSection(double width, double height);

public:
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
	bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
	double mMinLength, mMaxLength;
	double mWidth, mHeight;
	bool mFetchWithLength;
	bool mFetchWidthSection;
};

} // namespace eSawmill

#endif // ELEMENTSFILTER_H
