#ifndef ORDERDATERANGEFILTER_HPP
#define ORDERDATERANGEFILTER_HPP

#include "../orders_global.h"
#include <QSortFilterProxyModel>
#include <QDate>


namespace eSawmill::orders::filters {

class ORDERS_EXPORT OrderDateRangeFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	OrderDateRangeFilter(QObject* parent = nullptr);
	virtual ~OrderDateRangeFilter() {}

public slots:
	void setActive(bool enable) {
		mActive = enable;
		invalidateFilter();
	}

	void setMinDate(const QDate &date) {
		mMinDate = date;
		invalidateFilter();
	}

	void setMaxDate(const QDate &date) {
		mMaxDate = date;
		invalidateFilter();
	}

private:
	QDate mMinDate;
	QDate mMaxDate;
	bool mActive;

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

} // namespace

#endif // ORDERDATERANGEFILTER_HPP
