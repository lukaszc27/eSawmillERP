#ifndef ORDERROLEFILTER_H
#define ORDERROLEFILTER_H

#include "../orders_global.h"
#include <QSortFilterProxyModel>

namespace eSawmill::orders::filters {

/**
 * @brief The OrderRoleFilter class
 * filter orders by realised
 * user can change data in filter by left view in order manager
 */
class ORDERS_EXPORT OrderRoleFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	OrderRoleFilter(QObject* parent = nullptr);

	inline void filterByIndex(int index) {
		mIndex = index;
		invalidateFilter();
	}

	/**
	 * @brief The FilterIndexes enum
	 * indeksy kolumn według których odbywa się filtrowanie zamówień
	 */
	enum FilterIndexes {
		All = 0,		// wszystkie
		ToImplemented,	// do realizacji
		AfterDeadline,	// po terminie realizacji
		Realised		// zrealizowane
	};

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:
	unsigned int mIndex;
};

} // namespace eSawmill

#endif // ORDERROLEFILTER_H
