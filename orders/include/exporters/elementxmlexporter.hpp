#ifndef ELEMENTXMLEXPORTER_HPP
#define ELEMENTXMLEXPORTER_HPP

#include "../orders_global.h"
#include <abstracts/abstractexporter.hpp>
#include <dbo/orderElements.h>

namespace eSawmill::orders::exporters {

class ORDERS_EXPORT ElementXmlExporter : public core::abstracts::AbstractExporter<core::order::Element> {
public:
	explicit ElementXmlExporter();
	virtual ~ElementXmlExporter() = default;

	bool exportData(QIODevice& device) override;
	bool importData(QIODevice& data) override;

	QDomElement toDomNode(QDomDocument& document) override;
	void fromDomNode(const QDomElement& element) override;

protected:
	/// node names exported to file
	const QString widthTag		= "width";
	const QString heightTag		= "height";
	const QString lengthTag		= "length";
	const QString quantityTag	= "quantity";
	const QString woodTag		= "wood";
	const QString plannedTag	= "planned";
};

}

#endif // ELEMENTXMLEXPORTER_HPP
