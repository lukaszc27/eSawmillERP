#ifndef ORDERXMLEXPORTER_HPP
#define ORDERXMLEXPORTER_HPP

#include <abstracts/abstractexporter.hpp>
#include "../orders_global.h"
#include <dbo/order.h>


namespace eSawmill::orders::exporters {

///
/// \brief The OrderXmlExporter class
/// export or import multiple orders to XML file
/// this object is uses in normal export/import operation
/// and to importing orders by dropping XML file on order manager
///
class ORDERS_EXPORT OrderXmlExporter : public core::abstracts::AbstractExporter<core::Order> {
public:
	///@{
	/// ctors
	explicit OrderXmlExporter();
	virtual ~OrderXmlExporter() = default;
	///@}

	///
	/// \brief exportData export order direct to abstract input/output device
	/// \param device QFile or web socket
	/// \return true if operation will be successfull
	///
	bool exportData(QIODevice& device) override;

	///
	/// \brief importData import order direct from abstract source
	/// \param device - place where data will be search
	/// \return
	///
	bool importData(QIODevice& device) override;

	///
	/// \brief toDomNode - export one piece of order to XML structure
	/// and return valid object to save in main XML structure
	/// \param document
	/// \return
	///
	QDomElement toDomNode(QDomDocument& document) override;

	///
	/// \brief fromDomNode - import one piece of order from XML structure
	/// \param element
	///
	void fromDomNode(const QDomElement& element) override;

private:
	QDomElement exportOrder(QDomDocument& doc, core::Order& order);

protected:
	/// tag names uses to describe exported data in XML structure
	const QString nameTag			= "name";
	const QString endDateTag		= "endDate";
	const QString descriptionTag	= "description";
	const QString rootTag			= "order";
	const QString taxTag			= "Tax";
	const QString itemsNodeTag		= "items";
	const QString ordersNodeTag		= "orders";
};

} // namespace eSawmill::orders::exporters

#endif // ORDERXMLEXPORTER_HPP
