#ifndef ELEMENTCSVEXPORTER_HPP
#define ELEMENTCSVEXPORTER_HPP

#include "../orders_global.h"
#include "abstracts/abstractexporter.hpp"
#include <dbo/orderElements.h>
#include <QIODevice>

namespace eSawmill::orders::exporters {

class ORDERS_EXPORT ElementCsvExporter : public core::abstracts::AbstractExporter<core::order::Element> {
public:
	ElementCsvExporter();
	virtual ~ElementCsvExporter();

	bool exportData(QIODevice& device) override;
	bool importData(QIODevice& device) override;

	void setSeparator(char sep = ';') { mSeparator = sep; }
	char separator() const { return mSeparator; }

private:
	char mSeparator;
};

} // namespace

#endif // ELEMENTCSVEXPORTER_HPP
