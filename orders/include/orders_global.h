#ifndef ORDERS_GLOBAL_H
#define ORDERS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ORDERS_LIBRARY)
#  define ORDERS_EXPORT Q_DECL_EXPORT
#else
#  define ORDERS_EXPORT Q_DECL_IMPORT
#endif

#endif // ORDERS_GLOBAL_H
