#ifndef ELEMENTSDELEGATE_H
#define ELEMENTSDELEGATE_H

#include <QStyledItemDelegate>


namespace eSawmill::orders::delegates {

/**
 * @brief The ElementsDelegate class
 * zapewnia edycję danych elementów bez konieczności wyświetlania
 * dialogu do modyfikacji, edycja dokonywana wewnątrz kontrolki widgets::TableView
 */
class ElementsDelegate : public QStyledItemDelegate {
	Q_OBJECT

public:
	ElementsDelegate(QObject* parent = nullptr);
    virtual ~ElementsDelegate() {}

	QWidget * createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
};

} // namespace eSawmill

#endif // ELEMENTSDELEGATE_H
