#ifndef ORDERSMANAGER_H
#define ORDERSMANAGER_H

#include "orders_global.h"

#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <QSortFilterProxyModel>
#include <QListView>
#include <QGroupBox>
#include <QCheckBox>
#include <QAction>
#include <tableview.h>	// moduł widgets
#include <pushbutton.h>	// moduł widget
#include <eventagentclient.hpp>

#include "models/orders.h"
#include "filters/orderrolefilter.h"
#include "filters/ordersfilter.hpp"
#include "filters/orderdaterangefilter.hpp"
#include "widgets/datefilterwidget.hpp"
#include "widgets/recordfilterwidget.hpp"
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill::orders {

///
/// \brief The OrdersManager class
/// widget to manage orders data
///
class ORDERS_EXPORT OrdersManager : public QDialog {
	Q_OBJECT

public:
	OrdersManager(QWidget* parent = nullptr);
	virtual ~OrdersManager() = default;

public Q_SLOTS:
	void handleRepositoryNotification(const agent::RepositoryEvent& event);

private:
	//// Funkcje
	void createWidgets();
	void createConnections();
	void createModels();
	void createContextMenu();


	//// actions used in context menu
	QAction* mAddAction;
	QAction* mEditAction;
	QAction* mRemoveAction;
	QAction* mPrintAction;
	QAction* mImportAction;
	QAction* mExportAction;
	QAction* mGenerateSaleInvoiceAction;
	QAction* mShowSaleInvoiceAction;

	//// Kontrolki
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;
	::widgets::PushButton* mRemakeButton;
	::widgets::PushButton* mCloseButton;
	::widgets::PushButton* mPrintButton;
	::widgets::PushButton* mOperationButton;
	QListView* mFilterOption;

	// filters widget
	eSawmill::orders::widgets::DateFilterWidget* mDateFilterWidget;
	eSawmill::orders::widgets::RecordFilterWidget* mRecordFilterWidget;

	//// Modele
	models::Orders* mOrders;
	orders::filters::OrderRoleFilter* mOrderFilter;
	orders::filters::OrdersFilter* mFilter;
	eSawmill::orders::filters::OrderDateRangeFilter* mDateRangeFilter;

private Q_SLOTS:
	/**
	 * @brief addOrder_Clicked
	 * slot odpowiadający na kliknięcie przycisku Dodaj
	 */
	void addOrderHandle();

	/**
	 * @brief remakeOrder_Clicked
	 * slot wywołujący edycję wybranego zamówienia
	 */
	void editOrderHandle();

	/**
	 * @brief removeOrder_Clicked
	 * slot wywołujący funkcje usuwania zamówienia
	 */
	void removeOrderHandle();

	/**
	 * @brief importOrderHandle - imporotwanie zamówienia wygenerowanego w innym programie
	 * lub przez formularz dostępny na stronach internetowych
	 */
	void importOrderHandle();

	///
	/// \brief printOrderHandle
	/// print current selected order in list
	///
	void printOrderHandle();

	///
	/// \brief importOrderByDroppingFile
	/// import orders by dropping file on widget
	/// \param files
	///
	void importOrderByDroppingFile(const QStringList& files);

	///
	/// \brief exportOrderHandle
	///
	void exportOrderHandle();

	///
	/// \brief generateSaleInvoiceHandle
	/// generate sale invoice from realised order
	///
	void generateSaleInvoiceHandle();
	void showSaleInvoiceHandle();

	///
	/// \brief filterRecordHandle
	/// filter rows in models by data gived in form
	/// \param options`
	///
	void filterRecordHandle(struct eSawmill::orders::widgets::RecordFilterWidget::FilterOptions &options);

	///
	/// \brief filterByDateHandle
	/// filter rows in model by date range
	/// user can choose date to filter (added date or end date)
	///
	void filterByDateHandle(const struct eSawmill::orders::widgets::DateFilterWidget::DateFilterOptions &options);

	///
	/// \brief fitContextMenu fit options in context menu to current order
	///
	void fitContextMenu();
};

} // namespace eSawmill

#endif // ORDERSMANAGER_H
