#ifndef CREATEORDER_ORDERS_H
#define CREATEORDER_ORDERS_H

#include "orders_global.h"

#include <QDialog>
#include <QTabWidget>
#include <QLineEdit>
#include <QDateEdit>
#include <QComboBox>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QPlainTextEdit>
#include <QCheckBox>
#include <QLabel>

#include <pushbutton.h>
#include <tableview.h>
#include <widgets/contractorselector.h>
#include <dbo/order.h>

#include <widgets/articlewidget.h>
#include "widgets/elementswidget.h"


namespace eSawmill::orders {

///
/// \brief The CreateOrder class
/// dialog to create new order or edit existing
/// if yout want to run this dialog as editor
/// you have to use ctor with id param
/// ctor with QDomDocument param is uses to import order from file
///
class ORDERS_EXPORT CreateOrder : public QDialog {
	Q_OBJECT

public:
	///{@
	explicit CreateOrder(core::Order& order, QWidget* parent = nullptr);
	///@}

	/// @brief load data from order object (gave in ctor) into fields
	void load();
	/// @brief exchange data between fields and order object
	void exchange();

	/// @brief set fields in read onyl state
	void setReadOnly(bool readOnly);

	/// \brief accept
	/// this method will be running when user
	/// accept the dialog by click OK or SAVE button
	void accept() override;

private:
	///@{
	/// methods to create widgets on dialog
	/// and connect signals to slots
	void createWidgets();
	void createModels();
	void createConnections();
	void createValidators();
	///@}

	///@{
	/// additional widgets (tab pages on dialog)
	QWidget* generalWidget();
	///@}

	///@{
	QTabWidget* mTabWidget;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
	::widgets::PushButton* mPrintButton;

	QLineEdit* mName;
	eSawmill::contractors::widgets::ContractorSelector* mContractorSelector;
	QDateEdit* mAddDate;
	QDateEdit* mEndDate;
	QComboBox* mPrioritySelector;
	QGroupBox* mPriceGroup;
	QDoubleSpinBox* mPrice;
	QDoubleSpinBox* mRebate;
	QComboBox* mTax;
	QPlainTextEdit* mDescription;
	QCheckBox* mRealised;

	orders::widgets::ElementsWidget* mElementWidget;
	eSawmill::articles::widgets::ArticleWidget* mArticleWidget;

	QLabel* mTotalValueLabel;			///< total order costs (elements with articles)
	///@}


	core::Order& _order;	///< current editing order object

protected:
	///
	/// \brief mouseDoubleClickEvent
	/// show debug information (id from database)
	/// only on test time
	/// \param event
	///
	void mouseDoubleClickEvent(QMouseEvent* event) override;

private Q_SLOTS:
	///
	/// \brief recalculate
	/// calculate total costs
	/// it is sum of all elements price and all articles (assigned to order) price
	/// this method take into consideration acctual discount and tax
	///
	void recalculate();

	///
	/// \brief printButton_clicked
	/// action running when user click print button
	///
	void printButton_clicked(bool preview);

	///
	/// \brief checkOrderDates
	/// check add date and end date
	/// if end date is before add date inform user
	/// @return true when end date is before add date
	///
	bool checkOrderDates();
};

} // namespace eSawmill

#endif // CREATEORDER_ORDERS_H
