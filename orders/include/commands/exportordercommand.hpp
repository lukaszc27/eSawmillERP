#ifndef EXPORTORDERCOMMAND_HPP
#define EXPORTORDERCOMMAND_HPP

#include "../orders_global.h"
#include <abstracts/abstractcommand.hpp>
#include <vector>

namespace eSawmill::orders::commands {

/// @brief ExportOrderCommand class
/// command to export existing order into file (IODevice object)
class ORDERS_EXPORT ExportOrderCommand : public core::abstracts::AbstractCommand {
public:
	explicit ExportOrderCommand(
		const QString& filename,
		const std::vector<core::eloquent::DBIDKey>& ordersIds,
		QWidget* parent = nullptr);

	bool execute(core::repository::AbstractRepository& repository) override;

private:
	std::vector<core::eloquent::DBIDKey> _ordersIds;
	QString _fileName;
};

} // namespace eSawmill::orders::commands

#endif // EXPORTORDERCOMMAND_HPP
