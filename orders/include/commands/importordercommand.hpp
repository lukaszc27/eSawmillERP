#ifndef IMPORTORDERCOMMAND_HPP
#define IMPORTORDERCOMMAND_HPP

#include "../orders_global.h"
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::orders::commands {

/// @brief ImportOrderCommand class
/// import order from file.
/// The file must have XML or CSV extension
class ORDERS_EXPORT ImportOrderCommand : public core::abstracts::AbstractCommand {
public:
	explicit ImportOrderCommand(const QString& filename, QWidget* parent = nullptr);

	/// @brief execute function
	/// make current command
	bool execute(core::repository::AbstractRepository& repository) override;

	bool canUndo() const override { return true; }
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	QString _fileName;					///< file from which data will be imported
	QList<core::Order> _importedOrders;	///< list of imported orders
};

} // namespace eSawmill::orders::commands

#endif // IMPORTORDERCOMMAND_HPP
