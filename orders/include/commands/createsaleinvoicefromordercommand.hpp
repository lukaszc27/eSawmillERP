#ifndef CREATESALEINVOICEFROMORDERCOMMAND_H
#define CREATESALEINVOICEFROMORDERCOMMAND_H

#include "../orders_global.h"
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::orders::commands {

class CreateSaleInvoiceFromOrderCommand : public core::abstracts::AbstractCommand {
public:
	explicit CreateSaleInvoiceFromOrderCommand(const core::Order& order, QWidget* parent = nullptr);

	/// @brief execute
	/// run this command
	bool execute(core::repository::AbstractRepository& repository) override;

	void setWarehouseId(const core::eloquent::DBIDKey& warehouseId)
		{ _warehouseId = warehouseId; }
	void setPaymentName(const QString& name)
		{ _paymentName = name; }
	void setPaymentDeadlineDate(const QDate& date)
		{ _paymentDeadlineDate = date; }
	void setPaymentDeadlineDays(int days)
		{ _paymentDeadlineDays = days; }

private:
	core::eloquent::DBIDKey _warehouseId;
	core::Order				_order;
	QString					_paymentName;
	QDate					_paymentDeadlineDate;
	int						_paymentDeadlineDays;
};

} // namespace eSawmill::orders::commands

#endif // CREATESALEINVOICEFROMORDERCOMMAND_H
