#ifndef REMOVEORDERCOMMAND_HPP
#define REMOVEORDERCOMMAND_HPP

#include "../orders_global.h"
#include "abstracts/abstractcommand.hpp"
#include "dbo/order.h"

namespace eSawmill::orders::commands {

/// @brief RemoveOrderCommand
/// command to remove existing order from system
/// this object schould be sent by GUI layer into buisness layer
class RemoveOrderCommand final : public core::abstracts::AbstractCommand {
public:
	explicit RemoveOrderCommand(const core::eloquent::DBIDKey& orderId, QWidget* parent = nullptr);

	/// @brief execute function
	/// @param repository - reference to current repository in use
	/// @return true - if all operation will be done successfully
	bool execute(core::repository::AbstractRepository& repository) override;

	/// @brief canUndo command
	/// @return true - the command can be rollbacked
	bool canUndo() const override {
		return true;
	}

	/// @brief rollback command
	/// @param repository 
	/// @return 
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::eloquent::DBIDKey _orderId;
	core::Order _removedOrder;
};

} // namespace eSawmill::orders::commands

#endif // REMOVEORDERCOMMAND_HPP
