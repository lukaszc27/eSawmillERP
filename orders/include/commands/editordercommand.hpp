#ifndef EDITORDERCOMMAND_HPP
#define EDITORDERCOMMAND_HPP

#include "abstracts/abstractcommand.hpp"
#include "../orders_global.h"
#include "dbo/order.h"

namespace eSawmill::orders::commands {

/// @brief EditOrderCommand
/// command object sent by GUI layer to buisness layer
/// when user want to edit existing order
class ORDERS_EXPORT EditOrderCommand final : public core::abstracts::AbstractCommand {
public:
	explicit EditOrderCommand(const core::Order& order, QWidget* parent = nullptr);

	/// @brief execute
	/// @param repository - reference to current repository in use 
	/// @return true - when all operation will be done successfully
	bool execute(core::repository::AbstractRepository& repository) override;

	/// @brief canUndo
	/// @return returned information that command can be undo by the user
	bool canUndo() const override {
		return true;
	}

	/// @brief undo function
	/// undo all operations executed in current command
	/// @return true on successfull
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Order _editedOrder;	///< current order edited by user
	core::Order _previousOrder;	///< order existing in repository (before editing time)
};

} // namespace eSawmill::orders::commands

#endif // EDITORDERCOMMAND_HPP
