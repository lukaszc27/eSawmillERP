#ifndef CREATEORDERCOMMAND_HPP
#define CREATEORDERCOMMAND_HPP

#include "abstracts/abstractcommand.hpp"
#include "dbo/order.h"
#include "../orders_global.h"

namespace eSawmill::orders::commands {

///
/// @brief The CreateOrderCommand class
/// command to create new order in system
///
class ORDERS_EXPORT CreateOrderCommand final : public core::abstracts::AbstractCommand {
public:
	explicit CreateOrderCommand(const core::Order& order, QWidget* parent = nullptr);

	///
	/// @brief execute - execute current command
	/// @param repository - current repository in use
	/// @return
	///
	bool execute(core::repository::AbstractRepository& repository) override;

	/// @brief canUndo function
	/// the command can be undo by the user
	bool canUndo() const override {
		return true;
	}

	/// @brief undo operation
	/// executed when user type Ctrl+C shortcut
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Order _order;
};

} // namespace eSawmill::orders::commands

#endif // CREATEORDERCOMMAND_HPP
