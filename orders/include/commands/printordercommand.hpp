#ifndef PRINTORDERCOMMAND_HPP
#define PRINTORDERCOMMAND_HPP

#include "../orders_global.h"
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::orders::commands {

/// @brief The PrintOrderCommand class
/// command to print existing order
/// - the print dialog will be shown before printing
class ORDERS_EXPORT PrintOrderCommand final : public core::abstracts::AbstractCommand {
public:
	explicit PrintOrderCommand(const core::eloquent::DBIDKey& orderId, QWidget* parent = nullptr);
	explicit PrintOrderCommand(const core::Order& order, QWidget* parent = nullptr);

	void setPreviewMode(bool preview) noexcept
	{
		_preview = preview;
	}

	/// @brief execute all operation for this command
	/// @return true - when document is ready to print (and was send to printer)
	bool execute(core::repository::AbstractRepository& repository) override;

	bool overrideCursor() const override { return false; }

private:
	core::Order _order;
	bool _preview{};
};

} // namespace eSawmill::orders::commands

#endif // PRINTORDERCOMMAND_HPP
