#ifndef RECORDFILTERWIDGET_HPP
#define RECORDFILTERWIDGET_HPP

#include "../orders_global.h"
#include <QGroupBox>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>


namespace eSawmill::orders::widgets {

class ORDERS_EXPORT RecordFilterWidget : public QGroupBox {
	Q_OBJECT

public:
	RecordFilterWidget(QWidget* parent = nullptr);
	virtual ~RecordFilterWidget() {}

	QLineEdit* nameField() const { return mName; }
	QLineEdit* numberField() const { return mNumber; }
	QLineEdit* contractorNameField() const { return mContracotrName; }
	QLineEdit* contractorPhoneField() const { return mContracotrPhone; }
	QComboBox* priority() const { return mPriority; }
	QCheckBox* priorityEnable() const { return mUsePriority; }

	struct FilterOptions {
		QString name;
		QString number;
		QString priority;
		bool usePriority;
		QString contractorName;
		QString contractorPhone;
	};

private:
	void createAllWidgets();
	void createConnections();

	QLineEdit* mName;
	QLineEdit* mNumber;
	QComboBox* mPriority;
	QCheckBox* mUsePriority;
	QLineEdit* mContracotrName;
	QLineEdit* mContracotrPhone;
	QPushButton* mCleanButton;
	QPushButton* mFilterButton;

private Q_SLOTS:
	void filterButtonHandle();
	void cleanButtonHandle();

signals:
	void filterDataChanged(struct FilterOptions&);
};

} // namespace

#endif // RECORDFILTERWIDGET_HPP
