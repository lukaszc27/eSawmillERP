#ifndef ELEMENTSWIDGET_H
#define ELEMENTSWIDGET_H

#include "../orders_global.h"
#include "../models/elements.h"
#include "../filters/elementsfilter.h"

#include <QWidget>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QCheckBox>
#include <dbo/order.h>
#include <pushbutton.h>
#include <tableview.h>


namespace eSawmill::orders::widgets {

///
/// \brief The ElementsWidget class
/// widget to display elemetns assigned to order
///
class ORDERS_EXPORT ElementsWidget : public QWidget {
	Q_OBJECT

public:
	explicit ElementsWidget(core::models::AbstractElementsModel<core::order::Element>* model, QWidget* parent = nullptr);
	virtual ~ElementsWidget() = default;

	///
	/// \brief load
	/// load nessesary data from valid order object
	/// \param order
	///
	void load(core::Order& order);

	///
	/// \brief items
	/// \return
	///
	QList<core::order::Element> items() const;

	double totalCubature() const { return mElements->totalCubature(); }
	double totalPlannedCubature() const { return mElements->totalPlannedCubature(); }

	///
	/// \brief setReadOnly
	/// \param readonly
	///
	void setReadOnly(bool readonly);

	///
	/// \brief setPrice
	/// \param price
	///
	inline void setPrice(double price) const	{ mElements->setPrice(price); }
	inline void setTax(double Tax) const		{ mElements->setTax(Tax); }
	inline void setRebate(double rebate) const	{ mElements->setRebate(rebate); }

	///
	/// \brief totalPrice
	/// \return
	///
	inline double totalPrice() const { return mElements->totalPrice(); }

	///
	/// \brief importFromXML
	/// \param doc
	///
	void importFromXML(QDomDocument* doc) { mElements->importFromXML(doc); }

	// getters for spare length
	double spareLength() const { return mElements->spareLength(); }

public Q_SLOTS:
	void setSpareLength(double length);

private:
	void createWidgets();
	void createModels();
	void createConnections();
	void createActions();

	///@{
	/// additional controls display in widget
	::widgets::TableView* mElementsView;
	::widgets::PushButton* mElementAddButton;
	::widgets::PushButton* mElementRemoveButton;
	::widgets::PushButton* mOperationsButton;

	QGroupBox* mElementsFilterGroup;		// grupa filtrowania poprzez przekruj (szer x wys)
	QGroupBox* mElementsLengthFilterGroup;	// filtowanie elementów z długości
	QPushButton* mFilterButton;
	QDoubleSpinBox* mFilterWidth;
	QDoubleSpinBox* mFilterHeight;
	QDoubleSpinBox* mFilterMinLength;
	QDoubleSpinBox* mFilterMaxLength;
	QDoubleSpinBox* mAdditionalLength;
	QCheckBox* mUseAdditionalLength;

	QCheckBox* mHideWoodTypeColumnCheck;
	QCheckBox* mHidePlannedColumnCheck;
	QCheckBox* mHideNameColumnCheck;

	QLabel* mTotalValueElementsLabel;
	QLabel* mTotalPlannedElementsLabel;
	///@}

	///@{
	/// models uses in widget
	core::models::AbstractElementsModel<core::order::Element>* mElements;
	orders::filters::ElementsFilter* mElementFilter;
	///@}

private Q_SLOTS:
	void addElement_clicked();
	void addMultipleElementsHandle();
	void removeElement_clicked();
	void filterButton_clicked();

	void exportToFile();	///< export elements to document (user can choose type of document - CSV or XML)
	void importFromFile();	///< import elements from document (user can chhose type of document - CSV or XML)

	///
	/// \brief markCheckedAsPlannedHandle
	/// set all selected items in view
	/// to planned
	///
	void markCheckedAsPlannedHandle();

	///
	/// \brief markCheckedAsUnplannedHandle
	/// mark all selected items in view to unplanned
	///
	void markCheckedAsUnplannedHandle();

	///
	/// \brief importDataByDroppingFile
	/// importing data from file by dropping on widget
	/// \param mimedata informations about file dropped on widget
	///
	void importDataByDroppingFile(const QStringList& files);

	///
	/// \brief recalculateAndUpdatePrices
	/// update prices visible on widget
	///
	void recalculateAndUpdatePrices();

Q_SIGNALS:
	///
	/// \brief dataChanged
	///
	void dataChanged();
	void spareLengthChanged(double);
};

} // namespace eSawmill

#endif // ELEMENTSWIDGET_H
