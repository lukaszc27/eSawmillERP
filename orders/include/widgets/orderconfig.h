#ifndef ORDERS_H
#define ORDERS_H

#include <QWidget>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>

#include <configurationwidget.h>
#include <colorselector.hpp>
#include "orders_global.h"


namespace eSawmill::orders::widgets::config {

/**
 * @brief The Orders class
 * konfiguracja modułu zamówień
 */
class ORDERS_EXPORT Orders : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	Orders(QWidget* parent = nullptr);

	void save() override;
	void initialize() override;

private:
	void createWidgets();

	//// kontrolki
	QSpinBox* mDaysToRemind;					// na ile dni przypomina o końcówkach terminów
	::widgets::ColorSelector* mRemindColor;		// kolor przypominania o nadchodzącym terminie
	::widgets::ColorSelector* mDeadlineColor;	// kolor informujący że termin już został przekroczony
	QCheckBox* mColorPrority;					// kolorowanie zamówień na liście pod względem przydzielonego priotytetu
	QCheckBox* mRemindCheck;
	QCheckBox* mDeadlineCheck;
	QLineEdit* mPrefix;
};

} // namespace eSawmill

#endif // ORDERS_H
