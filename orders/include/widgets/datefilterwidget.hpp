#ifndef DATEFILTERWIDGET_HPP
#define DATEFILTERWIDGET_HPP

#include "../orders_global.h"
#include <QGroupBox>
#include <QDateEdit>
#include <QRadioButton>
#include <QPushButton>


namespace eSawmill::orders::widgets {

class ORDERS_EXPORT DateFilterWidget : public QGroupBox {
	Q_OBJECT

public:
	DateFilterWidget(QWidget* parent = nullptr);
	virtual ~DateFilterWidget() {}

	struct DateFilterOptions {
		int filterByColumn;
		QDate minDate;
		QDate maxDate;
	};

	QRadioButton* byAddDateRadioButton() const { return mFilterByAddedDate; }
	QRadioButton* byEndDateRadioButton() const { return mFilterByEndDate; }

private:
	void createAllWidgets();

	QDateEdit* mMinDate;
	QDateEdit* mMaxDate;
	QRadioButton* mFilterByAddedDate;
	QRadioButton* mFilterByEndDate;
	QPushButton* mFilterButton;

private Q_SLOTS:
	void filterButtonHangle();

signals:
	void dateRangeChanged(struct DateFilterOptions&);
};

} // namespace

#endif // DATEFILTERWIDGET_HPP
