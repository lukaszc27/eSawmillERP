#include "createmultipleelements.hpp"
#include "delegates/elementsdelegate.h"
#include <dbo/orderElements.h>
#include <models/woodtypelistmodel.hpp>
#include <messagebox.h>
#include <models/orders/elementnamemodel.hpp>	///< moved to widgets module
#include <globaldata.hpp>

namespace eSawmill::orders {

CreateMultipleElements::CreateMultipleElements(QWidget* parent)
	: QDialog(parent)
{
	createAllWidgets();
	createModels();
	createConnections();

	setWindowTitle(tr("Grupowe dodawanie elementów"));

	// hide unused columns in elemnts view
	mTableView->hideColumn(orders::models::Elements::Columns::Name);
	mTableView->hideColumn(orders::models::Elements::Columns::WoodType);
	mTableView->hideColumn(orders::models::Elements::Columns::Cubature);
	mTableView->hideColumn(orders::models::Elements::Columns::IsPlanned);
}

QList<core::order::Element> CreateMultipleElements::items() {
	return mElements->items();
}

void CreateMultipleElements::createAllWidgets() {
	mName = new QComboBox(this);
	mWoodType = new QComboBox(this);
	mWidth = new QDoubleSpinBox(this);
	mHeight = new QDoubleSpinBox(this);
	mLength = new QDoubleSpinBox(this);
	mCount = new QDoubleSpinBox(this);
	mPlanned = new QCheckBox(tr("Strugane"), this);
	mShowPlannedColumn = new QCheckBox(tr("Pokaż kolumnę \"Strugane\""), this);
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("Return"), this, QIcon(":/icons/add"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mAcceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/accept"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/reject"));

	mName->setEditable(true);
	mName->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	mWoodType->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	QHBoxLayout* propertisLayout = new QHBoxLayout;
	propertisLayout->addWidget(new QLabel(tr("Nazwa"), this));
	propertisLayout->addWidget(mName);
	propertisLayout->addSpacing(12);
	propertisLayout->addWidget(new QLabel(tr("Rodzaj"), this));
	propertisLayout->addWidget(mWoodType);
	propertisLayout->addSpacing(48);
	propertisLayout->addWidget(mPlanned);

	QHBoxLayout* topLayout = new QHBoxLayout;
	topLayout->addWidget(new QLabel(tr("Szerokość")));
	topLayout->addWidget(mWidth);
	topLayout->addWidget(new QLabel("cm", this));
	topLayout->addSpacing(24);
	topLayout->addWidget(new QLabel(tr("Wysokość")));
	topLayout->addWidget(mHeight);
	topLayout->addWidget(new QLabel("cm", this));
	topLayout->addStretch(1);

	QHBoxLayout* bottomLayout = new QHBoxLayout;
	bottomLayout->addWidget(new QLabel(tr("Długość")));
	bottomLayout->addWidget(mLength);
	bottomLayout->addWidget(new QLabel("m", this));
	bottomLayout->addSpacing(24);
	bottomLayout->addWidget(new QLabel("Ilość"));
	bottomLayout->addWidget(mCount);
	bottomLayout->addWidget(new QLabel("szt", this));
	bottomLayout->addStretch(1);
	bottomLayout->addWidget(mAddButton);
	bottomLayout->addWidget(mRemoveButton);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mShowPlannedColumn);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(propertisLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(topLayout);
	mainLayout->addWidget(mTableView);
	mainLayout->addLayout(bottomLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void CreateMultipleElements::createConnections() {
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateMultipleElements::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateMultipleElements::reject);
	connect(mAddButton, &::widgets::PushButton::clicked, this, &CreateMultipleElements::returnHandle);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &CreateMultipleElements::removeButtonHandle);

	connect(mShowPlannedColumn, &QCheckBox::stateChanged, [this](int state){
		if (state)
			mTableView->showColumn(orders::models::Elements::Columns::IsPlanned);
		else mTableView->hideColumn(orders::models::Elements::Columns::IsPlanned);
	});
}

void CreateMultipleElements::createModels()
{
	core::Order order;
	mElements = new orders::models::Elements(order, this);
	mTableView->setModel(mElements);
	mTableView->setItemDelegate(new eSawmill::orders::delegates::ElementsDelegate(this));
	mName->setModel(new ::widgets::models::orders::ElementName(this));
	mWoodType->setModel(new core::models::WoodTypeListModel(this));
}

void CreateMultipleElements::returnHandle() {
	const auto& wood = core::GlobalData::instance()
		.repository()
		.woodTypeRepository()
		.get(mWoodType->currentData(
			core::models::WoodTypeListModel::Id).toInt()
		);

	core::order::Element element {};
	element.setWidth(mWidth->value());
	element.setHeight(mHeight->value());
	element.setLength(mLength->value());
	element.setQuantity(mCount->value());
	element.setPlanned(mPlanned->isChecked());
	element.setName(mName->currentText());
	element.setWoodType(wood);

	mElements->insert(element);
	mLength->setFocus();
	mLength->selectAll();
}

void CreateMultipleElements::removeButtonHandle() {
	QModelIndexList selectedRows = mTableView->selectionModel()->selectedRows(0);
	if (selectedRows.size() <= 0) {
		::widgets::MessageBox::information(this, tr("Informacja"),
										   tr("Przed usunięciem elementów z listy musisz zaznaczyć te elementy które mają zostać usunięte"));
		return;
	}

	int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
												 tr("Znaleziono %1 zaznaczonych elementów. Czy na pewno chcesz je usunąć?").arg(selectedRows.size()), "",
												 QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::Yes) {
		// first virtual select items by setData method
		for (auto row : selectedRows) {
			mElements->setData(row, true, Qt::CheckStateRole);
		}

		// and remove all checked items in model
		mElements->deleteSelectedItems();
	}
}

} // namespace eSawmill
