#include "filters/elementsfilter.h"
#include "models/elements.h"

#include <QMessageBox>


namespace eSawmill::orders::filters {

ElementsFilter::ElementsFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
{
	mFetchWithLength = false;
	mFetchWidthSection = false;
}

bool ElementsFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	bool sectionFilter = true;
	if (mFetchWidthSection) {
		double width = sourceModel()->index(source_row, orders::models::Elements::Width, source_parent)
				.data(Qt::DisplayRole).toDouble();

		double height = sourceModel()->index(source_row, orders::models::Elements::Height, source_parent)
				.data(Qt::DisplayRole).toDouble();

		sectionFilter = (width == mWidth) && (height == mHeight);
	}

	bool lengthFilter = true;
	if (mFetchWithLength) {
		double length = sourceModel()->index(source_row, orders::models::Elements::Length, source_parent)
				.data(Qt::DisplayRole).toDouble();

		lengthFilter = length >= mMinLength && length <= mMaxLength;
	}
	else lengthFilter = true;

	return sectionFilter & lengthFilter;
}

bool ElementsFilter::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const {
	const QVariant left = sourceModel()->data(sourceLeft);
	const QVariant right = sourceModel()->data(sourceRight);

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	//
	// deprecated version for Qt5 (support win7 and win8)
	//
	if (left.type() == QVariant::Type::Double &&
		right.type() == QVariant::Type::Double)
	{
		return left.toDouble() < right.toDouble();
	}

	if (left.type() == QVariant::Type::Int && right.type() == QVariant::Type::Int)
		return left.toInt() < right.toInt();
#else
	//
	// Qt6 version
	//
	if (left.typeId() == QMetaType::Type::Double && right.typeId() == QMetaType::Type::Double)
		return left.toDouble() < right.toDouble();

	if (left.typeId() == QMetaType::Type::Int && right.typeId() == QMetaType::Type::Int)
		return left.toInt() < right.toInt();
#endif // QT_VERSION < QT_VERSION_CHECK(6, 0, 0)

	return left.toString().compare(right.toString(), Qt::CaseSensitivity::CaseInsensitive);
}

void ElementsFilter::enableFetchWithLength(bool enable) {
	mFetchWithLength = enable;
	invalidateFilter();
}

void ElementsFilter::setFilterLength(double min, double max) {
	mMinLength = min;
	mMaxLength = max;
	invalidateFilter();
}

void ElementsFilter::setFilterSection(double width, double height)
{
	mWidth = width;
	mHeight = height;
	invalidateFilter();
}

QVariant ElementsFilter::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Vertical && role == Qt::DisplayRole)
		return QVariant(QString::number(section + 1));

	return QSortFilterProxyModel::headerData(section, orientation, role);
}

void ElementsFilter::enableFetchWithSection(bool enable) {
	mFetchWidthSection = enable;
	invalidateFilter();
}

} // namespace eSawmill
