#include "filters/orderrolefilter.h"
#include "models/orders.h"
#include <globaldata.hpp>

namespace eSawmill::orders::filters {

OrderRoleFilter::OrderRoleFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mIndex(0)
{
}

bool OrderRoleFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	auto& repository = core::GlobalData::instance().repository().orderRepository();
	switch (mIndex) {
	case All:{
		return true;
	}

	case ToImplemented: {
		const quint32 orderId = sourceModel()->index(source_row, 0, source_parent).data(models::Orders::Id).toUInt();
//		return !core::Order::find(orderId).isRealised();
//		return !core::eloquent::Model::find<core::Order>(orderId).get("IsRealised").toBool();
		return !repository.getOrder(orderId).isRealised();
	}

	case AfterDeadline: {
		const quint32 orderId = sourceModel()->index(source_row, 0, source_parent).data(models::Orders::Id).toUInt();
//		core::Order order = core::Order::find(orderId);
//		core::Order order = core::eloquent::Model::find<core::Order>(orderId);
		core::Order order = repository.getOrder(orderId);
		return !order.isRealised() && order.get("EndDate").toDate() < QDate::currentDate();
	}

	case Realised:
		const quint32 orderId = sourceModel()->index(source_row, 0, source_parent).data(models::Orders::Id).toUInt();
//		return core::Order::find(orderId).isRealised();
//		return core::eloquent::Model::find<core::Order>(orderId).get("IsRealised").toBool();
		return repository.getOrder(orderId).isRealised();

	} // switch

	return false;
}

} // namespace eSawmill
