#include "filters/orderdaterangefilter.hpp"
#include "models/orders.h"
#include <QDebug>


namespace eSawmill::orders::filters {

OrderDateRangeFilter::OrderDateRangeFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
{
}

bool OrderDateRangeFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	const QString dateAsString = sourceModel()->index(source_row, filterKeyColumn(), source_parent)
			.data().toString();
	const QDate date = QDate::fromString(dateAsString, "dd.MM.yyyy");

	if (!mActive)
		return true;

	return date >= mMinDate && date <= mMaxDate;
}

}
