#include "filters/ordersfilter.hpp"
#include "models/orders.h"
#include <QDebug>


namespace eSawmill::orders::filters {

OrdersFilter::OrdersFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mIsActive(false)
{
}

void OrdersFilter::setActive(bool active) {
	mIsActive = active;
	invalidateFilter();
}

void OrdersFilter::setPriorityEnable(bool enable) {
	mPriorityEnable = enable;
	invalidateFilter();
}

void OrdersFilter::setName(const QString &name) {
	mName = name;
	invalidateFilter();
}

void OrdersFilter::setNumber(const QString &number) {
	mNumber = number;
	invalidateFilter();
}

void OrdersFilter::setPriority(const QString &priority) {
	mPriority = priority;
	invalidateFilter();
}

void OrdersFilter::setContractor(const QString &contractor) {
	mContractor = contractor;
}

void OrdersFilter::setPhone(const QString &phone) {
	mPhone = phone;
	invalidateFilter();
}

void OrdersFilter::clearAllFilter()
{
	mName = "";
	mPriorityEnable = false;
	mNumber = "";
	mContractor = "";
	mPhone = "48";
	mPriority = "";

	invalidateFilter();
}


bool OrdersFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	Q_UNUSED(source_parent);

	if (mIsActive) {
		return filterByName(sourceModel()->index(source_row, models::Orders::Columns::Name).data().toString()) &&
				filterByNumber(sourceModel()->index(source_row, models::Orders::Columns::Number).data().toString()) &&
				filterByContractor(sourceModel()->index(source_row, models::Orders::Columns::ContractorName).data().toString()) &&
				filterByPhone(sourceModel()->index(source_row, models::Orders::Columns::ContractorPhone).data().toString()) &&
				filterByPriority(sourceModel()->index(source_row, models::Orders::Columns::Priority).data().toString());
	}

	return true;
}

bool OrdersFilter::filterByName(const QString &value) const {
	return value.toUpper().contains(mName.toUpper(), Qt::CaseSensitivity::CaseInsensitive);
}

bool OrdersFilter::filterByNumber(const QString &value) const {
	return value.toUpper().contains(mNumber.toUpper(), Qt::CaseSensitivity::CaseInsensitive);
}

bool OrdersFilter::filterByPriority(const QString &value) const {
	if (mPriorityEnable)
		return value.toUpper().contains(mPriority.toUpper());

	return true;
}

bool OrdersFilter::filterByContractor(const QString &value) const {
	return value.toUpper().contains(mContractor.toUpper(), Qt::CaseSensitivity::CaseInsensitive);
}

bool OrdersFilter::filterByPhone(const QString &value) const {
	return value.contains(mPhone);
}

} // namespace
