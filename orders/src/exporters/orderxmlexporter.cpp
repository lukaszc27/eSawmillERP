#include "exporters/orderxmlexporter.hpp"
#include <exporters/contractorexporter.hpp>
#include <exporters/elementxmlexporter.hpp>
#include <memory>
#include <QSettings>
#include <eloquent/builder.hpp>
#include <dbo/auth.h>
#include <dbo/user.h>
#include <contractormanager.h>
#include <messagebox.h>
#include <globaldata.hpp>
#include <settings/appsettings.hpp>

namespace eSawmill::orders::exporters {

OrderXmlExporter::OrderXmlExporter()
	: core::abstracts::AbstractExporter<core::Order>{}
{
}

bool OrderXmlExporter::exportData(QIODevice &device) {
	QDomDocument document;
	QDomElement root = document.createElement("eSawmill");
	root.setAttribute("version", core::GlobalData::instance().currentApplicationVersion().toString());
	root.appendChild(toDomNode(document));
	document.appendChild(root);
	return device.write(document.toByteArray()) > 0;
}

bool OrderXmlExporter::importData(QIODevice &device) {
	mData.clear();

	QDomDocument document;
	QString errorMsg;
	if (!document.setContent(&device, &errorMsg)) {
		::widgets::MessageBox::information(nullptr, QString("Import zamówień"),
			QString("Podczas importu zamówień wystąpiły błędy!"), errorMsg);
		return false;
	}
	QDomElement rootElement = document.documentElement();
	QDomElement ordersElement = rootElement.firstChildElement(ordersNodeTag);
	QDomNodeList orders = ordersElement.elementsByTagName(rootTag);

	if (!orders.isEmpty()) {
		for (int i {0}; i < orders.size(); i++) {
			fromDomNode(orders.at(i).toElement());
		}
		return true;
	}
	return false;
}

QDomElement OrderXmlExporter::toDomNode(QDomDocument &document) {
	QDomElement ordersNode = document.createElement(ordersNodeTag);
	for (auto& map : mData) {
		ordersNode.appendChild(exportOrder(document, map));
	}
	return ordersNode;
}

void OrderXmlExporter::fromDomNode(const QDomElement &element) {
	// element is order node from XML stream
	QSettings settings;

	core::Order order;
	order.setName(element.firstChildElement(nameTag).text());
	order.setDescription(element.firstChildElement(descriptionTag).text());
	order.setEndDate(QDate::fromString(element.firstChildElement(endDateTag).text(), "dd/MM/yyyy"));
	order.setAddDate(QDate::currentDate());
	order.setPrice(core::settings::AppSettings::instance().order().defaultPrice());

	double TaxValue = element.firstChildElement(taxTag).text().toDouble();
	const core::Tax Tax = core::GlobalData::instance().repository().TaxRepository().getByValue(TaxValue);
	order.setTax(Tax);

	core::User currentUser = core::GlobalData::instance().repository().userRepository().autorizeUser();
	order.setUser(currentUser);
	order.setCompany(currentUser.company());

	int contractorId = eSawmill::contractors::ContractorManager::createFrom(element.firstChildElement("contractor"));
	if (contractorId <= 0) {
		::widgets::MessageBox::critical(nullptr, QString("Import zamówienia"),
			QString("Podczas importu zamówienia wystąpiły błędy które uniemożliwiają poprawnego dokonania importu\n"
				"Import zamówienia został zatrzymany!"));
		return;
	}
	core::Contractor contractor = core::GlobalData::instance().repository().contractorRepository().get(contractorId);
	order.setContractor(contractor);

	std::unique_ptr<eSawmill::orders::exporters::ElementXmlExporter> importer =
			std::make_unique<eSawmill::orders::exporters::ElementXmlExporter>();

	importer->fromDomNode(element.firstChildElement(itemsNodeTag));
	order.setElements(importer->data());
	mData.append(order);
}

QDomElement OrderXmlExporter::exportOrder(QDomDocument &doc, core::Order &order) {
	QDomElement root = doc.createElement(rootTag);
	QDomElement nameNode = doc.createElement(nameTag);
	QDomElement endDateNode = doc.createElement(endDateTag);
	QDomElement descriptionNode = doc.createElement(descriptionTag);
	QDomElement taxNode = doc.createElement(taxTag);

	nameNode.appendChild(doc.createTextNode(order.name()));
	endDateNode.appendChild(doc.createTextNode(order.endDate().toString("dd/MM/yyyy")));
	descriptionNode.appendChild(doc.createTextNode(order.description()));
	taxNode.appendChild(doc.createTextNode(QString::number(order.Tax().value())));

	std::unique_ptr<eSawmill::contractors::exporters::ContractorExporter> contractorExporter =
			std::make_unique<eSawmill::contractors::exporters::ContractorExporter>();

	// give data to exporter by list
	contractorExporter->setData(order.contractor());
	root.appendChild(contractorExporter->toDomNode(doc));

	// export elements to file
	std::unique_ptr<eSawmill::orders::exporters::ElementXmlExporter> elementsExporter =
			std::make_unique<eSawmill::orders::exporters::ElementXmlExporter>();

	const QList<core::order::Element>& elements = core::GlobalData::instance().repository().orderRepository().getOrderElements(order.id());
	elementsExporter->setData(elements);

	root.appendChild(nameNode);
	root.appendChild(taxNode);
	root.appendChild(endDateNode);
	root.appendChild(descriptionNode);
	root.appendChild(elementsExporter->toDomNode(doc));

	return root;
}

} // namespace eSawmill::orders::exporters
