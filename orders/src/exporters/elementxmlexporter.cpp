#include "exporters/elementxmlexporter.hpp"
#include <QDomDocument>
#include <QDomElement>
#include <QDebug>
#include <globaldata.hpp>
#include <dbo/woodtype.hpp>
#include <eloquent/builder.hpp>
#include <messagebox.h>


namespace eSawmill::orders::exporters {

ElementXmlExporter::ElementXmlExporter()
	: core::abstracts::AbstractExporter<core::order::Element>{}
{
}

bool ElementXmlExporter::exportData(QIODevice& device) {
	QDomDocument doc;
	QDomElement rootNode = doc.createElement("eSawmill");
	QDomElement ordersNode = doc.createElement("orders");
	QDomElement orderNode = doc.createElement("order");

	// add app version to exporting data
	// so that in future we can change importing way depends on app version
	rootNode.setAttribute("version", core::GlobalData::instance().currentApplicationVersion().toString());

	orderNode.appendChild(toDomNode(doc));
	ordersNode.appendChild(orderNode);
	rootNode.appendChild(ordersNode);
	doc.appendChild(rootNode);

	// write data to QIODevice object
	device.write(doc.toByteArray());

	return true;
}

bool ElementXmlExporter::importData(QIODevice& device) {
	QDomDocument doc;
	QString errorString;
	int errorLine, errorColumn;
	if (!doc.setContent(&device, &errorString, &errorLine, &errorColumn)) {
		::widgets::MessageBox::critical(nullptr, "Import danych z pliku XML",
			QString("Podczas przetważania pliku XML napotkano na błędy w kolumnie: %1 (wiersz: %2)")
			.arg(errorColumn, errorLine), errorString);
		core::GlobalData::instance().logger().warning(errorString);
		return false;
	}

	QDomElement rootElement = doc.documentElement();
	if (QDomElement ordersElement = rootElement.firstChildElement("orders"); rootElement.hasChildNodes()) {
		if (ordersElement.hasChildNodes()) {
			QDomNodeList ordersNode = ordersElement.childNodes();
			QDomElement order = ordersNode.at(0).toElement();
			if (!order.hasChildNodes())
				return false;

			fromDomNode(order);
			return true;
		}
	}

	return false;
}

QDomElement ElementXmlExporter::toDomNode(QDomDocument& document) {
	QDomElement orderNode = document.createElement("items");

	const QList<core::WoodType>& woods = core::GlobalData::instance().repository().woodTypeRepository().getAll();
	for (auto& val : mData) {
		QDomElement el = document.createElement("item");
		el.setAttribute(widthTag, val.width());
		el.setAttribute(heightTag, val.height());
		el.setAttribute(lengthTag, val.length());
		el.setAttribute(quantityTag, val.quantity());

		auto wood = std::find_if(woods.begin(), woods.end(), [&val](core::WoodType item)->bool{
			return val.woodType().id() == item.id();
		});
		if (wood != woods.end()) {
			el.setAttribute(woodTag, wood->name());
		} else el.setAttribute(woodTag, "");	// set default value

		el.setAttribute(plannedTag, val.planned());
		el.appendChild(document.createTextNode(val.name()));

		orderNode.appendChild(el);
	}
	return orderNode;
}

void ElementXmlExporter::fromDomNode(const QDomElement &element) {
	// elements is items node from XML structure
	const QList<core::WoodType> woods = core::GlobalData::instance().repository().woodTypeRepository().getAll();
	QDomNodeList items = element.elementsByTagName("item");
	for (int i {0}; i < items.count(); i++) {
		QDomElement el = items.item(i).toElement();

		core::order::Element element;
		element.setWidth(el.attribute(widthTag).toDouble());
		element.setHeight(el.attribute(heightTag).toDouble());
		element.setLength(el.attribute(lengthTag).toDouble());
		element.setQuantity(el.attribute(quantityTag).toDouble());
		element.setPlanned(el.attribute(plannedTag).toInt());
		element.setName(el.text());

		const QString woodName = el.attribute(woodTag, "").toUpper();
		auto wood = std::find_if(woods.begin(), woods.end(), [&woodName](const core::WoodType& item)->bool{
			return item.name() == woodName;
		});
		element.setWoodType(wood != woods.end() ? *wood : core::WoodType {});
		mData.append(element);
	}
}

} // namespace
