#include "exporters/elementcsvexporter.hpp"
#include <QTextStream>

namespace eSawmill::orders::exporters {

ElementCsvExporter::ElementCsvExporter()
	: core::abstracts::AbstractExporter<core::order::Element>{}
{
	mSeparator = ';';
}

ElementCsvExporter::~ElementCsvExporter() {}

bool ElementCsvExporter::exportData(QIODevice &device) {
	QTextStream file(&device);

	for (auto& item : mData) {
		file << item.width() << separator()		// 1
			 << item.height() << separator()	// 2
			 << item.length() << separator()	// 3
			 << item.quantity() << separator()	// 4
			 << item.planned() << separator()	// 5
			 << item.name() << separator();		// 6
		file << '\n';							// 7
	}
	return true;
}

bool ElementCsvExporter::importData(QIODevice &device) {
	while (!device.atEnd()) {
		QString line = device.readLine();
		if (line.trimmed().startsWith('#') || line.isEmpty())
			continue; // # it's comment line in file

		QStringList cols = line.split(mSeparator);
		// skip rows where user give more parameters
		if (cols.count() != 7)	// look inout exportData ;)
			continue;

		core::order::Element item {};
		item.setWidth(cols[0].toDouble());
		item.setHeight(cols[1].toDouble());
		item.setLength(cols[2].toDouble());
		item.setQuantity(cols[3].toDouble());
		item.setPlanned(cols[4].toInt());
		item.setName(cols[5]);
		mData.append(item);
	}
	return true;
}

} // namespace
