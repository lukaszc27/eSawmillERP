#include "ordersmanager.h"
#include "createorder.h"
#include "exporters/orderxmlexporter.hpp"
#include <messagebox.h>
#include <eloquent/builder.hpp>
#include <dbo/orderarticle.h>
#include <memory>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScopedPointer>
#include <QStringListModel>
#include <QDebug>
#include <QMenu>
#include <QFileDialog>
#include <QFormLayout>
#include <QCompleter>
#include <dbo/saleinvoice.hpp>
#include <dbo/saleinvoiceitem.hpp>
#include <dbo/measureunit_core.hpp>
#include <dbo/woodtype.hpp>
#include <settings/ordersettings.hpp>
#include <settings/appsettings.hpp>
#include "widgets/invoiceexportdialog.hpp"
#include <dbo/ordersaleinvoices.hpp>
#include <sales/salesinvoicesmanager.hpp>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>
#include <exceptions/repositoryexception.hpp>
#include <exceptions/notimplementexception.hpp>
#include <commandexecutor.hpp>
#include "commands/createordercommand.hpp"
#include "commands/editordercommand.hpp"
#include "commands/removeordercommand.hpp"
#include "commands/printordercommand.hpp"
#include "commands/importordercommand.hpp"
#include "commands/exportordercommand.hpp"
#include "commands/createsaleinvoicefromordercommand.hpp"

namespace eSawmill::orders {

OrdersManager::OrdersManager(QWidget* parent)
	: QDialog(parent)
{
	this->setWindowTitle(tr("Zamówienia"));

	createWidgets();
	createModels();
	createContextMenu();
	createConnections();
}

void OrdersManager::handleRepositoryNotification(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::Order) {
		const auto& selection = mTableView->selectionModel()->selection();
		mOrders->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void OrdersManager::createWidgets() {
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemakeButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mCloseButton = new ::widgets::PushButton(tr("Zakończ"), QKeySequence("ESC"), this, QIcon(":/icons/exit"));
	mOperationButton = new ::widgets::PushButton(tr("Operacje"), QKeySequence(), this);
	mDateFilterWidget = new eSawmill::orders::widgets::DateFilterWidget(this);
	mRecordFilterWidget = new eSawmill::orders::widgets::RecordFilterWidget(this);

	mFilterOption = new QListView(this);
	mFilterOption->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	mFilterOption->setEditTriggers(QListView::EditTrigger::NoEditTriggers);

	QMenu* operationMenu = new QMenu();
	operationMenu->addAction(QIcon(":/icons/import"), "Import", this, &OrdersManager::importOrderHandle);
	operationMenu->addAction(QIcon(":/icons/export"), "Eksport", this, &OrdersManager::exportOrderHandle);
	operationMenu->addSeparator();
	mOperationButton->setMenu(operationMenu);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemakeButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mOperationButton);
	buttonsLayout->addStretch(4);
	buttonsLayout->addWidget(mCloseButton);

	QHBoxLayout* filtersLayout = new QHBoxLayout;
	filtersLayout->addWidget(mRecordFilterWidget);
	filtersLayout->addSpacing(12);
	filtersLayout->addWidget(mDateFilterWidget);
	filtersLayout->addStretch(1);

	QVBoxLayout* rightLayout = new QVBoxLayout;
	rightLayout->addWidget(mTableView);
	rightLayout->addSpacing(12);
	rightLayout->addLayout(filtersLayout);

	QHBoxLayout* topLayout = new QHBoxLayout;
	topLayout->addWidget(mFilterOption);
	topLayout->addLayout(rightLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(topLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void OrdersManager::createConnections() {
	connect(mCloseButton, &::widgets::PushButton::clicked, this, &OrdersManager::close);
	connect(mAddButton, &::widgets::PushButton::clicked, this, &OrdersManager::addOrderHandle);
	connect(mRemakeButton, &::widgets::PushButton::clicked, this, &OrdersManager::editOrderHandle);
	connect(mTableView, &::widgets::TableView::returnPressed, this, &OrdersManager::editOrderHandle);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &OrdersManager::removeOrderHandle);
	connect(mRecordFilterWidget, &QGroupBox::clicked, mFilter, &orders::filters::OrdersFilter::setActive);
	connect(mDateFilterWidget, &QGroupBox::clicked,
			mDateRangeFilter, &eSawmill::orders::filters::OrderDateRangeFilter::setActive);

	connect(mRecordFilterWidget->priority(), &QComboBox::currentTextChanged, [&](const QString& priority){
		mFilter->setPriority(priority);
	});
	connect(mRecordFilterWidget->priorityEnable(), &QCheckBox::stateChanged, [&](int state){
		mFilter->setPriorityEnable(state);
	});
	connect(mRecordFilterWidget, &eSawmill::orders::widgets::RecordFilterWidget::filterDataChanged,
			this, &OrdersManager::filterRecordHandle);
	connect(mDateFilterWidget, &eSawmill::orders::widgets::DateFilterWidget::dateRangeChanged,
			this, &OrdersManager::filterByDateHandle);

	// podwojne kliknięcie na liscie z zamówieniami
	connect(mTableView, &::widgets::TableView::doubleClicked, this, [=](const QModelIndex& index){
		Q_UNUSED(index);
		editOrderHandle();
	});

	connect(mFilterOption, &QListView::clicked, this, [&](const QModelIndex &index){
		mOrderFilter->filterByIndex(index.row());
	});
	connect(mTableView, &::widgets::TableView::filesDropped, this, &OrdersManager::importOrderByDroppingFile);
	connect(mAddAction, &QAction::triggered, this, &OrdersManager::addOrderHandle);
	connect(mEditAction, &QAction::triggered, this, &OrdersManager::editOrderHandle);
	connect(mPrintAction, &QAction::triggered, this, &OrdersManager::printOrderHandle);
	connect(mRemoveAction, &QAction::triggered, this, &OrdersManager::removeOrderHandle);
	connect(mImportAction, &QAction::triggered, this, &OrdersManager::importOrderHandle);
	connect(mExportAction, &QAction::triggered, this, &OrdersManager::exportOrderHandle);
	connect(mGenerateSaleInvoiceAction, &QAction::triggered, this, &OrdersManager::generateSaleInvoiceHandle);
	connect(mShowSaleInvoiceAction, &QAction::triggered, this, &OrdersManager::showSaleInvoiceHandle);
	connect(mTableView, &::widgets::TableView::contextMenuShowed, this, &OrdersManager::fitContextMenu);
}

void OrdersManager::createModels() {
	mOrders = new models::Orders(this);
	mOrderFilter = new orders::filters::OrderRoleFilter(this);
	mDateRangeFilter = new eSawmill::orders::filters::OrderDateRangeFilter(this);
	mFilter = new orders::filters::OrdersFilter(this);

	mOrderFilter->setSourceModel(mOrders);
	mDateRangeFilter->setSourceModel(mOrderFilter);
	mFilter->setSourceModel(mDateRangeFilter);

	mTableView->setModel(mFilter);

	QStringList filterOption;
	filterOption << tr("Wszystkie") << tr("Do realizacji") << tr("Po terminie realizacji") << tr("Zrealizowane");

	QStringListModel* filterOptionModel = new QStringListModel(filterOption, this);
	mFilterOption->setModel(filterOptionModel);

	// create completer for filter fields
	QCompleter* nameCompleter = new QCompleter(this);
	nameCompleter->setModel(mOrders);
	nameCompleter->setCompletionColumn(models::Orders::Name);
	nameCompleter->setCompletionRole(Qt::DisplayRole);
	nameCompleter->setCompletionMode(QCompleter::InlineCompletion);
	nameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	mRecordFilterWidget->nameField()->setCompleter(nameCompleter);

	QCompleter* numberCompleter = new QCompleter(this);
	numberCompleter->setModel(mOrders);
	numberCompleter->setCompletionColumn(models::Orders::Number);
	numberCompleter->setCompletionRole(Qt::DisplayRole);
	numberCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	numberCompleter->setCompletionMode(QCompleter::InlineCompletion);
	mRecordFilterWidget->numberField()->setCompleter(numberCompleter);

	QCompleter* contractorCompleter = new QCompleter(this);
	contractorCompleter->setModel(mOrders);
	contractorCompleter->setCompletionColumn(models::Orders::ContractorName);
	contractorCompleter->setCompletionRole(Qt::DisplayRole);
	contractorCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	contractorCompleter->setCompletionMode(QCompleter::InlineCompletion);
	mRecordFilterWidget->contractorNameField()->setCompleter(contractorCompleter);

	QCompleter* phoneCompleter = new QCompleter(this);
	phoneCompleter->setModel(mOrders);
	phoneCompleter->setCompletionColumn(models::Orders::ContractorPhone);
	phoneCompleter->setCompletionRole(Qt::DisplayRole);
	phoneCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	phoneCompleter->setCompletionMode(QCompleter::InlineCompletion);
	mRecordFilterWidget->contractorPhoneField()->setCompleter(phoneCompleter);
}

void OrdersManager::createContextMenu()
{
	mAddAction = new QAction(QIcon(":/icons/add"), tr("Dodaj"), this);
	mAddAction->setShortcut(mAddButton->shortcut());

	mEditAction = new QAction(QIcon(":/icons/edit"), tr("Popraw"), this);
	mEditAction->setShortcut(mRemakeButton->shortcut());

	mRemoveAction = new QAction(QIcon(":/icons/trash"), tr("Usuń"), this);
	mRemoveAction->setShortcut(mRemoveButton->shortcut());

	mPrintAction = new QAction(QIcon(":/icons/print"), tr("Drukuj"), this);
	mImportAction = new QAction(QIcon(":/icons/import"), tr("Importuj"), this);
	mExportAction = new QAction(QIcon(":/icons/export"), tr("Eksportuj"), this);
	mGenerateSaleInvoiceAction = new QAction(QIcon(":/icons/settings"), tr("Generuj dokument sprzedaży"), this);
	mShowSaleInvoiceAction = new QAction(QIcon(":/icons/invoice"), tr("Pokaż dokument sprzedaży"), this);

	QMenu* contextMenu = new QMenu {this};
	contextMenu->addAction(mAddAction);
	contextMenu->addAction(mEditAction);
	contextMenu->addAction(mRemoveAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mPrintAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mImportAction);
	contextMenu->addAction(mExportAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mGenerateSaleInvoiceAction);
	contextMenu->addAction(mShowSaleInvoiceAction);

	mTableView->setContextMenu(contextMenu);
}

void OrdersManager::addOrderHandle() {
	try {
		core::Order order;
		QPointer<CreateOrder> dialog(new CreateOrder(order, this));
		if (dialog->exec() == QDialog::Accepted) {
			std::unique_ptr<eSawmill::orders::commands::CreateOrderCommand> createOrderCommand = std::make_unique<eSawmill::orders::commands::CreateOrderCommand>(order, this);
			if (!core::GlobalData::instance().commandExecutor().execute(std::move(createOrderCommand))) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Tworzenie nowgo zamówienia nie powiodło się!\n"
					   "Zmiany w bazie nie zostały wprowadzone."));
			} else {
				mOrders->get();
			}
		}
	} catch (core::exceptions::RepositoryException&) {
		::widgets::MessageBox::critical(this, tr("Błąd dostawcy danych"),
			tr("Podczas zapisu danych wystąpiły nieoczekiwane błędy.\n"
			   "Uruchom ponownie program, a następnie spróbuj ponownie utworzyć zamówienie."
			   "Jeśli sytuacja się powtórzy, skontaktuj się z dostawcą oprogramowania"));

		// log into file
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Wyjątek core::exceptions::RepositoryException w: %s", Q_FUNC_INFO)
		);
	} catch (core::exceptions::NotImplementException&) {
		::widgets::MessageBox::warning(this, tr("Brak implementacji funkcjonalności"),
			tr("Wywołano metodę która nie jest dostępna po stronie usługi produkcyjnej.\n"
			   "Może to być spowodowane niezgodnością wersji aplikacji klienckiej z aplikacją serwerową"));
	}
}

void OrdersManager::editOrderHandle() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
			tr("Przed wykonaniem tej operacji musisz zaznaczyć element na liście"));
		return;
	}

	try {
		auto& repository = core::GlobalData::instance().repository().orderRepository();
		const core::eloquent::DBIDKey id = mTableView->currentIndex().data(models::Orders::Id).toUInt();
		Q_ASSERT(id > 0);

		core::repository::ResourceLocker resourceLocker (id, repository);
		core::Order order = repository.getOrder(id);
		QPointer<CreateOrder> dialog(new CreateOrder(order, this));
		if (dialog->exec() == QDialog::Accepted) {
			std::unique_ptr<eSawmill::orders::commands::EditOrderCommand> editOrderCommand
					= std::make_unique<eSawmill::orders::commands::EditOrderCommand>(order, this);

			if (!core::GlobalData::instance().commandExecutor().execute(std::move(editOrderCommand))) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Podczas aktualizacji informacji o zamówieniu wystąpiły błędy.\n"
					   "Zmiany nie zostały wprowadzone."));
			} else {
				mOrders->get();
			}
		}
	} catch(core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this,
			tr("Blokada dostępu"),
			tr("Wybrane zamówienie jest aktualnie edytowane przez innego użytkownika - Blokada dostępu"));
	}
}

void OrdersManager::removeOrderHandle() {
	try {
		if (mTableView->selectionModel()->selectedRows().count() > 0) {
			int iRet = ::widgets::MessageBox::question(this, tr("Pytanie"),
				tr("Czy chcesz usunąć zaznaczone obiekty?"), "",
				QMessageBox::Yes | QMessageBox::No);

			if (iRet == QMessageBox::Yes) {
				auto& repository = core::GlobalData::instance().repository().orderRepository();
				const core::eloquent::DBIDKey id = mTableView->currentIndex().data(models::Orders::Id).toUInt();
				core::repository::ResourceLocker resourceLocker(id, repository);
				std::unique_ptr<eSawmill::orders::commands::RemoveOrderCommand> removeOrderCommand
						= std::make_unique<eSawmill::orders::commands::RemoveOrderCommand>(id, this);

				if (!core::GlobalData::instance().commandExecutor().execute(std::move(removeOrderCommand))) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Podczas usuwania zamówienia wystąpiły błędy.\n"
						   "Zmiany w bazie nie zostały wprowadzone!"));
				} else {
					mOrders->get();
				}
			}
		} else {
			::widgets::MessageBox::warning(this, tr("Informacja"),
				tr("Przed usunięciem zamówień musisz wybrać które mają zostać usunięte"));
		}
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Aktualnie usuwane zamówienie jest edytowane przez innego użytkownika.\n"
			   "Poczekaj na zakończenie edycji"));
	}
}

void OrdersManager::importOrderHandle() {
	const QString filename = QFileDialog::getOpenFileName(this, "Import zamówień", QDir::currentPath(), "Pliki XML (*.xml)");
	if (filename.isEmpty())
		return;

	std::unique_ptr<eSawmill::orders::commands::ImportOrderCommand> importCommand
		= std::make_unique<eSawmill::orders::commands::ImportOrderCommand>(filename, this);

	if (core::GlobalData::instance().commandExecutor().execute(std::move(importCommand))) {
		mOrders->get();
	} else {
		::widgets::MessageBox::warning(this, tr("Import zamówień"),
			tr("Nie można otworzyć wskazanego pliku!"));
	}
}

void OrdersManager::printOrderHandle() {
	try {
		const auto& orderId = mTableView->selectionModel()->currentIndex().data(eSawmill::orders::models::Orders::Roles::Id).toUInt();
		if (orderId > 0) {
			std::unique_ptr<eSawmill::orders::commands::PrintOrderCommand> printCommand
				= std::make_unique<eSawmill::orders::commands::PrintOrderCommand>(orderId, this);

			if (!core::GlobalData::instance().commandExecutor().execute(std::move(printCommand))) {
				::widgets::MessageBox::critical(this, tr("Błąd wydruku"),
					tr("Błąd podczas generowania oraz drukowania wskazanego zamówienia!"));
			}
		} else {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Identyfikator zamówienia jest nieprawidłowy!"));
		}
	} catch (core::exceptions::ForbiddenException&) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Wybrane zamówienie jest w trakcie edycji przez innego użytkownika! - Poczekaj na zakończenie edycji."));
	}
}

void OrdersManager::importOrderByDroppingFile(const QStringList &files)
{
	for (const auto& filename : files) {
		QFile file {filename};
		if (file.open(QFile::ReadOnly | QFile::Text)) {
			std::unique_ptr<eSawmill::orders::exporters::OrderXmlExporter> importer =
					std::make_unique<eSawmill::orders::exporters::OrderXmlExporter>();

			importer->importData(file);
			file.close();
			mOrders->get();
		}
		else {
			::widgets::MessageBox::critical(this, "Import zamówień z pliku",
				"Nie można zaimportować zamówień z pliku.\n"
				"Brak dostępu!");
			continue;
		}
	}
}

void OrdersManager::exportOrderHandle() {
	if (mTableView->selectionModel()->selectedRows().count() <= 0) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			("Przed wykonaniem operacji eksportu danych musisz znaznaczyć elementy przeznaczone do eksportu"));
		return;
	}

	QString filename = QFileDialog::getSaveFileName(this, tr("Export zamówienia do pliku"),
		QDir::currentPath(), "Plik XML (*.xml)");
	if (filename.isEmpty())
		return;

	if (!filename.endsWith(".xml"))
		filename += ".xml";

	std::vector<core::eloquent::DBIDKey> orderIds;
	for (const auto& selectedRow : mTableView->selectionModel()->selectedRows()) {
		orderIds.push_back(selectedRow.data(eSawmill::orders::models::Orders::Roles::Id).toUInt());
	}
	std::unique_ptr<eSawmill::orders::commands::ExportOrderCommand> exportCommand
			= std::make_unique<eSawmill::orders::commands::ExportOrderCommand>(filename, orderIds, this);

	if (!core::GlobalData::instance().commandExecutor().execute(std::move(exportCommand))) {
		::widgets::MessageBox::warning(this, tr("Eksport zamówienia do pliku"),
		   tr("NIe można zapisać eksportowanych danych do pliku!\n"
			  "Brak dostępu"));
	}
}

void OrdersManager::generateSaleInvoiceHandle()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
				tr("Nie wybrano dokumenu z listy"));
		return;
	}
	core::Order order;
	const core::eloquent::DBIDKey orderId = mTableView->currentIndex().data(orders::models::Orders::Roles::Id).toInt();
	auto& repository = core::GlobalData::instance().repository().orderRepository();
	order = repository.getOrder(orderId);
	if (!order) {
		::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Podczas pobierania danych zamówienia z bazy wystąpiły błędy"));
		return;
	}
	if (!order.isRealised()) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
			tr("Zamówienie nie zostało zrealizowane!\n"
			   "Przed generowaniem dokumentu sprzedaży należy oznaczyć zamówienie jako zrealizowane"));
		return;
	}
	if (order.hasInvoices(true)) {
		const core::SaleInvoice invoice = order.invoice();
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Dla zamówienia %1 wygenerowano dokument sprzedaży nr %2")
				.arg(order.number(), invoice.number()));
		return;
	}

	QPointer<eSawmill::invoices::widgets::InvoiceExportDialog> dialog(new eSawmill::invoices::widgets::InvoiceExportDialog(this));
	if (dialog->exec() == QDialog::Accepted) {
		std::unique_ptr<eSawmill::orders::commands::CreateSaleInvoiceFromOrderCommand> command
				= std::make_unique<eSawmill::orders::commands::CreateSaleInvoiceFromOrderCommand>(order, this);

		command->setWarehouseId(dialog->warehouseId());
		command->setPaymentDeadlineDate(dialog->paymentDeadlineDate());
		command->setPaymentDeadlineDays(dialog->paymentDeadlineDays());
		command->setPaymentName(dialog->paymentName());

		if (core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Pomyślnie utworzono dokument sprzedaży dla zamówienia nr %1").arg(order.number()));
		} else {
			::widgets::MessageBox::critical(this, tr("Błąd"), tr("Błąd podczas tworzenia dokumentu sprzedaży"));
		}
	}
}

void OrdersManager::showSaleInvoiceHandle()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie wybrano żadnego zamówienia do którego na być zastosowana operacja"));
		return;
	}

	try {
		auto& repositories = core::GlobalData::instance().repository();
		const core::eloquent::DBIDKey orderId = mTableView->selectionModel()->currentIndex().data(eSawmill::orders::models::Orders::Roles::Id).toInt();
		if (!repositories.orderRepository().lock(orderId))
			return;
		core::Order order;
		order = repositories.orderRepository().getOrder(orderId);
		if (order.hasInvoices()) {
			core::SaleInvoice invoice;
			invoice = order.invoice(true);
			if (repositories.invoiceRepository().hasSaleInvoiceCorrection(invoice.id()))
				invoice = repositories.invoiceRepository().getCorrectionForSaleInvoice(invoice.id());

			if (invoice) {
				eSawmill::invoices::sales::SalesInvoicesManager::editInvoice(invoice.id(), this);
			} else {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Nie znaleziono dokumentu w bazie!\n"
					   "Dokument prawdopodobnie został usunięty lub wystąpiły błędy przy odczytywaniu klucza dokumentu\n"
					   "Skontaktuj się z dostawcą oprogramowania w celu przeprowadzenia analizy bazy danych"));
			}
		} else {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Dla zamówienia %1 nie wygenerowano dokumentu sprzedaży").arg(order.number()));
		}
		if (!repositories.orderRepository().unlock(orderId))
			return;
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this,
			tr("Blokada dostępu"),
			tr("Wybrane zamówienie jest obecnie edytowane przez innego użytkownika - Blokada dostępu"));
	}
}

void OrdersManager::filterRecordHandle(orders::widgets::RecordFilterWidget::FilterOptions &options) {
	mFilter->setName(options.name);
	mFilter->setNumber(options.number);
	mFilter->setContractor(options.contractorName);
	mFilter->setPhone(options.contractorPhone);
	mFilter->setPriorityEnable(options.usePriority);
	mFilter->setPriority(options.priority);
}

void OrdersManager::filterByDateHandle(const orders::widgets::DateFilterWidget::DateFilterOptions &options) {
	mDateRangeFilter->setFilterKeyColumn(options.filterByColumn);
	mDateRangeFilter->setMinDate(options.minDate);
	mDateRangeFilter->setMaxDate(options.maxDate);
}

void OrdersManager::fitContextMenu() {
	bool enable = mTableView->selectionModel()->hasSelection();
	mEditAction->setEnabled(enable);
	mRemoveAction->setEnabled(enable);
	mGenerateSaleInvoiceAction->setEnabled(enable);
	mShowSaleInvoiceAction->setEnabled(enable);
	mShowSaleInvoiceAction->setEnabled(enable && mTableView->selectionModel()->currentIndex().data(eSawmill::orders::models::Orders::Roles::WasGeneratedInvoice).toBool());
	mGenerateSaleInvoiceAction->setEnabled(enable && !mShowSaleInvoiceAction->isEnabled());
}

} // namespace eSawmill
