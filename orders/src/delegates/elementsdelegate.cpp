#include "delegates/elementsdelegate.h"
#include "models/elements.h"
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>
#include <QCompleter>
#include <models/woodtypelistmodel.hpp>
#include <models/orders/elementnamemodel.hpp>


namespace eSawmill::orders::delegates {

ElementsDelegate::ElementsDelegate(QObject* parent)
	: QStyledItemDelegate(parent)
{
}

QWidget* ElementsDelegate::createEditor(QWidget *parent,
										const QStyleOptionViewItem &option,
										const QModelIndex &index) const
{
	Q_UNUSED(option);

	int col = index.column();
	if (col != orders::models::Elements::Cubature &&
		col != orders::models::Elements::Columns::IsPlanned &&
		col != orders::models::Elements::Name &&
		col != orders::models::Elements::WoodType) {
		return new QDoubleSpinBox(parent);
	}

	if (col == orders::models::Elements::Columns::IsPlanned)
		return new QCheckBox(parent);

	if (col == orders::models::Elements::Name) {
		QLineEdit* lineEdit = new QLineEdit(parent);
		QCompleter* completer = new QCompleter(parent);
		completer->setModel(new ::widgets::models::orders::ElementName(parent));
		completer->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
		completer->setCompletionMode(QCompleter::InlineCompletion);
		lineEdit->setCompleter(completer);
		return lineEdit;
	}

	if (col == orders::models::Elements::WoodType) {
		QComboBox* combo = new QComboBox(parent);
		combo->setModel(new core::models::WoodTypeListModel(parent));
		return combo;
	}

	return nullptr;
}

void ElementsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
	int col = index.column();
	if (col != orders::models::Elements::Columns::IsPlanned &&
		col != orders::models::Elements::Cubature &&
		col != orders::models::Elements::Name &&
		col != orders::models::Elements::WoodType) {
		QDoubleSpinBox* spinBox = qobject_cast<QDoubleSpinBox*>(editor);
		spinBox->setMaximum(10000);
		spinBox->setValue(index.data().toDouble());
	}

	if (col == orders::models::Elements::Columns::IsPlanned) {
		QCheckBox* checkbox = qobject_cast<QCheckBox*>(editor);
		checkbox->setChecked(index.data(orders::models::Elements::Roles::Planned).toBool());
	}

	if (col == orders::models::Elements::Name) {
		QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
		lineEdit->setText(index.data().toString());
	}

	if (col == orders::models::Elements::WoodType) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		combo->setCurrentText(index.data().toString());
	}
}

void ElementsDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
	int col = index.column();
	if (col != orders::models::Elements::Columns::IsPlanned &&
		col != orders::models::Elements::Columns::Cubature &&
		col != orders::models::Elements::Name &&
		col != orders::models::Elements::WoodType) {
		QDoubleSpinBox* spinbox = qobject_cast<QDoubleSpinBox*>(editor);
		model->setData(index, spinbox->value());
	}

	if (col == orders::models::Elements::Columns::IsPlanned) {
		QCheckBox* checkbox =qobject_cast<QCheckBox*>(editor);
		model->setData(index, checkbox->isChecked());
	}

	if (col == orders::models::Elements::Name) {
		QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
		model->setData(index, lineEdit->text().toUpper());
	}

	if (col == orders::models::Elements::WoodType) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		model->setData(index, combo->currentData(core::models::WoodTypeListModel::Id));
	}
}

} // namespace eSawmill
