#include "verifications/unrealisedordersverification.hpp"
#include <dbo/order.h>
#include <QList>
#include <QDate>
#include <globaldata.hpp>


namespace eSawmill::orders::verifications {

UnrealisedOrdersVerification::UnrealisedOrdersVerification()
	: core::verifications::AbstractVerification {}
{
}

void UnrealisedOrdersVerification::execute() {
	auto orders = core::GlobalData::instance().repository().orderRepository().getAllOrders();
	if (orders.size() <= 0) {
		mValid = true;
		return;
	}

	const QDate currentDate = QDate::currentDate();
	for (const core::Order& order : orders) {
		if (order.endDate() < currentDate && !order.isRealised()) {
			addNotification(QString("Przekroczono czas realizacji zamówienia %1 (%2 dni opóźnienia)")
				.arg(order.number())
				.arg(order.endDate().daysTo(currentDate)),
				core::verifications::Notification::Type::Warning);
		}
	}

	mValid = !(notifications().size() > 0);
}

} // namespace eSawmill::orders::verifications
