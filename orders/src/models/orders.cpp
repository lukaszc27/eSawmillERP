#include "models/orders.h"
#include "settings/appsettings.hpp"
#include <messagebox.h>
#include <dbo/auth.h>
#include <dbo/contractor.h>
#include <globaldata.hpp>


namespace eSawmill::orders::models {

Orders::Orders(QObject* parent)
	: core::models::AbstractModel<core::Order>{parent}
{
	get();

	core::GlobalData::instance().commandExecutor().attachListener(this);
}

Orders::~Orders()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);
}

QStringList Orders::headerData() const {
	return QStringList()
		<< tr("Nazwa")
		<< tr("Numer")
		<< tr("Kontrahent")
		<< tr("Telefon")
		<< tr("Priorytet")
		<< tr("Data dodania")
		<< tr("Termin realizacji");
}

int Orders::rowCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return items().size();
}

int Orders::columnCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return headerData().size();
}

QVariant Orders::headerData(int section, Qt::Orientation orientation, int role) const {
	if (orientation == Qt::Horizontal) {
		switch (role) {
		case Qt::DisplayRole:
			return headerData().at(section);
			break;
		}
	}
	return QVariant();
}

QVariant Orders::data(const QModelIndex &index, int role) const {
	const core::settings::OrderSettings& settings = core::settings::AppSettings::order();
	const int row = index.row();
	const int col = index.column();
	core::Order current = items().at(row);
	const core::Contractor& contractor = current.contractor();

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return current.name();
		case Number: return current.number();
		case AddDate: return current.addDate();
		case EndDate: return current.endDate();
		case ContractorPhone: return contractor.contact().phone();
		case Priority: {
			QStringList priority;
			priority << tr("Niski") << tr("Normalny") << tr("Wysoki");
			return priority.at(current.priority());
		}
		case ContractorName: {
			if (contractor.name().isEmpty())
				return contractor.personName() + " " + contractor.personSurname();
			else return contractor.name();
		} // case
		} // switch display role
		break;

	case Qt::ForegroundRole:
		// order not processed and after the deadline
		if (!current.isRealised()
			&& current.endDate() < QDate::currentDate()
			&& settings.deadlineAfterRemind()) {
			return QColor(settings.deadlineAfterRemindColor());
		}

		// order completed
		if (current.isRealised())
			return QColor(settings.value("Orders/realisedColor").value<QColor>());

		// an order with an impending completion date
		if (!current.isRealised()
			&& QDate::currentDate().addDays(settings.daysToWarning()) >= current.endDate()
			&& settings.deadlineBeforeRemind()) {
			return QColor(settings.deadlineBeforeRemindColor());
		}
		break;

	case Qt::BackgroundRole:
		{
		const int alpha = 100;
			const QColor colors[] = {
				QColor(79, 209, 168, alpha),	///< low color order
				QColor(255, 255, 255, alpha),	///< normal color order
				QColor(176, 94, 247, alpha)		///< high color order
			};
			if (!current.isRealised()
				&& current.endDate() > QDate::currentDate()
				&& QDate::currentDate().addDays(settings.daysToWarning()) <= current.endDate()
				&& settings.markByPriority()
				&& col == Columns::Priority)
			{
				return colors[current.priority()];
			}
		}
		break;

	case Roles::Id: return current.id();
	case Roles::IsRealised: return current.isRealised();
	case Roles::WasGeneratedInvoice: return current.hasInvoices();
	}

	return QVariant();
}

void Orders::createOrUpdate(core::Order element) {
	Q_UNUSED(element);
}

void Orders::destroy(core::Order element) {
	Q_UNUSED(element);
}

void Orders::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.orderRepository()
			.getAllOrders();
		endResetModel();
	} catch (...) {
		throw;	// rethrow all exceptions so that can be cached by views
	}
}

void Orders::commandExecuted()
{
	get();
}

} // namespace eSawmill
