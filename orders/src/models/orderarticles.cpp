#include "models/orderarticles.hpp"
#include "dbo/orderarticle.h"
#include "dbo/article.h"
#include "dbo/order.h"
#include <eloquent/builder.hpp>
#include <globaldata.hpp>


namespace eSawmill::orders::models {

Articles::Articles(QObject* parent)
	: core::models::AbstractModel<core::Article>(parent)
	, mId(0)
{
//	connect(this, &Articles::listDataChanged, [this](){
//		const double totalPrice = this->totalPrice();
//		emit totalPriceChanged(totalPrice);
//	});
}

Articles::Articles(int id, QObject *parent)
	: Articles {parent}
{
	setParentObjectId(id);

	// pobranie listy artykułów z bazy
	// przypisanych do konkrentego zamówienia
	get();
}

void Articles::setOrder(core::eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	setParentObjectId(orderId);
	get();
}

Qt::ItemFlags Articles::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	flags |= Qt::ItemIsEnabled;

	return flags;
}

//int Articles::rowCount(const QModelIndex &index) const {
//	Q_UNUSED(index);

//	return items().size();
//}

//int Articles::columnCount(const QModelIndex &index) const {
//	Q_UNUSED(index);

//	return headerData().size();
//}

QStringList Articles::headerData() const {
	QStringList list;
	list << tr("Nazwa") << tr("EAN") << tr("Ilość") << tr("Jedn. miary") << tr("Cena") << tr("Wartość");

	return list;
}

QVariant Articles::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return headerData().at(section);
		break;
	}
	return QVariant();
}

QVariant Articles::data(const QModelIndex &index, int role) const {
	int row = index.row(),
		col = index.column();

//	core::order::Article current = mList.at(row);
//	static core::Article article;
//	static int lastArticleId;
//	if (lastArticleId != current.get("ArticleId").toInt()) {
//		article = current.article();
//		lastArticleId = current.get("ArticleId").toInt();
//	}

//	switch (role) {
//	case Qt::DisplayRole:
//		switch (col) {
//		case Name: return article.get("Name");
//		case EAN: return article.get("BarCode");
//		case Unit: return article.unit()["ShortName"].toString().toUpper();
//		case Quantity: return current.get("Quantity");
//		case Price: return article.get("PriceSaleBrutto");
//		case Value: {
//			double qty = current.get("Quantity").toDouble();
//			double price = article.get("PriceSaleBrutto").toDouble();

//			return price * qty;
//		} // case Value
//		} break;
//	}
	const core::Article& current = items().at(row);
	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return current.name();
		case EAN: return current.barCode();
		case Unit: return current.unit().shortName();
		case Quantity: return current.quantity();
		case Price: return current.priceSaleBrutto();
		case Value: {
			double qty = current.quantity();
			double price = current.priceSaleBrutto();
			return qty * price;
			} break;
		}
		break;
	}

	return QVariant {};
}

bool Articles::setData(const QModelIndex &index, const QVariant &value, int role)
{
	switch (role) {
	case Qt::CheckStateRole:
		if (index.column() == 0)
			mList[index.row()].setChecked(value.toBool());
		break;
	}
	return true;
}

void Articles::insert(core::Article element) {
	this->beginResetModel();
	element.set("OrderId", mId);
	mList.append(element);
	notifyDataChanged();
	this->endResetModel();
}

void Articles::insert(QList<core::Article> elements) {
	beginResetModel();
	mList.append(elements);
	notifyDataChanged();
	endResetModel();
}

double Articles::totalPrice() {
	double total {0};
	for (const auto& item : items()) {
		total += (item.priceSaleBrutto() * item.quantity());
	}
	return total;
}

void Articles::get() {
	try {
		const core::eloquent::DBIDKey id = parentObjectId();
		if (id > 0) {
			beginResetModel();
			endResetModel();
			mList = core::GlobalData::instance().repository().orderRepository().getArticlesForOrder(id);
//			Q_EMIT listDataChanged();
		}
	} catch (...) {
		throw;
	}
}

void Articles::createOrUpdate(core::Article element) {
	Q_UNUSED(element);
}

void Articles::destroy(core::Article element) {
	Q_UNUSED(element);
}

} // namespace eSawmill
