#include "widgets/elementswidget.h"
#include "delegates/elementsdelegate.h"

#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QHeaderView>
#include <dialogs/orders/createelement.h>
#include "createmultipleelements.hpp"
#include "exporters/elementxmlexporter.hpp"
#include "exporters/elementcsvexporter.hpp"
#include <messagebox.h>
#include <dbo/order.h>
#include <memory>
#include <QDebug>
#include <globaldata.hpp>
#include <exceptions/repositoryexception.hpp>


namespace eSawmill::orders::widgets {

ElementsWidget::ElementsWidget(core::models::AbstractElementsModel<core::order::Element>* model, QWidget* parent)
	: QWidget(parent)
	, mElements(model)
{
	createWidgets();
	createActions();
	createModels();
	createConnections();
}

void ElementsWidget::load(core::Order& order) {
	Q_ASSERT(order.id() > 0);
	mElements->insert(order.elements());
}

QList<core::order::Element> ElementsWidget::items() const {
	auto items = mElements->items();
	// add spare length for all elements
	for (auto& item : items) {
		double len = item.length() + mElements->spareLength();
		item.setLength(len);
	}
	return items;
}

void ElementsWidget::setReadOnly(bool readonly) {
	setProperty("readonly", readonly);

	mElementAddButton->setDisabled(readonly);
	mElementRemoveButton->setDisabled(readonly);
	mAdditionalLength->setDisabled(readonly);
	mUseAdditionalLength->setDisabled(readonly);
	mUseAdditionalLength->setChecked(mAdditionalLength->value() > 0);
	mElements->setReadOnly(readonly);
	mOperationsButton->setDisabled(readonly);
}

void ElementsWidget::setSpareLength(double length) {
	const double value = length / 100.f;
	mUseAdditionalLength->setChecked(value > 0);
	mAdditionalLength->setValue(length);
	mElements->setSpareLength(value);

	emit spareLengthChanged(value);
}

void ElementsWidget::createWidgets()
{
	mElementsView = new ::widgets::TableView(this);
	mElementAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence(""), this, QIcon(":icons/add"));
	mElementRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mOperationsButton = new ::widgets::PushButton(tr("Operacje"), QKeySequence(), this);
	mAdditionalLength = new QDoubleSpinBox(this);
	mUseAdditionalLength = new QCheckBox(tr("Dodaj zapas długości"), this);

	mAdditionalLength->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUseAdditionalLength->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mAdditionalLength->setEnabled(false);
	mElementAddButton->setPlaceholderText("Ins");

	mTotalValueElementsLabel = new QLabel(tr("Razem: 0 m<sup>3</sup>"), this);
	mTotalPlannedElementsLabel = new QLabel(tr("Strugane: 0 m<sup>3</sup>"), this);

	// dodanie delegata umożliwiającego edycję elementów po dodaniu
	mElementsView->setItemDelegate(new orders::delegates::ElementsDelegate);

	QMenu* addElementButtonMenu = new QMenu(mElementAddButton);
	addElementButtonMenu->addAction(tr("Dodaj element"),
									this, &ElementsWidget::addElement_clicked, QKeySequence("INS"));

	addElementButtonMenu->addAction(tr("Grupowe dodawanie elementów"),
									this, &ElementsWidget::addMultipleElementsHandle, QKeySequence("CTRL+INS"));
	mElementAddButton->setMenu(addElementButtonMenu);

	mTotalValueElementsLabel->setAlignment(Qt::AlignRight);
	mTotalPlannedElementsLabel->setAlignment(Qt::AlignRight);

	QVBoxLayout* valuesLayout = new QVBoxLayout;
	valuesLayout->addWidget(mTotalValueElementsLabel);
	valuesLayout->addWidget(mTotalPlannedElementsLabel);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mElementAddButton);
	buttonsLayout->addWidget(mElementRemoveButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mOperationsButton);
	buttonsLayout->addStretch(2);

	// filtry elementów
	mElementsFilterGroup = new QGroupBox(tr("Filtr prze&kroju"), this);
	mElementsLengthFilterGroup = new QGroupBox(tr("Filtr &długości"), this);
	mFilterWidth = new QDoubleSpinBox(mElementsFilterGroup);
	mFilterHeight = new QDoubleSpinBox(mElementsFilterGroup);
	mFilterMinLength = new QDoubleSpinBox(mElementsLengthFilterGroup);
	mFilterMaxLength = new QDoubleSpinBox(mElementsLengthFilterGroup);
	mFilterButton = new QPushButton(tr("&Filtruj"), this);
	mHideWoodTypeColumnCheck = new QCheckBox(tr("Ukryj rodzaj drewna"), this);
	mHidePlannedColumnCheck = new QCheckBox(tr("Ukryj kolumnę strugane"), this);
	mHideNameColumnCheck = new QCheckBox(tr("Ukryj kolumnę nazwa"), this);

	mFilterButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mFilterButton->setDisabled(true);
	mElementsFilterGroup->setCheckable(true);
	mElementsLengthFilterGroup->setCheckable(true);
	mElementsFilterGroup->setChecked(false);
	mElementsLengthFilterGroup->setChecked(false);

	QHBoxLayout* filterLayout = new QHBoxLayout(mElementsFilterGroup);
	filterLayout->addWidget(new QLabel(tr("Szer"), mElementsFilterGroup));
	filterLayout->addWidget(mFilterWidth);
	filterLayout->addSpacing(24);
	filterLayout->addWidget(new QLabel(tr("Wys"), mElementsFilterGroup));
	filterLayout->addWidget(mFilterHeight);
	filterLayout->addStretch(1);

	QHBoxLayout* lengthFilter = new QHBoxLayout(mElementsLengthFilterGroup);
	lengthFilter->addWidget(new QLabel(tr("Od"), mElementsLengthFilterGroup));
	lengthFilter->addWidget(mFilterMinLength);
	lengthFilter->addSpacing(24);
	lengthFilter->addWidget(new QLabel(tr("Do"), mElementsLengthFilterGroup));
	lengthFilter->addWidget(mFilterMaxLength);
	lengthFilter->addStretch(1);

	QHBoxLayout* filtersLayout = new QHBoxLayout;
	filtersLayout->addWidget(mElementsFilterGroup);
	filtersLayout->addWidget(mElementsLengthFilterGroup);
	filtersLayout->addWidget(mFilterButton);

	QHBoxLayout* hidesLayout = new QHBoxLayout;
	hidesLayout->addWidget(mHideNameColumnCheck);
	hidesLayout->addSpacing(8);
	hidesLayout->addWidget(mHideWoodTypeColumnCheck);
	hidesLayout->addSpacing(8);
	hidesLayout->addWidget(mHidePlannedColumnCheck);
	hidesLayout->addStretch(1);
	hidesLayout->addLayout(valuesLayout);

	QHBoxLayout* additionalLengthLayout = new QHBoxLayout;
	additionalLengthLayout->addWidget(mUseAdditionalLength);
	additionalLengthLayout->addWidget(mAdditionalLength);
	additionalLengthLayout->addWidget(new QLabel("cm", this));
	additionalLengthLayout->addStretch(1);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(filtersLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(additionalLengthLayout);
	mainLayout->addWidget(mElementsView);
	mainLayout->addLayout(hidesLayout);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(buttonsLayout);
}

void ElementsWidget::createConnections() {
	connect(mFilterButton, &QPushButton::clicked, this, &ElementsWidget::filterButton_clicked);
	connect(mElementRemoveButton, &::widgets::PushButton::clicked, this, &ElementsWidget::removeElement_clicked);
	connect(mAdditionalLength, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &ElementsWidget::setSpareLength);

	connect(mUseAdditionalLength, QOverload<int>::of(&QCheckBox::stateChanged), this, [&](int state){
		mAdditionalLength->setEnabled(state);
		if (state == Qt::Unchecked)
			mElements->setSpareLength(0);
		else if (state == Qt::Checked)
			mElements->setSpareLength(mAdditionalLength->value() / 100.f);
	});
	connect(mAdditionalLength, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [&](double length){
		mElements->setSpareLength(length / 100.f);
	});

	//  obsługa sygnału zaznaczenia/odznaczenia filtra elementów
	connect(mElementsFilterGroup, &QGroupBox::clicked, this, [=](bool checked){
		mFilterButton->setEnabled(checked || mElementsLengthFilterGroup->isChecked());
		mElementFilter->enableFetchWithSection(checked);	// aktywacja/dezaktywacja filtra elementów (filtr przekroju)
	});

	// obsługa sygnału zaznaczenia/odznaczenia filtra długości elementu
	connect(mElementsLengthFilterGroup, &QGroupBox::clicked, this, [=](bool checked){
		mFilterButton->setEnabled(checked || mElementsFilterGroup->isChecked());
		mElementFilter->enableFetchWithLength(checked);	// aktywacja/dezaktywacja filtra elementów (filtr długości)
	});

	// przeliczanie m3 elementów dodanych do zamówienia
	mElements->onDataChanged([&]()->void{
		recalculateAndUpdatePrices();
	});
	mElements->onSpareLengthChanged([&](double spareLength)->void {
		Q_UNUSED(spareLength);
		recalculateAndUpdatePrices();
		Q_EMIT spareLengthChanged(spareLength);
	});

	connect(mHideWoodTypeColumnCheck, QOverload<int>::of(&QCheckBox::stateChanged), [&](int state){
		if (state)
			mElementsView->hideColumn(orders::models::Elements::Columns::WoodType);
		else mElementsView->showColumn(orders::models::Elements::Columns::WoodType);
	});

	connect(mHidePlannedColumnCheck, QOverload<int>::of(&QCheckBox::stateChanged), [&](int state){
		if (state)
			mElementsView->hideColumn(orders::models::Elements::Columns::IsPlanned);
		else mElementsView->showColumn(orders::models::Elements::Columns::IsPlanned);
	});

	connect(mHideNameColumnCheck, QOverload<int>::of(&QCheckBox::stateChanged), [&](int state){
		if (state)
			mElementsView->hideColumn(orders::models::Elements::Columns::Name);
		else mElementsView->showColumn(orders::models::Elements::Columns::Name);
	});
	connect(mElementsView, &::widgets::TableView::filesDropped, this, &ElementsWidget::importDataByDroppingFile);
}

void ElementsWidget::createActions()
{
	QAction* markAsPlannedAction = new QAction("Oznacz jako strugane", this);
	QAction* markAsUnplannedAction = new QAction("Oznacz jako nie strugane", this);

	bool readonly = property("readonly").toBool();
	markAsPlannedAction->setDisabled(readonly);
	markAsUnplannedAction->setDisabled(readonly);

	connect(markAsPlannedAction, &QAction::triggered, this, &ElementsWidget::markCheckedAsPlannedHandle);
	connect(markAsUnplannedAction, &QAction::triggered, this, &ElementsWidget::markCheckedAsUnplannedHandle);

	QMenu* operationMenu = new QMenu(this);
	operationMenu->addAction(markAsPlannedAction);
	operationMenu->addAction(markAsUnplannedAction);
	operationMenu->addSeparator();

	QAction* importAction = new QAction (QIcon(":/icons/import"), tr("Import z pliku"), this);
	connect(importAction, &QAction::triggered, this, &ElementsWidget::importFromFile);

	importAction->setDisabled(readonly);
	operationMenu->addAction(importAction);
    operationMenu->addAction(QIcon(":/icons/export"), "Eksport do pliku", this, &ElementsWidget::exportToFile);

	mOperationsButton->setMenu(operationMenu);
}

void ElementsWidget::addElement_clicked()
{
	QScopedPointer<::widgets::dialogs::orders::CreateElement> dialog(new ::widgets::dialogs::orders::CreateElement(this));
	if (dialog->exec() == QDialog::Accepted) {
		mElements->insert(dialog->element());
	}
}

void ElementsWidget::addMultipleElementsHandle()
{
	QScopedPointer<CreateMultipleElements> dialog(new CreateMultipleElements(this));
	if (dialog->exec() == QDialog::Accepted) {
		for (const auto& el : dialog->items())
			mElements->insert(el);
	}
}

void ElementsWidget::removeElement_clicked()
{
	if (auto rows = mElementsView->selectionModel()->selectedRows(); rows.size() > 0) {
		int ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Znaleziono %1 zaznaczonych elementów.\n"
				"Czy na pewno chcesz je usunąć?").arg(rows.size()), "",
			QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::Yes) {
			for (auto& row : rows) {
				mElements->setData(row, true, Qt::CheckStateRole);
			}
			mElements->deleteSelectedItems();
		}
	}
	else {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Przed usunięciem elementów z listy musisz wybrać które elementy mają zostać usunięte"));
	}
}

void ElementsWidget::filterButton_clicked()
{
	mElementFilter->enableFetchWithSection(mElementsFilterGroup->isChecked());
	mElementFilter->enableFetchWithLength(mElementsLengthFilterGroup->isChecked());
	mElementFilter->setFilterLength(mFilterMinLength->value(), mFilterMaxLength->value());
	mElementFilter->setFilterSection(mFilterWidth->value(), mFilterHeight->value());
}

void ElementsWidget::createModels()
{
	mElementFilter = new orders::filters::ElementsFilter(this);

	mElementFilter->setSourceModel(mElements);
	mElementsView->setModel(mElementFilter);
}

void ElementsWidget::exportToFile() {
	QString selectedFilter;
	QString filename = QFileDialog::getSaveFileName(this, tr("Eksport danych do pliku"), QDir::currentPath(),
		"Plik XML (*.xml);;Plik CSV (*.csv)", &selectedFilter);

	if (!filename.isEmpty()) {
		std::unique_ptr<core::abstracts::AbstractExporter<core::order::Element>> exporter;
		if (filename.trimmed().toUpper().endsWith("XML") || selectedFilter.toUpper().contains("XML")) {
			exporter = std::make_unique<eSawmill::orders::exporters::ElementXmlExporter>();
			if (!filename.endsWith(".xml"))
				filename += ".xml";
		}

		if (filename.trimmed().toUpper().endsWith("CSV") || selectedFilter.toUpper().contains("CSV")) {
			exporter = std::make_unique<eSawmill::orders::exporters::ElementCsvExporter>();
			if (!filename.endsWith(".csv"))
				filename += "csv";
		}

		if (QFile file(filename); file.open(QFile::WriteOnly | QFile::Text)) {
			exporter->setData(mElements);
			exporter->exportData(file);
			file.close();
		}
		else {
			::widgets::MessageBox::critical(this, tr("Błąd zapisu"),
				tr("Nie można zapisać pliku w wybranej lokalizacji!"
				"Prawdopodobnie nie masz uprawnień do zapisu w tej lokalizacji! (Brak dostępu)"));
		}
	}
}

void ElementsWidget::importFromFile() {
	if (mElements->items().size() > 0) {
		int ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Czy przed wykonaniem importu chcesz usunąć elementy z listy?"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::Yes)
			mElements->clear();
	}

	QString selectedFilter;
	QStringList filenames = QFileDialog::getOpenFileNames(this, tr("Import danych z pliku"), QDir::currentPath(),
		"Plik XML (*.xml);;Plik CSV (*.csv)", &selectedFilter);

	if (filenames.size() == 0)
		return;

	int count {0};
	for (QString& filename : filenames) {
		std::unique_ptr<core::abstracts::AbstractExporter<core::order::Element>> importer;
		if (filename.toUpper().endsWith("XML")) {
			importer = std::make_unique<eSawmill::orders::exporters::ElementXmlExporter>();
		} else if (filename.toUpper().endsWith("CSV")) {
			importer = std::make_unique<eSawmill::orders::exporters::ElementCsvExporter>();
		} else continue;

		if (QFile file {filename}; file.open(QFile::ReadOnly | QFile::Text)) {
			importer->importData(file);
			mElements->insert(importer->data());
			file.close();
		}
		++count;
	}
	if (count > 0) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Zakończono import %1 pliku/ów").arg(count));
	} else {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie zaimportowano danych z pliku!\n"
			   "Sprawdź format oraz poprawność importowanych danych, a następnie spróbuj ponownie"),
			tr("Prawdopodobnie nie rozpoznano rozszeżenia pliku, lub struktura danych w pliku jest nieprawidłowa"));
	}
}

void ElementsWidget::markCheckedAsPlannedHandle() {
	QModelIndexList selectedRows = mElementsView->selectionModel()->selectedRows(orders::models::Elements::Columns::IsPlanned);

	if (selectedRows.size() <= 0) {
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Nie zaznaczono pozycji na liście. Czy chcesz zastosować tą opcje dla wszystkich elementów"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			if (mElements->items().size() <= 0) {
				::widgets::MessageBox::information(this, tr("Infrmacja"),
					tr("Operacja nie mogła być wykonana ponieważ nie znaleziono żadnych elementów na liście"));
				return;
			}

			// mark all elements as planned
			for (int row = 0; row < mElements->items().size(); row++) {
				mElements->setData(mElements->index(row, orders::models::Elements::Columns::IsPlanned), true, Qt::EditRole);
			}
		}
	}

	// mark only selected items
	for (auto row : selectedRows)
		mElements->setData(row, true, Qt::EditRole);
}

void ElementsWidget::markCheckedAsUnplannedHandle() {
	QModelIndexList selectedRows = mElementsView->selectionModel()->selectedRows(orders::models::Elements::Columns::IsPlanned);

	if (selectedRows.size() <= 0) {
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Nie zaznaczono pozycji na liście. Czy chcesz zastosować tą opcję dla wszystkich elementów?"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			if (mElements->items().size() <= 0) {
				::widgets::MessageBox::information(this, tr("Informacja"),
					tr("Operacja nie mogła być wykonana ponieważ nie znaleziono żadnych elementów na liście"));
				return;
			}

			// mark all items as unplanned
			for (int row = 0; row < mElements->items().size(); row++)
				mElements->setData(mElements->index(row, orders::models::Elements::Columns::IsPlanned), false, Qt::EditRole);
		}
	}

	// mark only selected items as unplanned
	for (auto row : selectedRows)
        mElements->setData(row, false, Qt::EditRole);
}

void ElementsWidget::importDataByDroppingFile(const QStringList& files) {
	for (auto& file : files) {
		std::unique_ptr<core::abstracts::AbstractExporter<core::order::Element>> exporter;
		if (file.endsWith("xml"))
			exporter = std::make_unique<eSawmill::orders::exporters::ElementXmlExporter>();

		if (file.endsWith("csv"))
			exporter = std::make_unique<eSawmill::orders::exporters::ElementCsvExporter>();

		// opening file
		QFile f(file);
		if (!f.open(QFile::ReadOnly | QFile::Text)) {
			::widgets::MessageBox::warning(this, "Import danych z pliku",
										   "Nie można zaimportować danych z wskazanego pliku. Brak dostępu!");
			return;
		}
		if (exporter->importData(f))
			mElements->insert(exporter->data());
		else {
			::widgets::MessageBox::critical(this, "Import danych z pliku",
											"Nie można zaimportować danych z pliku z powodu napodkanych błędów");
		}
		f.close();
	}
}

void ElementsWidget::recalculateAndUpdatePrices() {
	mTotalValueElementsLabel->setText(QString().asprintf("Razem: %0.2f m<sup>3</sup>", mElements->totalCubature()));
	mTotalPlannedElementsLabel->setText(QString().asprintf("Strugane: %0.2f m<sup>3</sup>", mElements->totalPlannedCubature()));
	Q_EMIT dataChanged();
}

} // namespace eSawmill
