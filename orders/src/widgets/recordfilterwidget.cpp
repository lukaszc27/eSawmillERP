#include "widgets/recordfilterwidget.hpp"
#include <QFormLayout>
#include <QHBoxLayout>


namespace eSawmill::orders::widgets {

RecordFilterWidget::RecordFilterWidget(QWidget* parent)
	: QGroupBox(parent)
{
	setCheckable(true);
	setChecked(false);
	setTitle("Filtr zamówień");
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	createAllWidgets();
	createConnections();
}

void RecordFilterWidget::createAllWidgets() {
	mName = new QLineEdit(this);
	mNumber = new QLineEdit(this);
	mUsePriority = new QCheckBox("Priorytet", this);
	mPriority = new QComboBox(this);
	mContracotrName = new QLineEdit(this);
	mContracotrPhone = new QLineEdit(this);
	mCleanButton = new QPushButton("Wyczyść", this);
	mFilterButton = new QPushButton(QIcon(":/icons/find"), "Filtruj", this);

	mPriority->setEnabled(mUsePriority->isChecked());
	mPriority->addItems(QStringList()
						<< "Niski"
						<< "Normalny"
						<< "Wysoki");

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mCleanButton);
	buttonsLayout->addSpacing(3);
	buttonsLayout->addWidget(mFilterButton);

	QFormLayout* left = new QFormLayout;
	left->addRow("Nazwa", mName);
	left->addRow("Numer", mNumber);
	left->addRow(mUsePriority, mPriority);

	QFormLayout* right = new QFormLayout;
	right->addRow("Kontrahent", mContracotrName);
	right->addRow("Telefon", mContracotrPhone);
	right->addItem(buttonsLayout);

	QHBoxLayout* layout = new QHBoxLayout(this);
	layout->addLayout(left);
	layout->addSpacing(12);
	layout->addLayout(right);
}

void RecordFilterWidget::createConnections() {
	connect(mUsePriority, &QCheckBox::stateChanged, mPriority, &QComboBox::setEnabled);
	connect(mFilterButton, &QPushButton::clicked, this, &RecordFilterWidget::filterButtonHandle);
	connect(mCleanButton, &QPushButton::clicked, this, &RecordFilterWidget::cleanButtonHandle);
}

void RecordFilterWidget::filterButtonHandle() {
	struct FilterOptions options;
	options.name = mName->text().toUpper();
	options.number = mNumber->text().toUpper();
	options.priority = mPriority->currentText().toUpper();
	options.usePriority = mUsePriority->isChecked();
	options.contractorName = mContracotrName->text().toUpper();
	options.contractorPhone = mContracotrPhone->text().toUpper();

	emit filterDataChanged(options);
}

void RecordFilterWidget::cleanButtonHandle() {
	QList<QLineEdit*> fields = findChildren<QLineEdit*>();
	for (auto field : fields) {
		field->setText("");
	}

	struct FilterOptions options;
	options.name = "";
	options.number = "";
	options.usePriority = false;
	options.contractorName = "";
	options.contractorPhone = "";

	mUsePriority->setChecked(false);

	emit filterDataChanged(options);
}

} // namespace
