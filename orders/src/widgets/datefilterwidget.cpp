#include "widgets/datefilterwidget.hpp"
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include "models/orders.h"


namespace eSawmill::orders::widgets {

DateFilterWidget::DateFilterWidget(QWidget* parent)
	: QGroupBox(parent)
{
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	setCheckable(true);
	setChecked(false);
	setTitle("Filtruj zakres dat");

	createAllWidgets();
}

void DateFilterWidget::createAllWidgets() {
	mMinDate = new QDateEdit(this);
	mMaxDate = new QDateEdit(this);
	mFilterByAddedDate = new QRadioButton("Data dodania", this);
	mFilterByEndDate = new QRadioButton("Termin realizacji", this);
	mFilterButton = new QPushButton(QIcon(":/icons/find"), "Filtruj", this);

	mFilterByAddedDate->setChecked(true);

	mMinDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mMinDate->setDate(QDate::currentDate());
	mMinDate->setCalendarPopup(true);
	mMaxDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mMaxDate->setDate(QDate::currentDate());
	mMaxDate->setCalendarPopup(true);

	QHBoxLayout* filterByLayout = new QHBoxLayout;
	filterByLayout->addWidget(mFilterByAddedDate);
	filterByLayout->addSpacing(12);
	filterByLayout->addWidget(mFilterByEndDate);

	QHBoxLayout* rangeLayout = new QHBoxLayout;
	rangeLayout->addWidget(new QLabel("Od", this));
	rangeLayout->addWidget(mMinDate);
	rangeLayout->addSpacing(24);
	rangeLayout->addWidget(new QLabel("Do", this));
	rangeLayout->addWidget(mMaxDate);
	rangeLayout->addStretch(1);

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mFilterButton);

	QVBoxLayout* layout = new QVBoxLayout(this);
	layout->addLayout(filterByLayout);
	layout->addSpacing(6);
	layout->addLayout(rangeLayout);
	layout->addSpacing(6);
	layout->addLayout(buttonLayout);

	// create connections signal => slot
	connect(mFilterButton, &QPushButton::clicked, this, &DateFilterWidget::filterButtonHangle);
}

void DateFilterWidget::filterButtonHangle() {
	struct DateFilterOptions options;
	if (mFilterByAddedDate->isChecked())
		options.filterByColumn = eSawmill::orders::models::Orders::Columns::AddDate;
	else options.filterByColumn = eSawmill::orders::models::Orders::Columns::EndDate;

	options.minDate = mMinDate->date();
	options.maxDate = mMaxDate->date();

	emit dateRangeChanged(options);
}

} // namespace
