#include "widgets/orderconfig.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QSpacerItem>
#include <messagebox.h>
#include <settings/appsettings.hpp>


namespace eSawmill::orders::widgets::config {

Orders::Orders(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	setWindowTitle(tr("Zamówienia"));
	setWindowIcon(QIcon(":/icons/orders"));

	createWidgets();

	connect(mRemindCheck, QOverload<int>::of(&QCheckBox::stateChanged), this, [&](int state){
		mDaysToRemind->setEnabled(state);
		mRemindColor->setEnabled(state);
	});

	connect(mDeadlineCheck, QOverload<int>::of(&QCheckBox::stateChanged), this, [&](int state){
		mDeadlineColor->setEnabled(state);
	});
}

void Orders::createWidgets()
{
	mPrefix = new QLineEdit(this);
	mRemindCheck = new QCheckBox(tr("Wyróżnij zamówienia o zbliżającym się terminie realizacji"), this);
	mDeadlineCheck = new QCheckBox(tr("Wyróżnij zamówienia po terminie realizacji"), this);
	mDaysToRemind = new QSpinBox(this);
	mRemindColor = new ::widgets::ColorSelector(this, QColor(Qt::darkYellow));
	mDeadlineColor = new ::widgets::ColorSelector(this, QColor(Qt::darkRed));
	mColorPrority = new QCheckBox(tr("Wyróżnaj zamówienia na liście pod względem przydzielonego priotytetu"), this);

	mPrefix->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDaysToRemind->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDaysToRemind->setMinimumWidth(75);

	QFormLayout* layout = new QFormLayout(this);
	layout->addRow(tr("Prefix numerowania zamówień"), mPrefix);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mRemindCheck);
	layout->addRow(tr("Ilość dni na ostrzeganie przed upływem terminu realizacji"), mDaysToRemind);
	layout->addRow(tr("Kolor wyrużnienia"), mRemindColor);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mDeadlineCheck);
	layout->addRow(tr("Kolor oznakowania zamówień o przekroczonym terminie"), mDeadlineColor);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mColorPrority);
}

void Orders::save()
{
	core::settings::OrderSettings& settings = core::settings::AppSettings::order();
	settings.setDaysToWarning(mDaysToRemind->value());
	settings.setDeadlineAfterRemind(mDeadlineCheck->isChecked());
	settings.setDeadlineBeforeRemind(mRemindCheck->isChecked());
	settings.setDeadlineAfterRemindColor(mDeadlineColor->color());
	settings.setDeadlineBefoteRemindColor(mRemindColor->color());
	settings.setMarkByPriority(mColorPrority->isChecked());
	settings.setDocumentPrefix(mPrefix->text().toUpper());
}

void Orders::initialize()
{
	const core::settings::OrderSettings& settings = core::settings::AppSettings::order();
	mDaysToRemind->setValue(settings.daysToWarning());
	mDeadlineColor->setCurrentColor(settings.deadlineAfterRemindColor());
	mRemindColor->setCurrentColor(settings.deadlineBeforeRemindColor());
	mDeadlineCheck->setChecked(settings.deadlineAfterRemind());
	mRemindCheck->setChecked(settings.deadlineBeforeRemind());
	mColorPrority->setChecked(settings.markByPriority());
	mPrefix->setText(settings.documentPrefix());
}

} // namespace eSawmill
