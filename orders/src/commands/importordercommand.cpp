#include "commands/importordercommand.hpp"
#include "exporters/orderxmlexporter.hpp"
#include <exceptions/forbiddenexception.hpp>
#include <QFileDialog>
#include <QFile>


namespace eSawmill::orders::commands {

ImportOrderCommand::ImportOrderCommand(const QString& filename, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _fileName {filename}
{
}

bool ImportOrderCommand::execute(core::repository::AbstractRepository& repository) {
	if (_fileName.isEmpty())
		return false;

	{ // file operation block
		QFile file(_fileName);
		if (!file.open(QFile::ReadOnly | QFile::Text))
			return false;

		eSawmill::orders::exporters::OrderXmlExporter importer {};
		importer.importData(file);
		_importedOrders = importer.data();
	}
	if (_importedOrders.empty())
		return false;

	for (auto& order : _importedOrders) {
		repository.orderRepository().create(order);
		Q_ASSERT(order);
	}
	return true;
}

bool ImportOrderCommand::undo(core::repository::AbstractRepository& repository) {
	if (_importedOrders.empty())
		return false;

	bool success {true};
	for (const auto& order : _importedOrders) {
		try {
			Q_ASSERT(order.id() > 0);
			{
				core::repository::ResourceLocker locker(order.id(), repository.orderRepository());
				if (!repository.orderRepository().destroy(order.id())) {
					success = false;
				}
			}
		} catch (core::exceptions::ForbiddenException&) {
			success = false;
			continue;
		}
	} // for loop
	return success;
}

} // namespace eSawmill::orders::commands
