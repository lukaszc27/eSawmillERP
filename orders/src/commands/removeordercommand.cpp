#include "commands/removeordercommand.hpp"
#include <repository/abstractrepositorylocker.hpp>

namespace eSawmill::orders::commands {

RemoveOrderCommand::RemoveOrderCommand(const core::eloquent::DBIDKey& orderId, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _orderId {orderId}
{
}

bool RemoveOrderCommand::execute(core::repository::AbstractRepository& repository) {
	Q_ASSERT(_orderId > 0);
	if (_orderId <= 0)
		return false;

	_removedOrder = repository.orderRepository().getOrder(_orderId);
	return repository.orderRepository().destroy(_orderId);
}

bool RemoveOrderCommand::undo(core::repository::AbstractRepository& repository) {
	Q_ASSERT(_removedOrder.id() > 0);
	if (_removedOrder.id() <= 0)
		return false;
	
	core::repository::ResourceLocker locker(_removedOrder.id(), repository.orderRepository());
	// before recreated order we have to mark the order with all elements as invalid
	_removedOrder.setId(0);
	for (auto& el : _removedOrder.elements())
		el.setId(0);
	for (auto& clip : _removedOrder.articles())
		clip.setId(0);

	return repository.orderRepository().create(_removedOrder);
}

} // namespace eSawmill::orders::commands
