#include "commands/editordercommand.hpp"
#include "repository/abstractrepositorylocker.hpp"

namespace eSawmill::orders::commands {

EditOrderCommand::EditOrderCommand(const core::Order& order, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _editedOrder {order}
{
}

bool EditOrderCommand::execute(core::repository::AbstractRepository& repository) {
	Q_ASSERT(_editedOrder.id() > 0);
	if (_editedOrder.id() <= 0)
		return false;

	// save copy order object before editing
	_previousOrder = repository.orderRepository().getOrder(_editedOrder.id());
	_previousOrder.makeDirty();
	// and in next step update order in repository
	return repository.orderRepository().update(_editedOrder);
}

bool EditOrderCommand::undo(core::repository::AbstractRepository& repository) {
	core::repository::ResourceLocker locker(_previousOrder.id(), repository.orderRepository());
	return repository.orderRepository().update(_previousOrder);
}

} // namespace eSawmill::orders::commands
