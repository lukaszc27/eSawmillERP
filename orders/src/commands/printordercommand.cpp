#include "commands/printordercommand.hpp"
#include <messagebox.h>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <documents/orderdocument.h>
#include <globaldata.hpp>
#include <repository/abstractrepository.hpp>

namespace eSawmill::orders::commands {

PrintOrderCommand::PrintOrderCommand(const core::eloquent::DBIDKey& orderId, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
{
	Q_ASSERT(orderId > 0);
	_order = core::GlobalData::instance()
			 .repository()
			 .orderRepository()
			 .getOrder(orderId);
}

PrintOrderCommand::PrintOrderCommand(const core::Order& order, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _order {order}
{
}

bool PrintOrderCommand::execute(core::repository::AbstractRepository& repository) {
	if (_order.id() <= 0)
		return false;

	// lock order on print time
	core::repository::ResourceLocker locker(_order.id(), repository.orderRepository());

	const auto& user = repository.userRepository().autorizeUser();
	QPointer<core::documents::OrderDocument> doc(new core::documents::OrderDocument(user.company(), _order, parent()));
	QString documentTitle = QObject::tr("ZAMÓWIENIE");
	if (!_order.number().isEmpty())
		documentTitle += QObject::tr(" NR %1").arg(_order.number());

	doc->setDocumentTitle(documentTitle);
	doc->setDocumentSubtitle(_order.name());

	if (!doc->generate())
		return false;

	QPrinter* printer = new QPrinter(QPrinter::HighResolution);
	printer->setCreator(qApp->applicationName());

	if (_preview)
	{
		QScopedPointer<QPrintPreviewDialog> previewDialog(new QPrintPreviewDialog(printer, parent()));
		QObject::connect(previewDialog.get(), &QPrintPreviewDialog::paintRequested,
			[&doc](QPrinter* printer){
				doc->print(printer);
			});

		previewDialog->setWindowTitle(QObject::tr("Wydruk zamówienia"));
		previewDialog->exec();
	}
	else
	{
		QPrintDialog printDialog(printer, parent());
		QObject::connect(&printDialog,
			QOverload<QPrinter*>::of(&QPrintDialog::accepted),
			[&doc](QPrinter* printer){ doc->print(printer); });

		printDialog.exec();
	}

	return true;
}

}
