#include "commands/exportordercommand.hpp"
#include "exporters/orderxmlexporter.hpp"


namespace eSawmill::orders::commands {

ExportOrderCommand::ExportOrderCommand(
		const QString& filename,
		const std::vector<core::eloquent::DBIDKey>& ordersIds,
		QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _ordersIds {ordersIds}
	, _fileName {filename}
{
}

bool ExportOrderCommand::execute(core::repository::AbstractRepository& repository) {
	if (_ordersIds.empty() || _fileName.isEmpty())
		return false;

	QList<core::Order> orders;
	for (const auto& orderId : _ordersIds) {
		core::repository::ResourceLocker locker(orderId, repository.orderRepository());
		orders.append(repository.orderRepository().getOrder(orderId));
	}

	QFile file(_fileName);
	if (!file.open(QFile::WriteOnly | QFile::Text))
		return false;

	eSawmill::orders::exporters::OrderXmlExporter exporter {};
	exporter.setData(orders);
	return exporter.exportData(file);
}

} // namespace eSawmill::orders::commands
