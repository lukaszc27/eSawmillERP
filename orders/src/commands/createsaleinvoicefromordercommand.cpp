#include "commands/createsaleinvoicefromordercommand.hpp"
#include <dbo/saleinvoice.hpp>
#include <dbo/saleinvoiceitem.hpp>
#include <settings/ordersettings.hpp>
#include <memory>

namespace eSawmill::orders::commands {

CreateSaleInvoiceFromOrderCommand::CreateSaleInvoiceFromOrderCommand(const core::Order& order, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _order {order}
	, _paymentName {""}
	, _paymentDeadlineDate {QDate::currentDate()}
	, _paymentDeadlineDays {0}
{
}

bool CreateSaleInvoiceFromOrderCommand::execute(core::repository::AbstractRepository& repository) {
	double totalPrice {0};
	QList<core::SaleInvoiceItem> items;

	core::repository::ResourceLocker locker(_order.id(), repository.orderRepository());
	for (const auto& el : _order.elements()) {
		double price = el.stere(_order.spareLength() / 100.0f) * _order.price();
		if (el.planned())
			price += el.stere(_order.spareLength() / 100.0f) * core::settings::OrderSettings::instance().defaultPlannedPrice();
		double value = price * el.quantity();

		core::adapter::OrderElementSaleInvoiceItem item(el, _order.spareLength());
		item.setPrice(price);
		item.setValue(value);

		items.append(item);
		totalPrice += price;
	}
	for (const auto& article : repository.orderRepository().getArticlesForOrder(_order.id())) {
		double value = article.quantity() * article.priceSaleBrutto();
		core::adapter::SaleInvoiceItemFromArticle articleItem(article);
		articleItem.setValue(value);

		items.append(articleItem);
		totalPrice += value;
	}
	double nettoPrice = totalPrice * ((100.f + _order.rebate()) / 100.f);

	core::adapter::OrderSaleInvoice invoice(_order);
	invoice.setPayment(_paymentName);
	invoice.setPaymentDeadlineDate(_paymentDeadlineDate);
	invoice.setPaymentDeadlineDays(_paymentDeadlineDays);
	invoice.setWarehouse(repository.warehousesRepository().getWarehouse(_warehouseId));
	invoice.setNettoPrice(nettoPrice);
	invoice.setTotalPrice(nettoPrice);
	invoice.setToPaidPrice(nettoPrice);
	invoice.setItems(items);

	return repository.invoiceRepository().createSaleInvoice(invoice);
}

} // namespace eSawmill::orders::commands
