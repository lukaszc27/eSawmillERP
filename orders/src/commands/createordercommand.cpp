#include "commands/createordercommand.hpp"
#include "repository/abstractrepositorylocker.hpp"


namespace eSawmill::orders::commands {

CreateOrderCommand::CreateOrderCommand(const core::Order& order, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _order {order}
{
}

bool CreateOrderCommand::execute(core::repository::AbstractRepository& repository) {
	// now only added new order into current repository
	return repository.orderRepository().create(_order);
}

bool CreateOrderCommand::undo(core::repository::AbstractRepository& repository) {
	Q_ASSERT(_order.id() > 0);
	if (_order.id() <= 0)
		return false;

	core::repository::ResourceLocker locker(_order.id(), repository.orderRepository());
	return repository.orderRepository().destroy(_order.id());
}

} // namespace eSawmill::orders::commands
