#include "createorder.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QDate>
#include <QFileDialog>
#include <QFile>
#include <QMenu>
#include <QInputDialog>
#include <QPrinter>
#include <QPrintDialog>
#include <QDomDocument>
#include <QDomElement>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QPrintPreviewDialog>
#include <QCloseEvent>
#include <QPageLayout>
#include <QtSql>
#include <QPointer>
#include <memory>

#include <dbo/auth.h>
#include <models/tax.h>
#include <messagebox.h>
#include <dbo/orderarticle.h>
#include <documents/orderdocument.h>
#include <licence.hpp>
#include "models/orderarticles.hpp"
#include "settings/appsettings.hpp"
#include <globaldata.hpp>
#include "commands/printordercommand.hpp"
#include <commandexecutor.hpp>

namespace eSawmill::orders {

///
/// \brief CreateOrder::CreateOrder konstruktor importujący zamówienie z pliku
/// i wprowadzający dane do dialogu
/// \param doc - dokument XML z którego dane są odczytywane
/// \param parent - wskaźnik na obiekt rodzica
/// \param id - id zamówienia do wczytania (ignorowany w momęcie wczytywania z pliku)
///
CreateOrder::CreateOrder(core::Order& order, QWidget *parent)
	: QDialog {parent}
	, _order {order}
{
	setWindowTitle(tr("Tworzenie zamówienia"));
	createWidgets();
	createValidators();
	createModels();
	createConnections();

	// stylowanie CSS elementów
	setStyleSheet("#TotalValueLabel, #TotalValueElementsLabel {"
				  "font-size: 14px;"
				  "font-weight: bold;}");

	if (order.id() == 0) {
		// select max end date to realise order
		// and set endDate widget
		QDate firstAvaiableDeadlineDate = core::GlobalData::instance().repository().orderRepository().getFirstAvaiableDeadline();
		const QDate currentDate = QDate::currentDate();
		if (firstAvaiableDeadlineDate < currentDate)
			firstAvaiableDeadlineDate = currentDate;

		mEndDate->setDate(firstAvaiableDeadlineDate.addDays(core::settings::AppSettings::instance().order().minimumRealisedTime()));
		mPrice->setValue(core::settings::AppSettings::order().defaultPrice());
	} else {
		// load order for editing
		load();
		setWindowTitle(tr("Aktualizacja zamówienia [%1]").arg(_order.number()));
		mAcceptButton->setText(_order.isRealised() ? tr("OK") : tr("Zatwierdź"));
		mAddDate->setReadOnly(true);

		mRealised->setChecked(_order.isRealised());
		setReadOnly(_order.isRealised());
		recalculate();
	}
}

void CreateOrder::load() {
	mName->setText(_order.name());
	mContractorSelector->selectContractor(_order.contractor());
	mAddDate->setDate(_order.addDate());
	mEndDate->setDate(_order.endDate());
	mPrioritySelector->setCurrentIndex(_order.priority());
	mTax->setCurrentText(QString::number(_order.Tax().value()));
	mRebate->setValue(_order.rebate());
	mDescription->setPlainText(_order.description());
	mElementWidget->setSpareLength(_order.spareLength());
	mPrice->setValue(_order.price());
}

void CreateOrder::exchange() {
	_order.setName(mName->text());
	_order.setContractor(mContractorSelector->contractor());
	if (_order.addDate().isNull())
		_order.setAddDate(mAddDate->date());
	_order.setEndDate(mEndDate->date());
	_order.setPriority(mPrioritySelector->currentIndex());
	_order.setRealised(mRealised->isChecked());
	_order.setDescription(mDescription->toPlainText());
	_order.setPrice(mPrice->value());
	_order.setSpareLength(mElementWidget->spareLength());
	_order.setElements(mElementWidget->items());
	_order.setArticles(mArticleWidget->items());
	_order.setTax(core::GlobalData::instance()
		.repository()
		.TaxRepository()
		.get(mTax->currentData(articles::models::Tax::Id).toInt())
	);
	{ // to remove after refactoring time when columns UserAddId, CompanyId will be removed from db
		const auto& user = core::GlobalData::instance().repository().userRepository().autorizeUser();
		_order.setUser(user);
		_order.setCompany(user.company());
	}
}

void CreateOrder::setReadOnly(bool readOnly) {
	mName->setReadOnly(readOnly);
	mAddDate->setReadOnly(readOnly);
	mEndDate->setReadOnly(readOnly);
	mPrioritySelector->setDisabled(readOnly);
	mPrice->setReadOnly(readOnly);
	mRebate->setReadOnly(readOnly);
	mTax->setDisabled(readOnly);
	mDescription->setReadOnly(readOnly);
	mRealised->setDisabled(readOnly);
	mContractorSelector->setReadOnly(readOnly);
	mAcceptButton->setDisabled(readOnly);
	mElementWidget->setReadOnly(readOnly);
	mArticleWidget->setReadOnly(readOnly);
}

void CreateOrder::accept() {
	if (mRealised->isChecked()) {
		int result = ::widgets::MessageBox::question(this, "Pytanie",
			"Czy na pewno chcesz oznaczyć to zamówienie jako zrealizowane?\n"
			"Operacja ta nie może zostać cofnięta, a po zrealizowaniu zamównienia\n"
			"nie jest możliwa jego późniejsza edycja", "",
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::No)
			return;
	}
	if(!mContractorSelector->contractor()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
				tr("Aby można było dodać zamówienie musisz wybrać kontrahenta!"));
		return;
	}

	if (checkOrderDates())
		return;

	// validate order name
	if (mName->text().isEmpty()) {
		auto result = ::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie podałeś nazwy określającej zamówienie, czy chcesz zapisać zamówienie bez nazwy?"), "",
			QMessageBox::Yes | QMessageBox::No);
		if (result == QMessageBox::No)
			return;	// user schould set name field
	}

	exchange();
	return QDialog::accept();
}

void CreateOrder::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateOrder::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateOrder::reject);
	connect(mArticleWidget, &eSawmill::articles::widgets::ArticleWidget::articlesChanged,
			mElementWidget, &orders::widgets::ElementsWidget::dataChanged);
	connect(mElementWidget, &orders::widgets::ElementsWidget::dataChanged, this, &CreateOrder::recalculate);
	connect(mArticleWidget, &eSawmill::articles::widgets::ArticleWidget::articlesChanged, this, &CreateOrder::recalculate);

	// ponowne przeliczenie wartości po zmianie ceny
	connect(mPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double val){
		mElementWidget->setPrice(val);
		recalculate();
	});

	// po zmianie wartości rabatu przeliczamy ponownie wszystkie wartości
	connect(mRebate, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double val){
		mElementWidget->setRebate(val);
		recalculate();
	});

	// zmiana stawi Tax
	connect(mTax, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [&](int index){
		Q_UNUSED(index);
		const auto& tax = core::GlobalData::instance()
			.repository()
			.TaxRepository()
			.get(mTax->currentData(articles::models::Tax::Id).toInt());
		mElementWidget->setTax(tax.value());
		recalculate();
	});
	connect(mAddDate, &QDateEdit::userDateChanged, this, [&](const QDate& date){
		Q_UNUSED(date);
		checkOrderDates();
	});
	connect(mEndDate, &QDateEdit::userDateChanged, this, [&](const QDate& date){
		Q_UNUSED(date);
		checkOrderDates();
	});
	connect(mAddDate, &QDateEdit::userDateChanged, this, [&](const QDate& date){
		mEndDate->setMinimumDate(date);
	});
}

void CreateOrder::createValidators()
{
	QRegularExpressionValidator* nameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9- ]+"), this);
	mName->setValidator(nameValidator);
}

void CreateOrder::createWidgets() {
	mTabWidget = new QTabWidget(this);
	mAcceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
	mTotalValueLabel = new QLabel(this);
	mElementWidget = new orders::widgets::ElementsWidget(new orders::models::Elements(_order, this), this);
	mPrintButton = new ::widgets::PushButton(tr("Drukuj"), QKeySequence("CTRL+P"), this, QIcon(":/icons/print"));
	mArticleWidget = new articles::widgets::ArticleWidget(new orders::models::Articles(_order.id(), this), this);

	mTotalValueLabel->setObjectName("TotalValueLabel");

	mTabWidget->addTab(generalWidget(), tr("&Ogólne"));
	mTabWidget->addTab(mElementWidget, tr("&Elementy"));
	mTabWidget->addTab(mArticleWidget, tr("A&rtykuły"));

	QMenu* printMenu = new QMenu(this);
	printMenu->addAction(tr("Drukuj"),
		[this] { printButton_clicked(false); }, QKeySequence("CTRL+P"));
	printMenu->addAction(tr("Podgląd wydruku"),
		[this] { printButton_clicked(true); }, QKeySequence("SHIFT+CTRL+P"));
	mPrintButton->setMenu(printMenu);
	mPrintButton->setPlaceholderText("Ctrl+P");

#ifndef DEBUG
	core::Licence licence;
	if (!licence.isArticleModuleActive())
		mArticleWidget->setEnabled(false);
#endif

	QHBoxLayout* dialogButtonsLayout = new QHBoxLayout;
	dialogButtonsLayout->addWidget(mTotalValueLabel);
	dialogButtonsLayout->addStretch(2);
	dialogButtonsLayout->addWidget(mPrintButton);
	dialogButtonsLayout->addStretch(1);
	dialogButtonsLayout->addWidget(mAcceptButton);
	dialogButtonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTabWidget);
	mainLayout->addLayout(dialogButtonsLayout);
}

void CreateOrder::createModels()
{
	mTax->setModel(new articles::models::Tax(this));
	mPrioritySelector->addItems(QStringList() << "Niski" << "Normalny" << "Wysoki");
}

QWidget* CreateOrder::generalWidget()
{
	QWidget* parent = new QWidget(this);
	mName = new QLineEdit(parent);
	mContractorSelector = new eSawmill::contractors::widgets::ContractorSelector(parent);
	mAddDate = new QDateEdit(parent);
	mEndDate = new QDateEdit(parent);
	mPrioritySelector = new QComboBox(parent);
	mPrice = new QDoubleSpinBox(parent);
	mRebate = new QDoubleSpinBox(parent);
	mRealised = new QCheckBox("Zrealizowane", parent);
	mTax = new QComboBox(parent);
	mDescription = new QPlainTextEdit(parent);

	mPrice->setMaximum(10000);
	mRebate->setMaximum(100);
	mAddDate->setCalendarPopup(true);
	mEndDate->setCalendarPopup(true);
	mAddDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mEndDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPrioritySelector->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mAddDate->setDate(QDate::currentDate());
	mEndDate->setDate(QDate::currentDate());
	mAddDate->setMinimumDate(QDate::currentDate());
	mEndDate->setMinimumDate(mAddDate->date());

	QFormLayout* left = new QFormLayout;
	left->addRow("Nazwa", mName);
	left->addRow("Kontrahent", mContractorSelector);
	left->addRow("Data dodania", mAddDate);
	left->addRow("Termin realizacji", mEndDate);
	left->addRow("Priorytet", mPrioritySelector);

	QLabel* priceUnitLabel = new QLabel("PLN", parent);
	priceUnitLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QLabel* rebateUnit = new QLabel("%", parent);
	rebateUnit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QLabel* TaxUnit = new QLabel("%", parent);
	TaxUnit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* priceLayout = new QHBoxLayout;
	priceLayout->addWidget(mPrice);
	priceLayout->addWidget(priceUnitLabel);

	QHBoxLayout* rebateLayout = new QHBoxLayout;
	rebateLayout->addWidget(mRebate);
	rebateLayout->addWidget(rebateUnit);

	QHBoxLayout* TaxLayout = new QHBoxLayout;
	TaxLayout->addWidget(mTax);
	TaxLayout->addWidget(TaxUnit);

	QFormLayout* right = new QFormLayout;
	right->addRow("Cena", priceLayout);
	right->addRow("Rabat", rebateLayout);
	right->addRow("VAT", TaxLayout);
	right->addWidget(mRealised);

	QHBoxLayout* top = new QHBoxLayout;
	top->addLayout(left);
	top->addSpacing(6);
	top->addLayout(right);

	QVBoxLayout* main = new QVBoxLayout(parent);
	main->addLayout(top);
	main->addWidget(mDescription);

	return parent;
}

void CreateOrder::mouseDoubleClickEvent(QMouseEvent* event) {
	Q_UNUSED(event);
#ifdef _DEBUG
	::widgets::MessageBox::information(this, tr("Debug information"),
		QString("Id: %1").arg(_order.id()));
#endif
}

void CreateOrder::recalculate() {
	double total = mElementWidget->totalPrice() + mArticleWidget->totalPrice();

	mTotalValueLabel->setText(QString().asprintf("Wartość: %0.2f PLN", total));
	mTotalValueLabel->setVisible(total > 0);
}

void CreateOrder::printButton_clicked(bool preview) {
	exchange();
	std::unique_ptr<eSawmill::orders::commands::PrintOrderCommand> command
			= std::make_unique<eSawmill::orders::commands::PrintOrderCommand>(_order, this);
	command->setPreviewMode(preview);

	if (!core::GlobalData::instance().commandExecutor().execute(std::move(command)))
	{
		::widgets::MessageBox::critical(this, tr("Błąd wydruku"),
			tr("Błąd podczas generowania dokuemtnu do druku"));
	}
}

bool CreateOrder::checkOrderDates()
{
	if (mEndDate->date() < mAddDate->date()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Data terminu realizacji jest wcześniejsza niż data dodania zamówienia"));
		return true;
	}
	return false;
}

} // namespace eSawmill
