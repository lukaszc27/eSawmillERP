USE SAWMILL_TEST
--
-- Tabela przechowująca faktury zakupu
--
CREATE TABLE [PurchaseInvoices] (
    [Id]                    BIGINT PRIMARY KEY IDENTITY(1, 1),  -- indetyfikator
    [Number]                VARCHAR(32) NOT NULL,               -- kolejny numer faktury
    [ForeignNumber]         VARCHAR(32) NOT NULL,               -- numer obcy
    [DateOfReceipt]         SMALLDATETIME,                      -- data wpływu
    [ContractorId]          BIGINT,                             -- kontraktor
    [WarehouseId]           BIGINT,                             -- magazyn
    [DateOfIssue]           SMALLDATETIME DEFAULT GetDate(),    -- data wystawienia
    [DateOfPurchase]        SMALLDATETIME DEFAULT GetDate(),    -- data zakupu
    [Rebate]                FLOAT,                              -- rabat
    [Payment]               nVARCHAR(64),                       -- rodzaj płatności (gotówka/przelew)
    [PaymentDeadlineDays]   TINYINT,                            -- ilość dni na płatność
    [PaymentDeadlineDate]   SMALLDATETIME,                      -- termin płatności
    [NettoPrice]            FLOAT,                              -- wartość NETTO
    [TotalPrice]            FLOAT,                              -- wartość całkowita
    [PaidPrice]             FLOAT,                              -- zapłacona kwota
    [ToPaidPrice]           FLOAT,                              -- pozostająca część do zapłaty
    [Buffer]                BIT DEFAULT 1,                      -- czy dokument znajduje się w buforze?
    [WasPaid]               BIT DEFAULT 0                       -- czy dokument został opłacony?

    FOREIGN KEY(WarehouseId) REFERENCES Warehouses(Id) ON DELETE SET NULL,
    FOREIGN KEY(ContractorId) REFERENCES Contractors(Id) ON DELETE CASCADE,
);
GO

--
-- elementy na dokumęcie zakupu
--
CREATE TABLE [PurchaseInvoiceItems] (
    [Id]                    BIGINT PRIMARY KEY IDENTITY(1, 1),  -- indetyfikator
    [Name]                  nVARCHAR(255) NOT NULL,             -- nazwa artykułu/pozycji
    [Quantity]              FLOAT,                              -- ilość na dokumęcie
    [MeasureUnit]           nVARCHAR(16) DEFAULT 'SZT',         -- jednostka miary (dla quantity)
    [Rebate]                FLOAT DEFAULT 0,                    -- rabat
    [Price]                 FLOAT,                              -- cena
    [Value]                 FLOAT,                              -- wartość (ilość * cena)
    [Warehouse]             nVARCHAR(128),                      -- magazyn
    [InvoiceId]             BIGINT                              -- indetyfikator faktury zakupu do której należy obiekt

    FOREIGN KEY (InvoiceId) REFERENCES PurchaseInvoices(Id) ON DELETE CASCADE
);

--
-- tabela przechowująca informacje o fakturach sprzedarzy
--
CREATE TABLE [SaleInvoices] (
    [Id]                    BIGINT PRIMARY KEY IDENTITY(1, 1),
    [Number]                VARCHAR(32) NOT NULL,
    [ContractorId]          BIGINT,
    [WarehouseId]           BIGINT,
    [DateOfIssue]           SMALLDATETIME DEFAULT GetDate(),
    [DateOfSale]            SMALLDATETIME DEFAULT GEtDate(),
    [Rebate]                FLOAT DEFAULT 0,
    [Payment]               nVARCHAR(64),
    [PaymentDeadlineDays]   TINYINT,
    [PaymentDeadlineDate]   SMALLDATETIME,
    [NettoPrice]            FLOAT,
    [TotalPrice]            FLOAT,
    [PaidPrice]             FLOAT,
    [ToPaidPrice]           FLOAT,
    [Buffer]                BIT DEFAULT 1,
    [WasPaid]               BIT DEFAULT 0,

    FOREIGN KEY(WarehouseId) REFERENCES Warehouses(Id) ON DELETE SET NULL,
    FOREIGN KEY(ContractorId) REFERENCES Contractors(Id) ON DELETE CASCADE
);

--
-- pojedynczy element na fakturze sprzedży
--
CREATE TABLE [SaleInvoiceItems] (
    [Id]                    BIGINT PRIMARY KEY IDENTITY(1, 1),
    [Name]                  nVARCHAR(128) NOT NULL,
    [Quantity]              FLOAT DEFAULT 1,
    [MeasureUnit]           nVARCHAR(16) DEFAULT 'SZT',
    [Price]                 FLOAT,
    [InvoiceId]             BIGINT,
    [Rebate]                FLOAT DEFAULT 0,
    [Value]                 FLOAT DEFAULT 0,
    [Warehouse]             nVARCHAR(128),
    [ArticleId]             BIGINT NULL,

    FOREIGN KEY (InvoiceId) REFERENCES SaleInvoices(Id) ON DELETE CASCADE,
    FOREIGN KEY (ArticleId) REFERENCES Articles(Id) ON DELETE SET NULL
);

ALTER TABLE [PurchaseInvoiceItems] ADD
    [ArticleId] BIGINT NULL

CREATE TABLE [SaleInvoiceCorretions] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [InvoiceId]     BIGINT NOT NULL,    -- ID faktury korygowanej (ta będzie nieaktywna)
    [CorrectionId]  BIGINT NOT NULL,    -- ID korekty
    [CreatedDate]   SMALLDATETIME DEFAULT GetDate(),

    FOREIGN KEY(InvoiceId) REFERENCES SaleInvoices(Id) ON DELETE NO ACTION,
    FOREIGN KEY(CorrectionId) REFERENCES SaleInvoices(Id) ON DELETE NO ACTION, 
)
GO

CREATE TABLE [PurchaseInvoiceCorrections] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [InvoiceId]     BIGINT NOT NULL,    -- ID faktury korygowanej (ta będzie nieaktywna)
    [CorrectionId]  BIGINT NOT NULL,    -- ID korekty
    [CreatedDate]   SMALLDATETIME DEFAULT GetDate()

    FOREIGN KEY (InvoiceId) REFERENCES PurchaseInvoices(Id) ON DELETE NO ACTION,
    FOREIGN KEY (CorrectionId) REFERENCES PurchaseInvoices(Id) ON DELETE NO ACTION
)
GO

CREATE TABLE [OrderInvoices] (
    [Id]            BIGINT  IDENTITY(1, 1) PRIMARY KEY,
    [OrderId]       BIGINT NOT NULL,
    [InvoiceId]     BIGINT NOT NULL,
    [CreatedDate]   SMALLDATETIME DEFAULT GETDATE()
)
GO

CREATE TABLE [OrderSaleInvoices] (
	[Id]			BIGINT IDENTITY(1, 1) PRIMARY KEY,
	[OrderId]		BIGINT NOT NULL,
	[InvoiceId]		BIGINT NOT NULL,
	[CreatedDate]	SMALLDATETIME DEFAULT GETDATE()
)