@ECHO OFF
ECHO Deploy eSawmillERP on Windows platform

set DIR_NAME=windows
set QT_DIR=C:\Qt\6.3.1\msvc2019_64\bin

IF EXIST %DIR_NAME%\ (
    RMDIR /S /Q %DIR_NAME%
)
MKDIR %DIR_NAME%

CALL COPY_FILES
ECHO --- Adding Qt dependences ---
START /B /D %QT_DIR% windeployqt.exe --release app.exe


:COPY_FILES
    ECHO --- Copying files into target directory ---
    :: copy main app file
    IF EXIST app\release\app.exe (
        ECHO Copy executabe file (main app)
        COPY /Y app\release\app.exe %DIR_NAME%
    ) ELSE (
        ECHO Main app file not found!
    )
    :: copy service file
    IF EXIST server\release\server.exe (
        ECHO Copy executable file (server app)
        COPY /Y server\release\server.exe %DIR_NAME%
    ) ELSE (
        ECHO Server app file not found!
    )

    :: copy libraries
    ::----------------------------------------------
    :: copy core library
    IF EXIST core\release\core.dll (
        ECHO Copying CORE library
        COPY /Y core\release\core.dll %DIR_NAME%
    ) ELSE (
        ECHO Library not found!!!
    )

    :: copy widgets library
    IF EXIST widgets\release\widgets.dll (
        ECHO Copying WIDGETS library
        COPY /Y widgets\release\widgets.dll %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )

    :: copy account library
    IF EXIST account\release\account.dll (
        ECHO Copying ACCOUNT library
        COPY /Y account\release\account.dll %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )

    :: copy contractor library
    IF EXIST contractors\release\contractors.dll (
        ECHO Copying CONTRACTORS library
        COPY /Y contractors\release\contractors.dll  %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )

    :: copy articles library
    IF EXIST articles\release\articles.dll (
        ECHO Copying ARTICLES library
        COPY /Y articles\release\articles.dll  %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )

    :: copy orders library
    IF EXIST orders\release\orders.dll (
        ECHO Copying ORDERS library
        COPY /Y orders\release\orders.dll  %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )
    
    :: copy services library
    IF EXIST services\release\services.dll (
        ECHO Copying SERVICES library
        COPY /Y services\release\services.dll  %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )

    :: copy invoices library
    IF EXIST invoices\release\invoices.dll (
        ECHO Copying INVOICES library
        COPY /Y invoices\release\invoices.dll  %DIR_NAME%
    ) ELSE (
        ECHO Library not found!
    )
    EXIT /B