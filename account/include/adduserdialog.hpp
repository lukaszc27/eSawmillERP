#ifndef ADDUSERDIALOG_HPP
#define ADDUSERDIALOG_HPP

#include <QDialog>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QCheckBox>
#include <QTabWidget>
#include "account_global.h"
#include <dbo/user.h>
#include <dbo/contact.h>
#include <pushbutton.h>
#include <contactwidget.hpp>
#include <roleeditwidget.hpp>


namespace eSawmill::account {

class ACCOUNT_EXPORT AddUserDialog : public QDialog {
	Q_OBJECT

public:
	///
	/// \brief The DialogMode enum
	/// dialog mode to open (add new user or edit existing)
	///
	enum class DialogMode {
		Add,
		Edit
	};

	///@{
	/// ctors
	explicit AddUserDialog(core::User& user, QWidget* parent = nullptr, DialogMode mode = DialogMode::Add);
	virtual ~AddUserDialog() = default;
	///@}

public Q_SLOTS:
	virtual void accept() override;

private:
	void createAllWidgets();
	void createValidators();
	void createConnections();

	QLineEdit* mName;
	QLineEdit* mFirstname;
	QLineEdit* mSurname;
	QLineEdit* mPassword;
	QLineEdit* mConfirmPassword;
	QCheckBox* mChangePassword;
	QTabWidget* mTabs;

	// additional widgets
	::widgets::ContactWidget* mContactWidget;
	::widgets::RoleEditWidget* mRoleEditWidget;

	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;

	DialogMode _mode;	///< active dialog mode

	/// @brief user reference to object
	/// where will be stored valid data
	core::User& _user;
};

} // namesapce eSawmill

#endif // ADDUSERDIALOG_HPP
