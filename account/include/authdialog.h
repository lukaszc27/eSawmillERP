#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include "account_global.h"

#include <QDialog>
#include <QTabWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <pushbutton.h>	// moduł widgets
#include <dbo/user.h>		// moduł core, zarządzania użytkownikami
#include <settings/appsettings.hpp>


namespace eSawmill::account {

/**
 * @brief The AuthDialog class
 * Okno autoryzacji użytkownika w programie
 */
class ACCOUNT_EXPORT AuthDialog : public QDialog {
	Q_OBJECT

public:
	AuthDialog(QWidget* parent = nullptr);

	void accept() override;

private:
	/**
	 * @brief createWidgets
	 * tworzy wszystkie potrzebne kontrolki w oknie
	 */
	void createWidgets();
	QWidget* loginPanel();	// zakładka logowania użytkownika

	///
	/// \brief getAllUsers - get all users from database
	/// and put int combobox
	///
	void getAllUsers();

	// Obiekty
	QTabWidget* mTabWidget;
	::widgets::PushButton* mCloseButton;
	::widgets::PushButton* mLoginButton;
	QComboBox* mUserName;
	QLineEdit* mUserPassword;
	QCheckBox* mRememberMe;

private Q_SLOTS:
	///
	/// \brief connectToNewDatabase
	/// \param info
	///
	void connectToNewDatabase(const core::settings::AuthInformation& info);

signals:
	void logged();
};

}

#endif // AUTHDIALOG_H
