#ifndef USERMODEL_HPP
#define USERMODEL_HPP

#include "../account_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/user.h>
#include <eloquent/model.hpp>


namespace eSawmill::account::models {

class ACCOUNT_EXPORT UserModel : public core::models::AbstractModel<core::User> {
	Q_OBJECT

public:
	UserModel(QObject* parent = nullptr);
	~UserModel() {};

	int rowCount(const QModelIndex& index) const override;
	int columnCount(const QModelIndex& index) const override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override { return mColumnName; }

	void createOrUpdate(core::User user) override;
	void get() override;
	void destroy(core::User user) override;

	enum Columns {
		Name,
		FirstName,
		SurName,
		Phone,
		EMail,
		City,
		Street,
		HomeNumber,
		PostCode
	};

	// dodatkowe role wykożystywane w modelu
	enum Roles {
		Id = Qt::UserRole + 1
	};

	double totalPrice() override {
		return 0;
	}

private:
	QStringList mColumnName;
};

} // namespace eSawmill

#endif // USERMODEL_HPP
