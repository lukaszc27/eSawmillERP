#ifndef REMOTECONNECTIONWIDGET_HPP
#define REMOTECONNECTIONWIDGET_HPP

#include "../account_global.h"
#include <QWidget>
#include <QLineEdit>
#include <QSpinBox>
#include <QGroupBox>

namespace eSawmill::account::widgets {

class ACCOUNT_EXPORT RemoteConnectionWidget final : public QWidget {
	Q_OBJECT

public:
	explicit RemoteConnectionWidget(QWidget* parent = nullptr);
	virtual ~RemoteConnectionWidget() = default;

private:
	void createWidgets();
	QGroupBox* createServerAddressGroup();
	QGroupBox* createEventAgentGroup();

	void loadConfiguration();

private:
	// additional controls
	QLineEdit* serverAddr_;
	QLineEdit* eventAgentAddr_;
	QSpinBox* serverPort_;
	QSpinBox* eventAgentPort_;
};

} // namespace eSawmill::account::widgets

#endif // REMOTECONNECTIONWIDGET_HPP
