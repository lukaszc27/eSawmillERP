#ifndef DATABASECONNECTIONWIDGET_HPP
#define DATABASECONNECTIONWIDGET_HPP

#include "../account_global.h"
#include <QWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QPushButton>
#include <settings/appsettings.hpp>


namespace eSawmill::account::widgets {

class ACCOUNT_EXPORT DatabaseConnectionWidget : public QWidget {
	Q_OBJECT

public:
	explicit DatabaseConnectionWidget(QWidget* parent = nullptr);
	virtual ~DatabaseConnectionWidget() = default;

	///
	/// \brief serverAuthorizationInformation
	/// \return
	///
	core::settings::AuthInformation serverAuthorizationInformation() const;

private:
	void createAllWidgets();
	void createLayouts();
	void createConnections();

	// controls used in widget
	QCheckBox* mActiveWidget;
	QCheckBox* mTrustedConnection;
	QCheckBox* mWindowsAuthorization;
	QLineEdit* mDatabaseName;
	QLineEdit* mDriverName;
	QLineEdit* mHostAddress;
	QLineEdit* mUserName;
	QLineEdit* mUserPassword;
	QPushButton* mConnectionCheck;

private Q_SLOTS:
	///
	/// \brief checkDatabaseServerConnection
	///
	void checkDatabaseServerConnection();

Q_SIGNALS:
	///
	/// \brief databaseConnectionParameterChanged
	/// \param authInformation
	///
	void databaseConnectionParameterChanged(const core::settings::AuthInformation& authInformation);
};

} // namespace eSawmill::account::widgets

#endif // DATABASECONNECTIONWIDGET_HPP
