#ifndef USERCONFIG_HPP
#define USERCONFIG_HPP

#include "../account_global.h"
#include "../models/usermodel.hpp"
#include <tableview.h>
#include <configurationwidget.h>
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill::account::widgets {

class ACCOUNT_EXPORT UserConfig : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	explicit UserConfig(QWidget* parent = nullptr);
	virtual ~UserConfig();

	void initialize();
	void save();

public Q_SLOTS:
	void handleRepositoryNotification(const agent::RepositoryEvent& event);

private:
	void createAllWidgets();
	void createAllModels();

	::widgets::TableView* mTableView;
	eSawmill::account::models::UserModel* mUserModel;

private Q_SLOTS:
	/**
	 * @brief addUserHandle
	 */
	void addUserHandle();

	/**
	 * @brief editUserHandle
	 * obsłużenie edycji użytkownika wybranego z listy
	 */
	void editUserHandle();

	/**
	 * @brief removeUserHandle
	 * usuwanie zaznaczonego użytkownika
	 */
	void removeUserHandle();
};

} // namespace eSawmill

#endif // USERCONFIG_HPP
