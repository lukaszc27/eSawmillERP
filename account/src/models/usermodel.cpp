#include "models/usermodel.hpp"
#include <dbo/auth.h>
#include <messagebox.h>
#include <QDebug>
#include <globaldata.hpp>


namespace eSawmill::account::models {

UserModel::UserModel(QObject* parent)
	: core::models::AbstractModel<core::User>(parent)
{
	get();

	mColumnName << tr("Nazwa") << tr("Imię") << tr("Nazwisko") << tr("Telefon") << tr("E-Mail")
				<< tr("Miejscowość") << tr("Ulica") << tr("Nr budynku") << tr("Kod pocztowy");
}

int UserModel::rowCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mList.size();
}

int UserModel::columnCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mColumnName.size();
}

Qt::ItemFlags UserModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = core::models::AbstractModel<core::User>::flags(index);
	flags |= Qt::ItemIsEnabled | Qt::ItemIsSelectable;

	if (!index.column())
		flags |= Qt::ItemIsUserCheckable;

	return flags;
}

QVariant UserModel::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (role == Qt::DisplayRole) {
			return mColumnName.at(section);
		}
		break;

	case Qt::Vertical:
		if (role == Qt::DisplayRole)
			return section + 1;
		break;
	}
	return QVariant();
}

QVariant UserModel::data(const QModelIndex &index, int role) const {
	const int row = index.row();
	const int col = index.column();
	const core::User& currentUser = mList.at(row);
	const core::Contact& contact = currentUser.contact();
	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::Name: return currentUser.name();
		case Columns::FirstName: return currentUser.firstName();
		case Columns::SurName: return currentUser.surName();
		case Columns::Phone: return contact.phone();
		case Columns::EMail: return contact.email();
		case Columns::City: return contact.city();
		case Columns::Street: return contact.street();
		case Columns::HomeNumber: return contact.homeNumber();
		case Columns::PostCode: return contact.postCode();
		}
		break;

	case Qt::FontRole:
		if (currentUser.hasRole(core::Role::Administrator)) {
			auto font = QFont();
			font.setBold(true);
			return font;
		}
		break;

	case Roles::Id:
		return currentUser.id();
	}
	return QVariant();
}

void UserModel::createOrUpdate(core::User user) {
	Q_UNUSED(user);
}

void UserModel::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.userRepository()
			.getAllUsers();
		endResetModel();
	}
	catch (std::exception& ex) {
		core::GlobalData::instance().logger().warning(ex.what());
		throw ex;
	}
}

void UserModel::destroy(core::User user) {
	Q_UNUSED(user);
//	try {
//		// usuwamy kontakt przypisany do użytkownika
//		core::Contact contact;
//		contact.set("Id", user["ContactId"]);
//		contact.destroy();

//		// następnie usuwany jest użytkownik
//		core::eloquent::Model::destroy<core::User>(user);

//		// pobranie nowej listy użytkowników z bazy
//		get();
//	}
//	catch (const QString msg) {
//		::widgets::MessageBox::critical(nullptr, tr("SQL Error"), tr("Podczas usuwania użytkownika wystąpiły błędy"), msg);
//	}
}

} // namespace eSawmill
