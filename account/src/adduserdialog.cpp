#include "adduserdialog.hpp"
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <globaldata.hpp>
#include <messagebox.h>


namespace eSawmill::account {

AddUserDialog::AddUserDialog(core::User& user, QWidget *parent, DialogMode mode)
	: QDialog {parent}
	, _mode {mode}
	, _user {user}
{
	createAllWidgets();
	createValidators();
	createConnections();

	switch (_mode) {
	case DialogMode::Add: {
		setWindowTitle(tr("Dodawanie nowego użytkownika"));
		// when user create new user instance
		// hide option to change password because is not required
		mChangePassword->setVisible(false);
	} break;

	case DialogMode::Edit: {
		setWindowTitle(tr("Edycja użytkownika: %1 %2").arg(_user.firstName(), _user.surName()));

		mChangePassword->setVisible(true);
		mChangePassword->stateChanged(Qt::Unchecked);
	} break;
	}

	// at end load user data into fields
	if (_user) {
		mFirstname->setText(_user.firstName());
		mSurname->setText(_user.surName());
		mName->setText(_user.name());
		mContactWidget->load(_user.contact());
		mRoleEditWidget->assignRoles(_user.roles());
	}
}

void AddUserDialog::accept() {
	if (_mode == DialogMode::Edit
		&& mChangePassword->isChecked()
		&& mPassword->text() != mConfirmPassword->text())
	{
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Wprowadzone hasła nie są takie same!"));
		return;
	}

	_user.setFirstName(mFirstname->text());
	_user.setSurName(mSurname->text());
	_user.setName(mName->text());
	_user.setPassword(mPassword->text());

	// store contact object
	auto& contact = _user.contact();
	contact = mContactWidget->contact();

	// store assigned roles
	_user.assignRole(mRoleEditWidget->assignedRoles());

	if (_mode == DialogMode::Edit && !mChangePassword->isChecked()) {
		_user.removeKey("Password");
	}

	QDialog::accept();
}

void AddUserDialog::createAllWidgets() {
	mFirstname = new QLineEdit(this);
	mSurname = new QLineEdit(this);

	auto topPersonLayout = new QHBoxLayout{};
	topPersonLayout->addWidget(new QLabel(tr("Imię"), this), 0, Qt::AlignLeft);
	topPersonLayout->addSpacing(6);
	topPersonLayout->addWidget(mFirstname, 1);
	topPersonLayout->addSpacing(24);
	topPersonLayout->addWidget(new QLabel(tr("Nazwisko"), this), 0, Qt::AlignLeft);
	topPersonLayout->addSpacing(6);
	topPersonLayout->addWidget(mSurname, 1);

	mName = new QLineEdit(this);
	mPassword = new QLineEdit(this);
	mConfirmPassword = new QLineEdit(this);

	mPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPassword->setEchoMode(QLineEdit::PasswordEchoOnEdit);
	mConfirmPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mConfirmPassword->setEchoMode(QLineEdit::PasswordEchoOnEdit);

	mChangePassword = new QCheckBox(tr("Zmień hasło"), this);
	auto passwordLayout = new QHBoxLayout{};
	passwordLayout->addWidget(mPassword);
	passwordLayout->addWidget(mChangePassword);

	auto systemGroup = new QGroupBox(tr("Dane logowania do systemu eSawmill"), this);
	auto systemGroupLayout = new QFormLayout(systemGroup);
	systemGroupLayout->addRow(tr("Nazwa użytkownika"), mName);
	systemGroupLayout->addRow(tr("Hasło"), passwordLayout);
	systemGroupLayout->addRow(tr("Powtórz hasło"), mConfirmPassword);

	mContactWidget = new ::widgets::ContactWidget(_user.contact(), this);
	mRoleEditWidget = new ::widgets::RoleEditWidget(this);

	mTabs = new QTabWidget(this);
	mTabs->addTab(mContactWidget, tr("&Dane teleadresowe"));
	mTabs->addTab(mRoleEditWidget, tr("&Role"));

	mAcceptButton = new ::widgets::PushButton(tr("Zapisz"), QKeySequence("F10"), this, QIcon(":/icons/accept"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/reject"));

	auto buttonsLayout = new QHBoxLayout{};
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	auto mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(topPersonLayout);
	mainLayout->addSpacing(8);
	mainLayout->addWidget(systemGroup);
	mainLayout->addSpacing(8);
	mainLayout->addWidget(mTabs);
	mainLayout->addSpacing(8);
	mainLayout->addLayout(buttonsLayout);
}

void AddUserDialog::createValidators() {
}

void AddUserDialog::createConnections() {
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &AddUserDialog::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &AddUserDialog::reject);
	connect(mChangePassword, &QCheckBox::stateChanged, this, [this](int state){
		mPassword->setEnabled(state == Qt::Checked);
		mConfirmPassword->setEnabled(state == Qt::Checked);
	});
}

} // namespace eSawmill
