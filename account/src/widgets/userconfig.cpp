#include "widgets/userconfig.hpp"
#include "adduserdialog.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <pushbutton.h>
#include <messagebox.h>
#include <dbo/auth.h>
#include <QDebug>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>


namespace eSawmill::account::widgets {

UserConfig::UserConfig(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	setWindowTitle(tr("Użytkownicy"));
	setWindowIcon(QIcon(":/icons/users"));

	createAllWidgets();
	createAllModels();

	connect(mTableView, &::widgets::TableView::doubleClicked, this, [=](const QModelIndex& index){
		Q_UNUSED(index);
		editUserHandle();
	});
}

UserConfig::~UserConfig() {}

void UserConfig::initialize() {}

void UserConfig::save() {}

void UserConfig::handleRepositoryNotification(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::User) {
		const auto& selection = mTableView->selectionModel()->selection();
		mUserModel->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void UserConfig::createAllWidgets()
{
	mTableView = new ::widgets::TableView(this);
	::widgets::PushButton* addButton = new ::widgets::PushButton(tr("Dodaj użytkownika"), QKeySequence("Ins"), this, QIcon(":icons/add"));
	::widgets::PushButton* removeButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("Del"), this, QIcon(":/icons/trash"));

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(addButton);
	buttonsLayout->addWidget(removeButton);
	buttonsLayout->addStretch(1);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTableView);
	mainLayout->addLayout(buttonsLayout);

	connect(addButton, &::widgets::PushButton::clicked, this, &UserConfig::addUserHandle);
	connect(removeButton, &::widgets::PushButton::clicked, this, &UserConfig::removeUserHandle);
	connect(this, &eSawmill::widgets::ConfigurationWidget::repositoryChanged,
			this, &eSawmill::account::widgets::UserConfig::handleRepositoryNotification);

	// wyłączenie opcji przeznaczonych tylko dla administratora
	const auto& auth_user = core::GlobalData::instance().repository().userRepository().autorizeUser();
	bool enabled = auth_user.hasRole(core::Role::Administrator) && auth_user.hasRole(core::Role::Operator);

	addButton->setEnabled(enabled);
	removeButton->setEnabled(enabled);
}

void UserConfig::createAllModels()
{
	mUserModel = new account::models::UserModel(this);
	mTableView->setModel(mUserModel);
}

void UserConfig::addUserHandle()
{
	auto& repository = core::GlobalData::instance().repository().userRepository();
	if (const auto& auth_user = repository.autorizeUser();
		auth_user.hasRole(core::Role::Administrator) && auth_user.hasRole(core::Role::Operator))
	{
		try {
			core::User user {};
			std::unique_ptr<AddUserDialog> dialog = std::make_unique<AddUserDialog>(user, this);
			if (dialog->exec() == AddUserDialog::Accepted) {
				const auto& authUser = repository.autorizeUser();
				user.setCompany(authUser.company());
				if (!repository.create(user)) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Błąd zapisywania informacji o użytkowniku!"));

					return;
				}
				mUserModel->get();
			}
		} catch (std::invalid_argument&) {
			::widgets::MessageBox::critical(this, tr("Błąd - Debug"),
				tr("Obiekty przeznaczone do zapisu nie zostały prawidłowo utworzone.\n"
				   "Skontaktuj się z dostawcą oprogramowania jeśli błąd się powtarza."));
		} catch (std::exception&) {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Podczas tworzenia nowego użytkownika wystąpiły błędy!\n"
				   "Zmiany nie zostały zapisane!"));
		}
	} else {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie posiadasz wystarczających uprawnień do edycji użytkowników"),
			tr("Aby móc edytować użytkowników konieczne jest posiadanie ról \"Operator\" oraz \"Administrator\""));
	}
}

void UserConfig::editUserHandle() {
	auto& repository = core::GlobalData::instance().repository().userRepository();
	core::eloquent::DBIDKey userId {0};

	// edycja użytkownika możliwa tylko dla administratora
	if (const auto& auth_user = repository.autorizeUser();
		auth_user.hasRole(core::Role::Administrator) && auth_user.hasRole(core::Role::Operator))
	{
		try {
			userId = mTableView->currentIndex().data(eSawmill::account::models::UserModel::Roles::Id).toUInt();
			Q_ASSERT(userId > 0);
			if (!repository.lock(userId))
				return;

			core::User user = repository.getUser(userId);
			std::unique_ptr<AddUserDialog> dialog = std::make_unique<AddUserDialog>(user, this, AddUserDialog::DialogMode::Edit);
			if (dialog->exec() == AddUserDialog::Accepted) {
				try {
					if (!repository.update(user)) {
						::widgets::MessageBox::critical(this, tr("Błąd"),
							tr("Błąd podczas zapisywania informacji o użytkowniku!"));

						return;
					}
					mUserModel->get();	// refresh list
				} catch (std::invalid_argument&) {
					::widgets::MessageBox::critical(this, tr("Błąd - Debug"),
						tr("Obiekty przeznaczone do zapisu nie zostały prawidłowo utworzone.\n"
						   "Skontaktuj się z dostawcą oprogramowania jeśli błąd się powtarza"));
				} catch (std::exception&) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Podczas zapisu zmian wystąpiły błędy!\n"
						   "Zmiany nie zostały zapisane!"));
				}
			}
			if (!repository.unlock(userId))
				return;
		} catch (const core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this,
				tr("Blokada dostępu"),
				tr("Wskazany użytkownik jest edytowany w tym momencie przez inną osobę - brak dostępu"));
		}
	} else {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie posiadasz wystarczających uprawnień aby móc edytować użytkownika"),
			tr("Do edycji użytkownika konieczne jest posiadanie uprawnień: \"Operator\" oraz \"Administrator\""));
	}
}

void UserConfig::removeUserHandle() {
	auto& userRepository = core::GlobalData::instance().repository().userRepository();
	if (const auto& auth_user = userRepository.autorizeUser();
		auth_user.hasRole(core::Role::Administrator) && auth_user.hasRole(core::Role::Operator))
	{
		try {

			if (!mTableView->selectionModel()->hasSelection()) {
				::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
					tr("Nie zaznaczyłeś użytkownika do usunięcia"));
				return;
			}

			core::eloquent::DBIDKey userID = mTableView->currentIndex().data(eSawmill::account::models::UserModel::Roles::Id).toUInt();
			if (!userRepository.lock(userID))
				return;

			int queryResult = ::widgets::MessageBox::question(this, tr("Pytanie"),
				tr("Czy na pewno chcesz usunąć zaznaczonego użytkownika?"), "",
				QMessageBox::Yes | QMessageBox::No);

			if (queryResult == QMessageBox::Yes) {
				userRepository.destroy(userID);
				mUserModel->get();
			}

			if (!userRepository.unlock(userID))
				return;
		} catch (const core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this,
				tr("Blokada dostępu"),
				tr("Dostęp do użytkownika zablokowany ponieważ w tym momencie jest on edytowany przez innego użytkownika systemu."));
		} catch (const std::invalid_argument& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Nie znaleziono zaznaczonego użytkownika"));
			return;
		} catch (...) {
			::widgets::MessageBox::critical(this, tr("Nieznany błąd"),
				tr("Podczas usuwania użytkownika wystąpił nieoczekiwany błąd"));
		}
	} else {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie posiadasz wystarczających uprawień do usunięcia użytkownika."),
			tr("Aby móc usunąć użzytkownika konieczne jest posiadanie ról: \"Operator\" oraz \"Administrator\""));
	}
}

} // namespace eSawmill
