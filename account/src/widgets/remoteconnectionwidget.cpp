#include "widgets/remoteconnectionwidget.hpp"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QSettings>

namespace eSawmill::account::widgets {

RemoteConnectionWidget::RemoteConnectionWidget(QWidget* parent)
	: QWidget {parent}
{
	createWidgets();
	loadConfiguration();
}

void RemoteConnectionWidget::createWidgets() {
	serverAddr_ = new QLineEdit {this};
	serverAddr_->setReadOnly(true);

	serverPort_ = new QSpinBox {this};
	serverPort_->setRange(0, 65'535);
	serverPort_->setReadOnly(true);

	eventAgentAddr_ = new QLineEdit {this};
	eventAgentAddr_->setReadOnly(true);

	eventAgentPort_ = new QSpinBox {this};
	eventAgentPort_->setRange(0, 65'535);
	eventAgentPort_->setReadOnly(true);

	auto mainLayout = new QVBoxLayout {this};
	mainLayout->addWidget(createServerAddressGroup());
	mainLayout->addSpacing(6);
	mainLayout->addWidget(createEventAgentGroup());
	mainLayout->addStretch(1);
}

QGroupBox* RemoteConnectionWidget::createServerAddressGroup() {
	auto grp = new QGroupBox {tr("Adres serwera produkcyjnego"), this};
	auto layout = new QGridLayout(grp);
	layout->addWidget(new QLabel{tr("Adres IP"), this}, 0, 0, Qt::AlignLeft);
	layout->addWidget(new QLabel{tr("Port"), this}, 0, 1, Qt::AlignLeft);
	layout->addWidget(serverAddr_, 1, 0, Qt::AlignLeft);
	layout->addWidget(serverPort_, 1, 1, Qt::AlignLeft);

	return grp;
}

QGroupBox* RemoteConnectionWidget::createEventAgentGroup() {
	auto grp = new QGroupBox{tr("Adres usługi powiadomień"), this};
	auto layout = new QGridLayout{grp};

	layout->addWidget(new QLabel{tr("Adres IP"), this}, 0, 0, Qt::AlignLeft);
	layout->addWidget(new QLabel{tr("Port"), this}, 0, 1, Qt::AlignLeft);
	layout->addWidget(eventAgentAddr_, 1, 0, Qt::AlignLeft);
	layout->addWidget(eventAgentPort_, 1, 1, Qt::AlignLeft);

	return grp;
}

void RemoteConnectionWidget::loadConfiguration() {
	QSettings settings;
	serverAddr_->setText(settings.value("remote/serviceHost").toString());
	serverPort_->setValue(settings.value("remote/servicePort").toUInt());
	eventAgentAddr_->setText(settings.value("remote/eventAgent").toString());
	eventAgentPort_->setValue(settings.value("remote/eventAgentPort").toUInt());
}

} // namespace eSawmill::account::widgets
