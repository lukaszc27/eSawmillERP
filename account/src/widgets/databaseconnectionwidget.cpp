#include "widgets/databaseconnectionwidget.hpp"
#include <QSqlDatabase>
#include <QSqlError>
#include <QLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <settings/appsettings.hpp>
#include <messagebox.h>


namespace eSawmill::account::widgets {

DatabaseConnectionWidget::DatabaseConnectionWidget(QWidget* parent)
	: QWidget {parent}
{
	createAllWidgets();
	createLayouts();
	createConnections();

	const core::settings::AuthInformation ai = core::settings::AppSettings::instance().readDatabaseAuthInformation();
	mDriverName->setText(ai.driver);
	mHostAddress->setText(ai.host);
	mDatabaseName->setText(ai.databaseName);
	mUserName->setText(ai.userName);
	mUserPassword->setText(ai.password);
	mTrustedConnection->setChecked(ai.trustedConnection);

	mActiveWidget->stateChanged(Qt::Unchecked);
	mTrustedConnection->stateChanged(Qt::Unchecked);
}

core::settings::AuthInformation DatabaseConnectionWidget::serverAuthorizationInformation() const {
	core::settings::AuthInformation ai;
	ai.driver = mDriverName->text();
	ai.host = mHostAddress->text();
	ai.databaseName = mDatabaseName->text();
	ai.userName = mUserName->text();
	ai.password = mUserPassword->text();
	ai.trustedConnection = mTrustedConnection->isChecked();
	ai.windowsAuthorization = false;
	return ai;
}

void DatabaseConnectionWidget::createAllWidgets() {
	mActiveWidget		= new QCheckBox(tr("Zmień bazę danych"), this);
	mTrustedConnection	= new QCheckBox(tr("Trusted Connection"), this);
	mDriverName			= new QLineEdit(this);
	mHostAddress		= new QLineEdit(this);
	mDatabaseName		= new QLineEdit(this);
	mUserName			= new QLineEdit(this);
	mUserPassword		= new QLineEdit(this);
	mWindowsAuthorization = new QCheckBox(tr("Autoryzacja Windows"), this);
	mConnectionCheck	= new QPushButton(tr("Sprawdź połączenie"), this);

	mDatabaseName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserPassword->setEchoMode(QLineEdit::Password);
}

void DatabaseConnectionWidget::createLayouts() {
	QFormLayout* formLayout = new QFormLayout {};
	formLayout->addRow(tr("Sterownik"), mDriverName);
	formLayout->addRow(tr("Host"), mHostAddress);
	formLayout->addRow(tr("Baza danych"), mDatabaseName);
	formLayout->addRow(tr("Nazwa użytkownika"), mUserName);
	formLayout->addRow(tr("Hasło"), mUserPassword);
	formLayout->addRow("", mWindowsAuthorization);
	formLayout->addRow("", mTrustedConnection);

	QVBoxLayout* mainLayout = new QVBoxLayout {this};
	mainLayout->addWidget(mActiveWidget);
	mainLayout->addSpacing(8);
	mainLayout->addLayout(formLayout);
	mainLayout->addSpacing(12);
	mainLayout->addWidget(mConnectionCheck);
}

void DatabaseConnectionWidget::createConnections() {
	connect(mActiveWidget, &QCheckBox::stateChanged, this, [&](int state){
		mDriverName->setEnabled(state);
		mHostAddress->setEnabled(state);
		mDatabaseName->setEnabled(state);
		mUserName->setEnabled(state);
		mUserPassword->setEnabled(state);
		mTrustedConnection->setEnabled(state);
		mWindowsAuthorization->setEnabled(state);
		mConnectionCheck->setEnabled(state);
	});
	connect(mWindowsAuthorization, &QCheckBox::stateChanged, this, [&](int state){
		mUserName->setDisabled(state);
		mUserPassword->setDisabled(state);
	});
	connect(mConnectionCheck, &QPushButton::clicked, this, &DatabaseConnectionWidget::checkDatabaseServerConnection);
}

void DatabaseConnectionWidget::checkDatabaseServerConnection() {
	try {
		QSqlDatabase db = QSqlDatabase::addDatabase("QODBC", "checkConnection");
		if (db.isOpen())
			db.close();

		db.setDatabaseName(serverAuthorizationInformation().connectionString());
		if (db.open()) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Nawiązano połączenie z bazą danych"));

			emit databaseConnectionParameterChanged(serverAuthorizationInformation());
		}
		else {
			::widgets::MessageBox::critical(this, tr("Błąd połączenia z bazą SQL"), db.lastError().text());
		}
	}
	catch (std::exception& ex) {
		::widgets::MessageBox::critical(this, tr("Ostrzeżenie"), ex.what());
	}
}

} // namespace eSawmill::account::widgets
