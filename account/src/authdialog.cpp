#include "authdialog.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QStringListModel>
#include <QList>
#include <QMessageBox>
#include <QFile>
#include <QtXml>
#include <QApplication>
#include <QFileDialog>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <dbo/auth.h>			// moduł core
#include <simplecrypt.hpp>
#include <messagebox.h>			// moduł widgets
#include <licence.hpp>
#include <settings/appsettings.hpp>
#include <globaldata.hpp>
#include <repository/database/databaserepository.hpp>
#include <repository/remote/remoterepository.hpp>
#include <exceptions/useralreadyloggedinexception.hpp>
#include <exceptions/deviceunregisteredexception.hpp>
#include "widgets/databaseconnectionwidget.hpp"
#include "widgets/remoteconnectionwidget.hpp"

using namespace widgets;
using namespace core;

namespace eSawmill::account {

AuthDialog::AuthDialog(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle(tr("Autoryzacja"));

	createWidgets();

	// połączenia sygnał -> slot
	connect(mCloseButton, &PushButton::clicked, this, &AuthDialog::close);
	connect(mLoginButton, &::widgets::PushButton::clicked, this, &AuthDialog::accept);

	// pobranie listy wszystkich użytkowników programu i dodanie ich do listy rozwijanej
	getAllUsers();
}

void AuthDialog::createWidgets()
{
	mTabWidget = new QTabWidget(this);
	mCloseButton = new ::widgets::PushButton(tr("Zakończ"), QKeySequence("ESC"), this, QIcon(":/icons/exit"));

	mTabWidget->addTab(loginPanel(), tr("&Logowanie"));
	if (auto repo = dynamic_cast<core::repository::database::DatabaseRepository*>(&core::GlobalData::instance().repository()); repo != nullptr)
	{
		auto widget = new widgets::DatabaseConnectionWidget {this};
		connect(widget, &widgets::DatabaseConnectionWidget::databaseConnectionParameterChanged, this, &AuthDialog::connectToNewDatabase);
		mTabWidget->addTab(widget, tr("&Baza danych"));
	}
	else if (auto repo = dynamic_cast<core::repository::remote::RemoteRepository*>(&core::GlobalData::instance().repository()); repo != nullptr)
	{
		auto widget = new widgets::RemoteConnectionWidget {this};
		mTabWidget->addTab(widget, tr("Serwer"));
	}
	else {
		Q_ASSERT(false);
	}

	QHBoxLayout* dialogButtonLayout = new QHBoxLayout();
	dialogButtonLayout->addStretch(1);
	dialogButtonLayout->addWidget(mCloseButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTabWidget);
	mainLayout->addLayout(dialogButtonLayout);
}

QWidget* AuthDialog::loginPanel()
{
	QWidget* widget = new QWidget;
	mUserName = new QComboBox(widget);
	mUserPassword = new QLineEdit(widget);
	mLoginButton = new ::widgets::PushButton(tr("Zaloguj"), QKeySequence("F10"), widget, QIcon(":/icons/key"));
	mRememberMe = new QCheckBox(tr("Pamiętaj mnie"), widget);

	mUserPassword->setEchoMode(QLineEdit::Password);
	mUserName->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	mUserName->setEditable(true);

	QFormLayout* formLayout = new QFormLayout();
	formLayout->addRow(tr("Użytkownik"), mUserName);
	formLayout->addRow(tr("Hasło"), mUserPassword);
	formLayout->addWidget(mRememberMe);

	QHBoxLayout* buttonLayout = new QHBoxLayout();
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mLoginButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(widget);
	mainLayout->addStretch(1);
	mainLayout->addLayout(formLayout);
	mainLayout->addLayout(buttonLayout);
	mainLayout->addStretch(1);

	return widget;
}

void AuthDialog::getAllUsers() {
	QStringList nicks;
	const QList<core::User> users = GlobalData::instance()
		.repository()
		.userRepository()
		.getAllUsers();

	for (const auto& user : users) {
		if (user.hasRole(core::Role::SystemRole::Operator))
			nicks.append(user.name());
	}
	QStringListModel* stringListModel = new QStringListModel(nicks, this);
	mUserName->setModel(stringListModel);
}

void AuthDialog::connectToNewDatabase(const core::settings::AuthInformation& info) {
	core::settings::AppSettings::instance().writeDatabaseAuthInformation(info);
	QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
	if (db.isOpen())
		db.close();

	db.setDatabaseName(info.connectionString());
	if (!db.open()) {
		::widgets::MessageBox::critical(this, tr("Błąd z połączeniem do bazy danych"), db.lastError().text());
		return;
	}
	getAllUsers();
}

void AuthDialog::accept()
{
	try {
		if (!GlobalData::instance().repository().userRepository().login(mUserName->currentText(), mUserPassword->text())) {
			::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
				tr("Nieprawidłowa nazwa użytkownika lub hasło"), "");
		}
		else {
	#ifndef _DEBUG
			// sprawdzenie czy zalogowana firma posiada taki sam NIP jak NIP podany w licencji
			// wyklucza to możliwość kożystania z jednej licencji przez wiele firm
			core::Licence licence;
			if (licence.customer().NIP() != core::Auth().user().company().NIP()) {
				// w tym przypadku doszło do kradzieży licencji
				::widgets::MessageBox::warning(this, tr("Licencja programu eSawmill"),
											   tr("Aktualnie zalogowana firma posiada inny NIP niż ten na który została wystawiona licencja programu! "
												  "Dalsze kożystanie z programu będzie nie możliwe."));

				int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
															 tr("Czy chcesz wczytać teraz prawidłowy plik licencji?"), "",
															 QMessageBox::Yes | QMessageBox::No);

				if (result == QMessageBox::Yes) {
					const QString filename = QFileDialog::getOpenFileName(this, tr("Wczytywanie pliku licencji"), QDir::currentPath(), "*.dat");
					if (!filename.isEmpty()) {
						QSettings settings;
						settings.setValue("app/licence", filename);

						::widgets::MessageBox::information(this, tr("Informacja"), tr("Aby wczytać ponownie licencję, musisz ponownie uruchomić aplikację"));
					}
				}

				// w przypaku naruszenia licencji zamknięcie aplikacji
				QTimer::singleShot(100, qApp, QApplication::quit);
			}
	#endif
			core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);

			const bool remember = mRememberMe->isChecked();
			QSettings settings;
			settings.setValue("profile/rememberme", remember);
			settings.setValue("profile/username", remember ? mUserName->currentText() : "");
			settings.setValue("profile/userpass", remember ? crypt.encryptToString(mUserPassword->text()) : "");

			Q_EMIT logged();
			QDialog::accept();
		}
	} catch (const core::exceptions::UserAlreadyLoggedinException& ex) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
			tr("Użytkownik ten jest już zalogowany na innym stanowisku (%1)").arg(ex.hostName()),
			tr("Nie jest możliwa praca jednego użytkownika na dwóch stanowiskach."));
	} catch (const core::exceptions::DeviceUnregisteredException& ex) {
		::widgets::MessageBox::critical(this, tr("Błąd rejestrowania użądzenia"),
			tr("Urządzenie nie jest zarejestrowane u usłudze produkcyjnej. Praca w systemie nie jest możliwa.\n"
			   "Ponowne uruchomienie aplikacji może rozwiązać problem"));
	}
}

} // namespace eSawmill
