add_library(account-lib)
target_sources(account-lib PRIVATE
    "include/account_global.h"
    "include/adduserdialog.hpp"
    "include/authdialog.h"
    "src/adduserdialog.cpp"
    "src/authdialog.cpp"

    # models directory
    "include/models/usermodel.hpp"
    "src/models/usermodel.cpp"

    # widgets directory
    "include/widgets/databaseconnectionwidget.hpp"
    "include/widgets/remoteconnectionwidget.hpp"
    "include/widgets/userconfig.hpp"
    "src/widgets/databaseconnectionwidget.cpp"
    "src/widgets/remoteconnectionwidget.cpp"
    "src/widgets/userconfig.cpp"
)
target_compile_definitions(account-lib PRIVATE ACCOUNT_LIBRARY)
target_include_directories(account-lib
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
)
target_link_libraries(account-lib
    Qt::Gui
    Qt::Widgets
    Qt::Sql
    Qt::Xml
    Qt::Network
    core-lib
    widgets-lib
)
install(TARGETS account-lib)
