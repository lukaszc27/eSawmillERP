#ifndef PURCHASEINVOCIEITEMSMODEL_HPP
#define PURCHASEINVOCIEITEMSMODEL_HPP

#include "../../invoices_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/purchaseinvoice.hpp>
#include <dbo/purchaseinvoiceitem.hpp>
#include <messagebox.h>


namespace eSawmill::invoices::models::purchases {

class INVOICES_EXPORT InvoiceItemModel : public core::models::AbstractModel<core::PurchaseInvoiceItem> {
	Q_OBJECT

public:
	explicit InvoiceItemModel(QObject* parent = nullptr, int invoiceId = 0);
	virtual ~InvoiceItemModel() = default;

	// store invoice items from db in current model
	//
	void loadInvoiceItems(const QList<core::PurchaseInvoiceItem>& items);
	void loadInvoiceItems(core::eloquent::DBIDKey invoiceId);
	void loadInvoiceItems(const core::PurchaseInvoice& invoice) { loadInvoiceItems(invoice.id()); }

	// column index in model
	enum Columns {
		Name,
		Quantity,
		MeasureUnit,
		Rebate,
		Price,
		Value,
		Warehouse
	};

	void createOrUpdate(core::PurchaseInvoiceItem element) override;
	void destroy(core::PurchaseInvoiceItem element) override;
	void get() override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	QStringList headerData() const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// display data in model
	//
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

	///
	/// \brief calculateTotalItemsValue
	/// recalculate total value all items in invoice
	/// \return
	///
	double calculateTotalItemsValue();

private:
	int mInvoiceId;

protected:
	void notifyDataChanged() override;

Q_SIGNALS:
	///
	/// \brief rebateChanged - signal emited when user change rebate value on list
	/// \param val current rebate valuesss
	///
//	void rebateChanged(int val);
	///
	/// \brief dataChanged
	/// this signal will be emitted when user change data in model
	/// e.g. by adding new/edit/remove elements in model
	///
	void dataChanged();
};

}

#endif // PURCHASEINVOCIEITEMSMODEL_HPP
