#ifndef PURCHASEINVOICESMODEL_HPP
#define PURCHASEINVOICESMODEL_HPP

#include "../../invoices_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/purchaseinvoice.hpp>


namespace eSawmill::invoices::models::purchases {

class INVOICES_EXPORT InvoicesModel : public core::models::AbstractModel<core::PurchaseInvoice> {
	Q_OBJECT

public:
	InvoicesModel(QObject* parent = nullptr);
	virtual ~InvoicesModel() = default;

	///
	/// \brief The Columns enum
	/// columns indexes uses in model
	///
	enum Columns {
		DocumentNumber,
		SourceDocument,
		Status,
		DateOfReceipt,
		Contractor,
		NIP,
		Warehouse,
		City,
		Value
	};

	///
	/// \brief The Roles enum
	/// additional roles
	///
	enum Roles {
		Id = Qt::UserRole + 1,
		PaidStatus,
		DateOfIssue,
		DateOfPurchase,
		BufferStatus,
		HasCorrection,
	};

	///@{
	/// THIS METHODS ARE DEPRECATED
	/// YOU SCHOULDN'T USE IT NOW BECAUSE IN NEXT CODE REFACTORING CAN BE REMOVED
	void createOrUpdate(core::PurchaseInvoice element) override;
	void destroy(core::PurchaseInvoice element) override;
	void get() override;
	///@}

	///@{
	/// methods uses by Qt framework to display data
	Qt::ItemFlags flags(const QModelIndex& index) const override;
	int rowCount(const QModelIndex& index) const override;
	QStringList headerData() const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	///@}

//private:
//	QList<core::PurchaseInvoice> m_invoices;
};

}

#endif // PURCHASEINVOICESMODEL_HPP
