#ifndef SALEINVOICESMODEL_H
#define SALEINVOICESMODEL_H

#include "../../invoices_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/saleinvoice.hpp>

namespace eSawmill::invoices::models::sales {

class INVOICES_EXPORT InvoicesModel : public core::models::AbstractModel<core::SaleInvoice> {
	Q_OBJECT

public:
	explicit InvoicesModel(QObject* parent = nullptr);
	virtual ~InvoicesModel();

	///
	/// \brief setReadOnly
	/// set model as read only (users can't edit values)
	/// \param readonly
	///
	void setReadOnly(bool readonly) { mReadOnly = readonly; }

	///
	/// \brief The Columns enum
	/// columns uses in model
	///
	enum Columns {
		DocumentNumber,
		Status,
		DateOfReceipt,
		Contractor,
		NIP,
		Warehouse,
		City,
		Value
	};

	///
	/// \brief The Roles enum
	/// additional roles uses in model
	/// (it is uses in data method to return values from model)
	///
	enum Roles {
		Id = Qt::UserRole + 1,	///< sores invoide id
		PaidStatus,				///< inform about pay status
		DateOfIssue,
		DateOfSale,				///< stores date of sale (when invoice was generated)
		BufferStatus,			///< inform db invoice is set in buffer (in buffer only users can edit invoice)
		HasCorrectionDocument	///< tells do invoice has corrections
	};

	QStringList headerData() const override final;

	void createOrUpdate(core::SaleInvoice element) override final;
	void destroy(core::SaleInvoice element) override final;
	void get() override final;

	int rowCount(const QModelIndex& index) const override;
	Qt::ItemFlags flags(const QModelIndex& index) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

private:
	bool mReadOnly;
//	QList<core::SaleInvoice> m_invoices;
};

} // namespace

#endif // SALEINVOICESMODEL_H
