#ifndef INVOICEITEMSMODEL_H
#define INVOICEITEMSMODEL_H

#include "../../invoices_global.h"
#include <abstracts/abstractmodel.h>
#include "eloquent/model.hpp"
#include <dbo/saleinvoiceitem.hpp>
#include <dbo/saleinvoice.hpp>


namespace eSawmill::invoices::models::sales {

class INVOICES_EXPORT InvoiceItemsModel : public core::models::AbstractModel<core::SaleInvoiceItem> {
	Q_OBJECT

public:
	explicit InvoiceItemsModel(QObject* parent = nullptr);
	virtual ~InvoiceItemsModel() = default;

//	void loadInvoiceItems(const QList<core::SaleInvoiceItem>& items);
	void loadInvoiceItems(core::eloquent::DBIDKey invoiceId);
	void loadInvoiceItems(const core::SaleInvoice& invoice) { loadInvoiceItems(invoice.id()); }

	///
	/// \brief flags for specific cell in model
	/// \param index
	/// \return
	///
	Qt::ItemFlags flags(const QModelIndex& index) const override;

	QStringList headerData() const override final;

	void createOrUpdate(core::SaleInvoiceItem element) override;
	void destroy(core::SaleInvoiceItem element) override;
	void get() override;

	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// columns uses in model
	enum Columns {
		Name,
		Quantity,
		MeasureUnit,
		Rebate,
		Price,
		Value,
		Warehouse
	};

	///
	/// \brief calculateTotalNettoPrice
	/// calculate total price for invoice
	/// \return total sum items
	///
	double calculateTotalNettoPrice();

private:
	core::eloquent::DBIDKey mInvoiceId;	///< id actual editing invoice, uses only when user load invoice

	///
	/// \brief updateValuesInList
	/// update values in list to show actual data in model
	///
	void updateValuesInList();

protected:
	void notifyDataChanged() override;

Q_SIGNALS:
	///
	/// \brief dataChanged
	/// data will be changed in model
	/// this signal is emitted after notifyDataChanged
	///
	void dataChanged();
};

} // namespaces eSawmill::invoices::models

#endif // INVOICEITEMSMODEL_H
