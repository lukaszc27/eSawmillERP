#ifndef PAYMENTMETHODMODEL_H
#define PAYMENTMETHODMODEL_H

#include <QStringListModel>
#include "../invoices_global.h"


namespace eSawmill::invoices::models {

///
/// \brief The PaymentMethodModel class
/// payment methods uses by system
///
class INVOICES_EXPORT PaymentMethodModel : public QStringListModel {
	Q_OBJECT

public:
	explicit PaymentMethodModel(QObject* parent);
	virtual ~PaymentMethodModel() = default;
};

} // namespace eSawmill::invoices::models

#endif // PAYMENTMETHODMODEL_H
