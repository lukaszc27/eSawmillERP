#ifndef SALEINVOICESFILTER_HPP
#define SALEINVOICESFILTER_HPP

#include "invoices_global.h"
#include <QSortFilterProxyModel>
#include <QDate>

namespace eSawmill::invoices::filters::sales {

class INVOICES_EXPORT SaleInvoicesFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	SaleInvoicesFilter(QObject* parent = nullptr);
	virtual ~SaleInvoicesFilter() = default;

	// filter all records and set only where date of sale is between 'from' to 'to'
	void setDateOfSaleFilter(
			bool active,
			const QDate& from,
			const QDate& to);

	void setDateOfIssueFilter(
			bool active,
			const QDate& from,
			const QDate& to);

	void setValueFilter(
			bool active,
			double minValue,
			double maxValue);

public slots:
	///
	/// \brief setContractorNameFilter
	/// enable filtering list by contractor name
	/// \param contractor - contractor name (only that name will be display in view)
	///
	void setContractorNameFilter(const QString& contractor);
	void setWarehouseNameValue(const QString& warehouse);

	///
	/// \brief setBufferFilterEnabled show only invoices existing in buffer
	/// \param enabled
	///
	void setBufferFilterEnabled(bool enabled) {
		mBufferFilterEnabled = enabled;
		invalidateFilter();
	}

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
	bool filterByDateOfSale(int source_row, const QModelIndex& source_parent) const;
	bool filterByDateOfIssue(int source_row, const QModelIndex& source_parent) const;
	bool filterByValue(int source_row, const QModelIndex& source_parent) const;
	bool filterByContractorName(int source_row, const QModelIndex& source_parent) const;
	bool filterByWarehouseName(int source_row, const QModelIndex& source_parent) const;
	bool filterByExistInBuffer(int source_row, const QModelIndex& source_parent) const;

private:
	// for sale date filter
	QDate mDateOfSaleFrom;
	QDate mDateOfSaleTo;
	bool mDateOfSaleActive;

	// for issue date filter
	QDate mDateOfIssueFrom;
	QDate mDateOfIssueTo;
	bool mDateOfIssueActive;

	// for total value filter
	double mValueFrom;
	double mValueTo;
	bool mValueActive;
	bool mBufferFilterEnabled;

	QString mContractorNameValue;
	QString mWarehouseNameValue;
};

}

#endif // SALEINVOICESFILTER_HPP
