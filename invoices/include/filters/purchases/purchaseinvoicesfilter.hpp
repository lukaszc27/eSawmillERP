#ifndef PURCHASEINVOICESFILTER_HPP
#define PURCHASEINVOICESFILTER_HPP

#include "../../invoices_global.h"
#include <QSortFilterProxyModel>
#include <QDate>


namespace eSawmill::invoices::filters::purchases {

class INVOICES_EXPORT PurchaseInvoicesFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	PurchaseInvoicesFilter(QObject* parent = nullptr);
	virtual ~PurchaseInvoicesFilter() = default;

	// enable/disable filtering by purchase date on list
	//
	void setFilterByPurchaseDateEnabled(
			bool enabled,
			const QDate& minDate,
			const QDate& maxDate)
	{
		mFilterByPurchaseDateEnabled = enabled;
		mPurchaseMaxDate = maxDate;
		mPurchaseMinDate = minDate;
		invalidateFilter();
	}

	// enable/disable filtersing by issue date on list
	//
	void setFilterByIssueDate(
			bool enabled,
			const QDate& minDate,
			const QDate& maxDate)
	{
		mFilterByIssueDateEnabled = enabled;
		mIssueMinDate = minDate;
		mIssueMaxDate = maxDate;
		invalidateFilter();
	}

	void setFilterByValueEnabled(
			bool enabled,
			double minValue,
			double maxValue)
	{
		mFilterByValueEnabled = enabled;
		mMinValue = minValue;
		mMaxValue = maxValue;
		invalidateFilter();
	}

public slots:
	void setWarehouseName(const QString& name) {
		mWarehouseName = name;
		invalidateFilter();
	}

	void setContractorName(const QString& name) {
		mContractorName = name;
		invalidateFilter();
	}

	void setBufferFilter(bool enabled) {
		mBufferFilterEnabled = enabled;
		invalidateFilter();
	}

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
	bool filterByPurchaseDate(int source_row, const QModelIndex& parent) const;
	bool filterByIssueDate(int source_row, const QModelIndex& parent) const;
	bool filterByValue(int source_row, const QModelIndex& parent) const;
	bool filterByWarehouseName(int source_row, const QModelIndex& parent) const;
	bool filterByContractorName(int source_row, const QModelIndex& parent) const;
	bool filterByExistInBuffer(int source_row, const QModelIndex& parent) const;

private:
	bool mFilterByPurchaseDateEnabled;
	bool mFilterByIssueDateEnabled;
	bool mFilterByValueEnabled;
	bool mBufferFilterEnabled;
	double mMinValue, mMaxValue;
	QDate mPurchaseMinDate, mPurchaseMaxDate;
	QDate mIssueMinDate, mIssueMaxDate;
	QString mWarehouseName, mContractorName;
};

}

#endif // PURCHASEINVOICESFILTER_HPP
