#ifndef SALEINVOCIESVERIFICATION_HPP
#define SALEINVOCIESVERIFICATION_HPP

#include <verifications/abstractverification.hpp>
#include "../invoices_global.h"


namespace eSawmill::invoices::verifications {

class INVOICES_EXPORT SaleInvoicesVerification : public core::verifications::AbstractVerification {
public:
	explicit SaleInvoicesVerification();
	virtual ~SaleInvoicesVerification() = default;

	void execute() override;
};

} // namespace eSawmill::invoices::verifications

#endif // SALEINVOCIESVERIFICATION_HPP
