#ifndef PURCHASEINVOICESVERIFICATION_HPP
#define PURCHASEINVOICESVERIFICATION_HPP

#include <verifications/abstractverification.hpp>
#include "invoices_global.h"


namespace eSawmill::invoices::verifications {

class INVOICES_EXPORT PurchaseInvoicesVerification : public core::verifications::AbstractVerification {
public:
	explicit PurchaseInvoicesVerification();
	virtual ~PurchaseInvoicesVerification() = default;

	void execute() override;
};

} // namespace eSawmill::invoices::verifications

#endif // PURCHASEINVOICESVERIFICATION_HPP
