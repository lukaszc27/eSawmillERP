#ifndef SALESINVOICESMANAGER_H
#define SALESINVOICESMANAGER_H

#include "invoices_global.h"
#include "tableview.h"
#include "pushbutton.h"
#include "models/sales/saleinvoicesmodel.hpp"
#include "../filters/sales/saleinvoicesfilter.hpp"
#include <QDialog>
#include <QCheckBox>
#include <QDateEdit>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QList>
#include <dbo/saleinvoiceitem.hpp>
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>


namespace agent = core::providers::agent;

namespace eSawmill::invoices::sales {

class INVOICES_EXPORT SalesInvoicesManager : public QDialog {
	Q_OBJECT

public:
	explicit SalesInvoicesManager(bool selectedMode, QWidget* parent = nullptr);
	explicit SalesInvoicesManager(QWidget* parent = nullptr);
	virtual ~SalesInvoicesManager();

	// ID selected invoice
	//
	int selectedInvoiceId() const { return mSaleInvoiceId; }

	///
	/// \brief editInvoice main functionto edit existing invoice
	/// run edit dialog, and save edited object in db
	/// \param documentId - document to edit
	/// \param parent - parent widget for dialog
	///
	static void editInvoice(int documentId, QWidget* parent = nullptr);

public Q_SLOTS:
	void handleRepositoryNotification(const agent::RepositoryEvent& event);

private:
	// true - when manager is running in select mode
	// (user can select single invoice)
	bool mSelectedMode;
	int mSaleInvoiceId;

	void createWidgets();
	void createLayouts();
	void createConnections();
	void createModels();
	void createContextMenu();

	// context menu actions
	QAction* mEditAction;
	QAction* mPrintAction;
	QAction* mPostAction;
	QAction* mMarkAsPaidAction;
	QAction* mShowCorrectionDocumentAction;

	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mEditButton;
	::widgets::PushButton* mPrintButton;
	::widgets::PushButton* mCloseButton;
	QCheckBox* mSaleDate;
	QDateEdit *mSaleDateFrom, *mSaleDateTo;
	QCheckBox* mIssueDate;
	QDateEdit *mIssueDateFrom, *mIssueDateTo;
	QCheckBox* mSum;
	QDoubleSpinBox *mSumFrom, *mSumTo;
	QLineEdit* mWarehouseName;
	QLineEdit* mContractorName;
	QCheckBox* mBufferFilter;

	///
	/// \brief mInvoices - model to manage sale invoices
	///
	models::sales::InvoicesModel* mInvoices;
	filters::sales::SaleInvoicesFilter* mInvoicesFilter;

	///
	/// \brief applyChangesInWarehouses
	/// remove quantity in warehouses for articles
	/// in sale invoice
	/// \param invoice - invoice to modify
	///
	void applyChangesInWarehouses(const core::SaleInvoiceItem& item);

private Q_SLOT:
	///
	/// \brief addInvoiceHandle
	/// adding new invoice to db
	///
	void addInvoiceHandle();

	///
	/// \brief editInvoiceHandle
	/// edit existing invoice
	///
	void editInvoiceHandle();

	///
	/// \brief printInvoiceHandle
	///
	void printInvoiceHandle();

	///
	/// \brief markSelectedAsPaid
	/// mark all selected rows as paid
	///
	void markSelectedAsPaid();

	///
	/// \brief postSelectedInvoices
	/// remove invoices from buffer
	/// so in future user can't adding or review items from invoice
	///
	void postSelectedInvoices();

	///
	/// \brief showCorrectionDocumentHandle
	///
	void showCorrectionDocumentHandle();

	///
	/// \brief fitActionInContextMenu
	/// enable and disable action in context menu
	///
	void fitActionInContextMenu();
};

} // namespace invoices

#endif // SALESINVOICESMANAGER_H
