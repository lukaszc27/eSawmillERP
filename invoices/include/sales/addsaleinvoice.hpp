#ifndef ADDSALEINVOICE_H
#define ADDSALEINVOICE_H

#include <QDialog>
#include <QTabWidget>
#include <QCheckBox>
#include "invoices_global.h"
#include "pushbutton.h"
#include "widgets/sales/generalwidget.hpp"
#include <dbo/saleinvoice.hpp>


namespace eSawmill::invoices::sales {

class INVOICES_EXPORT AddSaleInvoice : public QDialog {
	Q_OBJECT

public:
	AddSaleInvoice(QWidget* parent = nullptr);
	AddSaleInvoice(const core::SaleInvoice& invoice, QWidget* parent = nullptr);
	virtual ~AddSaleInvoice();

	QList<core::SaleInvoiceItem> invoiceItems() const { return mGeneralWidget->invoiceItems(); }
	core::SaleInvoice invoice() const;

	// return true when user created correction to existing invoice
	bool isCorrectionInvoice() const { return mGeneralWidget->isCorrectionInvoice(); }
	int correctionInvoiceId() const { return mGeneralWidget->correctionInvoiceId(); }

private:
	///
	/// \brief mInvoiceId - current editing invoice
	/// if value is zero user will create new invoice
	///
	int mInvoiceId;

	void createWidgets();
	void createLayouts();
	void createConnections();

	QTabWidget* mTabWidget;
	QCheckBox* mBuffer;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;

	// addictional widgets
	widgets::sales::GeneralWidget* mGeneralWidget;
};

} // namespace eSawmill::invoices

#endif // ADDSALEINVOICE_H
