#ifndef ADDPURCHASEINVOICE_H
#define ADDPURCHASEINVOICE_H

#include "../invoices_global.h"
#include "pushbutton.h"
#include <QDialog>
#include <QTabWidget>
#include <QCheckBox>
#include "../widgets/purchase/purchasegeneralwidget.hpp"
#include <dbo/purchaseinvoice.hpp>
#include <dbo/purchaseinvoice.hpp>


namespace eSawmill::invoices::purchase {

class INVOICES_EXPORT AddPurchaseInvoice : public QDialog {
	Q_OBJECT

public:
	AddPurchaseInvoice(QWidget* parent = nullptr);
	AddPurchaseInvoice(core::PurchaseInvoice& invoice, QWidget* parent = nullptr);
	virtual ~AddPurchaseInvoice();

	///
	/// \brief purchaseInvoice
	/// create purchase invoice object with data from user
	/// \return
	///
	core::PurchaseInvoice purchaseInvoice() const;
	bool isCorrectionInvoice() const { return mGeneralWidget->isCorrectionInvoice(); }
	int correctionInvoiceId() const { return mGeneralWidget->correctionInvoiceId(); }

	///
	/// \brief purchaseInvoiceItems
	/// \return
	///
	QList<core::PurchaseInvoiceItem> purchaseInvoiceItems() const { return mGeneralWidget->invoiceItems(); }

private:
	void createWidgets();
	void createLayouts();
	void createConnections();

	// additionals widgets used in dialog
	QTabWidget* mTabWidget;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
	QCheckBox* mBuffer;

	// widgets used in tab widget
	eSawmill::invoices::widgets::purchase::PurchaseGeneralWidget* mGeneralWidget;

	// id current editing invoice
	// if it's equal zero, the invoide will be created
	int mInvoideId;
};

} // namespace

#endif // ADDPURCHASEINVOICE_H
