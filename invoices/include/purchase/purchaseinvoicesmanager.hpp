#ifndef PURCHASEINVOICESMANAGER_H
#define PURCHASEINVOICESMANAGER_H

#include "../invoices_global.h"
#include <QDialog>
#include "tableview.h"
#include "pushbutton.h"
#include "../models/purchases/purchaseinvoicesmodel.hpp"
#include "../filters/purchases/purchaseinvoicesfilter.hpp"
#include <QCheckBox>
#include <QDateEdit>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <dbo/purchaseinvoiceitem.hpp>
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>


namespace agent = core::providers::agent;

namespace eSawmill::invoices::purchase {

class INVOICES_EXPORT PurchaseInvoiceManager : public QDialog {
	Q_OBJECT

public:
	explicit PurchaseInvoiceManager(bool selectedMode, QWidget* parent = nullptr);
	explicit PurchaseInvoiceManager(QWidget* parent = nullptr);
	virtual ~PurchaseInvoiceManager();

	int selectedInvoiceId() const { return mSelectedInvoiceId; }

public Q_SLOTS:
	void handleRepositoryNotification(const agent::RepositoryEvent& event);

private:
	bool mSelectedMode;
	int mSelectedInvoiceId;

	void createWidgets();
	void createLayouts();
	void createConnections();
	void createModels();
	void createContrextMenu();

	// actions used in context menu
	QAction* mEditAction;
	QAction* mPrintAction;
	QAction* mPostAction;
	QAction* mMarkAsPaidAction;
	QAction* mShowCorrectionDocumentAction;

	// additional widgets used in manager
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mEditButton;
	::widgets::PushButton* mPrintButton;
	::widgets::PushButton* mCloseButton;
	QCheckBox* mDateOfPurchase;
	QDateEdit* mDateOfPurchaseFrom;
	QDateEdit* mDateOfPurchaseTo;
	QCheckBox* mBufferFilter;	// show only invoices existing in buffer

	QCheckBox* mDateOfIssue;
	QDateEdit* mDateOfIssueFrom;
	QDateEdit* mDateOfIssueTo;

	QCheckBox* mTotalPrice;
	QDoubleSpinBox* mTotalPriceFrom;
	QDoubleSpinBox* mTotalPriceTo;
	
	QLineEdit* mWarehouse;
	QLineEdit* mContractor;

	// models
	eSawmill::invoices::models::purchases::InvoicesModel* mInvoicesModel;
	eSawmill::invoices::filters::purchases::PurchaseInvoicesFilter* mInvoicesProxyModel;


private Q_SLOTS:
	///
	/// \brief addInvoiceHandle add new invoice from manager to database
	///
	void addInvoiceHandle();

	///
	/// \brief editInvoideHandle
	/// is running when user double click on list
	///
	void editInvoideHandle();
	void editInvoice(core::eloquent::DBIDKey invoiceId);

	///
	/// \brief printInvoiceHandle
	///
	void printInvoiceHandle();

	///
	/// \brief filterByPurchaseDate
	/// filter list with invoices by rage purchase date
	/// given by user
	///
	void filterByPurchaseDate();
	void filterByIssueDate();
	void filterByValue();

	///
	/// \brief markInvoicesAsPaid
	/// mark all selected invoces as paid
	///
	void markInvoicesAsPaid();

	///
	/// \brief postSelectedInvoices
	/// post all selected invoices
	/// Warning: now don't change qty in warehouses for articles
	///
	void postSelectedInvoices();

	///
	/// \brief showCorrectionDocument
	///
	void showCorrectionDocument();

	///
	/// \brief fitContextMenuToSelectedOption
	/// fitted options in menu (disable or enable)
	/// afer that how user selected row in view
	///
	void fitContextMenuToSelectedOption();
};

} // namespace eSawmill::invocies::purchase

#endif // PURCHASEINVOICESMANAGER_H
