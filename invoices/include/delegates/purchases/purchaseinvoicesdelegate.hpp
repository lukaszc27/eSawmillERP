#ifndef PURCHASEINVOICESDELEGATE_HPP
#define PURCHASEINVOICESDELEGATE_HPP

#include <QStyledItemDelegate>
#include "../../invoices_global.h"


namespace eSawmill::invoices::delegates::purchases {

class INVOICES_EXPORT PurchaseInvoicesDelegate final : public QStyledItemDelegate {
	Q_OBJECT

public:
	PurchaseInvoicesDelegate(QObject* parent = nullptr);
	virtual ~PurchaseInvoicesDelegate() = default;

	QWidget* createEditor(QWidget* parent,
						  const QStyleOptionViewItem& option,
						  const QModelIndex& index) const override;

	void setEditorData(QWidget* editor,
					   const QModelIndex& index) const override;

	void setModelData(QWidget* editor,
					  QAbstractItemModel* model,
					  const QModelIndex& index) const override;

	void paint(QPainter* painter,
			   const QStyleOptionViewItem& option,
			   const QModelIndex& index) const override;
};

}

#endif // PURCHASEINVOICESDELEGATE_HPP
