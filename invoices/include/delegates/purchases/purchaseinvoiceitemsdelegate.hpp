#ifndef PURCHASEINVOICEITEMSDELEGATE_HPP
#define PURCHASEINVOICEITEMSDELEGATE_HPP

#include "../../invoices_global.h"
#include <QStyledItemDelegate>

namespace eSawmill::invoices::delegates::purchases {

class INVOICES_EXPORT PurchaseInvoiceItemsDelegate : public QStyledItemDelegate {
	Q_OBJECT

public:
	PurchaseInvoiceItemsDelegate(QObject* parent = nullptr);
	virtual ~PurchaseInvoiceItemsDelegate() = default;

	QWidget* createEditor(QWidget* parent,
						  const QStyleOptionViewItem& option,
						  const QModelIndex& index) const override;

	void setEditorData(QWidget* editor,
					   const QModelIndex& index) const override;

	void setModelData(QWidget* editor,
					  QAbstractItemModel* model,
					  const QModelIndex& index) const override;
};

}

#endif // PURCHASEINVOICEITEMSDELEGATE_HPP
