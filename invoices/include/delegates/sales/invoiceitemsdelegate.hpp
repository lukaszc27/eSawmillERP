#ifndef INVOICEITEMSDELEGATE_HPP
#define INVOICEITEMSDELEGATE_HPP

#include "../../invoices_global.h"
#include <QStyledItemDelegate>


namespace eSawmill::invoices::delegates::sales {

class INVOICES_EXPORT InvoiceItemsDelegate : public QStyledItemDelegate {
    Q_OBJECT

public:
    InvoiceItemsDelegate(QObject* parent = nullptr);
    virtual ~InvoiceItemsDelegate() = default;

    QWidget* createEditor(QWidget* parent,
                          const QStyleOptionViewItem& option,
                          const QModelIndex& index) const override;

    void setEditorData(QWidget* editor,
                       const QModelIndex& index) const override;

    void setModelData(QWidget* editor,
                      QAbstractItemModel* model,
                      const QModelIndex& index) const override;
};

}

#endif // INVOICEITEMSDELEGATE_HPP
