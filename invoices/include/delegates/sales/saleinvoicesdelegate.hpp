#ifndef SALEINVOICESDELEGATE_HPP
#define SALEINVOICESDELEGATE_HPP

#include "../../invoices_global.h"
#include <QStyledItemDelegate>


namespace eSawmill::invoices::delegates::sales {

class INVOICES_EXPORT SaleInvoicesDelegate final : public QStyledItemDelegate {
	Q_OBJECT

public:
	SaleInvoicesDelegate(QObject* parent = nullptr);
	virtual ~SaleInvoicesDelegate() = default;

	QWidget* createEditor(QWidget* parent,
						  const QStyleOptionViewItem& option,
						  const QModelIndex& index) const override;

	void setEditorData(QWidget* editor,
					   const QModelIndex& index) const override;

	void setModelData(QWidget* editor,
					  QAbstractItemModel* model,
					  const QModelIndex& index) const override;

	void paint(QPainter* painter,
			   const QStyleOptionViewItem& option,
			   const QModelIndex& index) const override;
};

}

#endif // SALEINVOICESDELEGATE_HPP
