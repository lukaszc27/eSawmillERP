#ifndef PURCHASEGENERALWIDGET_HPP
#define PURCHASEGENERALWIDGET_HPP

#include "../../invoices_global.h"
#include <QWidget>
#include <QLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDateEdit>
#include <QFrame>
#include "widgets/contractorselector.h"
#include "tableview.h"
#include "pushbutton.h"
#include "invoicenumberspinbox.hpp"
#include "models/warehouselist.h"
#include "models/purchases/purchaseinvocieitemsmodel.hpp"
#include "dbo/purchaseinvoice.hpp"
#include "dbo/purchaseinvoiceitem.hpp"
#include "../invoiceselector.hpp"


namespace eSawmill::invoices::widgets::purchase {

class INVOICES_EXPORT PurchaseGeneralWidget : public QWidget {
	Q_OBJECT

public:
	PurchaseGeneralWidget(QWidget* parent = nullptr);
	virtual ~PurchaseGeneralWidget() = default;

	// return created object from field on widget
	//
	core::PurchaseInvoice purchaseInvoice() const;
	bool isCorrectionInvoice() const { return mDocumentPrefix->text().contains("KOR", Qt::CaseInsensitive); }
	int correctionInvoiceId() const { return mInvoiceSelector->selectedInvoiceId(); }

	// get all invoice items created by user
	//
	QList<core::PurchaseInvoiceItem> invoiceItems() const { return mInvoiceItemsModel->items(); }

	// load data from db object
	// and set in form field
	//
	void loadInvoice(core::PurchaseInvoice& invoice);

private:
	void createWidgets();
	void createLayouts();
	void createModels();
	void createConnections();

	/// models
	eSawmill::articles::models::WarehouseList* mWarehouseModel;
	eSawmill::invoices::models::purchases::InvoiceItemModel* mInvoiceItemsModel;

	// additional widgets used in dialog
	QComboBox* mDocumentType;
	QLineEdit* mDocumentPrefix;
	::widgets::InvoiceNumberSpinBox* mDocumentNumber;
	QLineEdit* mDocumentNumberMonth;
	QLineEdit* mDocumentNumberYear;

	QDateEdit* mDateOfDepature;		// data wpływu dokumentu
	QLineEdit* mForeignNumber;		// numer obcy dokumentu

	eSawmill::contractors::widgets::ContractorSelector* mContractor;
	QComboBox* mWarehouse;

	QDateEdit* mDateOfIssue;		// data wystawienia
	QDateEdit* mDateOfPurchase;		// data zakupu
	QSpinBox* mRebate;				// rabat
	QComboBox* mPayMethod;			// sposób płatności
	QDateEdit* mPayDeadline;		// termin płatności
	QSpinBox* mPayDeadlineDays;		// termin płatności (ilość dni)

	QDoubleSpinBox* mNettoPrice;	// netto
	QDoubleSpinBox* mTotalPrice;	// wartość całkowita (razem)
	QDoubleSpinBox* mPaidPrice;		// zapłacono
	QDoubleSpinBox* mToPayPrice;	// pozostaje

	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;

	QFrame* mForeignInvoice;
	widgets::InvoiceSelectror* mInvoiceSelector;

	///
	/// \brief createDocumentTypeWidgets created all widget
	/// to manage type of invoice and invoice number
	/// \return
	///
	QLayout* createDocumentTypeWidgets();

	///
	/// \brief createForeignDocumentWidgets
	/// created all widgets in foreign number section
	/// \return layouts with widgets in section
	///
	QLayout* createForeignDocumentWidgets();

	///
	/// \brief createHeaderWidgets
	/// create document header section
	/// \return
	///
	QWidget* createHeaderWidgets();

	///
	/// \brief createDateAndValuesWidgets
	/// create section date and values
	/// \return
	///
	QWidget* createDateAndValuesWidgets();

private Q_SLOTS:
	///
	/// \brief addArticleHandle
	///
	void addArticleHandle();

	///
	/// \brief addWoodElementDialog
	///
	void addWoodElementDialog();

	///
	/// \brief removeSelectedItems
	///
	void removeSelectedItems();

	///
	/// \brief selectCorrectionInvoice
	/// \param invoiceId - correction invoice id
	/// \param type - type of invoice
	///
	void selectCorrectionInvoice(int invoiceId, widgets::InvoiceSelectror::InvoiceType type);
};

} // namespace

#endif // PURCHASEGENERALWIDGET_HPP
