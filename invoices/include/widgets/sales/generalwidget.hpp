#ifndef GENERALWIDGET_HPP
#define GENERALWIDGET_HPP

#include "../../invoices_global.h"
#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QGroupBox>
#include <QDateEdit>
#include <QFrame>
#include "models/warehouselist.h"
#include "models/sales/invoiceitemsmodel.hpp"
#include "widgets/contractorselector.h"
#include "tableview.h"
#include "pushbutton.h"
#include "invoicenumberspinbox.hpp"
#include "dbo/saleinvoice.hpp"
#include "../invoiceselector.hpp"


namespace eSawmill::invoices::widgets::sales {

class INVOICES_EXPORT GeneralWidget : public QWidget {
	Q_OBJECT

public:
	explicit GeneralWidget(QWidget* parent = nullptr);
	virtual ~GeneralWidget();

	///
	/// \brief invoiceItems
	/// return all items assigned to invoice
	/// \return list of SaleInvoceItem elements
	///
	QList<core::SaleInvoiceItem> invoiceItems() const;
	core::SaleInvoice invoice() const;

	///
	/// \brief loadInvoice load data from existing invoice in db
	/// \param invoice invoice object downloaded from db
	///
	void loadInvoice(const core::SaleInvoice& invoice);
	bool isCorrectionInvoice() const { return mCorrectionInvoice; }
	int correctionInvoiceId() const { return mInvoiceSelector->selectedInvoiceId(); }

private:
	// true when current document is correction
	bool mCorrectionInvoice;
	core::eloquent::DBIDKey mInvoiceId;	///< current loaded invoice identificator

	void createWidgets();
	void createConnections();
	void createModels();

	QComboBox* mInvoiceType;
	QLineEdit* mInvoicePrefix;
	::widgets::InvoiceNumberSpinBox* mInvoiceNumber;
	QLineEdit* mInvoiceNumberMonth;
	QLineEdit* mInvoiceYear;

	// items in invoice group
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;

	// header group
	contractors::widgets::ContractorSelector* mContractorSelector;
	QComboBox* mWarehouse;

	// date and values group
	QDateEdit* mDateOfIssue;
	QDateEdit* mDateOfSale;
	QSpinBox* mRebate;
	QComboBox* mTypeOfPay;
	QSpinBox* mPayDeadLineDays;
	QDateEdit* mPayDeadline;
	// prices
	QDoubleSpinBox* mNettoPrice;
	QDoubleSpinBox* mTotalPrice;
	QDoubleSpinBox* mPaidPrice;
	QDoubleSpinBox* mToPayPrice;

	QFrame* mForeignInvoiceFrame;
	widgets::InvoiceSelectror* mInvoiceSelector;

	//-->> models
	eSawmill::articles::models::WarehouseList* mWarehouseList;
	models::sales::InvoiceItemsModel* mInvoiceItems;

	///
	/// \brief createDocumentTypeWidgets
	/// create all widgets to manage of invoice and invoice number
	/// \return
	///
	QLayout* createDocumentTypeWidgets();

	///
	/// \brief createHeaderWidgets
	/// create document header group
	/// \return
	///
	QWidget* createHeaderWidgets();

	///
	/// \brief createDateAndValuesWidgets
	/// create section to date and values
	/// \return
	///
	QWidget* createDateAndValuesWidgets();

private Q_SLOTS:
	///
	/// \brief addArticleHandle
	/// add new article to invoice
	///
	void addArticleHandle();

	///
	/// \brief addWoodElementHandle
	/// add new wood element to invoice
	///
	void addWoodElementHandle();

    ///
    /// \brief removeElementHandle
    /// remove selected item from invoice
    ///
    void removeElementHandle();

	///
	/// \brief selectCorrectionInvoice
	/// \param invoiceId
	/// \param type
	///
	void selectCorrectionInvoice(int invoiceId, widgets::InvoiceSelectror::InvoiceType type);
};

} // namespace eSawmill::invoices::widgets

#endif // GENERALWIDGET_HPP
