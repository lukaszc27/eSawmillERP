#ifndef INVOICESELECTOR_HPP
#define INVOICESELECTOR_HPP

#include "../invoices_global.h"
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include "eloquent/model.hpp"


namespace eSawmill::invoices::widgets {

class InvoiceSelectror : public QWidget {
Q_OBJECT

public:
	// type of invoice to select
	//
	enum class InvoiceType : short {
		PurchaseInvoce,
		SaleInvoce
	};

	// constructor and descrutor
	//
	InvoiceSelectror(InvoiceType type, QWidget* parent = nullptr);
	virtual ~InvoiceSelectror() = default;

	int selectedInvoiceId() const { return mInvoiceId; }

	///
	/// \brief setInvoice select invoice in widget
	/// \param invoiceId id invoice to selected
	/// \param type type of invoice
	///
	void setInvoice(core::eloquent::DBIDKey invoiceId, InvoiceType type);

	///
	/// \brief setReadOnly show widget in readonly mode
	/// \param readonly
	///
	void setReadOnly(bool readonly);

private:
	void createWidgets();

	QLineEdit* mInvoiceNumber;
	QPushButton* mSelectButton;

	// type of invoice to select
	InvoiceType mInvoiceType;
	int mInvoiceId;

private Q_SLOTS:
	void selectSaleInvoiceHandle();
	void selectPurchaseInvoiceHandle();

Q_SIGNALS:
	void invoiceSelected(int invoiceId, InvoiceType invoiceType);
};

} // namespace eSawmill::invoices::widgets

#endif // INVOICESELECTOR_HPP
