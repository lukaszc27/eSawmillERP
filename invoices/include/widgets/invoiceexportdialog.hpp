#ifndef INVOICEEXPORTDIALOG_HPP
#define INVOICEEXPORTDIALOG_HPP

#include <QDialog>
#include <QComboBox>
#include <QSpinBox>
#include <QDateTimeEdit>
#include "../invoices_global.h"
#include <eloquent/builder.hpp>


namespace eSawmill::invoices::widgets {

class INVOICES_EXPORT InvoiceExportDialog : public QDialog {
	Q_OBJECT

public:
	InvoiceExportDialog(QWidget* parent = nullptr);
	virtual ~InvoiceExportDialog() = default;

	int paymentDeadlineDays() const		{ return mPaymentDays->value(); }
	QDate paymentDeadlineDate() const	{ return mPaymentDate->date(); }
	QString paymentName() const			{ return mPayment->currentText(); }
	core::eloquent::DBIDKey warehouseId() const;

private:
	void createAllWidgets();
	void createModels();
	void createConnections();

	// controls used in widget
	QComboBox* mWarehouse;
	QComboBox* mPayment;
	QSpinBox* mPaymentDays;
	QDateEdit* mPaymentDate;
};

} // namespace eSawmill::invoices::widgets

#endif // INVOICEEXPORTDIALOG_HPP
