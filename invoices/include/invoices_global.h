#ifndef INVOICES_GLOBAL_H
#define INVOICES_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(INVOICES_LIBRARY)
#  define INVOICES_EXPORT Q_DECL_EXPORT
#else
#  define INVOICES_EXPORT Q_DECL_IMPORT
#endif

#endif // INVOICES_GLOBAL_H
