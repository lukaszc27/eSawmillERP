#include "sales/addsaleinvoice.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLayout>
#include <QLabel>


namespace eSawmill::invoices::sales {

AddSaleInvoice::AddSaleInvoice(QWidget* parent)
	: QDialog {parent}
	, mInvoiceId {0}
{
	setWindowTitle("Faktura sprzedaży");

	createWidgets();
	createLayouts();
	createConnections();

	mBuffer->setChecked(true);
	mBuffer->setToolTip("Czy dokument znajduje się w buforze");
}

AddSaleInvoice::AddSaleInvoice(const core::SaleInvoice &invoice, QWidget *parent)
	: AddSaleInvoice {parent}
{
	setWindowTitle(tr("Edycja faktury sprzedaży"));
	mAcceptButton->setText(tr("Zapisz"));
	mBuffer->setChecked(invoice.inBuffer());
	mInvoiceId = invoice.id();
	mGeneralWidget->loadInvoice(invoice);
}

AddSaleInvoice::~AddSaleInvoice()
{
}

core::SaleInvoice AddSaleInvoice::invoice() const
{
	core::SaleInvoice invoice = mGeneralWidget->invoice();
	invoice.existInBuffer(mBuffer->isChecked());
	invoice.set("Id", mInvoiceId);

	return invoice;
}

void AddSaleInvoice::createWidgets()
{
	mTabWidget = new QTabWidget(this);
	mBuffer = new QCheckBox("Bufor", this);
	mAcceptButton = new ::widgets::PushButton("Zapisz", QKeySequence("F10"), this, QIcon(":/icons/accept"));
	mRejectButton = new ::widgets::PushButton("Anuluj", QKeySequence("Esc"), this, QIcon(":/icons/reject"));

	// add addictional widgets to tab widget
	mGeneralWidget = new widgets::sales::GeneralWidget(this);
	mTabWidget->addTab(mGeneralWidget, "O&gólne");
}

void AddSaleInvoice::createLayouts()
{
	QLabel* titleLabel = new QLabel("Faktura sprzedaży");
	QFont font;
	font.setPixelSize(18);
	titleLabel->setFont(font);

	QHBoxLayout* docOptionLayout = new QHBoxLayout;
	docOptionLayout->addWidget(titleLabel);
	docOptionLayout->addStretch(1);
	docOptionLayout->addWidget(mBuffer);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout{this};
	mainLayout->addLayout(docOptionLayout);
	mainLayout->addWidget(mTabWidget);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(buttonsLayout);
}

void AddSaleInvoice::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &AddSaleInvoice::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &AddSaleInvoice::reject);
}

} // namespace eSawmill::invoices
