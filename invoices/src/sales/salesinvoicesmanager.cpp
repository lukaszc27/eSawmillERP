#include "sales/salesinvoicesmanager.hpp"
#include <memory>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QApplication>
#include "sales/addsaleinvoice.hpp"
#include "delegates/sales/saleinvoicesdelegate.hpp"
#include <eloquent/builder.hpp>
#include <dbo/saleinvoicecorrections.hpp>
#include <QDebug>
#include <QCompleter>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <messagebox.h>
#include <dbo/saleinvoiceitem.hpp>
#include <memory.h>
#include <documents/saleinvoicedocument.hpp>
#include <dbo/auth.h>
#include <dbo/article.h>
#include <settings/appsettings.hpp>
#include <globaldata.hpp>
#include <logs/abstractlogger.hpp>
#include <exceptions/forbiddenexception.hpp>


namespace eSawmill::invoices::sales {

SalesInvoicesManager::SalesInvoicesManager(bool selectedMode, QWidget* parent)
	: SalesInvoicesManager {parent}
{
	mSelectedMode = selectedMode;

	mAddButton->setDisabled(true);
	mPrintButton->setDisabled(true);
	mEditButton->setText(tr("Wybierz"));
	setWindowTitle(tr("Wybór faktury sprzedaży"));
	mInvoices->setReadOnly(true);

	// user selected invoice
	auto selectInvoiceHandle = [this]() -> void {
		if(!mTableView->selectionModel()->hasSelection()) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Nie wybrano żadnej pozycji na liście"));
			return;
		}

		mSaleInvoiceId = mTableView->selectionModel()->currentIndex().data(eSawmill::invoices::models::sales::InvoicesModel::Roles::Id).toInt();
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		const core::SaleInvoice& invoice = repository.getSaleInvoice(mSaleInvoiceId);
		if (repository.hasSaleInvoiceCorrection(mSaleInvoiceId)) {
			::widgets::MessageBox::information(this, tr("Informacja"),
					tr("Wybrany dokument posiada już dokument korekty!"));
			return;
		}

		if (invoice.inBuffer()) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Wybrany dokument znajduje się w buforze"));
			return;
		}
		accept();	// close manager with accepted status
	};

	disconnect(mEditButton, &::widgets::PushButton::clicked, this, &SalesInvoicesManager::editInvoiceHandle);
	connect(mEditButton, &::widgets::PushButton::clicked, selectInvoiceHandle);
	connect(mTableView, &::widgets::TableView::doubleClicked, selectInvoiceHandle);
}

SalesInvoicesManager::SalesInvoicesManager(QWidget* parent)
	: QDialog {parent}
	, mSaleInvoiceId {0}
{
	core::GlobalData::instance().logger().information("Otwarto menadżera faktur sprzedaży");
	setWindowTitle("Faktury sprzedaży");

	createWidgets();
	createModels();
	createLayouts();
	createContextMenu();
	createConnections();

	// set unchecked filters
	mSaleDate->stateChanged(0);
	mIssueDate->stateChanged(0);
	mSum->stateChanged(0);
	mBufferFilter->setChecked(true);
}

SalesInvoicesManager::~SalesInvoicesManager() {
	core::GlobalData::instance().logger().information("Zamknięto menadżera faktur sprzedaży");
}

void SalesInvoicesManager::createWidgets()
{
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton("Dodaj", QKeySequence("Ins"), this, QIcon(":/icons/add"));
	mEditButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mPrintButton = new ::widgets::PushButton(tr("Drukuj"), QKeySequence("Ctrl+P"), this, QIcon(":/icons/print"));
	mCloseButton = new ::widgets::PushButton("Zamknij", QKeySequence("Esc"), this, QIcon(":/icons/exit"));

	mSaleDate = new QCheckBox("Data sprzedaży", this);
	mSaleDateFrom = new QDateEdit(this);
	mSaleDateTo = new QDateEdit(this);

	mIssueDate = new QCheckBox("Data wystawienia", this);
	mIssueDateFrom = new QDateEdit(this);
	mIssueDateTo = new QDateEdit(this);

	mSum = new QCheckBox("Kwota", this);
	mSumFrom = new QDoubleSpinBox(this);
	mSumTo = new QDoubleSpinBox(this);

	mBufferFilter = new QCheckBox(tr("Bufor"), this);
	mBufferFilter->setToolTip(tr("Pokaż dokumenty znajdujące się w boforze"));

	for (auto& field : findChildren<QDateEdit*>()) {
		field->setCalendarPopup(true);
		field->setDate(QDate::currentDate());
	}

	for (auto& field : findChildren<QDoubleSpinBox*>()) {
		field->setMaximum(1'000'000);
	}

	mWarehouseName = new QLineEdit(this);
	mContractorName = new QLineEdit(this);
}

void SalesInvoicesManager::createLayouts()
{
	QGridLayout* filtersLayout = new QGridLayout();
	filtersLayout->addWidget(mSaleDate, 0, 0);
	filtersLayout->addWidget(new QLabel("Od", this), 0, 1);
	filtersLayout->addWidget(mSaleDateFrom, 0, 2);
	filtersLayout->addWidget(new QLabel("Do", this), 0, 3);
	filtersLayout->addWidget(mSaleDateTo, 0, 4);

	filtersLayout->addWidget(mIssueDate, 1, 0);
	filtersLayout->addWidget(new QLabel("Od", this), 1, 1);
	filtersLayout->addWidget(mIssueDateFrom, 1, 2);
	filtersLayout->addWidget(new QLabel("Do", this), 1, 3);
	filtersLayout->addWidget(mIssueDateTo, 1, 4);

	filtersLayout->addWidget(mSum, 2, 0);
	filtersLayout->addWidget(new QLabel("Od", this), 2, 1);
	filtersLayout->addWidget(mSumFrom, 2, 2);
	filtersLayout->addWidget(new QLabel("Do", this), 2, 3);
	filtersLayout->addWidget(mSumTo, 2, 4);

	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mEditButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mPrintButton);
	buttonsLayout->addStretch(4);
	buttonsLayout->addWidget(mCloseButton);

	QFormLayout* textFiltersLayout = new QFormLayout;
	textFiltersLayout->addRow("Magazyn", mWarehouseName);
	textFiltersLayout->addRow("Kontrahent", mContractorName);

	QVBoxLayout* additionalCheckLayout = new QVBoxLayout();
	additionalCheckLayout->addWidget(mBufferFilter, 0, Qt::AlignTop);

	QHBoxLayout* filtersHorlizontalLayout = new QHBoxLayout();
	filtersHorlizontalLayout->addLayout(filtersLayout);
	filtersHorlizontalLayout->addSpacing(36);
	filtersHorlizontalLayout->addLayout(additionalCheckLayout);
	filtersHorlizontalLayout->addStretch(1);
	filtersHorlizontalLayout->addLayout(textFiltersLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTableView);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(filtersHorlizontalLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void SalesInvoicesManager::createConnections()
{
	connect(mCloseButton, &::widgets::PushButton::clicked, this, &SalesInvoicesManager::close);
	connect(mSaleDate, &QCheckBox::stateChanged, this, [this](int state){
		mSaleDateFrom->setEnabled(state);
		mSaleDateTo->setEnabled(state);
	});
	connect(mIssueDate, &QCheckBox::stateChanged, this, [this](int state){
		mIssueDateFrom->setEnabled(state);
		mIssueDateTo->setEnabled(state);
	});
	connect(mSum, &QCheckBox::stateChanged, this, [this](int state){
		mSumFrom->setEnabled(state);
		mSumTo->setEnabled(state);
	});
	connect(mAddButton, &::widgets::PushButton::clicked, this, &SalesInvoicesManager::addInvoiceHandle);
	connect(mEditButton, &::widgets::PushButton::clicked, this, &SalesInvoicesManager::editInvoiceHandle);

	// connecting filter widgets to slots in model
	connect(mSaleDate, &QCheckBox::stateChanged, this, [&](int checked){
		mInvoicesFilter->setDateOfSaleFilter(checked, mSaleDateFrom->date(), mSaleDateTo->date());
	});
	connect(mSaleDateTo, &QDateEdit::dateChanged, mSaleDateFrom, &QDateEdit::dateChanged);
	connect(mSaleDateFrom, &QDateEdit::dateChanged, this, [&](const QDate&){
		mInvoicesFilter->setDateOfSaleFilter(mSaleDate->isChecked(), mSaleDateFrom->date(), mSaleDateTo->date());
	});

	// for sorting by issue date
	connect(mIssueDate, &QCheckBox::stateChanged, this, [&](int checked){
		mInvoicesFilter->setDateOfIssueFilter(checked, mIssueDateFrom->date(), mIssueDateTo->date());
	});
	connect(mIssueDateTo, &QDateEdit::dateChanged, mIssueDateFrom, &QDateEdit::dateChanged);
	connect(mIssueDateFrom, &QDateEdit::dateChanged, this, [&](const QDate&){
		mInvoicesFilter->setDateOfIssueFilter(mIssueDate->isChecked(), mIssueDateFrom->date(), mIssueDateTo->date());
	});
	connect(mSum, &QCheckBox::stateChanged, this, [&](int checked){
		mInvoicesFilter->setValueFilter(checked, mSumFrom->value(), mSumTo->value());
	});
	connect(mSumFrom, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double){
		mInvoicesFilter->setValueFilter(mSum->isChecked(), mSumFrom->value(), mSumTo->value());
	});
	connect(mSumTo, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double){
		mInvoicesFilter->setValueFilter(mSum->isChecked(), mSumFrom->value(), mSumTo->value());
	});

	connect(mContractorName, &QLineEdit::textChanged, mInvoicesFilter, &filters::sales::SaleInvoicesFilter::setContractorNameFilter);
	connect(mWarehouseName, &QLineEdit::textChanged, mInvoicesFilter, &filters::sales::SaleInvoicesFilter::setWarehouseNameValue);
	connect(mPrintButton, &::widgets::PushButton::clicked, this, &SalesInvoicesManager::printInvoiceHandle);
	connect(mBufferFilter, &QCheckBox::clicked, mInvoicesFilter, &filters::sales::SaleInvoicesFilter::SaleInvoicesFilter::setBufferFilterEnabled);
	connect(mEditAction, &QAction::triggered, this, &SalesInvoicesManager::editInvoiceHandle);
	connect(mPrintAction, &QAction::triggered, this, &SalesInvoicesManager::printInvoiceHandle);
	connect(mPostAction, &QAction::triggered, this, &SalesInvoicesManager::postSelectedInvoices);
	connect(mMarkAsPaidAction, &QAction::triggered, this, &SalesInvoicesManager::markSelectedAsPaid);
	connect(mShowCorrectionDocumentAction, &QAction::triggered, this, &SalesInvoicesManager::showCorrectionDocumentHandle);
	connect(mTableView, &::widgets::TableView::contextMenuShowed, this, &SalesInvoicesManager::fitActionInContextMenu);
}

void SalesInvoicesManager::createModels()
{
	mInvoices = new models::sales::InvoicesModel(this);
	mInvoicesFilter = new filters::sales::SaleInvoicesFilter(this);

	mInvoicesFilter->setSourceModel(mInvoices);
	mTableView->setModel(mInvoicesFilter);
	mTableView->setItemDelegate(new eSawmill::invoices::delegates::sales::SaleInvoicesDelegate(this));

	// creating completers for contractor and warehouse fields
	QCompleter* contractorCompleter = new QCompleter(this);
	contractorCompleter->setCompletionColumn(models::sales::InvoicesModel::Columns::Contractor);
	contractorCompleter->setModel(mInvoicesFilter);
	contractorCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	contractorCompleter->setCompletionRole(Qt::DisplayRole);
	contractorCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mContractorName->setCompleter(contractorCompleter);

	QCompleter* warehouseCompleter = new QCompleter(this);
	warehouseCompleter->setCompletionColumn(models::sales::InvoicesModel::Columns::Warehouse);
	warehouseCompleter->setModel(mInvoicesFilter);
	warehouseCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	warehouseCompleter->setCompletionRole(Qt::DisplayRole);
	warehouseCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mWarehouseName->setCompleter(warehouseCompleter);
}

void SalesInvoicesManager::createContextMenu()
{
	QMenu* contextMenu = new QMenu(this);
	mTableView->setContextMenu(contextMenu);

	mEditAction = new QAction(QIcon(":/icons/edit"), tr("Popraw"), this);
	mPrintAction = new QAction(QIcon(":/icons/print"), tr("Drukuj"), this);
	mPostAction = new QAction(QIcon(":/icons/bolt"), tr("Zaksięguj"), this);
	mMarkAsPaidAction = new QAction(QIcon(":/icons/pay-hand"), tr("Oznacz jako opłacone"), this);
	mShowCorrectionDocumentAction = new QAction(QIcon(":/icons/invoice"), tr("Pokaż dokument korekty"), this);

	contextMenu->addAction(mEditAction);
	contextMenu->addAction(mPrintAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mPostAction);
	contextMenu->addAction(mMarkAsPaidAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mShowCorrectionDocumentAction);
}

void SalesInvoicesManager::applyChangesInWarehouses(const core::SaleInvoiceItem& item)
{
	try {
		auto& repository = core::GlobalData::instance().repository().articleRepository();
		core::Article article = repository.getArticle(item.articleId());
		double qty = article.quantity();
		qty -= item.quantity();
		article.setQuantity(qty);
		repository.updateArticle(article);
	}
	catch (const QString& msg) {
		::widgets::MessageBox::critical(this, tr("Błąd"), tr("Podczas aktuaizacji stanów magazynowych wystąpiły błędy"), msg);
		return;
	}
}

void SalesInvoicesManager::addInvoiceHandle() {
	std::unique_ptr<AddSaleInvoice> addInvoice = std::make_unique<AddSaleInvoice>(this);
	if (addInvoice->exec() == AddSaleInvoice::Accepted) {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		auto invoice = addInvoice->invoice();
		if (repository.createSaleInvoice(invoice)) {
			if (addInvoice->isCorrectionInvoice()) {
				repository.markSaleInvoiceAsCorrectionDocument(invoice.id(), addInvoice->correctionInvoiceId());
			}
		} else {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Podczas tworzenia nowego dokumentu sprzedaży wystąpiły nieoczekiwane błędy.\n"
				   "Zmiany w bazie nie zostały wprowadzone!"));
		}
		mInvoices->get();
	}
}

void SalesInvoicesManager::editInvoiceHandle() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Przed edycją dokumentu musisz wybrać dokument do edycji na liście"));
		return;
	}
	try {
		core::eloquent::DBIDKey id = mTableView->currentIndex().data(eSawmill::invoices::models::sales::InvoicesModel::Roles::Id).toUInt();
		SalesInvoicesManager::editInvoice(id, this);
		mInvoices->get();
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Wybrana faktura sprzedaży jest w tym momencie edytowana przez innego użytkownika,"
			   "Poczekaj na zakończenie edycji"));
	} catch (std::exception& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::critical(this, tr("Błąd"),
			tr("Podczas edycji dokumentu wystąpiły błędy!\n"
			   "Zmiany w bazie nie zostały zapisane."));
	}
}

void SalesInvoicesManager::editInvoice(int documentId, QWidget* parent) {
	if (documentId <= 0)
		return;
	try {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		core::GlobalData::instance().logger()
			.information(QString::asprintf("Rozpoczęto edycję faktury sprzedaży o id:%d ", documentId));

		core::SaleInvoice si = repository.getSaleInvoice(documentId);
		if (repository.hasSaleInvoiceCorrection(documentId)) {
			::widgets::MessageBox::information(parent, tr("Informacja"),
					tr("Dokument jest nieaktualny, dla tego dokumentu wystawiono dokument korekty"));
			return;
		}

		if (!si.inBuffer()) {
			::widgets::MessageBox::information(parent, tr("Informacja"),
					tr("Dokument znajduje się poza buforem.\n"
					   "Edycja dokumętu nie jesst możliwa"));
			return;
		}

		if (!repository.lockSaleInvoice(documentId))
			return;

		std::unique_ptr<AddSaleInvoice> invoiceDlg = std::make_unique<AddSaleInvoice>(si, parent);
		if (invoiceDlg->exec() == QDialog::Accepted) {
			// get edited invoice by user and in next step store information in db
			core::SaleInvoice invoice = invoiceDlg->invoice();
			const QList<core::SaleInvoiceItem> items = invoiceDlg->invoiceItems();
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			if (!repository.updateSaleInvoice(invoice)) {
				throw std::exception();
			}
		}
		if (!repository.unlockSaleInvoice(documentId))
			return;
	} catch (...) {
		throw;
	}
}

void SalesInvoicesManager::handleRepositoryNotification(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::SaleInvoice) {
		const auto& selection = mTableView->selectionModel()->selection();
		mInvoices->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void SalesInvoicesManager::printInvoiceHandle()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this,
			tr("Drukowanie dokumentu"),
			tr("Nie zaznaczono żadnego dokumentu do druku"));
		return;
	}
	try {
		QApplication::setOverrideCursor(Qt::WaitCursor);
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		core::eloquent::DBIDKey id = mTableView->currentIndex().data(models::sales::InvoicesModel::Roles::Id).toInt();

		if (!repository.lockSaleInvoice(id))
			return;
		const core::SaleInvoice invoice = repository.getSaleInvoice(id);

		std::unique_ptr<core::documents::SaleInvoiceDocument> document = std::make_unique<core::documents::SaleInvoiceDocument>(this);
		std::unique_ptr<QPrinter> printer = std::make_unique<QPrinter>(QPrinter::HighResolution);
		printer->setCreator("eSawmillERP");

		core::Company company = core::GlobalData::instance().repository().userRepository().autorizeUser().company();

		document->setDocumentNumber(invoice.number());
		document->setPayment(invoice.payment());
		document->setPaymentDays(invoice.paymentDeadlineDays());
		document->setPaymentDate(invoice.paymentDeadlineDate());
		document->setInvoiceItems(repository.getSaleInvoiceItems(invoice.id()));
		document->setBuyer(invoice.contractor());
		document->setSeller(core::Contractor::fromCompany(company));
		document->setPlaceOfIssue(company.contact().get("City").toString());
		document->setDateOfIsse(invoice.dateOfIssue());
		document->setDateOfService(invoice.dateOfSale());

		if (document->generate()) {
			std::unique_ptr<QPrintPreviewDialog> dialog = std::make_unique<QPrintPreviewDialog>(printer.get(), this);
			connect(dialog.get(), &QPrintPreviewDialog::paintRequested, this, [&document](QPrinter* printer){
				document->print(printer);
			});

			dialog->setWindowTitle(tr("Wydruk faktury sprzedaży"));
			QApplication::restoreOverrideCursor();
			dialog->exec();
		} else {
			::widgets::MessageBox::critical(this, tr("Błąd generowania dokumentu"),
					tr("Dokument sprzedaży nie mógł być wygenerowany do druku."));
		}
		if (!repository.unlockSaleInvoice(id))
			return;
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		QApplication::restoreOverrideCursor();
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Aktualnie wybrana faktura sprzedaży jest edytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	} catch (...) {
		QApplication::restoreOverrideCursor();
		core::GlobalData::instance().logger().warning(
			QString::asprintf("Nieprzechwycony wyjątek w %s", Q_FUNC_INFO)
		);
		qCritical() << "Nieprzechwycony wyjątek w: " << Q_FUNC_INFO;
		Q_ASSERT(false);
	}
}

void SalesInvoicesManager::markSelectedAsPaid() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie zaznaczono żadnych dokumentów!\n"
			   "Operacja nie może być wykonana"));
		return;
	}

	try {
		const QModelIndexList rows = mTableView->selectionModel()->selectedRows(models::sales::InvoicesModel::Columns::Status);
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
				tr("Zaznaczono %1 dokumenty.\n"
				   "Czy chcesz oznaczyć zaznaczone dokumenty jako opłacone?\n"
				   "UWAGA: Operacja nie może być cofnięta").arg(rows.size()),
				"", QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::No)
			return;

		for (const QModelIndex& row : rows) {
			mInvoices->setData(row, true, Qt::EditRole);
		}
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Aktualnie wybrana faktura sprzedaży jest w tym momencie edytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	}
}

void SalesInvoicesManager::postSelectedInvoices() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie zaznaczono żadnego dokumentu do zaksięgowania!"));
		return;
	}

	const QModelIndexList selectedRows = mTableView->selectionModel()->selectedRows();
	int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Zaznaczono %1 dokumenty.\n"
			   "Czy chcesz zaksięgować zaznaczone dokumenty?\n"
			   "UWAGA: Operacje nie może być cofnięta").arg(selectedRows.size()),
			"", QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::No)
		return;
	try {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		for (QModelIndex row : selectedRows) {
			const int id = row.data(models::sales::InvoicesModel::Roles::Id).toInt();
			repository.lockSaleInvoice(id);
			core::SaleInvoice invoice = repository.getSaleInvoice(id);
			if (invoice.items().size() <= 0) {
				::widgets::MessageBox::information(this, tr("Informacja"),
						tr("Dokument %1 nie może być zaksięgowany ponieważ nie posiada żadnych artykułów.\n").arg(invoice.number()));
				continue;
			}
			// at end post invoice
			if (!repository.postSaleInvoice(invoice.id())) {
				::widgets::MessageBox::critical(this, tr("Informacja"),
					tr("Podczas księgowania dokumentu %1 wystąpił błąd!").arg(invoice.number()));
			}
			repository.unlockSaleInvoice(id);
		}
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Wybrana faktura sprzedaży jest w tym momencie dytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	}

	mInvoices->get();
}

void SalesInvoicesManager::showCorrectionDocumentHandle() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Przed wykonaniem operacji konieczne jest wybranie dokumentu na liście do którego ma zostać wykonana operacja"));
		return;
	}

	const int id = mTableView->selectionModel()->currentIndex().data(eSawmill::invoices::models::sales::InvoicesModel::Roles::Id).toInt();
	auto& repository = core::GlobalData::instance().repository().invoiceRepository();
	core::SaleInvoice saleInvoice = repository.getSaleInvoice(id);
	if (repository.hasSaleInvoiceCorrection(id)) {
		try {
			editInvoice(repository.getCorrectionForSaleInvoice(id));
		} catch (core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
				tr("Faktura korekty jest w tym momencie edytowana przez innego użytkownika,"
				   "poczekaj na zakończenie edycji"));
		}
	}
	else {
		::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Dla dokumętu %1 nie ma wystawionych dokumentów korekty").arg(saleInvoice.number()));
	}
}

void SalesInvoicesManager::fitActionInContextMenu() {
	if (mTableView->selectionModel()->hasSelection()) {
		// user select row in table view
		//
		const QModelIndex currentRow = mTableView->selectionModel()->currentIndex();
		mEditAction->setEnabled(currentRow.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::BufferStatus).toBool());
		mPrintAction->setEnabled(true);
		mPostAction->setEnabled(currentRow.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::BufferStatus).toBool());
		mMarkAsPaidAction->setEnabled(!currentRow.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::PaidStatus).toBool());
		mShowCorrectionDocumentAction->setEnabled(currentRow.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::HasCorrectionDocument).toBool());
	}
	else {
		// user didn't select row in view
		//
		mEditAction->setDisabled(true);
		mPrintAction->setDisabled(true);
		mPostAction->setDisabled(true);
		mMarkAsPaidAction->setDisabled(true);
		mShowCorrectionDocumentAction->setDisabled(true);
	}
}

} // namespace invoices
