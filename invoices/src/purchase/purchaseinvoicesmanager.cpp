#include "purchase/purchaseinvoicesmanager.hpp"
#include <memory>
#include <QDebug>
#include <QLabel>
#include <QGridLayout>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include "purchase/addpurchaseinvoice.hpp"
#include <dbo/purchaseinvoice.hpp>
#include "delegates/purchases/purchaseinvoicesdelegate.hpp"
#include <messagebox.h>
#include <exceptions/repositoryexception.hpp>
#include <QDebug>
#include <QCompleter>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <memory>
#include <dbo/auth.h>
#include <documents/purchaseinvoicedocument.hpp>
#include <dbo/purchaseinvoicecorrection.hpp>
#include <settings/appsettings.hpp>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>


namespace eSawmill::invoices::purchase {

PurchaseInvoiceManager::PurchaseInvoiceManager(bool selectedMode, QWidget* parent)
	: PurchaseInvoiceManager {parent}
{
	mSelectedMode = selectedMode;
	mSelectedInvoiceId = 0;

	setWindowTitle(tr("Wybór faktury zakupu"));

	mAddButton->setDisabled(true);
	mPrintButton->setDisabled(true);
	mEditButton->setText(tr("Wybierz"));
	mInvoicesModel->setReadOnly(true);

	auto selectInvoice = [&]()->void {
		if (!mTableView->selectionModel()->hasSelection()) {
			::widgets::MessageBox::information(this, tr("Informacja"),
					tr("Nie zaznaczono na liście żadnego dokumentu"));
			return;
		}

		mSelectedInvoiceId = mTableView->currentIndex().data(models::purchases::InvoicesModel::Roles::Id).toInt();
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		const core::PurchaseInvoice invoice = repository.getPurchaseInvoice(mSelectedInvoiceId);
		if (repository.hasPurchaseInvoiceCorrection(mSelectedInvoiceId)) {
			::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
					tr("Wybrany dokument posiada dokument korekty"));
			return;
		}

		if (invoice.inBuffer()) {
			::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
					tr("Dokument znajduje się w buforze"));
			return;
		}
		accept();
	};

	disconnect(mEditButton, &::widgets::PushButton::clicked, this, &PurchaseInvoiceManager::editInvoideHandle);
	connect(mEditButton, &::widgets::PushButton::clicked, this, selectInvoice);
	connect(mTableView, &::widgets::TableView::doubleClicked, this, selectInvoice);
}

PurchaseInvoiceManager::PurchaseInvoiceManager(QWidget* parent)
	: QDialog {parent}
{
	core::GlobalData::instance().logger().information("Otwarto menadżera faktur zakupu");
	setWindowTitle("Faktury zakupu");

	createWidgets();
	createLayouts();
	createModels();
	createContrextMenu();
	createConnections();

	mDateOfPurchase->stateChanged(Qt::Unchecked);
	mDateOfIssue->stateChanged(Qt::Unchecked);
	mTotalPrice->stateChanged(Qt::Unchecked);
	mBufferFilter->setChecked(true);
}

PurchaseInvoiceManager::~PurchaseInvoiceManager() {
	core::GlobalData::instance().logger().information("Zamknięto menadżera faktur zakupu");
}

void PurchaseInvoiceManager::handleRepositoryNotification(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::PurchaseInvoice) {
		const auto& selection = mTableView->selectionModel()->selection();
		mInvoicesModel->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void PurchaseInvoiceManager::createWidgets()
{
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("Ins"), this, QIcon(":/icons/add"));
	mEditButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mPrintButton = new ::widgets::PushButton(tr("Drukuj"), QKeySequence("Ctrl+P"), this, QIcon(":/icons/print"));
	mCloseButton = new ::widgets::PushButton(tr("Zakończ"), QKeySequence("Esc"), this, QIcon(":/icons/exit"));


	mDateOfPurchase = new QCheckBox("Data zakupu", this);
	mDateOfPurchaseFrom = new QDateEdit(this);
	mDateOfPurchaseTo = new QDateEdit(this);

	mBufferFilter = new QCheckBox(tr("Bufor"), this);
	mBufferFilter->setToolTip(tr("Pokaż tylo dokumenty znajdujące się w buforze"));

	mDateOfIssue = new QCheckBox("Data wystawienia", this);
	mDateOfIssueFrom = new QDateEdit(this);
	mDateOfIssueTo = new QDateEdit(this);

	mTotalPrice = new QCheckBox("Kwota", this);
	mTotalPriceFrom = new QDoubleSpinBox(this);
	mTotalPriceTo = new QDoubleSpinBox(this);

	mWarehouse = new QLineEdit(this);
	mContractor = new QLineEdit(this);


	for (auto& field : findChildren<QDoubleSpinBox*>())
		field->setMaximum(1'000'000);

	for (auto& field : findChildren<QDateEdit*>()) {
		field->setCalendarPopup(true);
		field->setDate(QDate::currentDate());
	}
}

void PurchaseInvoiceManager::createLayouts() {
	QGridLayout* filterOptionLayout = new QGridLayout();
	filterOptionLayout->addWidget(mDateOfPurchase, 0, 0);
	filterOptionLayout->addWidget(new QLabel("Od"), 0, 1);
	filterOptionLayout->addWidget(mDateOfPurchaseFrom, 0, 2);
	filterOptionLayout->addWidget(new QLabel("Do", this), 0, 3);
	filterOptionLayout->addWidget(mDateOfPurchaseTo, 0, 4);

	filterOptionLayout->addWidget(mDateOfIssue, 1, 0);
	filterOptionLayout->addWidget(new QLabel("Od", this), 1, 1);
	filterOptionLayout->addWidget(mDateOfIssueFrom, 1, 2);
	filterOptionLayout->addWidget(new QLabel("Do", this), 1, 3);
	filterOptionLayout->addWidget(mDateOfIssueTo, 1, 4);

	filterOptionLayout->addWidget(mTotalPrice, 2, 0);
	filterOptionLayout->addWidget(new QLabel("Od", this), 2, 1);
	filterOptionLayout->addWidget(mTotalPriceFrom, 2, 2);
	filterOptionLayout->addWidget(new QLabel("Do", this), 2, 3);
	filterOptionLayout->addWidget(mTotalPriceTo, 2, 4);

	QFormLayout* filterWarehouseLayout = new QFormLayout();
	filterWarehouseLayout->addRow("Magazyn", mWarehouse);
	filterWarehouseLayout->addRow("Kontrahent", mContractor);

	QVBoxLayout* additionalCheckFilterLayout = new QVBoxLayout();
	additionalCheckFilterLayout->addWidget(mBufferFilter, 0, Qt::AlignTop);

	QHBoxLayout* filtersLayout = new QHBoxLayout();
	filtersLayout->addLayout(filterOptionLayout);
	filtersLayout->addSpacing(36);
	filtersLayout->addLayout(additionalCheckFilterLayout);
	filtersLayout->addStretch(1);
	filtersLayout->addLayout(filterWarehouseLayout);

	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mEditButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mPrintButton);
	buttonsLayout->addStretch(4);
	buttonsLayout->addWidget(mCloseButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTableView);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(filtersLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void PurchaseInvoiceManager::createConnections() {
	connect(mDateOfPurchase, &QCheckBox::stateChanged, this, [this](int state){
		mDateOfPurchaseFrom->setEnabled(state);
		mDateOfPurchaseTo->setEnabled(state);
	});
	connect(mDateOfIssue, &QCheckBox::stateChanged, this, [this](int state){
		mDateOfIssueFrom->setEnabled(state);
		mDateOfIssueTo->setEnabled(state);
	});
	connect(mTotalPrice, &QCheckBox::stateChanged, this, [this](int state){
		mTotalPriceFrom->setEnabled(state);
		mTotalPriceTo->setEnabled(state);
	});
	connect(mCloseButton, &::widgets::PushButton::clicked, this, &PurchaseInvoiceManager::close);
	connect(mAddButton, &::widgets::PushButton::clicked, this, &PurchaseInvoiceManager::addInvoiceHandle);
	connect(mEditButton, &::widgets::PushButton::clicked, this, &PurchaseInvoiceManager::editInvoideHandle);
	connect(mWarehouse, &QLineEdit::textChanged, this, [this](const QString& name){
		mInvoicesProxyModel->setWarehouseName(name);
	});
	connect(mContractor, &QLineEdit::textChanged, this, [this](const QString& contractor){
		mInvoicesProxyModel->setContractorName(contractor);
	});
	connect(mDateOfPurchase, &QCheckBox::stateChanged, this, [this](int){
		filterByPurchaseDate();
	});
	connect(mDateOfPurchaseFrom, &QDateEdit::dateChanged, this, [this](const QDate&){
		filterByPurchaseDate();
	});
	connect(mDateOfPurchaseTo, &QDateEdit::dateChanged, this, [this](const QDate&){
		filterByIssueDate();
	});

	connect(mDateOfIssue, &QCheckBox::stateChanged, this, [this](int){
		filterByIssueDate();
	});
	connect(mDateOfIssueFrom, &QDateEdit::dateChanged, this, [this](const QDate&){
		filterByIssueDate();
	});
	connect(mDateOfIssueTo, &QDateEdit::dateChanged, this, [this](const QDate&){
		filterByIssueDate();
	});

	connect(mTotalPrice, &QCheckBox::stateChanged, this, [this](int){
		filterByValue();
	});
	connect(mTotalPriceFrom, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double){
		filterByValue();
	});
	connect(mTotalPriceTo, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double){
		filterByValue();
	});

	connect(mPrintButton, &::widgets::PushButton::clicked, this, &PurchaseInvoiceManager::printInvoiceHandle);
	connect(mBufferFilter, &QCheckBox::clicked, mInvoicesProxyModel, &filters::purchases::PurchaseInvoicesFilter::setBufferFilter);
	connect(mEditAction, &QAction::triggered, this, &PurchaseInvoiceManager::editInvoideHandle);
	connect(mPrintAction, &QAction::triggered, this, &PurchaseInvoiceManager::printInvoiceHandle);
	connect(mPostAction, &QAction::triggered, this, &PurchaseInvoiceManager::postSelectedInvoices);
	connect(mMarkAsPaidAction, &QAction::triggered, this, &PurchaseInvoiceManager::markInvoicesAsPaid);
	connect(mShowCorrectionDocumentAction, &QAction::triggered, this, &PurchaseInvoiceManager::showCorrectionDocument);
	connect(mTableView, &::widgets::TableView::contextMenuShowed, this, &PurchaseInvoiceManager::fitContextMenuToSelectedOption);
}

void PurchaseInvoiceManager::createModels()
{
	mInvoicesModel = new eSawmill::invoices::models::purchases::InvoicesModel(this);
	mInvoicesProxyModel = new eSawmill::invoices::filters::purchases::PurchaseInvoicesFilter(this);
	mInvoicesProxyModel->setSourceModel(mInvoicesModel);

	mTableView->setModel(mInvoicesProxyModel);
	mTableView->setItemDelegate(new eSawmill::invoices::delegates::purchases::PurchaseInvoicesDelegate(this));

	// adding completers for warehouses and contractors
	QCompleter* warehouseCompleter = new QCompleter(this);
	warehouseCompleter->setModel(mInvoicesProxyModel);
	warehouseCompleter->setCompletionColumn(eSawmill::invoices::models::purchases::InvoicesModel::Columns::Warehouse);
	warehouseCompleter->setCompletionRole(Qt::DisplayRole);
	warehouseCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	warehouseCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mWarehouse->setCompleter(warehouseCompleter);

	QCompleter* contractorCompleter = new QCompleter(this);
	contractorCompleter->setModel(mInvoicesProxyModel);
	contractorCompleter->setCompletionColumn(eSawmill::invoices::models::purchases::InvoicesModel::Columns::Contractor);
	contractorCompleter->setCompletionRole(Qt::DisplayRole);
	contractorCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	warehouseCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mContractor->setCompleter(contractorCompleter);
}

void PurchaseInvoiceManager::createContrextMenu()
{
	mEditAction = new QAction(QIcon(":/icons/edit"), tr("Popraw"), this);
	mPrintAction = new QAction(QIcon(":/icons/print"), tr("Drukuj"), this);
	mPostAction = new QAction(QIcon(":/icons/bolt"), tr("Zaksięguj"), this);
	mMarkAsPaidAction = new QAction(QIcon(":/icons/pay-hand"), tr("Oznacz jako opłacone"), this);
	mShowCorrectionDocumentAction = new QAction(QIcon(":/icons/invoice"), tr("Pokaż dokument korekty"), this);

	QMenu* contextMenu = new QMenu(this);
	mTableView->setContextMenu(contextMenu);

	contextMenu->addAction(mEditAction);
	contextMenu->addAction(mPrintAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mPostAction);
	contextMenu->addAction(mMarkAsPaidAction);
	contextMenu->addSeparator();
	contextMenu->addAction(mShowCorrectionDocumentAction);
}

void PurchaseInvoiceManager::addInvoiceHandle() {
	core::GlobalData::instance().logger().information("Rozpoczęto proces tworzenia nowej faktury zakupu");
	std::unique_ptr<eSawmill::invoices::purchase::AddPurchaseInvoice> addInvoice
			= std::make_unique<eSawmill::invoices::purchase::AddPurchaseInvoice>(this);

	if (addInvoice->exec() == eSawmill::invoices::purchase::AddPurchaseInvoice::Accepted) {
		try {
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			auto invoice = addInvoice->purchaseInvoice();
			if (!repository.createPurchaseInvoice(invoice)) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Podczas tworzenia nowej faktury zakupu wystąpiły nieoczekiwane błędy!\n"
					   "Zmiany w bazie nie zostały wprowadzone."));
			}
			mInvoicesModel->get();
		} catch (...) {
			core::GlobalData::instance().logger().warning("Błąd podczas tworzenia nowej faktury zakupu");
		}
	}
	core::GlobalData::instance().logger().information("Zakończono proces tworzenia faktury zakupu");
}

void PurchaseInvoiceManager::editInvoideHandle() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, "Informajca",
			"Przed edycją dokumętu muszisz wybrać z listy który dokumnet chcesz edytować.");
		return;
	}
	try {
		int id = mTableView->currentIndex().data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::Id).toUInt();
		mInvoicesModel->startInsertData();
		editInvoice(id);
		mInvoicesModel->get();
		mInvoicesModel->endInsertData();
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Wybrana faktura zakupu jest obecnie edytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	} catch (core::exceptions::RepositoryException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::critical(this,
			tr("Błąd odczytywania danych"),
			tr("Podczas odczytu danych doszło do błędu. Uruchom aplikację ponownie oraz powtórz próbę edycji faktury zakupu"));

		// save info into log file
		core::GlobalData::instance()
			.logger().critical(QString::asprintf(
				"Wyjątek core::exceptions::RepositoryException w %s (what: %s)", Q_FUNC_INFO, ex.what())
		);
	} catch (...) {
		qCritical() << "Nieprzechwycony wyjątek w " << Q_FUNC_INFO;
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Nieprzechwycony wyjątek w %s", Q_FUNC_INFO)
		);
	}
}

void PurchaseInvoiceManager::editInvoice(core::eloquent::DBIDKey invoiceId) {
	if (invoiceId <= 0)
		return;

	try {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		core::PurchaseInvoice pi = repository.getPurchaseInvoice(invoiceId);

		if (repository.hasPurchaseInvoiceCorrection(invoiceId)) {
			::widgets::MessageBox::warning(this, tr("Informacja"),
				tr("Dokument jest nieaktualny, dla tego dokumentu wystawiono dokument korekty"));
			return;
		}

		if (!pi.inBuffer()) {
			::widgets::MessageBox::information(this, "Informacja",
				"Dokument znajduje się poza buforem.\n"
				"Edycja dokumęntu nie jest możliwa!");
			return;
		}

		if (!repository.lockPurchaseInvoice(invoiceId))
			return;

		core::GlobalData::instance().logger().information(QString("Rozpoczęto edycję faktury zakupu %1").arg(pi.number()));
		std::unique_ptr<eSawmill::invoices::purchase::AddPurchaseInvoice> editPurchase
				= std::make_unique<eSawmill::invoices::purchase::AddPurchaseInvoice>(pi, this);

		if (editPurchase->exec() == QDialog::Accepted) {
			try {
				auto& repository = core::GlobalData::instance().repository().invoiceRepository();
				auto invoice = editPurchase->purchaseInvoice();
				if (!repository.updatePurchaseInvoice(invoice)) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Podczas aktualizacji faktury zakupu wystąpiły nieoczekiwane błędy.\n"
						   "Zmiany w bazie nie zostały wprowadzone."));
				}
				mInvoicesModel->get();
			} catch (...) {
				core::GlobalData::instance().logger().warning(QString("Błąd podczas edycji faktury zakupu %1").arg(pi.number()));
			}
		}
		if (!repository.unlockPurchaseInvoice(invoiceId))
			return;
		core::GlobalData::instance().logger().information("Zakończono edycję faktury zakupu");
	} catch (...) {
		throw;
	}
}

void PurchaseInvoiceManager::printInvoiceHandle()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Drukowanie dokumentu"),
									   tr("Nie zaznaczono żadnego dokumentu do druku"));
		return;
	}

	try {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		int id = mTableView->currentIndex().data(models::purchases::InvoicesModel::Roles::Id).toInt();
		if (!repository.lockPurchaseInvoice(id))
			return;

		QApplication::setOverrideCursor(Qt::WaitCursor);
		const core::PurchaseInvoice invoice = repository.getPurchaseInvoice(id);
		std::unique_ptr<core::documents::PurchaseInvoiceDocument> document
			= std::make_unique<core::documents::PurchaseInvoiceDocument>(this);

		std::unique_ptr<QPrinter> printer = std::make_unique<QPrinter>(QPrinter::HighResolution);
		printer->setCreator("eSawmillERP");

		core::Company company = core::GlobalData::instance()
			.repository()
			.userRepository()
			.autorizeUser().company();

		document->setDocumentNumber(invoice.number());
		document->setPayment(invoice.payment());
		document->setPaymentDays(invoice.paymentDeadlineDays());
		document->setPaymentDate(invoice.paymentDeadlineDate());
		document->setInvoiceItems(repository.getPurchaseInvoiceItems(invoice.id()));
		document->setBuyer(invoice.contractor());
		document->setSeller(core::Contractor::fromCompany(company));
		document->setPlaceOfIssue(company.contact().get("City").toString());
		document->setDateOfIsse(invoice.dateOfIssue());
		document->setDateOfService(invoice.dateOfPurchase());

		if (document->generate()) {
			std::unique_ptr<QPrintPreviewDialog> dialog = std::make_unique<QPrintPreviewDialog>(printer.get(), this);
			connect(dialog.get(), &QPrintPreviewDialog::paintRequested, this, [&document, &invoice](QPrinter* printer){
				core::GlobalData::instance().logger().information(QString("Drukowanie faktury zakupu %1").arg(invoice.number()));
				document->print(printer);
			});

			dialog->setWindowTitle(tr("Wydruk faktury sprzedaży"));
			QApplication::restoreOverrideCursor();
			dialog->exec();
		} else {
			QApplication::restoreOverrideCursor();
			core::GlobalData::instance().logger().warning(
				QString("Podczas generowania faktury zakupu %1 do druku wystąpiły błędy").arg(invoice.number()));

			::widgets::MessageBox::critical(this, tr("Błąd generowania dokumentu"),
					tr("Dokument sprzedaży nie mógł być wygenerowany do druku."));
			return;
		}
		if (!repository.unlockPurchaseInvoice(id))
			return;
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		QApplication::restoreOverrideCursor();
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Wybrana faktura zakupu jest obecnie edytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	}
}

void PurchaseInvoiceManager::filterByPurchaseDate()
{
	mInvoicesProxyModel->setFilterByPurchaseDateEnabled(
				mDateOfPurchase->isChecked(),
				mDateOfPurchaseFrom->date(),
				mDateOfPurchaseTo->date());
}

void PurchaseInvoiceManager::filterByIssueDate()
{
	mInvoicesProxyModel->setFilterByIssueDate(
				mDateOfIssue->isChecked(),
				mDateOfIssueFrom->date(),
				mDateOfIssueTo->date());
}

void PurchaseInvoiceManager::filterByValue()
{
	mInvoicesProxyModel->setFilterByValueEnabled(
				mTotalPrice->isChecked(),
				mTotalPriceFrom->value(),
				mTotalPriceTo->value());
}

void PurchaseInvoiceManager::markInvoicesAsPaid()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Nie zaznaczono żadnych dokumentów!\n"
			   "Operacja nie może być wykonana"));
		return;
	}

	const QModelIndexList selectedRows = mTableView->selectionModel()->selectedRows(models::purchases::InvoicesModel::Columns::Status);
	int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Zaznaczono %1 dokumentów\n"
			   "Czy na pewno chcesz zmienić status dokumentów na opłacone?\n"
			   "UWAGA: Operacja nie może być cofnięta").arg(selectedRows.size()), "",
			QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::No)
		return;

	for (const QModelIndex& row : selectedRows) {
		// change invoice status by default method in model
		mInvoicesModel->setData(row, true, Qt::EditRole);
	}

	// at end of operation get all records from db by model
	mInvoicesModel->get();
}

void PurchaseInvoiceManager::postSelectedInvoices() {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Nie zaznaczono żadnych dokumentów!\n"
			   "Operacja nie może być wykonana"));
		return;
	}

	const QModelIndexList selectedRows = mTableView->selectionModel()->selectedRows();
	int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Zaznaczono %1 dokumentów. Czy chcesz je zaksięgować?\n"
			   "UWAGA: Operacja nie może być cofnięta!").arg(selectedRows.size()), "",
			QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::No)
		return;

	auto& repository = core::GlobalData::instance().repository().invoiceRepository();
	auto& logger = core::GlobalData::instance().logger();
	for (const QModelIndex& row : selectedRows) {
		try {
			const core::eloquent::DBIDKey id = mInvoicesModel->data(row, models::purchases::InvoicesModel::Roles::Id).toUInt();
			repository.lockPurchaseInvoice(id);
			core::PurchaseInvoice invoice = repository.getPurchaseInvoice(id);
			QList<core::PurchaseInvoiceItem> items = repository.getPurchaseInvoiceItems(invoice.id());
			if (items.size() > 0) {
				auto& articleRepository = core::GlobalData::instance().repository().articleRepository();
				for (const core::PurchaseInvoiceItem& item : items) {
					core::eloquent::DBIDKey articleId = item.articleId();
					if (articleId == 0)
						continue;	// because wood elements not have article id in Articles table
					core::Article article = articleRepository.getArticle(articleId);
					article.setQuantity(item.quantity());
					articleRepository.updateArticle(article);
				}
				logger.information(QString("Zaksięgowano fakturę zakupu %1").arg(invoice.number()));
			} else {
				logger.warning(QString("Faktura %1 nie posiada żadnych artykułów. Przerwano księgowanie").arg(invoice.number()));
				::widgets::MessageBox::information(this, tr("Informacja"),
					tr("Faktura nie może być zaksięgowana ponieważ nie posiada żadnych artykułów"));
				return;
			}

			if (!repository.postPurchaseInvoice(id)) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Dokument %1 nie został zaksięgowany z powodu błędów na serwerze SQL").arg(invoice.number()));
				continue;	// continue posting documents if list has more than one piece
			}
			repository.unlockPurchaseInvoice(id);
		} catch (core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
				tr("Wybrana faktura zakupu jest obecnie edytowana prze innego użytkownika."
				   "Poczekaj na zakończenie edycji"));
		}  catch (...) {
			logger.warning("Podczas księgowania faktur zakupu wystąpiły nieoczekiwane błędy!");
		}
	} // for
	mInvoicesModel->get();
}

void PurchaseInvoiceManager::showCorrectionDocument()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Przed wykonaniem tej operacji konieczne jest zaznaczenie pozycji na liście"));
		return;
	}
	try {
		const core::eloquent::DBIDKey id = mTableView->selectionModel()->currentIndex().data(models::purchases::InvoicesModel::Roles::Id).toInt();
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		const core::PurchaseInvoice invoice = repository.getPurchaseInvoice(id);
		if (!repository.hasPurchaseInvoiceCorrection(id)) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Zaznaczony dokument nie posiada powiązanych dokumentów korekty"));
			return;
		}
		editInvoice(repository.getCorrectionForPurchaseInvoice(id).id());
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Faktura korekty dla wybranego dokumentu jest obecnie edytowana przez innego użytkownika."
			   "Poczekaj na zakończenie edycji"));
	}
}

void PurchaseInvoiceManager::fitContextMenuToSelectedOption()
{
	if (mTableView->selectionModel()->hasSelection()) {
		// user selected row in view
		//
		const QModelIndex selection = mTableView->selectionModel()->currentIndex();
		mEditAction->setEnabled(selection.data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::BufferStatus).toBool());
		mPrintAction->setEnabled(true);
		mPostAction->setEnabled(selection.data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::BufferStatus).toBool());
		mShowCorrectionDocumentAction->setEnabled(selection.data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::HasCorrection).toBool());
		mMarkAsPaidAction->setEnabled(!selection.data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::PaidStatus).toBool()
									  && !selection.data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::HasCorrection).toBool());
	}
	else {
		// user didn't select row in view
		//
		mEditAction->setEnabled(false);
		mPrintAction->setEnabled(false);
		mPostAction->setEnabled(false);
		mMarkAsPaidAction->setEnabled(false);
		mShowCorrectionDocumentAction->setEnabled(false);
	}
}

} // namesapce
