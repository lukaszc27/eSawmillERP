#include "purchase/addpurchaseinvoice.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>

namespace eSawmill::invoices::purchase {

AddPurchaseInvoice::AddPurchaseInvoice(QWidget* parent)
	: QDialog(parent)
	, mInvoideId {0}
{
	setWindowTitle("Dodawanie faktury zakupu");

	createWidgets();
	createLayouts();
	createConnections();
}

AddPurchaseInvoice::AddPurchaseInvoice(core::PurchaseInvoice &invoice, QWidget *parent)
	: AddPurchaseInvoice(parent)
{
	setWindowTitle(QString("Edycja faktury zakupu [%1]").arg(invoice.number()));
	mAcceptButton->setText("Zapisz");
	mBuffer->setChecked(invoice.inBuffer());

	mGeneralWidget->loadInvoice(invoice);
	mInvoideId = invoice.id();
}

AddPurchaseInvoice::~AddPurchaseInvoice()
{
}

core::PurchaseInvoice AddPurchaseInvoice::purchaseInvoice() const {
	core::PurchaseInvoice purchaseInvoice = mGeneralWidget->purchaseInvoice();	// get created object in general group
	purchaseInvoice.existInBuffer(mBuffer->isChecked());
	purchaseInvoice.set("Id", mInvoideId);

	return purchaseInvoice;
}

void AddPurchaseInvoice::createWidgets()
{
	mTabWidget = new QTabWidget(this);
	mAcceptButton = new ::widgets::PushButton("Dodaj", QKeySequence("F10"), this, QIcon(":/icons/accept"));
	mRejectButton = new ::widgets::PushButton("Anuluj", QKeySequence("ESC"), this, QIcon(":/icons/reject"));
	mBuffer = new QCheckBox("Bufor", this);
	mBuffer->setToolTip("Czy dokument znajduje się w buforze");
	mBuffer->setChecked(true);

	mGeneralWidget = new eSawmill::invoices::widgets::purchase::PurchaseGeneralWidget(this);
	mTabWidget->addTab(mGeneralWidget, "O&gólne");
}

void AddPurchaseInvoice::createLayouts()
{
	QLabel* titleLabel = new QLabel("Faktura zakupu", this);
	QFont font;
	font.setPixelSize(18);
	titleLabel->setFont(font);

	QHBoxLayout* optionDocLayout = new QHBoxLayout();
	optionDocLayout->addWidget(titleLabel);
	optionDocLayout->addStretch(1);
	optionDocLayout->addWidget(mBuffer);

	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);


	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(optionDocLayout);
	mainLayout->addSpacing(3);
	mainLayout->addWidget(mTabWidget);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(buttonsLayout);
}

void AddPurchaseInvoice::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &AddPurchaseInvoice::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &AddPurchaseInvoice::reject);
}

}
