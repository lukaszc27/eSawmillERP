#include "filters/purchases/purchaseinvoicesfilter.hpp"
#include "models/purchases/purchaseinvoicesmodel.hpp"
#include <QDebug>


namespace eSawmill::invoices::filters::purchases {

PurchaseInvoicesFilter::PurchaseInvoicesFilter(QObject* parent)
	: QSortFilterProxyModel {parent}
{
	mFilterByPurchaseDateEnabled = false;
	mBufferFilterEnabled = true;
}

bool PurchaseInvoicesFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	return filterByPurchaseDate(source_row, source_parent)
			&& filterByIssueDate(source_row, source_parent)
			&& filterByValue(source_row, source_parent)
			&& filterByWarehouseName(source_row, source_parent)
			&& filterByContractorName(source_row, source_parent)
			&& filterByExistInBuffer(source_row, source_parent);
}

bool PurchaseInvoicesFilter::filterByPurchaseDate(int source_row, const QModelIndex &parent) const
{
	if (mFilterByPurchaseDateEnabled) {
		QDate date = sourceModel()->index(source_row, 0, parent)
					 .data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::DateOfPurchase).toDate();

		return date >= mPurchaseMinDate && date <= mPurchaseMaxDate;
	}

	return true;
}

bool PurchaseInvoicesFilter::filterByIssueDate(int source_row, const QModelIndex &parent) const
{
	if (mFilterByIssueDateEnabled) {
		QDate date = sourceModel()->index(source_row, 0, parent)
					 .data(eSawmill::invoices::models::purchases::InvoicesModel::Roles::DateOfIssue).toDate();

		return date >= mIssueMinDate && date <= mIssueMaxDate;
	}

	return true;
}

bool PurchaseInvoicesFilter::filterByValue(int source_row, const QModelIndex &parent) const
{
	if (mFilterByValueEnabled) {
		QString stringValue = sourceModel()->index(source_row, eSawmill::invoices::models::purchases::InvoicesModel::Columns::Value, parent).data().toString();
		double value = stringValue.split(' ').at(0).toDouble();

		return value >= mMinValue && value <= mMaxValue;
	}

	return true;
}

bool PurchaseInvoicesFilter::filterByWarehouseName(int source_row, const QModelIndex &parent) const
{
	if (mWarehouseName.length() > 0) {
		const QString name = sourceModel()
			->index(source_row, eSawmill::invoices::models::purchases::InvoicesModel::Warehouse, parent).data().toString();

		return name.contains(mWarehouseName, Qt::CaseInsensitive);
	}

	return true;
}

bool PurchaseInvoicesFilter::filterByContractorName(int source_row, const QModelIndex &parent) const
{
	if (mContractorName.length() > 0) {
		const QString contractor = sourceModel()
			->index(source_row, eSawmill::invoices::models::purchases::InvoicesModel::Contractor, parent).data().toString();

		return contractor.contains(mContractorName, Qt::CaseInsensitive);
	}

	return true;
}

bool PurchaseInvoicesFilter::filterByExistInBuffer(int source_row, const QModelIndex& parent) const
{
	return sourceModel()->index(source_row, 0, parent).data(models::purchases::InvoicesModel::Roles::BufferStatus).toBool() == mBufferFilterEnabled;
}

} // namespace
