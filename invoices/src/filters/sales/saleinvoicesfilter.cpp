#include "filters/sales/saleinvoicesfilter.hpp"
#include "models/sales/saleinvoicesmodel.hpp"


namespace eSawmill::invoices::filters::sales {

SaleInvoicesFilter::SaleInvoicesFilter(QObject* parent)
	: QSortFilterProxyModel {parent}
	, mDateOfSaleActive {false}
	, mDateOfIssueActive {false}
	, mValueActive {false}
	, mBufferFilterEnabled {true}
{
}

void SaleInvoicesFilter::setDateOfSaleFilter(bool active, const QDate& from, const QDate& to)
{
	mDateOfSaleActive = active;
	mDateOfSaleFrom = from;
	mDateOfSaleTo = to;

	invalidateFilter();
}

void SaleInvoicesFilter::setDateOfIssueFilter(bool active, const QDate &from, const QDate &to)
{
	mDateOfIssueActive = active;
	mDateOfIssueFrom = from;
	mDateOfIssueTo = to;

	invalidateFilter();
}

void SaleInvoicesFilter::setValueFilter(bool active, double minValue, double maxValue)
{
	mValueActive = active;
	mValueFrom = minValue;
	mValueTo = maxValue;

	invalidateFilter();
}

void SaleInvoicesFilter::setContractorNameFilter(const QString &contractor)
{
	mContractorNameValue = contractor;
	invalidateFilter();
}

void SaleInvoicesFilter::setWarehouseNameValue(const QString &warehouse)
{
	mWarehouseNameValue = warehouse;
	invalidateFilter();
}

bool SaleInvoicesFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	return filterByDateOfSale(source_row, source_parent)
			&& filterByDateOfIssue(source_row, source_parent)
			&& filterByValue(source_row, source_parent)
			&& filterByContractorName(source_row, source_parent)
			&& filterByWarehouseName(source_row, source_parent)
			&& filterByExistInBuffer(source_row, source_parent);
}

bool SaleInvoicesFilter::filterByDateOfSale(int source_row, const QModelIndex &source_parent) const
{
	if (mDateOfSaleActive) {
		const QDate saleDate = sourceModel()->index(source_row, 0, source_parent)
				.data(models::sales::InvoicesModel::Roles::DateOfSale).toDate();

		return saleDate >= mDateOfSaleFrom && saleDate <= mDateOfSaleTo;
	}

	return true;
}

bool SaleInvoicesFilter::filterByDateOfIssue(int source_row, const QModelIndex &source_parent) const
{
	if (mDateOfIssueActive) {
		const QDate issueDate = sourceModel()->index(source_row, models::sales::InvoicesModel::Columns::DateOfReceipt, source_parent).data().toDate();
		return issueDate >= mDateOfIssueFrom && issueDate <= mDateOfIssueTo;
	}

	return true;
}

bool SaleInvoicesFilter::filterByValue(int source_row, const QModelIndex &source_parent) const
{
	if (mValueActive) {
		QString valueDisplayed = sourceModel()->index(source_row, models::sales::InvoicesModel::Columns::Value, source_parent).data().toString();
		double value = valueDisplayed.split(' ').at(0).trimmed().toDouble();

		return value >= mValueFrom && value <= mValueTo;
	}
	return true;
}

bool SaleInvoicesFilter::filterByContractorName(int source_row, const QModelIndex &source_parent) const
{
	if (mContractorNameValue.isEmpty())
		return true;

	QString contractor = sourceModel()->index(source_row, models::sales::InvoicesModel::Columns::Contractor, source_parent).data().toString();
	return contractor.contains(mContractorNameValue, Qt::CaseInsensitive);
}

bool SaleInvoicesFilter::filterByWarehouseName(int source_row, const QModelIndex &source_parent) const
{
	if (mWarehouseNameValue.isEmpty())
		return true;

	QString warehouse = sourceModel()->index(source_row, models::sales::InvoicesModel::Columns::Warehouse, source_parent).data().toString();
	return warehouse.contains(mWarehouseNameValue, Qt::CaseInsensitive);
}

bool SaleInvoicesFilter::filterByExistInBuffer(int source_row, const QModelIndex& source_parent) const
{
	return sourceModel()->index(source_row, 0, source_parent).data(models::sales::InvoicesModel::Roles::BufferStatus).toBool() == mBufferFilterEnabled;
}

} // namespace eSawmill::invoices::filters::sales
