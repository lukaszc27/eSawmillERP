#include "verifications/purchaseinvoicesverification.hpp"
#include <dbo/purchaseinvoice.hpp>
#include <QDate>
#include <globaldata.hpp>


namespace eSawmill::invoices::verifications {

PurchaseInvoicesVerification::PurchaseInvoicesVerification()
	: core::verifications::AbstractVerification {}
{
}

void PurchaseInvoicesVerification::execute()
{
	auto& repository = core::GlobalData::instance().repository().invoiceRepository();
	const QList<core::PurchaseInvoice>& invoices = repository.getAllPurchaseInvoices();
	if (invoices.size() <= 0) {
		mValid = true;
		return;
	}
	const QDate currentDate = QDate::currentDate();
	for (const core::PurchaseInvoice& invoice : invoices) {
		if (!invoice.wasPaid()
			&& invoice.totalPrice() > invoice.paidPrice()
			&& invoice.paymentDeadlineDate() < currentDate
			&& !repository.hasPurchaseInvoiceCorrection(invoice.id()))
		{
			addNotification(QString("Faktura zakupu Nr %1 jest nieopłacona po terminie płatności (%2 dni)")
				.arg(invoice.number())
				.arg(invoice.paymentDeadlineDate().daysTo(currentDate)),
				core::verifications::Notification::Type::Warning);
		}
	}
	mValid = !(notifications().size() > 0);
}

} // namespace eSawmill::invoices::verifications
