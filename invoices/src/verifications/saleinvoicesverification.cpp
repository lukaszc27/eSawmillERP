#include "verifications/saleinvociesverification.hpp"
#include <dbo/saleinvoice.hpp>
#include <QDate>
#include <globaldata.hpp>

namespace eSawmill::invoices::verifications {

SaleInvoicesVerification::SaleInvoicesVerification()
	: core::verifications::AbstractVerification {}
{
}

void SaleInvoicesVerification::execute() {
	auto& repository = core::GlobalData::instance().repository().invoiceRepository();
	const QList<core::SaleInvoice>& invoices = repository.getAllSaleInvoices();
	if (invoices.size() == 0) {
		mValid = true;
		return;
	}

	const QDate currentDate = QDate::currentDate();
	for (const core::SaleInvoice& invoice : invoices) {
		if (!invoice.wasPaid()
			&& invoice.totalPrice() > invoice.paidPrice()
			&& invoice.paymentDeadlineDate() < currentDate
			&& !repository.hasSaleInvoiceCorrection(invoice.id())) {
			addNotification(QString("Faktura sprzedaży Nr %1 jest nie opłacona po terminie płatności (%2 dni)")
							.arg(invoice.number())
							.arg(invoice.paymentDeadlineDate().daysTo(currentDate)),
							core::verifications::Notification::Type::Warning);
		}
	}
	mValid = !(notifications().size() > 0);
}

} // namespace eSawmill::invoices::verifications
