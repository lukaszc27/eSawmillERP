#include "models/paymentmethodmodel.hpp"

namespace eSawmill::invoices::models {

PaymentMethodModel::PaymentMethodModel(QObject* parent)
	: QStringListModel(parent)
{
	setStringList(QStringList()
		<< tr("Gotówka")
		<< tr("Przelew")
	);
}

}
