#include "models/sales/invoiceitemsmodel.hpp"
#include <globaldata.hpp>

namespace eSawmill::invoices::models::sales {

InvoiceItemsModel::InvoiceItemsModel(QObject* parent)
	: core::models::AbstractModel<core::SaleInvoiceItem> {parent}
	, mInvoiceId {0}
{
}

//void InvoiceItemsModel::loadInvoiceItems(const QList<core::SaleInvoiceItem>& items)
//{
//	beginResetModel();
//	this->insert()
//	endResetModel();
//}

void InvoiceItemsModel::loadInvoiceItems(core::eloquent::DBIDKey invoiceId) {
	if (invoiceId <= 0)
		return;

	beginResetModel();
	mList = core::GlobalData::instance().repository().invoiceRepository().getSaleInvoiceItems(invoiceId);
	notifyDataChanged();
	endResetModel();

	// store invoice id on next operations
	// (if will be nessesary)
	mInvoiceId = invoiceId;
}

Qt::ItemFlags InvoiceItemsModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);
	flags |= Qt::ItemIsEnabled | Qt::ItemIsSelectable;

	if (index.column() == Columns::Quantity
		|| index.column() == Columns::MeasureUnit
		|| index.column() == Columns::Rebate)
		flags |= Qt::ItemIsEditable;

	return flags;
}

QStringList InvoiceItemsModel::headerData() const {
	return QStringList()
			<< tr("Nazwa")
			<< tr("Ilość")
			<< tr("Jm")
			<< tr("Rabat")
			<< tr("Cena (z rabatem)")
			<< tr("Wartość")
			<< tr("Magazyn");
}

void InvoiceItemsModel::createOrUpdate(core::SaleInvoiceItem element) {
	Q_UNUSED(element);
}

void InvoiceItemsModel::destroy(core::SaleInvoiceItem element) {
	Q_UNUSED(element);
}

void InvoiceItemsModel::get() {
}

QVariant InvoiceItemsModel::data(const QModelIndex &index, int role) const {
	core::SaleInvoiceItem item = items().at(index.row());
	Q_UNUSED(index);
	if (role == Qt::DisplayRole) {
		switch (index.column()) {
		case Name: return item.name();
		case Quantity: return item.quantity();
		case MeasureUnit: return item.measureUnit();
		case Price: return item.price();
		case Rebate: return item.rebate();
		case Value: return item.value();
		case Warehouse: return item.warehouse();
		}
	}
	return QVariant{};
}

bool InvoiceItemsModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	int row = index.row(),
		col = index.column();

	core::SaleInvoiceItem& item = mList[row];
	switch (role) {
	case Qt::EditRole:
		switch (col) {
		case Columns::Quantity:
			item.setQuantity(value.toInt());
			item.setValue(item.price()*item.quantity());
			break;

		case Columns::MeasureUnit:
			item.setMeasureUnit(value.toString());
			break;

		case Columns::Rebate: {
			double rebate = value.toDouble();
			const double price = item.price();
			item.setRebate(rebate);

			rebate = (100.f - rebate) / 100.f;
			item.setPrice(double(price * rebate));
			item.setValue(item.price()*item.quantity());
			} break;
		}
		break;

	case Qt::CheckStateRole:
		item.setChecked(value.toBool());
		break;
	}
	notifyDataChanged();
	return true;
}

QVariant InvoiceItemsModel::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return headerData().at(section);

		if (orientation == Qt::Vertical)
			return QString::number(section + 1);
		break;
	}
	return QVariant();
}

double InvoiceItemsModel::calculateTotalNettoPrice() {
	updateValuesInList();
	double sum {0};
	for (auto& el : items()) {
		sum += el.get("Value").toDouble();
	}
	return sum;
}

void InvoiceItemsModel::updateValuesInList() {
	for (auto& el : items()) {
		int qty = el.get("Quantity").toInt();
		double price = el.get("Price").toDouble();
		el.set("Value", static_cast<double>(qty * price));
	}
}

void InvoiceItemsModel::notifyDataChanged() {
	updateValuesInList();
	Q_EMIT dataChanged();
}

} // namespace eSawmill::invoices::models
