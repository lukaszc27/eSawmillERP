#include "models/sales/saleinvoicesmodel.hpp"
#include <dbo/saleinvoice.hpp>
#include <QBrush>
#include <QFont>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>
#include <messagebox.h>


namespace eSawmill::invoices::models::sales {

InvoicesModel::InvoicesModel(QObject* parent)
	: core::models::AbstractModel<core::SaleInvoice>{parent}
	, mReadOnly {false}
{
	get();
}

InvoicesModel::~InvoicesModel()
{
}

QStringList InvoicesModel::headerData() const {
	return QStringList()
			<< "Numer dokumentu"
			<< "Status płatności"
			<< "Data wystawienia"
			<< "Kontrahent"
			<< "NIP"
			<< "Magazyn"
			<< "Miasto"
			<< "Wartość";
}

void InvoicesModel::createOrUpdate(core::SaleInvoice element) {
	Q_UNUSED(element);
}

void InvoicesModel::destroy(core::SaleInvoice element) {
	Q_UNUSED(element);
}

void InvoicesModel::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.invoiceRepository()
			.getAllSaleInvoices();
		endResetModel();
	} catch (...) {
		throw;
	}
}

int InvoicesModel::rowCount(const QModelIndex& index) const {
	Q_UNUSED(index);
	return items().size();
}

Qt::ItemFlags InvoicesModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = QAbstractItemModel::flags(index)
			| Qt::ItemIsEnabled | Qt::ItemIsSelectable;

	if (!mReadOnly && index.column() == Columns::Status
		&& !index.data(Roles::PaidStatus).toBool())
	{
		flags |= Qt::ItemIsEditable;
	}

	return flags;
}

QVariant InvoicesModel::data(const QModelIndex &index, int role) const {
	int col = index.column(),
		row = index.row();
	const core::SaleInvoice& invoice = items().at(row);
	const bool hasCorrection = core::GlobalData::instance().repository().invoiceRepository().hasSaleInvoiceCorrection(invoice.id(), true);
	const core::Contractor& contractor = invoice.contractor();

	switch (role) {
	case Qt::DisplayRole: {
		switch (col) {
		case Columns::DocumentNumber: return invoice.number(); break;
		case Columns::DateOfReceipt: return invoice.dateOfIssue(); break;
		case Columns::Warehouse: return invoice.warehouse().name(); break;
		case Columns::Contractor:
			if (contractor.name().isEmpty())
				return QString("%1 %2").arg(contractor.personName(), contractor.personSurname());
			else return contractor.name();
			break;
		case Columns::NIP: return contractor.NIP(); break;
		case Columns::City: return contractor.contact().city(); break;
		case Columns::Value:
			return QString("%1 PLN").arg(invoice.totalPrice());
			break;
		}
		}break;

	case Qt::ForegroundRole:
		if (hasCorrection)
			return QBrush(Qt::gray);

		if (!invoice.inBuffer())
			return QBrush(Qt::darkGreen);

		if (!invoice.wasPaid() && invoice.paymentDeadlineDate() < QDate::currentDate())
			return QBrush(Qt::red);
		break;

	case Qt::FontRole:
		if (hasCorrection) {
			QFont font;
			font.setStrikeOut(true);
			return font;
		}
		break;

	case Qt::TextAlignmentRole:
		if (col == Columns::Value)
			return Qt::AlignRight;
		break;

//	case Qt::ToolTipRole: {
//		QString tooltip = invoice.number();
//		if (hasCorrection)
//			tooltip += QString(" -> %1").arg(static_cast<core::SaleInvoice>(invoice.correctionDocument()).number());
//		return tooltip;
//		} break;

	// additional roles
	case Roles::Id: return invoice.id(); break;
	case Roles::PaidStatus: return invoice.wasPaid(); break;
	case Roles::DateOfIssue: return invoice.dateOfIssue(); break;
	case Roles::DateOfSale: return invoice.dateOfSale(); break;
	case Roles::BufferStatus: return invoice.inBuffer(); break;
	case Roles::HasCorrectionDocument: return hasCorrection; break;
	}

	return QVariant();
}

bool InvoicesModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	auto& repository = core::GlobalData::instance().repository().invoiceRepository();

	try {
		core::SaleInvoice& invoice = items()[index.row()];
		switch (role) {
		case Qt::EditRole: {
			Q_ASSERT(invoice.id() > 0);
			if (!repository.lockSaleInvoice(invoice.id()))
				return false;

			switch (index.column()) {
			case Columns::Status:
				if (value.toBool()) {
					invoice.setPaid(true);
					invoice.setPaidPrice(invoice.totalPrice());
					invoice.setToPaidPrice(0);

					// update invoice on servier side without recreated items
					core::GlobalData::instance()
						.repository()
						.invoiceRepository()
						.updateSaleInvoice(invoice/*, QList<core::SaleInvoiceItem>{}*/);
				}
				break;
			} // switch (index.column())
			if (!repository.unlockSaleInvoice(invoice.id()))
				return false;
			} break;
		} // switch
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::information(qobject_cast<QWidget*>(parent()),
			tr("Blokada dostępu"),
			tr("Wybrany element jest aktualnie modyfikowany przez innego użytkownika.\n"
				"Poczekaj na zakończenie aktualizacji"));
	} catch (...) {
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Nieprzechwycony wyjątek w: %s", Q_FUNC_INFO));
	}

	return true;
}

QVariant InvoicesModel::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return headerData().at(section);
		else if (orientation == Qt::Vertical)
			return QString::number(section + 1);
		break;
	}
	return QVariant();
}

} // namespace eSawmill::invoices::models
