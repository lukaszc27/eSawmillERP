#include "models/purchases/purchaseinvoicesmodel.hpp"
#include "dbo/purchaseinvoice.hpp"
#include <messagebox.h>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>


namespace eSawmill::invoices::models::purchases {

InvoicesModel::InvoicesModel(QObject* parent)
	: core::models::AbstractModel<core::PurchaseInvoice>(parent)
{
	get();
}

void InvoicesModel::createOrUpdate(core::PurchaseInvoice element) {
	Q_UNUSED(element);
}

void InvoicesModel::destroy(core::PurchaseInvoice element) {
	Q_UNUSED(element);
}

void InvoicesModel::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.invoiceRepository()
			.getAllPurchaseInvoices();
		endResetModel();
	}
	catch (...) {
		// rethrow all exceptions
		throw;
	}
}

Qt::ItemFlags InvoicesModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = core::models::AbstractModel<core::PurchaseInvoice>::flags(index);
	flags |= Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	if (!readOnly() &&
		index.column() == Columns::Status
		&& !index.data(Roles::PaidStatus).toBool())
	{
		flags |= Qt::ItemIsEditable;
	}

	return flags;
}

int InvoicesModel::rowCount(const QModelIndex& index) const {
	Q_UNUSED(index);
	return items().size();
}

QStringList InvoicesModel::headerData() const {
	return QStringList()
			<< tr("Numer dokumentu")
			<< tr("Dokument źródłowy")
			<< tr("Status zapłaty")
			<< tr("Data wpływu")
			<< tr("Kontrahent")
			<< tr("NIP")
			<< tr("Magazyn")
			<< tr("Miasto")
			<< tr("Wartość");
}

QVariant InvoicesModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (role == Qt::DisplayRole) {
		switch (orientation) {
		case Qt::Horizontal:
			return headerData().at(section);
			break;

		case Qt::Vertical:
			return QString::number(section + 1);
			break;
		}
	}

	return QVariant {};
}

QVariant InvoicesModel::data(const QModelIndex &index, int role) const {
	int row = index.row(),
		col = index.column();

	core::PurchaseInvoice invoice = items().at(row);
	core::Contractor& contractor = invoice.contractor();
	core::Warehouse& warehouse = invoice.warehouse();
	const bool hasCorrection = core::GlobalData::instance().repository().invoiceRepository().hasPurchaseInvoiceCorrection(invoice.id(), true);

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::DocumentNumber: return  invoice.number();
		case Columns::SourceDocument: return invoice.foreignNumber();
		case Columns::DateOfReceipt: return invoice.dateOfReceipt();
		case Columns::Contractor:
			if (contractor.name().isEmpty())
				return QString("%1 %2")
						.arg(contractor.personName(),
							 contractor.personSurname());

			return contractor.name();
			break;

		case Columns::NIP: return contractor.NIP();
		case Columns::Warehouse: return warehouse.name();
		case Columns::City: return contractor.contact().city();
		case Columns::Value:
			return QString("%1 PLN").arg(invoice.totalPrice());
			break;
		}

		break;

	case Qt::ForegroundRole:
		if (hasCorrection)
			return QBrush(Qt::gray);

		if (!invoice.wasPaid() && invoice.paymentDeadlineDate() < QDate::currentDate())
			return QBrush(Qt::red);

		if (!invoice.inBuffer())
			return QBrush(Qt::darkGreen);
		break;

	case Qt::FontRole:
		if (hasCorrection) {
			QFont font;
			font.setStrikeOut(true);
			return font;
		}
		break;

	case Qt::TextAlignmentRole:
		if (col == Columns::Value)
			return Qt::AlignRight;
		break;

	case Roles::Id: return invoice.id();
	case Roles::PaidStatus: return invoice.wasPaid();
	case Roles::DateOfIssue: return invoice.dateOfIssue();
	case Roles::DateOfPurchase: return invoice.dateOfPurchase();
	case Roles::BufferStatus: return invoice.inBuffer();
	case Roles::HasCorrection: return hasCorrection;
	}

	return QVariant {};
}

bool InvoicesModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	auto& repository = core::GlobalData::instance().repository().invoiceRepository();

	try {
		core::PurchaseInvoice& invoice = items()[index.row()];
		switch (role) {
		case Qt::EditRole: {
			Q_ASSERT(invoice.id() > 0);
			if (!repository.lockPurchaseInvoice(invoice.id()))
				return false;

			if (index.column() == Columns::Status) {
				if (value.toBool()) {
					invoice.setPaid(true);
					invoice.setPaidPrice(invoice.totalPrice());
					invoice.setToPaidPrice(0);

					repository.updatePurchaseInvoice(invoice/*, QList<core::PurchaseInvoiceItem>{}*/);
				}
			} // Columns::Status
			if (!repository.unlockPurchaseInvoice(invoice.id()))
				return false;
			} break;
		}
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::information(qobject_cast<QWidget*>(parent()),
			tr("Blokada dostępu"),
			tr("Wybrana faktura zakupu jest aktualnie modyfikowana przez innego użytkownika.\n"
				"Poczekaj na zakończenie edycji"));
	} catch (...) {
		core::GlobalData::instance().logger().critical(
			QString::asprintf("Nieprzechwycony wyjątek w: %s", Q_FUNC_INFO));
	}
	return true;
}

} // namespace eSawmill::invoices::models::purchases
