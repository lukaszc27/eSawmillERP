#include "models/purchases/purchaseinvocieitemsmodel.hpp"
#include <dbo/purchaseinvoiceitem.hpp>
#include <globaldata.hpp>

namespace eSawmill::invoices::models::purchases {

InvoiceItemModel::InvoiceItemModel(QObject* parent, int invoiceId)
	: core::models::AbstractModel<core::PurchaseInvoiceItem> {parent}
	, mInvoiceId {invoiceId}
{
}

void InvoiceItemModel::loadInvoiceItems(const QList<core::PurchaseInvoiceItem> &items) {
	beginResetModel();
	mList = items;
	notifyDataChanged();
	endResetModel();
}

void InvoiceItemModel::loadInvoiceItems(core::eloquent::DBIDKey invoiceId) {
	beginResetModel();
	if (invoiceId <= 0)
		return;

	mList = core::GlobalData::instance().repository().invoiceRepository().getPurchaseInvoiceItems(invoiceId);
	// store invocie id on next operations (if will be nessesary)
	mInvoiceId = invoiceId;
	notifyDataChanged();
	endResetModel();
}

void InvoiceItemModel::createOrUpdate(core::PurchaseInvoiceItem element) {
	Q_UNUSED(element);
}

void InvoiceItemModel::destroy(core::PurchaseInvoiceItem element) {
	Q_UNUSED(element);
}

void InvoiceItemModel::get() {
	beginResetModel();
	try {
		if (mInvoiceId > 0) {
			core::PurchaseInvoiceItem purchaseItems;
//			mList = purchaseItems.builder().where("InvoiceId", mInvoiceId).get();
		}
	}
	catch(const QString& msg) {
		::widgets::MessageBox::critical(nullptr, "Błąd pobierania danych", msg);
		return;
	}
	endResetModel();
}

Qt::ItemFlags InvoiceItemModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	flags |= Qt::ItemIsEnabled | Qt::ItemIsSelectable;

	if (int col = index.column();
		col == Columns::MeasureUnit
		|| col == Columns::Quantity
		|| col == Columns::Rebate)
	{
		flags |= Qt::ItemIsEditable;
	}

	return flags;
}

QStringList InvoiceItemModel::headerData() const {
	return QStringList()
			<< "Nazwa"
			<< "Ilość"
			<< "Jm"
			<< "Rabat"
			<< "Cena (z rabatem)"
			<< "Wartość"
			<< "Magazyn";
}

QVariant InvoiceItemModel::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return headerData().at(section);
		else if (orientation == Qt::Vertical)
			return QString::number(section + 1);
		break;
	}
	return QVariant();
}

QVariant InvoiceItemModel::data(const QModelIndex &index, int role) const {
	int row = index.row(),
		col = index.column();
	const auto& item = items().at(row);

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return item.name();
		case Quantity: return item.quantity();
		case MeasureUnit: return item.measureUnit();
		case Rebate: return item.rebate();
		case Price: return item.price();
		case Value: return item.value();
		case Warehouse: return item.warehouse();
		}

		break;
	}

	return QVariant{};
}

bool InvoiceItemModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	core::PurchaseInvoiceItem& item = mList[index.row()];
	switch (role) {
	case Qt::EditRole:
		switch (index.column()) {
		case Rebate: {
			double rebate = value.toDouble();
			item.setRebate(rebate);

			// calculate new price for item
			double price = items().at(index.row()).get("Price").toDouble();
			rebate = (100.f - rebate) / 100.f;
			item.setPrice(price*rebate);
			item.setValue(item.price()*item.quantity());
		} break;

		case Quantity:
			item.setQuantity(value.toInt());
			item.setValue(item.price()*item.quantity());
			break;

		case MeasureUnit:
			item.setMeasureUnit(value.toString());
			break;
		}
		break;
	case Qt::CheckStateRole:
		item.setChecked(value.toBool());
		break;
	}
	notifyDataChanged();
	return true;
}

double InvoiceItemModel::calculateTotalItemsValue() {
	double sum {0};
	for (auto& el : items()) {
		// update value on item
		double price = el.price();
		double qty = el.quantity();
		el.setValue(price*qty);

		sum += el.value();
	}
	return sum;
}

void InvoiceItemModel::notifyDataChanged() {
	Q_EMIT dataChanged();
}

}
