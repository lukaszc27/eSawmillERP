#include "delegates/purchases/purchaseinvoiceitemsdelegate.hpp"
#include "models/purchases/purchaseinvocieitemsmodel.hpp"
#include "models/measureunit.hpp"
#include <QSpinBox>
#include <QComboBox>


namespace model = eSawmill::invoices::models::purchases;
namespace eSawmill::invoices::delegates::purchases {

PurchaseInvoiceItemsDelegate::PurchaseInvoiceItemsDelegate(QObject* parent)
	: QStyledItemDelegate(parent)
{
}

QWidget* PurchaseInvoiceItemsDelegate::createEditor(QWidget *parent,
													const QStyleOptionViewItem &option,
													const QModelIndex &index) const
{
	Q_UNUSED(option);

	if (index.column() == model::InvoiceItemModel::Columns::Quantity) {
		QSpinBox* spinbox = new QSpinBox(parent);
		spinbox->setMaximum(1'000'000);
		return spinbox;
	}
	else if (index.column() == model::InvoiceItemModel::Columns::Rebate) {
		return new QSpinBox(parent);
	}
	else if (index.column() == model::InvoiceItemModel::Columns::MeasureUnit) {
		QComboBox* combo = new QComboBox(parent);
		combo->setModel(new eSawmill::articles::models::MeasureUnit(eSawmill::articles::models::MeasureUnit::ModelType::ShortName, parent));
		return combo;
	}
	return new QWidget();
}

void PurchaseInvoiceItemsDelegate::setEditorData(QWidget *editor,
												 const QModelIndex &index) const
{
	int col = index.column();
	if (col == model::InvoiceItemModel::Columns::Rebate
		|| col == model::InvoiceItemModel::Columns::Quantity)
	{
		QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		spinbox->setValue(index.data().toInt());
	}
	else if (col == model::InvoiceItemModel::Columns::MeasureUnit) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		combo->setCurrentText(index.data().toString());
	}
}

void PurchaseInvoiceItemsDelegate::setModelData(QWidget* editor,
												QAbstractItemModel* model,
												const QModelIndex &index) const
{
	int col = index.column();
	if (col == model::InvoiceItemModel::Rebate
		|| col == model::InvoiceItemModel::Quantity)
	{
		QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		model->setData(index, spinbox->value());
	}
	if (col == model::InvoiceItemModel::MeasureUnit) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		model->setData(index, combo->currentData(eSawmill::articles::models::MeasureUnit::Roles::ShortName).toString());
	}
}

} // namesapce
