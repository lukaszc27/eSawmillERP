#include "delegates/sales/saleinvoicesdelegate.hpp"
#include "models/sales/saleinvoicesmodel.hpp"
#include <messagebox.h>
#include <QCheckBox>
#include <QPainter>


namespace eSawmill::invoices::delegates::sales {

SaleInvoicesDelegate::SaleInvoicesDelegate(QObject* parent)
	: QStyledItemDelegate {parent}
{
}

QWidget* SaleInvoicesDelegate::createEditor(
		QWidget *parent,
		const QStyleOptionViewItem &option,
		const QModelIndex &index) const
{
	Q_UNUSED(option);
	if (index.column() == eSawmill::invoices::models::sales::InvoicesModel::Columns::Status) {
		return new QCheckBox(parent);
	}

	return new QWidget(parent);
}

void SaleInvoicesDelegate::setEditorData(
		QWidget *editor,
		const QModelIndex &index) const
{
	if (index.column() == eSawmill::invoices::models::sales::InvoicesModel::Columns::Status) {
		QCheckBox* checkbox = qobject_cast<QCheckBox*>(editor);
		checkbox->setChecked(index.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::PaidStatus).toBool());
	}
}

void SaleInvoicesDelegate::setModelData(
		QWidget *editor,
		QAbstractItemModel *model,
		const QModelIndex &index) const
{
	if (index.column() == eSawmill::invoices::models::sales::InvoicesModel::Columns::Status) {
		const QCheckBox* checkbox = qobject_cast<QCheckBox*>(editor);

		if (checkbox->isChecked()) {
			int result = ::widgets::MessageBox::question(editor->parentWidget(),
				tr("Pytanie"),
				tr("Czy chcesz oznaczyć pozycję jako zapłaconą?\n"
				   "UWAGA: Operacja ta nie może być cofnięta"), "",
					QMessageBox::Yes | QMessageBox::No);

			if (result  == QMessageBox::Yes)
				model->setData(index, checkbox->isChecked());
		}
	}
}

void SaleInvoicesDelegate::paint(QPainter* painter,
		const QStyleOptionViewItem& option,
		const QModelIndex& index) const
{
	QStyledItemDelegate::paint(painter, option, index);

	if (index.column() == eSawmill::invoices::models::sales::InvoicesModel::Columns::Status) {
		painter->save();

		QImage checkMark(":/icons/check-mark");
		QImage uncheckMark(":/icons/cross-mark");

		QRect rect = option.rect;
		rect.setSize(QSize(24,24));
		rect.moveTo(QPoint(option.rect.left() + (option.rect.right() - option.rect.left()) / 2 - 12,
						   option.rect.top() + (option.rect.bottom() - option.rect.top()) / 2 - 12));

		if (index.data(eSawmill::invoices::models::sales::InvoicesModel::Roles::PaidStatus).toBool())
			painter->drawImage(rect, checkMark);
		else painter->drawImage(rect, uncheckMark);

		painter->restore();
	}
}

} // namespace eSawmill::invoices::delegates::sales
