#include "delegates/sales/invoiceitemsdelegate.hpp"
#include "models/sales/invoiceitemsmodel.hpp"
#include <QSpinBox>
#include <QComboBox>
#include <models/measureunit.hpp>


namespace eSawmill::invoices::delegates::sales {

InvoiceItemsDelegate::InvoiceItemsDelegate(QObject* parent)
	: QStyledItemDelegate(parent)
{
}

QWidget* InvoiceItemsDelegate::createEditor(
		QWidget* parent,
		const QStyleOptionViewItem &option,
		const QModelIndex &index) const
{
	Q_UNUSED(option);
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Quantity) {
		QSpinBox* spinbox = new QSpinBox(parent);
		spinbox->setMaximum(1'000'000);

		return spinbox;
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::MeasureUnit) {
		QComboBox* combobox= new QComboBox {parent};
		combobox->setModel(new eSawmill::articles::models::MeasureUnit(eSawmill::articles::models::MeasureUnit::ModelType::ShortName, combobox));
		return combobox;
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Rebate) {
		QSpinBox* spinbox = new QSpinBox{parent};
		spinbox->setRange(0, 99);
		return spinbox;
	}
	return new QWidget(parent);
}

void InvoiceItemsDelegate::setEditorData(
		QWidget* editor,
		const QModelIndex &index) const
{
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Quantity) {
		QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		spinbox->setValue(index.data().toInt());
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::MeasureUnit) {
		QComboBox* combobox = qobject_cast<QComboBox*>(editor);
		combobox->setCurrentText(index.data().toString());
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Rebate) {
		QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		spinbox->setValue(index.data().toDouble());
	}
}

void InvoiceItemsDelegate::setModelData(
		QWidget* editor,
		QAbstractItemModel* model,
		const QModelIndex &index) const
{
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Quantity) {
		QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		model->setData(index, spinbox->value());
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::MeasureUnit) {
		const QComboBox* combobox = qobject_cast<QComboBox*>(editor);
		model->setData(index, combobox->currentText());
	}
	if (index.column() == eSawmill::invoices::models::sales::InvoiceItemsModel::Columns::Rebate) {
		const QSpinBox* spinbox = qobject_cast<QSpinBox*>(editor);
		model->setData(index, spinbox->value());
	}
}


}
