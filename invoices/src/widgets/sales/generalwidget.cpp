#include "widgets/sales/generalwidget.hpp"
#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QFont>
#include <QInputDialog>
#include <QSettings>
#include <QString>
#include <memory>
#include <messagebox.h>
#include <dialogs/orders/createelement.h>
#include "delegates/sales/invoiceitemsdelegate.hpp"
#include "eloquent/model.hpp"
#include <dbo/woodtype.hpp>
#include <dbo/saleinvoice.hpp>
#include "articlesmanager.h"
#include <dbo/measureunit_core.hpp>
#include <dbo/saleinvoicecorrections.hpp>
#include "models/paymentmethodmodel.hpp"
#include "eloquent/builder.hpp"
#include <globaldata.hpp>

namespace eSawmill::invoices::widgets::sales {

GeneralWidget::GeneralWidget(QWidget* parent)
	: QWidget {parent}
	, mInvoiceId {-1}
{
	createWidgets();
	createModels();
	createConnections();

	// set startup values in widgets
	const QDate currentDate = QDate::currentDate();
	mDateOfIssue->setDate(currentDate);
	mDateOfSale->setDate(currentDate);
	mPayDeadLineDays->setValue(7);
	mTypeOfPay->setModel(new eSawmill::invoices::models::PaymentMethodModel(this));
	mInvoiceYear->setText(QString::number(currentDate.year()));
	mInvoiceNumberMonth->setText(QString().asprintf("%2.0d", currentDate.month()));
	mInvoiceType->addItems(QStringList() << "FA" << "FV" << "FKOR");
}

GeneralWidget::~GeneralWidget()
{
}

QList<core::SaleInvoiceItem> GeneralWidget::invoiceItems() const {
	return mInvoiceItems->items();
}

core::SaleInvoice GeneralWidget::invoice() const {
	core::SaleInvoice saleInvoice;
	if (mInvoiceId > 0) {
		// if widget is in edit mode
		// load invoice information from cache memory
		// and than edit it
		saleInvoice = core::GlobalData::instance().repository().invoiceRepository().getSaleInvoice(mInvoiceId);
	}

	// get warehouse object by repository to avoid situations when client app want to get data direct from db server
	const auto& warehouse = core::GlobalData::instance()
		.repository()
		.warehousesRepository()
		.getWarehouse(
			mWarehouse->currentData(
				eSawmill::articles::models::WarehouseList::Roles::Id
			).toInt()
	);

	saleInvoice.setContractor(mContractorSelector->contractor());
	saleInvoice.setWarehouse(warehouse);
	saleInvoice.setDateOfIssue(mDateOfIssue->date());
	saleInvoice.setDateOfSale(mDateOfSale->date());
	saleInvoice.setRebate(mRebate->value());
	saleInvoice.setPayment(mTypeOfPay->currentText());
	saleInvoice.setPaymentDeadlineDays(mPayDeadLineDays->value());
	saleInvoice.setPaymentDeadlineDate(mPayDeadline->date());
	saleInvoice.setNettoPrice(mNettoPrice->value());
	saleInvoice.setTotalPrice(mTotalPrice->value());
	saleInvoice.setPaidPrice(mPaidPrice->value());
	saleInvoice.setToPaidPrice(mToPayPrice->value());
	saleInvoice.setPrefix(mInvoicePrefix->text().toUpper());
	saleInvoice.setPaid(mToPayPrice->value() == 0 || (mTotalPrice->value() == mPaidPrice->value()));

	if (isCorrectionInvoice()) {
		core::SaleInvoice badInvoice = core::SaleInvoice().findRecord(correctionInvoiceId());
		QString number = QString("%1 %2")
			.arg(mInvoicePrefix->text())
			.arg(badInvoice.number().split(' ').at(1));

		saleInvoice.setNumber(number);
	}

	return saleInvoice;
}

void GeneralWidget::loadInvoice(const core::SaleInvoice &invoice)
{
	mInvoiceId = invoice.id();
	mContractorSelector->selectContractor(invoice.contractor());
	mWarehouse->setCurrentText(invoice.warehouse().name());
	mDateOfIssue->setDate(invoice.dateOfIssue());
	mDateOfSale->setDate(invoice.dateOfSale());
	mRebate->setValue(invoice.rebate());
	mTypeOfPay->setCurrentText(invoice.payment());
	mPayDeadLineDays->setValue(invoice.paymentDeadlineDays());
	mPayDeadline->setDate(invoice.paymentDeadlineDate());
	mNettoPrice->setValue(invoice.nettoPrice());
	mTotalPrice->setValue(invoice.totalPrice());
	mPaidPrice->setValue(invoice.paidPrice());
	mToPayPrice->setValue(invoice.toPaidPrice());
	mInvoiceItems->loadInvoiceItems(invoice);

	// load document prefix from invoice number
	const QStringList numParts = invoice.number().split(' ');
	const QStringList nums = numParts[1].split('/');
	mInvoiceType->setCurrentText(numParts[0]);
	mInvoiceNumber->setValue(nums[0].toInt());
	mInvoiceNumberMonth->setText(nums[1]);
	mInvoiceYear->setText(nums[2]);

	// if this invoice is correction to other invoice
	if (mInvoicePrefix->text().contains("KOR", Qt::CaseInsensitive)) {
		core::SaleInvocieCorrections correction;
		correction = correction.builder().where("CorrectionId", invoice.id()).orderBy("CreatedDate").first();
		mInvoiceSelector->setInvoice(correction.invoice(true), widgets::InvoiceSelectror::InvoiceType::SaleInvoce);
		mInvoiceSelector->setReadOnly(true);
	}

	// disable widgets
	mInvoiceType->setDisabled(true);
	mInvoiceNumber->setDisabled(true);
	mContractorSelector->setReadOnly(true);
}

void GeneralWidget::createWidgets()
{
	// items in invoice group
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton("Dodaj", QKeySequence("Ins"), this, QIcon(":/icons/add"));
	mRemoveButton = new ::widgets::PushButton("Usuń", QKeySequence("Del"), this, QIcon(":/icons/trash"));

	QMenu* menu = new QMenu(this);
	mAddButton->setMenu(menu);
	menu->addAction("Artykuł", this, &GeneralWidget::addArticleHandle, QKeySequence("Ins"));
	menu->addAction("Element drewniany", this, &GeneralWidget::addWoodElementHandle, QKeySequence("Ctrl+Ins"));

	// create context menu for table view
	{
		QMenu* contextMenu = new QMenu();
		QMenu* addingMenu = new QMenu("Dodaj", this);
		addingMenu->addAction("Artykuł", this, &GeneralWidget::addArticleHandle);
		addingMenu->addAction("Element drewniany", this, &GeneralWidget::addWoodElementHandle);

		contextMenu->addMenu(addingMenu);
		contextMenu->addAction("Usuń zaznaczone", this, &GeneralWidget::removeElementHandle);
		mTableView->setContextMenu(contextMenu);
	}

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(createDocumentTypeWidgets());
	mainLayout->addSpacing(3);
	mainLayout->addWidget(createHeaderWidgets());
	mainLayout->addSpacing(6);
	mainLayout->addWidget(createDateAndValuesWidgets());
	mainLayout->addSpacing(8);
	mainLayout->addWidget(mTableView);
	mainLayout->addLayout(buttonsLayout);

	// set startup settings for widgets
	for (auto& field : findChildren<QDateEdit*>())
		field->setCalendarPopup(true);

	for (auto& field : findChildren<QDoubleSpinBox*>()) {
		field->setMaximum(1'000'000);
		field->setReadOnly(true);
	}
	mPaidPrice->setReadOnly(false);
}

void GeneralWidget::createConnections() {
	connect(mPayDeadLineDays, qOverload<int>(&QSpinBox::valueChanged), this, [this](int days){
		mPayDeadline->setDate(mDateOfIssue->date().addDays(days));
	});
	connect(mPayDeadline, &QDateEdit::dateChanged, this, [this](const QDate& date){
		mPayDeadLineDays->setValue(mDateOfIssue->date().daysTo(date));
	});
	connect(mDateOfIssue, &QDateEdit::dateChanged, this, [this](const QDate& date){
		mPayDeadline->setDate(date.addDays(mPayDeadLineDays->value()));
	});
	connect(mInvoiceType, &QComboBox::currentTextChanged, this, [this](const QString& type){
		mInvoicePrefix->setText(type);
		mCorrectionInvoice = type.contains("KOR", Qt::CaseInsensitive);
		mForeignInvoiceFrame->setVisible(mCorrectionInvoice);
	});
	connect(mTypeOfPay, &QComboBox::currentTextChanged, this, [this](const QString& text){
		if (text.toUpper() == "GOTÓWKA") {
			mPayDeadLineDays->setDisabled(true);
			mPayDeadline->setDisabled(true);
		}
		else {
			mPayDeadLineDays->setEnabled(true);
			mPayDeadline->setEnabled(true);
		}
	});
	connect(mTotalPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double price){
		mToPayPrice->setValue(price - mPaidPrice->value());
	});
	connect(mInvoiceItems, &models::sales::InvoiceItemsModel::dataChanged, this, [&](){
		double nettoPrice = mInvoiceItems->calculateTotalNettoPrice();
		mNettoPrice->setValue(nettoPrice);
		mTotalPrice->setValue(nettoPrice);
	});
	connect(mPaidPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val){
		mToPayPrice->setValue(mTotalPrice->value() - val);
	});

    // reaction to change rebate percent
    // and calculate new value of invocie
    connect(mRebate, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int val){
        double totalPrice = mInvoiceItems->calculateTotalNettoPrice();
        double rebate = (100.f - val) / 100.f;
        mTotalPrice->setValue(totalPrice * rebate);
    });
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &GeneralWidget::removeElementHandle);
	connect(mInvoiceSelector, &widgets::InvoiceSelectror::invoiceSelected, this, &GeneralWidget::selectCorrectionInvoice);
}

void GeneralWidget::createModels()
{
	mWarehouseList = new eSawmill::articles::models::WarehouseList(this);
	mWarehouse->setModel(mWarehouseList);

	mInvoiceItems = new eSawmill::invoices::models::sales::InvoiceItemsModel(this);
	mTableView->setModel(mInvoiceItems);
	mTableView->setItemDelegate(new eSawmill::invoices::delegates::sales::InvoiceItemsDelegate(this));
}

QLayout* GeneralWidget::createDocumentTypeWidgets()
{
	mInvoiceType = new QComboBox {this};
	mInvoicePrefix = new QLineEdit {this};
	mInvoiceNumber = new ::widgets::InvoiceNumberSpinBox {this};
	mInvoiceNumberMonth = new QLineEdit {this};
	mInvoiceYear = new QLineEdit {this};
	mForeignInvoiceFrame = new QFrame {this};
	mInvoiceSelector = new widgets::InvoiceSelectror {widgets::InvoiceSelectror::InvoiceType::SaleInvoce, this};

	mForeignInvoiceFrame->setFrameShape(QFrame::StyledPanel);
	mForeignInvoiceFrame->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mForeignInvoiceFrame->setVisible(false);
	QVBoxLayout* frameLayout = new QVBoxLayout { mForeignInvoiceFrame };
	frameLayout->addWidget(new QLabel(tr("<b>Dokument korygowany</b>"), this), 1, Qt::AlignLeft);
	frameLayout->addWidget(mInvoiceSelector);

	mInvoicePrefix->setMaximumWidth(75);
	mInvoicePrefix->setDisabled(true);
	mInvoiceNumber->setMaximum(80);
	mInvoiceNumberMonth->setMaximumWidth(64);
	mInvoiceNumberMonth->setDisabled(true);
	mInvoiceYear->setMaximumWidth(64);
	mInvoiceYear->setDisabled(true);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->addWidget(new QLabel("Dokument", this), 0, Qt::AlignTop);
	layout->addWidget(mInvoiceType, 0, Qt::AlignTop);
	layout->addSpacing(24);
	layout->addWidget(new QLabel("Numer", this), 0, Qt::AlignTop);
	layout->addWidget(mInvoicePrefix, 0, Qt::AlignTop);
	layout->addWidget(mInvoiceNumber, 0, Qt::AlignTop);
	layout->addWidget(mInvoiceNumberMonth, 0, Qt::AlignTop);
	layout->addWidget(mInvoiceYear, 0, Qt::AlignTop);
	layout->addStretch(1);
	layout->addWidget(mForeignInvoiceFrame);

	return layout;
}

QWidget* GeneralWidget::createHeaderWidgets()
{
	QGroupBox* headerGroup = new QGroupBox("Nagłówek", this);
	headerGroup->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	// header group
	mContractorSelector = new contractors::widgets::ContractorSelector(this);
	mWarehouse = new QComboBox(this);
	mWarehouse->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* headerLayout = new QFormLayout(headerGroup);
	headerLayout->addRow("Kontrahent", mContractorSelector);
	headerLayout->addRow("Magazyn", mWarehouse);

	return headerGroup;
}

QWidget* GeneralWidget::createDateAndValuesWidgets()
{
	QGroupBox* dateValuesGroup = new QGroupBox("Daty i wartości", this);
	dateValuesGroup->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	mDateOfIssue = new QDateEdit(this);
	mDateOfSale = new QDateEdit(this);

	QGroupBox* dateGroup = new QGroupBox(this);
	QFormLayout* dateGroupLayout = new QFormLayout(dateGroup);
	dateGroupLayout->addRow("Data wystawienia", mDateOfIssue);
	dateGroupLayout->addRow("Data sprzedaży", mDateOfSale);

	mRebate = new QSpinBox(this);
	mTypeOfPay = new QComboBox(this);
	mPayDeadLineDays = new QSpinBox(this);
	mPayDeadline = new QDateEdit(this);

	mTypeOfPay->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	auto createUnitLayout = [this](QWidget* widget, const QString& unit = "PLN") -> QLayout* {
		QHBoxLayout* layout = new QHBoxLayout;
		QLabel* label = new QLabel(unit, this);
		label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

		layout->addWidget(widget);
		layout->addWidget(label);
		return layout;
	};

	QHBoxLayout* payLayout = new QHBoxLayout;
	payLayout->addStretch(1);
	payLayout->addWidget(mPayDeadLineDays);
	payLayout->addWidget(new QLabel("dni", this));
	payLayout->addSpacing(24);
	payLayout->addWidget(mPayDeadline);

	QGroupBox* payMethodGroup = new QGroupBox(this);
	QFormLayout* payMethodLayout = new QFormLayout(payMethodGroup);
	payMethodLayout->addRow("Rabat", createUnitLayout(mRebate, "%"));
	payMethodLayout->addRow("Płatność", mTypeOfPay);
	payMethodLayout->addRow("Termin", payLayout);

	mNettoPrice = new QDoubleSpinBox(this);
	mTotalPrice = new QDoubleSpinBox(this);
	mPaidPrice = new QDoubleSpinBox(this);
	mToPayPrice = new QDoubleSpinBox(this);

	QGroupBox* priceGroup = new QGroupBox();
	QFormLayout* priceLayout = new QFormLayout(priceGroup);
	priceLayout->addRow("Netto", createUnitLayout(mNettoPrice));
	priceLayout->addRow("<b>Razem</b>", createUnitLayout(mTotalPrice));
	priceLayout->addRow("Zapłacono", createUnitLayout(mPaidPrice));
	priceLayout->addRow("Pozostaje", createUnitLayout(mToPayPrice));


	QFont font;
	font.setBold(true);
	mTotalPrice->setFont(font);

	QHBoxLayout* dateValuesGroupLayout = new QHBoxLayout(dateValuesGroup);
	dateValuesGroupLayout->addWidget(dateGroup);
	dateValuesGroupLayout->addWidget(payMethodGroup);
	dateValuesGroupLayout->addWidget(priceGroup);

	return dateValuesGroup;
}

void GeneralWidget::addArticleHandle()
{
	std::unique_ptr<eSawmill::articles::ArticlesManager> dialog = std::make_unique<eSawmill::articles::ArticlesManager>(this, true);
	if (dialog->exec() == eSawmill::articles::ArticlesManager::Accepted) {
		double qty = QInputDialog::getDouble(this, "Ilość na dokumęcie", "Ilość", 1);
		const core::Article article = dialog->article();

		if (qty > article.get("Quantity").toDouble()) {
			::widgets::MessageBox::warning(this, "Ostrzeżenie",
				"Na magazynie nie ma wystarczającej ilości artykułów");

			return;
		}

		double price = article.get("PriceSaleBrutto").toDouble();
		double value = qty * price;
		core::SaleInvoiceItem el;
		el.setName(article.name());
		el.setQuantity(qty);
		el.setMeasureUnit(article.unit().shortName());
		el.setPrice(price);
		el.setValue(value);
		el.setArticle(article.id());
		el.setWarehouse(article.warehouse().name());

		mInvoiceItems->insert(el);
	}
}

void GeneralWidget::addWoodElementHandle() {
	std::unique_ptr<::widgets::dialogs::orders::CreateElement> dialog = std::make_unique<::widgets::dialogs::orders::CreateElement>(this);
	if (dialog->exec() == ::widgets::dialogs::orders::CreateElement::Accepted) {
		auto& repository = core::GlobalData::instance().repository().woodTypeRepository();
		const core::order::Element woodElement = dialog->element();
		core::SaleInvoiceItem el;

		QString elementName;
		QTextStream name(&elementName);

		if (!woodElement.get("Name").toString().isEmpty())
			name << woodElement.get("Name").toString() << " ";
		else name << "ELEMENT ";

//		const core::WoodType woodType = core::WoodType::find<core::WoodType>(woodElement.get("WoodTypeId").toUInt());
		const core::WoodType woodType = repository.get(woodElement.get("WoodTypeId").toInt());

		name << woodElement.width() << "X"
			 << woodElement.height() << "X"
			 << woodElement.length() << "M - "
			 << woodType.name();

		if (woodElement.planned())
			name << " (STRUGANY)";

		QSettings settings;
		double qty = woodElement.quantity();
		double price = settings.value("Orders/defaultPrice").toDouble();
		double plannedPrice = settings.value("Orders/defaultPlannedPrice").toDouble();
		double stere = woodElement.stere() / qty;

		/// calclate price for specific wood type
		price *= woodType.factor();

		/// calculate price for element
		price *= stere;
		if (woodElement.planned())
			price += (stere * plannedPrice);

		el.setName(elementName);
		el.setQuantity(qty);
		el.setMeasureUnit(tr("SZT"));
		el.setPrice(price);

		price *= qty;
		el.setValue(price);

		mInvoiceItems->insert(el);
	}
}

void GeneralWidget::removeElementHandle()
{
	if (auto rows = mTableView->selectionModel()->selectedRows(); rows.size() > 0) {
		int ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Czy na pewno chcesz usunąć zaznaczone elementy?"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::Yes) {
			for (auto& row : rows)
				mInvoiceItems->setData(row, true, Qt::CheckStateRole);

			mInvoiceItems->deleteSelectedItems();
		}
	}
	else {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Przed usunięciem elementów z listy musisz wybrać które elementy mają zostać usunięte"));

		return;
	}
}

void GeneralWidget::selectCorrectionInvoice(int invoiceId, InvoiceSelectror::InvoiceType type)
{
	if (type == widgets::InvoiceSelectror::InvoiceType::SaleInvoce) {
		const core::SaleInvoice invoice = core::SaleInvoice().findRecord(invoiceId);
		const QStringList num = invoice.number().split(' ').at(1).split('/');
		mInvoiceNumber->setValue(num[0].toInt());
		mInvoiceNumberMonth->setText(num[1]);
		mInvoiceYear->setText(num[2]);

		mContractorSelector->selectContractor(invoice.contractor());
		mContractorSelector->setDisabled(true);
		mInvoiceNumber->setDisabled(true);
	}
}

} // namespace eSawmill::invoices::widgets
