#include "widgets/invoiceselector.hpp"
#include <QHBoxLayout>
#include <memory>
#include <messagebox.h>
#include "sales/salesinvoicesmanager.hpp"
#include "purchase/purchaseinvoicesmanager.hpp"
#include <dbo/saleinvoice.hpp>
#include <dbo/purchaseinvoice.hpp>
#include <globaldata.hpp>


namespace eSawmill::invoices::widgets {

InvoiceSelectror::InvoiceSelectror(InvoiceType type, QWidget* parent)
	: QWidget {parent}
	, mInvoiceType {type}
	, mInvoiceId {0}
{
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	createWidgets();

	if (mInvoiceType == InvoiceType::PurchaseInvoce)
		connect(mSelectButton, &QPushButton::clicked, this, &InvoiceSelectror::selectPurchaseInvoiceHandle);
	else connect(mSelectButton, &QPushButton::clicked, this, &InvoiceSelectror::selectSaleInvoiceHandle);
}

void InvoiceSelectror::setInvoice(core::eloquent::DBIDKey invoiceId, InvoiceType type)
{
	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0)
		return;

	mInvoiceType = type;
	mInvoiceId = invoiceId;

	auto& repository = core::GlobalData::instance().repository().invoiceRepository();
	switch (type) {
	case InvoiceType::PurchaseInvoce: {
		core::PurchaseInvoice purchaseInvoice = repository.getPurchaseInvoice(invoiceId);
		mInvoiceNumber->setText(purchaseInvoice.number());
		} break;

	case InvoiceType::SaleInvoce: {
		core::SaleInvoice saleInvoice = repository.getSaleInvoice(invoiceId);
		mInvoiceNumber->setText(saleInvoice.number());
		} break;
	}
}

void InvoiceSelectror::setReadOnly(bool readonly)
{
	mSelectButton->setDisabled(readonly);
}

void InvoiceSelectror::createWidgets()
{
	mInvoiceNumber = new QLineEdit {this};
	mSelectButton = new QPushButton {this};
	mSelectButton->setIcon(QIcon(":/icons/invoice"));

	QHBoxLayout* layout = new QHBoxLayout {this};
	layout->setContentsMargins(QMargins(0, 0, 0, 0));
	layout->setSpacing(0);
	layout->addWidget(mInvoiceNumber);
	layout->addWidget(mSelectButton);

	mInvoiceNumber->setReadOnly(true);
	mSelectButton->setMaximumWidth(26);
}

void InvoiceSelectror::selectSaleInvoiceHandle()
{
	std::unique_ptr<eSawmill::invoices::sales::SalesInvoicesManager> manager = std::make_unique<eSawmill::invoices::sales::SalesInvoicesManager>(true, this);
	if (manager->exec() == QDialog::Accepted) {
		const core::SaleInvoice invoice = core::SaleInvoice().findRecord(manager->selectedInvoiceId());
		mInvoiceNumber->setText(invoice.number());
		mInvoiceId = invoice.id();

		emit invoiceSelected(mInvoiceId, InvoiceType::SaleInvoce);
	}
}

void InvoiceSelectror::selectPurchaseInvoiceHandle()
{
	std::unique_ptr<eSawmill::invoices::purchase::PurchaseInvoiceManager> manager = std::make_unique<eSawmill::invoices::purchase::PurchaseInvoiceManager>(true, this);
	if (manager->exec() == QDialog::Accepted) {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		const core::PurchaseInvoice invocie = repository.getPurchaseInvoice(manager->selectedInvoiceId());
		mInvoiceNumber->setText(invocie.number());
		mInvoiceId = invocie.id();

		emit invoiceSelected(mInvoiceId, InvoiceType::PurchaseInvoce);
	}
}

} // namespace eSawmill::invoices::widgets
