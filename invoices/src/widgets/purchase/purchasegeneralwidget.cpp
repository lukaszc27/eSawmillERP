#include "widgets/purchase/purchasegeneralwidget.hpp"
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QGroupBox>
#include <QInputDialog>
#include "models/paymentmethodmodel.hpp"
#include "delegates/purchases/purchaseinvoiceitemsdelegate.hpp"
#include <dbo/woodtype.hpp>
#include <articlesmanager.h>
#include <dialogs/orders/createelement.h>
#include <memory>
#include <eloquent/model.hpp>
#include <messagebox.h>
#include <dbo/purchaseinvoicecorrection.hpp>
#include <settings/appsettings.hpp>
#include <globaldata.hpp>

namespace eSawmill::invoices::widgets::purchase {

PurchaseGeneralWidget::PurchaseGeneralWidget(QWidget* parent)
	: QWidget(parent)
{
	createWidgets();
	createModels();
	createConnections();

	// when dialog start pay method set to cash method
	mPayMethod->currentTextChanged("Gotówka");
	mPayDeadlineDays->setValue(7);
	mDocumentType->addItems(QStringList()
		<< "FZ"
		<< "FZKOR"
	);
	mDocumentNumberYear->setText(QString::number(QDate::currentDate().year()));
	mDocumentNumberMonth->setText(QString().asprintf("%2.0d", QDate::currentDate().month()));

	for (auto& field : findChildren<QDoubleSpinBox*>())
		field->setReadOnly(true);
	mPaidPrice->setReadOnly(false);

	// adding menu on add button and table view context menu
	QAction* addArticleAction = new QAction("Artykuł", this);
	addArticleAction->setToolTip("Dodaje nowy artykuł do dokumentu");
	connect(addArticleAction, &QAction::triggered, this, &PurchaseGeneralWidget::addArticleHandle);

	QAction* addWoodElementAction = new QAction("Element drewniany", this);
	addWoodElementAction->setToolTip("Dodaje element drewniany na dokument");
	connect(addWoodElementAction, &QAction::triggered, this, &PurchaseGeneralWidget::addWoodElementDialog);

	QMenu* addButtonMenu = new QMenu(this);
	addButtonMenu->addAction(addArticleAction);
	addButtonMenu->addAction(addWoodElementAction);

	QMenu* contextMenu = new QMenu(this);
	QMenu* contextAddMenu = new QMenu("Dodaj", this);
	contextAddMenu->addAction(addArticleAction);
	contextAddMenu->addAction(addWoodElementAction);
	contextMenu->addMenu(contextAddMenu);
	contextMenu->addAction(QIcon(":/icons/trash"), "Usuń", this, &PurchaseGeneralWidget::removeSelectedItems);
	mAddButton->setMenu(addButtonMenu);
	mTableView->setContextMenu(contextMenu);
}

core::PurchaseInvoice PurchaseGeneralWidget::purchaseInvoice() const
{
	// get warehouse for invoice
	const auto& warehouse = core::GlobalData::instance()
		.repository()
		.warehousesRepository()
		.getWarehouse(mWarehouse->currentData(
			eSawmill::articles::models::WarehouseList::Roles::Id).toInt()
		);

	core::PurchaseInvoice purchaseInvoice;
	purchaseInvoice.setDocumentPrefix(mDocumentPrefix->text().toUpper());
	purchaseInvoice.setForeignNumber(mForeignNumber->text());
	purchaseInvoice.setDateOfReceipt(mDateOfDepature->date());
	purchaseInvoice.setDateOfIssue(mDateOfIssue->date());
	purchaseInvoice.setDateOfPurchase(mDateOfPurchase->date());
	purchaseInvoice.setRebate(mRebate->value());
	purchaseInvoice.setPayment(mPayMethod->currentText());
	purchaseInvoice.setPaymentDeadlineDate(mPayDeadline->date());
	purchaseInvoice.setPaymentDeadlineDays(mPayDeadlineDays->value());
	purchaseInvoice.setNettoPrice(mNettoPrice->value());
	purchaseInvoice.setTotalPrice(mTotalPrice->value());
	purchaseInvoice.setPaidPrice(mPaidPrice->value());
	purchaseInvoice.setToPaidPrice(mToPayPrice->value());
	purchaseInvoice.setContractor(mContractor->contractor());
	purchaseInvoice.setWarehouse(warehouse);
	purchaseInvoice.setPaid(mToPayPrice->value() == 0 || (mTotalPrice->value() == mPaidPrice->value()));

	if (isCorrectionInvoice()) {
		// this document is correction for other invoice
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		core::PurchaseInvoice firstInvoice = repository.getPurchaseInvoice(mInvoiceSelector->selectedInvoiceId());
		const QString number = QString("%1 %2")
			.arg(mDocumentPrefix->text().toUpper())
			.arg(firstInvoice.number().split(' ').at(1));

		purchaseInvoice.setNumber(number);
	}

	return purchaseInvoice;
}

void PurchaseGeneralWidget::loadInvoice(core::PurchaseInvoice &invoice)
{
	QStringList numberParts = invoice.number().split(' ');
	QStringList dateParts = numberParts[1].split('/');

	mDocumentType->setCurrentText(numberParts[0]);
	mDocumentPrefix->setText(numberParts[0]);
	mDocumentNumber->setValue(dateParts[0].toInt());
	mDocumentNumberMonth->setText(dateParts[1]);
	mDocumentNumberYear->setText(dateParts[2]);

	if (numberParts[0].contains("KOR", Qt::CaseInsensitive)) {
		core::PurchaseInvoiceCorrection correction;
		correction = correction.builder().where("CorrectionId", invoice.id()).orderBy("CreatedDate").first();
		mInvoiceSelector->setInvoice(correction.invoice(true), widgets::InvoiceSelectror::InvoiceType::PurchaseInvoce);
		mInvoiceSelector->setReadOnly(true);
	}

	mForeignNumber->setText(invoice.foreignNumber());
	mContractor->selectContractor(invoice.contractor());
	mDateOfDepature->setDate(invoice.dateOfReceipt());
	mWarehouse->setCurrentText(invoice.warehouse().name());
	mDateOfIssue->setDate(invoice.dateOfIssue());
	mDateOfPurchase->setDate(invoice.dateOfPurchase());
	mRebate->setValue(invoice.rebate());
	mPayMethod->setCurrentText(invoice.payment());
	mPayDeadlineDays->setValue(invoice.paymentDeadlineDays());
	mPayDeadline->setDate(invoice.paymentDeadlineDate());
	mNettoPrice->setValue(invoice.nettoPrice());
	mTotalPrice->setValue(invoice.totalPrice());
	mPaidPrice->setValue(invoice.paidPrice());
	mToPayPrice->setValue(invoice.toPaidPrice());
	// load items data to model
	mInvoiceItemsModel->loadInvoiceItems(invoice);

	// when invoice was loaded
	// user can't change number
	mDocumentNumber->setReadOnly(true);
}

void PurchaseGeneralWidget::createWidgets()
{
	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(createDocumentTypeWidgets());
	mainLayout->addLayout(createForeignDocumentWidgets());
	mainLayout->addSpacing(6);
	mainLayout->addWidget(createHeaderWidgets());
	mainLayout->addSpacing(3);
	mainLayout->addWidget(createDateAndValuesWidgets());

	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton("Dodaj", QKeySequence("Ins"), this, QIcon(":/icons/add"));
	mRemoveButton = new ::widgets::PushButton("Usuń", QKeySequence("ESC"), this, QIcon(":/icons/trash"));

	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);

	mainLayout->addSpacing(6);
	mainLayout->addWidget(mTableView);
	mainLayout->addLayout(buttonsLayout);

	for (auto& field : findChildren<QDateEdit*>()) {
		field->setCalendarPopup(true);
		field->setDate(QDate::currentDate());
	}

	for (auto& field : findChildren<QDoubleSpinBox*>())
		field->setMaximum(1'000'000);
}

void PurchaseGeneralWidget::createModels()
{
	mPayMethod->setModel(new eSawmill::invoices::models::PaymentMethodModel(this));

	mWarehouseModel = new eSawmill::articles::models::WarehouseList(this);
	mInvoiceItemsModel = new eSawmill::invoices::models::purchases::InvoiceItemModel(this);

	mWarehouse->setModel(mWarehouseModel);
	mTableView->setModel(mInvoiceItemsModel);
	mTableView->setItemDelegate(new eSawmill::invoices::delegates::purchases::PurchaseInvoiceItemsDelegate(this));
}

void PurchaseGeneralWidget::createConnections()
{
	connect(mPayMethod, &QComboBox::currentTextChanged, this, [this](const QString& text){
		if (text.toUpper() == "GOTÓWKA") {
			mPayDeadline->setDisabled(true);
			mPayDeadlineDays->setDisabled(true);
		}
		else {
			mPayDeadline->setDisabled(false);
			mPayDeadlineDays->setDisabled(false);
		}
	});
	connect(mDateOfIssue, &QDateEdit::dateChanged, this, [this](const QDate& date){
		mPayDeadline->setDate(date.addDays(mPayDeadlineDays->value()));
	});
	connect(mPayDeadlineDays, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int days){
		mPayDeadline->setDate(mDateOfIssue->date().addDays(days));
	});
	connect(mPayDeadline, &QDateEdit::dateChanged, this, [this](const QDate& date){
		mPayDeadlineDays->setValue(mDateOfIssue->date().daysTo(date));
	});
	connect(mDocumentType, &QComboBox::currentTextChanged, this, [&](const QString& text){
		mDocumentPrefix->setText(text);
		mForeignInvoice->setVisible(isCorrectionInvoice());
	});
	connect(mTotalPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double price){
		mToPayPrice->setValue(price - mPaidPrice->value());
	});
	connect(mInvoiceItemsModel, &eSawmill::invoices::models::purchases::InvoiceItemModel::dataChanged, this, [&](){
		double price = mInvoiceItemsModel->calculateTotalItemsValue();
		mNettoPrice->setValue(price);
		mTotalPrice->setValue(price);
	});
	connect(mPaidPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double val){
		mToPayPrice->setValue(mTotalPrice->value() - val);
	});
	connect(mRebate, QOverload<int>::of(&QSpinBox::valueChanged), this, [&](int val){
		double totalPrice = mInvoiceItemsModel->calculateTotalItemsValue();
		double rebate = (100.0f - val) / 100.0f;

		mTotalPrice->setValue(totalPrice * rebate);
	});
	connect(mInvoiceSelector, &widgets::InvoiceSelectror::invoiceSelected, this, &PurchaseGeneralWidget::selectCorrectionInvoice);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &PurchaseGeneralWidget::removeSelectedItems);
}

QLayout* PurchaseGeneralWidget::createDocumentTypeWidgets()
{
	mDocumentType = new QComboBox(this);
	mDocumentPrefix = new QLineEdit(this);
	mDocumentNumber = new ::widgets::InvoiceNumberSpinBox(this);
	mDocumentNumberMonth = new QLineEdit(this);
	mDocumentNumberYear = new QLineEdit(this);
	mForeignInvoice = new QFrame(this);
	mInvoiceSelector = new widgets::InvoiceSelectror(widgets::InvoiceSelectror::InvoiceType::PurchaseInvoce, this);

	mDocumentType->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDocumentPrefix->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDocumentNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDocumentNumberMonth->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDocumentNumberYear->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mInvoiceSelector->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mDocumentPrefix->setMaximumWidth(75);
	mDocumentNumber->setMaximumWidth(110);
	mDocumentNumberYear->setMaximumWidth(64);
	mDocumentNumberMonth->setMaximumWidth(32);

	mDocumentPrefix->setDisabled(true);
	mDocumentNumberYear->setDisabled(true);
	mDocumentNumberMonth->setDisabled(true);

	mForeignInvoice->setFrameShape(QFrame::StyledPanel);
	mForeignInvoice->setVisible(false);
	QVBoxLayout* foreignInvoiceLayout = new QVBoxLayout(mForeignInvoice);
	foreignInvoiceLayout->addWidget(new QLabel(tr("<b>Dokument korygowany</b>"), mForeignInvoice));
	foreignInvoiceLayout->addWidget(mInvoiceSelector);

	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(new QLabel("Dokument", this), 0, Qt::AlignTop);
	layout->addWidget(mDocumentType, 0, Qt::AlignTop);
	layout->addSpacing(24);
	layout->addWidget(new QLabel("Numer", this), 0, Qt::AlignTop);
	layout->addWidget(mDocumentPrefix, 0, Qt::AlignTop);
	layout->addWidget(mDocumentNumber, 0, Qt::AlignTop);
	layout->addWidget(mDocumentNumberMonth, 0, Qt::AlignTop);
	layout->addWidget(mDocumentNumberYear, 0, Qt::AlignTop);
	layout->addStretch(1);
	layout->addWidget(mForeignInvoice, 0, Qt::AlignRight | Qt::AlignTop);

	return layout;
}

QLayout* PurchaseGeneralWidget::createForeignDocumentWidgets()
{
	mDateOfDepature = new QDateEdit(this);
	mForeignNumber = new QLineEdit(this);

	mDateOfDepature->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mForeignNumber->setMinimumWidth(220);

	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(new QLabel("<b>Numer obcy</b>", this));
	layout->addWidget(mForeignNumber);
	layout->addSpacing(24);
	layout->addStretch(1);
	layout->addWidget(new QLabel("Data wpływu", this));
	layout->addWidget(mDateOfDepature, Qt::AlignRight);

	return layout;
}

QWidget* PurchaseGeneralWidget::createHeaderWidgets()
{
	QGroupBox* groupbox = new QGroupBox("Nagłówek", this);
	groupbox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	mContractor = new eSawmill::contractors::widgets::ContractorSelector(this);
	mWarehouse = new QComboBox(this);
	mWarehouse->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* layout = new QFormLayout(groupbox);
	layout->addRow("Kontrahent", mContractor);
	layout->addRow("Magazyn", mWarehouse);

	return groupbox;
}

QWidget* PurchaseGeneralWidget::createDateAndValuesWidgets()
{
	QGroupBox* groupbox = new QGroupBox("Daty i wartości", this);

	mDateOfIssue = new QDateEdit(this);
	mDateOfPurchase = new QDateEdit(this);
	mRebate = new QSpinBox(this);
	mPayMethod = new QComboBox(this);
	mPayDeadline = new QDateEdit(this);
	mPayDeadlineDays = new QSpinBox(this);

	mNettoPrice = new QDoubleSpinBox(this);
	mTotalPrice = new QDoubleSpinBox(this);
	mPaidPrice = new QDoubleSpinBox(this);
	mToPayPrice = new QDoubleSpinBox(this);

	QFont boldFont;
	boldFont.setBold(true);
	mTotalPrice->setFont(boldFont);

	mPayMethod->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPayDeadline->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPayDeadlineDays->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QGroupBox* dateGroup = new QGroupBox(this);
	QFormLayout* dateLayout = new QFormLayout(dateGroup);
	dateLayout->addRow("Data wystawienia", mDateOfIssue);
	dateLayout->addRow("Data zakupu", mDateOfPurchase);

	QHBoxLayout* paymentDeadlineLayout = new QHBoxLayout();
	paymentDeadlineLayout->addStretch(1);
	paymentDeadlineLayout->addWidget(mPayDeadlineDays);
	paymentDeadlineLayout->addWidget(new QLabel("dni", this));
	paymentDeadlineLayout->addSpacing(24);
	paymentDeadlineLayout->addWidget(mPayDeadline);

	auto addUnit = [this](QWidget* field, const QString& unit = "PLN") -> QLayout* {
		QLabel* label = new QLabel(unit, this);
		label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

		QHBoxLayout* layout = new QHBoxLayout();
		layout->addWidget(field);
		layout->addWidget(label);
		return layout;
	};

	QGroupBox* paymentGroup = new QGroupBox(this);
	QFormLayout* paymentLayout = new QFormLayout(paymentGroup);
	paymentLayout->addRow("Rabat", addUnit(mRebate, "%"));
	paymentLayout->addRow("Płatność", mPayMethod);
	paymentLayout->addRow("Termin", paymentDeadlineLayout);

	QGroupBox* priceGroup = new QGroupBox(this);
	QFormLayout* priceLayout = new QFormLayout(priceGroup);
	priceLayout->addRow("Netto", addUnit(mNettoPrice));
	priceLayout->addRow("<b>Razem</b>", addUnit(mTotalPrice));
	priceLayout->addRow("Zapłacono", addUnit(mPaidPrice));
	priceLayout->addRow("Pozostaje", addUnit(mToPayPrice));

	QHBoxLayout* mainLayout = new QHBoxLayout(groupbox);
	mainLayout->addWidget(dateGroup);
	mainLayout->addWidget(paymentGroup);
	mainLayout->addWidget(priceGroup);

	return groupbox;
}

void PurchaseGeneralWidget::addArticleHandle() {
	std::unique_ptr<eSawmill::articles::ArticlesManager> manager = std::make_unique<eSawmill::articles::ArticlesManager>(this, true);
	if (manager->exec() == QDialog::Accepted) {
		int qty = QInputDialog::getInt(this, "Ilość na dokumęcie", "Ilość", 1, 0);

		core::Article sa = manager->article();
		double price = sa.priceSaleBrutto();

		core::PurchaseInvoiceItem model;
		model.setName(sa.name());
		model.setQuantity(qty);
		model.setMeasureUnit(sa.unit().shortName());
		model.setRebate(0);
		model.setPrice(price);
		model.setValue(double(price * qty));
		model.setWarehouse(sa.warehouse().name());
		model.setOrginalArticle(sa.id());

		mInvoiceItemsModel->insert(model);
	}
}

void PurchaseGeneralWidget::addWoodElementDialog() {
	std::unique_ptr<::widgets::dialogs::orders::CreateElement> creator = std::make_unique<::widgets::dialogs::orders::CreateElement>(this);
	if (creator->exec() == QDialog::Accepted) {
		const core::order::Element element = creator->element();
		core::PurchaseInvoiceItem model;

		QString elementName;
		QTextStream name(&elementName);
		core::WoodType woodType = element.woodType();

		if (element.name().isEmpty())
			name << "ELEMENT ";
		else name << element.name() << " ";

		name << element.width() << "X"
			 << element.height() << "X"
			 << element.length() << "M - "
			 << woodType.name();

		if (element.planned())
			name << " (STRUGANY)";

		double qty = element.quantity();
		double price = core::settings::OrderSettings::instance().defaultPrice();
		double plannedPrice = core::settings::OrderSettings::instance().defaultPlannedPrice();
		double stere = element.stere() / qty;

		/// calclate price for specific wood type
		price *= woodType.get("Factor").toDouble();

		/// calculate price for element
		price *= stere;
		if (element.planned())
			price += (stere * plannedPrice);

		model.setName(elementName);
		model.setQuantity(qty);
		model.setMeasureUnit(tr("SZT"));
		model.setRebate(0);
		model.setPrice(price);
		model.setValue(double(price * qty));
		model.setWarehouse("");

		mInvoiceItemsModel->insert(model);
	}
}

void PurchaseGeneralWidget::removeSelectedItems() {
	if (auto rows = mTableView->selectionModel()->selectedRows(); rows.size() > 0) {
		int ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
						tr("Znaleziono: %1 elementów do usunięcia. Czy na pewno chcesz je usunąć?").arg(rows.size()),
						"", QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::Yes) {
			for (auto& row : rows)
				mInvoiceItemsModel->setData(row, true, Qt::CheckStateRole);
			mInvoiceItemsModel->deleteSelectedItems();
		}
	}
	else {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Przed usunięciem elementów z listy musisz zaznaczyć elementy które mają zostać usunięte"));
		return;
	}
}

void PurchaseGeneralWidget::selectCorrectionInvoice(int invoiceId, widgets::InvoiceSelectror::InvoiceType type)
{
	if (type == widgets::InvoiceSelectror::InvoiceType::PurchaseInvoce) {
		auto& repository = core::GlobalData::instance().repository().invoiceRepository();
		core::PurchaseInvoice invoice = repository.getPurchaseInvoice(invoiceId);
		mForeignNumber->setText(invoice.foreignNumber());
		mContractor->selectContractor(invoice.contractor());
		mDateOfDepature->setDate(invoice.dateOfReceipt());

		// after that disable widgets
		mForeignNumber->setReadOnly(true);
		mContractor->setDisabled(true);
		mDateOfDepature->setReadOnly(true);
		mDocumentNumber->setDisabled(true);

		// show invoice number
		QStringList num = invoice.number().split(' ').at(1).split('/');
		mDocumentNumber->setValue(num[0].toInt());
		mDocumentNumberMonth->setText(num[1]);
		mDocumentNumberYear->setText(num[2]);
	}
}

} // namespace
