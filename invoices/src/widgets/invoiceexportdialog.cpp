#include "widgets/invoiceexportdialog.hpp"
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <pushbutton.h>
#include <messagebox.h>
#include <models/warehouselist.h>


namespace eSawmill::invoices::widgets {

InvoiceExportDialog::InvoiceExportDialog(QWidget* parent)
	: QDialog {parent}
{
	setWindowTitle(tr("Generowanie dokumentu sprzedaży"));
	createAllWidgets();
	createModels();
	createConnections();

	mPayment->currentTextChanged(tr("Gotówka"));
	mPaymentDays->setValue(7);
}

core::eloquent::DBIDKey InvoiceExportDialog::warehouseId() const
{
	return mWarehouse->currentData(eSawmill::articles::models::WarehouseList::Roles::Id).toInt();
}

void InvoiceExportDialog::createAllWidgets()
{
	mWarehouse	 = new QComboBox {this};
	mPayment	 = new QComboBox {this};
	mPaymentDays = new QSpinBox {this};
	mPaymentDate = new QDateEdit {this};
	::widgets::PushButton* acceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/accept"));
	::widgets::PushButton* rejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/reject"));

	mPaymentDate->setCalendarPopup(true);
	mPaymentDate->setMinimumDate(QDate::currentDate());

	QFormLayout* formLayout = new QFormLayout {};
	formLayout->addRow(tr("Magazyn"), mWarehouse);
	formLayout->addRow(tr("Płatność"), mPayment);

	QHBoxLayout* paymentRow = new QHBoxLayout {};
	paymentRow->addWidget(mPaymentDays);
	paymentRow->addWidget(new QLabel {tr("dni"), this});
	paymentRow->addSpacing(12);
	paymentRow->addWidget(mPaymentDate);
	formLayout->addRow(tr("Termin"), paymentRow);

	QHBoxLayout* buttonsLayout = new QHBoxLayout {};
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(acceptButton);
	buttonsLayout->addWidget(rejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout {this};
	mainLayout->addLayout(formLayout);
	mainLayout->addSpacing(8);
	mainLayout->addLayout(buttonsLayout);

	// create connections only for accept and reject button
	connect(acceptButton, &::widgets::PushButton::clicked, this, &InvoiceExportDialog::accept);
	connect(rejectButton, &::widgets::PushButton::clicked, this, &InvoiceExportDialog::reject);
}

void InvoiceExportDialog::createModels()
{
	mPayment->addItems(QStringList() << tr("Gotówka") << tr("Przelew"));
	mWarehouse->setModel(new eSawmill::articles::models::WarehouseList {this});
}

void InvoiceExportDialog::createConnections()
{
	connect(mPayment, &QComboBox::currentTextChanged, this, [&](const QString& value){
		const bool enable = value.toUpper().compare("GOTÓWKA") == 0;
		mPaymentDate->setDisabled(enable);
		mPaymentDays->setDisabled(enable);
	});

	connect(mPaymentDays, QOverload<int>::of(&QSpinBox::valueChanged), this, [&](int value){
		const QDate date = QDate::currentDate();
		mPaymentDate->setDate(date.addDays(value));
	});
	connect(mPaymentDate, &QDateEdit::dateChanged, this, [&](const QDate& date){
		const QDate currentDate = QDate::currentDate();
		int days = currentDate.daysTo(date);
		if (days < 0) {
			::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
				tr("Data terminu płatności jest przed datą aktualną!"));

			QDateEdit* dateEdit = qobject_cast<QDateEdit*>(sender());
			dateEdit->setDate(QDate::currentDate());
			return;
		}
		mPaymentDays->setValue(days);
	});
}

} // namespace eSawmill::invoices::widgets
