#include "commands/createcontractorcommand.hpp"

namespace eSawmill::contractors::commands {

CreateContractorCommand::CreateContractorCommand(const core::Contractor& contractor, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _contractor {contractor}
{
}

bool CreateContractorCommand::execute(core::repository::AbstractRepository& repository) {
	bool success = repository.contractorRepository().create(_contractor);
	if (success) {
		Q_ASSERT(_contractor.id() > 0);
	}
	return success;
}

bool CreateContractorCommand::undo(core::repository::AbstractRepository& repository) {
	Q_ASSERT(_contractor.id() > 0);
	return repository.contractorRepository().destroy(_contractor.id());
}

} // namespace eSawmill::contractors::commands
