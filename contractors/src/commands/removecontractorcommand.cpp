#include "commands/removecontractorcommand.hpp"

namespace eSawmill::contractors::commands {

RemoveContractorCommand::RemoveContractorCommand(const core::eloquent::DBIDKey& contractorId, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _contractorId {contractorId}
{
}

bool RemoveContractorCommand::execute(core::repository::AbstractRepository& repository) {
	// save object removed contractor
	_removedContractor = repository.contractorRepository().get(_contractorId);
	return repository.contractorRepository().destroy(_contractorId);
}

bool RemoveContractorCommand::undo(core::repository::AbstractRepository& repository) {
	return repository.contractorRepository().create(_removedContractor);
}

} // namespace eSawmill::contractors::commands
