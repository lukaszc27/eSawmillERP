#include "commands/updatecontractorcommand.hpp"

namespace eSawmill::contractors::commands {

UpdateContractorCommand::UpdateContractorCommand(const core::Contractor& contractor, QWidget* parent)
	: core::abstracts::AbstractCommand {parent}
	, _contractor {contractor}
{
	Q_ASSERT(_contractor.id() > 0);
}

bool UpdateContractorCommand::execute(core::repository::AbstractRepository& repository) {
	_beforeEditingContractor = repository.contractorRepository().get(_contractor.id());
	_beforeEditingContractor.makeDirty();
	return repository.contractorRepository().update(_contractor);
}

bool UpdateContractorCommand::undo(core::repository::AbstractRepository& repository) {
	return repository.contractorRepository().update(_beforeEditingContractor);
}

} // namespace eSawmill::contractors::commands
