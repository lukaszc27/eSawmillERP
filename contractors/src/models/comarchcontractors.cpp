#include "models/comarchcontractors.hpp"
#include <messagebox.h>


namespace eSawmill::contractors::models {

ComarchContractorsModel::ComarchContractorsModel(QObject* parent)
	: core::models::AbstractModel<core::comarch::Contractor> {parent}
{
	get();
	mColumnName << tr("Nazwa") << tr("NIP") << tr("REGON")
				<< tr("Miejscowość") << tr("Ulica") << tr("Nr budynku")
				<< tr("Kod pocztowy") << tr("Poczta") << tr("Telefon") << tr("E-Mail");
}

ComarchContractorsModel::~ComarchContractorsModel()
{
}

Qt::ItemFlags ComarchContractorsModel::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = core::models::AbstractModel<core::comarch::Contractor>::flags(index);
	flags |= Qt::ItemIsEnabled;

	if (!index.column())
		flags |= Qt::ItemIsUserCheckable;

	return flags;
}

int ComarchContractorsModel::rowCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mList.size();
}

int ComarchContractorsModel::columnCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mColumnName.size();
}

QVariant ComarchContractorsModel::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return mColumnName.at(section);
		break;
	}
	return QVariant();
}

QVariant ComarchContractorsModel::data(const QModelIndex &index, int role) const {
	int row = index.row();
	int col = index.column();
	core::comarch::Contractor current = items().at(row);

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::Name: return current.name();
		case Columns::NIP: return current.nip();
		case Columns::REGON: return current.regon();
		case Columns::City: return current.city();
		case Columns::Street: return current.street();
		case Columns::HomeNumber: return current.homeNumber();
		case Columns::PostCode: return current.postCode();
		case Columns::PostOffice: return current.postOffice();
		case Columns::Phone: return current.phone();
		case Columns::Email: return current.email();
		}
		break;

	case Roles::Id:
		return current.get("Knt_KntId").toInt();

	case Qt::CheckStateRole:
		if (col == 0)
			return items().at(row).isChecked() ? Qt::Checked : Qt::Unchecked;
		break;
	}
	return QVariant();
}

bool ComarchContractorsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	switch (role) {
	case Qt::CheckStateRole:
		if (!index.column())
			mList[index.row()].setChecked(value.toBool());
		break;
	}
	return true;
}

void ComarchContractorsModel::get()
{
	try {
		beginResetModel();
//		mList = core::comarch::ContractorComarch::all();
//		mList = core::eloquent::Model::all<core::comarch::Contractor>();
		endResetModel();
	}
	catch (const QString msg) {
		::widgets::MessageBox::critical(nullptr, tr("SQL Syntax Error"), msg);
	}
}

void ComarchContractorsModel::createOrUpdate(core::comarch::Contractor contractor)
{
	try {
		//core::comarch::Contractor().createOrUpdate(contractor);;
		get();
	}
	catch (QString msg) {
		::widgets::MessageBox::critical(nullptr, tr("Comarch SQL Error"),
										tr("Błąd dodawania nowego kontrahenta do bazy Comarch ERP"), msg);
	}
}

} // namespace eSawmill
