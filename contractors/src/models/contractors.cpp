#include "models/contractors.h"
#include <messagebox.h>
#include <globaldata.hpp>


namespace eSawmill::contractors::models {

Contractors::Contractors(QObject* parent)
	: core::models::AbstractModel<core::Contractor> {parent}
{
	core::GlobalData::instance().commandExecutor().attachListener(this);
	get();
}

Contractors::~Contractors()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);
}

QStringList Contractors::headerData() const {
	return QStringList()
			<< tr("Nazwa")
			<< tr("Imię")
			<< tr("Nazwisko")
			<< tr("NIP")
			<< tr("REGON")
			<< tr("PESEL")
			<< tr("Miejscowość")
			<< tr("Kod pocztowy")
			<< tr("Ulica")
			<< tr("Nr lokalu")
			<< tr("Telefon")
			<< tr("E-Mail");
}

int Contractors::rowCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return items().size();
}

int Contractors::columnCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return headerData().size();
}

Qt::ItemFlags Contractors::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = core::models::AbstractModel<core::Contractor>::flags(index);
	flags |= Qt::ItemIsEnabled;
	if (!index.column())
		flags |= Qt::ItemIsUserCheckable;

	return flags;
}

QVariant Contractors::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (role == Qt::DisplayRole) {
			return headerData().at(section);
		}
		break;

	case Qt::Vertical:
		break;
	};

	return QVariant();
}

QVariant Contractors::data(const QModelIndex &index, int role) const {
	const core::Contractor& current = items().at(index.row());
	switch(role) {
	case Qt::DisplayRole: {
		switch (index.column()) {
		case Name: return current.name();
		case PersonName: return current.personName();
		case PersonSurname: return current.personSurname();
		case NIP: return current.NIP();
		case REGON: return current.REGON();
		case PESEL: return current.PESEL();
		case City: return current.contact().city();
		case ZipCode: return current.contact().postCode();
		case Street: return current.contact().street();
		case HomeNumber: return current.contact().homeNumber();
		case Phone: return current.contact().phone();
		case EMail: return current.contact().email();
		}
	} break;

	case Qt::ForegroundRole:
		if (current.blocked())
			return QColor(Qt::darkRed);

		if (current.comarchId() > 0)
			return QColor(Qt::blue);
		break;

	case Roles::ContractorType:
		if (current.provider())
			return ContractorType::Provider;

		return ContractorType::Customer;
		break;

	case Roles::ContractorId:
		return current.id();
		break;
	}
	return QVariant();
}

bool Contractors::setData(const QModelIndex &index, const QVariant &value, int role) {
	Q_UNUSED(index);
	Q_UNUSED(value);
	Q_UNUSED(role);
//	switch (role) {
//	case Qt::CheckStateRole:
//		if (!index.column())
//			mList[index.row()].setChecked(value.toBool());
//		break;
//	}
	return true;
}

void Contractors::get() {
	try {
		beginResetModel();
		auto& repository = core::GlobalData::instance().repository().contractorRepository();
		mList = repository.getAll();
		endResetModel();
	}
	catch (const QString& msg) {
		Q_UNUSED(msg);
		core::GlobalData::instance().logger().warning("Błąd pobierania obiektów kontrahentów");
		throw;
	}
}

void Contractors::createOrUpdate(core::Contractor contrator) {
	Q_UNUSED(contrator);
}

void Contractors::destroy(core::Contractor contractor) {
	Q_UNUSED(contractor);
}

void Contractors::commandExecuted()
{
	get();
}

} // namespace eSawmill
