#include "filters/contractorfilter.hpp"
#include "models/contractors.h"

namespace eSawmill::contractors::filters {

ContractorFilter::ContractorFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mIsActive(false)
{
}

void ContractorFilter::setActive(bool active) {
	mIsActive = active;
	invalidateFilter();
}

void ContractorFilter::setName(const QString &name) {
	mName = name;
	invalidateFilter();
}

void ContractorFilter::setNip(const QString &nip) {
	mNip = nip;
	invalidateFilter();
}

void ContractorFilter::setPesel(const QString &pesel) {
	mPesel = pesel;
	invalidateFilter();
}

void ContractorFilter::setPhone(const QString &phone) {
	mPhone = phone;
	invalidateFilter();
}

void ContractorFilter::clearAllFilters()
{
	mName = "";
	mPersonName = "";
	mPersonSurname = "";
	mNip = "";
	mPesel = "";
	mPhone = "";
	invalidateFilter();
}

void ContractorFilter::setPersonName(const QString &personName) {
	mPersonName = personName;
	invalidateFilter();
}

void ContractorFilter::setPersonSurname(const QString &personSurname) {
	mPersonSurname = personSurname;
	invalidateFilter();
}

bool ContractorFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	Q_UNUSED(source_parent);

	if (mIsActive) {
		return filterByName(sourceModel()->index(source_row, models::Contractors::Name).data().toString()) &&
				filterByPersonName(sourceModel()->index(source_row, models::Contractors::PersonName).data().toString()) &&
				filterByPersonSurname(sourceModel()->index(source_row, models::Contractors::PersonSurname).data().toString()) &&
				filterByNip(sourceModel()->index(source_row, models::Contractors::NIP).data().toString()) &&
				filterByPesel(sourceModel()->index(source_row, models::Contractors::PESEL).data().toString()) &&
				filterByPhone(sourceModel()->index(source_row, models::Contractors::Phone).data().toString());
	}

	// if user don't check filter group checkbox,
	// filter must show all records in view
	return true;
}

bool ContractorFilter::filterByName(const QString &value) const {
	return value.toUpper().contains(mName.toUpper());
}

bool ContractorFilter::filterByPersonName(const QString &value) const {
	return value.toUpper().contains(mPersonName.toUpper());
}

bool ContractorFilter::filterByPersonSurname(const QString &value) const {
	return value.toUpper().contains(mPersonSurname.toUpper());
}

bool ContractorFilter::filterByNip(const QString &value) const {
	return value.toUpper().contains(mNip);
}

bool ContractorFilter::filterByPesel(const QString &value) const {
	return value.contains(mPesel);
}

bool ContractorFilter::filterByPhone(const QString &value) const {
	return value.contains(mPhone);
}

} // namespace eSawmill
