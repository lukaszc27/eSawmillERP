#include "filters/contractorrolefilter.h"
#include <QMessageBox>


namespace eSawmill::contractors::filters {

ContractorRoleFilter::ContractorRoleFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mFilterType(models::Contractors::Customer)
{
}

bool ContractorRoleFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	QModelIndex index = sourceModel()->index(source_row, 0, source_parent);

	switch (mFilterType) {
	case models::Contractors::Customer:
		return index.data(models::Contractors::Roles::ContractorType).toInt() == models::Contractors::Customer;
		break;

	case models::Contractors::Provider:
		return index.data(models::Contractors::Roles::ContractorType).toInt() == models::Contractors::Provider;
		break;
	}
	return false;
}

} // namespace eSawmill
