#include "filters/comarchcontractorfilter.hpp"
#include "models/comarchcontractors.hpp"


namespace eSawmill::contractors::filters {

ComarchContractorFilter::ComarchContractorFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mIsActive(false)
{
}

bool ComarchContractorFilter::checkName(const QString name) const {
	if (name.isEmpty()) return true;
	return name.contains(mName, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkNip(const QString nip) const {	
	if (nip.isEmpty()) return true;
	return nip.contains(mNIP, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkRegon(const QString regon) const {
	if (regon.isEmpty()) return true;
	return regon.contains(mREGON, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkCity(const QString city) const {
	if (city.isEmpty()) return true;
	return city.contains(mCity, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkStreet(const QString street) const {
	if (street.isEmpty()) return true;
	return street.contains(mStreet, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkPostCode(const QString postCode) const {
	if (postCode.isEmpty()) return true;
	return postCode.contains(mPostCode, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::checkPhone(const QString phone) const {
	if (phone.isEmpty()) return true;
	return phone.contains(mPhone, Qt::CaseSensitivity::CaseInsensitive);
}

bool ComarchContractorFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	if (!mIsActive)
		return true;

	const QString name = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::Name, source_parent).data().toString();
	const QString nip = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::NIP, source_parent).data().toString();
	const QString regon = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::REGON, source_parent).data().toString();
	const QString city = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::City, source_parent).data().toString();
	const QString street = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::Street, source_parent).data().toString();
	const QString postCode = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::PostCode, source_parent).data().toString();
	const QString phone = sourceModel()->index(source_row, models::ComarchContractorsModel::Columns::Phone, source_parent).data().toString();

	return checkName(name)
			&& checkNip(nip)
			&& checkRegon(regon)
			&& checkCity(city)
			&& checkStreet(street)
			&& checkPostCode(postCode)
			&& checkPhone(phone);
}

} // namespace eSawmill
