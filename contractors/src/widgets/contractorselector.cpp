#include "widgets/contractorselector.h"
#include <QHBoxLayout>
#include <QScopedPointer>

#include "contractormanager.h"


namespace eSawmill::contractors::widgets {

ContractorSelector::ContractorSelector(QWidget* parent)
	: QWidget(parent)
{
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	createWidgets();

	connect(mButton, &QPushButton::clicked, this, &ContractorSelector::button_Clicked);
	connect(mNameView, &LineEdit::doubleClicked, this, [&] { button_Clicked(); });
	connect(this, &ContractorSelector::contractorSelected,
			this, &ContractorSelector::userSelectContractor);
}

void ContractorSelector::setReadOnly(bool readonly)
{
	mNameView->setReadOnly(readonly);
	mButton->setEnabled(!readonly);
}

void ContractorSelector::createWidgets()
{
	mNameView = new LineEdit(this);
	mNameView->setReadOnly(true);

	mButton = new QPushButton(this);
	mButton->setMaximumWidth(36);
	mButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mButton->setIcon(QIcon(":/icons/contractors"));

	QHBoxLayout* mainLayout = new QHBoxLayout(this);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(QMargins(0, 0, 0, 0));
	mainLayout->addWidget(mNameView);
	mainLayout->addWidget(mButton);
}

void ContractorSelector::button_Clicked()
{
	QScopedPointer<ContractorManager> manager(new ContractorManager(this, true, true));
	if (manager->exec() == QDialog::Accepted) {
		core::Contractor c = manager->selectedContractor();
		mCurrentSelectedContractor = c;
		emit contractorSelected(c);
	}
}

void ContractorSelector::userSelectContractor(core::Contractor c)
{
	auto name = c.name();
	if (name.isEmpty())
		name = QString("%1 %2").arg(c.personName(), c.personSurname());

	if (!c.NIP().isEmpty())
		name.append(QString(" [%1]").arg(c.NIP()));

	mNameView->setText(name);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
LineEdit::LineEdit(QWidget* parent)
	: QLineEdit{parent}
{
}

void LineEdit::mouseDoubleClickEvent(QMouseEvent* event)
{
	QLineEdit::mouseDoubleClickEvent(event);
	Q_EMIT doubleClicked();
}

} // namespace eSawmill
