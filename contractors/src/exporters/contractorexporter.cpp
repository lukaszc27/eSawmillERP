#include "exporters/contractorexporter.hpp"
#include <dbo/contact.h>


namespace eSawmill::contractors::exporters {

ContractorExporter::ContractorExporter()
	: core::abstracts::AbstractExporter<core::Contractor>()
{
}

bool ContractorExporter::exportData(QIODevice& device) {
	Q_UNUSED(device);
	return false;
}

bool ContractorExporter::importData(QIODevice& device) {
	Q_UNUSED(device);
	return false;
}

QDomElement ContractorExporter::toDomNode(QDomDocument& document) {
	core::Contractor contractor {mData.at(0)};

	QDomElement root = document.createElement("contractor");
	QDomElement nameNode = document.createElement("name");
	QDomElement firstNameNode = document.createElement("firstName");
	QDomElement lastNameNode = document.createElement("lastName");
	QDomElement nipNode = document.createElement("NIP");
	QDomElement regonNode = document.createElement("REGON");
	QDomElement peselNode = document.createElement("PESEL");

	nameNode.appendChild(document.createTextNode(contractor["Name"].toString()));
	firstNameNode.appendChild(document.createTextNode(contractor["PersonName"].toString()));
	lastNameNode.appendChild(document.createTextNode(contractor["PersonSurname"].toString()));
	nipNode.appendChild(document.createTextNode(contractor.NIP()));
	regonNode.appendChild(document.createTextNode(contractor.REGON()));
	peselNode.appendChild(document.createTextNode(contractor.PESEL()));

	root.appendChild(nameNode);
	root.appendChild(firstNameNode);
	root.appendChild(lastNameNode);
	root.appendChild(nipNode);
	root.appendChild(regonNode);
	root.appendChild(peselNode);

	core::Contact contact = contractor.contact();
	QDomElement address = document.createElement("address");
	auto countryNode = document.createElement("country");
	countryNode.appendChild(document.createTextNode(contact.country()));
	countryNode.setAttribute("iso", contact.IsoCountry());
	address.appendChild(countryNode);

	QDomElement cityNode = document.createElement("city");
	QDomElement postOfficeNode = document.createElement("postOffice");
	QDomElement postCodeNode = document.createElement("postCode");
	QDomElement stateNode = document.createElement("state");
	QDomElement phoneNode = document.createElement("phone");
	QDomElement streetNode = document.createElement("street");
	QDomElement homeNumberNode = document.createElement("homeNumber");
	QDomElement localNumberNode = document.createElement("localNumber");
	QDomElement emailNode = document.createElement("email");
	QDomElement urlNode = document.createElement("URL");

	cityNode.appendChild(document.createTextNode(contact.city()));
	postOfficeNode.appendChild(document.createTextNode(contact.postOffice()));
	postCodeNode.appendChild(document.createTextNode(contact.postCode()));
	stateNode.appendChild(document.createTextNode(contact.state()));
	phoneNode.appendChild(document.createTextNode(contact.phone()));
	streetNode.appendChild(document.createTextNode(contact.street()));
	homeNumberNode.appendChild(document.createTextNode(contact.homeNumber()));
	localNumberNode.appendChild(document.createTextNode(contact.localNumber()));
	emailNode.appendChild(document.createTextNode(contact.email()));
	urlNode.appendChild(document.createTextNode(contact.url()));

	address.appendChild(cityNode);
	address.appendChild(postOfficeNode);
	address.appendChild(postCodeNode);
	address.appendChild(stateNode);
	address.appendChild(phoneNode);
	address.appendChild(streetNode);
	address.appendChild(homeNumberNode);
	address.appendChild(localNumberNode);
	address.appendChild(emailNode);
	address.appendChild(urlNode);

	root.appendChild(address);

	return root;
}

void ContractorExporter::fromDomNode(const QDomElement& element) {
	Q_UNUSED(element);
}

} // namespace eSawmill::contractors::exporters
