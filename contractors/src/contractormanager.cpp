#include "contractormanager.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStringListModel>
#include <QScopedPointer>
#include <QMenu>
#include <QFormLayout>
#include <QCompleter>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <messagebox.h>
#include <pushbutton.h>
#include <dbo/contact.h>
#include <settings/appsettings.hpp>
#include <globaldata.hpp>
#include <exceptions/forbiddenexception.hpp>
#include <exceptions/repositoryexception.hpp>
#include <commandexecutor.hpp>
#include "commands/createcontractorcommand.hpp"
#include "commands/updatecontractorcommand.hpp"
#include "commands/removecontractorcommand.hpp"

namespace eSawmill::contractors {

ContractorManager::ContractorManager(QWidget *parent, bool selectorMode, bool providerSelector)
	: QDialog(parent)
	, mSelectorMode(selectorMode)
{
	core::GlobalData::instance().logger().information("Otwarto menadżera kontrahentów");
	setWindowTitle(tr("Kontrahenci"));
	createWidgets();
	createModels();
	createConnections();

	if (mSelectorMode) {
		mRemakeButton->setText(tr("Wybierz"));
		mDeleteButton->setDisabled(true);

		if (providerSelector) {
			// mColumnSelector->setCurrentIndex(0);	// wybór dostawców jako domyślny
		}
	}
}

core::Contractor ContractorManager::selectedContractor() const {
	Q_ASSERT(mSelectedCurrentContractorId > 0);
	auto& repository = core::GlobalData::instance().repository().contractorRepository();
	return repository.get(mSelectedCurrentContractorId);
}

int ContractorManager::createFrom(const QDomElement &element, bool checkBefore) {
	Q_UNUSED(checkBefore);

	core::Contractor contractor;
	contractor.set("Name", element.firstChildElement("name").text());
	contractor.set("PersonName", element.firstChildElement("firstName").text());
	contractor.set("PersonSurname", element.firstChildElement("lastName").text());
	contractor.set("NIP", element.firstChildElement("NIP").text());
	contractor.set("REGON", element.firstChildElement("REGON").text());
	contractor.set("PESEL", element.firstChildElement("PESEL").text());

	QDomElement address = element.firstChildElement("address");
	core::Contact contact;
	contact.set("Country", address.firstChildElement("country").text());
	contact.set("IsoCountry", address.firstChildElement("country").attribute("iso"));
	contact.set("City", address.firstChildElement("city").text());
	contact.set("PostOffice", address.firstChildElement("postOffice").text());
	contact.set("ZipCode", address.firstChildElement("postCode").text());
	contact.set("State", address.firstChildElement("state").text());
	contact.set("Phone", address.firstChildElement("phone").text());
	contact.set("Street", address.firstChildElement("street").text());
	contact.set("HomeNumber", address.firstChildElement("homeNumber").text());
	contact.set("LocalNumber", address.firstChildElement("localNumber").text());
	contact.set("Email", address.firstChildElement("email").text());
	contact.set("Url", address.firstChildElement("URL").text());

	auto createNewContractorInstance = [&contractor, &contact]()-> int {
		contact.builder().beginTransaction();
		contact.save();
		contractor.set("ContactId", contact.lastInsertedId());
		contractor.save();
		contact.builder().commitTransaction();
		return contractor.lastInsertedId();
	};

	// check existing contractors
	// return id contractors or 0 if contractors don't exist
	auto contractorExist = [&contractor, &contact, &createNewContractorInstance]() -> int {
		QString sqlQuery;
		QTextStream sql(&sqlQuery);
		sql << "SELECT [Contractors].[Id] AS [ContractorId], [Contacts].[Id] AS [ContactId] FROM [Contractors] JOIN [Contacts] ON [Contractors].[ContactId] = [Contacts].[Id] WHERE ";
		int whereIndex {0};
		if (!contractor.NIP().isEmpty()) {
			if (whereIndex > 0) sql << " OR ";
			sql << "[Contractors].[NIP] IS NOT NULL AND [Contractors].[NIP] = '" << contractor.NIP() <<"' ";
			++whereIndex;
		}

		if (!contractor.REGON().isEmpty()) {
			if (whereIndex > 0) sql << " OR ";
			sql << "[Contractors].[REGON] IS NOT NULL AND [Contractors].[REGON] = '" << contractor.REGON() << "' ";
			++whereIndex;
		}

		if (!contractor.PESEL().isEmpty()) {
			if (whereIndex > 0) sql << " OR ";
			sql << "[Contractors].[PESEL] IS NOT NULL AND [Contractors].[PESEL] = '" << contractor.PESEL() << "' ";
			++whereIndex;
		}

		if (!contact.phone().isEmpty()) {
			if (whereIndex > 0) sql << " OR ";
			sql << "[Contacts].[Phone] IS NOT NULL AND [Contacts].[Phone] LIKE '%" << contact.phone() << "%' ";
			++whereIndex;
		}

		if (!contact.email().isEmpty()) {
			if (whereIndex > 0) sql << " OR ";
			sql << "[Contacts].[Email] IS NOT NULL AND [Contacts].[Email] = '" << contact.email() << "' ";
			++whereIndex;
		}

#ifdef DEBUG
	qDebug() << sqlQuery;
#endif
		QSqlDatabase db = QSqlDatabase::database();
		QSqlQuery query {db};
		if (!query.exec(sqlQuery)) {
			qDebug() << query.lastError().text();
			return 0;
		}

		if (query.first()) {
			int contractorId = query.value("ContractorId").toUInt();
			return contractorId > 0 ? contractorId : createNewContractorInstance();
		}
		else return createNewContractorInstance();
	}; // lambda contractorExist

	return contractorExist();
}

void ContractorManager::notifyRepository(const agent::RepositoryEvent& event) {
	if (mTableView == nullptr || mContractors == nullptr)
		return;

	if (event.resourceType == agent::Resource::Contractor) {
		const auto& selection = mTableView->selectionModel()->selection();
		mContractors->get();	// update contractors model
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void ContractorManager::createWidgets() {
    mContractorSelector = new QListView(this);
    mTableView = new ::widgets::TableView(this);
	mFilterButton = new QPushButton(QIcon(":/icons/find"), tr("&Filtruj"), this);
	mFilterClearButton = new QPushButton(tr("W&yczyść"), this);

    mTableView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mContractorSelector->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
    mRemakeButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
    mDeleteButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
    mCloseButton = new ::widgets::PushButton(tr("Zamknij"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
    ::widgets::PushButton* operationButton = new ::widgets::PushButton(tr("Operacje"), QKeySequence(), this);

    QMenu* operationMenu = new QMenu(operationButton);
    operationButton->setMenu(operationMenu);

	QSettings settings;
	// import from comarch action
	QAction* importFromComarchAction = new QAction(tr("Import z Comarch ERP Optima"), this);
	importFromComarchAction->setVisible(settings.value("comarch/allowToConnect").toBool());
	operationMenu->addAction(importFromComarchAction);

	connect(importFromComarchAction, &QAction::triggered, this, &ContractorManager::importFromComarchHandle);

    QHBoxLayout* buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(mAddButton);
    buttonLayout->addWidget(mRemakeButton);
    buttonLayout->addWidget(mDeleteButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(operationButton);
    buttonLayout->addStretch(4);
    buttonLayout->addWidget(mCloseButton);

	mFilterGroup = new QGroupBox(tr("Filtr kontrahentów"), this);
	mFilterGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mFilterGroup->setCheckable(true);
	mFilterGroup->setChecked(false);

	mName = new QLineEdit(this);
	mPersonName = new QLineEdit(this);
	mPersonSurname = new QLineEdit(this);
	mNip = new QLineEdit(this);
	mPesel = new QLineEdit(this);
	mPhone = new QLineEdit(this);

	QFormLayout* filterLeftColumnLayout = new QFormLayout;
	filterLeftColumnLayout->addRow(tr("Nazwa"), mName);
	filterLeftColumnLayout->addRow(tr("Imię"), mPersonName);
	filterLeftColumnLayout->addRow(tr("Nazwisko"), mPersonSurname);

	QFormLayout* filterRightColumnLayout = new QFormLayout;
	filterRightColumnLayout->addRow(tr("NIP"), mNip);
	filterRightColumnLayout->addRow(tr("PESEL"), mPesel);
	filterRightColumnLayout->addRow(tr("Telefon"), mPhone);

	QVBoxLayout* filterButtonsLayout = new QVBoxLayout;
	filterButtonsLayout->addWidget(mFilterButton);
	filterButtonsLayout->addWidget(mFilterClearButton);
	filterButtonsLayout->addStretch(1);

	QHBoxLayout* filterLayout = new QHBoxLayout(mFilterGroup);
	filterLayout->addLayout(filterLeftColumnLayout);
	filterLayout->addSpacing(24);
	filterLayout->addLayout(filterRightColumnLayout);
	filterLayout->addLayout(filterButtonsLayout);
	filterLayout->addStretch(1);

    QVBoxLayout* rightLayout = new QVBoxLayout;
//    rightLayout->addLayout(searchLayout);
    rightLayout->addWidget(mTableView);
	rightLayout->addSpacing(12);
	rightLayout->addWidget(mFilterGroup);

    QHBoxLayout* listsLayout = new QHBoxLayout;
    listsLayout->addWidget(mContractorSelector);
    listsLayout->addLayout(rightLayout);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(listsLayout);
	mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonLayout);
}

void ContractorManager::createConnections() {
    connect(mCloseButton, &::widgets::PushButton::clicked, this, &ContractorManager::close);
    connect(mAddButton, &::widgets::PushButton::clicked, this, &ContractorManager::addButton_Clicked);
    connect(mDeleteButton, &::widgets::PushButton::clicked, this, &ContractorManager::removeButtonHandle);
    connect(mContractorSelector, &QListView::clicked, this, &ContractorManager::selectedContractor_Clicked);
    connect(mTableView, &::widgets::TableView::returnPressed, this, &ContractorManager::editContractors_ReturnPressed);
    connect(mTableView, &::widgets::TableView::doubleClicked, this, &ContractorManager::editContractors_ReturnPressed);
	connect(mFilterClearButton, &QPushButton::clicked, this, &ContractorManager::clearFilterButtonHandle);
	connect(mFilterButton, &QPushButton::clicked, this, &ContractorManager::filterRecordsHandle);
	connect(mFilterGroup, &QGroupBox::clicked, mFilter, &filters::ContractorFilter::setActive);

	// edit button
    connect(mRemakeButton, &::widgets::PushButton::clicked, this, [&]{
		editContractors_ReturnPressed(mTableView->currentIndex());
    });
}

void ContractorManager::createModels() {
    QStringList contracotrTypes;
    contracotrTypes << tr("Dostawcy") << tr("Odbiorcy");

    QStringListModel* contractorSelectorModel = new QStringListModel(contracotrTypes, this);
    mContractorSelector->setModel(contractorSelectorModel);

	mContractorSelector->setCurrentIndex(contractorSelectorModel->index(1));

    // model kontrahentów
    mContractors = new models::Contractors(this);
	mContractorFilter = new filters::ContractorRoleFilter(this);
	mFilter = new filters::ContractorFilter(this);

    mContractorFilter->setSourceModel(mContractors);
	mFilter->setSourceModel(mContractorFilter);

	mTableView->setModel(mFilter);
//    mColumnSelector->addItems(mContractors->headerData());

	// set input mask for filter fields
	mPesel->setInputMask("99999999999;_");
	mPhone->setInputMask("+99 999999999;_");
	mPhone->setText("48");

	// create completers for filter fields
	QCompleter* nameCompleter = new QCompleter(this);
	nameCompleter->setModel(mContractors);
	nameCompleter->setCompletionColumn(models::Contractors::Columns::Name);
	nameCompleter->setCompletionRole(Qt::DisplayRole);
	nameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	nameCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mName->setCompleter(nameCompleter);

	QCompleter* personNameCompleter = new QCompleter(this);
	personNameCompleter->setModel(mContractors);
	personNameCompleter->setCompletionColumn(models::Contractors::Columns::PersonName);
	personNameCompleter->setCompletionRole(Qt::DisplayRole);
	personNameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	personNameCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mPersonName->setCompleter(personNameCompleter);

	QCompleter* personSurnameCompleter = new QCompleter(this);
	personSurnameCompleter->setModel(mContractors);
	personSurnameCompleter->setCompletionColumn(models::Contractors::Columns::PersonSurname);
	personSurnameCompleter->setCompletionRole(Qt::DisplayRole);
	personSurnameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	personSurnameCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mPersonSurname->setCompleter(personSurnameCompleter);

	QCompleter* nipCompleter = new QCompleter(this);
	nipCompleter->setModel(mContractors);
	nipCompleter->setCompletionColumn(models::Contractors::Columns::NIP);
	nipCompleter->setCompletionRole(Qt::DisplayRole);
	nipCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	nipCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mNip->setCompleter(nipCompleter);

	QCompleter* peselCompleter = new QCompleter(this);
	peselCompleter->setModel(mContractors);
	peselCompleter->setCompletionColumn(models::Contractors::PESEL);
	peselCompleter->setCompletionRole(Qt::DisplayRole);
	peselCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	peselCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mPesel->setCompleter(peselCompleter);

	QCompleter* phoneCompleter = new QCompleter(this);
	phoneCompleter->setModel(mContractors);
	phoneCompleter->setCompletionColumn(models::Contractors::Phone);
	phoneCompleter->setCompletionRole(Qt::DisplayRole);
	phoneCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	phoneCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	mPhone->setCompleter(phoneCompleter);
}

void ContractorManager::addButton_Clicked() {
	std::unique_ptr<CreateContractor> dialog = std::make_unique<CreateContractor>(this);
	if (dialog->exec() == CreateContractor::Accepted) {
		core::Contractor contractor = dialog->contractor();

		std::unique_ptr<eSawmill::contractors::commands::CreateContractorCommand> createCommand
			= std::make_unique<eSawmill::contractors::commands::CreateContractorCommand>(contractor, this);

		if (!core::GlobalData::instance().commandExecutor().execute(std::move(createCommand))) {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Błąd podczas wykonywania akcji tworzenia nowego kontrahenta!\n"
				   "Zmiany w bazie nie zostały wprowadzone"));
		} else {
			mContractors->get();
		}
	}
}

void ContractorManager::removeButtonHandle() {
	const auto& selectionList = mTableView->selectionModel()->selectedRows();
	if (!selectionList.empty()) {
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"), tr("Zaznaczono %1 obiektów, czy chcesz aby zostały one usunięte?")
			.arg(selectionList.size()), "",
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			try {
				auto& repository = core::GlobalData::instance().repository().contractorRepository();
				for (const auto& item : selectionList) {
					const core::eloquent::DBIDKey& contractorId = item.data(models::Contractors::Roles::ContractorId).toUInt();
					core::repository::ResourceLocker locker(contractorId, repository);

					std::unique_ptr<eSawmill::contractors::commands::RemoveContractorCommand> removeCommand
							= std::make_unique<eSawmill::contractors::commands::RemoveContractorCommand>(contractorId, this);

					if (!core::GlobalData::instance().commandExecutor().execute(std::move(removeCommand))) {
						::widgets::MessageBox::critical(this, tr("Błąd"),
							tr("Błąd podczas usuwania kontrahenta z systemu!\n"
							   "Zmiany w bazie nie zostały wprowadzone"));
					} else {
						mContractors->get();
					}
				}
			} catch (core::exceptions::ForbiddenException&) {
				::widgets::MessageBox::warning(this,
					tr("Blokada dostępu"),
					tr("Wybrany kontrahent jest w tym momencie edytowany przez innego użytkownika, poczekaj na zakończenie edycji"));
			} catch (core::exceptions::RepositoryException&) {
				::widgets::MessageBox::critical(this, tr("Błąd dostawy danych"),
					tr("Podczas pobierania danych od dostawcy wystąpiły błędy!\n"
					   "Może to być spowodowane niedostosowaniem wersji klienta oraz serwera"));

				// and log into file
				core::GlobalData::instance().logger().critical(
					QString::asprintf("core::exceptions::RepositoryException w ContractorManager (%s)", Q_FUNC_INFO));
			}
		}
	} else {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Przed wykonaniem operacji usuń musisz zaznaczyć elementy które mają zostać usunięte"));
	}
}

void ContractorManager::selectedContractor_Clicked(const QModelIndex &index) {
    QString data = index.data().toString().toUpper();
    if (data == "DOSTAWCY")
        mContractorFilter->setFilterType(models::Contractors::Provider);
    else if (data == "ODBIORCY")
        mContractorFilter->setFilterType(models::Contractors::Customer);
}

void ContractorManager::editContractors_ReturnPressed(const QModelIndex &index) {
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Nie zaznaczono kontrahenta do edycji!"));
		return;
	}

	core::eloquent::DBIDKey contractorId = index.data(models::Contractors::Roles::ContractorId).toInt();
    if (!mSelectorMode) {
		try {
			core::GlobalData::instance().logger().information(QString::asprintf("Edycja kontrahenta o id: %d", contractorId));
			auto& repository = core::GlobalData::instance().repository().contractorRepository();
			// lock contractor on editing time
			core::repository::ResourceLocker locker(contractorId, repository);

			std::unique_ptr<CreateContractor> dialog = std::make_unique<CreateContractor>(contractorId, this);
			if (dialog->exec() == CreateContractor::Accepted) {
				std::unique_ptr<eSawmill::contractors::commands::UpdateContractorCommand> updateCommand
					= std::make_unique<eSawmill::contractors::commands::UpdateContractorCommand>(dialog->contractor(), this);

				if (!core::GlobalData::instance().commandExecutor().execute(std::move(updateCommand))) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Podczas wykonywania akcji aktualizacji kontrahenta wystąpiły błędy!\n"
						   "Zmiany w bazie nie zostały zapisane"));

					// log information into log file
					core::GlobalData::instance().logger()
							.critical(QString::asprintf("Błąd podczas aktualizacji danych o kontrahencie (id: %d)", contractorId));
				} else {
					mContractors->get();
				}
			}
		} catch (core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
				tr("Wybrany kontrahent jest w tym momencie edytowany przez innego użytkownika, poczekaj na zakończenie edycji"));
		} catch (core::exceptions::RepositoryException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::critical(this, tr("Błąd dostawcy danych"),
				tr("Podczas pobierania danych od dostawcy wystąpiły błędy!\n"
				   "Może to byś spowodowane niezgodnością wersji pomiędzy klientem a serwerem."));

			// and log into file
			core::GlobalData::instance().logger().critical(
				QString::asprintf("core::exceptions::RepositoryException w ContractorManager (%s)", Q_FUNC_INFO));
		}
	} else {
		core::GlobalData::instance().logger()
			.information(QString::asprintf("Wybranie kontrahenta o id: %d", contractorId));

		mSelectedCurrentContractorId = contractorId;
		accept();
	}
}

void ContractorManager::clearFilterButtonHandle() {
	auto editList = findChildren<QLineEdit*>();
	for (auto* lineEdit : editList) {
		lineEdit->setText("");
	}
	mPhone->setText("48");
	mFilter->clearAllFilters();
}

void ContractorManager::filterRecordsHandle() {
	mFilter->setName(mName->text().toUpper());
	mFilter->setPersonName(mPersonName->text().toUpper());
	mFilter->setPersonSurname(mPersonSurname->text().toUpper());
	mFilter->setNip(mNip->text());
	mFilter->setPesel(mPesel->text());
	mFilter->setPhone(mPhone->text());
}
void ContractorManager::importFromComarchHandle() {
//    QScopedPointer<comarch::ComarchImporter> dialog(new comarch::ComarchImporter(this));
//    if (dialog->exec() == comarch::ComarchImporter::Accepted) {
//		core::comarch::Contractor comarchContractor;
//		comarchContractor = comarchContractor.builder().where("Knt_KntId", dialog->selectedContractor()).first();

//		// searching for a contractor in the local database
//		// in order to eliminate the create of duplicates

//		// first check by NIP
//		core::Contractor localContractor;
//		localContractor = localContractor.builder().where("NIP", "<>", "")
//				.where("NIP", "=", comarchContractor["Knt_Nip"].toString()).first();

//		if (localContractor) {
//			localContractor.set("ComarchId", comarchContractor["Knt_KntId"].toInt());
//			localContractor.save();

//			mContractors->get();
//			return;
//		}

//		// second check by PESEL
//		localContractor = localContractor.builder().where("PESEL", "<>", "")
//				.where("PESEL", "=", comarchContractor["Knt_Pesel"].toString()).first();

//		if (localContractor) {
//			localContractor.set("ComarchId", comarchContractor["Knt_KntId"].toInt());
//			localContractor.save();

//			mContractors->get();
//			return;
//		}

//		// third check by REGON
//		localContractor = localContractor.builder().where("REGON", "<>", "")
//				.where("REGON", "=", comarchContractor["Knt_Regon"].toString()).first();

//		if (localContractor) {
//			localContractor.set("ComarchId", comarchContractor["Knt_KntId"].toInt());
//			localContractor.save();

//			mContractors->get();
//			return;
//		}

//		// at the end check by phone number
//		core::Contact contact;
//		contact.builder().where("Phone", "<>", "")
//				.where("Phone", "=", comarchContractor["Knt_Telefon1"])
//				.where("Phone", "=", comarchContractor["Knt_Telefon2"], "OR").first();

//		if (contact.id() > 0) {
//			localContractor = localContractor.builder().where("ContactId", contact.id()).first();
//			localContractor.set("ComarchId", comarchContractor["Knt_KntId"].toInt());
//			localContractor.save();

//			mContractors->get();
//			return;
//		}

//		QScopedPointer<CreateContractor> dialog(new CreateContractor(comarchContractor.contractor(), comarchContractor.contact(), this));
//		if (dialog->exec() == CreateContractor::Accepted) {
//			dialog->contact().save();
//			core::Contractor contractor = dialog->contractor();
//			contractor.set("ContactId", core::Contact().lastInsertedId());
//			contractor.set("ComarchId", comarchContractor["Knt_KntId"].toInt());

//			// create contractor in local database with filled ComarchId column
//			core::Contractor().createOrUpdate(contractor);

//			// at the end, reload contractors model (user can see new contractor on list)
//			mContractors->get();
//		}
//    }
}

} // namespace eSawmill
