#include "createcontractor.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QMessageBox>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTabWidget>
#include <QCompleter>
#include <QApplication>
#include <QEventLoop>
#include <dialogs/citydictionarydialog.hpp>
#include <messagebox.h>
#include <dbo/company.h>
#include <models/citydictionarymodel.hpp>
#include <network/regon/regon.hpp>
#include <eloquent/builder.hpp>
#include <models/stateslistmodel.hpp>
#include <globaldata.hpp>
#include <array>

namespace eSawmill::contractors {

CreateContractor::CreateContractor(QWidget *parent)
	: QDialog(parent)
{
	createWidgets();
	createValidators();
	createConnections();
	createModels();

	setWindowTitle(tr("Dodawanie kontrahenta"));
	mCountry->setText(tr("Polska"));
	mIsoCountry->setText(tr("PL"));

	mEditContractor = false;
}

CreateContractor::CreateContractor(int id, QWidget *parent)
	: CreateContractor(parent)
{
	Q_ASSERT(id > 0);
	setWindowTitle(tr("Edycja kontrahenta"));
	mEditContractor = true;

	auto& repository = core::GlobalData::instance().repository().contractorRepository();
	mContractor = repository.get(id);

	mContact = mContractor.contact();
	mName->setText(mContractor.name());
	mPersonName->setText(mContractor.personName());
	mPersonSurname->setText(mContractor.personSurname());
	mNip->setText(mContractor.NIP());
	mRegon->setText(mContractor.REGON());
	mPesel->setText(mContractor.PESEL());

	mCountry->setText(mContact.country());
	mIsoCountry->setText(mContact.IsoCountry());
	mCity->setText(mContact.city());
	mPostOffice->setText(mContact.postOffice());
	mZipCode->setText(mContact.postCode());
	mState->setCurrentText(mContact.state());
	mStreet->setText(mContact.street());
	mHomeNumber->setText(mContact.homeNumber());
	mLocalNumber->setText(mContact.localNumber());
	mPhone->setText(mContact.phone());
	mEmail->setText(mContact.email());
	mUrl->setText(mContact.url());

	mProvider->setChecked(mContractor.provider());
	mCustomer->setChecked(!mProvider->isChecked());

	if (mContractor.parentId() > 0) {
		mMasterContractorGroup->setChecked(true);
		core::Contractor masterContractor = repository.get(mContractor.parentId());;
		mMasterContractor->selectContractor(masterContractor);
	}
}

CreateContractor::CreateContractor(core::Contractor contractor, core::Contact contact, QWidget *parent)
	: CreateContractor(parent)
{
	setWindowTitle(tr("Import kontrahenta z Comarch ERP"));

	mName->setText(contractor.name());
	mNip->setText(contractor.NIP());
	mRegon->setText(contractor.REGON());
	mPesel->setText(contractor.PESEL());
	if (contractor.provider())
		mProvider->setChecked(true);
	else mCustomer->setChecked(true);

	mCity->setText(contact.city());
	mStreet->setText(contact.street());
	mHomeNumber->setText(contact.homeNumber());
	mLocalNumber->setText(contact.localNumber());
	mZipCode->setText(contact.postCode());
	mPostOffice->setText(contact.postOffice());
	mEmail->setText(contact.email());
	mUrl->setText(contact.url());
	mState->setCurrentText(contact.state());
	mCountry->setText(contact.country());
	mIsoCountry->setText(contact.IsoCountry());

	if (contact.phone().startsWith("+48"))
		mPhone->setText(contact.phone());
	else mPhone->setText("48" + contact.phone());
}

void CreateContractor::createValidators()
{
	// maski wprowadzania chnych
	mZipCode->setInputMask("99-999;_");
	mNip->setInputMask("9999999999;_");
	mPesel->setInputMask("99999999999;_");
	mPhone->setInputMask("+99 999999999;_");
	mIsoCountry->setInputMask(">AA;_");

	// uzupełnienie numeru kierunkowego na Polskę
	mPhone->setText("48");

	// walidatory pól tekstowych
	QRegularExpressionValidator* stringValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z]+"), this);
	QRegularExpressionValidator* companyNameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9\\s\"-]*"), this);
	QRegularExpressionValidator* regonValidator = new QRegularExpressionValidator(QRegularExpression("[0-9]{9,14}"), this);
	QRegularExpressionValidator* streetValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9 ]+"), this);
	QRegularExpressionValidator* emailValidator = new QRegularExpressionValidator(QRegularExpression("^[A-Za-z0-9]+@[A-Za-z]+.[A-Za-z]{2,3}$"), this);
	QRegularExpressionValidator* homeNumberValidator = new QRegularExpressionValidator(QRegularExpression("[0-9]+[A-Za-z]?"), this);

	//walidatory tekstowe dla imienia, nazwiska, nazwy miejscowości
	mPersonName->setValidator(stringValidator);
	mPersonSurname->setValidator(stringValidator);
	mCity->setValidator(stringValidator);
	mName->setValidator(companyNameValidator);
	mRegon->setValidator(regonValidator);
	mStreet->setValidator(streetValidator);
	mEmail->setValidator(emailValidator);
	mHomeNumber->setValidator(homeNumberValidator);
}

void CreateContractor::checkNipNumber()
{
	if (mEditContractor)
		return;	// dont check nip when user edit existing object

	/// sprawdzanie poprawności numeru NIP opiera się o standardowy algorytm
	/// 1. Pomnożyć cyfry 1-9 przez następujące wagi: 6, 5, 7, 2, 3, 4, 5, 6, 7
	/// 2. Zsumować wyniki mnożenia
	/// 3. Obliczyć resztę z dzielenia sumy przez 11 (modulo 11)
	/// 4. Reszta z dzielenia ma być różna od 10 !!!

	const QString text = mNip->text();
	int values[9] = {0};
	const int weights[] = { 6, 5, 7, 2, 3, 4, 5, 6, 7};

	int sum = 0;
	for (int i = 0; i < 9; i++) {
		values[i] = (text.at(i).unicode() - QChar('0').unicode());

		sum += values[i] * weights[i];
	}

	if ((sum % 11) == 10) {
		::widgets::MessageBox::warning(this, tr("Numer NIP"),
									   tr("Wprowadzony numer NIP jest nieprawidłowy!"));
		mNip->selectAll();
		mNip->setFocus(Qt::FocusReason::ActiveWindowFocusReason);
	}
	else {
		auto& repository = core::GlobalData::instance().repository().contractorRepository();
		if (repository.checkDoExistNipNumberForContractor(mNip->text())) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Kontrahent o podanym numerze NIP istnieje w bazie systemu eSawmill"));

			mNip->selectAll();
			mNip->setFocus(Qt::FocusReason::ActiveWindowFocusReason);
		}
	}
}

void CreateContractor::checkPeselNumber()
{
	/// Obliczanie cyfry kontrolnej
	/// 1. Dla koljenych cyfr indentyfikatora PESEL obliczany jest iloczyn cyfry i jej wagi
	/// 2. Obliczana jest suma tych iloczynów
	/// 3. Obliczana jest wartość M operacji modulo S % 10
	/// 4. Od liczy 10 odejmowana jest liczba M
	/// 5. Poza przypadkiem szczególnym wskazanym w pkt.6 wynik odejmowania uzyskany w pkt. 4
	///    stanowi cyfrę kontrolną numeru PESEL
	/// 6. Jeżeli liczba M jest równa 0 to liczba (cyfra) kontolna też jest równa 0

	const QString text = mPesel->text();
	std::array<int, 11> values {0};
	constexpr std::array<int, 11> weights { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1 };
	const int CRC = text.at(10).unicode() - QChar('0').unicode();	// wyznaczenie cyfry kontrolnej

	// krok 1 i 2
	int S = 0;
	for (int i = 0; i < 10; i++) {
		values[i] = text.at(i).unicode() - QChar('0').unicode();
		S += values[i] * weights[i];
	}

	int M = S % 10;	// krok 3
	M = 10 - M;		// krok 4

	if (M != CRC) {
		::widgets::MessageBox::warning(this, tr("Numer PESEL"),
			tr("Wprowadzony numer PESEL jest nieprawidłowy!"));

		mPesel->selectAll();
		mPesel->setFocus(Qt::FocusReason::ActiveWindowFocusReason);
	}
	else {
		auto& repository = core::GlobalData::instance().repository().contractorRepository();
		const auto& contractors = repository.getAll();

		bool exist = std::any_of(contractors.begin(), contractors.end(),
			[this](const core::Contractor& contractor)->bool {
				return contractor.NIP() == mNip->text();
		});
		if (exist) {
			::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
				tr("Kontrahent o podanym numerze PESEL już istnieje!"));
			mPesel->selectAll();
			mPesel->setFocus();
		}
	}
}

void CreateContractor::checkRegonNumber()
{
	if (mEditContractor)
		return;	// dont check when user edit existing object

	auto& repository = core::GlobalData::instance().repository().contractorRepository();
	const auto& contractors = repository.getAll();

	bool exist = std::any_of(contractors.begin(), contractors.end(),
		[this](const core::Contractor& contracotr)->bool {
			return contracotr.REGON() == mRegon->text();
	});
	if (exist) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Kontrahent o wprowadzonym numerze REGON już istnieje!"));

		mRegon->selectAll();
		mRegon->setFocus();
	}
}

void CreateContractor::checkPhoneNumber() {
	if (mEditContractor)
		return;	// dont check phone number when user edit existing object

	auto& repository = core::GlobalData::instance().repository().contractorRepository();
	const auto& contractors = repository.getAll();

	bool exist = std::any_of(contractors.begin(), contractors.end(),
		[this](const core::Contractor& contractor)->bool {
			return contractor.contact().phone() == mPhone->text();
	});
	if (exist) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
			tr("Kontrahent o podanym numerze telefonu już istnieje"));
		mPhone->selectAll();
		mPhone->setFocus();
	}
}

void CreateContractor::showCityDictionary(QLineEdit *lineEdit)
{
	QScopedPointer<::widgets::dialogs::CityDictionaryDialog> dialog(new ::widgets::dialogs::CityDictionaryDialog(this));
	if (dialog->exec() == ::widgets::dialogs::CityDictionaryDialog::Accepted) {
		lineEdit->setText(dialog->city());
	}
}

void CreateContractor::gusButtonHandle()
{
	const QString& nip = mNip->text().trimmed();
	if (!nip.size()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Aby wyszukać podmiot w bazie REGON GUS musisz uzupełnić pole NIP"));
		return;
	}

	QEventLoop* loop = new QEventLoop(this);
	core::network::Regon* regon = new core::network::Regon(this);
	regon->login(core::network::Regon::userKey());

	connect(regon, &core::network::Regon::userIsLogged, this, [&regon, &nip, this](const QByteArray& token){
		Q_UNUSED(token);

		// user is loggin
		// disable all input fields
		QList<QLineEdit*> fields = findChildren<QLineEdit*>();
		for (auto* field : fields) {
			field->setDisabled(true);
		}
		setCursor(Qt::WaitCursor);
		regon->searchEntities(nip);
	});

	connect(regon, &core::network::Regon::userIsLogout, this, [&loop, this](){
		// user is logged out
		// so, enable all input fields
		QList<QLineEdit*> fields = findChildren<QLineEdit*>();
		for (auto* field : fields) {
			field->setEnabled(true);
		}
		setCursor(Qt::ArrowCursor);
		loop->quit();
	});

	connect(regon, &core::network::Regon::finished, this, [&regon, this](QDomElement& element){
		// all data are ready to read
		if (!element.hasChildNodes()) {
			::widgets::MessageBox::information(this, tr("Informacja"),
				tr("Nie znaleziono podmiotu o podanym numerze NIP w bazie GUS"));
		}
		else {
			mName->setText(element.firstChildElement("Nazwa").text().toUpper());
			mRegon->setText(element.firstChildElement("Regon").text().toUpper());
			mState->setCurrentText(element.firstChildElement("Wojewodztwo").text().toUpper());
			mCity->setText(element.firstChildElement("Miejscowosc").text().toUpper());
			mZipCode->setText(element.firstChildElement("KodPocztowy").text());
			mHomeNumber->setText(element.firstChildElement("NrNieruchomosci").text().toUpper());
			mLocalNumber->setText(element.firstChildElement("NrLokalu").text().toUpper());
			mPostOffice->setText(element.firstChildElement("MiejscowoscPoczty").text().toUpper());
		}

		// and we can logout from GUS system
		regon->logout();
	});

	// start local event loop
	loop->exec();
}

core::Contractor CreateContractor::contractor() {
	if (mEditContractor) {
		Q_ASSERT(mContractor.id() > 0);
	}
	mContractor.setName(mName->text());
	mContractor.setPersonName(mPersonName->text());
	mContractor.setPersonSurname(mPersonSurname->text());
	mContractor.setNIP(mNip->text());
	mContractor.setREGON(mRegon->text());
	mContractor.setPESEL(mPesel->text());
	mContractor.setProvider(mProvider->isChecked());
	mContractor.setContact(contact());
	if (mMasterContractorGroup->isChecked())
		mContractor.set("ParentId", mMasterContractor->contractor().id());

	return mContractor;
}

core::Contact CreateContractor::contact() {
	if (mEditContractor) {
		Q_ASSERT(mContact.id() > 0);
	}
	mContact.setCountry(mCountry->text());
	mContact.setIsoCountry(mIsoCountry->text());
	mContact.setCity(mCity->text());
	mContact.setState(mState->currentText());
	mContact.setPostCode(mZipCode->text());
	mContact.setPostOffice(mPostOffice->text());
	mContact.setStreet(mStreet->text());
	mContact.setHomeNumber(mHomeNumber->text());
	mContact.setLocalNumber(mLocalNumber->text());
	mContact.setPhone(mPhone->text());
	mContact.setEmail(mEmail->text());
	mContact.setUrl(mUrl->text());

	return mContact;
}

void CreateContractor::createWidgets()
{
	mAcceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));

	QTabWidget* tabWidget = new QTabWidget(this);
	tabWidget->addTab(createGeneralWidget(), tr("Ogólne"));

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(tabWidget);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void CreateContractor::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateContractor::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateContractor::reject);
	connect(mNip, &QLineEdit::editingFinished, this, &CreateContractor::checkNipNumber);
	connect(mPesel, &QLineEdit::editingFinished, this, &CreateContractor::checkPeselNumber);
	connect(mRegon, &QLineEdit::editingFinished, this, &CreateContractor::checkRegonNumber);
	connect(mPhone, &QLineEdit::editingFinished, this, &CreateContractor::checkPhoneNumber);
	connect(mGusButton, &QPushButton::clicked, this, &CreateContractor::gusButtonHandle);

	connect(mCityButton, &QPushButton::clicked, this, [&]{
		showCityDictionary(mCity);
	});
	connect(mPostButton, &QPushButton::clicked, this, [&]{
		showCityDictionary(mPostOffice);
	});
}

void CreateContractor::createModels()
{
	QCompleter* cityCompleter = new QCompleter(this);
	cityCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	cityCompleter->setModel(new ::widgets::models::CityDictionaryModel(cityCompleter));
	cityCompleter->setCompletionRole(Qt::DisplayRole);
	cityCompleter->setCaseSensitivity(Qt::CaseInsensitive);

	mCity->setCompleter(cityCompleter);
	mPostOffice->setCompleter(cityCompleter);
	mState->setModel(new core::models::StatesListModel(this));
}

QWidget *CreateContractor::createGeneralWidget()
{
	QWidget* widget = new QWidget(this);

	mProvider = new QRadioButton(tr("Dostawca"), this);
	mCustomer = new QRadioButton(tr("Odbiorca"), this);

	mCustomer->setChecked(true);

	QVBoxLayout* contractorTypeLayout = new QVBoxLayout;
	contractorTypeLayout->addWidget(mProvider);
	contractorTypeLayout->addWidget(mCustomer);

	mName = new QLineEdit(this);
	mPersonName = new QLineEdit(this);
	mPersonSurname = new QLineEdit(this);

	QFormLayout* personDataLayout = new QFormLayout;
	personDataLayout->addRow(tr("Nazwa"), mName);
	personDataLayout->addRow(tr("Imię"), mPersonName);
	personDataLayout->addRow(tr("Nazwisko"), mPersonSurname);
	personDataLayout->addRow(tr("Rodzaj"), contractorTypeLayout);

	mNip = new QLineEdit(this);
	mRegon = new QLineEdit(this);
	mPesel = new QLineEdit(this);
	mGusButton = new QPushButton(tr("&GUS"), this);
	mGusButton->setMaximumWidth(36);
	mGusButton->setToolTip(tr("Wyszukuje podmiot w bazie REGON GUS po podanym numerze NIP"));

	mNip->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mRegon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPesel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mGusButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* nipLayout = new QHBoxLayout;
	nipLayout->addWidget(mNip);
	nipLayout->addWidget(mGusButton);

	QFormLayout* personDocumentLayout = new QFormLayout;
	personDocumentLayout->addRow(tr("NIP"), nipLayout);
	personDocumentLayout->addRow(tr("REGON"), mRegon);
	personDocumentLayout->addRow(tr("PESEL"), mPesel);

	QGroupBox* addressGroup = new QGroupBox(tr("Dane teleadresowe"), this);
	mCountry = new QLineEdit(this);
	mCity = new QLineEdit(this);
	mPostOffice = new QLineEdit(this);
	mStreet = new QLineEdit(this);
	mPhone = new QLineEdit(this);

	mCityButton = new QPushButton(tr("Miasto"), this);
	mCityButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mPostButton = new QPushButton(tr("Poczta"), this);
	mPostButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* addressLeftLayout = new QFormLayout;
	addressLeftLayout->addRow(tr("Kraj"), mCountry);
	addressLeftLayout->addRow(mCityButton, mCity);
	addressLeftLayout->addRow(mPostButton, mPostOffice);
	addressLeftLayout->addRow(tr("Ulica"), mStreet);
	addressLeftLayout->addRow(tr("Telefon"), mPhone);

	mIsoCountry = new QLineEdit(this);
	mState = new QComboBox(this);
	mZipCode = new QLineEdit(this);
	mHomeNumber = new QLineEdit(this);
	mLocalNumber = new QLineEdit(this);
	mEmail = new QLineEdit(this);
	mUrl = new QLineEdit(this);

	mState->setEditable(true);
	mIsoCountry->setMaximumWidth(48);
	mHomeNumber->setMaximumWidth(65);
	mLocalNumber->setMaximumWidth(65);

	mState->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mZipCode->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* homeNumberLayout = new QHBoxLayout;
	homeNumberLayout->addWidget(mHomeNumber);
	homeNumberLayout->addWidget(new QLabel("/", this));
	homeNumberLayout->addWidget(mLocalNumber);
	homeNumberLayout->addStretch(1);

	QFormLayout* addressRightLayout = new QFormLayout;
	addressRightLayout->addRow(tr("Kraj ISO"), mIsoCountry);
	addressRightLayout->addRow(tr("Województwo"), mState);
	addressRightLayout->addRow(tr("Kod pocztowy"), mZipCode);
	addressRightLayout->addRow("", homeNumberLayout);
	addressRightLayout->addRow(tr("E-Mail"), mEmail);
	addressRightLayout->addRow(tr("URL"), mUrl);

	QHBoxLayout* personLayout = new QHBoxLayout;
	personLayout->addLayout(personDataLayout);
	personLayout->addSpacing(24);
	personLayout->addLayout(personDocumentLayout);

	QHBoxLayout* addressLayout = new QHBoxLayout(addressGroup);
	addressLayout->addLayout(addressLeftLayout);
	addressLayout->addSpacing(24);
	addressLayout->addLayout(addressRightLayout);

	mMasterContractorGroup = new QGroupBox(tr("Centralizacja jednostek podmiotowych"), this);
	mMasterContractorGroup->setCheckable(true);
	mMasterContractorGroup->setChecked(false);
	mMasterContractorGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mMasterContractor = new widgets::ContractorSelector(this);

	QHBoxLayout* m = new QHBoxLayout(mMasterContractorGroup);
	m->addWidget(new QLabel(tr("Jednostka podległa pod")));
	m->addWidget(mMasterContractor);

	QVBoxLayout* mainLayout = new QVBoxLayout(widget);
	mainLayout->addLayout(personLayout);
	mainLayout->addSpacing(8);
	mainLayout->addWidget(addressGroup);
	mainLayout->addSpacing(8);
	mainLayout->addWidget(mMasterContractorGroup);

	return widget;
}

} // namespace eSawmill
