#include "comarch/comarchcreatecontractor.hpp"
#include <pushbutton.h>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <dialogs/citydictionarydialog.hpp>
#include <models/stateslistmodel.hpp>


namespace eSawmill::contractors::comarch {

CreateContractor::CreateContractor(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle(tr("Tworzenie nowego kontrahenta dla Comarch ERP"));

	createAllWidgets();
	createValidators();
}

core::comarch::Contractor CreateContractor::contractor()
{
        core::comarch::Contractor contractorComarch;
	contractorComarch.setCode(mCode->text().toUpper());
	contractorComarch.setGroup(mGroup->text().toUpper());
	contractorComarch.setName(mName[0]->text().toUpper());
	contractorComarch.setName(mName[1]->text().toUpper(), 1);
	contractorComarch.setName(mName[2]->text().toUpper(), 2);
	contractorComarch.setNip(mNip->text(), mNip->text().length() > 0 ? mNipCode->text() : "");
	contractorComarch.setRegon(mRegon->text());
	contractorComarch.setPesel(mPesel->text());
	contractorComarch.setDocumentId(mDocumentId->text().toUpper());
	contractorComarch.setCountry(mCountry->text().toUpper());
	contractorComarch.setCountryIso(mCountryIso->text().toUpper());
	contractorComarch.setCity(mCity->text().toUpper());
	contractorComarch.setPostOffice(mPostOffice->text().toUpper());
	contractorComarch.setStreet(mStreet->text().toUpper());
	contractorComarch.setPhone(mPhone[0]->text().length() > 3 ? mPhone[0]->text() : "");
	contractorComarch.setPhone(mPhone[1]->text().length() > 3 ? mPhone[1]->text() : "", 1);
	contractorComarch.setSmsPhone(mSmsPhone->text().length() > 3 ? mSmsPhone->text() : "");
	contractorComarch.setState(mState->currentText().toUpper());
	contractorComarch.setPostCode(mPostCode->text().toUpper());
	contractorComarch.setHomeNumber(mHomeNumber->text().toUpper());
	contractorComarch.setLocalNumber(mLocalNumber->text().toUpper());
	contractorComarch.setFax(mFax->text());
	contractorComarch.setEmail(mEmail->text().toUpper());
	contractorComarch.setUrl(mUrl->text().toUpper());

	return contractorComarch;
}

void CreateContractor::createAllWidgets()
{
	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(createGenericGroup());
	mainLayout->addWidget(createAddressGroup());

	::widgets::PushButton* acceptButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("F10"), this, QIcon(":/icons/accept"));
	::widgets::PushButton* rejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icon/reject"));

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(acceptButton);
	buttonsLayout->addWidget(rejectButton);

	mainLayout->addLayout(buttonsLayout);

	// połączenia sygnał -> slot
	connect(acceptButton, &::widgets::PushButton::clicked, this, &CreateContractor::accept);
	connect(rejectButton, &::widgets::PushButton::clicked, this, &CreateContractor::reject);

	connect(mCityButton, &QPushButton::clicked, this, [&](){
		showCityDictionary(mCity);
	});
	connect(mOfficeButton, &QPushButton::clicked, this, [&](){
		showCityDictionary(mPostOffice);
	});
}

void CreateContractor::createValidators()
{
	mPostCode->setInputMask("99-999;_");
	mNipCode->setInputMask(">AA;_");
	mCountryIso->setInputMask(">AA;_");
	mPhone[0]->setInputMask("+99 999999999;_");
	mPhone[1]->setInputMask("+99 999999999;_");
	mSmsPhone->setInputMask("+99 999999999;_");
	mFax->setInputMask("+48 999999999;_");
	mPesel->setInputMask("99999999999;_");
	mNip->setInputMask("9999999999;_");

	mPhone[0]->setText("48");
	mPhone[1]->setText("48");
	mSmsPhone->setText("48");
	mFax->setText("48");
	mNipCode->setText("PL");
	mCountryIso->setText("PL");
	mCountry->setText(tr("Polska"));

	// walidacja wprowadzenia tylko tekstu razem z białymi znakami np. "Nowy Sacz"
	QRegularExpressionValidator* nameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z\\s]+"), this);
//	QRegExpValidator* stringValidator = new QRegExpValidator(QRegExp("[A-Za-z]+"), this);
	QRegularExpressionValidator* homeNumberValidator = new QRegularExpressionValidator(QRegularExpression("[\\d]+[A-Za-z]*"), this);

	for (int i = 0; i < 3; i++)
		mName[i]->setValidator(nameValidator);

	mCountry->setValidator(nameValidator);
	mCity->setValidator(nameValidator);
	mPostOffice->setValidator(nameValidator);
	mStreet->setValidator(nameValidator);
//	mState->setValidator(stringValidator);
	mHomeNumber->setValidator(homeNumberValidator);
	mLocalNumber->setValidator(homeNumberValidator);
}

void CreateContractor::showCityDictionary(QLineEdit *lineEdit)
{
	::widgets::dialogs::CityDictionaryDialog* dialog = new ::widgets::dialogs::CityDictionaryDialog(this);
	if (dialog->exec() == QDialog::Accepted) {
		lineEdit->setText(dialog->city());
	}
}

QWidget* CreateContractor::createGenericGroup()
{
	QGroupBox* group = new QGroupBox(tr("Dane ogólne"), this);

	mCode = new QLineEdit(this);
	mGroup = new QLineEdit(this);
	mReceiver = new QCheckBox(tr("O"), this);
	mProvider = new QCheckBox(tr("D"), this);
	mCompetition = new QCheckBox(tr("K"), this);
	mPartner = new QCheckBox(tr("P"), this);
	mPotentialCustomer = new QCheckBox(tr("PT"), this);

	mReceiver->setToolTip(tr("Odbiorca"));
	mProvider->setToolTip(tr("Dostawca"));
	mCompetition->setToolTip(tr("Konkurencja"));
	mPartner->setToolTip(tr("Partner"));
	mPotentialCustomer->setToolTip(tr("Klient potencjalny"));

	for (int i = 0; i < 3; i++)
		mName[i] = new QLineEdit(this);

	mReceiver->setChecked(true);	// odbiorca zaznaczony jako podstawowy rodzaj kontrahenta

	mNipCode = new QLineEdit(this);
	mNip = new QLineEdit(this);
	mRegon = new QLineEdit(this);
	mPesel = new QLineEdit(this);
	mDocumentId = new QLineEdit(this);

	mNipCode->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mNipCode->setMaximumWidth(60);

	QHBoxLayout* typesLayout = new QHBoxLayout();
	typesLayout->addWidget(mReceiver);
	typesLayout->addWidget(mProvider);
	typesLayout->addWidget(mCompetition);
	typesLayout->addWidget(mPartner);
	typesLayout->addWidget(mPotentialCustomer);
	typesLayout->addStretch(1);

	QHBoxLayout* nipLayout = new QHBoxLayout();
	nipLayout->addWidget(mNipCode);
	nipLayout->addWidget(mNip);

	QFormLayout* leftColumnLayout = new QFormLayout;
	leftColumnLayout->addRow(tr("Kod"), mCode);
	leftColumnLayout->addRow(tr("Grupa"), mGroup);
	leftColumnLayout->addRow(tr("Rodzaj"), typesLayout);

	QFormLayout* rightColumnLayout = new QFormLayout;
	rightColumnLayout->addRow(tr("NIP"), nipLayout);
	rightColumnLayout->addRow(tr("REGON"), mRegon);
	rightColumnLayout->addRow(tr("PESEL"), mPesel);
	rightColumnLayout->addRow(tr("Dok. tożsamości"), mDocumentId);

	QVBoxLayout* nameRowLayout = new QVBoxLayout;
	for (int i = 0; i < 3; i++)
		nameRowLayout->addWidget(mName[i]);

	QFormLayout* nameLayout = new QFormLayout;
	nameLayout->addRow(tr("Nazwa"), nameRowLayout);

	QHBoxLayout* columnLayout = new QHBoxLayout;
	columnLayout->addLayout(leftColumnLayout);
	columnLayout->addLayout(rightColumnLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(group);
	mainLayout->addLayout(columnLayout);
	mainLayout->addLayout(nameLayout);

	return group;
}

QWidget *CreateContractor::createAddressGroup()
{
	QGroupBox* group = new QGroupBox(tr("Dane teleadresowe"), this);
	mCountry = new QLineEdit(this);
	mCity = new QLineEdit(this);
	mPostOffice = new QLineEdit(this);
	mStreet = new QLineEdit(this);
	mPhone[0] = new QLineEdit(this);
	mPhone[1] = new QLineEdit(this);
	mSmsPhone = new QLineEdit(this);
	mCityButton = new QPushButton(tr("Miasto"), this);
	mOfficeButton = new QPushButton(tr("Poczta"), this);

	QVBoxLayout* phonesLayout = new QVBoxLayout;
	phonesLayout->addWidget(mPhone[0]);
	phonesLayout->addWidget(mPhone[1]);

	QFormLayout* leftColumnLayout = new QFormLayout;
	leftColumnLayout->addRow(tr("Kraj"), mCountry);
	leftColumnLayout->addRow(mCityButton, mCity);
	leftColumnLayout->addRow(mOfficeButton, mPostOffice);
	leftColumnLayout->addRow(tr("Ulica"), mStreet);
	leftColumnLayout->addRow(tr("Telefony"), phonesLayout);
	leftColumnLayout->addRow(tr("Telefon SMS"), mSmsPhone);

	mCountryIso = new QLineEdit(this);
	mState = new QComboBox(this);
	mPostCode = new QLineEdit(this);
	mHomeNumber = new QLineEdit(this);
	mLocalNumber = new QLineEdit(this);
	mFax = new QLineEdit(this);
	mEmail = new QLineEdit(this);
	mUrl = new QLineEdit(this);

	mState->setEditable(true);
	// add model to field
	mState->setModel(new core::models::StatesListModel(this));

	mPostCode->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mHomeNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mLocalNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountryIso->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCityButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mOfficeButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountryIso->setMaximumWidth(65);

	QHBoxLayout* homeNumberLayout = new QHBoxLayout;
	homeNumberLayout->addWidget(mHomeNumber);
	homeNumberLayout->addWidget(new QLabel("/", this));
	homeNumberLayout->addWidget(mLocalNumber);
	homeNumberLayout->addStretch(1);

	QFormLayout* rightColumnLayout = new QFormLayout;
	rightColumnLayout->addRow(tr("Kraj ISO"), mCountryIso);
	rightColumnLayout->addRow(tr("Województwo"), mState);
	rightColumnLayout->addRow(tr("Kod pocztowy"), mPostCode);
	rightColumnLayout->addRow(tr("Nr domu"), homeNumberLayout);
	rightColumnLayout->addRow(tr("Fax"), mFax);
	rightColumnLayout->addRow(tr("E-Mail"), mEmail);
	rightColumnLayout->addRow(tr("URL"), mUrl);

	QHBoxLayout* mainLayout = new QHBoxLayout(group);
	mainLayout->addLayout(leftColumnLayout);
	mainLayout->addLayout(rightColumnLayout);

	return group;
}

} // namespace eSawmill
