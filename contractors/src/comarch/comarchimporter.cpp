#include "comarch/comarchimporter.hpp"
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QFormLayout>
#include <QScopedPointer>
#include "comarch/comarchcreatecontractor.hpp"


namespace eSawmill::contractors::comarch {

ComarchImporter::ComarchImporter(QWidget* parent)
	: QDialog(parent)
	, mCurrentContractorId(-1)
{
	setWindowTitle(tr("Import kontrahentów z Comarch ERP"));

	createAllWidgets();
	createAllModels();

	connect(mTableView, &::widgets::TableView::doubleClicked, this, [=](const QModelIndex &index){
		Q_UNUSED(index);
		doubleClickedHandle();
	});

	connect(mAddButton, &::widgets::PushButton::clicked, this, &ComarchImporter::addContractorHandle);
	connect(mFilterButton, &QPushButton::clicked, this, &ComarchImporter::filterButtonHandle);
	connect(mFilterGroup, &QGroupBox::clicked, [this](bool checked){
		mFilter->setActive(checked);
	});
}

void ComarchImporter::createAllWidgets()
{
	mTableView = new ::widgets::TableView(this);
	mFilterGroup = new QGroupBox(tr("Filtry"), this);
	mFilterGroup->setCheckable(true);
	mFilterGroup->setChecked(false);

	mName = new QLineEdit(this);
	mNip = new QLineEdit(this);
	mRegon = new QLineEdit(this);
	mCity = new QLineEdit(this);
	mStreet = new QLineEdit(this);
	mPostCode = new QLineEdit(this);
	mPhone = new QLineEdit(this);
	mFilterButton = new QPushButton(QIcon(":/icons/find"), tr("&Filtruj"), this);

	mFilterButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mFilterGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* filterGeneralLayout = new QFormLayout;
	filterGeneralLayout->addRow(tr("Nazwa"), mName);
	filterGeneralLayout->addRow(tr("NIP"), mNip);
	filterGeneralLayout->addRow(tr("REGON"), mRegon);

	QFormLayout* filterAddressLayout = new QFormLayout;
	filterAddressLayout->addRow(tr("Miejscowość"), mCity);
	filterAddressLayout->addRow(tr("Ulica"), mStreet);
	filterAddressLayout->addRow(tr("Kod pocztowy"), mPostCode);
	filterAddressLayout->addRow(tr("Telefon"), mPhone);

	QVBoxLayout* filterButtonsLayout = new QVBoxLayout;
	filterButtonsLayout->addWidget(mFilterButton);
	filterButtonsLayout->addStretch(1);

	QHBoxLayout* filterLayout = new QHBoxLayout(mFilterGroup);
	filterLayout->addLayout(filterGeneralLayout);
	filterLayout->addSpacing(12);
	filterLayout->addLayout(filterAddressLayout);
	filterLayout->addLayout(filterButtonsLayout);
	filterLayout->addStretch(1);

	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addStretch(1);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTableView);
	mainLayout->addWidget(mFilterGroup);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void ComarchImporter::createAllModels()
{
	mModel = new models::ComarchContractorsModel(this);
	mFilter = new eSawmill::contractors::filters::ComarchContractorFilter(this);

	mFilter->setSourceModel(mModel);
	mTableView->setModel(mFilter);
}

void ComarchImporter::doubleClickedHandle()
{
	mCurrentContractorId = mTableView->currentIndex().data(models::ComarchContractorsModel::Roles::Id).toUInt();
	accept();
}

void ComarchImporter::addContractorHandle()
{
	QScopedPointer<eSawmill::contractors::comarch::CreateContractor> dialog(new eSawmill::contractors::comarch::CreateContractor(this));
	if (dialog->exec() == eSawmill::contractors::comarch::CreateContractor::Accepted) {
		mModel->createOrUpdate(dialog->contractor());
	}
}

void ComarchImporter::filterButtonHandle() {
	mFilter->setName(mName->text());
	mFilter->setNIP(mNip->text());
	mFilter->setREGON(mRegon->text());
	mFilter->setCity(mCity->text());
	mFilter->setStreet(mStreet->text());
	mFilter->setPostCode(mPostCode->text());
	mFilter->setPhone(mPhone->text());
}

} // namespace eSawmill
