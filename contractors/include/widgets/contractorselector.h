#ifndef CONTRACTORSELECTOR_H
#define CONTRACTORSELECTOR_H

#include "../contractors_global.h"
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>

#include <dbo/contractor.h>	// moduł core



namespace eSawmill::contractors::widgets {

class LineEdit;
class CONTRACTORS_EXPORT ContractorSelector : public QWidget {
	Q_OBJECT

public:
	ContractorSelector(QWidget* parent = nullptr);

	inline core::Contractor contractor() const {
		return mCurrentSelectedContractor;
	}

	inline void selectContractor(core::Contractor contractor) {
		mCurrentSelectedContractor = contractor;
		emit contractorSelected(contractor);
	}

	/**
	 * @brief setReadOnly - ustawia możliwość edycji danych w kontrolce,
	 * lub możliwość tylko do odczytu
	 * @param readonly
	 */
	void setReadOnly(bool readonly);

private:
	LineEdit* mNameView;
	QPushButton* mButton;

	void createWidgets();
	core::Contractor mCurrentSelectedContractor;

private Q_SLOTS:
	void button_Clicked();
	void userSelectContractor(core::Contractor c);

signals:
	void contractorSelected(const core::Contractor c);
};

class LineEdit : public QLineEdit {
	Q_OBJECT

public:
	explicit LineEdit(QWidget* parent = nullptr);

protected:
	virtual void mouseDoubleClickEvent(QMouseEvent* event) override;

Q_SIGNALS:
	void doubleClicked();
};

} // namespace eSawmill

#endif // CONTRACTORSELECTOR_H
