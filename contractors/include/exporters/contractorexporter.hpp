#ifndef CONTRACTOREXPORTER_HPP
#define CONTRACTOREXPORTER_HPP

#include <abstracts/abstractexporter.hpp>
#include "../contractors_global.h"
#include <dbo/contractor.h>

namespace eSawmill::contractors::exporters {

class CONTRACTORS_EXPORT ContractorExporter : public core::abstracts::AbstractExporter<core::Contractor> {
public:
	explicit ContractorExporter();
	virtual ~ContractorExporter() = default;

	bool exportData(QIODevice& device) override;
	bool importData(QIODevice& device) override;

	QDomElement toDomNode(QDomDocument& document) override;
	void fromDomNode(const QDomElement& element) override;
};

} // namespace eSawmill::contractors::exporters

#endif // CONTRACTOREXPORTER_HPP
