#ifndef CREATECONTRACTOR_H
#define CREATECONTRACTOR_H

#include "contractors_global.h"

#include <QDialog>
#include <QGroupBox>
#include <QRadioButton>
#include <QLineEdit>
#include <QComboBox>
#include <QDomElement>

#include "widgets/contractorselector.h"
#include <pushbutton.h>	// moduł widgets
#include <dbo/contractor.h>	// moduł core
#include <dbo/company.h>


namespace eSawmill::contractors {

///
/// \brief The CreateContractor class
/// dialog to create or edit contractor
///
class CONTRACTORS_EXPORT CreateContractor : public QDialog {
	Q_OBJECT

public:
	explicit CreateContractor(QWidget* parent = nullptr);
	explicit CreateContractor(int id, QWidget* parent = nullptr);
	virtual ~CreateContractor() = default;

	///
	/// \brief CreateContractor
	/// constructor to load data about contractor imported from Comarch ERP database
	/// \param contractor - contractor object
	/// \param contact
	/// \param parent
	///
	CreateContractor(core::Contractor contractor,
					 core::Contact contact,
					 QWidget* parent = nullptr);


	///
	/// \brief contractor
	/// return contractor object created from data entered in form by user
	/// \return
	///
	core::Contractor contractor();
	///
	/// \brief contact
	/// return contact object created from data endtered in form by user
	/// \return
	///
	core::Contact contact();

private:
	///@{
	/// controls group
	QGroupBox* mContractorTypeGroup;
	QGroupBox* mContactGroup;
	QGroupBox* mContractorGroup;

	QRadioButton* mProvider;
	QRadioButton* mCustomer;

	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;

	///< data contractor group
	QLineEdit* mName;
	QLineEdit* mPersonName;
	QLineEdit* mPersonSurname;
	QLineEdit* mNip;
	QLineEdit* mRegon;
	QLineEdit* mPesel;
	QPushButton* mGusButton;

	///< data contact group
	QLineEdit* mCountry;
	QLineEdit* mIsoCountry;
	QComboBox* mState;
	QLineEdit* mCity;
	QLineEdit* mZipCode;
	QLineEdit* mPostOffice;
	QLineEdit* mStreet;
	QLineEdit* mHomeNumber;
	QLineEdit* mLocalNumber;
	QLineEdit* mPhone;
	QLineEdit* mEmail;
	QLineEdit* mUrl;
	QPushButton* mCityButton;	///< button to choose city from dictionary
	QPushButton* mPostButton;	///< button to choose post office from distionary

	QGroupBox* mMasterContractorGroup;
	widgets::ContractorSelector* mMasterContractor;
	///@}

	void createWidgets();
	void createConnections();
	void createModels();

	QWidget* createGeneralWidget();

	///
	/// \brief createValidators
	/// create validatros to check entered data before saved in db
	///
	void createValidators();

	///@{
	/// variables
	core::Contractor mContractor;	///< object loaded to edit
	core::Contact mContact;			///< object loaded to edit
	bool mEditContractor;			///< true - if window is opened in edit mode
	///@}

private Q_SLOTS:
	///
	/// \brief checkNipNumber
	/// validate entered nip number
	///
	void checkNipNumber();

	///
	/// \brief checkPeselNumber
	/// validate entered pesel number
	///
	void checkPeselNumber();

	///
	/// \brief checkRegonNumber
	/// validate entered regon number
	///
	void checkRegonNumber();

	///
	/// \brief checkPhoneNumber
	/// checks if the entered phone number alredy exists in the database
	///
	void checkPhoneNumber();

	///
	/// \brief showCityDictionary
	/// \param lineEdit
	///
	void showCityDictionary(QLineEdit *lineEdit);

	///
	/// \brief gusButtonHandle
	/// handle for action search contractor in REGON GUS database
	///
	void gusButtonHandle();

Q_SIGNALS:
	///
	/// \brief contractorCreated (SIGNAL)
	/// emitted when user created new contractor
	/// \param contractor
	/// \param contact
	///
	void contractorCreated(
		const core::Contractor &contractor,	///< [in]
		const core::Contact &contact		///< [in]
	);


	///
	/// \brief contractorUpdated (SIGNAL)
	/// emitted when user update data in existing contractor
	/// \param contractor
	///
	void contractorUpdated(
		const core::Contractor &contractor	///< [in]
	);
};

} // namespace eSawmill

#endif // CREATECONTRACTOR_H
