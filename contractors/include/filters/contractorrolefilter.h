#ifndef CONTRACTORROLEFILTER_H
#define CONTRACTORROLEFILTER_H

#include "../contractors_global.h"

#include <QSortFilterProxyModel>
#include "../models/contractors.h"

namespace eSawmill::contractors::filters {

/**
 * Rozdziela dostawców z odbiorcami
 * zapewnia wyświetlenie wybranej grupy kontrahentów (dostawca lub odbiorca)
 * według informacji
 */
class CONTRACTORS_EXPORT ContractorRoleFilter : public QSortFilterProxyModel
{
public:
	ContractorRoleFilter(QObject* parent = nullptr);

	/**
	 * @brief filterType
	 * @return ContractorType
	 *
	 * @details zwraca aktualny typ wybranego kontrahenta
	 */
	inline int filterType() const { return mFilterType; }

public slots:
	/**
	 * @brief setFilterType
	 * @param type
	 *
	 * @details ustawia typ kontrahenta jaki aktualnie ma być wyświetlany
	 */
	inline void setFilterType(int type) {
		mFilterType = type;
		this->invalidateFilter();
	}

private:
	int mFilterType;	// typ kontrahenta (Dostawca, Odbiorca)

	// odfiltrowuje wiersze
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

} // namespace eSawmill

#endif // CONTRACTORROLEFILTER_H
