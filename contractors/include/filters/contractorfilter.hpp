#ifndef CONTRACTORFILTER_HPP
#define CONTRACTORFILTER_HPP

#include "../contractors_global.h"
#include <QSortFilterProxyModel>


namespace eSawmill::contractors::filters {

class CONTRACTORS_EXPORT ContractorFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	ContractorFilter(QObject* parent = nullptr);

public slots:
	void setActive(bool active);
	void setPersonName(const QString &name);
	void setPersonSurname(const QString &surname);
	void setName(const QString &name);
	void setNip(const QString &nip);
	void setPesel(const QString &pesel);
	void setPhone(const QString &phone);
	void clearAllFilters();

private:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
	bool filterByName(const QString &value) const;
	bool filterByPersonName(const QString &value) const;
	bool filterByPersonSurname(const QString &value) const;
	bool filterByNip(const QString &value) const;
	bool filterByPesel(const QString &value) const;
	bool filterByPhone(const QString &value) const;

	bool mIsActive;
	QString mName;
	QString mPersonName;
	QString mPersonSurname;
	QString mNip;
	QString mPesel;
	QString mPhone;
};

} // namespace eSawmill

#endif // CONTRACTORFILTER_HPP
