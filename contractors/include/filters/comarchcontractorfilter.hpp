#ifndef COMARCHCONTRACTORFILTER_HPP
#define COMARCHCONTRACTORFILTER_HPP

#include "../contractors_global.h"
#include <QSortFilterProxyModel>
#include <QDebug>


namespace eSawmill::contractors::filters {

class CONTRACTORS_EXPORT ComarchContractorFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	ComarchContractorFilter(QObject* parent = nullptr);

public slots:
	void setName(const QString name) {
		mName = name;
		invalidateFilter();
	}

	void setNIP(const QString nip) {
		mNIP = nip;
		invalidateFilter();
	}

	void setREGON(const QString regon) {
		mREGON = regon;
		invalidateFilter();
	}

	void setCity(const QString city) {
		mCity = city;
		invalidateFilter();
	}

	void setStreet(const QString street) {
		mStreet = street;
		invalidateFilter();
	}

	void setPostCode(const QString postCode) {
		mPostCode = postCode;
		invalidateFilter();
	}

	void setPhone(const QString phone) {
		mPhone = phone;
		invalidateFilter();
	}

	void setActive(bool active) {
		mIsActive = active;
		invalidateFilter();
	}

private:
	QString mName;
	QString mNIP;
	QString mREGON;
	QString mCity;
	QString mStreet;
	QString mPostCode;
	QString mPhone;
	bool mIsActive;

	bool checkName(const QString name) const;
	bool checkNip(const QString nip) const;
	bool checkRegon(const QString regon) const;
	bool checkCity(const QString city) const;
	bool checkStreet(const QString street) const;
	bool checkPostCode(const QString postCode) const;
	bool checkPhone(const QString phone) const;

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

} // namespace eSawmill

#endif // COMARCHCONTRACTORFILTER_HPP
