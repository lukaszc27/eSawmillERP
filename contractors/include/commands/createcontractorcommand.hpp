#ifndef CREATECONTRACTORCOMMAND_HPP
#define CREATECONTRACTORCOMMAND_HPP

#include "../contractors_global.h"
#include <dbo/contractor.h>
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::contractors::commands {

/// @brief CreateContractorCommand class
/// create new contractor instance in database
class CONTRACTORS_EXPORT CreateContractorCommand : public core::abstracts::AbstractCommand {
public:
	explicit CreateContractorCommand(const core::Contractor& contractor, QWidget* parent = nullptr);

	/// @brief execute
	/// execute this command
	bool execute(core::repository::AbstractRepository& repository) override;

	/// @brief canUndo
	/// return information to global command executor
	/// so this command can be rollback
	bool canUndo() const override { return true; }

	/// @brief undo
	/// rollback this command
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Contractor _contractor;
};

} // namespace eSawmill::contractors::commands


#endif // CREATECONTRACTORCOMMAND_HPP
