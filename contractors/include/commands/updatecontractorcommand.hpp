#ifndef UPDATECONTRACTORCOMMAND_HPP
#define UPDATECONTRACTORCOMMAND_HPP

#include "../contractors_global.h"
#include <abstracts/abstractcommand.hpp>
#include <dbo/contractor.h>

namespace eSawmill::contractors::commands {

/// @brief UpdateContractorCommand class
/// command to update existing contractor in repository
class CONTRACTORS_EXPORT UpdateContractorCommand : public core::abstracts::AbstractCommand {
public:
	explicit UpdateContractorCommand(const core::Contractor& contractor, QWidget* parent = nullptr);

	/// @brief execute
	/// execute this command actions
	bool execute(core::repository::AbstractRepository& repository) override;

	bool canUndo() const override { return true; }
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Contractor _contractor;
	core::Contractor _beforeEditingContractor;	///< copy of object before editing
};

} // namespace eSawmill::contractors::commands

#endif // UPDATECONTRACTORCOMMAND_HPP
