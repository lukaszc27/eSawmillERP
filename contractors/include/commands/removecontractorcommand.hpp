#ifndef REMOVECONTRACTORCOMMAND_HPP
#define REMOVECONTRACTORCOMMAND_HPP

#include "../contractors_global.h"
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::contractors::commands {

class CONTRACTORS_EXPORT RemoveContractorCommand : public core::abstracts::AbstractCommand {
public:
	explicit RemoveContractorCommand(const core::eloquent::DBIDKey& contractorId, QWidget* parent = nullptr);

	bool execute(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Contractor _removedContractor;
	core::eloquent::DBIDKey _contractorId;
};

} // namespace eSawmill::contractors::commands

#endif // REMOVECONTRACTORCOMMAND_HPP
