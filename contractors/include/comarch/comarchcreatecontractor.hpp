#ifndef COMARCHCREATECONTRACTOR_HPP
#define COMARCHCREATECONTRACTOR_HPP

#include "../contractors_global.h"
#include <QDialog>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <comarch/contractorscomarch.hpp>	// obiekt kontrahenta z modułu core


namespace eSawmill::contractors::comarch {

class CONTRACTORS_EXPORT CreateContractor : public QDialog {
	Q_OBJECT

public:
	CreateContractor(QWidget* parent = nullptr);
	~CreateContractor() {};

	/**
	 * @brief contractor - zwraca obiekt kontrahenta utworzonego
	 * z danych wprowadzonych w dialogu
	 * @return Obiekt ContractorComarch
	 */
	core::comarch::Contractor contractor();

private:
	void createAllWidgets();
	QWidget* createGenericGroup();
	QWidget* createAddressGroup();

	/**
	 * @brief createValidators - walidacja wprowadzanych danych do formularza
	 */
	void createValidators();

	QLineEdit* mCode;
	QLineEdit* mGroup;
	QLineEdit* mName[3];
	QLineEdit* mNip;
	QLineEdit* mNipCode;
	QLineEdit* mRegon;
	QLineEdit* mPesel;
	QLineEdit* mDocumentId;			// dokument tożsamości
	QCheckBox* mReceiver;			// odbiorca
	QCheckBox* mProvider;			// dostawca
	QCheckBox* mCompetition;		// konkurencja
	QCheckBox* mPartner;			// partner
	QCheckBox* mPotentialCustomer;	// potencialny klient
	QPushButton* mCityButton;
	QPushButton* mOfficeButton;

	QLineEdit* mCountry;
	QLineEdit* mCity;
	QLineEdit* mPostOffice;
	QLineEdit* mStreet;
	QLineEdit* mPhone[2];
	QLineEdit* mSmsPhone;
	QLineEdit* mCountryIso;
	QComboBox* mState;
	QLineEdit* mPostCode;
	QLineEdit* mHomeNumber;
	QLineEdit* mLocalNumber;
	QLineEdit* mFax;
	QLineEdit* mEmail;
	QLineEdit* mUrl;

	/**
	 * @brief showCityDictionary - show dialog where user can select city from list
	 * @param lineEdit - field where selected city name is set
	 */
	void showCityDictionary(QLineEdit* lineEdit);
};

} // namespace eSawmill

#endif // COMARCHCREATECONTRACTOR_HPP
