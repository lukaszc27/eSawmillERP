#ifndef COMARCHIMPORTER_HPP
#define COMARCHIMPORTER_HPP

#include "contractors_global.h"
#include <QDialog>
#include <QLineEdit>
#include <QGroupBox>
#include <tableview.h>
#include <pushbutton.h>
#include "models/comarchcontractors.hpp"
#include "filters/comarchcontractorfilter.hpp"


namespace eSawmill::contractors::comarch {

class CONTRACTORS_EXPORT ComarchImporter : public QDialog {
	Q_OBJECT

public:
	explicit ComarchImporter(QWidget* parent = nullptr);
	virtual ~ComarchImporter() = default;

	/**
	 * @brief selectedContractor - zwraca aktualnie zaznaczonego użytkownika
	 * @return id zaznaczonego użytkownika
	 */
	int selectedContractor() const { return mCurrentContractorId; }

private:
	void createAllWidgets();
	void createAllModels();

	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	QLineEdit* mName;
	QLineEdit* mNip;
	QLineEdit* mRegon;
	QLineEdit* mCity;
	QLineEdit* mStreet;
	QLineEdit* mPostCode;
	QLineEdit* mPhone;
	QPushButton* mFilterButton;
	QGroupBox* mFilterGroup;

	// zmienne
	int mCurrentContractorId;

	// modele
	eSawmill::contractors::models::ComarchContractorsModel* mModel;
	eSawmill::contractors::filters::ComarchContractorFilter* mFilter;

private Q_SLOTS:
	///
	/// \brief doubleClickedHandle
	/// handle to user double clicked in list/table
	///
	void doubleClickedHandle();

	///
	/// \brief addContractorHandle
	/// show dialog to add new contractor do Comarch database and eSawmill database
	/// and add data to database
	///
	void addContractorHandle();

	///
	/// \brief filterButtonHandle
	///
	void filterButtonHandle();
};

} // namespace eSawmill

#endif // COMARCHIMPORTER_HPP
