#ifndef CONTRACTORS_H
#define CONTRACTORS_H

#include "../contractors_global.h"
#include <QStringList>
#include <abstracts/abstractmodel.h>
#include <commandexecutor.hpp>
#include "dbo/contractor.h"
#include "dbo/contact.h"
#include "../createcontractor.h"


namespace eSawmill::contractors::models {

///
/// \brief The Contractors class
/// main contractors model present data in manager
/// model get data from repository
///
class CONTRACTORS_EXPORT Contractors
		: public core::models::AbstractModel<core::Contractor>
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	explicit Contractors(QObject* parent = nullptr);
	virtual ~Contractors();

	///@{
	/// methods used by Qt framework to show data in model

	///
	/// \brief rowCount
	/// return number of rows in model
	/// this method should return number of elements in m_contractors list
	/// \param index
	/// \return
	///
	int rowCount(const QModelIndex& index) const override;
	///
	/// \brief columnCount
	/// return number of columns in model
	/// this method should return length array returned by headerData method
	/// \param index
	/// \return
	///
	int columnCount(const QModelIndex& index) const override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& vaule, int role = Qt::EditRole) override;
	///@}

	///
	/// \brief headerData
	/// set title for columns
	/// \return title for column by index
	///
	QStringList headerData() const override;


	void get() override;

	///
	/// \brief The Columns enum
	/// column indexes in model
	///
	enum Columns {
		Name,
		PersonName,
		PersonSurname,
		NIP,
		REGON,
		PESEL,
		City,
		ZipCode,
		Street,
		HomeNumber,
		Phone,
		EMail
	};

	///
	/// \brief The Roles enum
	/// additional roles
	///
	enum Roles {
		ContractorType = Qt::UserRole + 1,
		ContractorId
	};

	///
	/// \brief The ContractorType enum
	/// contractor type stored in model
	///
	enum ContractorType {
		Customer,	///< customers, has orders or services in sawmill
		Provider	///< providers
	};

	///@{
	/// DEPRECATED METHODS - SCHOULD BE REMOVED IN NEXT REFACTORING

	///
	/// \brief createOrUpdate
	/// create or update existing contractors
	/// \param contrator
	///
	void createOrUpdate(core::Contractor contrator) override;
	///
	/// \brief destroy
	/// remove from database existing contractors
	/// \param contractor
	///
	void destroy(core::Contractor contractor) override;
	///@}

private:
	// CommandExecutor::Listener implementation
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;
};

} // namespace eSawmill

#endif // CONTRACTORS_H
