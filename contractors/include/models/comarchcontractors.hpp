#ifndef COMARCHCONTRACTORS_HPP
#define COMARCHCONTRACTORS_HPP

#include <abstracts/abstractmodel.h>
#include <comarch/contractorscomarch.hpp>
#include "contractors_global.h"


namespace eSawmill::contractors::models {

class CONTRACTORS_EXPORT ComarchContractorsModel : public core::models::AbstractModel<core::comarch::Contractor> {
	Q_OBJECT

public:
	ComarchContractorsModel(QObject* parent = nullptr);
	~ComarchContractorsModel();

	int rowCount(const QModelIndex& index) const override;
    int columnCount(const QModelIndex& index) const override;

	/**
     * @brief flags
     * @param index
     * @return
     */
	Qt::ItemFlags flags(const QModelIndex& index) const override;

	/**
         * @brief headerData
         * @param section - nr kolumny
         * @param orientation - orientacja (pionowa/pozioma)
         * @param role
         * @return QVariant
         *
         * @details nadaje tytuły dla odpowiednich kolumn w modelu
         */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override { return mColumnName; }

    /**
         * @brief data
         * @param index
         * @param role
         * @return QVariant
         *
         * @details uzupełnia model danymi z listy mList
         */
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

	void get() override;
	void createOrUpdate(core::comarch::Contractor contractor) override;
	void destroy(core::comarch::Contractor contractor) override {
		Q_UNUSED(contractor);
	}

	enum Columns {
		Name,
		NIP,
		REGON,
		City,
		Street,
		HomeNumber,
		PostCode,
		PostOffice,
		Phone,
		Email
	};

	enum Roles {
		Id = Qt::UserRole + 1
	};

private:
	QStringList mColumnName;
};

} // namespace eSawmill

#endif // COMARCHCONTRACTORS_HPP
