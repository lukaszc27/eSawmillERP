#ifndef CONTRACTORMANAGER_H
#define CONTRACTORMANAGER_H

#include "contractors_global.h"

#include <QDialog>
#include <QListView>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QSortFilterProxyModel>

#include "tableview.h"
#include "pushbutton.h"
#include "models/contractors.h"
#include "filters/contractorrolefilter.h"
#include "filters/contractorfilter.hpp"
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill::contractors {

///
/// \brief The ContractorManager class
/// main contractor manager
///
class CONTRACTORS_EXPORT ContractorManager : public QDialog {
	Q_OBJECT

public:
	explicit ContractorManager(
			QWidget *parent = nullptr,
			bool selectorMode = false,		///< dialog opened in selector mode
			bool provierSelector = false);	///< information so user want to choose providers

	virtual ~ContractorManager() = default;

	///
	/// \brief selectedContractor
	/// get current selected contracotr object
	///
	core::Contractor selectedContractor() const;

	///
	/// \brief createFrom create new contractor with data get from XML stream
	/// \param element - root node of XML tree
	/// \param checkBefore - check if a user exist before in database
	/// \return id number created contractor
	///
	static int createFrom(const QDomElement& element, bool checkBefore = true);

public Q_SLOTS:
	void notifyRepository(const agent::RepositoryEvent& event);

private:
	///@{
	/// controls
	::widgets::TableView*	mTableView;
	::widgets::PushButton*	mAddButton;
	::widgets::PushButton*	mRemakeButton;
	::widgets::PushButton*	mDeleteButton;
	::widgets::PushButton*	mCloseButton;
	QListView*		mContractorSelector;
	///@}

	///@{
	/// controls for filter widget
	QGroupBox* mFilterGroup;
	QLineEdit* mName;
	QLineEdit* mPersonName;
	QLineEdit* mPersonSurname;
	QLineEdit* mNip;
	QLineEdit* mPesel;
	QLineEdit* mPhone;
	QPushButton* mFilterButton;
	QPushButton* mFilterClearButton;
	///@}

	///@{
	/// models
	models::Contractors*			mContractors;
	filters::ContractorRoleFilter*	mContractorFilter;
	filters::ContractorFilter*		mFilter;
	///@}

	void createWidgets();
	void createConnections();
	void createModels();

	///@{
	/// variables
	bool mSelectorMode;										///< mode to choose contractor
	core::eloquent::DBIDKey mSelectedCurrentContractorId;	///< id current editing contractor
	///@}

private Q_SLOTS:
	void addButton_Clicked();
	void removeButtonHandle();
	void selectedContractor_Clicked(const QModelIndex& index);
	void editContractors_ReturnPressed(const QModelIndex &index);

	///
	/// @brief clearFilterButtonHandle
	/// clear all fields for filter
	///
	void clearFilterButtonHandle();

	///
	/// @brief filterRecordsHandle
	/// filter records in view by filter proxy model
	///
	void filterRecordsHandle();

	///
	/// \brief importFromComarchHandle
	/// import contractor from Comarch ERP Optima
	///
	void importFromComarchHandle();
};

} // namespace eSawmill

#endif // CONTRACTORMANAGER_H
