add_library(contractors-lib)
target_sources(contractors-lib PRIVATE
    "include/contractormanager.h"
    "include/contractors_global.h"
    "include/createcontractor.h"
    "src/contractormanager.cpp"
    "src/createcontractor.cpp"

    # comarch sub directory
    "include/comarch/comarchimporter.hpp"
    "include/comarch/comarchcreatecontractor.hpp"
    "src/comarch/comarchcreatecontractor.cpp"
    "src/comarch/comarchimporter.cpp"

    # commands sub directory
    "include/commands/updatecontractorcommand.hpp"
    "include/commands/createcontractorcommand.hpp"
    "include/commands/removecontractorcommand.hpp"
    "src/commands/createcontractorcommand.cpp"
    "src/commands/updatecontractorcommand.cpp"
    "src/commands/removecontractorcommand.cpp"

    # exporters sub directory
    "include/exporters/contractorexporter.hpp"
    "src/exporters/contractorexporter.cpp"

    # filters sub directory
    "include/filters/comarchcontractorfilter.hpp"
    "include/filters/contractorfilter.hpp"
    "include/filters/contractorrolefilter.h"
    "src/filters/comarchcontractorfilter.cpp"
    "src/filters/contractorfilter.cpp"
    "src/filters/contractorrolefilter.cpp"

    # models sub dir
    "include/models/comarchcontractors.hpp"
    "include/models/contractors.h"
    "src/models/comarchcontractors.cpp"
    "src/models/contractors.cpp"

    # widgets sub dir
    "include/widgets/contractorselector.h"
    "src/widgets/contractorselector.cpp"
)
target_compile_definitions(contractors-lib PRIVATE CONTRACTORS_LIBRARY)
target_include_directories(contractors-lib
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
)
target_link_libraries(contractors-lib
    Qt::Gui
    Qt::Widgets
    Qt::Sql
    Qt::Xml
    Qt::Network
    
    core-lib
    widgets-lib
)
install(TARGETS contractors-lib)
