# eSawmillERP
eSawmillERP is a production management application fo a family sawmill.
The first version of the program was created in 2018 and is still used today.

The program allows you to create production cost statements and export data to XML or CSV file.

created with:
* Qt5 (last compiled in version 5.15)
* C++17 (I try use it version ;-) )
* T-SQL (the database engine is MS SQL Server)

## Target operating systems
* Linux - Created and tested on Ubuntu
* Windows - for other users who don't like linux :blush:)
* macOS - On this time I don't have anything Apple devices, so maybe in future tests this app on Apple system.

## Plans for future.
I wont to create production centre app where workers will can see tasks and orders to be performed.
I think the best solution will be create web app to production center, so I will create it Laravel framework and ReactJS (in other repro (I will give link in this place when I start create))

<img src="./docs/assets/eSawmill_main_window.png" alt="eSawmill main window" width="100%" />
<img src="./docs/assets/orders.png" alt="orders manager" width="33%" />
<img src="./docs/assets/add_order.png" alt="add order widget" width="33%" />
<img src="./docs/assets/add_contractor.png" alt="add contractor widget" width="33%" />