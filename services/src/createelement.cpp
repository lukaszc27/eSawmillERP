#include "createelement.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGroupBox>
#include <QSettings>
#include <models/woodtypelistmodel.hpp>
#include <dbo/woodtype.hpp>
#include <globaldata.hpp>

namespace eSawmill {
namespace services {

CreateElement::CreateElement(QWidget* parent)
	: QDialog (parent)
{
	setWindowTitle(tr("Dodawanie kłody"));
	createWidgets();
	createConnections();

	mWoodType->setModel(new core::models::WoodTypeListModel(this));
}

core::service::Element CreateElement::element() {
	// get wood type by repository
	const auto& wood = core::GlobalData::instance().repository().woodTypeRepository().get(
		mWoodType->currentData(core::models::WoodTypeListModel::Roles::Id).toInt());

	// subtract wood bark from diameter (if that option is enabled)
	double diameter = mDiameter->value();
	if (QSettings settings; settings.value("Service/usedRemoveBark").toBool()) {
		if (diameter > wood.woodBark())
			diameter -= wood.woodBark();
	}
	core::service::Element element;
	element.setDiameter(diameter);
	element.setLength(mLength->value());
	element.setRotated(mRotated->isChecked());
	element.setWoodType(wood);
	return element;
}

void CreateElement::createWidgets()
{
	mDiameter = new QDoubleSpinBox(this);
	mLength = new QDoubleSpinBox(this);
	mRotated = new QCheckBox(tr("Kantowane"), this);
	mWoodType = new QComboBox(this);
	mAcceptButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));

	mDiameter->setMaximum(200);	// max 200cm średnicy drzewa

	QLabel* diameterUnitLabel = new QLabel(tr("cm"), this);
	diameterUnitLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QLabel* lengthUnitLabel = new QLabel(tr("m"), this);
	lengthUnitLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* diameterLayout = new QHBoxLayout;
	diameterLayout->addWidget(mDiameter);
	diameterLayout->addWidget(diameterUnitLabel);

	QHBoxLayout* lengthLayout = new QHBoxLayout;
	lengthLayout->addWidget(mLength);
	lengthLayout->addWidget(lengthUnitLabel);

	QGroupBox* sizeGroup = new QGroupBox(tr("Wymiary"), this);
	QFormLayout* formLayout = new QFormLayout(sizeGroup);
	formLayout->addRow(tr("Średnica"), diameterLayout);
	formLayout->addRow(tr("Długość"), lengthLayout);
//	formLayout->addWidget(mRotated);

	QGroupBox* detailsGroup = new QGroupBox(tr("Właściwości"), this);
	QFormLayout* detailsLayout = new QFormLayout(detailsGroup);
	detailsLayout->addRow(tr("Rodzaj"), mWoodType);
	detailsLayout->addRow("", mRotated);

	QHBoxLayout* groupsLayout = new QHBoxLayout;
	groupsLayout->addWidget(sizeGroup);
	groupsLayout->addWidget(detailsGroup);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(groupsLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);

	mDiameter->setFocus();
	mDiameter->selectAll();
}

void CreateElement::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateElement::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateElement::reject);
}

} // namespace services
} // namespace eSawmill
