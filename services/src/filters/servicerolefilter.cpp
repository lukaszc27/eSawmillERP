#include "filters/servicerolefilter.hpp"
#include "models/services.hpp"
#include <globaldata.hpp>


namespace eSawmill {
namespace services {
namespace filters {

ServiceRoleFilter::ServiceRoleFilter(QObject *parent)
	: QSortFilterProxyModel (parent)
	, mIndex(0)
{
}

bool ServiceRoleFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	auto& repository = core::GlobalData::instance().repository().serviceRepository();

	auto id = sourceModel()->index(source_row, 0, source_parent).data(models::Services::Id).toUInt();
	const core::Service service = repository.get(id);
//	core::Service service = core::eloquent::Model::find<core::Service>(id);
	switch (mIndex) {
	case All:
		return true;

	case ToImplemented:	// do realizacji
		return !service.isRealised();

	case AfterDeadline:
		return service.endDate() < QDate::currentDate() && !service.isRealised();

	case Realised:
		return service.isRealised();
	}
	return QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);
}

} // namespace filters
} // namespace services
} // namespace eSawmill
