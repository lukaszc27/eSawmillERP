#include "filters/servicesfilter.hpp"
#include "models/services.hpp"


namespace eSawmill::services::filters {

ServicesFilter::ServicesFilter(QObject* parent)
	: QSortFilterProxyModel(parent)
	, mEnableFilterByPriority(false)
	, mActiveFilter(false)
{
}

QVariant ServicesFilter::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Vertical && role == Qt::DisplayRole)
		return QVariant(QString::number(section + 1));

	return QSortFilterProxyModel::headerData(section, orientation, role);
}

bool ServicesFilter::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
	Q_UNUSED(source_parent);

	if(!mActiveFilter)
		return true;

	return filterByName(sourceModel()->index(source_row, models::Services::Columns::Name).data().toString()) &&
			filterByNumber(sourceModel()->index(source_row, models::Services::Columns::Number).data().toString())&&
			filterByContractorName(sourceModel()->index(source_row, models::Services::Columns::ContractorName).data().toString()) &&
			filterByPhone(sourceModel()->index(source_row, models::Services::Columns::ContractorPhone).data().toString()) &&
			filterByPriority(sourceModel()->index(source_row, models::Services::Columns::Priority).data().toString());
}

bool ServicesFilter::filterByName(const QString &value) const {
	return value.toUpper().contains(mName.toUpper());
}

bool ServicesFilter::filterByNumber(const QString &number) const {
	return number.contains(mNumber);
}

bool ServicesFilter::filterByPriority(const QString &priority) const {
	if (!mEnableFilterByPriority)
		return true;

	return priority.toUpper().contains(mPriority.toUpper());
}

bool ServicesFilter::filterByContractorName(const QString &contractor) const {
	return contractor.toUpper().contains(mContractorName.toUpper());
}

bool ServicesFilter::filterByPhone(const QString &phone) const {
	return phone.contains(mContractorPhone);
}


} // namespace
