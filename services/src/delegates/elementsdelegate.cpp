#include "delegates/elementsdelegate.hpp"
#include "models/elements.hpp"
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <models/woodtypelistmodel.hpp>


namespace eSawmill {
namespace services {
namespace delegates {

ElementsDelegate::ElementsDelegate(QObject* parent)
	: QStyledItemDelegate (parent)
{
}

QWidget* ElementsDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	Q_UNUSED(option);

	if (index.column() != eSawmill::services::models::Elements::Cubature &&
		index.column() != eSawmill::services::models::Elements::Rotated &&
		index.column() != eSawmill::services::models::Elements::WoodType) { // 2 - id kolumny Kubatury
		QDoubleSpinBox* spin = new QDoubleSpinBox(parent);
		spin->setMaximum(10000);
		return spin;
	}
	else if (index.column() == eSawmill::services::models::Elements::Rotated) {
		QCheckBox* check = new QCheckBox(parent);
		return check;
	}
	else if (index.column() == eSawmill::services::models::Elements::WoodType) {
		QComboBox* combo = new QComboBox(parent);
		combo->setModel(new core::models::WoodTypeListModel(parent));
		return combo;
	}
	return new QWidget();
}

void ElementsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() != eSawmill::services::models::Elements::Cubature &&
		index.column() != eSawmill::services::models::Elements::Rotated &&
		index.column() != eSawmill::services::models::Elements::WoodType) {
		QDoubleSpinBox* spin = qobject_cast<QDoubleSpinBox*>(editor);
		spin->setValue(index.data().toDouble());
	}
	else if (index.column() == eSawmill::services::models::Elements::Rotated) {
		QCheckBox* check = qobject_cast<QCheckBox*>(editor);
		check->setChecked(index.data(eSawmill::services::models::Elements::IsRotated).toBool());
	}
	else if (index.column() == eSawmill::services::models::Elements::WoodType) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		combo->setCurrentText(index.data().toString());
	}
}

void ElementsDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() != eSawmill::services::models::Elements::Cubature &&
		index.column() != eSawmill::services::models::Elements::Rotated &&
		index.column() != eSawmill::services::models::Elements::WoodType) {
		QDoubleSpinBox* spin = qobject_cast<QDoubleSpinBox*>(editor);
		model->setData(index, spin->value());
	}
	else if (index.column() == eSawmill::services::models::Elements::Rotated) {
		QCheckBox* check = qobject_cast<QCheckBox*>(editor);
		model->setData(index, check->isChecked());
	}
	else if (index.column() == eSawmill::services::models::Elements::WoodType) {
		QComboBox* combo = qobject_cast<QComboBox*>(editor);
		model->setData(index, combo->currentData(core::models::WoodTypeListModel::Id));
	}
}

} // namespace delegates
} // namespace services
} // eSawmill
