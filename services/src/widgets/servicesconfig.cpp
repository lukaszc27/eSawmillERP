#include "widgets/servicesconfig.hpp"
#include <QFormLayout>
#include <QLayout>
#include <QSettings>
#include <QSpacerItem>
#include <QLabel>


namespace eSawmill {
namespace config {
namespace widgets {

Services::Services(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget (parent)
{
	setWindowTitle(tr("Usługi/Zlecenia"));
	setWindowIcon(QIcon(":/icons/services"));

	createAllWidgets();

	connect(mRemindCheck, QOverload<int>::of(&QCheckBox::stateChanged), this, [&](int state){
		mDaysToRemind->setEnabled(state);
		mBeforeColorSelector->setEnabled(state);
	});

	connect(mDeadlineCheck, QOverload<int>::of(&QCheckBox::stateChanged), this, [&](int state){
		mDeadlineColorSelector->setEnabled(state);
	});
}

void Services::createAllWidgets()
{
	mRemindCheck = new QCheckBox(tr("Wyróżnij zlecenia o zbliżającym się terminie realizacji"), this);
	mDeadlineCheck = new QCheckBox(tr("Wyróżnij zlecenia po terminie realizacji"), this);
	mDaysToRemind = new QSpinBox(this);
	mPriorityColor = new QCheckBox(tr("Wyróżnaj zlecenia na liście pod względem przydzielonego priotytetu"), this);
	mFastInsert = new QCheckBox(tr("Szybkie wprowadzanie kłód"), this);
	mPrefix = new QLineEdit(this);
	mUsedRemoveBark = new QCheckBox("Zmniejszaj średnicę kłody o grubość kory", this);

	mBeforeColorSelector = new ::widgets::ColorSelector(this);
	mDeadlineColorSelector = new ::widgets::ColorSelector(this);

	mDaysToRemind->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDaysToRemind->setMinimumWidth(75);
	mPrefix->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* layout = new QFormLayout(this);
	layout->addRow(tr("Prefix numerowania zleceń"), mPrefix);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mRemindCheck);
	layout->addRow(tr("Ilość dni na ostrzeganie przed upływem terminu realizacji"), mDaysToRemind);
	layout->addRow(tr("Kolor wyrużnienia"), mBeforeColorSelector);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mDeadlineCheck);
	layout->addRow(tr("Kolor oznakowania zamówień o przekroczonym terminie"), mDeadlineColorSelector);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Fixed));
	layout->addRow(mPriorityColor);
	layout->addItem(new QSpacerItem(0, 3));
	layout->addRow(new QLabel("<b>Wprowadzanie kłód</b>", this));
	layout->addRow(mFastInsert);
	layout->addRow(mUsedRemoveBark);
}

void Services::save()
{
	QSettings settings;
	settings.setValue("Service/daysBeforeWarning", mDaysToRemind->value());
	settings.setValue("Service/beforeWarningColor", mBeforeColorSelector->color());
	settings.setValue("Service/RemindBefore", mRemindCheck->isChecked());
	settings.setValue("Service/RemindAfter", mDeadlineCheck->isChecked());
	settings.setValue("Service/deadlineColor", mDeadlineColorSelector->color());
	settings.setValue("Service/prefix", mPrefix->text().toUpper());
	settings.setValue("Service/fastInsert", mFastInsert->isChecked());
	settings.setValue("Service/usedRemoveBark", mUsedRemoveBark->isChecked());
}

void Services::initialize()
{
	QSettings settings;
	mDaysToRemind->setValue(settings.value("Service/daysBeforeWarning", 0).toInt());
	mRemindCheck->setChecked(settings.value("Service/RemindBefore", true).toBool());
	mDeadlineCheck->setChecked(settings.value("Service/RemindAfter", true).toBool());
	mBeforeColorSelector->setCurrentColor(settings.value("Service/beforeWarningColor").value<QColor>());
	mDeadlineColorSelector->setCurrentColor(settings.value("Service/deadlineColor").value<QColor>());
	mPrefix->setText(settings.value("Service/prefix").toString());
	mFastInsert->setChecked(settings.value("Service/fastInsert").toBool());
	mUsedRemoveBark->setChecked(settings.value("Service/usedRemoveBark", true).toBool());
}

} // namespace widgets
} // namespace config
} // namespace eSawmill
