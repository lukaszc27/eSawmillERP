#include "widgets/elementswidget.hpp"
#include "createelement.hpp"
#include "delegates/elementsdelegate.hpp"
#include "exporters/elementsxmlexporter.hpp"
#include "exporters/elementscsvexporter.hpp"
#include <messagebox.h>
#include <memory>
#include <abstracts/abstractexporter.hpp>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScopedPointer>
#include <QMenu>
#include <QFileDialog>
#include <QFile>
#include <QSettings>
#include <QTimer>
#include <globaldata.hpp>


namespace eSawmill {
namespace services {
namespace widgets {

ElementsWidget::ElementsWidget(core::Service& service, QWidget* parent)
	: QWidget {parent}
	, service_{service}
{
	createWidgets();
	createModels();
	createConnections();
}

//void ElementsWidget::loadService(const core::Service& service) {
//	Q_ASSERT(service.id() > 0);

//	auto& repository = core::GlobalData::instance().repository().serviceRepository();
//	QList<core::service::Element> elements = repository.getServiceElements(service.id());
//	mElements->insert(elements);
//}

void ElementsWidget::createWidgets()
{
	mElementsView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mTotalCubatureView = new QLabel(this);
	mShowRotatedColumn = new QCheckBox(tr("Ukryj kolumnę \"Kantowane\""), this);
	mShowWoodTypeColumn = new QCheckBox(tr("Ukryj kolumnę \"Rodzaj drewna\""), this);

	mTotalCubatureView->setAlignment(Qt::AlignRight);

	QMenu* operationMenu = new QMenu(this);
	operationMenu->addAction("Oznacz jako kantowane", this, &ElementsWidget::markElementsAsRotated);
	operationMenu->addAction("Oznacz jako nie kantowane", this, &ElementsWidget::markElementsAsNoRotated);
	operationMenu->addSeparator();
	operationMenu->addAction(QIcon(":/icons/import"), tr("Import"), this, &ElementsWidget::importAction_triggered);
	operationMenu->addAction(QIcon(":/icons/export"), tr("Eksport"), this, &ElementsWidget::exportAction_triggered);

	::widgets::PushButton* operationButton = new ::widgets::PushButton(tr("Operacje"), QKeySequence(""), this);
	operationButton->setMenu(operationMenu);

	QHBoxLayout* checkLayout = new QHBoxLayout;
	checkLayout->addWidget(mShowWoodTypeColumn);
	checkLayout->addSpacing(12);
	checkLayout->addWidget(mShowRotatedColumn);
	checkLayout->addStretch(1);
	checkLayout->addWidget(mTotalCubatureView);

	QBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(operationButton);
	buttonsLayout->addStretch(2);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mElementsView);
	mainLayout->addLayout(checkLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void ElementsWidget::createModels()
{
	mElements = new eSawmill::services::models::Elements(this, service_.id());
	mElementsView->setModel(mElements);
	mElementsView->setItemDelegate(new eSawmill::services::delegates::ElementsDelegate(this));

	mElements->insert(service_.elements());
}

void ElementsWidget::createConnections()
{
	connect(mAddButton, &::widgets::PushButton::clicked, this, &ElementsWidget::addButton_clicked);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &ElementsWidget::removeButton_clicked);
	connect(mElements, &eSawmill::services::models::Elements::modelDataChanged, this, &ElementsWidget::recalculate);
	connect(mShowWoodTypeColumn, &QCheckBox::stateChanged, [this](int state){
		if (state) mElementsView->hideColumn(models::Elements::Columns::WoodType);
		else mElementsView->showColumn(models::Elements::Columns::WoodType);
	});
	connect(mShowRotatedColumn, &QCheckBox::stateChanged, [this](int state){
		if (state) mElementsView->hideColumn(models::Elements::Columns::Rotated);
		else mElementsView->showColumn(models::Elements::Columns::Rotated);
	});
	connect(mElementsView, &::widgets::TableView::filesDropped, this, &ElementsWidget::importDataFromDropedFile);
}

void ElementsWidget::addButton_clicked()
{
	QScopedPointer<CreateElement> dialog(new CreateElement(this));
	if (dialog->exec() == QDialog::Accepted) {
		mElements->insert(dialog->element());

		if (QSettings settings; settings.value("Service/fastInsert").toBool()) {
			QTimer::singleShot(60, this, &ElementsWidget::addButton_clicked);
		}
	}
}

void ElementsWidget::removeButton_clicked() {
	auto selectedRows = mElementsView->selectionModel()->selectedRows();
	if (selectedRows.size() <= 0) {
		::widgets::MessageBox::information(this, "Informacja",
										   "Przed wykonaniem operacji usuń musisz zaznaczyć elementy które chcesz usunąć");
		return;
	}

	int result = ::widgets::MessageBox::question(this, "Pytanie",
												 QString("Znaleziono %1 zaznaczonych obiektów czy na pewno chcesz je usunąć?").arg(selectedRows.size()), "",
												 QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::No)
		return;

	// select all elements to delete in model object
	for (auto row : selectedRows)
		mElements->setData(row, true, Qt::CheckStateRole);

	// and remove selected items from list
	mElements->deleteSelectedItems();
}

void ElementsWidget::recalculate()
{
	mTotalCubatureView->setText(QString::asprintf("Razem: %0.2f m<sup>3</sup>", mElements->totalCubature()));
	Q_EMIT totalPriceChanged(totalPrice());
}

void ElementsWidget::importAction_triggered() {
	if (mElements->items().count() > 0) {
		int iRet = ::widgets::MessageBox::question(this, tr("Pytanie"),
												   tr("Na liście znajdują się elementy, czy chcesz usunąć dane przed dokonaniem importu?"), "",
												   QMessageBox::Yes | QMessageBox::No);

		if (iRet == QMessageBox::Yes) mElements->clear();
	}

	QString selectedFilter;
	QString filename = QFileDialog::getOpenFileName(this, "Import danych z pliku", QDir::currentPath(), "Plik XML (*.xml);;Plik CSV (*.csv)", &selectedFilter);
	if (!filename.isEmpty()) {
		std::unique_ptr<core::abstracts::AbstractExporter<core::service::Element>> exporter;
		if (selectedFilter.contains("xml", Qt::CaseInsensitive)) {
			if (!filename.toUpper().endsWith("XML"))
				filename += ".xml";

			exporter = std::make_unique<eSawmill::services::exporters::ElementsXmlExporter>();
		}

		if (selectedFilter.contains("csv", Qt::CaseInsensitive)) {
			if (!filename.toUpper().endsWith("CSV"))
				filename += ".csv";

			exporter = std::make_unique<eSawmill::services::exporters::ElementsCsvExporter>();
		}

		if (QFile file(filename); file.open(QFile::ReadOnly | QFile::Text)) {
			if (exporter->importData(file)) {
				mElements->insert(exporter->data());
			} else {
				::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
											   tr("Podczas importu wystąpiły błędy! Nie wszystkie dane mogły być zaimportowane"));
			}
			file.close();
		}
	}
}

void ElementsWidget::exportAction_triggered() {
	QString selectedFilter;
	QString filename = QFileDialog::getSaveFileName(this, "Eksport danych do pliku",
													QDir::currentPath(),
													"Plik XML (*.xml);;Plik CSV (*.csv)",
													&selectedFilter);

	std::unique_ptr<core::abstracts::AbstractExporter<core::service::Element>> exporter;
	if (selectedFilter.contains("xml", Qt::CaseInsensitive)) {
		if (!filename.toUpper().endsWith("XML"))
			filename += ".xml";

		exporter = std::make_unique<eSawmill::services::exporters::ElementsXmlExporter>();
	}

	if (selectedFilter.contains("csv", Qt::CaseInsensitive)) {
		if (!filename.toUpper().endsWith("CSV"))
			filename += ".csv";

		exporter = std::make_unique<eSawmill::services::exporters::ElementsCsvExporter>();
	}

	if (QFile file(filename); file.open(QFile::WriteOnly | QFile::Text)) {
		exporter->setData(mElements);
		exporter->exportData(file);
		file.close();
	}
	else {
		::widgets::MessageBox::critical(this, tr("Bład eksportu danych"),
			tr("Nie można zapisać danych w wybranej lokalizacji."
			   "Prawdopodobnie nie masz uprawnień do zapisu w tej lokalizacji."));
	}
}

void ElementsWidget::markElementsAsRotated() {
	auto selectedRows = mElementsView->selectionModel()->selectedRows(eSawmill::services::models::Elements::Columns::Rotated);
	if (selectedRows.size() <= 0) {
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Nie znaleziono zaznaczonych obiektów. Czy chcesz wykonać tą operację dla wszystkich obiektów na liście?"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			for (int i = 0; i < mElements->items().size(); i++) {
				const QModelIndex index = mElements->index(i, eSawmill::services::models::Elements::Columns::Rotated);
				mElements->setData(index, true);
			}
		}
	}

	for (auto row : selectedRows)
		mElements->setData(row, true);
}

void ElementsWidget::markElementsAsNoRotated() {
	auto selectedRows = mElementsView->selectionModel()->selectedRows(eSawmill::services::models::Elements::Columns::Rotated);
	if (selectedRows.size() <= 0) {
		int result = ::widgets::MessageBox::question(this, "Pytanie",
													 "Nie znaleziono znaznaczonych obiektów. Czy chcesz wykonać tą operację dla wszystkich obiektów na liście?", "",
													 QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::Yes) {
			for (int i = 0; i < mElements->items().size(); i++) {
				const QModelIndex index = mElements->index(i, eSawmill::services::models::Elements::Columns::Rotated);
				mElements->setData(index, false);
			}
		}
	}

	for (auto row : selectedRows)
		mElements->setData(row, false);
}

void ElementsWidget::importDataFromDropedFile(const QStringList &files) {
	for (auto& file : files) {
		std::unique_ptr<core::abstracts::AbstractExporter<core::service::Element>> exporter;
		if (file.trimmed().endsWith("xml"))
			exporter = std::make_unique<eSawmill::services::exporters::ElementsXmlExporter>();
	}
}

} // namespace widgets
} // namespace services
} // namespace eSawmill
