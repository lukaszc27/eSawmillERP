#include "servicemanager.hpp"
#include "createservice.hpp"
#include "eloquent/builder.hpp"
#include <dbo/servicearticle.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QDebug>
#include <QStringListModel>
#include <QCompleter>
#include <QMenu>
#include <QFileDialog>
#include <QFile>
#include <messagebox.h>
#include <memory>
#include "exporters/servicesxmlexporter.hpp"
#include "commands/updateservicecommand.hpp"
#include "commands/createservicecommand.hpp"
#include "commands/destroyservicecommand.hpp"
#include <commandexecutor.hpp>
#include <globaldata.hpp>
#include <exceptions/invalidimportexception.hpp>
#include <exceptions/forbiddenexception.hpp>


namespace eSawmill {
namespace services {

ServiceManager::ServiceManager(QWidget* parent)
	: QDialog (parent)
{
	setWindowTitle(tr("Usługi"));

	createWidgets();
	createModels();
	createConnections();
}

void ServiceManager::handleRepositoryNotification(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::Service) {
		const auto& selection = mServicesView->selectionModel()->selection();
		mServices->get();
		mServicesView->selectionModel()->select(selection, QItemSelectionModel::Select);
	}
}

void ServiceManager::createWidgets()
{
	mServiceOption = new QListView(this);
	mServicesView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemakeButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mCloseButton = new ::widgets::PushButton(tr("Zamknij"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
	mRecordFilterWidget = new eSawmill::orders::widgets::RecordFilterWidget(this);
	mDateFilterWidget = new eSawmill::orders::widgets::DateFilterWidget(this);

	mServiceOption->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

	::widgets::PushButton* optionButton = new ::widgets::PushButton("Operacje", QKeySequence(), this);
	QMenu* optionMenu = new QMenu(this);
	optionMenu->addAction(QIcon(":/icons/import"), "Import", this, &ServiceManager::importActionHandle);
	optionMenu->addAction(QIcon(":/icons/export"), "Eksport", this, &ServiceManager::exportActionHandle);
	optionButton->setMenu(optionMenu);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemakeButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(optionButton);
	buttonsLayout->addStretch(4);
	buttonsLayout->addWidget(mCloseButton);

	QHBoxLayout* filtersLayout = new QHBoxLayout;
	filtersLayout->addWidget(mRecordFilterWidget);
	filtersLayout->addSpacing(6);
	filtersLayout->addWidget(mDateFilterWidget);
	filtersLayout->addStretch(1);

	QVBoxLayout* rightLayout = new QVBoxLayout;
	rightLayout->addWidget(mServicesView);
	rightLayout->addSpacing(8);
	rightLayout->addLayout(filtersLayout);

	QHBoxLayout* topLayout = new QHBoxLayout;
	topLayout->addWidget(mServiceOption);
	topLayout->addLayout(rightLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(topLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonsLayout);
}

void ServiceManager::createModels()
{
	mServices = new eSawmill::services::models::Services(this);
	mServiceRoleFilter = new eSawmill::services::filters::ServiceRoleFilter(this);
	mServicesFilter = new filters::ServicesFilter(this);
	mDateRangeFilter = new eSawmill::orders::filters::OrderDateRangeFilter(this);

	mServiceRoleFilter->setSourceModel(mServices);
	mServicesFilter->setSourceModel(mServiceRoleFilter);
	mDateRangeFilter->setSourceModel(mServicesFilter);

	mServicesView->setModel(mDateRangeFilter);

	QStringList serviceOption;
	serviceOption << tr("Wszystkie") << tr("Do realizacji") << tr("Po terminie realizacji") << tr("Zrealizowane");
	QStringListModel* serviceOptionModel = new QStringListModel(serviceOption, this);
	mServiceOption->setModel(serviceOptionModel);

	// create completers for fields in record filter
	QCompleter* nameCompleter = new QCompleter(this);
	nameCompleter->setModel(mServices);
	nameCompleter->setCompletionColumn(eSawmill::services::models::Services::Columns::Name);
	nameCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	nameCompleter->setCompletionRole(Qt::DisplayRole);
	nameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	mRecordFilterWidget->nameField()->setCompleter(nameCompleter);

	QCompleter* numberCompleter = new QCompleter(this);
	numberCompleter->setModel(mServices);
	numberCompleter->setCompletionColumn(eSawmill::services::models::Services::Columns::Number);
	numberCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	numberCompleter->setCompletionRole(Qt::DisplayRole);
	numberCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	mRecordFilterWidget->numberField()->setCompleter(numberCompleter);

	QCompleter* contractorNameCompleter = new QCompleter(this);
	contractorNameCompleter->setModel(mServices);
	contractorNameCompleter->setCompletionColumn(eSawmill::services::models::Services::Columns::ContractorName);
	contractorNameCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	contractorNameCompleter->setCompletionRole(Qt::DisplayRole);
	contractorNameCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	mRecordFilterWidget->contractorNameField()->setCompleter(contractorNameCompleter);

	QCompleter* contractorPhoneCompleter = new QCompleter(this);
	contractorPhoneCompleter->setModel(mServices);
	contractorPhoneCompleter->setCompletionColumn(eSawmill::services::models::Services::Columns::ContractorPhone);
	contractorPhoneCompleter->setCompletionMode(QCompleter::CompletionMode::InlineCompletion);
	contractorPhoneCompleter->setCompletionRole(Qt::DisplayRole);
	contractorPhoneCompleter->setCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
	contractorPhoneCompleter->setCompletionPrefix("+48");
	mRecordFilterWidget->contractorPhoneField()->setCompleter(contractorPhoneCompleter);
}

void ServiceManager::createConnections()
{
	connect(mCloseButton, &::widgets::PushButton::clicked, this, &ServiceManager::reject);
	connect(mAddButton, &::widgets::PushButton::clicked, this, &ServiceManager::addService_clicked);
	connect(mRemakeButton, &::widgets::PushButton::clicked, this, &ServiceManager::editService_clicked);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &ServiceManager::removeServiceHandle);
	connect(mRecordFilterWidget, &QGroupBox::clicked,
			mServicesFilter, &eSawmill::services::filters::ServicesFilter::setActiveFilter);
	connect(mRecordFilterWidget->priorityEnable(), &QCheckBox::stateChanged,
			mServicesFilter, &eSawmill::services::filters::ServicesFilter::setEnableFilterByPriority);
	connect(mRecordFilterWidget->priority(), &QComboBox::currentTextChanged, [this](const QString &priority){
		mServicesFilter->setPriority(priority);
	});
	connect(mDateFilterWidget, &eSawmill::orders::widgets::DateFilterWidget::clicked,
			mDateRangeFilter, &eSawmill::orders::filters::OrderDateRangeFilter::setActive);
	connect(mDateFilterWidget, &eSawmill::orders::widgets::DateFilterWidget::dateRangeChanged,
			this, &ServiceManager::recordFilterByDateHandle);

	connect(mRecordFilterWidget, &eSawmill::orders::widgets::RecordFilterWidget::filterDataChanged,
			this, &ServiceManager::recordFilterHandle);

	connect(mServicesView, &::widgets::TableView::doubleClicked, this, [&](const QModelIndex &index){
		Q_UNUSED(index);
		editService_clicked();
	});

	/// wybranie kryteriów filtrowania usług
	connect(mServiceOption, &QListView::clicked, this, [&](const QModelIndex &index){
		mServiceRoleFilter->filterByIndex(index.row());
	});
	connect(mServicesView, &::widgets::TableView::filesDropped, this, &ServiceManager::importServicesByDroppingFile);
}

void ServiceManager::commandExecuted()
{
	mServices->get();
}

void ServiceManager::addService_clicked() {
	core::Service service{};
	QScopedPointer<eSawmill::services::CreateService> dialog(
		new eSawmill::services::CreateService(service, this));
	
	if (dialog->exec() == QDialog::Accepted)
	{
		auto& commandExecutor = core::GlobalData::instance().commandExecutor();
		std::unique_ptr<commands::CreateServiceCommand> command
				= std::make_unique<commands::CreateServiceCommand>(service, this);

		if (!commandExecutor.execute(std::move(command)))
		{
			::widgets::MessageBox::critical(this,
				tr("Błąd operacji"), tr("Błąd podczas tworzenia nowego zlecenia w bazie"));
		}
	}
}

void ServiceManager::editService_clicked() {
	const quint32 serviceId = mServicesView->currentIndex().data(eSawmill::services::models::Services::Id).toUInt();
	Q_ASSERT(serviceId > 0);
	try {
		auto& repository = core::GlobalData::instance().repository().serviceRepository();
		if (!repository.lock(serviceId))
			return;

		core::Service service = repository.get(serviceId);

		QScopedPointer<CreateService> dialog(new CreateService(service, this));
		if (dialog->exec() == QDialog::Accepted)
		{
			std::unique_ptr<commands::UpdateServiceCommand> command
					= std::make_unique<commands::UpdateServiceCommand>(service, this);

			auto& commandExecutor = core::GlobalData::instance().commandExecutor();
			if (!commandExecutor.execute(std::move(command)))
			{
				::widgets::MessageBox::critical(this,
					tr("Błąd"), tr("Błąd podczas aktualizacji zlecenia w bazie danych"));
			}
		}
		if (!repository.unlock(serviceId))
			return;
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this,
			tr("Blokada dostępu"),
			tr("Wybrane zlecenie jest aktualnie edytowane przez innego użytkownika.\n"
				"Poczekaj na zakończenie edycji - blokada dostępu"));
	}
}

void ServiceManager::removeServiceHandle()
{
	const core::eloquent::DBIDKey serviceId = mServicesView->currentIndex()
			.data(eSawmill::services::models::Services::Id).toUInt();

	Q_ASSERT(serviceId > 0);
	if (serviceId <= 0)
	{
		::widgets::MessageBox::critical(this, tr("Bład"),
			tr("Nie pobrano prawidłowego identyfikatora zlecenia"));
		return;
	}

	auto const ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
		tr("Czy na pewno chcesz usunąć zaznaczoną pozycję?"), "",
		QMessageBox::Yes | QMessageBox::No);

	if (ret == QMessageBox::No)
		return;

	std::unique_ptr<commands::DestroyServiceCommand> command
			= std::make_unique<commands::DestroyServiceCommand>(serviceId, this);

	auto& commandExecutor = core::GlobalData::instance().commandExecutor();
	if (!commandExecutor.execute(std::move(command)))
	{
		::widgets::MessageBox::critical(this, tr("Błąd wykonywania operacji"),
			tr("Błąd podczas wykonywania operacji usuwania usługi z bazy!"));
	}
}

void ServiceManager::recordFilterHandle(const orders::widgets::RecordFilterWidget::FilterOptions &options) {
	mServicesFilter->setName(options.name);
	mServicesFilter->setNumber(options.number);
	mServicesFilter->setEnableFilterByPriority(options.usePriority);
	mServicesFilter->setPriority(options.priority);
	mServicesFilter->setContractorName(options.contractorName);
	mServicesFilter->setContractorPhone(options.contractorPhone);

}

void ServiceManager::recordFilterByDateHandle(const orders::widgets::DateFilterWidget::DateFilterOptions &options) {
	mDateRangeFilter->setFilterKeyColumn(options.filterByColumn);
	mDateRangeFilter->setMinDate(options.minDate);
	mDateRangeFilter->setMaxDate(options.maxDate);
}

void ServiceManager::exportActionHandle() {
	if (!mServicesView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::information(this, "Eksport usług do pliku",
										   "Przed wykonaniem operacji eksportu musisz zaznaczyć elementy przeznaczone do eksportu");
		return;
	}

	QString filename = QFileDialog::getSaveFileName(this, "Eksport usług do pliku", QDir::currentPath(), "Plik XML (*.xml)");
	if (filename.isEmpty())
		return;

	if (!filename.endsWith(".xml"))
		filename += ".xml";

	try {
		QFile file(filename);
		if (file.open(QFile::WriteOnly | QFile::Text)) {
			std::unique_ptr<eSawmill::services::exporters::ServicesXmlExporter> exporter =
					std::make_unique<eSawmill::services::exporters::ServicesXmlExporter>();

			QList<core::Service> services;
			auto rows = mServicesView->selectionModel()->selectedRows();
			for (auto& row : rows) {
				core::Service service = core::GlobalData::instance().repository()
										.serviceRepository().get(row.data(eSawmill::services::models::Services::Id).toUInt());
				services.append(service);
			}
			exporter->setData(services);
			exporter->exportData(file);
			file.close();
		}
	} catch (core::exceptions::InvalidImportException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::critical(this, tr("Błąd"),
										tr("Podczas eksportu zamówienia wystąpiły błędy uniemożliwiające zapisanie danych do pliku."));
	}
}

void ServiceManager::importActionHandle() {
	QString filename = QFileDialog::getOpenFileName(this, "Importowanie usługi z pliku",
													QDir::currentPath(), "Plik XML (*.xml)");

	if (filename.isEmpty())
		return;

	try {
		QFile file(filename);
		if (file.open(QFile::ReadOnly | QFile::Text)) {
			std::unique_ptr<eSawmill::services::exporters::ServicesXmlExporter> importer =
					std::make_unique<eSawmill::services::exporters::ServicesXmlExporter>();

			importer->importData(file);
			file.close();
			mServices->get();
		}
	} catch (core::exceptions::InvalidImportException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::critical(this, tr("Błąd"),
			tr("Podczas importu danych z pliku wystąpiły błędy uniemożliwiające odczytanie danych z pliku"));
	}
}

void ServiceManager::importServicesByDroppingFile(const QStringList &files)
{
	try {
		for (auto& filename : files) {
			QFile file(filename);
			if (file.open(QFile::ReadOnly | QFile::Text)) {
				std::unique_ptr<eSawmill::services::exporters::ServicesXmlExporter> importer =
						std::make_unique<eSawmill::services::exporters::ServicesXmlExporter>();

				importer->importData(file);
				file.close();
			} else {
				::widgets::MessageBox::critical(this, tr("Błąd otwierania pliku do importu"),
					tr("Nie można otworzyć wskazanego pliku.\n"
					   "Brak dosępu!"));
				continue;
			}
		}
	} catch (core::exceptions::InvalidImportException&) {
		::widgets::MessageBox::critical(this, tr("Błąd"),
			tr("Podczas importu danych z pliku wystąpiły błędy uniemożliwiające odczytanie danych z pliku"));
	}
}

} // namespace services
} // namespace eSawmill
