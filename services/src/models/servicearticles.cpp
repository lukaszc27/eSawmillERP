#include "models/servicearticles.hpp"
#include <dbo/service.h>
#include <dbo/servicearticle.h>
#include <dbo/article.h>


namespace eSawmill {
namespace service {
namespace model {

Articles::Articles(QObject* parent)
	: eSawmill::orders::models::Articles(parent)
{
}

Articles::Articles(int id, QObject* parent)
	: eSawmill::orders::models::Articles(id, parent)
{
	mId = id;
	get();
}

QVariant Articles::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant {};

	int row = index.row();
	int col = index.column();

	const core::Article& current = mList[row];
	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return current.name();
		case EAN: return current.barCode();
		case Quantity: return current.quantity();
		case Unit: return current.unit().shortName();
		case Price: return current.priceSaleBrutto();
		case Value: {
			double qty = current.quantity();
			double price = current.priceSaleBrutto();
			return QString::asprintf("%0.2f", price * qty);
		}
		} break;

		break;
	}

	return QVariant();
}

void Articles::get() {
	beginResetModel();
//	core::Service service = core::eloquent::Model::find<core::Service>(mId);
//	mList = service.articles();
	endResetModel();
}

double Articles::totalPrice() {
	double total {0};
	for (const auto& item : items()) {
		total += (item.quantity() * item.priceSaleBrutto());
	}
	return total;
}

void Articles::notifyDataChanged() {
	Q_EMIT articlesChanged();
}

} // namespace model
} // namespace service
} // namespace eSawmill
