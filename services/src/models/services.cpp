#include "models/services.hpp"
#include "dbo/contractor.h"
#include <messagebox.h>
#include <globaldata.hpp>
#include <QSettings>


namespace eSawmill {
namespace services {
namespace models {

Services::Services(QObject* parent)
	: core::models::AbstractModel<core::Service>{parent}
{
	mHeaders << tr("Nazwa") << tr("Numer") << tr("Kontrahent") << tr("Nr. telefonu")
			 << tr("Priorytet") << tr("Data dodania") << tr("Termin realizacji");

	get();
}

Qt::ItemFlags Services::flags(const QModelIndex &index) const {
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);
	flags |= Qt::ItemIsEnabled;

	return flags;
}

//int Services::rowCount(const QModelIndex &index) const
//{
//	if (index.isValid())
//		return 0;

//	return mList.size();
//}

//int Services::columnCount(const QModelIndex &index) const
//{
//	if (index.isValid())
//		return 0;

//	return mHeaders.size();
//}

QVariant Services::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		switch (role) {
		case Qt::DisplayRole:
			return mHeaders.at(section);
		}
		break;

	case Qt::Vertical: break;
	}
	return QVariant();
}

QVariant Services::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant{};

	int row = index.row();
	int col = index.column();

	const core::Service& service = mList[row];
	QSettings settings;
	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return service.name();
		case Number: return service.number();
		case ContractorName: {
			const core::Contractor& contractor = service.contractor();
			if (contractor.name().isEmpty()) {
				return QString("%1 %2").arg(contractor.personName(), contractor.personSurname());
			}
			return contractor.name();
		}
		case ContractorPhone:
			return service.contractor().contact().phone();

		case Priority: {
			QStringList priorities;
			priorities << tr("Niski") << tr("Normalny") << tr("Wysoki");
			return priorities[static_cast<int>(service.priority())];
			}

		case AddDate: return service.addDate().toString("dd.MM.yyyy");
		case EndDate: return service.endDate().toString("dd.MM.yyyy");
		}
		break; // Qt::DisplayRole

	case Roles::Id:
		return service.id();

	case Qt::ForegroundRole:
		// usługi nie zrealizowane i po terminie realizacji
		if (!service.isRealised()
			&& service.endDate() < QDate::currentDate()
			&& settings.value("Service/RemindAfter").toBool())
		{
			return QColor(settings.value("Service/deadlineColor").value<QColor>());
		}

		// usługi nie zrealizowane o zblizającym się terminie realizacji
		if (!service.isRealised()
			&& QDate::currentDate().addDays(settings.value("Service/daysBeforeWarning").toUInt()) >= service.endDate()
			&& settings.value("Service/RemindBefore").toBool())
		{
			return QColor(settings.value("Service/beforeWarningColor").value<QColor>());
		}

		// usługi zrealizowane
		if (service.isRealised()) {
			return QColor(settings.value("Orders/realisedColor").value<QColor>());
		}
		break;
	}
	return QVariant{};
}

void Services::createOrUpdate(core::Service service) {
	Q_UNUSED(service);
}

void Services::destroy(core::Service service) {
	Q_UNUSED(service);
}

void Services::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance()
			.repository()
			.serviceRepository()
			.getAll();
		endResetModel();
	} catch (const QString &msg) {
		::widgets::MessageBox::critical(0, tr("Błąd"),
			tr("Podczas pobierania usług z bazy danych wystąpiły błędy!"), msg);
	}
}

} // namespace models
} // namespace services
} // namespace eSawmill
