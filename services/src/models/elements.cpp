#include "models/elements.hpp"
#include <eloquent/builder.hpp>
#include <messagebox.h>
#include <dbo/auth.h>
#include <dbo/service.h>
#include <dbo/woodtype.hpp>

#include <QIODevice>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QTextStream>
#include <QApplication>
#include <QDate>
#include <QSettings>
#include <globaldata.hpp>


namespace eSawmill {
namespace services {
namespace models {

Elements::Elements(QObject* parent, core::eloquent::DBIDKey serviceId)
	: core::models::AbstractElementsModel<core::service::Element>(parent)
{
}

Qt::ItemFlags Elements::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);
	flags |= Qt::ItemIsEnabled;
	if (index.column() != Columns::Cubature)
		flags |= Qt::ItemIsEditable;

	return flags;
}

QVariant Elements::headerData(int section, Qt::Orientation orientation, int role) const
{
	switch (orientation) {
	case Qt::Horizontal:
		switch (role) {
		case Qt::DisplayRole:
			return headerData().at(section);
		}
		break;

	case Qt::Vertical:
		if (role == Qt::DisplayRole) return section + 1;
		break;
	}
	return QVariant();
}

QStringList Elements::headerData() const {
	return QStringList()
		<< "Rodzaj drewna"
		<< "Średnica"
		<< "Długość"
		<< "Kantowanie"
		<< "Kubatura";
}

QVariant Elements::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant{};

	int row = index.row();
	int col = index.column();
	const core::service::Element& element = mList[row];
	switch (role) {
	case Qt::DisplayRole: {
		switch (col) {
		case WoodType: return element.woodType().name();
		case Diameter: return element.diameter();
		case Length: return element.length();
		case Rotated: return element.isRotated() ? "Tak" : "Nie";
		case Cubature: return QString::asprintf("%0.2f", element.stere());
		}
	} break;

	case Qt::ForegroundRole:
		if (element.isRotated())
			return QBrush(QColor(Qt::darkGreen));

		if (col == WoodType || col == Cubature)
			return QBrush(QColor(Qt::darkGray));
		break;

	case Roles::Id: return element.id();
	case Roles::IsRotated: return element.isRotated();
	}

	return QVariant();
}

bool Elements::setData(const QModelIndex &index, const QVariant &value, int role) {
	int row = index.row();
	int col = index.column();

	core::service::Element& element = mList[row];
	switch (role) {
	case Qt::EditRole: {
		switch (col) {
		case Diameter: element.setDiameter(value.toDouble()); break;
		case Length: element.setLength(value.toDouble()); break;
		case Rotated: element.setRotated(value.toBool()); break;
		case WoodType: element.setWoodType(value.toInt()); break;
		}
	} break;

	case Qt::CheckStateRole:
		if (col == 0)
			mList[row].setChecked(value.toBool());
		break;
	}
	notifyDataChanged();
	return true;
}

void Elements::createOrUpdate(core::service::Element element) {
	Q_UNUSED(element);
}

void Elements::destroy(core::service::Element element) {
	Q_UNUSED(element);
}

void Elements::get()
{
	try {
		beginResetModel();
		endResetModel();
	}
	catch (const QString &msg) {
		::widgets::MessageBox::critical(0, tr("Bład"),
										tr("Podczas pobierania danych z bazy wystąiły błędy!"), msg);
	}
}

QDomDocument Elements::exportToXML() {
	return QDomDocument{};
//	QDomDocument doc;
//	QDomElement root = doc.createElement("eSawmill");
//	doc.appendChild(root);
//
//	QString commentText;
//	QTextStream out(&commentText);
//	out << "Generated by: " << QApplication::applicationName() << " ver. " << QApplication::applicationVersion() << '\n';
//		out << "By: " << core::GlobalData::instance().repository().userRepository().autorizeUser()["Name"].toString()
//			<< " date: " << QDate::currentDate().toString("dd/MM/yyyy");
//	QDomComment comment = doc.createComment(commentText);
//	root.appendChild(comment);
//
//	QDomElement service = doc.createElement("service");
//	root.appendChild(service);
//
//	for (int i = 0; i < mList.size(); i++) {
//		QDomElement item = doc.createElement("item");
//		item.setAttribute("diameter", mList[i].get("Diameter").toString().replace(',', '.'));
//		item.setAttribute("length", mList[i].get("Length").toString().replace(',', '.'));
//		item.setAttribute("rotated", mList[i].get("IsRotated").toString());
//
//		core::service::Element el = mList[i];
//		item.setAttribute("woodType",el.woodType()["Name"].toString());
//		service.appendChild(item);
//	}
//	return doc;
}

double Elements::totalCubature() const {
	double totalValue = 0;
	for (const core::service::Element& e : mList) {
		totalValue += e.stere();
	}
	return QString::number(totalValue, 'g', 3).toDouble();
}

double Elements::totalRotatedCoubature() {
	double totalValue = 0;
	for (const core::service::Element& e : mList) {
		if (e.isRotated())
			totalValue += e.stere();
	}
	return totalValue;
}

double Elements::totalPrice() {
	QSettings settings;
	double totalPrice {0};
	for (const core::service::Element& item : items()) {
		double price {0};
		const core::WoodType& wood = item.woodType();
		if (item.isRotated())
			price = wood.factor() * settings.value("Service/rotatedPrice", 1).toDouble();
		else price = wood.factor() * this->price();
		totalPrice += price * item.stere();
	}
	double Tax = (100.0f + this->Tax()) / 100.0f;
	double rebate = (100.0f - this->rebate()) / 100.0f;

	totalPrice *= Tax;
	totalPrice *= rebate;
	return totalPrice;
}

void Elements::notifyDataChanged() {
	Q_EMIT totalPriceChanged(totalPrice());
	Q_EMIT modelDataChanged();
}

} // namespace models
} // namespace services
} // namespace eSawmill
