#include "createservice.hpp"
#include "models/tax.h"
#include "commands/printservicecommand.hpp"
#include <dbo/auth.h>
#include <dbo/tax.h>
#include <documents/servicedocument.h>
#include <messagebox.h>
#include <licence.hpp>
#include <models/servicearticles.hpp>
#include <memory>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QTextDocument>
#include <QTextBlock>
#include <QSettings>
#include <QDebug>
#include <globaldata.hpp>
#include <commandexecutor.hpp>


namespace eSawmill {
namespace services {

CreateService::CreateService(core::Service& service, QWidget *parent)
	: QDialog {parent}
	, service_{service}
{
	createWidgets();
	createValidators();
	createModels();
	createConnections();

	QSettings settings;
	mPrice->setValue(settings.value("Service/defaultPrice").toDouble());
	//mNumber = "";
	mDescription->setPlaceholderText(tr("Dodatkowe notatki do zlecenia..."));

	QDate firstAvaiableDate = core::GlobalData::instance().repository().serviceRepository().getFirstAvaiableDeadline();
	mEndDate->setDate(firstAvaiableDate.addDays(settings.value("Service/minDaysToRealise", 3).toInt()));

	showServiceData();

	if (service_.id() > core::eloquent::NullDBIDKey)
	{
		setWindowTitle(tr("Edycja usługi [%1]").arg(service_.number()));
	}
	else
	{
		setWindowTitle(tr("Tworzenie nowej usługi"));
	}
}

void CreateService::accept()
{
	exchangeData();
	QDialog::accept();
}

void CreateService::createWidgets() {
	mTabWidget = new QTabWidget(this);
	mAcceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
	mPrintButton = new ::widgets::PushButton(tr("Drukuj"), QKeySequence("CRTL+P"), this, QIcon(":/icons/print"));
	mTotalPriceLabel = new QLabel(this);
	mServiceElementWidget = new widgets::ElementsWidget(service_, this);
	mArticleWidget = new eSawmill::articles::widgets::ArticleWidget(new eSawmill::service::model::Articles(service_.id(), this), this);

	mTotalPriceLabel->setStyleSheet("font-weight: bold; font-size: 16px;");

	mTabWidget->addTab(generalWidget(), tr("Ogólne"));
	mTabWidget->addTab(mServiceElementWidget, tr("Kło&dy"));
	mTabWidget->addTab(mArticleWidget, tr("A&rtykuły"));

	QMenu* printMenu = new QMenu(this);
	printMenu->addAction(tr("Drukuj"),
		[this] { printButton_clicked(false); }, QKeySequence("CTRL+P"));
	printMenu->addAction(tr("Podgląd wydruku"),
		[this] { printButton_clicked(true); }, QKeySequence("SHIFT+CTRL+P"));
	mPrintButton->setMenu(printMenu);
	mPrintButton->setPlaceholderText("Ctrl+P");

#ifndef DEBUG
	core::Licence licence;
	if (!licence.isArticleModuleActive())
		mArticleWidget->setEnabled(false);
#endif

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget(mTotalPriceLabel);
	buttonsLayout->addStretch(2);
	buttonsLayout->addWidget(mPrintButton);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mAcceptButton);
	buttonsLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTabWidget);
	mainLayout->addLayout(buttonsLayout);
}

void CreateService::createModels() {
	mTax->setModel(new eSawmill::articles::models::Tax(this));

	QStringList priorities;
	priorities << tr("Niski") << tr("Normalny") << tr("Wysoki");
	mPriority->addItems(priorities);
	mAddDate->setDate(QDate::currentDate());
	mEndDate->setDate(QDate::currentDate());
}

void CreateService::createConnections() {
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateService::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this,&CreateService::reject);
	connect(mRealised, &QCheckBox::clicked, this, &CreateService::completedStateChangedHandle);
	connect(mRealised, &QCheckBox::stateChanged, this, &CreateService::blockWidgetsForCompletedService);
	connect(mArticleWidget, &eSawmill::articles::widgets::ArticleWidget::articlesChanged,
		this, &CreateService::recalculate);

	connect(mServiceElementWidget, &widgets::ElementsWidget::totalPriceChanged,
			this, &CreateService::recalculate);

	/// zmiana wartości ceny
	connect(mPrice, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double val){
		mServiceElementWidget->setPrice(val);
		this->recalculate();
	});

	/// zmiana wielkości rabatu
	connect(mRebate, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [&](double val){
		mServiceElementWidget->setRebate(val);
		this->recalculate();
	});

	connect(mTax, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [&](int index){
		Q_UNUSED(index);
		mServiceElementWidget->setTax(core::Tax().findRecord(mTax->currentData(eSawmill::articles::models::Tax::Id).toUInt()).get("Value").toDouble());
		this->recalculate();
	});
}

void CreateService::createValidators() {
	QRegularExpressionValidator* nameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9- ]+"), this);
	mName->setValidator(nameValidator);
}

void CreateService::showServiceData() const noexcept
{
	if (service_.id() <= core::eloquent::NullDBIDKey)
		return;	// the service will be created

	mName->setText(service_.name());
	mContractorSelector->selectContractor(service_.contractor());
	mAddDate->setDate(service_.addDate());
	mEndDate->setDate(service_.endDate());
	mPriority->setCurrentIndex(static_cast<int>(service_.priority()));
	mPrice->setValue(service_.price());
	mRebate->setValue(service_.rebate());
	mRealised->setChecked(service_.isRealised());
	mTax->setCurrentText(QString::number(service_.Tax().value()));
	mDescription->setPlainText(service_.descripion());
}

void CreateService::exchangeData()
{
	service_.setName(mName->text());
	service_.setContractor(mContractorSelector->contractor());
	service_.setEndDate(mEndDate->date());
	if (!service_.addDate().isValid())
		service_.setAddDate(mAddDate->date());
	service_.setPriority(static_cast<core::Service::Priority>(mPriority->currentIndex()));
	service_.setPrice(mPrice->value());
	service_.setRebate(mRebate->value());
	service_.setRealised(mRealised->isChecked());
	service_.setDescription(mDescription->document()->toPlainText());
	service_.setElements(mServiceElementWidget->elements());
	service_.setArticles(mArticleWidget->items());

	auto const& tax = core::GlobalData::instance()
			.repository()
			.TaxRepository()
			.getByValue(mTax->currentText().toDouble());
	service_.setTax(tax);
}

QWidget* CreateService::generalWidget() {
	QWidget* parent = new QWidget(this);

	mName = new QLineEdit(parent);
	mContractorSelector = new eSawmill::contractors::widgets::ContractorSelector(parent);
	mAddDate = new QDateEdit(parent);
	mEndDate = new QDateEdit(parent);
	mPriority = new QComboBox(parent);
	mPrice = new QDoubleSpinBox(parent);
	mRebate = new QDoubleSpinBox(parent);
	mRealised = new QCheckBox(tr("Zrealizowane"), parent);
	mTax = new QComboBox(parent);
	mDescription = new QPlainTextEdit(parent);

	mPrice->setMaximum(10000);
	mRebate->setMaximum(100);
	mAddDate->setCalendarPopup(true);
	mEndDate->setCalendarPopup(true);

	mAddDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mEndDate->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPriority->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* leftLayout = new QFormLayout;
	leftLayout->addRow(tr("Nazwa"), mName);
	leftLayout->addRow(tr("Kontrahent"), mContractorSelector);
	leftLayout->addRow(tr("Data dodania"), mAddDate);
	leftLayout->addRow(tr("Termin realizacji"), mEndDate);
	leftLayout->addRow(tr("Priorytet"), mPriority);

	QLabel* priceLabel = new QLabel(tr("PLN"), this);
	priceLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QLabel* rebateLabel = new QLabel("%", this);
	rebateLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QLabel* TaxLabel = new QLabel("%", this);
	TaxLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* priceRowLayout = new QHBoxLayout;
	priceRowLayout->addWidget(mPrice);
	priceRowLayout->addWidget(priceLabel);

	QHBoxLayout* rebateRowLayout = new QHBoxLayout;
	rebateRowLayout->addWidget(mRebate);
	rebateRowLayout->addWidget(rebateLabel);

	QHBoxLayout* TaxRowLayout = new QHBoxLayout;
	TaxRowLayout->addWidget(mTax);
	TaxRowLayout->addWidget(TaxLabel);

	QFormLayout* rightLayout = new QFormLayout;
	rightLayout->addRow(tr("Cena"), priceRowLayout);
	rightLayout->addRow(tr("Rabat"), rebateRowLayout);
	rightLayout->addRow(tr("VAT"), TaxRowLayout);
	rightLayout->addWidget(mRealised);

	QHBoxLayout* topLayout = new QHBoxLayout;
	topLayout->addLayout(leftLayout);
	topLayout->addLayout(rightLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(parent);
	mainLayout->addLayout(topLayout);
	mainLayout->addWidget(mDescription);

	return parent;
}

void CreateService::printButton_clicked(bool preview) {
	exchangeData();

	auto command = std::make_unique<commands::PrintServiceCommand>(service_, this);
	command->setPreviewMode(preview);

	auto& commandExecutor = core::GlobalData::instance().commandExecutor();
	if (!commandExecutor.execute(std::move(command)))
	{
		::widgets::MessageBox::critical(this, tr("Bład"),
			tr("Podczas wykonywania polecenia drukowania wystąpiły błędy!"));
	}
}

void CreateService::recalculate()
{
	double total = mServiceElementWidget->totalPrice() + mArticleWidget->totalPrice();

	mTotalPriceLabel->setText(QString::asprintf("Wartość: %0.2f PLN", total));
	mTotalPriceLabel->setVisible(total > 0 ? true : false);
}

void CreateService::blockWidgetsForCompletedService(int state) {
	bool completed = state == Qt::Checked;
	mName->setReadOnly(completed);
	mContractorSelector->setReadOnly(completed);
	mEndDate->setReadOnly(completed);
	mPriority->setDisabled(completed);
	mPrice->setReadOnly(completed);
	mRebate->setReadOnly(completed);
	mTax->setDisabled(completed);
	mRealised->setDisabled(completed);
	mDescription->setReadOnly(completed);
}

void CreateService::completedStateChangedHandle(bool checked) {
	if (checked) {
		int result = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Czy na pewno chcesz oznaczyć zlecenie jako zrealizowane?\n"
			   "Operacja ta nie może być cofnięta."), tr(""),
			QMessageBox::Yes | QMessageBox::No);

		if (result == QMessageBox::No) {
			QCheckBox* completed = qobject_cast<QCheckBox*>(sender());
			completed->setChecked(false);
			completed->stateChanged(Qt::Unchecked);
		} else {
			blockWidgetsForCompletedService(Qt::Checked);
		}
	}
}

} // namespace services
} // namespace eSawmill
