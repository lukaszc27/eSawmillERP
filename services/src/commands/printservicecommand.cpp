#include "commands/printservicecommand.hpp"
#include <documents/servicedocument.h>
#include <messagebox.h>
#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <QPrinter>

namespace eSawmill::services::commands {

PrintServiceCommand::PrintServiceCommand(const core::Service &service, QWidget *parent)
	: core::abstracts::AbstractCommand{parent}
	, service_{service}
{
}

bool PrintServiceCommand::execute(core::repository::AbstractRepository& repository)
{
	auto const& authUser = repository.userRepository().autorizeUser();
	core::documents::ServiceDocument doc(authUser.company(), service_.contractor(), service_, parent());

	QString title = QObject::tr("USŁUGA");
	if (!service_.number().isEmpty())
		title += QObject::tr(" NR %1").arg(service_.number());

	doc.setDocumentTitle(title);
	doc.setDocumentSubtitle(service_.name());

	if (!doc.generate())
	{
		::widgets::MessageBox::warning(parent(), QObject::tr("Błąd"),
			QObject::tr("Nie można wygenerować dokumentu do druku.\n"
			   "Sprawdź wprowadzone dane dla drukowanej usługi i spróbuj ponownie"));
		return false;
	}

	QPrinter printer(QPrinter::PrinterMode::HighResolution);
	printer.setCreator("eSawmillERP");

	if (preview_)
	{
		QPrintPreviewDialog previewDialog(&printer, parent());
		QObject::connect(&previewDialog, &QPrintPreviewDialog::paintRequested,
			[&doc](QPrinter* printer) { doc.print(printer); });

		previewDialog.setWindowTitle(QObject::tr("Podgląd wydruku"));
		previewDialog.exec();
	}
	else
	{
		QPrintDialog printDialog(&printer, parent());
		QObject::connect(&printDialog, QOverload<QPrinter*>::of(&QPrintDialog::accepted),
			[&doc](QPrinter* printer){ doc.print(printer); });

		printDialog.exec();
	}

	return true;
}

bool PrintServiceCommand::undo(core::repository::AbstractRepository &)
{
	return false;
}

} // namespace eSawmill::services::commands
