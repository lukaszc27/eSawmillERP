#include "commands/updateservicecommand.hpp"

namespace eSawmill::services::commands {

UpdateServiceCommand::UpdateServiceCommand(core::Service const& service, QWidget* parent)
	: core::abstracts::AbstractCommand{parent}
	, service_{service}
{
}

bool UpdateServiceCommand::execute(core::repository::AbstractRepository& repository)
{
	Q_ASSERT(service_.id() > 0);

	auto const& authUser = repository.userRepository().autorizeUser();
	service_.setUser(authUser);
	service_.setCompany(authUser.company());

	previousService_ = repository.serviceRepository().get(service_.id());
	previousService_.makeDirty();

	return repository.serviceRepository().update(service_);
}

bool UpdateServiceCommand::undo(core::repository::AbstractRepository& repository)
{
	return repository.serviceRepository().update(previousService_);
}

bool UpdateServiceCommand::canUndo() const
{
	return true;
}

} // namespace eSawmill::services::commands
