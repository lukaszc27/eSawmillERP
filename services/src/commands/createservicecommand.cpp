#include "commands/createservicecommand.hpp"

namespace eSawmill::services::commands {

CreateServiceCommand::CreateServiceCommand(core::Service const& service, QWidget* parent)
	: core::abstracts::AbstractCommand{parent}
	, service_{service}
{
}

bool CreateServiceCommand::execute(core::repository::AbstractRepository& repository)
{
	Q_ASSERT(service_.id() == 0);

	auto const& user = repository.userRepository().autorizeUser();
	service_.setUser(user);
	service_.setCompany(user.company());

	bool const success = repository.serviceRepository().create(service_);
	if (success)
	{
		Q_ASSERT(service_.id() > 0);
		lastInsertedServiceId_ = service_.id();
	}
	return success;
}

bool CreateServiceCommand::undo(core::repository::AbstractRepository& repository)
{
	Q_ASSERT(lastInsertedServiceId_ > 0);
	return repository
			.serviceRepository()
			.destroy(lastInsertedServiceId_);
}

bool CreateServiceCommand::canUndo() const
{
	return true;
}

} // namespace eSawmill::services::commands
