#include "commands/destroyservicecommand.hpp"

namespace eSawmill::services::commands {

DestroyServiceCommand::DestroyServiceCommand(core::eloquent::DBIDKey serviceId, QWidget* parent)
	: core::abstracts::AbstractCommand{parent}
	, serviceId_{serviceId}
{
	Q_ASSERT(serviceId_ > 0);
}

bool DestroyServiceCommand::execute(core::repository::AbstractRepository& repository)
{
	auto& serviceRepository = repository.serviceRepository();
	service_ = serviceRepository.get(serviceId_);

	return serviceRepository.destroy(serviceId_);
}

bool DestroyServiceCommand::undo(core::repository::AbstractRepository& repository)
{
	return repository.serviceRepository().create(service_);
}

bool DestroyServiceCommand::canUndo() const
{
	return true;
}

} // namespace eSawmill::services::commands
