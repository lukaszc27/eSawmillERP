#include "exporters/elementsxmlexporter.hpp"
#include <dbo/serviceelement.h>
#include <dbo/woodtype.hpp>
#include <messagebox.h>
#include <eloquent/builder.hpp>
#include <QDomDocument>
#include <QDomElement>
#include <globaldata.hpp>


namespace eSawmill::services::exporters {

ElementsXmlExporter::ElementsXmlExporter()
	: core::abstracts::AbstractExporter<core::service::Element>{}
{
}

bool ElementsXmlExporter::exportData(QIODevice &device) {
	QDomDocument document;
	QDomElement rootNode = document.createElement(rootTag);
	rootNode.setAttribute("version", core::GlobalData::instance().currentApplicationVersion().toString());

	QDomElement servicesNode = document.createElement(servicesGroupTag);
	QDomElement serviceNode = document.createElement(serviceTag);

	serviceNode.appendChild(toDomNode(document));
	servicesNode.appendChild(serviceNode);
	rootNode.appendChild(servicesNode);
	document.appendChild(rootNode);

	return device.write(document.toByteArray()) > 0;
}

bool ElementsXmlExporter::importData(QIODevice &device) {
	if (QDomDocument doc; doc.setContent(device.readAll())) {
		QDomElement rootElement = doc.documentElement();
		QDomElement servicesElement = rootElement.firstChildElement(servicesGroupTag);
		QDomElement serviceElement = servicesElement.firstChildElement(serviceTag);

		fromDomNode(serviceElement.firstChildElement(woodGroupItemsTag));
		return true;
	}

	return false;
}

QDomElement ElementsXmlExporter::toDomNode(QDomDocument &document) {
	QDomElement itemsNode = document.createElement(woodGroupItemsTag);
	for (const auto& item : mData) {
		QDomElement node = document.createElement(woodItemTag);
		node.setAttribute(diameterAttr, item.diameter());
		node.setAttribute(lengthAttr, item.length());
		node.setAttribute(rotatedAttr, item.isRotated());
		node.setAttribute(woodTypeAttr, item.woodType().name());
		itemsNode.appendChild(node);
	}
	return itemsNode;
}

void ElementsXmlExporter::fromDomNode(const QDomElement &element) {
	QDomNodeList items = element.elementsByTagName(woodItemTag);
	const QList<core::WoodType>& woods = core::GlobalData::instance().repository().woodTypeRepository().getAll();
	for (int index {0}; index < items.size(); index++) {
		const QDomElement node = items.at(index).toElement();
		core::service::Element item;
		item.setDiameter(node.attribute(diameterAttr).toDouble());
		item.setLength(node.attribute(lengthAttr).toDouble());
		item.setRotated(node.attribute(rotatedAttr).toInt());

		const QString woodName = node.attribute(woodTypeAttr).toUpper();
		auto woodItr = std::find_if(woods.begin(), woods.end(), [&woodName](const core::WoodType& wood)->bool{
			return wood.name() == woodName;
		});
		if (woodItr != woods.end())
			item.setWoodType(*woodItr);

		mData.append(item);
	}
}

}
