#include "exporters/elementscsvexporter.hpp"
#include <QTextStream>
#include <dbo/woodtype.hpp>
#include <QIODevice>
#include <QIODevice>
#include <messagebox.h>
#include <globaldata.hpp>


namespace eSawmill::services::exporters {

ElementsCsvExporter::ElementsCsvExporter()
	: core::abstracts::AbstractExporter<core::service::Element> {}
{
	mSeparator = ';';
}

bool ElementsCsvExporter::exportData(QIODevice &device) {
	QTextStream out(&device);

	// diameter | lenght | rotated | woodType?
	for (const auto& item : mData) {
		out << item.diameter() << mSeparator
			<< item.length() << mSeparator
			<< item.isRotated() << mSeparator
			<< item.woodType().name() << '\n';
	}

	return true;
}

bool ElementsCsvExporter::importData(QIODevice &device) {
	const QList<core::WoodType>& woods = core::GlobalData::instance().repository().woodTypeRepository().getAll();
	while (!device.atEnd()) {
		QString line = device.readLine();
		if (line.trimmed().startsWith('#') || line.isEmpty() || line.length() == 0)
			continue;

		QStringList cols = line.split(mSeparator);

		// skip rows where user give more than theree parameters
		if (cols.count() != 4) // have to tak into consideration last new line char (\n)
			break;

		core::service::Element item;
		item.setDiameter(cols[0].toDouble());
		item.setLength(cols[1].toDouble());
		item.setRotated(cols[2].toInt());

		// find wood by name
		QString woodName = cols[3].trimmed().toUpper();
		auto woodItr = std::find_if(woods.begin(), woods.end(), [&woodName](const core::WoodType& item)->bool{
			return item.name().contains(woodName, Qt::CaseInsensitive);
		});
		if (woodItr != woods.end())
			item.setWoodType(*woodItr);

		mData.append(item);
	}
	return true;
}



} // namespace eSawmill::services::exporters
