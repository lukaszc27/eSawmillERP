#include "exporters/servicesxmlexporter.hpp"
#include "exporters/elementsxmlexporter.hpp"
#include "exceptions/invalidimportexception.hpp"
#include <exporters/contractorexporter.hpp>
#include <messagebox.h>
#include <memory>
#include <dbo/auth.h>
#include <dbo/user.h>
#include <dbo/tax.h>
#include <eloquent/builder.hpp>
#include <contractormanager.h>
#include <dbo/serviceelement.h>
#include <QSettings>
#include <globaldata.hpp>

namespace eSawmill::services::exporters {

ServicesXmlExporter::ServicesXmlExporter()
	: core::abstracts::AbstractExporter<core::Service>{}
{
}

bool ServicesXmlExporter::exportData(QIODevice& device) {
	QDomDocument doc;
	QDomElement rootNode = doc.createElement("eSawmill");
	rootNode.setAttribute("version", core::GlobalData::instance().currentApplicationVersion().toString());
	rootNode.appendChild(toDomNode(doc));
	doc.appendChild(rootNode);

	return device.write(doc.toByteArray()) > 0;
}

bool ServicesXmlExporter::importData(QIODevice& device) {
	QDomDocument doc;
	QString errorMsg;

	if (!doc.setContent(device.readAll(), &errorMsg)) {
		throw core::exceptions::InvalidImportException();
		return false;
	}

	QDomElement rootElement = doc.documentElement();
	QDomElement servicesNode = rootElement.firstChildElement(servicesTag);
	QDomNodeList services = servicesNode.elementsByTagName(serviceTag);

	int serviceCounter {0};
	for (int i {0}; i < services.size(); i++) {
		fromDomNode(services.at(i).toElement());
		++serviceCounter;
	}

	return serviceCounter > 0;
}

QDomElement ServicesXmlExporter::toDomNode(QDomDocument &doc) {
	QDomElement servicesNode = doc.createElement(servicesTag);
	for (auto& val : mData) {
		core::Service service {val};
		servicesNode.appendChild(exportService(doc, service));
	}
	return servicesNode;
}

void ServicesXmlExporter::fromDomNode(const QDomElement &element) {
	QSettings settings;
	core::Service service;
	service.setTax(core::GlobalData::instance().repository().TaxRepository().getByValue(23));
	service.setName(element.firstChildElement(nameTag).text().toUpper());
	service.setEndDate(QDate::fromString(element.firstChildElement(endDateTag).text(), "dd/MM/yyyy"));
	service.setDescription(element.firstChildElement(descriptionTag).text());
	service.setAddDate(QDate::currentDate());
	service.setPrice(settings.value("Service/defaultPrice").toDouble());

	int contractorId = eSawmill::contractors::ContractorManager::createFrom(element.firstChildElement("contractor"));
	if (contractorId <= 0) {
		throw core::exceptions::InvalidImportException();
	}
	service.setContractor(core::GlobalData::instance().repository().contractorRepository().get(contractorId));

	core::eloquent::DBIDKey lastServiceId = service.lastInsertedId();
	std::unique_ptr<eSawmill::services::exporters::ElementsXmlExporter> importer =
			std::make_unique<eSawmill::services::exporters::ElementsXmlExporter>();

	importer->fromDomNode(element.firstChildElement("items"));
	for (auto& item : importer->data()) {
		core::service::Element element = item;
		element.set("ServiceId", lastServiceId);
	}
//	QList<core::Article> articles {};
	core::GlobalData::instance().repository().serviceRepository().create(service/*, importer->data(), articles*/);
}

QDomElement ServicesXmlExporter::exportService(QDomDocument &doc, core::Service service) {
	QDomElement root = doc.createElement(serviceTag);
	QDomElement nameNode = doc.createElement(nameTag);
	QDomElement endDateNode = doc.createElement(endDateTag);
	QDomElement descriptionNode = doc.createElement(descriptionTag);

	nameNode.appendChild(doc.createTextNode(service.name()));
	endDateNode.appendChild(doc.createTextNode(service.endDate().toString("dd/MM/yyyy")));
	descriptionNode.appendChild(doc.createTextNode(service.descripion()));

	std::unique_ptr<eSawmill::contractors::exporters::ContractorExporter> contractorExporter =
			std::make_unique<eSawmill::contractors::exporters::ContractorExporter>();

	contractorExporter->setData(service.contractor());
	root.appendChild(contractorExporter->toDomNode(doc));

	std::unique_ptr<eSawmill::services::exporters::ElementsXmlExporter> elementsExporter =
			std::make_unique<eSawmill::services::exporters::ElementsXmlExporter>();

	const QList<core::service::Element>& elements = core::GlobalData::instance().repository().serviceRepository().getServiceElements(service.id());
	elementsExporter->setData(elements);
	root.appendChild(elementsExporter->toDomNode(doc));

	root.appendChild(nameNode);
	root.appendChild(endDateNode);
	root.appendChild(descriptionNode);

	return root;
}

}
