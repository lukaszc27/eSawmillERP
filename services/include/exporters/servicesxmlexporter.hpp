#ifndef SERVICESXMLEXPORTER_HPP
#define SERVICESXMLEXPORTER_HPP

#include "../services_global.hpp"
#include <abstracts/abstractexporter.hpp>
#include <dbo/service.h>


namespace eSawmill::services::exporters {

///
/// \brief The ServicesXmlExporter class
/// export full data about service into abstract IODevice
/// it can be store in local file or sent by socket to other machine
///
class ServicesXmlExporter : public core::abstracts::AbstractExporter<core::Service> {
public:
	explicit ServicesXmlExporter();
	virtual ~ServicesXmlExporter() = default;

	bool exportData(QIODevice& device) override;
	bool importData(QIODevice& device) override;

	QDomElement toDomNode(QDomDocument& doc) override;
	void fromDomNode(const QDomElement& element) override;

private:
	QDomElement exportService(QDomDocument& doc, core::Service service);

	const QString serviceTag		= "service";
	const QString servicesTag		= "services";
	const QString nameTag			= "name";
	const QString endDateTag		= "endDate";
	const QString descriptionTag	= "description";
};

} // namespace eSawmill::services::exporters

#endif // SERVICESXMLEXPORTER_HPP
