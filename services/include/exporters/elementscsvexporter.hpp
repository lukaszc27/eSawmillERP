#ifndef ELEMENTSCSVEXPORTER_H
#define ELEMENTSCSVEXPORTER_H

#include <abstracts/abstractexporter.hpp>
#include "../services_global.hpp"
#include <dbo/serviceelement.h>


namespace eSawmill::services::exporters {

///
/// \brief The ElementsCsvExporter class
/// export service element to abstract IODevice
/// in XML foramt, use to store elements in file
///
class SERVICES_EXPORT ElementsCsvExporter : public core::abstracts::AbstractExporter<core::service::Element> {
public:
	///@{
	/// ctors
	explicit ElementsCsvExporter();
	virtual ~ElementsCsvExporter() = default;
	///@}

	///
	/// \brief exportData
	/// export elements to abstract IODevice
	/// \param device
	/// \return
	///
	bool exportData(QIODevice& device) override;

	///
	/// \brief importData
	/// import elements from IODevice
	/// \param device
	/// \return
	///
	bool importData(QIODevice& device) override;

private:
	char mSeparator;
};

} // namespace eSawmill::services::exporters

#endif // ELEMENTSCSVEXPORTER_H
