#ifndef ELEMENTSXMLEXPORTER_HPP
#define ELEMENTSXMLEXPORTER_HPP

#include "../services_global.hpp"
#include <abstracts/abstractexporter.hpp>
#include <dbo/serviceelement.h>


namespace eSawmill::services::exporters {

///
/// \brief The ElementsXmlExporter class
/// export elements to XML format to abstract IODevice
/// uses to export items to file
///
class SERVICES_EXPORT ElementsXmlExporter : public core::abstracts::AbstractExporter<core::service::Element> {
public:
	///@{
	/// ctors
	explicit ElementsXmlExporter();
	virtual ~ElementsXmlExporter() = default;
	///@}

    bool exportData(QIODevice& device) override;
    bool importData(QIODevice& device) override;

    QDomElement toDomNode(QDomDocument& document) override;
	void fromDomNode(const QDomElement& element) override;

private:
	const QString rootTag			= "eSawmill";
	const QString servicesGroupTag	= "services";
	const QString serviceTag		= "service";
	const QString woodItemTag		= "item";
	const QString woodGroupItemsTag	= "items";
	const QString diameterAttr		= "diameter";
	const QString lengthAttr		= "length";
	const QString rotatedAttr		= "rotated";
	const QString woodTypeAttr		= "wood";
};

} // namespace eSawmill::services::exporters

#endif // ELEMENTSXMLEXPORTER_HPP
