#ifndef SERVICES_GLOBAL_HPP
#define SERVICES_GLOBAL_HPP

#include <QtCore/qglobal.h>

#if defined(SERVICES_LIBRARY)
#  define SERVICES_EXPORT Q_DECL_EXPORT
#else
#  define SERVICES_EXPORT Q_DECL_IMPORT
#endif

#endif // SERVICES_GLOBAL_HPP
