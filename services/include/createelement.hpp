#ifndef CREATEELEMENT_HPP
#define CREATEELEMENT_HPP

#include "services_global.hpp"
#include <dbo/serviceelement.h>

#include <QDialog>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>

#include <pushbutton.h>


namespace eSawmill {
namespace services {

/**
 * @brief The CreateElement class
 * dialog tworzenia elementu usługi (kłody)
 */
class SERVICES_EXPORT CreateElement : public QDialog {
	Q_OBJECT

public:
	CreateElement(QWidget* parent = nullptr);

	/**
	 * @brief element - zwraca utworzony element przy pomocy dialogu
	 * @return
	 */
	core::service::Element element();

private:
	void createWidgets();
	void createConnections();

	QDoubleSpinBox* mDiameter;
	QDoubleSpinBox* mLength;
	QCheckBox* mRotated;
	QComboBox* mWoodType;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
};

} // namespace services
} // namespace eSawmill

#endif // CREATEELEMENT_HPP
