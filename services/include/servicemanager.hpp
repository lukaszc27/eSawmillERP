#ifndef SERVICEMANAGER_HPP
#define SERVICEMANAGER_HPP

#include "services_global.hpp"

#include <QDialog>
#include <QListView>
#include <QComboBox>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>

#include <tableview.h>
#include <pushbutton.h>
#include <widgets/recordfilterwidget.hpp>	/// widgets from orders module
#include <widgets/datefilterwidget.hpp>		/// widgets from orders module
#include "models/services.hpp"
#include "filters/servicerolefilter.hpp"
#include "filters/servicesfilter.hpp"
#include <filters/orderdaterangefilter.hpp>	/// filter date range from orders module
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>
#include <commandexecutor.hpp>

namespace agent = core::providers::agent;

namespace eSawmill {
namespace services {

/**
 * @brief The ServiceManager class
 * zarządzanie usługami (obróbka drewna dostarczonego do tartaku)
 */
class SERVICES_EXPORT ServiceManager
		: public QDialog
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	ServiceManager(QWidget* parent = nullptr);

public Q_SLOTS:
	void handleRepositoryNotification(const agent::RepositoryEvent& event);

private:
	void createWidgets();
	void createModels();
	void createConnections();

	QListView* mServiceOption;
	::widgets::TableView* mServicesView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemakeButton;
	::widgets::PushButton* mRemoveButton;
	::widgets::PushButton* mCloseButton;

	eSawmill::orders::widgets::RecordFilterWidget* mRecordFilterWidget;
	eSawmill::orders::widgets::DateFilterWidget* mDateFilterWidget;

	/// modele
	models::Services* mServices;
	filters::ServiceRoleFilter* mServiceRoleFilter;
	filters::ServicesFilter* mServicesFilter;
	eSawmill::orders::filters::OrderDateRangeFilter* mDateRangeFilter;

private:
	// CommandExecutor listener interface implementation
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;

private Q_SLOTS:
	void addService_clicked();
	void editService_clicked();
	void removeServiceHandle();
	void recordFilterHandle(const struct eSawmill::orders::widgets::RecordFilterWidget::FilterOptions &options);
	void recordFilterByDateHandle(const struct eSawmill::orders::widgets::DateFilterWidget::DateFilterOptions & options);
	///
	/// \brief exportActionHandle
	/// action to export existing service to file
	///
	void exportActionHandle();
	void importActionHandle();

	///
	/// \brief importServicesByDroppingFile
	/// import new services from file by dropping on services list
	/// \param files - path to dropped files
	///
	void importServicesByDroppingFile(const QStringList& files);
};

} // namespace services
} // namespace eSawmill

#endif // SERVICEMANAGER_HPP
