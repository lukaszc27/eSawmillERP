#ifndef CREATESERVICE_HPP
#define CREATESERVICE_HPP

#include "services_global.hpp"
#include <QDialog>
#include <QTabWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QDateEdit>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QPlainTextEdit>
#include <QLabel>

#include <pushbutton.h>
#include <widgets/contractorselector.h>
#include <dbo/service.h>

#include "widgets/elementswidget.hpp"
#include "widgets/articlewidget.h"

namespace eSawmill {
namespace services {

///
/// \brief The CreateService class
/// dialog to create new service
/// has tabs "Elements" from this module
/// and Articles from articles module (articlewidget.h)
///
class SERVICES_EXPORT CreateService : public QDialog {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit CreateService(core::Service& service, QWidget* parent = nullptr);
	virtual ~CreateService() = default;
	///@}

	virtual void accept() override;

private:
	void createWidgets();
	void createModels();
	void createConnections();
	void createValidators();
	void showServiceData() const noexcept;

	QWidget* generalWidget();
	void exchangeData();

private:
	core::Service& service_;

private:
	//// kontrolki
	QTabWidget* mTabWidget;
	QLabel* mTotalPriceLabel;
	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
	::widgets::PushButton* mPrintButton;

	/// główny widget
	QLineEdit* mName;
	eSawmill::contractors::widgets::ContractorSelector* mContractorSelector;
	QDateEdit* mAddDate;
	QDateEdit* mEndDate;
	QComboBox* mPriority;
	QDoubleSpinBox* mPrice;
	QDoubleSpinBox* mRebate;
	QCheckBox* mRealised;
	QComboBox* mTax;
	QPlainTextEdit* mDescription;

	//// widgety
	widgets::ElementsWidget* mServiceElementWidget;
	eSawmill::articles::widgets::ArticleWidget* mArticleWidget;

private Q_SLOTS:
	/**
	 * @brief printButton_clicked
	 * przygotowanie dokumentu do wydruku oraz jego wydruk
	 */
	void printButton_clicked(bool preview);

	/**
	 * @brief recalculate - ponowne obliczenie wartości zamówienia po zmianie danych
	 */
	void recalculate();

	///
	/// \brief blockWidgetsForCompletedService
	/// block fileds when service is completed
	/// so that users can't entered data in some fields
	///
	void blockWidgetsForCompletedService(int state);

	///
	/// \brief completedStateChangedHandle
	/// connected with clicked on QCheckBox completed
	/// ask user before block fields
	/// \param state
	///
	void completedStateChangedHandle(bool checked);
};

} // namespace services
} // namespace eSawmill

#endif // CREATESERVICE_HPP
