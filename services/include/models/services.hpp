#ifndef SERVICES_HPP
#define SERVICES_HPP

#include "../services_global.hpp"

#include <abstracts/abstractmodel.h>
#include <dbo/service.h>


namespace eSawmill {
namespace services {
namespace models {

class SERVICES_EXPORT Services : public core::models::AbstractModel<core::Service> {
	Q_OBJECT

public:
	explicit Services(QObject* parent = nullptr);
	virtual ~Services() = default;

	Qt::ItemFlags flags(const QModelIndex &index) const override;

//	int rowCount(const QModelIndex &index) const override;
//	int columnCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override { return mHeaders; }
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	QStringList headers() const { return mHeaders; }

	/**
	 * @brief The Columns enum
	 * indeksy kolumn użytwanych w modelu
	 */
	enum Columns {
		Name,
		Number,
		ContractorName,
		ContractorPhone,
		Priority,
		AddDate,
		EndDate
	};

	/**
	 * @brief The Roles enum
	 * dodatkowe inforamcje zapisane w modelu
	 */
	enum Roles {
		Id = Qt::UserRole + 1
	};

	void createOrUpdate(core::Service service) override;
	void destroy(core::Service service) override;
	void get() override;

private:
	QStringList mHeaders;
};

} // namespace models
} // namespace services
} // namespace eSawmill

#endif // SERVICES_HPP
