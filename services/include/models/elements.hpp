﻿#ifndef ELEMENTS_HPP
#define ELEMENTS_HPP

#include "../services_global.hpp"
//#include <abstracts/abstractmodel.h>
#include <dbo/serviceelement.h>
#include <abstracts/abstractelementsmodel.hpp>
#include <QStringList>
#include <QDomDocument>


namespace eSawmill {
namespace services {
namespace models {

class SERVICES_EXPORT Elements : public core::models::AbstractElementsModel<core::service::Element> {
	Q_OBJECT

public:
	explicit Elements(QObject* parent = nullptr, core::eloquent::DBIDKey serviceId = 0);
	virtual ~Elements() = default;

	Qt::ItemFlags flags(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

	///
	/// \brief The Columns enum
	///
	enum Columns {
		WoodType,
		Diameter,
		Length,
		Rotated,
		Cubature
	};

	///
	/// \brief The Roles enum
	/// additional roles uses by model
	///
	enum Roles {
		Id = Qt::UserRole + 1,
		IsRotated
	};

	///@{
	/// DEPRECATED METHOD
	/// IN NEXT CODE REFACTORING SCHOULD BE REMOVED
	void createOrUpdate(core::service::Element element) override;
	void destroy(core::service::Element element) override;
	void get() override;
	///@}

	///
	/// this method is implement because in AbstractElementModel is mark as abstract virtual method
	/// now is unused because import and export is operated by exporter classess
	bool importFromXML(const QDomDocument* doc) override {
		Q_UNUSED(doc);
		return false;
	}
	///
	/// the same situation as up
	QDomElement toDomElement() const override {
		return QDomElement {};
	}

	void fromDomElement(QDomElement& element) override {
		Q_UNUSED(element);
	}

	///
	/// \brief exportToXML
	/// \deprecated
	/// not used now to remove in next code refactoring
	/// \return
	///
	QDomDocument exportToXML() override;

	///
	/// \brief totalCubature
	/// return total cubature for all items in model
	/// \return
	///
	double totalCubature() const override;

	///
	/// \brief totalRotatedCoubature
	/// return cubature for only rotated items
	/// \return
	///
	double totalRotatedCoubature();

	///
	/// \brief totalPrice
	/// return sum of all items price
	/// \return
	///
	double totalPrice() override;

protected:
	///
	/// \brief notifyDataChanged
	/// running every time when user change data in model
	///
	void notifyDataChanged() override;

Q_SIGNALS:
	///
	/// \brief modeldataChanged
	/// emitted by notifyDataChanged
	///
	void modelDataChanged();

	///
	/// \brief totalPriceChanged
	/// emitted when total price will be changed
	///
	void totalPriceChanged(double);
};

} // namespace models
} // namespace services
} // namespace eSawmill

#endif // ELEMENTS_HPP
