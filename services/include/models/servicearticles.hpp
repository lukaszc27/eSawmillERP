#ifndef SERVICEARTICLES_HPP
#define SERVICEARTICLES_HPP

#include "../services_global.hpp"
#include <models/orderarticles.hpp>


namespace eSawmill {
namespace service {
namespace model {

class SERVICES_EXPORT Articles : public eSawmill::orders::models::Articles {
	Q_OBJECT

public:
	explicit Articles(QObject* parent = nullptr);
	explicit Articles(int id, QObject* parent);
	virtual ~Articles() = default;

	/**
	 * @brief data - prezentuje dane z modelu użytkownikowi
	 * @param index - komórka w której zostają wyświetlone określone dane
	 * @param role - rodzaj prezentacji
	 * @return QVariant
	 */
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	/**
	 * @brief get - pobiera dane z bazy danych
	 * oraz zapisuje je w liście tymczasowej modelu
	 */
	void get() override;

	///
	/// \brief totalPrice
	/// override function from AbstractModel to calculate total price for all elements in model
	/// \return
	///
	double totalPrice() override;

protected:
	///
	/// \brief notifyDataChanged
	/// execute every time when data in model will be chagned
	///
	void notifyDataChanged() override;
};

} // namespace model
} // namespace service
} // namespace eSawmill

#endif // SERVICEARTICLES_HPP
