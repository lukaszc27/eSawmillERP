#ifndef CONFIGSERVICES_HPP
#define CONFIGSERVICES_HPP

#include "services_global.hpp"
#include <QWidget>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>

#include <configurationwidget.h>
#include <colorselector.hpp>


namespace eSawmill {
namespace config {
namespace widgets {

/**
 * @brief The Services class
 * Widget przedstawiany w dialogu konfiguracyjnym aplikacji
 * umożliwia spersianalizowanie wyglądu do własnych potrzeb
 */
class SERVICES_EXPORT Services : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	Services(QWidget* parent = nullptr);

	void save() override;
	void initialize() override;

private:
	void createAllWidgets();

	QCheckBox* mUsedRemoveBark;
	QCheckBox* mRemindCheck;						// oznakowanie usług przed terminem
	QCheckBox* mDeadlineCheck;						// oznakowanie usług po terminie
	QCheckBox* mPriorityColor;
	QCheckBox* mFastInsert;
	QSpinBox* mDaysToRemind;						// ilość dni wstecz do ostrzeżenia
	QLineEdit* mPrefix;
	::widgets::ColorSelector* mBeforeColorSelector;
	::widgets::ColorSelector* mDeadlineColorSelector;
};

} // namespace widgets
} // namespace config
} // namespace eSawmill

#endif // CONFIGSERVICES_HPP
