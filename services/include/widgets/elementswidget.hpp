#ifndef ELEMENTSWIDGET_HPP
#define ELEMENTSWIDGET_HPP

#include "../services_global.hpp"
#include "../models/elements.hpp"

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <tableview.h>
#include <pushbutton.h>
#include <dbo/service.h>


namespace eSawmill {
namespace services {
namespace widgets {

class SERVICES_EXPORT ElementsWidget : public QWidget {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit ElementsWidget(core::Service& service, QWidget* parent = nullptr);
	virtual ~ElementsWidget() = default;
	///@}

	/**
	 * @brief items - zwraca listę elementów wprowadzonych do widgetu edycji
	 * @return
	 */
	inline QList<core::service::Element> elements() { return mElements->items(); }

	/**
	 * @brief insert - uzupełnia listę z elementami nową listą podaną w agrumęcie
	 * @param elements
	 */
	inline void insert(QList<core::service::Element> elements) { mElements->insert(elements); }


	inline double price() const { return mElements->price(); }
	inline double rebate() const { return mElements->rebate(); }
	inline double Tax() const { return mElements->Tax(); }
	inline double totalPrice() const { return mElements->totalPrice(); }

	inline void setPrice(double price) { mElements->setPrice(price); }
	inline void setRebate(double rebate) { mElements->setRebate(rebate); }
	inline void setTax(double Tax) { mElements->setTax(Tax); }

private:
	void createWidgets();
	void createModels();
	void createConnections();

	::widgets::TableView* mElementsView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;
	QLabel* mTotalCubatureView;
	QCheckBox* mShowWoodTypeColumn;
	QCheckBox* mShowRotatedColumn;

	//// modele
	eSawmill::services::models::Elements* mElements;

private:
	core::Service& service_;

private Q_SLOTS:
	void addButton_clicked();
	void removeButton_clicked();
	void recalculate();

	/**
	 * @brief importAction_triggered
	 * importuje z pliku XML dane i ładuje je do modelu
	 */
	void importAction_triggered();

	/**
	 * @brief exportAction_triggered
	 * eksportuje dane z modelu do postaci XML
	 * gotowych do zapisania w pliku
	 */
	void exportAction_triggered();

	///
	/// \brief markElementsAsRotated
	/// mark selected elements as rotated wood
	///
	void markElementsAsRotated();
	void markElementsAsNoRotated();

    ///
    /// \brief importDataFromDragFile
    /// \param files
    ///
    void importDataFromDropedFile(const QStringList& files);

signals:
	void totalPriceChanged(double);
};

} // namespace widgets
} // namespace services
} // namespace eSawmill

#endif // ELEMENTSWIDGET_HPP
