#ifndef CREATESERVICECOMMAND_HPP
#define CREATESERVICECOMMAND_HPP

#include <abstracts/abstractcommand.hpp>
#include "services_global.hpp"

namespace eSawmill::services::commands {

class CreateServiceCommand final : public core::abstracts::AbstractCommand
{
public:
	explicit CreateServiceCommand(core::Service const& service, QWidget* parent = nullptr);

public:
	// implementation of AbstractCommand interface
	virtual bool execute(core::repository::AbstractRepository&) override;
	virtual bool undo(core::repository::AbstractRepository&) override;
	virtual bool canUndo() const override;

private:
	core::Service service_;
	core::eloquent::DBIDKey lastInsertedServiceId_{};
};

} // namespace eSawmill::services::commands

#endif // CREATESERVICECOMMAND_HPP
