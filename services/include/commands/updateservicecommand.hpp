#ifndef UPDATESERVICECOMMAND_HPP
#define UPDATESERVICECOMMAND_HPP

#include "abstracts/abstractcommand.hpp"
#include "services_global.hpp"

namespace eSawmill::services::commands {

class UpdateServiceCommand final : public core::abstracts::AbstractCommand
{
public:
	explicit UpdateServiceCommand(core::Service const& service, QWidget* parent = nullptr);

public:
	// implementation of AbstractCommand interface
	virtual bool execute(core::repository::AbstractRepository&) override;
	virtual bool canUndo() const override;
	virtual bool undo(core::repository::AbstractRepository&) override;

private:
	core::Service previousService_{};
	core::Service service_{};
};

} // namespace eSawmill::services::commands

#endif // UPDATESERVICECOMMAND_HPP
