#ifndef DESTROYSERVICECOMAND_HPP
#define DESTROYSERVICECOMAND_HPP

#include <abstracts/abstractcommand.hpp>
#include "services_global.hpp"

namespace eSawmill::services::commands {

class DestroyServiceCommand final : public core::abstracts::AbstractCommand
{
public:
	explicit DestroyServiceCommand(core::eloquent::DBIDKey serviceId, QWidget* parent = nullptr);

public:
	// implementation of AbstractCommand interface
	virtual bool execute(core::repository::AbstractRepository&) override;
	virtual bool undo(core::repository::AbstractRepository&) override;
	virtual bool canUndo() const override;

private:
	core::eloquent::DBIDKey serviceId_{};
	core::Service service_{};
};

} // namespace eSawmill::services::commands

#endif // DESTROYSERVICECOMAND_HPP
