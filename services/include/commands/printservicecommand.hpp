#ifndef PRINTSERVICECOMMAND_HPP
#define PRINTSERVICECOMMAND_HPP

#include <abstracts/abstractcommand.hpp>
#include <repository/abstractrepository.hpp>

namespace eSawmill::services::commands {

class PrintServiceCommand final : public core::abstracts::AbstractCommand
{
public:
	explicit PrintServiceCommand(core::Service const& service, QWidget* parent = nullptr);

	void setPreviewMode(bool preview) noexcept
	{
		preview_ = preview;
	}

public:
	// implementation of AbstractCommand interface
	virtual bool execute(core::repository::AbstractRepository&) override;
	virtual bool undo(core::repository::AbstractRepository&) override;

	virtual bool canUndo() const override
	{
		return false;
	}

	virtual bool overrideCursor() const override
	{
		return false;
	}

private:
	core::Service service_{};
	bool preview_{};
};

} // namespace eSawmill::services::commands

#endif // PRINTSERVICECOMMAND_HPP
