#ifndef ELEMENTS_DELEGATE_HPP
#define ELEMENTS_DELEGATE_HPP

#include "../services_global.hpp"
#include <QStyledItemDelegate>

namespace eSawmill {
namespace services {
namespace delegates {

class SERVICES_EXPORT ElementsDelegate : public QStyledItemDelegate {
    Q_OBJECT

public:
    ElementsDelegate(QObject* parent);

    QWidget * createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
};

} // namespace delegates
} // namespace services
} // eSawmill

#endif // ELEMENTS_DELEGATE_HPP
