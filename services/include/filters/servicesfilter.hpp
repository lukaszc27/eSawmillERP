#ifndef SERVICESFILTER_H
#define SERVICESFILTER_H

#include "../services_global.hpp"
#include <QSortFilterProxyModel>


namespace eSawmill::services::filters {

class SERVICES_EXPORT ServicesFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	ServicesFilter(QObject* parent = nullptr);
	virtual ~ServicesFilter() {}

	// setters
	void setName(const QString &name) {
		mName = name;
		invalidateFilter();
	}

	void setNumber(const QString &number) {
		mNumber = number;
		invalidateFilter();
	}

	void setPriority(const QString &priority) {
		mPriority = priority;
		invalidateFilter();
	}

	void setEnableFilterByPriority(bool enable) {
		mEnableFilterByPriority = enable;
		invalidateFilter();
	}

	void setContractorName(const QString &name) {
		mContractorName = name;
		invalidateFilter();
	}

	void setContractorPhone(const QString &phone) {
		mContractorPhone = phone;
		invalidateFilter();
	}

	void setActiveFilter(bool active) {
		mActiveFilter = active;
		invalidateFilter();
	}

public:
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
	bool filterByName(const QString &value) const;
	bool filterByNumber(const QString &number) const;
	bool filterByPriority(const QString &priority) const;
	bool filterByContractorName(const QString &contractor) const;
	bool filterByPhone(const QString &phone) const;

private:
	QString mName;
	QString mNumber;
	QString mPriority;
	QString mContractorName;
	QString mContractorPhone;
	bool mEnableFilterByPriority;
	bool mActiveFilter;
};

} // namespace

#endif // SERVICESFILTER_H
