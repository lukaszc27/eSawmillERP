#ifndef SERVICEROLEFILTER_H
#define SERVICEROLEFILTER_H

#include "../services_global.hpp"
#include <QSortFilterProxyModel>


namespace eSawmill {
namespace services {
namespace filters {

class SERVICES_EXPORT ServiceRoleFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	ServiceRoleFilter(QObject* parent = nullptr);

	inline void filterByIndex(int index) {
		mIndex = index;
		invalidateFilter();
	}

	/**
	 * @brief The FilterIndexes enum
	 * indeksy kolumn według których odbywa się filtrowanie zamówień
	 */
	enum FilterIndexes {
		All = 0,		// wszystkie
		ToImplemented,	// do realizacji
		AfterDeadline,	// po terminie realizacji
		Realised		// zrealizowane
	};

private:
	int mIndex;	// numer opcji do zastosowania filtra

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

} // namespace filters
} // namespace services
} // namespace eSawmill

#endif // SERVICEROLEFILTER_H
