add_library(services-lib)
target_sources(services-lib PRIVATE
    "include/createelement.hpp"
    "include/createservice.hpp"
    "include/servicemanager.hpp"
    "include/services_global.hpp"
    "src/createservice.cpp"
    "src/servicemanager.cpp"
    "src/createelement.cpp"

    "include/delegates/elementsdelegate.hpp"
    "src/delegates/elementsdelegate.cpp"

    "include/exporters/elementscsvexporter.hpp"
    "include/exporters/servicesxmlexporter.hpp"
    "include/exporters/elementsxmlexporter.hpp"
    "src/exporters/elementsxmlexporter.cpp"
    "src/exporters/servicesxmlexporter.cpp"
    "src/exporters/elementscsvexporter.cpp"

    "include/filters/servicesfilter.hpp"
    "include/filters/servicerolefilter.hpp"
    "src/filters/servicesfilter.cpp"
    "src/filters/servicerolefilter.cpp"

    "include/models/servicearticles.hpp"
    "include/models/elements.hpp"
    "include/models/services.hpp"
    "src/models/elements.cpp"
    "src/models/services.cpp"
    "src/models/servicearticles.cpp"

    "include/widgets/elementswidget.hpp"
    "include/widgets/servicesconfig.hpp"
    "src/widgets/servicesconfig.cpp"
    "src/widgets/elementswidget.cpp"

    "include/commands/updateservicecommand.hpp"
    "src/commands/updateservicecommand.cpp"
    "include/commands/createservicecommand.hpp"
    "src/commands/createservicecommand.cpp"
    "include/commands/destroyservicecommand.hpp"
    "src/commands/destroyservicecommand.cpp"
    "include/commands/printservicecommand.hpp"
    "src/commands/printservicecommand.cpp"
)
target_compile_definitions(services-lib PRIVATE SERVICES_LIBRARY)
target_include_directories(services-lib
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
)
target_link_libraries(services-lib
    Qt::Widgets
    Qt::Sql
    Qt::Xml
    Qt::Network
    
    core-lib
    widgets-lib
    contractors-lib
    articles-lib
    orders-lib
)
install(TARGETS services-lib)
