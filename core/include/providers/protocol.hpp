#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

#include "../core_global.h"
//#include "providers/controlcenterclient.hpp"
#include <eloquent/model.hpp>
#include <cstdint>

#include <QDataStream>


namespace core::providers {

namespace protocol {

enum class MessageType : std::uint32_t;

namespace msg {

constexpr MessageType UnknownMessage			= MessageType(0xFF'FF'FF'FF);
constexpr MessageType RegisterDevice			= MessageType(0x00'00'00'01);
constexpr MessageType UnregisterDevice			= MessageType(0x00'00'00'02);
constexpr MessageType DeviceStatus				= MessageType(0x00'00'00'03);

constexpr MessageType GetAllUsers				= MessageType(0x00'00'10'00);
constexpr MessageType GetAllUsersResponse		= MessageType(0xA0'00'10'00);
constexpr MessageType LoginUser					= MessageType(0x00'00'10'01);
constexpr MessageType LogoutUser				= MessageType(0x00'00'10'02);
constexpr MessageType GetUser					= MessageType(0x00'00'10'03);
constexpr MessageType GetUserResponse			= MessageType(0xA0'00'10'03);
constexpr MessageType CreateUser				= MessageType(0x00'00'10'04);
constexpr MessageType CreateUserResponse		= MessageType(0xA0'00'10'04);
constexpr MessageType UpdateUser				= MessageType(0x00'00'10'05);
constexpr MessageType UpdateUserResponse		= MessageType(0xA0'00'10'05);
constexpr MessageType DestroyUser				= MessageType(0x00'00'10'06);
constexpr MessageType DestroyUserResponse		= MessageType(0xA0'00'10'06);
constexpr MessageType GetUserRoles				= MessageType(0x00'00'10'07);
constexpr MessageType GetUserRolesResponse		= MessageType(0xA0'00'10'07);

constexpr MessageType UserStatusResponse		= MessageType(0xA0'00'10'FF);

constexpr MessageType GetCompany				= MessageType(0x00'00'20'01);
constexpr MessageType GetCompanyResponse		= MessageType(0xA0'00'20'01);
constexpr MessageType CreateCompany				= MessageType(0x00'00'20'02);
constexpr MessageType CreateCompanyResponse		= MessageType(0xA0'00'20'02);
constexpr MessageType UpdateCompany				= MessageType(0x00'00'20'03);
constexpr MessageType UpdateCompanyResponse		= MessageType(0xA0'00'20'03);

constexpr MessageType GetWoodTypes				= MessageType(0x00'00'30'01);
constexpr MessageType GetWoodTypesResponse		= MessageType(0xA0'00'30'01);
constexpr MessageType GetWoodType				= MessageType(0x00'00'30'02);
constexpr MessageType GetWoodTypeResponse		= MessageType(0xA0'00'30'02);
constexpr MessageType CreateWoodType			= MessageType(0x00'00'30'03);
constexpr MessageType CreateWoodTypeResponse	= MessageType(0xA0'00'30'03);
constexpr MessageType UpdateWoodType			= MessageType(0x00'00'30'04);
constexpr MessageType UpdateWoodTypeResponse	= MessageType(0xA0'00'30'04);
constexpr MessageType DestroyWoodType			= MessageType(0x00'00'30'05);
constexpr MessageType DestroyWoodTypeResponse	= MessageType(0xA0'00'30'05);

constexpr MessageType GetAllContractors			= MessageType(0x00'00'40'01);
constexpr MessageType GetAllContractorsResponse = MessageType(0xA0'00'40'01);
constexpr MessageType GetContractor				= MessageType(0x00'00'40'02);
constexpr MessageType GetContractorResponse		= MessageType(0xA0'00'40'03);
constexpr MessageType CreateContractor			= MessageType(0x00'00'40'04);
constexpr MessageType CreateContractorResponse  = MessageType(0xA0'00'40'04);
constexpr MessageType UpdateContractor			= MessageType(0x00'00'40'05);
constexpr MessageType UpdateContractorResponse  = MessageType(0xA0'00'40'05);
constexpr MessageType DestroyContractor			= MessageType(0x00'00'40'06);
constexpr MessageType DestroyContractorResponse = MessageType(0xA0'00'40'06);

constexpr MessageType GetAllWarehouses			= MessageType(0x00'00'50'01);
constexpr MessageType GetAllWarehousesResponse	= MessageType(0xA0'00'50'01);
constexpr MessageType GetWarehouse				= MessageType(0x00'00'50'02);
constexpr MessageType GetWarehouseResponse		= MessageType(0xA0'00'50'02);
constexpr MessageType CreateWarehouse			= MessageType(0x00'00'50'03);
constexpr MessageType CreateWarehouseResponse	= MessageType(0xA0'00'50'03);
constexpr MessageType UpdateWarehouse			= MessageType(0x00'00'50'04);
constexpr MessageType UpdateWarehouseResponse	= MessageType(0xA0'00'50'04);
constexpr MessageType DestroyWarehouse			= MessageType(0x00'00'50'05);
constexpr MessageType DestroyWarehouseResponse	= MessageType(0xA0'00'50'05);
constexpr MessageType GetArticles				= MessageType(0x00'00'50'06);
constexpr MessageType GetArticlesResponse		= MessageType(0xA0'00'50'06);
constexpr MessageType GetArticle				= MessageType(0x00'00'50'07);
constexpr MessageType GetArticleResponse		= MessageType(0xA0'00'50'07);
constexpr MessageType GetArticleTypes			= MessageType(0x00'00'50'08);
constexpr MessageType GetArticleTypesResponse	= MessageType(0xA0'00'50'08);
constexpr MessageType GetArticleType			= MessageType(0x00'00'50'09);
constexpr MessageType GetArticleTypeResponse	= MessageType(0xA0'00'50'09);
constexpr MessageType CreateArticle				= MessageType(0x00'00'50'0A);
constexpr MessageType CreateArticleResponse		= MessageType(0xA0'00'50'0A);
constexpr MessageType UpdateArticle				= MessageType(0x00'00'50'0B);
constexpr MessageType UpdateArticleResponse		= MessageType(0xA0'00'50'0B);
constexpr MessageType DestroyArticle			= MessageType(0x00'00'50'0C);
constexpr MessageType DestroyArticleResponse	= MessageType(0xA0'00'50'0C);

constexpr MessageType GetAllOrders				= MessageType(0x00'00'60'01);
constexpr MessageType GetAllOrdersResponse		= MessageType(0xA0'00'60'01);
constexpr MessageType GetOrder					= MessageType(0x00'00'60'02);
constexpr MessageType GetOrderResponse			= MessageType(0xA0'00'60'02);
constexpr MessageType CreateOrder				= MessageType(0x00'00'60'03);
constexpr MessageType CreateOrderResponse		= MessageType(0xA0'00'60'03);
constexpr MessageType UpdateOrder				= MessageType(0x00'00'60'04);
constexpr MessageType UpdateOrderResponse		= MessageType(0xA0'00'60'04);
constexpr MessageType DestroyOrder				= MessageType(0x00'00'60'05);
constexpr MessageType DestroyOrderResponse		= MessageType(0xA0'00'60'05);

constexpr MessageType GetAllServices			= MessageType(0x00'00'70'01);
constexpr MessageType GetAllServicesResponse	= MessageType(0xA0'00'70'01);
constexpr MessageType GetService				= MessageType(0x00'00'70'02);
constexpr MessageType GetServiceResponse		= MessageType(0xA0'00'70'03);
constexpr MessageType CreateService				= MessageType(0x00'00'70'04);
constexpr MessageType CreateServiceResponse		= MessageType(0xA0'00'70'04);
constexpr MessageType UpdateService				= MessageType(0x00'00'70'05);
constexpr MessageType UpdateServiceResponse		= MessageType(0xA0'00'70'05);
constexpr MessageType DestroyService			= MessageType(0x00'00'70'06);
constexpr MessageType DestroyServiceResponse	= MessageType(0xA0'00'70'06);

constexpr MessageType GetAllSaleInvoices		= MessageType(0x00'00'80'01);
constexpr MessageType GetAllSaleInvoicesResponse= MessageType(0xA0'00'80'01);

constexpr MessageType GetAllPurchaseInvoices			= MessageType(0x00'00'90'01);
constexpr MessageType GetAllPurchaseInvoicesResponse	= MessageType(0xA0'00'90'01);

constexpr MessageType GetAllCities				= MessageType(0x00'00'F0'01);
constexpr MessageType GetAllCitiesResponse		= MessageType(0xA0'00'F0'01);
constexpr MessageType GetCity					= MessageType(0x00'00'F0'02);
constexpr MessageType GetCityResponse			= MessageType(0xA0'00'F0'02);
constexpr MessageType CreateCity				= MessageType(0x00'00'F0'03);
constexpr MessageType CreateCityResponse		= MessageType(0xA0'00'F0'03);

constexpr MessageType GetMeasureUnits			= MessageType(0x00'00'F1'01);
constexpr MessageType GetMeasureUnitsResponse	= MessageType(0xA0'00'F1'01);
constexpr MessageType GetMeasureUnit			= MessageType(0x00'00'F1'02);
constexpr MessageType GetMeasureUnitResponse	= MessageType(0xA0'00'F1'02);

constexpr MessageType GetAllTaxes				= MessageType(0x00'00'F2'01);
constexpr MessageType GetAllTaxesResponse		= MessageType(0xA0'00'F2'01);
constexpr MessageType GetTaxById				= MessageType(0x00'00'F2'02);
constexpr MessageType GetTaxByIdResponse		= MessageType(0xA0'00'F2'02);
constexpr MessageType GetTaxByValue				= MessageType(0x00'00'F2'03);
constexpr MessageType GetTaxByValueResponse		= MessageType(0xA0'00'F2'03);
constexpr MessageType DestroyTax				= MessageType(0x00'00'F2'04);
constexpr MessageType DestroyTaxResponse		= MessageType(0xA0'00'F2'04);
constexpr MessageType CreateTax					= MessageType(0x00'00'F2'05);
constexpr MessageType CreateTaxResponse			= MessageType(0xA0'00'F2'05);
constexpr MessageType UpdateTax					= MessageType(0x00'00'F2'06);
constexpr MessageType UpdateTaxResponse			= MessageType(0xA0'00'F2'07);

} // msg namespace

struct MessageHeader
{
	MessageType type{};
	std::uint32_t dataSize{};
	std::uint32_t headerSize{};
	std::uint32_t crc{};

	// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const MessageHeader& header) noexcept
	{
		stream << header.crc
			   << header.headerSize
			   << header.dataSize
			   << header.type;

		return stream;
	}

	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, MessageHeader& header) noexcept
	{
		stream >> header.crc
			>> header.headerSize
			>> header.dataSize
			>> header.type;
		return stream;
	}
};

struct DeviceStatus
{
	bool registered{};

	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const DeviceStatus& status) noexcept
	{
		stream << status.registered;
		return stream;
	}
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, DeviceStatus& status) noexcept
	{
		stream >> status.registered;
		return stream;
	}
};

enum class UserStatus : uint8_t
{
	Rejected = 1,
	Accepted = 2,
	AlreadyLogged = 3,
	Unlogged = 4
};

enum class OperationStatus : std::uint8_t
{
	Fail = 0,
	Successfull = 0xFF
};

} // namespace protocol

enum Protocol : std::uint32_t {
//	MsgRegisterDevice	= 100,	// rejestrowanie urządzenia na serwerze {uniqueId, machineHostName}
	MsgUnregisterDevice,		// wyrejestrowanie urządzenia
	MsgDeviceStatus,			// status rejestracji urządzenia {status}
	MsgDeviceAccepted,			// urządzenie zarejestrowane
	MsgDeviceRejected,			// urządzenie odrzucone

	MsgUserLogin		= 200,	// {userName, password}
	MsgUserLogout,

	MsgGetAllUser,				// {}		|-> <status> <users list>
	MsgGetUser,					// {userId} |-> <status> <user>
	MsgCreateUser,				// {user}	|-> <status> <user>
	MsgUpdateUser,				// {user}	|-> <status>
	MsgDestroyUser,				// {userId} |-> <status>
	MsgGetUserRoles,			// {}		|-> <roles>

	MsgGetCompany		= 300,	// {companyId}	|-> <status> <company>
	MsgUpdateCompany,
	MsgCreateCompany,

	MsgGetWarehouses	= 400,	// {}				|-> <status> <warehouse list>
	MsgGetWarehouse,			// {warehouseId}	|-> <status> <warehouse>
	MsgCreateWarehouse,			// {warehouse}		|-> <status> <warehouse>
	MsgUpdateWarehouse,			// {warehouse}		|-> <status> <warehouse>
	MsgDestroyWarehouse,		// {warehouseId}	|-> <status>

	MsgGetArticle,
	MsgGetArticlesForWarehouse,
	MsgCreateArticle,
	MsgUpdateArticle,
	MsgDestroyArticle,

	MsgGetArticleTypes,			// {}		|-> <status> <list>
	MsgGetArticleType,			// {}		|-> <status> <article type>
	MsgGetAllTaxes,
	MsgGetTaxById,
	MsgGetTaxByValue,
	MsgDestroyTax,
	MsgUpdateTax,
	MsgCreateTax,

	MsgUpdateContractor = 500,
	MsgCreateContractor,
	MsgDestroyContractor,
	MsgGetContractors,
	MsgGetContractor,

	MsgGetOrders,
	MsgGetOrder,
	MsgCreateOrder,
	MsgUpdateOrder,
	MsgDestroyOrder,

	MsgGetServices,
	MsgGetService,
	MsgCreateService,
	MsgUpdateService,
	MsgDestroyService,

	MsgCreateSaleInvoice,
	MsgUpdateSaleInvoice,
	MsgGetSaleInvoice,
	MsgGetAllSaleInvoices,
	MsgMarkSaleInvoiceAsCorrectionDocument,
	MsgGetSaleInvoiceCorrection,
	MsgPostSaleInvoice,

	MsgCreatePurchaseInvoice,
	MsgUpdatePurchaseInvoice,
	MsgGetPurchaseInvoice,
	MsgGetAllPurchaseInvoices,
	MsgGetItemsForPurchaseInvoice,
	MsgPostPurchaseInvoice,
	MsgGetPurchaseInvoiceCorrection,

	MsgGetWoodTypes,
	MsgGetWoodType,
	MsgDestroyWoodType,
	MsgCreateWoodType,
	MsgUpdateWoodType,

	MsgGetMeasureUnits,			// {}		|-> <status> <measure unit list>
	MsgGetMeasureUnit,			// {id}		|-> <status> <measure unit>

	MsgGetCities		= 900,	// {}		|-> <status> <cities list>
	MsgGetCity,					// {cityId}	|-> <status> <city>
	MsgCreateCity,				// {city}	|-> <status> <city>

	MsgLockResource,
	MsgUnlockResource,

// błędy
	MsgMethodNotImplement	= 999,	// wywołanie nie zostało znalezione po stronie serwera
	MsgFail,
	MsgResponseUserWasLogged,

// sukcesy
	MsgSuccess = 10'000
};


//
// protocol dedicated for notification service (event agnet)
//
namespace agent {

enum class EventType : std::uint32_t
{
	RepositorySync,
	ServerStatus,
};

///
/// \brief The ResourceOperation enum
/// type of operations performed on resources
///
enum class ResourceOperation : std::uint32_t {
	Created,
	Updated,
	Removed,

	// uses only in user operations
	Logged,
	Logout
};

///
/// \brief The Resources enum
/// resource identificators whose can be locked on editing time
///
enum class Resource : std::uint32_t {
	User,
	Warehouse,
	Contractor,
	Article,
	PurchaseInvoice,
	SaleInvoice,
	Order,
	Service,
	Company,
	WoodType,
	Tax
};

///
/// \brief The ServerStatus enum
/// error codes
///
enum class ServerStatus : std::uint32_t {
	NoError		= 0x00'00'00'00
};

struct CORE_EXPORT RepositoryEvent {
	Resource resourceType{};				///< the type of resource that has been modified
	ResourceOperation operation{};			///< type of operation performed
	core::eloquent::DBIDKey resourceId{};	///< ID of the modified resource
	core::eloquent::DBIDKey userId{};		///< ID of the user who made change
	QTime time{};							///< received time event

	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const RepositoryEvent& event) noexcept {
		stream << event.resourceType << event.resourceId << event.operation << event.time << event.userId;
		return stream;
	}

	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, RepositoryEvent& event) noexcept {
		stream >> event.resourceType >> event.resourceId >> event.operation >> event.time >> event.userId;
		return stream;
	}
};

struct CORE_EXPORT ServerStatusEvent {
	ServerStatus status;

	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const ServerStatusEvent& event) noexcept {
		stream << std::uint32_t(event.status);
		return stream;
	}

	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, ServerStatusEvent& event) noexcept {
		stream >> event.status;
		return stream;
	}
};

struct CORE_EXPORT Event
{
	EventType type{};
	std::uint32_t crc{};
	std::uint32_t payloadSize{};
	QByteArray payload{};

	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const Event& event) noexcept {
		stream << event.type << event.crc << event.payloadSize << event.payload;
		return stream;
	}

	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, Event& event) noexcept {
		stream >> event.type >> event.crc >> event.payloadSize >> event.payload;
		return stream;
	}
};

} // namespace agent
} // namespace core::protocol

#endif // PROTOCOL_HPP
