#ifndef CONTROLCENTERCLIENT_HPP
#define CONTROLCENTERCLIENT_HPP

#include "../core_global.h"
#include "../dbo/user.h"
#include "../dbo/measureunit_core.hpp"
#include "../dbo/wareHouse.h"
#include "../dbo/article.h"
#include "../dbo/woodtype.hpp"
#include "../dbo/saleinvoice.hpp"
#include "../dbo/purchaseinvoice.hpp"
#include "../dbo/order.h"
#include "../dbo/service.h"
#include "../dbo/city.hpp"
#include <providers/protocol.hpp>
#include <QTcpSocket>
#include <QMutex>
#include <QWaitCondition>


namespace agent = core::providers::agent;

namespace core::providers {

class CORE_EXPORT ControlCenterClient : public QTcpSocket {
	Q_OBJECT

public:
	explicit ControlCenterClient(QObject* parent = nullptr);
	virtual ~ControlCenterClient() = default;

	static ControlCenterClient* createSingelton(QObject* parent);
	static void destroySingelton();
	static ControlCenterClient* instance();

private Q_SLOTS:
	void readPendindData();

private:
	bool waitForReadyData(int msecs = 30000);
	QByteArray pendingData_{};
	QByteArray responsePayload_{};

Q_SIGNALS:
	void dataReady();

public:
	///
	/// \brief The ErrorCode enum
	/// error codes used to inform other users about problems on server side
	///
	enum class StatusCode {
		// successfull responses
		Ok								= 200,
		Created							= 201,
		Accepted						= 202,
		NoContent						= 204,
		ResetContent					= 205,

		// client error responses
		BadRequest						= 400,
		Unauthorized					= 401,
		Forbidden						= 403,
		NotFound						= 404,
		MethodNowAllowed				= 405,

		// server error responses
		InternalServerError				= 500,
		NotImplemented					= 501,
		ServiceUnavaiable				= 503,
		NetworkAuthenticationRequired	= 511
	};

public:
	StatusCode registerDevice();
	StatusCode unregisterDevice();
	bool isDeviceRegistered() const { return !m_uniqueDeviceId.isEmpty(); }

	StatusCode lock(agent::Resource resource, core::eloquent::DBIDKey resourceId, core::eloquent::DBIDKey userId);
	StatusCode unlock(agent::Resource resource, core::eloquent::DBIDKey resourceId);

public:
	core::User loginUser(const QString& userName, const QString& password);
	void logoutUser(const core::eloquent::DBIDKey& userId);
	StatusCode getAllUsers(QList<core::User>& users);
	StatusCode getUser(const core::eloquent::DBIDKey& userId, core::User& user);
	StatusCode createUser(core::User& user);
	StatusCode updateUser(core::User& user);
	StatusCode destroyUser(core::eloquent::DBIDKey userId);
	StatusCode getUserRoles(core::RoleCollection& roles);

public:
	StatusCode getCompany(core::eloquent::DBIDKey companyId, core::Company& company);
	StatusCode createCompany(core::Company& company);
	StatusCode updateCompany(core::Company& company);

public:
	StatusCode getAllMeasureUnits(QList<core::MeasureUnit>& units);
	StatusCode getMeasureUnit(core::eloquent::DBIDKey unitId, core::MeasureUnit& unit);

public:
	StatusCode getAllWarehouses(QList<core::Warehouse>& warehouses);
	StatusCode getWarehouse(core::eloquent::DBIDKey warehouseId, core::Warehouse& warehouse);
	StatusCode createWarehouse(core::Warehouse& warehouse);
	StatusCode updateWarehouse(core::Warehouse& warehouse);
	StatusCode destroyWarehouse(core::eloquent::DBIDKey warehouseId);
	StatusCode getArticlesForWarehouse(core::eloquent::DBIDKey warehouseId, QList<core::Article>& articles);

public:
	StatusCode getArticleTypes(QList<core::ArticleType>& types);
	StatusCode getArticleType(core::eloquent::DBIDKey typeId, core::ArticleType& articleType);
	StatusCode getArticle(core::eloquent::DBIDKey articleId, core::Article& article);
	StatusCode updateArticle(core::Article& article);
	StatusCode createArticle(core::Article& article);
	StatusCode destroyArticle(core::eloquent::DBIDKey articleId);

public:
	StatusCode getAllTaxes(QList<core::Tax>& taxes);
	StatusCode getTaxById(core::eloquent::DBIDKey id, core::Tax& tax);
	StatusCode getTaxByValue(double value, core::Tax& tax);
	StatusCode createTax(core::Tax& tax);
	StatusCode updateTax(const core::Tax& tax);
	StatusCode destroyTax(core::eloquent::DBIDKey id);

public:
	StatusCode getAllWoodsType(QList<core::WoodType>& woods);
	StatusCode getWoodType(core::eloquent::DBIDKey id, core::WoodType& type);
	StatusCode createWoodType(core::WoodType& wood);
	StatusCode updateWoodType(core::WoodType& wood);
	StatusCode destroyWoodType(core::eloquent::DBIDKey woodId);

public:
	StatusCode getAllContractors(QList<core::Contractor>& contractors);
	StatusCode getContractor(core::eloquent::DBIDKey id, core::Contractor& contractor);
	StatusCode updateContractor(core::Contractor& contractor);
	StatusCode createContractor(core::Contractor& contractor);
	StatusCode destroyContractor(core::eloquent::DBIDKey id);

public:
	StatusCode getSaleInvoices(QList<core::SaleInvoice>& invoices);
	StatusCode getSaleInvoice(core::eloquent::DBIDKey id, core::SaleInvoice& invoice);
	StatusCode updateSaleInvoice(core::SaleInvoice& invoice);
	StatusCode createSaleInvoice(core::SaleInvoice& invocie);
	StatusCode getSaleInvoiceAttributes(core::eloquent::DBIDKey id, QMap<QString, QVariant>& map);
	StatusCode markSaleInvoiceAsCorrectionDocument(core::eloquent::DBIDKey orginalId, core::eloquent::DBIDKey correctionId);
	StatusCode getSaleInvoiceCorrection(core::eloquent::DBIDKey orginalId, core::SaleInvoice& invoice);
	StatusCode postSaleInvoice(core::eloquent::DBIDKey invoiceId);

public:
	/// purchase invoices
	StatusCode getPurchaseInvoices(QList<core::PurchaseInvoice>& invoices);
	StatusCode getPurchaseInvoice(core::eloquent::DBIDKey invoiceId, core::PurchaseInvoice& invoice);
	StatusCode getPurchaseInvoiceItems(core::eloquent::DBIDKey id, QList<core::PurchaseInvoiceItem>& items);
	StatusCode getPurchaseInvoiceAttributes(core::eloquent::DBIDKey id, QMap<QString, QVariant>& map);
	StatusCode postPurchaseInvoice(core::eloquent::DBIDKey purchaseInvoiceId);
	StatusCode getPurchaseInvoiceCorrection(core::eloquent::DBIDKey orginalId, core::PurchaseInvoice& invoice);
	StatusCode updatePurchaseInvoice(core::PurchaseInvoice& invoice);
	StatusCode createPurchaseInvoice(core::PurchaseInvoice& invoice);

public:
	/// orders
	StatusCode getOrder(core::eloquent::DBIDKey orderId, core::Order& order);
	StatusCode getOrders(QList<core::Order>& orders);
	StatusCode updateOrder(core::Order& order);
	StatusCode createOrder(core::Order& order);
	StatusCode destroyOrder(core::eloquent::DBIDKey orderId);

public:
	/// services
	StatusCode getService(core::eloquent::DBIDKey serviceId, core::Service& service);
	StatusCode getServices(QList<core::Service>& services);
	StatusCode destroyService(core::eloquent::DBIDKey serviceId);
	StatusCode updateService(core::Service& service);
	StatusCode createService(core::Service& service);

public:
	/// city dictionary
	StatusCode getCities(QList<core::City>& cities);
	StatusCode getCity(const core::eloquent::DBIDKey& cityId, core::City& city);
	StatusCode createCity(core::City& city);

private:
	QDataStream stream;
	QString m_uniqueDeviceId;
};

} // namespace core::providers

#endif // CONTROLCENTERCLIENT_HPP
