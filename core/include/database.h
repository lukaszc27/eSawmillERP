#ifndef CORE_H
#define CORE_H

#include "core_global.h"
#include <QtXml>


namespace core
{

class CORE_EXPORT Database
{
public:
    Database();
    ~Database();

    /**
     * @brief connect - Nawiązuje połączenie z serwerem SQL
     */
    static void connect();
	static void disconnect();

    /**
     * @brief configFileExist - Sprawdza czy plik konfiguracji połączenia istnieje
     * jeśli nie to znaczy że jest to pierwsze uruchomienie programu
     * @return true - jeśli plik istnieje
     */
//	static bool configFileExist();

    /**
     * @brief writeConfigFile - Zapisuje konfigurację do połączenia z serwerem SQL do pliku
     * @param driver - nazwa sterownika do nawiązania połączenia
     * @param host - adres IP
     * @param userName - nazwa użytkownika serwera SQL
     * @param password - hasło
     * @param dbName - Nazwa bazy danych z którą się łączymy
     */
	static void writeConfig(const QString &driver,
							const QString &host,
							const QString &userName,
							const QString &password,
							const QString &dbName,
							bool trustedConnection = true);

	/**
	 * @brief restore
	 * odtwarza bazę danych z plików kopii bezpieczeństwa
	 * @param fileName - nazwa pliku z kopią bezpieczeństwa
	 * @return
	 */
	static bool restore(const QString fileName);

	/**
	 * @brief createBackup - tworzy główny element XML zawierający wszystkie dane z bazy SQL
	 * @return obiekt XML gotowy do zapisu w głównym dokumecie kopii bezpieczeństwa
	 */
	static bool createBackup(const QString &fileName,
							 const QString &name,
							 const QString &description = QString());
};

}

#endif // CORE_H
