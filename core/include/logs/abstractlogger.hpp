#ifndef ABSTRACTLOGGER_HPP
#define ABSTRACTLOGGER_HPP

#include "../core_global.h"
#include <QMutex>

namespace core::logs {

///
/// \brief The AbstractLogger struct
/// base struct for all logger uses in system
///
struct CORE_EXPORT AbstractLogger {
	///
	/// \brief The Type enum
	/// log level type
	///
	enum class Type {
		Information,
		Warning,
		Critical,
		Debug,
		Trace
	};

	///
	/// \brief operator bool
	/// use it before use logger to check avaiable
	/// this operator should be implement by all object inherit from AbstractLogger object
	///
	virtual operator bool() = 0;

	///
	/// \brief operator ()
	/// override call operator to execute log function
	/// only for convenience
	/// \param message
	/// \param type
	///
	void operator()(const QString& message, Type type = Type::Information)
		{ log(message, type); }

	///
	/// \brief log
	/// write information to source
	/// \param message - message to write
	/// \param type - log level type
	///
	virtual void log(const QString& message, Type type = Type::Information) = 0;

	///@{
	/// some methods for convenience use logger object

	virtual void information(const QString& message)
		{ log(message, Type::Information); }

	virtual void warning(const QString& message)
		{ log(message, Type::Warning); }

	virtual void critical(const QString& message)
		{ log(message, Type::Critical); }

	virtual void debug(const QString& message)
		{ log(message, Type::Debug); }
	///@}
};

} // namespace core::logs

#endif // ABSTRACTLOGGER_HPP
