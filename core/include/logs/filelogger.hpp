#ifndef FILELOGGER_HPP
#define FILELOGGER_HPP

#include "../core_global.h"
#include "abstractlogger.hpp"
#include <QFile>

namespace core::logs {

///
/// \brief The FileLogger struct
/// logger to store information in local file (in logs dictionary)
/// implement RAII pattern to manage pointer to file
///
struct CORE_EXPORT FileLogger : public AbstractLogger {
	///@{
	/// ctors
	explicit FileLogger();
	virtual ~FileLogger();
	///@}

	///
	/// \brief operator bool
	/// check avaiable logger before use
	///
	virtual operator bool() override;

	///
	/// \brief log
	/// save data to file
	/// the filename schould be current date in format DDMMYYY.log
	/// \param message
	/// \param type
	///
	virtual void log(const QString& message, Type type = Type::Information) override;

private:
	QFile* _file;	///< pointer to file uses to store logs
};

} // namespace core::logs

#endif // FILELOGGER_HPP
