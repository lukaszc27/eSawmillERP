#ifndef ORDERDOCUMENT_H
#define ORDERDOCUMENT_H

#include "../core_global.h"
#include "abstractdocument.h"
#include "../dbo/order.h"
#include "../dbo/orderElements.h"
#include "../dbo/orderarticle.h"


namespace core::documents {

///
/// \brief The OrderDocument class
/// document to print full information about order
///
class CORE_EXPORT OrderDocument : public AbstractDocument {
	Q_OBJECT

public:
	explicit OrderDocument(
		const core::Company& company,
		const core::Order& order,
		QObject* parent = nullptr);

	/// @brief body
	/// draw all order items, and prepare doc to print
	/// @return true when document can be print
	QString body() override;

protected:
	core::Order mOrder;
};

} // namespace eSawmill

#endif // ORDERDOCUMENT_H
