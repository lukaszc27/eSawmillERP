#ifndef SERVICEDOCUMENT_H
#define SERVICEDOCUMENT_H

#include "../core_global.h"
#include "../dbo/service.h"
#include "../dbo/serviceelement.h"
#include "../dbo/servicearticle.h"
#include "abstractdocument.h"


namespace core::documents {

/**
 * @brief The ServiceDocument class
 * dokument (kosztorys) usługi prezentujące wszystkie informacje
 * nt. wybranej usługi
 */
class CORE_EXPORT ServiceDocument : public AbstractDocument {
	Q_OBJECT

public:
	ServiceDocument(core::Company company,
					core::Contractor contractor,
					core::Service service,
					QObject* parent = nullptr);

	QString body() override;

protected:
	core::Service mService;

	///
	/// \brief elementsTable
	/// draw table to display all elements in service
	/// \param elementsPrice - total price for elements
	/// \return QString to display (with HTML tag)
	///
	virtual QString elementsTable(double* elementsPrice) override;

};

} // namespace eSawmill

#endif // SERVICEDOCUMENT_H
