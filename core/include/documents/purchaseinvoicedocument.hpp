#ifndef PURCHASEINVOICEDOCUMENT_HPP
#define PURCHASEINVOICEDOCUMENT_HPP

#include "../core_global.h"
#include "abstractinvoice.hpp"
#include "dbo/purchaseinvoice.hpp"
#include "dbo/purchaseinvoiceitem.hpp"


namespace core::documents {

class CORE_EXPORT PurchaseInvoiceDocument final : public AbstractInvoice<core::PurchaseInvoice, core::PurchaseInvoiceItem> {
	Q_OBJECT

public:
	PurchaseInvoiceDocument(QObject* parent = nullptr);
	virtual ~PurchaseInvoiceDocument() = default;

	QString documentName() const override { return tr("Faktura zakupu"); }

protected:
	QString createTableOfItems(double* totalValue) override;
};

} // namespace core::documents

#endif // PURCHASEINVOICEDOCUMENT_HPP
