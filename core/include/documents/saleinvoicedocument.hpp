#ifndef SALEINVOICEDOCUMENT_HPP
#define SALEINVOICEDOCUMENT_HPP

#include "abstractinvoice.hpp"
#include "../dbo/saleinvoice.hpp"
#include "../dbo/saleinvoiceitem.hpp"

namespace core::documents {

class CORE_EXPORT SaleInvoiceDocument final : public AbstractInvoice<core::SaleInvoice, core::SaleInvoiceItem> {
	Q_OBJECT

public:
	SaleInvoiceDocument(QObject* parent = nullptr);
	virtual ~SaleInvoiceDocument() = default;

	QString documentName() const override { return tr("Faktura sprzedaży"); }

protected:
	QString createTableOfItems(double* totalPrice) override;
};

} // namespace core::documents

#endif // SALEINVOICEDOCUMENT_HPP
