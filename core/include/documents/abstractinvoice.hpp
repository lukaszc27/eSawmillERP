#ifndef ABSTRACTINVOICE_HPP
#define ABSTRACTINVOICE_HPP

#include "core_global.h"
#include <QTextDocument>
#include <QList>
#include <QDate>
#include "dbo/contractor.h"


namespace core::documents {
///
/// \brief The AbstractInvoice class
/// base class for all invoices in application
/// it's abstract class so any instance of this object can't exist
/// you have to extends this class if yout want to use it
///
template <typename InvoiceType, typename InvoiceItemsType>
class AbstractInvoice : public QTextDocument {
public:
	///@{
	/// ctors
	AbstractInvoice(QObject* parent = nullptr)
		: QTextDocument {parent}
	{
	}
	virtual ~AbstractInvoice() = default;
	///@}

	///
	/// \brief generate generate document
	/// and prepare it to printing
	///
	virtual bool generate() {
		if (documentName().isEmpty() || documentNumber().isEmpty())
			return false;

		QString html;
		QTextStream out(&html);

		double totalPrice = 0;

		out << "<style type='text/css'>"
			<< "th {"
			   "background-color: #e6e6e6;"
			   "padding:2px;"
			   "}"
			   "table, tr, th, td {"
			   "border-collapse: collapse;"
			   "}"
			<< "</style>";

		out << "<body>"
			<< "<table width='100%' border='0'>"
			<< createHeader()
			<< "<tr><td colspan='3'><p style='font-size:18pt;font-weight:bold;text-align:center;'>" << documentName() << " " << documentNumber() << "</p></td></tr>"
			<< createTableOfItems(&totalPrice)
			<< "<tr><td colspan='3' style='text-align:right;'>"
			   "<p style='font-size:14pt;font-weight:500;margin-top:10pt;l'>" << tr("Do zapłaty: ") << totalPrice << " PLN" << "</p>"
			   "</td></tr>"
			<< createFooter()
			<< "</table>"
			<< "</body>";

		html.replace('\n', "<br/>");
		setHtml(html);

		return true;
	}

	///
	/// \brief documentName return title of document
	/// it must be override in extends class
	/// \return title for document
	///
	virtual QString documentName() const = 0;

	void setDocumentNumber(const QString& number)	{ mDocumentNumber = number; }
	void setPayment(const QString& payment)			{ mPayment = payment; }
	void setPaymentDays(int days)					{ mPaymentDays = days; }
	void setPaymentDate(const QDate& date)			{ mPaymentDate = date; }
	void setDateOfIsse(const QDate& date)			{ mDateOfIsse = date; }
	void setPlaceOfIssue(const QString& place)		{ mPlaceOfIssue = place; }
	QString placeOfIssue() const					{ return mPlaceOfIssue; }
	QString documentNumber() const					{ return mDocumentNumber; }
	QString payment() const							{ return mPayment; }
	int paymentDays() const							{ return mPaymentDays; }
	QDate paymentDate() const						{ return mPaymentDate; }
	QDate dateOfIssue() const						{ return mDateOfIsse; }
	QDate dateOfService() const						{ return mDateOfService; }


	void setDateOfService(const QDate& date)		{ mDateOfService = date; }
	void setSeller(const core::Contractor& seller)	{ mSeller = seller; }
	void setBuyer(const core::Contractor& buyer)	{ mBuyer = buyer; }
	core::Contractor seller() const					{ return mSeller; }
	core::Contractor buyer() const					{ return mBuyer; }

	void setInvoiceItems(const core::eloquent::Collection<InvoiceItemsType>& items) { mInvoiceItems = items; }
	core::eloquent::Collection<InvoiceItemsType> invoiceItems() const				{ return mInvoiceItems; }


protected:
	///
	/// \brief createTableOfItems create table to show all items on document
	/// \param totalValue write total price to pay by items
	/// \return QString with HTML tags
	///
	virtual QString createTableOfItems(double* totalValue) = 0;

	///
	/// \brief createHeader create document header with brand logo
	/// and other required informations
	/// \return QString with HTML tags
	///
	QString createHeader() const {
		QString html;
		QTextStream out(&html);

		out << "<tr>";

		out << "<td with='48%'>"
			   // brand logo
			<< "</td>";

		out << "<td></td>";	// separator

		out << "<td width='48%'>"
			<< "<table width='100%' border='0'>"
			//----
			<< "<tr><th style='font-weight:normal;'>" << tr("Miejsce wystawienia") << "</th></tr>"
			<< "<tr><td style='text-align:center;margin-bottom:2px'>" << placeOfIssue() << "</td></tr>"
			//----
			<< "<tr><th style='font-weight:normal;'>" << tr("Data wystawienia") << "</th></tr>"
			<< "<tr><td style='text-align:center;margin-bottom:2px'>" << dateOfIssue().toString(Qt::DateFormat::ISODate) << "</td></tr>"
			//----
			<< "<tr><th style='font-weight:normal;'>" << tr("Data wykonania usługi") << "</th><tr>"
			<< "<tr><td style='text-align:center;margin-bottom:2px'>" << dateOfService().toString(Qt::DateFormat::ISODate) << "</td></tr>"
			<< "</table>"
			<< "</td>"
			<< "</tr>";

		out << "<tr>"
			<< "<td width='48%'>"
			<< "<table width='100%' border='0'>"
			<< "<tr><th>" << tr("Sprzedawca") << "</th></tr>"
			<< "<tr><td>" << mSeller.toString() << "</td></tr>"
			<< "</table>"
			<< "</td>";

		out << "<td></td>";	// separator

		out << "<td width='48%'>"
			<< "<table width='100%' border='0'>"
			<< "<tr><th>" << tr("Nabywca") << "</th></tr>"
			<< "<tr><td>" << mBuyer.toString() << "</td></tr>"
			<< "</table>"
			<< "</td>"
			<< "</tr>";

		return html;
	}

	QString createFooter() const {
		QString html;
		QTextStream out(&html);

		out << "<tr>"
			<< "<td colspan='3'>"
			<< "<table width='100%' border='0'>"
			<< "<tr><td width='25%'>" << tr("Sposób płatności") << "</td><td>" << payment() << "</td></tr>";

		if (payment().toUpper() != "GOTÓWKA") {
			out << "<tr><td width='25%'>" << tr("Termin płatności") << "</td><td>"
				<< paymentDate().toString(Qt::DateFormat::ISODate) << " (" << paymentDays() << " dni)" << "</td></tr>"
				<< "<tr><td width='25%'>Bank</td><td>" << tr("Bank spółdzielczy w Lipinkach") << "</td></tr>"
				<< "<tr><td width='25%'>Numer konta</td><td>" << "05 1020 00000000 0000 0000 0000" << "</td></tr>";
		}

		out << "<tr style='height:100%'><td width='100%'></td></tr>"
			<< "<tr><td colspan='3'><p style='font-size:10pt;'>" << tr("Wydrukowano z programu eSawmill ERP dla Linux") << "</p></td></tr>";
		out << "</table>"
			<< "</td>";

		out << "</tr>";

		return html;
	}

private:
	QString mDocumentNumber;
	QString mPayment;
	QString mPlaceOfIssue;
	int mPaymentDays;
	QDate mPaymentDate;
	QDate mDateOfIsse;
	QDate mDateOfService;
	core::Contractor mSeller;
	core::Contractor mBuyer;
	core::eloquent::Collection<InvoiceItemsType> mInvoiceItems;
};

} // namespace core::documents

#endif // ABSTRACTINVOICE_HPP
