#ifndef ABSTRACTDOCUMENT_H
#define ABSTRACTDOCUMENT_H

#include "../core_global.h"
#include "../dbo/company.h"
#include "../dbo/contractor.h"
#include "../dbo/article.h"

#include <QTextDocument>


namespace core::documents {

///
/// \brief The AbstractDocument class
/// basic class for documents in app
/// this object is use to generate orders or service document for client
///
class CORE_EXPORT AbstractDocument : public QTextDocument {
	Q_OBJECT

public:
	AbstractDocument(core::Company company,
					 core::Contractor contractor,
					 QObject* parent = nullptr);

	void setDocumentTitle(const QString title) { mTitle = title; }
	void setDocumentSubtitle(const QString subtitle) { mSubtitle = subtitle; }

	///
	/// \brief setCompany
	/// sets the company that issued the document
	/// propapby in all cases it will be company assigned to curret logged user
	/// \param company
	///
	void setCompany(const core::Company company);
	core::Company company() const { return mCompany; }

	///
	/// \brief setContractor
	/// contractor for whom the document was issued
	/// \param contractor
	///
	void setContractor(const core::Contractor contractor);
	core::Contractor contractor() const { return mContractor; };

	///
	/// \brief generate
	/// generate document text (with all nessesary mark) and prepare to print
	/// \return
	///
	virtual bool generate();

	///
	/// \brief body
	/// serve to place custom data in document
	/// \return
	///
	virtual QString body() = 0;

protected:
	/**
	 * @brief articlesTable - tworzy tabelę z artykułami
	 * @param articles - lista artykułów które mają zostać wyświetlone w dokumęcie
	 * @param totalArticlesPrice - całkowita cena artykułów która ma być dodana do całkowitej ceny zamówienia
	 * @return
	 */
	virtual QString articlesTable(QList<core::Article> articles, double* totalArticlesPrice);

	///
	/// \brief elementsTable
	/// draw table to display full list of assigned elements to order/service
	/// in abstract class this function do nothing and schould be overwritten in next classes
	/// \param price
	/// \return
	///
	virtual QString elementsTable(double* price);

//protected:
	QString mTitle;					// tytuł dokumentu
	QString mSubtitle;				// subtytuł dokumentu
	core::Company mCompany;			// firma wystawiająca dokument
	core::Contractor mContractor;	// kontrahent

	/**
	 * @brief applicationVersion - zwraca informację o wersji programu z którego dokonano wydruku dokumentu
	 * @return QString
	 */
	QString applicationVersion();

signals:
	/**
	 * @brief companyChanged
	 * emitowany po zmianie firmy wystawiającej dokument
	 */
	void companyChanged();

	/**
	 * @brief contractorChanged
	 * emitowany po zmianie kontrahenta na dokumencie
	 */
	void contractorChanged();
};

} // namespace eSawmill

#endif // ABSTRACTDOCUMENT_H
