#ifndef ABSTRACTSERVICEREPOSITORY_HPP
#define ABSTRACTSERVICEREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/service.h"
#include "../dbo/serviceelement.h"
#include "../dbo/servicearticle.h"
#include "../dbo/article.h"
#include <unordered_map>
#include <QReadWriteLock>

namespace core::repository {

///
/// \brief The AbstractServiceRepository struct
/// base repository to manage services
/// this struct schould be inherit before use
/// to make specific object to manage data direct on db or remote by server
///
struct CORE_EXPORT AbstractServiceRepository {
	///
	/// \brief get
	/// get service object from source
	/// \param serviceId - service identificator in database
	/// \return valid full Service object
	///
	virtual core::Service get(
			core::eloquent::DBIDKey serviceId
	) = 0;

	///
	/// \brief getAll
	/// get all services from source
	/// \return list of Service objects
	///
	virtual QList<core::Service> getAll() = 0;

	///
	/// \brief create
	/// create new service instance in source
	/// \param service
	/// \param elements
	/// \param articles
	/// \return
	///
	virtual bool create(
		core::Service& service
	) = 0;

	///
	/// \brief update
	/// update existing service in source
	/// \param service
	/// \return
	///
	virtual bool update(
		const core::Service& service	///< [in]
	) = 0;

	///
	/// \brief getServiceElements
	/// get all assigned elements to service
	/// \param serviceId
	/// \return valid full list of service element objects
	///
	virtual QList<core::service::Element> getServiceElements(
		core::eloquent::DBIDKey serviceId	///< [in]
	) = 0;

	///
	/// \brief getServiceArticles
	/// get all assigned articles to service
	/// \param serviceId
	/// \return valid full list of articles objects
	///
	virtual QList<core::Article> getServiceArticles(
		core::eloquent::DBIDKey serviceId				///< [in]
	) = 0;

	///
	/// \brief destroy
	/// remove service from source with all assigned elements and articles
	/// \param serviceId - service to remove
	/// \return true if operation will be success
	///
	virtual bool destroy(
		core::eloquent::DBIDKey serviceId				///< [in]
	) = 0;

	///
	/// \brief getFirstAvaiableDeadline
	/// get completed date for last existing service (not finished)
	/// \return
	///
	virtual QDate getFirstAvaiableDeadline() = 0;

	/// \brief lock
	/// safe service object before editing
	/// uses only by remote repository
	/// \param serviceId
	/// \return
	virtual bool lock(const core::eloquent::DBIDKey& serviceId) = 0;
	virtual bool unlock(const core::eloquent::DBIDKey& serviceId) = 0;

	/// @brief update service object in cache memory
	void updateServiceInCache(const core::eloquent::DBIDKey& serviceId);
	void removeServiceFromCache(const core::eloquent::DBIDKey& serviceId);

protected:
	QReadWriteLock m_cache_lock;
	std::unordered_map<core::eloquent::DBIDKey, core::Service> m_services_cache;			///< cache memory for service objects

	///////////////////////////////////////////////////////////////////////////////////////
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey serviceId)
			: _serviceId {serviceId}
		{}

		bool operator()(const std::pair<core::eloquent::DBIDKey, core::Service>& item) {
			return item.first == _serviceId;
		}
	private:
		core::eloquent::DBIDKey _serviceId;
	};
	///////////////////////////////////////////////////////////////////////////////////////
};

} // namespace core::repository

#endif // ABSTRACTSERVICEREPOSITORY_HPP
