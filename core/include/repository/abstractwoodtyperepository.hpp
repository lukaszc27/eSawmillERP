#ifndef ABSTRACTWOODTYPEREPOSITORY_H
#define ABSTRACTWOODTYPEREPOSITORY_H

#include "../core_global.h"
#include "../dbo/woodtype.hpp"
#include <QReadWriteLock>
#include <QList>
#include <unordered_map>

namespace core::repository {

///
/// \brief The WoodTypeRepository struct
///
struct CORE_EXPORT AbstractWoodTypeRepository {
	///
	/// \brief getAll - get all wood types
	/// \return list of WoodType objects
	///
	virtual QList<core::WoodType> getAll() = 0;

	///
	/// \brief create - create new element
	/// \param wood element to create
	/// \return true if operation will be success
	///
	virtual bool create(
		core::WoodType& wood		///< [in]
	) = 0;

	///
	/// \brief update - update existing element
	/// \param wood element to update
	/// \return
	///
	virtual bool update(
		core::WoodType& wood		///< [in]
	) = 0;

	///
	/// \brief destroy - destroy object if exist
	/// \param key object id to remove
	/// \return
	///
	virtual bool destroy(
		core::eloquent::DBIDKey& key
	) = 0;

	///
	/// \brief get
	/// \param id
	/// \return
	///
	virtual core::WoodType get(core::eloquent::DBIDKey id) = 0;

	////////////////////////////////////////////////////////////
	/// Comparators
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey woodId) noexcept
			: _woodId {woodId}
		{}

		// comparator for cache memory
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::WoodType>& item) noexcept {
			return item.first == _woodId;
		}
	private:
		core::eloquent::DBIDKey _woodId;
	};
	///////////////////////////////////////////////////////////////

protected:
	std::unordered_map<core::eloquent::DBIDKey, core::WoodType> m_cache;
	QReadWriteLock m_cache_lock;
};

} // namespace core::repository

#endif // ABSTRACTWOODTYPEREPOSITORY_H
