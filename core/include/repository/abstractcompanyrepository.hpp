#ifndef ABSTRACTCOMPANYREPOSITORY_HPP
#define ABSTRACTCOMPANYREPOSITORY_HPP

#include "../core_global.h"
#include <dbo/company.h>
#include <dbo/contact.h>
#include <unordered_map>


namespace core::repository {

struct CORE_EXPORT AbstractCompanyRepository {
	///
	/// \brief update existing company
	/// \param company
	/// \param contact
	///
	virtual bool update(core::Company& company) = 0;

	///
	/// \brief create new company
	/// \param company
	/// \param contact
	/// \return
	///
	virtual bool create(core::Company& company) = 0;

	///
	/// \brief getCompany - get company object from source by id
	/// \param companyId
	/// \return valid company object
	///
	virtual core::Company getCompany(
		core::eloquent::DBIDKey companyId	///< [in]
	) = 0;

	///@{
	/// cache operations

	///
	/// \brief updateCache
	/// update company in cache memory
	/// \param companyId - the company identificator to update
	///
	virtual void updateCache(const core::eloquent::DBIDKey& companyId);

	///
	/// \brief getByNIPNumber
	/// get company object by NIP number
	/// this method operate only on cache memory if object will not exist in cache
	/// the method return invalid company object
	/// \param nip
	/// \return valid company object IF EXIST IN CACHE MEMORY
	///
	virtual core::Company getByNIPNumber(const QString& nip);
	///@}

	/////////////////////////////////////////////////////////////////////
	/// comparators
	struct Comparator {
		explicit Comparator(const core::eloquent::DBIDKey& companyId)
			: _comapnyId {companyId}
		{
		}
		/// @brief set identificator for compared comapny
		void setCompanyId(const core::eloquent::DBIDKey& companyId) {
			_comapnyId = companyId;
		}

		/// @brief comparator uses by cache
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::Company>& item) {
			return item.first == _comapnyId;
		}

	private:
		core::eloquent::DBIDKey _comapnyId;
	};
	/////////////////////////////////////////////////////////////////////

protected:
	std::unordered_map<core::eloquent::DBIDKey, core::Company> m_cache;
};

} // namespace core::repository

#endif // ABSTRACTCOMPANYREPOSITORY_HPP
