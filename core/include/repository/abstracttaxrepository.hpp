#ifndef ABSTRACTTaxREPOSITORY_HPP
#define ABSTRACTTaxREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/tax.h"
#include <QList>
#include <unordered_map>

namespace core::repository {

///
/// \brief The AbstractTaxRepository struct
/// base struct for all Tax repositories
/// this object must be used to create next repositories (e.g local or remote)
///
struct CORE_EXPORT AbstractTaxRepository {
	///
	/// \brief getAll - get all Taxs existing in system
	/// \return list of Tax dbo elements
	///
	virtual QList<core::Tax> getAll() = 0;

	///
	/// \brief get - get Tax object from source
	/// \param TaxId - Tax id in db
	/// \return valid Tax object
	///
	virtual core::Tax get(
		core::eloquent::DBIDKey TaxId
	) = 0;

	///
	/// \brief getByValue
	/// get tax from db by value
	/// \param value - tax
	/// \return
	///
	virtual core::Tax getByValue(
			double value
	) = 0;

	///
	/// \brief create new element
	/// \param Tax - object to save
	/// \return true if operation will be success
	///
	virtual bool create(
		core::Tax& Tax			///< [in]
	) = 0;

	///
	/// \brief update - update existing object
	/// \param Tax - object to update
	/// \return true if operation will be success
	///
	virtual bool update(
		const core::Tax& Tax			///< [in]
	) = 0;

	///
	/// \brief destroy
	/// \param key
	/// \return
	///
	virtual bool destroy(
			core::eloquent::DBIDKey key	///< [in]
	) = 0;

protected:
	std::unordered_map<core::eloquent::DBIDKey, core::Tax> m_cache;
};

} // namespace core::repository

#endif // ABSTRACTTaxREPOSITORY_HPP
