#ifndef ABSTRACTMEASUREUNITREPOSITORY_HPP
#define ABSTRACTMEASUREUNITREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/measureunit_core.hpp"
#include <QList>
#include <unordered_map>

namespace core::repository {

///
/// \brief The AbstractMeasureUnitRepository struct
/// interface for measure unit repository
///
struct CORE_EXPORT AbstractMeasureUnitRepository {
	///
	/// \brief getAll
	/// get all measure unit from source
	/// \return list of MeasureUnit objects
	///
	virtual QList<core::MeasureUnit> getAll() = 0;

	///
	/// \brief get - get one record of measure unit from source by id
	/// \param unitId - measure unit id
	/// \return valid MeasureUnit object
	///
	virtual core::MeasureUnit get(
		core::eloquent::DBIDKey unitId		///< [in]
	) = 0;

protected:
	///
	/// \brief m_cache
	/// cache to use in next repository
	///
	std::unordered_map<core::eloquent::DBIDKey, core::MeasureUnit> m_cache;
};

} // namespace core::repository

#endif // ABSTRACTMEASUREUNITREPOSITORY_HPP
