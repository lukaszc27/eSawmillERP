#ifndef ABSTRACTINVOICEREPOSITORY_HPP
#define ABSTRACTINVOICEREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/saleinvoice.hpp"
#include "../dbo/purchaseinvoice.hpp"
#include "../dbo/saleinvoiceitem.hpp"
#include "../dbo/purchaseinvoiceitem.hpp"
#include "../dbo/order.h"
#include <QList>
#include <unordered_map>

namespace core::repository {

///
/// \brief The AbstractInvoiceRepository struct
/// interface for all invoices repository uses in app
///
struct CORE_EXPORT AbstractInvoiceRepository {
	///@{
	/// this methods group is only for sale invoices
	/// if yout want to add next methods you scholud use correct group
	/// it help us in future :)
	///
	/// \brief getAllSaleInvoices
	/// return all sale invoices from source (db)
	/// \return list of sale invoice objects
	///
	virtual QList<core::SaleInvoice> getAllSaleInvoices() = 0;

	///
	/// \brief getSaleInvoice - get invoice by ID
	/// \param saleInvoiceId - sale invoice ID
	/// \return SaleInvoice object
	///
	virtual core::SaleInvoice getSaleInvoice(
		core::eloquent::DBIDKey saleInvoiceId	///< [in]
	) = 0;

	///
	/// \brief hasSaleInvoiceCorrection
	/// check do exist correction document for sale invoice
	/// \param saleInvoiceId - invlice identificator to check
	/// \param useCache - if it is true, before check cache memory and return information from cache
	/// \return true if correction document exist
	///
	virtual bool hasSaleInvoiceCorrection(
		core::eloquent::DBIDKey saleInvoiceId,	///< [in]
		bool useCache = false					///< [in]
	) = 0;

	///
	/// \brief getCorrectionForSaleInvoice
	/// get sale invoice correction for gave document
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::SaleInvoice getCorrectionForSaleInvoice(
		core::eloquent::DBIDKey saleInvoiceId	///< [in]
	) = 0;

	///
	/// \brief hasSaleInvoiceAssignedOrder
	/// check do invoice as assigned order (is invoice to order)
	/// \param saleInvoiceId - invoice to check
	/// \param useCache - do use cache memory
	/// \return
	///
	virtual bool hasSaleInvoiceAssignedOrder(
		core::eloquent::DBIDKey saleInvoiceId,	///< [in]
		bool useCache = false					///< [in]
	) = 0;

	///
	/// \brief getAssignedOrderForSaleInvoice
	/// get order for invoice (if exist)
	/// you scholud before check it by hasSaleInvoiceAssignedOrder method
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::Order getAssignedOrderForSaleInvoice(
		core::eloquent::DBIDKey saleInvoiceId	///< [in]
	) = 0;

	///
	/// \brief postSaleInvoice
	/// remove invoice from buffer (post)
	/// \param saleInvoiceId - sale to posted
	/// \return
	///
	virtual bool postSaleInvoice(
		core::eloquent::DBIDKey saleInvoiceId
	) = 0;

	///
	/// \brief createSaleInvoice
	/// create new instance of sale invoice
	/// \param saleInvoice - object to save in database
	/// \return true if operation result will be success
	///
	virtual bool createSaleInvoice(
		core::SaleInvoice& saleInvoice		///< [in]
	) = 0;

	///
	/// \brief updateSaleInvoice
	/// update existing sale invoice in system
	/// and recreated all assigned items
	/// \param saleInvoice - invoice to update
	/// \return true if operation will be success
	///
	virtual bool updateSaleInvoice(
		core::SaleInvoice& saleInvoice		///< [in]
	) = 0;

	///
	/// \brief markSaleInvoiceAsCorrectionDocument
	/// mark invoce as correction
	/// \param orignalInvoiceId - this document is left remove, and for it will be make correction document
	/// \param correctionInvoiceId - this is correction document for removed document (look at top)
	/// \return
	///
	virtual bool markSaleInvoiceAsCorrectionDocument(
		core::eloquent::DBIDKey orignalInvoiceId,	///< [in]
		core::eloquent::DBIDKey correctionInvoiceId	///< [in]
	) = 0;

	///
	/// \brief getSaleInvoiceItems
	/// get all items assigned to sale invoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual QList<core::SaleInvoiceItem> getSaleInvoiceItems(
		core::eloquent::DBIDKey saleInvoiceId		///< [in]
	) = 0;
	///@}


	///@{
	/// particular methods for purchase invoice
	///
	/// \brief getAllPurchaseInvoices
	/// get all purchase invoices stored in db
	/// \return list of PurchaseInvoice objects
	///
	virtual QList<core::PurchaseInvoice> getAllPurchaseInvoices() = 0;

	///
	/// \brief getPurchaseInvoice - get purchase invoice from db by ID
	/// \param purchaseInvoiceId - purchase invoice ID
	/// \return purchase invoice object
	///
	virtual core::PurchaseInvoice getPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId
	) = 0;

	///
	/// \brief hasPurchaseInvoiceCorrection
	/// check do exist correction document for purchase invoice
	/// \param purchaseInvoiceId - invoice to check
	/// \param useCache - use cache memory for faster process
	/// \return true if correction document exist for purchase invoice described by current ID
	///
	virtual bool hasPurchaseInvoiceCorrection(
		core::eloquent::DBIDKey purchaseInvoiceId,	///< [in]
		bool useCache = false						///< [in]
	) = 0;

	///
	/// \brief getCorrectionForSaleInvoice
	/// get purchase invoice correction for gave document
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::PurchaseInvoice getCorrectionForPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId	///< [in]
	) = 0;

	///
	/// \brief postPurchaseInvoice
	/// remove purchase invoice from buffer
	/// ftom this time users can't edit invoice
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual bool postPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId	///< [in]
	) = 0;

	///
	/// \brief getPurchaseInvoiceItems
	/// get all items for purchase invoice
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual QList<core::PurchaseInvoiceItem> getPurchaseInvoiceItems(
		core::eloquent::DBIDKey purchaseInvoiceId
	) = 0;

	///
	/// @brief create new invoice in data storage
	/// @param invoice invoice object to create
	/// @param items assigned items to invoice
	/// @return true if operation will be success
	///
	virtual bool createPurchaseInvoice(
		core::PurchaseInvoice& invoice			///< [in]
	) = 0;

	///
	/// @brief update existing object in data storage
	/// @param invoice 
	/// @param items 
	/// @return true if operation will be success
	///
	virtual bool updatePurchaseInvoice(
		core::PurchaseInvoice& invoice			///< [in]
	) = 0;
	///@}

	virtual bool lockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId)		= 0;
	virtual bool unlockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId)	= 0;
	virtual bool lockSaleInvoice(const core::eloquent::DBIDKey& invoiceId)			= 0;
	virtual bool unlockSaleInvoice(const core::eloquent::DBIDKey& invoiceId)		= 0;

	///@{
	/// cache operations

	void removeSaleInvoiceFromCache(const core::eloquent::DBIDKey& saleInvoiceId);
	void updateSaleInvoiceCache(const core::eloquent::DBIDKey& saleInvoiceId);
	void removePurchaseInvoiceFromCache(const core::eloquent::DBIDKey& purchaseInvoiceId);
	void updatePurchaseInvoiceCache(const core::eloquent::DBIDKey& purchaseInvoiceId);
	///@}

protected:
	///
	/// \brief m_saleInvoicesCache
	/// cache memory objects for sale and purchase invoices
	///
	std::unordered_map<core::eloquent::DBIDKey, core::SaleInvoice> m_saleInvoicesCache;
	std::unordered_map<core::eloquent::DBIDKey, core::eloquent::DBIDKey>  m_saleInvoiceCorrections;

	std::unordered_map<core::eloquent::DBIDKey, core::PurchaseInvoice> m_purchaseInvoicesCache;
	std::unordered_map<core::eloquent::DBIDKey, core::eloquent::DBIDKey> m_purchaseInvoiceCorrections;

	/////////////////////////////////////////////////////////////////////////
	/// it can be uses with sales and purchase invocies
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey invoiceId)
			: _invoiceId {invoiceId}
		{}

		// comparator for saleInvoice
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::SaleInvoice>& item) noexcept {
			return item.first == _invoiceId;
		}

		// comparator for purchase invoice
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::PurchaseInvoice>& item) noexcept {
			return item.first == _invoiceId;
		}

	private:
		core::eloquent::DBIDKey _invoiceId;
	};
	/////////////////////////////////////////////////////////////////////////
};

} // namespace core::repository

#endif // ABSTRACTINVOICEREPOSITORY_HPP
