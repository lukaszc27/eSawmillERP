#ifndef REMOTESERVICEREPOSITORY_HPP
#define REMOTESERVICEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractservicerepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteServiceRepository : public core::repository::AbstractServiceRepository {
	///
	/// \brief get
	/// \param serviceId
	/// \return
	///
	virtual core::Service get(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief getAll
	/// \return
	///
	virtual QList<core::Service> getAll() override;

	///
	/// \brief create
	/// \param service
	/// \param elements
	/// \param articles
	/// \return
	///
	virtual bool create(
		core::Service& service
	) override;

	///
	/// \brief update
	/// \param service
	/// \param elements
	/// \param articles
	/// \return
	///
	virtual bool update(
		const core::Service& service
	) override;

	///
	/// \brief getServiceElements
	/// \param serviceId
	/// \return
	///
	virtual QList<core::service::Element> getServiceElements(
		core::eloquent::DBIDKey serviceId
	) override;

	///
	/// \brief getServiceArticles
	/// \param serviceId
	/// \return
	///
	virtual QList<core::Article> getServiceArticles(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief destroy
	/// \param serviceId
	/// \return
	///
	virtual bool destroy(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief getFirstAvaiableDeadline
	/// \return
	///
	virtual QDate getFirstAvaiableDeadline() override;

	virtual bool lock(const core::eloquent::DBIDKey& serviceId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& serviceId) override;
};

}

#endif // REMOTESERVICEREPOSITORY_HPP
