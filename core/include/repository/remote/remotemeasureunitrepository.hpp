#ifndef REMOTEMEASUREUNITREPOSITORY_HPP
#define REMOTEMEASUREUNITREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractmeasureunitrepository.hpp"

namespace core::repository::remote {

struct RemoteMeasureUnitRepository : public core::repository::AbstractMeasureUnitRepository {
	///
	/// \brief getAll
	/// get all measure units from production server
	/// this is the same list as returned by DatabaseMeasureUnitRepository
	/// \return
	///
	virtual QList<core::MeasureUnit> getAll() override;

	///
	/// \brief get
	/// \param unitId
	/// \return
	///
	virtual core::MeasureUnit get(core::eloquent::DBIDKey unitId) override;
};

} // namespace core::repository::remote

#endif // REMOTEMEASUREUNITREPOSITORY_HPP
