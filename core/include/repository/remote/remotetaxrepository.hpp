#ifndef REMOTETaxREPOSITORY_HPP
#define REMOTETaxREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstracttaxrepository.hpp"
#include "../../dbo/tax.h"
#include <QList>

namespace core::repository::remote {

struct CORE_EXPORT RemoteTaxRepository : public core::repository::AbstractTaxRepository {
	///
	/// \brief getAll
	/// get tax list from database by remote server
	/// \return
	///
	virtual QList<core::Tax> getAll() override;

	///
	/// \brief get
	/// get tax object from database by primary key
	/// \param TaxId
	/// \return
	///
	virtual core::Tax get(core::eloquent::DBIDKey TaxId) override;

	///
	/// \brief getByValue
	/// get tax object from database by tax value
	/// \param value
	/// \return
	///
	virtual core::Tax getByValue(double value) override;

	///
	/// \brief create
	/// create new tax object in database by remote server
	/// \param tax
	/// \return
	///
	virtual bool create(core::Tax& tax) override;

	///
	/// \brief update
	/// update information about existing tax by remote server
	/// \param tax
	/// \return
	///
	virtual bool update(const core::Tax& tax) override;

	///
	/// \brief destroy
	/// remove tax from database by primary key
	/// \param taxId
	/// \return
	///
	virtual bool destroy(core::eloquent::DBIDKey taxId) override;
};

} // namespace core::repository::remote

#endif // REMOTETaxREPOSITORY_HPP
