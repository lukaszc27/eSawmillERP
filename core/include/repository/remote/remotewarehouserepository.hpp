#ifndef REMOTEWAREHOUSEREPOSITORY_HPP
#define REMOTEWAREHOUSEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractwarehouserepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteWarehouseRepository : public repository::AbstractWarehouseRepository {
	///
	/// \brief getAll
	/// get all warehouses via production center server
	/// \return
	///
	virtual QList<core::Warehouse> getAll() override;

	///
	/// \brief getWarehouse
	/// get one warehouse from production center by database identificator
	/// \param warehouseId
	/// \return
	///
	virtual core::Warehouse getWarehouse(core::eloquent::DBIDKey warehouseId) override;

	///
	/// \brief create
	/// create new warehouse via production center
	/// \param warehouse
	/// \return
	///
	virtual bool create(core::Warehouse& warehouse) override;

	///
	/// \brief update
	/// update information about existing warehouse in database by production center
	/// \param warehouse
	/// \return
	///
	virtual bool update(core::Warehouse& warehouse) override;

	///
	/// \brief destroy
	/// remove object from database by production center
	/// \param warehouseId
	/// \return
	///
	virtual bool destroy(const core::eloquent::DBIDKey& warehouseId) override;

	///
	/// \brief getArticles
	/// get all articles from warehouse by remote server
	/// \param warehouseId
	/// \return
	///
	virtual QList<core::Article> getArticles(core::eloquent::DBIDKey warehouseId) override;

	virtual bool lock(const core::eloquent::DBIDKey& warehouseId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& warehouseId) override;
};

}

#endif // REMOTEWAREHOUSEREPOSITORY_HPP
