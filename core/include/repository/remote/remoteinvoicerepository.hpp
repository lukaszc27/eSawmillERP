#ifndef REMOTEINVOICEREPOSITORY_HPP
#define REMOTEINVOICEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractinvoicerepository.hpp"


namespace core::repository::remote {

struct CORE_EXPORT RemoteInvoiceRepository : public core::repository::AbstractInvoiceRepository {
	///
	/// \brief getAllSaleInvoices
	/// get list of sale invoices from database by remote server
	/// \return
	///
	virtual QList<core::SaleInvoice> getAllSaleInvoices() override;

	///
	/// \brief getSaleInvoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::SaleInvoice getSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief hasSaleInvoiceCorrection
	/// \param saleInvoiceId
	/// \param useCache
	/// \return
	///
	virtual bool hasSaleInvoiceCorrection(core::eloquent::DBIDKey saleInvoiceId, bool useCache = false) override;

	///
	/// \brief getCorrectionForSaleInvoice
	/// get correction document for given invoice id
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::SaleInvoice getCorrectionForSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief hasSaleInvoiceAssignedOrder
	/// get order for invoice
	/// \param saleInvoiceId
	/// \param useCache
	/// \return
	///
	virtual bool hasSaleInvoiceAssignedOrder(core::eloquent::DBIDKey saleInvoiceId, bool useCache = false) override;

	///
	/// \brief getAssignedOrderForSaleInvoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::Order getAssignedOrderForSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief postSaleInvoice
	/// remove invoice from buffer
	/// from this time users can not edit invocie
	/// \param saleInvoiceId
	/// \return
	///
	virtual bool postSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief createSaleInvoice
	/// create new sale invoice in database and assign given items
	/// \param invoice
	/// \param items
	/// \return
	///
	virtual bool createSaleInvoice(
		core::SaleInvoice& invoice
	) override;

	///
	/// \brief updateSaleInvoice
	/// \param invoice
	/// \param items
	/// \return
	///
	virtual bool updateSaleInvoice(
		core::SaleInvoice& invoice
	) override;

	///
	/// \brief markSaleInvoiceAsCorrectionDocument
	/// assign correction docuemnt (correctionInvoiceId) to existing invoice with mistake (outside buffer) - orginal invoice id
	/// \param orginalInvoiceId
	/// \param correctionInvoiceId
	/// \return
	///
	virtual bool markSaleInvoiceAsCorrectionDocument(core::eloquent::DBIDKey orginalInvoiceId, core::eloquent::DBIDKey correctionInvoiceId) override;

	///
	/// \brief getSaleInvoiceItems
	/// get assigned items (articles or wood elements) to given invoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual QList<core::SaleInvoiceItem> getSaleInvoiceItems(core::eloquent::DBIDKey saleInvoiceId) override;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// methods for purchase invoices
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	///
	/// \brief getAllPurchaseInvoices
	/// get purchase invoices existing in system
	/// \return
	///
	virtual QList<core::PurchaseInvoice> getAllPurchaseInvoices() override;

	///
	/// \brief getPurchaseInvoice
	/// get purchase invoice by primary key
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual core::PurchaseInvoice getPurchaseInvoice(core::eloquent::DBIDKey purchaseInvoiceId) override;

	///
	/// \brief hasPurchaseInvoiceCorrection
	/// check existing correction document for given invoice
	/// correction document will be create when user make mistake and post invoice
	/// \param purchaseInvoideId
	/// \param useCache
	/// \return
	///
	virtual bool hasPurchaseInvoiceCorrection(core::eloquent::DBIDKey purchaseInvoideId, bool useCache = false) override;

	///
	/// \brief getCorrectionForPurchaseInvoice
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual core::PurchaseInvoice getCorrectionForPurchaseInvoice(core::eloquent::DBIDKey purchaseInvoiceId) override;

	///
	/// \brief postPurchaseInvocie
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual bool postPurchaseInvoice(core::eloquent::DBIDKey purchaseInvoiceId) override;

	///
	/// \brief getPurchaseInvoiceItems
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual QList<core::PurchaseInvoiceItem> getPurchaseInvoiceItems(core::eloquent::DBIDKey purchaseInvoiceId) override;

	///
	/// @brief create purchase invoice via remote service 
	/// @param invoice 
	/// @param items 
	/// @return
	/// 
	virtual bool createPurchaseInvoice(
		core::PurchaseInvoice& invoice
	) override;

	/// @brief update existing invoice in remote service
	/// @param invoice 
	/// @param items 
	/// @return 
	virtual bool updatePurchaseInvoice(
		core::PurchaseInvoice& invoice
	) override;

	virtual bool lockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool unlockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool lockSaleInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool unlockSaleInvoice(const core::eloquent::DBIDKey& invoiceId) override;
};

} // namespace core::repository::remote

#endif // REMOTEINVOICEREPOSITORY_HPP
