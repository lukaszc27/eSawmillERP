#ifndef REMOTEREPOSITORY_HPP
#define REMOTEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractrepository.hpp"


namespace core::repository::remote {

///
/// \brief The RemoteRepository class
/// remote repository is used to manage data with production server (control center)
///
struct CORE_EXPORT RemoteRepository : public core::repository::AbstractRepository {
	///
	/// \brief connect
	/// connect to data provider used by repository
	/// in this case connect to production center via sockets
	/// \return
	///
	bool connect(QObject* parent = nullptr) override final;

	///
	/// \brief disconnect
	/// disconnect from data provider and unregister from production service
	/// \return
	///
	bool disconnect() override final;

	///
	/// \brief userRepository
	/// \return instance of singelton to user repository to allow manage user data on server side (by production center)
	///
	virtual AbstractUserRepository& userRepository() const override;

	///
	/// \brief companiesRepository
	/// \return
	///
	virtual AbstractCompanyRepository& companiesRepository() const override;

	///
	/// \brief warehousesRepository
	/// \return instance of singelton to warehouse repository to allow manage warehouse data on server side (by production center server)
	///
	virtual AbstractWarehouseRepository& warehousesRepository() const override;

	///
	/// \brief TaxRepository
	/// \return instance of singelton to Tax repository to allow manage Taxs object on server side
	///
	virtual AbstractTaxRepository& TaxRepository() const override;

	///
	/// \brief woodTypeRepository
	/// \return instance of singelton to wood type repository
	///
	virtual AbstractWoodTypeRepository& woodTypeRepository() const override;

	///
	/// \brief articleTypeRepository
	/// \return instance of singelton to ArticleType repository to allow manage article type object on server side
	///
	virtual AbstractArticleRepository& articleRepository() const override;

	///
	/// \brief measureUnitRepository
	/// \return instance of singelton to MesaureUnit repository to allo manage measure units object
	///
	virtual AbstractMeasureUnitRepository& measureUnitRepository() const override;

	///
	/// \brief contractorRepository
	/// \return
	///
	virtual AbstractContractorRepository& contractorRepository() const override;

	///
	/// \brief invoiceRepository
	/// \return
	///
	virtual AbstractInvoiceRepository& invoiceRepository() const override;

	///
	/// \brief orderRepository
	/// \return
	///
	virtual AbstractOrderRepository& orderRepository() const override;

	///
	/// \brief serviceRepository
	/// \return
	///
	virtual AbstractServiceRepository& serviceRepository() const override;

	virtual AbstractCityRepository& cityRepository() const override;
};

} // namespace core::repository::remote

#endif // REMOTEREPOSITORY_HPP
