#ifndef REMOTEUSERREPOSITORY_HPP
#define REMOTEUSERREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractuserrepository.hpp"

namespace core::repository::remote {

///
/// \brief The RemoteUserRepository class
/// repository used to manage user data with external server
///
struct CORE_EXPORT RemoteUserRepository : public AbstractUserRepository {
	/// login user in app by external server
	///
	bool login(const QString& userName, const QString& password) override;
	void logout() override;

	/// get all users from external server
	///
	QList<core::User> getAllUsers() override;

	/// get user from external server
	///
	core::User getUser(const core::eloquent::DBIDKey& userId) override;

	/// create user on server side
	///
	bool create(core::User& user) override;

	/// update user on server side
	///
	bool update(core::User& user) override;

	/// destroy user from db (by server) and server cache
	///
	bool destroy(core::eloquent::DBIDKey& key) override;

	///
	/// \brief lock
	/// lock user before editing on server side
	/// it is nessesary to synchronize editing operation in system
	/// (users can not edit one user object in the same time)
	/// \param userId
	/// \return
	///
	bool lock(const core::eloquent::DBIDKey& userId) override;
	bool unlock(const core::eloquent::DBIDKey& userId) override;

	///@{
	/// user roles
	core::RoleCollection getAllRoles() override;
	///@}
};

} // namespace core::repository::remote

#endif // REMOTEUSERREPOSITORY_HPP
