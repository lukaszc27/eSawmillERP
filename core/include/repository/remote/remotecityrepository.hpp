#ifndef REMOTECITYREPOSITORY_HPP
#define REMOTECITYREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractcityrepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteCityRepository : public core::repository::AbstractCityRepository {
	///
	/// \brief get
	/// get city object from remote service
	/// \param cityId
	/// \return
	///
	core::City get(const core::eloquent::DBIDKey& cityId) override;

	///
	/// \brief getAll
	/// get all citties from data provider by remote service
	/// \return
	///
	QList<core::City> getAll() override;

	///
	/// \brief create
	/// create new city object
	/// \param city
	/// \return
	///
	bool create(core::City& city) override;
};

} // namespace core::repository::remote

#endif // REMOTECITYREPOSITORY_HPP
