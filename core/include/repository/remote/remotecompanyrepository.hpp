#ifndef REMOTECOMPANYREPOSITORY_HPP
#define REMOTECOMPANYREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractcompanyrepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteCompanyRepository : public AbstractCompanyRepository {
	///
	/// \brief update
	/// \param company
	/// \param contact
	/// \return
	///
	virtual bool update(core::Company& company) override;

	///
	/// \brief create
	/// \param company
	/// \param contact
	/// \return
	///
	virtual bool create(core::Company& company) override;

	///
	/// \brief getCompany
	/// \param userId
	/// \param company
	/// \param contact
	/// \return
	///
	virtual core::Company getCompany(core::eloquent::DBIDKey companyId) override;
};

} // namespace core::repository::remote

#endif // REMOTECOMPANYREPOSITORY_HPP
