#ifndef REMOTEORDERREPOSITORY_HPP
#define REMOTEORDERREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractorderrepository.hpp"

namespace core::repository::remote {

struct RemoteOrderRepository : public core::repository::AbstractOrderRepository {
	///
	/// \brief getAllOrders
	/// \return
	///
	virtual QList<core::Order> getAllOrders() override;

	///
	/// \brief getOrderElements
	/// \param orderId
	/// \return
	///
	virtual QList<core::order::Element> getOrderElements(core::eloquent::DBIDKey orderId) override;

	///
	/// \brief getArticlesForOrder
	/// \param orderId
	/// \return
	///
	virtual QList<core::Article> getArticlesForOrder(core::eloquent::DBIDKey orderId) override;

	///
	/// \brief getOrder
	/// \param orderId
	/// \return
	///
	virtual core::Order getOrder(core::eloquent::DBIDKey orderId) override;

	///
	/// \brief create
	/// \param order
	/// \param elements
	/// \param articles
	/// \return
	///
	virtual bool create(
		core::Order& order
	) override;

	///
	/// \brief update
	/// \param order
	/// \param elements
	/// \param articles
	/// \return
	///
	virtual bool update(
		core::Order& order
	) override;

	///
	/// \brief destroy
	/// \param orderId
	/// \return
	///
	virtual bool destroy(core::eloquent::DBIDKey orderId) override;

	///
	/// \brief getFirstAvaiableDeadline
	/// \return
	///
	virtual QDate getFirstAvaiableDeadline() override;

	/// \brief lock
	/// lock order before editing on server side
	/// \param userId
	/// \return
	virtual bool lock(const core::eloquent::DBIDKey& orderId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& orderId) override;
};

} // namespace core::repository::remote

#endif // REMOTEORDERREPOSITORY_HPP
