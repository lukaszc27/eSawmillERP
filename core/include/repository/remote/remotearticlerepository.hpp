#ifndef REMOTEARTICLEREPOSITORY_HPP
#define REMOTEARTICLEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractarticlerepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteArticleRepository : public core::repository::AbstractArticleRepository {
	///
	/// \brief getAll
	/// get all articles from warehouse
	/// \param warehouseId
	/// \return
	///
	virtual QList<core::Article> getAll(core::eloquent::DBIDKey warehouseId) override;

	///
	/// \brief getArticle
	/// get article by primary key in db
	/// \param articleId
	/// \return
	///
	virtual core::Article getArticle(core::eloquent::DBIDKey articleId) override;

	///
	/// \brief createArticle
	/// create new article object in database
	/// \param article
	/// \return
	///
	virtual bool createArticle(core::Article& article) override;

	///
	/// \brief destroyArticle
	/// remove article from database
	/// \param articleId
	/// \return
	///
	virtual bool destroyArticle(core::eloquent::DBIDKey articleId) override;

	///
	/// \brief updateArticle
	/// update information about existing article in database
	/// \param article
	/// \return
	///
	virtual bool updateArticle(const core::Article& article) override;

	///
	/// \brief getArticleTypes
	/// get all uses types for article
	/// \return
	///
	virtual QList<core::ArticleType> getArticleTypes() override;

	///
	/// \brief getArticleType
	/// get article type object from production server by unique id
	/// \param typeId
	/// \return
	///
	virtual core::ArticleType getArticleType(core::eloquent::DBIDKey typeId) override;

	virtual bool lock(const core::eloquent::DBIDKey& articleId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& articleId) override;
};

} // namespace core::repository::remote

#endif // REMOTEARTICLEREPOSITORY_HPP
