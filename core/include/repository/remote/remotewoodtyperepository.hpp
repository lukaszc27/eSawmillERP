#ifndef REMOTEWOODTYPEREPOSITORY_HPP
#define REMOTEWOODTYPEREPOSITORY_HPP

#include "../../core_global.h"
#include "../../dbo/woodtype.hpp"
#include "../abstractwoodtyperepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteWoodTypeRepository : public core::repository::AbstractWoodTypeRepository {
	///
	/// \brief getAll
	/// get wood type list from database by remote server (service)
	/// \return
	///
	virtual QList<core::WoodType> getAll() override;

	///
	/// \brief create
	/// create new object in database
	/// \param wood
	/// \return
	///
	virtual bool create(core::WoodType& wood) override;

	///
	/// \brief update
	/// update existing object in database
	/// \param wood
	/// \return
	///
	virtual bool update(core::WoodType& wood) override;

	///
	/// \brief destroy
	/// remove existing wood type from database by service
	/// \param key
	/// \return
	///
	virtual bool destroy(core::eloquent::DBIDKey& key) override;

	///
	/// \brief get
	/// get wood type object by service
	/// \param id
	/// \return
	///
	virtual core::WoodType get(core::eloquent::DBIDKey id) override;
};

} // namespace core::repository::remote

#endif // REMOTEWOODTYPEREPOSITORY_HPP
