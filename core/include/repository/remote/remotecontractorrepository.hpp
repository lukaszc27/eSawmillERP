#ifndef REMOTECONTRACTORREPOSITORY_HPP
#define REMOTECONTRACTORREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractcontractorrepository.hpp"

namespace core::repository::remote {

struct CORE_EXPORT RemoteContractorRepository : public core::repository::AbstractContractorRepository {
	///
	/// \brief get
	/// get contractor object from remote productions server
	/// \param contractorId
	/// \return
	///
	virtual core::Contractor get(core::eloquent::DBIDKey contractorId) override;

	///
	/// \brief getAll
	/// get list of all contractors registered in system via production center
	/// \return
	///
	virtual QList<core::Contractor> getAll() override;

	///
	/// \brief create
	/// create new contractor object
	/// \param contractor
	/// \return
	///
	virtual bool create(core::Contractor& contractor) override;

	///
	/// \brief update
	/// update information about existing contracotr in system
	/// \param contractor
	/// \return
	///
	virtual bool update(const core::Contractor& contractor) override;

	///
	/// \brief destroy
	/// remove existing contractor from db and system
	/// \param contractorId
	/// \return
	///
	virtual bool destroy(const core::eloquent::DBIDKey contractorId) override;

	/// \brief lock
	/// lock and unlock contractor on server side (before editing)
	/// \param contractorId
	/// \return
	virtual bool lock(const core::eloquent::DBIDKey& contractorId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& contractorId) override;
};

} // namespace core::reposiotry::remote

#endif // REMOTECONTRACTORREPOSITORY_HPP
