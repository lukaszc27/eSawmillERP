#ifndef ABSTRACTUSERREPOSITORY_HPP
#define ABSTRACTUSERREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/user.h"
#include "../dbo/role.hpp"
#include <unordered_map>
#include <QMutex>


namespace core::repository {

struct CORE_EXPORT AbstractUserRepository {
	///
	/// \brief login
	/// \param userName
	/// \param password
	/// \return
	///
	virtual bool login(const QString& userName, const QString& password) = 0;
	bool login(const core::User& user);

	///
	/// \brief logout current login user
	/// \return
	///
	virtual void logout() = 0;

	///
	/// \brief getAllUsers get all users
	/// \return list of users
	///
	virtual QList<core::User> getAllUsers() = 0;

	///
	/// \brief getUser
	/// \param userId
	/// \return
	///
	virtual core::User getUser(const core::eloquent::DBIDKey& userId) = 0;

	///
	/// \brief create new user
	/// \param user user data
	/// \param contact user address data
	/// \return true if user will be created
	///
	virtual bool create(core::User& user) = 0;

	///
	/// \brief update user (default the same as create method)
	/// \param user user data to update
	/// \return
	///
	virtual bool update(User& user) = 0;

	///
	/// \brief destroy - remove user from database
	/// \param key - primary key
	/// \return true if user will be removed
	///
	virtual bool destroy(core::eloquent::DBIDKey& key) = 0;

	///
	/// \brief autorizeUser current logged user
	/// \return
	///
	virtual core::User autorizeUser() const { return mCurrentUser; }

	////////////////////////////////////////////////////////////////
	/// user roles

	///
	/// \brief getAllRoles
	/// get all user roles existing in system
	/// \return
	///
	virtual RoleCollection getAllRoles() = 0;

	/// @brief get role from cache memory
	/// if cache memory is empty get all roles from source
	virtual Role getRole(const core::eloquent::DBIDKey& roleId);
	////////////////////////////////////////////////////////////////

	///
	/// \brief lock
	/// lock user object before to save before editing object
	/// (now uses only on server side operations by remote repository)
	/// \param userId - user to lock
	/// \return true if user will be lock (if was lock before by other system users return false)
	///
	virtual bool lock(const core::eloquent::DBIDKey& userId) {
		Q_UNUSED(userId);
		// we have to return true in this place because
		// if we return false users cannot edit users objects in database repository
		return true;
	}
	virtual bool unlock(const core::eloquent::DBIDKey& userId) {
		Q_UNUSED(userId);
		return true;
	}

	///@{
	/// cache operations

	///
	/// \brief updateCache
	/// update user in cache memory on client side
	/// \param userId
	///
	void updateCache(const core::eloquent::DBIDKey& userId);
	bool removeFromCache(const core::eloquent::DBIDKey& userId);
	///@}


	//////////////////////////////////////////////////////////////////////////
	/// comparators
	struct Comparator {
		explicit Comparator(const core::eloquent::DBIDKey& userId)
			: _userId {userId}
		{
		}

		bool operator()(const std::pair<core::eloquent::DBIDKey, core::User>& item) {
			return item.first == _userId;
		}

	private:
		core::eloquent::DBIDKey _userId;
	};

	/////////////////////////////////////////////////////////////////////////////

protected:
	// current logged user (if user is logged) or invalid object
	core::User mCurrentUser;
	///
	/// \brief m_usersCache
	/// cache memory for users
	///
	std::unordered_map<core::eloquent::DBIDKey, core::User> m_usersCache;
	/// @brief cache memory for user roles
	std::unordered_map<core::eloquent::DBIDKey, core::Role> m_roles_cache;
};

} // namespace core::repository

#endif // ABSTRACTUSERREPOSITORY_HPP
