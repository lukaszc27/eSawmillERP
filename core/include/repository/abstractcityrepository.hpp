#ifndef ABSTRACTCITYREPOSITORY_H
#define ABSTRACTCITYREPOSITORY_H

#include "../core_global.h"
#include "../dbo/city.hpp"
#include <unordered_map>

namespace core::repository {

///
/// \brief The CityRepository struct
/// interface for city repository (base class for database and remote repository)
///
struct CORE_EXPORT AbstractCityRepository {
	///
	/// \brief get
	/// get city object from repository by unique identificator
	/// \param cityId - db primary key
	/// \return valid city object
	///
	virtual core::City get(const core::eloquent::DBIDKey& cityId) = 0;

	///
	/// \brief getAll
	/// get all cities from repository
	/// \return
	///
	virtual QList<core::City> getAll() = 0;

	///
	/// \brief add
	/// add new city name to dictionary and save in database by repository
	/// \param name
	/// \return
	///
	virtual bool create(core::City& city) = 0;

	///
	/// \brief hasCity
	/// find in cache city by name
	/// CAUTION: this method don't get city from database if not exist in db
	/// if you want to get city from data providder yout have to override this method
	/// in inherited classes
	/// \param name
	/// \return true if city exist in repository
	///
	virtual bool hasCity(const QString& name);

protected:
	///
	/// \brief m_cache
	/// cache memory uses by repositories to avoid
	/// sending the same query to database or service
	///
	std::unordered_map<core::eloquent::DBIDKey, core::City> m_cache;
};

}

#endif // ABSTRACTCITYREPOSITORY_H
