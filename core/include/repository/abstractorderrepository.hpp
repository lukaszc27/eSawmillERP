#ifndef ABSTRACTORDERREPOSITORY_HPP
#define ABSTRACTORDERREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/order.h"
#include "../dbo/orderElements.h"
#include "dbo/article.h"
#include "abstractrepositorylocker.hpp"
#include <unordered_map>
#include <QReadWriteLock>

namespace core::repository {

///
/// \brief The AbstractOrderRepository struct
/// base struct to manage orders in app
///
struct CORE_EXPORT AbstractOrderRepository  : public AbstractRepositoryLocker {
	///
	/// @brief getAllOrders
	/// get all orders from db (without filtering)
	/// @return
	///
	virtual QList<core::Order> getAllOrders() = 0;

	///
	/// @brief getOrderElements
	/// @param orderId
	/// @return
	///
	virtual QList<core::order::Element> getOrderElements(
		core::eloquent::DBIDKey orderId
	) = 0;

	///
	/// @brief getArticlesForOrder
	/// get all articles assigned to order
	/// @param orderId
	/// @return
	///
	virtual QList<core::Article> getArticlesForOrder(
		core::eloquent::DBIDKey orderId
	) = 0;

	///
	/// @brief getOrder
	/// get information about order and create valid model
	/// @param orderId
	/// @return
	///
	virtual core::Order getOrder(
		core::eloquent::DBIDKey orderId
	) = 0;

	///
	/// @brief create - create new order is system
	/// @param order - order object to save
	/// @param elements - assigned elements to current order
	/// @param articles - assigned articles to current order
	/// @return
	///
	virtual bool create(
		core::Order& order
	) = 0;

	///
	/// @brief update - update existing order is system
	/// @param order - order object to save
	/// @param elements - assigned elements to current order
	/// @param articles - assigned articles to current order
	/// @return
	///
	virtual bool update(
		core::Order& order
	) = 0;

	///
	/// @brief destroy
	/// remove order from db with all assigned elements and articles
	/// @param orderId
	/// @return
	///
	virtual bool destroy(
		core::eloquent::DBIDKey orderId
	) = 0;

	///
	/// @brief getFirstAvaiableDeadline
	/// get compleed date for last exisitng order (not finished)
	/// @return
	///
	virtual QDate getFirstAvaiableDeadline() = 0;

	///@{
	/// cache operations
	/// @brief update order object in cache memory
	void updateOrderInCache(const core::eloquent::DBIDKey& orderId);

	/// @brief removeFromCache
	/// remove order from cache with all assigned items and articles
	/// @param orderId
	void removeFromCache(const core::eloquent::DBIDKey& orderId);
	///@}

protected:
	///
	/// \brief m_ordersCache
	/// containers for store cached object on client side
	///
	std::unordered_map<core::eloquent::DBIDKey, core::Order> m_orders_cache;			///< cache memory for order objects
	QReadWriteLock m_orders_cache_lock;

	/////////////////////////////////////////////////////////////////////////////////
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey id)
			: _orderId {id}
		{}

		// comparator for cache memory
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::Order>& item) noexcept {
			return item.first == _orderId;
		}

	private:
		core::eloquent::DBIDKey _orderId;
	};
	/////////////////////////////////////////////////////////////////////////////////
};

} // namespace core::repository

#endif // ABSTRACTORDERREPOSITORY_HPP
