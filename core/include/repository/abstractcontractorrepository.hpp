#ifndef ABSTRACTCONTRACTORREPOSITORY_HPP
#define ABSTRACTCONTRACTORREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/contractor.h"
#include "abstractrepositorylocker.hpp"
#include <unordered_map>
#include <QReadWriteLock>

namespace core::repository {

///
/// \brief The AbstractContractorRepository struct
/// manage contractors data in system
///
struct CORE_EXPORT AbstractContractorRepository : public core::repository::AbstractRepositoryLocker {
	///
	/// \brief get - get contractor from source
	/// \param contractorId
	/// \return valid contractor db object ready to use
	///
	virtual core::Contractor get(
		core::eloquent::DBIDKey contractorId
	) = 0;

	///
	/// \brief getAll - get all contractors (customers and providers)
	/// \return list of contractor object
	///
	virtual QList<core::Contractor> getAll() = 0;

	///
	/// \brief create - create new contractor
	/// \param contractor
	///
	virtual bool create(
		core::Contractor& contractor			///< [in]
	) = 0;

	///
	/// \brief update - update existing contractor in system
	/// \param contractor valid contractor object with assigned contact object
	/// \return standard boolean value
	///
	virtual bool update(
		const core::Contractor& contractor			///< [in]
	) = 0;

	///
	/// \brief destroy - virtual method to destroy contractor from source
	/// \return standard boolean value
	///
	virtual bool destroy(
		const core::eloquent::DBIDKey contractorId	///< [in]
	) = 0;

	///@{
	/// cache operations
	/// \brief updateCache update contractor object in cache memory
	/// \param contractorId
	///
	void updateCache(const core::eloquent::DBIDKey& contractorId);
	void removeFromCache(const core::eloquent::DBIDKey& contractorId);
	///@}

	///
	/// \brief checkDoExistNipNumberForContractor
	/// check do exist NIP assigned to contracor
	/// (try find contractor by nip number)
	/// \param nip
	/// \return
	///
	bool checkDoExistNipNumberForContractor(const QString& nip);

	/////////////////////////////////////////////////////////////////////////////
	/// Comparators
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey contractorId);

		// comparator for cache memory
		bool operator()(const std::pair<core::eloquent::DBIDKey, core::Contractor>& item) const noexcept;
	private:
		core::eloquent::DBIDKey _contractorId;
	};

	////////////////////////////////////////////////////////////////////////////////

protected:
	std::unordered_map<core::eloquent::DBIDKey, core::Contractor> m_cache;
	QReadWriteLock m_cache_lock;
};

}

#endif // ABSTRACTCONTRACTORREPOSITORY_HPP
