#ifndef DBCONTRACTORREPOSITORY_HPP
#define DBCONTRACTORREPOSITORY_HPP

#include "../abstractcontractorrepository.hpp"
#include "../../core_global.h"


namespace core::repository::database {

struct CORE_EXPORT ContractorRepository : public AbstractContractorRepository {
	///
	/// \brief get - get one contractor direct from db (from Contractors table)
	/// \param contractorId - existing id in table
	/// \return valid Contractor object representing one row from table
	///
	core::Contractor get(core::eloquent::DBIDKey contractorId) override;

	///
	/// \brief getAll - get all contractors direct from database
	/// \return
	///
	QList<core::Contractor> getAll() override;

	///
	/// \brief create - create new contractor direct in database
	/// \param contractor
	/// \return
	///
	bool create(core::Contractor& contractor) override;

	///
	/// \brief update - update existing contractor direct in db
	/// this method aslo update contact object
	/// \param contractor - valid contractor object with assigned contract obj
	/// \return
	///
	bool update(const core::Contractor& contractor) override;

	///
	/// \brief destroy - destroy contractor direct from database
	/// \param contractorId
	/// \return
	///
	bool destroy(const core::eloquent::DBIDKey contractorId) override;

	bool lock(const core::eloquent::DBIDKey& contractorId) override;
	bool unlock(const core::eloquent::DBIDKey& contractorId) override;
};

} // namespace core::repository::database

#endif // DBCONTRACTORREPOSITORY_HPP
