#ifndef DBINVOICEREPOSITORY_HPP
#define DBINVOICEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractinvoicerepository.hpp"


namespace core::repository::database {

struct CORE_EXPORT InvoiceRepository : public AbstractInvoiceRepository {
	///@{
	/// methods group to manage sale invoices
	/// for sale invoice and purchase invoice is one repository
	/// if you want to add next methods, use correct group please.
	/// it help us in future :)
	///
	/// \brief getAllSaleInvoices - get invoices direct from database
	/// \return
	///
	virtual QList<core::SaleInvoice> getAllSaleInvoices() override;

	///
	/// \brief getSaleInvoice - get invoice from db
	/// \param invoiceId - object id to load
	/// \return
	///
	virtual core::SaleInvoice getSaleInvoice(core::eloquent::DBIDKey invoiceId) override;

	///
	/// \brief checkSaleInvoiceCorrection
	/// \param saleInvoiceId
	/// \return
	///
	virtual bool hasSaleInvoiceCorrection(core::eloquent::DBIDKey saleInvoiceId, bool useCache = false) override;

	///
	/// \brief getCorrectionForSaleInvoice
	/// get correction docuemtn for sale invoice direct from database
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::SaleInvoice getCorrectionForSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief hasSaleInvoiceAssignedOrder
	/// \param saleInvoiceId
	/// \param useCache
	/// \return
	///
	virtual bool hasSaleInvoiceAssignedOrder(core::eloquent::DBIDKey saleInvoiceId, bool useCache = false) override;

	///
	/// \brief getAssignedOrderForSaleInvoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::Order getAssignedOrderForSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief postSaleInvoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual bool postSaleInvoice(core::eloquent::DBIDKey saleInvoiceId) override;

	///
	/// \brief createSaleInvoice
	/// create invoice direct in database without proxy server
	/// \param saleInvoice - invoice to save
	/// \param items - assigned items to invoice
	/// \return
	///
	virtual bool createSaleInvoice(
		core::SaleInvoice& saleInvoice
	) override;

	///
	/// \brief updateSaleInvoice
	/// update existing sale invoice in system
	/// and recreated all assigned items
	/// \param saleInvoice - invoice to update
	/// \param items - this items will be recreated
	/// \return true if operation will be success
	///
	virtual bool updateSaleInvoice(
		core::SaleInvoice& saleInvoice
	) override;

	///
	/// \brief markSaleInvoiceAsCorrectionDocument
	/// mark invoce as correction
	/// \param orignalInvoiceId - this document is left remove, and for it will have make correction document
	/// \param correctionInvoiceId - this is correction document for removed document (look at top)
	/// \return
	///
	virtual bool markSaleInvoiceAsCorrectionDocument(
		core::eloquent::DBIDKey orignalInvoiceId,
		core::eloquent::DBIDKey correctionInvoiceId
	) override;

	///
	/// \brief getSaleInvoiceItems
	/// get all items assigned to sale invoice
	/// \param saleInvoiceId
	/// \return
	///
	virtual QList<core::SaleInvoiceItem> getSaleInvoiceItems(
		core::eloquent::DBIDKey saleInvoiceId
	) override;
	///@}


	virtual QList<core::PurchaseInvoice> getAllPurchaseInvoices() override;
	///
	/// \brief getPurchaseInvoice - get purchase invoice from db by ID
	/// \param purchaseInvoiceId - purchase invoice ID
	/// \return purchase invoice object
	///
	virtual core::PurchaseInvoice getPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId
	) override;

	///
	/// \brief hasPurchaseInvoiceCorrection
	/// check do exist correction document for purchase invoice
	/// \param purchaseInvoiceId - invoice to check
	/// \param useCache - use cache memory for faster process
	/// \return true if correction document exist for purchase invoice described by current ID
	///
	virtual bool hasPurchaseInvoiceCorrection(
		core::eloquent::DBIDKey purchaseInvoiceId,	///< [in]
		bool useCache = false						///< [in]
	) override;

	///
	/// \brief getCorrectionForSaleInvoice
	/// get purchase invoice correction for gave document
	/// \param saleInvoiceId
	/// \return
	///
	virtual core::PurchaseInvoice getCorrectionForPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId	///< [in]
	) override;

	///
	/// \brief postPurchaseInvoice
	/// remove purchase invoice from buffer
	/// ftom this time users can't edit invoice
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual bool postPurchaseInvoice(
		core::eloquent::DBIDKey purchaseInvoiceId	///< [in]
	) override;

	///
	/// \brief getPurchaseInvoiceItems
	/// get all items for purchase invoice
	/// \param purchaseInvoiceId
	/// \return
	///
	virtual QList<core::PurchaseInvoiceItem> getPurchaseInvoiceItems(
		core::eloquent::DBIDKey purchaseInvoiceId
	) override;

	///
	/// @brief create purchase invoice direct in database
	/// @param invoice 
	/// @param items 
	/// @return
	/// 
	virtual bool createPurchaseInvoice(
		core::PurchaseInvoice& invoice
	) override;

	///
	/// @brief update existing invoice direct in database
	/// @param invoice 
	/// @param items 
	/// @return
	///
	virtual bool updatePurchaseInvoice(
		core::PurchaseInvoice& invoice
	) override;

	virtual bool lockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool unlockPurchaseInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool lockSaleInvoice(const core::eloquent::DBIDKey& invoiceId) override;
	virtual bool unlockSaleInvoice(const core::eloquent::DBIDKey& invoiceId) override;
};

} // namespace core::repository::database

#endif // DBINVOICEREPOSITORY_HPP
