#ifndef DBMEASUREUNITREPOSITORY_HPP
#define DBMEASUREUNITREPOSITORY_HPP

#include "../abstractmeasureunitrepository.hpp"
#include "../../core_global.h"

namespace core::repository::database {

struct CORE_EXPORT MeasureUnitRepository : public AbstractMeasureUnitRepository {
	///
	/// \brief getAll
	/// get all measure units direct from database
	/// \return list of valid measure unit objects
	///
	QList<core::MeasureUnit> getAll() override;

	///
	/// \brief get - get one record from MeasureUnit table direct from db
	/// \param unitId - unit id existing in db
	/// \return valid measure unit db object
	///
	core::MeasureUnit get(core::eloquent::DBIDKey unitId) override;
};

} // namespace core::repository::database

#endif // DBMEASUREUNITREPOSITORY_HPP
