#ifndef DBUSERREPOSITORY_HPP
#define DBUSERREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractuserrepository.hpp"


namespace core::repository::database {

struct CORE_EXPORT UserRepository final : public AbstractUserRepository {
	///
	/// \brief login - login user in app by raw sql query
	/// app work as single mode (can't work with other eSawmill apps in network
	/// \param userName - user name to login (nick)
	/// \param password - user password
	/// \return true if user will be found
	///
	bool login(const QString& userName, const QString& password) override;
	void logout() override;

	///
	/// \brief getAllUsers get all users from local database
	/// \return list of users in local database
	///
	QList<core::User> getAllUsers() override;
	core::User getUser(const core::eloquent::DBIDKey& userId) override;

	///
	/// \brief create create new user in local database
	/// \param user - user data to create
	/// \param contact - user address data
	/// \return
	///
	bool create(core::User& user) override;

	///
	/// \brief update
	/// \param user
	/// \return
	///
	bool update(core::User& user) override;

	///
	/// \brief destroy
	/// \param key
	/// \return
	///
	bool destroy(eloquent::DBIDKey& key) override;

	///////////////////////////////////////////////////////
	/// user roles

	///
	/// \brief getAllRoles
	/// get all roles existing in system
	/// \return
	///
	RoleCollection getAllRoles() override;
	///////////////////////////////////////////////////////
};

} // namespace core::repository::database

#endif // DBUSERREPOSITORY_HPP
