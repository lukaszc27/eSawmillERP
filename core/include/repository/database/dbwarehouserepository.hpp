#ifndef DBWAREHOUSEREPOSITORY_HPP
#define DBWAREHOUSEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractwarehouserepository.hpp"


namespace core::repository::database {

///
/// \brief The DatabaseWarehouseRepository class
/// manage warehouse data direct on SQL server
/// this repository is use when app work without production center (single mode)
///
struct CORE_EXPORT WarehouseRepository : public core::repository::AbstractWarehouseRepository {
	///
	/// \brief getAll get all warehouses direct from database
	/// \return list of warehouse objects
	///
	virtual QList<core::Warehouse> getAll() override;

	///
	/// \brief getWarehouse
	/// get warehouse from database by id
	/// \param id
	/// \return
	///
	virtual core::Warehouse getWarehouse(core::eloquent::DBIDKey id) override;

	///
	/// \brief create create new object direct in server (without production center)
	/// \param warehouse valid warehouse object who will be create
	/// \return boolean value
	///
	virtual bool create(core::Warehouse& warehouse) override;

	///
	/// \brief update
	/// \param warehouse
	/// \return
	///
	virtual bool update(core::Warehouse& warehouse) override;

	///
	/// \brief destroy
	/// \param warehouseId
	/// \return
	///
	virtual bool destroy(const core::eloquent::DBIDKey& warehouseId) override;

	///
	/// \brief getArticles - get all articles assigned to warehouse direct from database
	/// \param warehouseId - warehouse identificatior
	/// \return list of article db object
	///
	virtual QList<core::Article> getArticles(core::eloquent::DBIDKey warehouseId) override;

	virtual bool lock(const core::eloquent::DBIDKey& warehouseId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& warehouseId) override;
};

} // namespace core::repository::database

#endif // DBWAREHOUSEREPOSITORY_HPP
