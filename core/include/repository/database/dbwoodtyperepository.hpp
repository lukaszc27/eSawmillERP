#ifndef DBWOODTYPEREPOSITORY_HPP
#define DBWOODTYPEREPOSITORY_HPP

#include "../abstractwoodtyperepository.hpp"
#include "../../core_global.h"

namespace core::repository::database {

///
/// \brief The WoodTypeRepository struct
///
struct CORE_EXPORT WoodTypeRepository : public AbstractWoodTypeRepository {
	///
	/// \brief getAll - get object direcet from database
	/// \return list of WoodType elements
	///
	QList<core::WoodType> getAll() override;

	///
	/// \brief create - create new object direct in database
	/// \param wood - element to create
	/// \return
	///
	bool create(core::WoodType& wood) override;

	///
	/// \brief update - update existing object in database
	/// \param wood - element to update
	/// \return
	///
	bool update(core::WoodType& wood) override;

	///
	/// \brief destroy - destroy object from database
	/// \param key - object id to remove
	/// \return
	///
	bool destroy(core::eloquent::DBIDKey& key) override;

	///
	/// \brief get
	/// \param id
	/// \return
	///
	core::WoodType get(core::eloquent::DBIDKey id) override;
};

} // namespace core::repository::database

#endif // DBWOODTYPEREPOSITORY_HPP
