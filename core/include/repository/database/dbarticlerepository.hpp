#ifndef DBARTICLEREPOSITORY_HPP
#define DBARTICLEREPOSITORY_HPP

#include "../abstractarticlerepository.hpp"
#include "../../core_global.h"

namespace core::repository::database {

///
/// \brief The ArticleRepository struct
/// repository to manage article data on SQL server side
///
struct CORE_EXPORT ArticleRepository : public AbstractArticleRepository {
	///
	/// \brief getAll - get all articles from warehouse stored in db
	/// \param warehouseId - warehouse identificator where articles are stored
	/// \return list of valid article dbo objects
	///
	QList<core::Article> getAll(core::eloquent::DBIDKey warehouseId) override;

	///
	/// \brief getArticle - get article direct from database
	/// \param articleId - identificator used in database
	/// \return
	///
	core::Article getArticle(core::eloquent::DBIDKey articleId) override;

	///
	/// \brief createArticle - create new article direct in db
	/// \param article article to save
	/// \return boolean value
	///
	bool createArticle(core::Article& article) override;

	///
	/// \brief updateArticle - update existing article direct in db
	/// \param article
	/// \return
	///
	bool updateArticle(const core::Article& article) override;

	///
	/// \brief destroyArticle - remove existing article direct from db
	/// \param articleId
	/// \return
	///
	bool destroyArticle(core::eloquent::DBIDKey articleId) override;

	///
	/// \brief getArticleTypes
	/// get all types direct from database
	/// \return list of article type db object
	///
	QList<core::ArticleType> getArticleTypes() override;

	///
	/// \brief getArticleType - get type from db (ArticleTypes table)
	/// \param typeId
	/// \return valid db object (represent one row from ArticleTypes table)
	///
	core::ArticleType getArticleType(core::eloquent::DBIDKey typeId) override;

	/// \brief lock/unlock article before editin
	/// this feature will use ony in remote mode
	/// in database do nothing
	/// \param articleId
	/// \return
	bool lock(const core::eloquent::DBIDKey& articleId) override;
	bool unlock(const core::eloquent::DBIDKey& articleId) override;
};

} // namespace core::repository::database

#endif // DBARTICLEREPOSITORY_HPP
