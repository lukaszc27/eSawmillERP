#ifndef DBTaxREPOSITORY_H
#define DBTaxREPOSITORY_H

#include "../abstracttaxrepository.hpp"
#include "../../core_global.h"

namespace core::repository::database {

///
/// \brief The TaxRepository struct
/// specialised struct to manage data direct in database
/// without intermediary service
///
struct CORE_EXPORT TaxRepository : public AbstractTaxRepository {
	///
	/// \brief getAll - get all Taxs direct from database
	/// \return list of Taxs objects
	///
	QList<core::Tax> getAll() override;

	///
	/// \brief get - get one Tax object from database by id
	/// \details this method cache data between app and SQL server
	/// \param TaxId - id in db
	/// \return db object represet one row from Taxs table
	///
	core::Tax get(core::eloquent::DBIDKey TaxId) override;

	///
	/// \brief getByValue
	/// \param value
	/// \return
	///
	core::Tax getByValue(double value) override;

	///
	/// \brief create - create new Tax object in database
	/// \param Tax - Tax object to save
	/// \return true if operation will be success
	///
	bool create(core::Tax& Tax) override;

	///
	/// \brief update - update existing object in database
	/// \param Tax - object to update
	/// \return true if operation will be success
	///
	bool update(const core::Tax& Tax) override;

	///
	/// \brief destroy - remove object from database
	/// \param key - object id to remove
	/// \return boolean
	///
	bool destroy(core::eloquent::DBIDKey key) override;
};

} // namespace core::repository::database

#endif // DBTaxREPOSITORY_H
