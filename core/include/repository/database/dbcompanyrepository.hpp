#ifndef DBCOMPANYREPOSITORY_HPP
#define DBCOMPANYREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractcompanyrepository.hpp"
#include <dbo/company.h>
#include <dbo/contact.h>

namespace core::repository::database {

struct CORE_EXPORT CompanyRepository : public AbstractCompanyRepository {
	///
	/// \brief update company in database by raw sql query
	/// \param company
	/// \param contact
	/// \return
	///
	virtual bool update(core::Company& company) override;

	///
	/// \brief create company in database by raw sql query
	/// \param company
	/// \param contact
	/// \return
	///
	virtual bool create(core::Company& company) override;

	///
	/// \brief getCompany
	/// \param companyId
	/// \return
	///
	virtual core::Company getCompany(core::eloquent::DBIDKey companyId) override;
};

} // namespace core::repository::database

#endif // DBCOMPANYREPOSITORY_HPP
