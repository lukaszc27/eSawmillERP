#ifndef DBORDERREPOSITORY_HPP
#define DBORDERREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractorderrepository.hpp"

namespace core::repository::database {

struct CORE_EXPORT OrderRepository : public AbstractOrderRepository {
	///
	/// \brief getAllOrders
	/// get all orders from db (without filtering)
	/// \return
	///
	virtual QList<core::Order> getAllOrders() override;

	///
	/// \brief getOrderElements
	/// \param orderId
	/// \return
	///
	virtual QList<core::order::Element> getOrderElements(core::eloquent::DBIDKey orderId) override;

	///
	/// \brief getOrder
	/// get information about order and create valid model
	/// \param orderId
	/// \return
	///
	virtual core::Order getOrder(
		core::eloquent::DBIDKey orderId
	) override;

	///
	/// \brief getArticlesForOrder
	/// get all articles assigned to order
	/// \param orderId
	/// \return
	///
	virtual QList<core::Article> getArticlesForOrder(
		core::eloquent::DBIDKey orderId
	) override;

	///
	/// \brief create - create new order is system
	/// \param order - order object to save
	/// \param elements - assigned elements to current order
	/// \param articles - assigned articles to current order
	/// \return
	///
	virtual bool create(
		core::Order& order
	) override;

	///
	/// \brief update - update existing order is system
	/// \param order - order object to save
	/// \param elements - assigned elements to current order
	/// \param articles - assigned articles to current order
	/// \return
	///
	virtual bool update(
		core::Order& order
	) override;

	///
	/// \brief destroy
	/// remove order from db with all assigned elements and articles
	/// \param orderId
	/// \return
	///
	virtual bool destroy(
		core::eloquent::DBIDKey orderId
	) override;

	///
	/// \brief getFirstAvaiableDeadline
	/// get compleed date for last exisitng order (not finished)
	/// \return
	///
	virtual QDate getFirstAvaiableDeadline() override;

	/// \brief lock
	/// lock and unlock function to safe order on editing time
	/// unused in db repository (always return true)
	/// \param userId
	/// \return - true when user can start editing
	virtual bool lock(const core::eloquent::DBIDKey& orderId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& orderId) override;
};

} // namespace core::repository::database

#endif // DBORDERREPOSITORY_HPP
