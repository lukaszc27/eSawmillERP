#ifndef DATABASEREPOSITORY_HPP
#define DATABASEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractrepository.hpp"

namespace core::repository::database {

struct CORE_EXPORT DatabaseRepository : public core::repository::AbstractRepository {
	///
	/// \brief connect
	/// connect repository to data provider
	/// in this case connect direct to database
	/// \return standard bool value
	///
	virtual bool connect(QObject* parent = nullptr) override;

	///
	/// \brief disconnect
	/// unused on connected direct to database
	/// \return
	///
	virtual bool disconnect() override;

	///
	/// \brief userRepository
	/// singelton handle for users repository
	/// \return singelton of user repositoy
	///
	virtual AbstractUserRepository& userRepository() const override;

	///
	/// \brief companiesRepository
	/// singelton handle for companies repository
	/// \return singelton of company repository
	///
	virtual AbstractCompanyRepository& companiesRepository() const override;

	///
	/// \brief warehousesRepository
	/// singelton handle for warehouses repository
	/// this object allow to manage data for warehouses
	/// \return
	///
	virtual AbstractWarehouseRepository& warehousesRepository() const override;

	///
	/// \brief TaxRepository
	/// singelton handle for Taxs repository
	/// this object allow to manager data direct on database
	/// \return
	///
	virtual AbstractTaxRepository& TaxRepository() const override;

	///
	/// \brief woodTypeRepository
	/// singelton handle for wood types repository
	/// \return
	///
	virtual AbstractWoodTypeRepository& woodTypeRepository() const override;

	///
	/// \brief articleRepository
	/// \return
	///
	virtual AbstractArticleRepository& articleRepository() const override;

	///
	/// \brief measureUnitRepository
	/// singelton handle for measure unit repository
	/// \return
	///
	virtual AbstractMeasureUnitRepository& measureUnitRepository() const override;

	///
	/// \brief contractorRepository - manage contractor data direct in SQL database
	/// \return singelton to Contractor repository (reference)
	///
	virtual AbstractContractorRepository& contractorRepository() const override;

	///
	/// \brief invoiceRepository
	/// \return
	///
	virtual AbstractInvoiceRepository& invoiceRepository() const override;

	///
	/// \brief orderRepository
	/// \return
	///
	virtual AbstractOrderRepository& orderRepository() const override;

	///
	/// \brief serviceRepository
	/// \return
	///
	virtual AbstractServiceRepository& serviceRepository() const override;

	virtual AbstractCityRepository& cityRepository() const override;
};

} // namespace core::repository::database

#endif // DATABASEREPOSITORY_HPP
