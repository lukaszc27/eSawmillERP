#ifndef DBCITYREPOSITORY_HPP
#define DBCITYREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractcityrepository.hpp"

namespace core::repository::database {

///
/// \brief The CityRepository class
/// object to operate direct on database
///
struct CORE_EXPORT CityRepository : public AbstractCityRepository {
	///
	/// \brief get
	/// \param cityId
	/// \return
	///
	core::City get(const core::eloquent::DBIDKey& cityId) override;

	///
	/// \brief getAll
	/// \return
	///
	QList<core::City> getAll() override;

	///
	/// \brief create
	/// \param city
	/// \return
	///
	bool create(core::City& city) override;
};

}

#endif // DBCITYREPOSITORY_HPP
