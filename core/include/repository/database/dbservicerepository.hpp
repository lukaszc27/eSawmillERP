#ifndef DBSERVICEREPOSITORY_HPP
#define DBSERVICEREPOSITORY_HPP

#include "../../core_global.h"
#include "../abstractservicerepository.hpp"
#include "dbo/serviceelement.h"
#include "dbo/article.h"


namespace core::repository::database {

///
/// \brief The ServiceRepository struct
/// repository to manage data direct in database
/// is uses by app in single mode (app operate direct on db)
/// or multi workstation mode (with production center server)
///
struct CORE_EXPORT ServiceRepository : public AbstractServiceRepository {
	///
	/// \brief get
	/// get service from database
	/// \param serviceId - indetificator in db
	/// \return valid complete Service object
	///
	core::Service get(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief getAll
	/// get all services from database
	/// \return
	///
	QList<core::Service> getAll() override;

	///
	/// \brief create
	/// create new instance in source
	/// \param service - full service object to save in source
	/// \return
	///
	bool create(
		core::Service& service
	) override;

	///
	/// \brief update
	/// update existing service direct in db
	/// \param service
	/// \return
	///
	bool update(const core::Service& service) override;

	///
	/// \brief getServiceElements
	/// get service elements direct in database
	/// \param serviceId
	/// \return the same description in AbstractServiceRepository
	///
	QList<core::service::Element> getServiceElements(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief getServiceArticles
	/// get all assigned articles to service
	/// \param serviceId
	/// \return valid full list of articles objects
	///
	virtual QList<core::Article> getServiceArticles(
		core::eloquent::DBIDKey serviceId				///< [in]
	) override;

	///
	/// \brief destroy
	/// remove service from database and all assigned elements and articles
	/// \param serviceId
	/// \return
	///
	bool destroy(core::eloquent::DBIDKey serviceId) override;

	///
	/// \brief getFirstAvaiableDeadline
	/// get completed date for last existing service (not finished)
	/// \return
	///
	QDate getFirstAvaiableDeadline() override;

	/// \brief lock
	/// this methods are unused on database repository
	/// \param serviceId
	/// \return
	virtual bool lock(const core::eloquent::DBIDKey& serviceId) override;
	virtual bool unlock(const core::eloquent::DBIDKey& serviceId) override;

private:
	///
	/// \brief prepareService
	/// prepare full service object before return from repository
	/// uses in getAll and get methods
	/// \param service
	///
	void prepareService(core::Service& service);
};

} // namespace core::repository::database

#endif // DBSERVICEREPOSITORY_HPP
