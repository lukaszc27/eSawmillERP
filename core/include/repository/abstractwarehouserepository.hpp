#ifndef ABSTRACTWAREHOUSEREPOSITORY_HPP
#define ABSTRACTWAREHOUSEREPOSITORY_HPP

#include "../core_global.h"
#include "../dbo/wareHouse.h"
#include "../dbo/article.h"
#include "abstractrepositorylocker.hpp"
#include <QList>
#include <unordered_map>

namespace core::repository {

///
/// \brief The AbstractWarehouseRepository class
/// interface for repository manage warehouse data
///
struct CORE_EXPORT AbstractWarehouseRepository : public AbstractRepositoryLocker {
	///
	/// \brief getAll warehouses existing in app
	/// \return list of Warehouses object
	///
	virtual QList<core::Warehouse> getAll() = 0;

	///
	/// \brief getWarehouse get warehouse from source by id
	/// \param warehouseId - identificator
	/// \return valid dbo warehouse object
	///
	virtual core::Warehouse getWarehouse(
		core::eloquent::DBIDKey warehouseId			///< [in]
	) = 0;

	///
	/// \brief create - create new warehouse
	/// \param warehouse - object with valid data
	/// \return boolean (true if operation result will be success)
	///
	virtual bool create(
		core::Warehouse& warehouse			///< [in]
	) = 0;

	///
	/// \brief update - update existing warehouse object
	/// \param warehouse - valid object with data
	/// \return boolean (true if operation result will be success)
	///
	virtual bool update(
		core::Warehouse& warehouse			///< [in]
	) = 0;

	///
	/// \brief destroy - remove existing warehouse object
	/// \param warehouseId - warehouse identificator to remove
	/// \return
	///
	virtual bool destroy(
		const core::eloquent::DBIDKey& warehouseId	///< [in]
	) = 0;

	///
	/// \brief getArticles - get all articles from warehouse
	/// \param warehouseId warehouse identificatior
	/// \return list of Article objects
	///
	virtual QList<core::Article> getArticles(
		core::eloquent::DBIDKey warehouseId			///< [in]
	) = 0;

	///@{
	/// operation on cache memory

	void updateCache(const core::eloquent::DBIDKey& warehouseId);
	void removeFromCache(const core::eloquent::DBIDKey& warehouseId);

	///@}

protected:
	///
	/// \brief m_cache
	/// cache memory for warehouse objects
	///
	std::unordered_map<core::eloquent::DBIDKey, core::Warehouse> m_cache;
};

} // namespace core::repository

#endif // ABSTRACTWAREHOUSEREPOSITORY_HPP
