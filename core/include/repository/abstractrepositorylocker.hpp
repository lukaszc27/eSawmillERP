#ifndef ABSTRACTREPOSITORYLOCKER_H
#define ABSTRACTREPOSITORYLOCKER_H

#include "../core_global.h"
#include "../eloquent/model.hpp"

namespace core::repository {

///
/// \brief The AbstractRepositoryLocker struct
/// interface to other repositories where before editing resource schould be blocked
/// it is important if you want to use ResourceLocker object who implements RAII pattern
///
struct CORE_EXPORT AbstractRepositoryLocker
{
	virtual bool lock(const core::eloquent::DBIDKey& resourceId) = 0;
	virtual bool unlock(const core::eloquent::DBIDKey& resourceId) = 0;
};

///
/// \brief The ResourceLocker struct
/// resource locker object implement RAII pattern
/// (block resource in constructor and unlock in destructor)
/// you schould use it when you like use try...catch blocks
/// (or if you have very bad memory  and often forgot use unlock method;) )
///
struct CORE_EXPORT ResourceLocker {
	explicit ResourceLocker(const core::eloquent::DBIDKey& resourceId, core::repository::AbstractRepositoryLocker& repository)
		: _repository {repository}
		, _resourceId {resourceId}
	{
		_repository.lock(_resourceId);
	}

	~ResourceLocker() {
		_repository.unlock(_resourceId);
	}

private:
	core::repository::AbstractRepositoryLocker& _repository;
	core::eloquent::DBIDKey _resourceId;
};

}

#endif // ABSTRACTREPOSITORYLOCKER_H
