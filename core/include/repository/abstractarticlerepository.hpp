#ifndef ABSTRACTARTICLEREPOSITORY_HPP
#define ABSTRACTARTICLEREPOSITORY_HPP

#include "core_global.h"
#include "dbo/article.h"
#include <unordered_map>
#include <QReadWriteLock>
#include <repository/abstractrepositorylocker.hpp>

namespace core::repository {

///
/// \brief The AbstractArticleRepository struct
/// interface for repository to manage article data in app
///
struct CORE_EXPORT AbstractArticleRepository : public AbstractRepositoryLocker {
	///
	/// \brief getAll - get all articles from source arrached to warehouse
	/// \param warehouseId - warehouse id where are stored articles
	/// \return
	///
	virtual QList<core::Article> getAll(
		core::eloquent::DBIDKey warehouseId	///< [in]
	) = 0;

	///
	/// \brief getArticle - get article by id from source
	/// \param articleId - identificator used in source (db)
	/// \return valid article object
	///
	virtual core::Article getArticle(
		core::eloquent::DBIDKey articleId	///< [in]
	) = 0;

	///
	/// \brief createArticle - add new article to warehouse
	/// \param article article to add
	/// \return boolean
	///
	virtual bool createArticle(
		core::Article& article			///< [in]
	) = 0;

	///
	/// \brief destroyArticle - remove existing article
	/// \param articleId
	/// \return
	///
	virtual bool destroyArticle(
		core::eloquent::DBIDKey articleId	///< [in]
	)= 0;

	///
	/// \brief updateArticle - update existing article
	/// \param article - valid article object
	/// \return boolean value
	///
	virtual bool updateArticle(
		const core::Article& article
	) = 0;

	///
	/// \brief getArticleTypes get all existing article types
	/// \return list of db object (ArticleType)
	///
	virtual QList<core::ArticleType> getArticleTypes() = 0;

	///
	/// \brief getArticleType - get article type by identificator from source
	/// \param typeId
	/// \return valid article type db object
	///
	virtual core::ArticleType getArticleType(
		core::eloquent::DBIDKey typeId		///< [in]
	) = 0;

	///@{
	/// cache operations
	void updateCache(const core::eloquent::DBIDKey& articleId);
	void removeFromCache(const core::eloquent::DBIDKey& articleId);
	///@}

	////////////////////////////////////////////////////////////////////
	/// comparators
	struct Comparator {
		explicit Comparator(core::eloquent::DBIDKey articleId)
			: _articleId {articleId}
		{}

		bool operator()(const std::pair<core::eloquent::DBIDKey, core::Article>& row) {
			return row.first == _articleId;
		}

	private:
		core::eloquent::DBIDKey _articleId;
	};

	////////////////////////////////////////////////////////////////////

protected:
	/// \brief m_articleTypeCache
	/// cache memory only for article types
	std::unordered_map<core::eloquent::DBIDKey, core::ArticleType> m_articleTypeCache;
	QReadWriteLock m_articleTypeLock;

	/// \brief m_articleCache
	/// cache memory for articles
	std::unordered_map<core::eloquent::DBIDKey, core::Article> m_articleCache;
	QReadWriteLock m_articleCacheLock;
};

}

#endif // ABSTRACTARTICLEREPOSITORY_HPP
