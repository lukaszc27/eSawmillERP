#ifndef ABSTRACTREPOSITORY_HPP
#define ABSTRACTREPOSITORY_HPP

#include "../core_global.h"
#include <memory>
#include <QProgressDialog>
#include "abstractuserrepository.hpp"
#include "abstractcompanyrepository.hpp"
#include "abstractwarehouserepository.hpp"
#include "abstracttaxrepository.hpp"
#include "abstractwoodtyperepository.hpp"
#include "abstractmeasureunitrepository.hpp"
#include "abstractarticlerepository.hpp"
#include "abstractcontractorrepository.hpp"
#include "abstractinvoicerepository.hpp"
#include "abstractorderrepository.hpp"
#include "abstractservicerepository.hpp"
#include "abstractcityrepository.hpp"


namespace core::repository {

struct CORE_EXPORT AbstractRepository {
	///
	/// \brief connect connect to data provider used by repository
	/// \return
	///
	virtual bool connect(QObject* parent = nullptr) = 0;

	///
	/// \brief disconnect
	/// disconnect from data provider
	/// \return
	///
	virtual bool disconnect() = 0;

	///
	/// \brief loadRepositories
	/// load startup data into cache memory
	/// \return
	///
	virtual void loadRepositories(std::shared_ptr<QProgressDialog>& dialog);

	///
	/// \brief userRepository
	/// \return
	///
	virtual AbstractUserRepository& userRepository() const = 0;

	///
	/// \brief companiesRepository
	/// \return
	///
	virtual AbstractCompanyRepository& companiesRepository() const = 0;

	///
	/// \brief warehousesRepository - return singelton to warehouse repository
	/// \return
	///
	virtual AbstractWarehouseRepository& warehousesRepository() const = 0;

	///
	/// \brief TaxRepository - return singelton to Taxs repository
	/// \return
	///
	virtual AbstractTaxRepository& TaxRepository() const = 0;

	///
	/// \brief woodTypeRepository - return singelton to wood types repository
	/// \return
	///
	virtual AbstractWoodTypeRepository& woodTypeRepository() const = 0;

	///
	/// \brief articleTypeRepository
	/// \return
	///
	virtual AbstractArticleRepository& articleRepository() const = 0;

	///
	/// \brief measureUnitReposytory
	/// \return
	///
	virtual AbstractMeasureUnitRepository& measureUnitRepository() const = 0;

	///
	/// \brief contractorRepository - manage contracors data
	/// \return
	///
	virtual AbstractContractorRepository& contractorRepository() const = 0;

	///
	/// \brief invoiceRepository
	/// \return
	///
	virtual AbstractInvoiceRepository& invoiceRepository() const = 0;

	///
	/// \brief orderRepository
	/// \return
	///
	virtual AbstractOrderRepository& orderRepository() const = 0;

	///
	/// \brief serviceRepository
	/// \return
	///
	virtual AbstractServiceRepository& serviceRepository() const = 0;

	///
	/// \brief cityRepository
	/// return current uses city repository
	/// \return
	///
	virtual AbstractCityRepository& cityRepository() const = 0;

protected:
	///@{
	/// additional repository object

	/// \brief type of connections served by app
	enum class ConnectionType {
		Local,	///< use local database to manage data (DatabaseRepository)
		Remote	///< use producton center to manage data (RemoteRepository)
	};
	ConnectionType m_connectionType;	///< current connection type
	///@}
};

} // namespace core::repository

#endif // ABSTRACTREPOSITORY_HPP
