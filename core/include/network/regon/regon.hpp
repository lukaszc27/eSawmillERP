#ifndef REGON_HPP
#define REGON_HPP

#include "../../core_global.h"
#include "../../eloquent/model.hpp"
#include <QObject>
#include <QtNetwork>
#include <QtXml>


namespace core::network {

///
/// \brief The Regon class
/// communicate with REGON GUS API and get
/// data about contractors by NIP
/// + used protocol: SOAP
///
class CORE_EXPORT Regon : public QObject {
	Q_OBJECT

	///
	/// \brief The Request class
	/// template class for all request
	/// used in Regon class
	///
	class Request : public QNetworkRequest {
	public:
		explicit Request();
	};

public:
	Regon(QObject* parent = nullptr);
	virtual ~Regon();

	static QByteArray host();
	static QByteArray userKey() { return "df7045eaddd14e21bfcf"; }

	///
	/// \brief login - login user in REGON GUS and get token used when
	/// we want to get data about contrator
	/// \param userKey - user access key to REGON GUS
	/// \return true if user will have authorisation
	///
	bool login(const QByteArray &userKey = QByteArray("abcde12345abcde12345"));

	///
	/// \brief logout  - logout user in REGON GUS server side
	///
	void logout();

	///
	/// \brief searchEntities - find contractor by nip number
	/// \param nip
	///
	void searchEntities(const QString& nip);

private Q_SLOTS:
	void processLoginRequest(QNetworkReply* reply);
	void processLogoutRequest(QNetworkReply* reply);
	void processSearchRequest(QNetworkReply* reply);

Q_SIGNALS:
	///
	/// \brief userIsLogged emited when user in logged to service
	/// \param token
	///
	void userIsLogged(const QByteArray& token);

	///
	/// \brief userIsLogout - emited when user is logout from service
	///
	void userIsLogout();

	///
	/// \brief finished - emited when user was found by service
	/// <dane>
	///	   <Regon>852721748</Regon>
	///    <Nip>7381081570</Nip>
	///    <StatusNip />
	///    <Nazwa>XXXXXXXX ZOFIA Sklep Zielarsko-Medyczny</Nazwa>
	///    <Wojewodztwo>MAŁOPOLSKIE</Wojewodztwo>
	///    <Powiat>gorlicki</Powiat>
	///    <Gmina>Lipinki</Gmina>
	///    <Miejscowosc>Lipinki</Miejscowosc>
	///    <KodPocztowy>38-305</KodPocztowy>
	///    <NrNieruchomosci>XX</NrNieruchomosci>
	///    <NrLokalu />
	///    <Typ>F</Typ>
	///    <SilosID>1</SilosID>
	///    <DataZakonczeniaDzialalnosci />
	///    <MiejscowoscPoczty>Lipinki</MiejscowoscPoczty>
	///  </dane>
	///
	void finished(QDomElement& element);

private:
	bool mUserIsLogged;	// true if user is login (login function set this value)
	QByteArray mToken;	// token used to access contractor data
};

} // namespace core


#endif // REGON_HPP
