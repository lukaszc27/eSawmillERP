#ifndef SQUAREELEMENTS_CORE_H
#define SQUAREELEMENTS_CORE_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "woodtype.hpp"


namespace core {

struct CORE_EXPORT Order;

namespace order {

struct CORE_EXPORT Element : public eloquent::Model<Element> {
	///@{
	/// ctors
	explicit Element();
	///@}

	static QString tableName() {
		return "OrderElements";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	core::Order order(bool forceUpdate = false) const;
	double stere(double sparedLength = 0) const;

	// getters and setters
	double width() const				{ return get("Width").toDouble(); }
	double height() const				{ return get("Height").toDouble(); }
	double length() const				{ return get("Length").toDouble(); }
	double quantity() const				{ return get("Quantity").toDouble(); }
	bool planned() const				{ return get("Planed").value<bool>(); }
	QString name() const				{ return get("Name").toString(); }
	core::eloquent::DBIDKey orderId() const { return get("OrderId").toInt(); }

	void setWidth(double width)			{ set("Width", width); }
	void setHeight(double height)		{ set("Height", height); }
	void setLength(double length)		{ set("Length", length); }
	void setQuantity(double qty)		{ set("Quantity", qty); }
	void setPlanned(bool planned)		{ set("Planed", planned); }
	void setName(const QString& name)	{ set("Name", name); }

	WoodType& woodType() { return _woodType; }
	WoodType  woodType() const { return _woodType; }
	void setWoodType(const WoodType& woodtype);
	void setWoodType(core::eloquent::DBIDKey woodTypeId);

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::order::Element& element) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::order::Element& element) noexcept;
	///@}

private:
	WoodType _woodType;
};

} // namespace order
} //namespace core


//#include "orderElements.tpp"

#endif // SQUAREELEMENTS_CORE_H
