#ifndef SERVICES_H
#define SERVICES_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDate>
#include <QDomElement>

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "../dbo/tax.h"
#include "../dbo/contractor.h"
#include "../dbo/company.h"
#include "../dbo/user.h"
#include "../dbo/serviceelement.h"
#include "../dbo/servicearticle.h"


namespace core {

///
/// \brief The Service struct
/// object describe one record in Services database
/// schould be use to operate on data in db
/// inherit from Model struct who is paterrned on Eloquent ORM from Laravel framework
///
struct CORE_EXPORT Service : public eloquent::Model<Service> {
	///@{
	/// ctors
	explicit Service();
	///@}

	static QString tableName() {
		return "Services";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief The Priority enum
	/// priorities for services
	///
	enum class Priority {
		Low,
		Normal,
		High
	};

	bool createRecord(const Model<Service>& model) override;
	bool destroyRecord(const Model<Service>& model) override;

	void setUser(core::eloquent::DBIDKey userId) { set("UserAddId", userId); }
	void setUser(core::User const& user) { setUser(user.id()); }
	void setCompany(core::eloquent::DBIDKey comanyId) { set("CompanyId", comanyId); }
	void setCompany(core::Company const& company) { setCompany(company.id()); }

	///@{
	/// getters and setters for field in db
	QString name() const							{ return get("Name").toString(); }
	QString number() const							{ return get("Number").toString(); }
	QDate addDate() const							{ return get("AddDate").toDate(); }
	QDate endDate() const							{ return get("EndDate").toDate(); }
	bool isRealised() const							{ return get("IsRealised").toBool(); }
	double rebate() const							{ return get("Rebate").toDouble(); }
	Priority priority() const						{ return static_cast<Priority>(get("Priority").toInt()); }
	double price() const							{ return get("Price").toDouble(); }
	QString descripion() const						{ return get("Description").toString(); }

	void setName(const QString& name)				{ set("Name", name.toUpper()); }
	void setNumber(const QString& number)			{ set("Number", number.toUpper()); }
	void setAddDate(const QDate& date)				{ set("AddDate", date); }
	void setEndDate(const QDate& date)				{ set("EndDate", date); }
	void setRealised(bool completed)				{ set("IsRealised", completed); }
	void setRebate(double rebate)					{ set("Rebate", rebate); }
	void setPriority(Priority priority)				{ set("Priority", static_cast<int>(priority)); }
	void setPrice(double price)						{ set("Price", price); }
	void setDescription(const QString& description)	{ set("Description", description.toUpper()); }
	///@}

	Contractor& contractor() { return _contractor; }
	Contractor contractor() const { return _contractor; }
	void setContractor(core::eloquent::DBIDKey contractorId);
	void setContractor(const Contractor& contractor);

	core::Tax& Tax() { return _tax; }
	core::Tax  Tax() const { return _tax; }
	void setTax(core::eloquent::DBIDKey taxId);
	void setTax(const core::Tax& tax);

	void setElements(const QList<core::service::Element>& elements) { _elements = elements; }
	QList<core::service::Element>& elements()		{ return _elements; }
	QList<core::service::Element> elements() const	{ return _elements; }

	void setArticles(const QList<core::Article>& articles);
	void setArticles(const QList<core::service::Article>& articles) { _articles_clip = articles; }
	QList<core::service::Article>& articles()		{ return _articles_clip; }
	QList<core::service::Article> articles() const	{ return _articles_clip; }

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Service& service) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Service& service) noexcept;
	///@}

private:
	core::Contractor				_contractor;
	core::Tax						_tax;
	QList<core::service::Element>	_elements;
	QList<core::service::Article>	_articles_clip;
};

} // namespace core

#endif // SERVICES_H
