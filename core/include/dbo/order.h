#ifndef ORDER_CORE_H
#define ORDER_CORE_H

#include "core_global.h"
#include "contractor.h"
#include "tax.h"
#include "user.h"
#include "orderarticle.h"
#include "orderElements.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "saleinvoice.hpp"
#include <QDate>


namespace core {

struct Contractor;
struct Tax;

struct CORE_EXPORT Order : public eloquent::Model<Order> {
	///@{
	/// ctors
	explicit Order();
	///@}

	static QString tableName() {
		return "Orders";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	bool createRecord(const Model<Order>& model) override;
	bool destroyRecord(const Model<Order>& model) override;

	void setUser(eloquent::DBIDKey userId)				{ set("UserAddId", userId); }
	void setUser(const User& user)						{ setUser(user.id()); }
	void setCompany(eloquent::DBIDKey companyId)		{ set("CompanyId", companyId); }
	void setCompany(const Company& company)				{ setCompany(company.id()); }

	// getters and setters
	void setSpareLength(double spare)				{ set("SpareLength", spare); }
	double spareLength() const						{ return get("SpareLength").value<double>(); }
	void setPrice(double price)						{ set("Price", price); }
	void setRebate(double rebate)					{ set("Rebate", rebate); }
	double rebate() const							{ return get("Rebate").toDouble(); }
	QString name() const							{ return get("Name").toString(); }
	QDate addDate() const							{ return get("AddDate").toDate(); }
	QDate endDate() const							{ return get("EndDate").toDate(); }
	bool isRealised() const							{ return get("IsRealised").toBool(); }
	int priority() const							{ return get("Priority").toInt(); }
	double price() const							{ return get("Price").toDouble(); }
	QString description() const						{ return get("Description").toString(); }
	QString number() const							{ return get("Number").toString(); }
	core::eloquent::DBIDKey userAddId() const		{ return get("UserAddId").toInt(); }
	core::eloquent::DBIDKey companyId() const		{ return get("CompanyId").toInt(); }

	void setName(const QString& name)				{ set("Name", name.toUpper()); }
	void setAddDate(const QDate& date)				{ set("AddDate", date); }
	void setEndDate(const QDate& date)				{ set("EndDate", date); }
	void setRealised(bool realised)					{ set("IsRealised", realised); }
	void setPriority(int priority)					{ set("Priority", priority); }
	void setDescription(const QString& description)	{ set("Description", description.toUpper()); }
	void setNumber(const QString& number)			{ set("Number", number); }

	///
	/// \brief setContractor
	/// assign existing contractor object
	/// \param contractor
	///
	void setContractor(const core::Contractor& contractor);
	void setContractor(core::eloquent::DBIDKey contractorId);
	core::Contractor& contractor() { return _contractor; }
	core::Contractor contractor() const { return _contractor; }

	void setTax(const core::Tax& Tax);
	void setTax(core::eloquent::DBIDKey TaxId);
	core::Tax& Tax() { return _tax; }
	core::Tax  Tax() const { return _tax; }

	void setElements(const QList<core::order::Element>& elements)
		{ _elements = elements; }
	QList<core::order::Element>& elements()
		{ return _elements; }
	QList<core::order::Element> elements() const
		{ return _elements; }

	void setArticles(const QList<core::Article>& articles);
	void setArticles(const QList<core::order::Article>& articles)
		{ _articles_clip = articles; }
	QList<core::order::Article>& articles()
		{ return _articles_clip; }
	QList<core::order::Article> articles() const
		{ return _articles_clip; }

	///
	/// \brief hasInvoices - checking do orders has created invoice before
	/// \warning use only in db repository - this function operate direct in db
	/// \param forceUpdate
	/// \return true if order has invoices
	///
	bool hasInvoices(bool forceUpdate = false) const;

	///
	/// \brief invoice - returned assigned sale invoice to order
	/// \warning use only by db repository
	/// \param forceUpdate
	/// \return sale invoice object
	///
	SaleInvoice invoice(bool forceUpdate = false) const;

	///@{
	/// serialize and deserialize
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Order& order) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Order& order) noexcept;
	///@}

	operator bool() const noexcept {
		return id() > 0;
	}

private:
	Contractor	_contractor;
	core::Tax	_tax;
	QList<core::order::Element>	_elements;
	// clip for articles associated into order
	QList<core::order::Article> _articles_clip;
};

} // namespace core

#endif // ORDER_CORE_H
