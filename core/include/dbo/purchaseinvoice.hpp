#ifndef PURCHASEINVOICE_HPP
#define PURCHASEINVOICE_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QDate>
#include "contractor.h"
#include "wareHouse.h"
#include "purchaseinvoiceitem.hpp"


namespace core {

///
/// \brief The PurchaseInvoice struct
/// base struct describe purchase invoice
///
struct CORE_EXPORT PurchaseInvoice : public eloquent::Model<PurchaseInvoice> {
	///@{
	/// ctors
	explicit PurchaseInvoice();
	///@}

	///
	/// \brief createRecord - create new record in db
	/// this method is override because before save we need generate new invoice unique number
	/// \param model - invoice model object to save
	/// \return
	///
	bool createRecord(const Model<PurchaseInvoice>& model) override final;

	///
	/// \brief tableName
	/// return table name (in db) where object will be saved/updated
	/// \return
	///
	static QString tableName() { return "PurchaseInvoices"; }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// getters and setters
	void setDocumentPrefix(const QString& prefix)	{ mPrefix = prefix; }
	QString prefix() const							{ return mPrefix; }

	QString number() const							{ return get("Number").toString(); }
	void setNumber(const QString& number)			{ set("Number", number); }

	QString foreignNumber() const					{ return get("ForeignNumber").toString(); }
	void setForeignNumber(const QString& number)	{ set("ForeignNumber", number); }

	QDate dateOfReceipt() const					{ return get("DateOfReceipt").toDate(); }
	void setDateOfReceipt(const QDate& date)	{ set("DateOfReceipt", date); }

	QDate dateOfIssue() const					{ return get("DateOfIssue").toDate(); }
	void setDateOfIssue(const QDate& date)		{ set("DateOfIssue", date); }

	QDate dateOfPurchase() const				{ return get("DateOfPurchase").toDate(); }
	void setDateOfPurchase(const QDate& date)	{ set("DateOfPurchase", date); }

	double rebate() const					{ return get("Rebate").toDouble(); }
	void setRebate(double val)				{ set("Rebate", val); }

	QString payment() const					{ return get("Payment").toString(); }
	void setPayment(const QString& payment) { set("Payment", payment); }

	int paymentDeadlineDays() const			{ return get("PaymentDeadlineDays").toUInt(); }
	void setPaymentDeadlineDays(int days)	{ set("PaymentDeadlineDays", days); }

	QDate paymentDeadlineDate() const				{ return get("PaymentDeadlineDate").toDate(); }
	void setPaymentDeadlineDate(const QDate& date)	{ set("PaymentDeadlineDate", date); }

	double nettoPrice() const			{ return get("NettoPrice").toDouble(); }
	void setNettoPrice(double price)	{ set("NettoPrice", price); }

	double totalPrice() const			{ return get("TotalPrice").toDouble(); }
	void setTotalPrice(double price)	{ set("TotalPrice", price); }

	double paidPrice() const			{ return get("PaidPrice").toDouble(); }
	void setPaidPrice(double price)		{ set("PaidPrice", price); }

	double toPaidPrice() const			{ return get("ToPaidPrice").toDouble(); }
	void setToPaidPrice(double price)	{ set("ToPaidPrice", price); }

	bool inBuffer() const			{ return get("Buffer").toBool(); }
	void existInBuffer(bool exist)	{ set("Buffer", exist); }

	bool wasPaid() const	{ return get("WasPaid").toBool(); }
	void setPaid(bool paid) { set("WasPaid", paid); }
	///@}

	///
	/// \brief contractor - get contractor object attached to invoice object
	/// \return
	///
	core::Contractor& contractor() { return _contractor; }
	core::Contractor  contractor() const { return _contractor; }
	///
	/// \brief setContractor - set existing contractor object
	/// \param contractor - object to set (must be valid)
	///
	void setContractor(const core::Contractor& contractor);
	///
	/// \brief setContractor - find contractor by id amd attach to model
	/// \param contractorId
	///
	void setContractor(core::eloquent::DBIDKey contractorId);

	///
	/// \brief warehouse - get warehouse object attached to invoice
	/// \return
	///
	core::Warehouse& warehouse() { return _warehouse; }
	core::Warehouse  warehouse() const { return _warehouse; }
	///
	/// \brief setWarehouse - attach existing warehouse object
	/// \param warehouse - object to attach (must be valid)
	///
	void setWarehouse(const core::Warehouse& warehouse);
	///
	/// \brief setWarehouse - find warehouse by id and attach to model
	/// \param warehouseId
	///
	void setWarehouse(core::eloquent::DBIDKey warehouseId);

	///
	/// \brief items return list of all articles pushed on purchase document
	/// \return list of model object
	///
	QList<PurchaseInvoiceItem> items() const {
		return hasMany<core::PurchaseInvoiceItem>("PurchaseInvoiceItems", "InvoiceId");
	}

	CORE_EXPORT friend QDataStream& operator<<(QDataStream&, const core::PurchaseInvoice&) noexcept;
	CORE_EXPORT friend QDataStream& operator>>(QDataStream&, core::PurchaseInvoice&) noexcept;
	
	operator bool() const noexcept {
		return id() > 0;
	}
	///
	/// \brief post remove current invoice from buffer
	/// \return
	///
//	bool post();

	///
	/// \brief hasCorrection
	/// \return
	///
//	bool hasCorrection(bool forceUpdate = false) const;
//	Model correctionDocument(bool forceUpdate = false) const;

private:
	QString mPrefix;				///< first chars in number
	core::Contractor _contractor;	///< attached contractor object
	core::Warehouse _warehouse;		///< attached warehouse object
};

}

#endif // PURCHASEINVOICE_HPP
