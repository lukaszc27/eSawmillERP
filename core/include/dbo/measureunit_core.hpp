#ifndef MEASUREUNIT_CORE_H
#define MEASUREUNIT_CORE_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QString>
#include <QList>


namespace core {

class CORE_EXPORT MeasureUnit : public eloquent::Model<MeasureUnit> {
public:
	///@{
	/// ctors
	explicit MeasureUnit();
	///@}

	static QString tableName() {
		return "UnitMeasure";
	}

	// getters and setters
	QString shortName() const				{ return get("ShortName").toString(); }
	QString fullName() const				{ return get("FullName").toString(); }
	void setShortName(const QString& name)	{ set("ShortName", name.toUpper()); }
	void setFullName(const QString& name)	{ set("FullName", name.toUpper()); }

	///
	/// \brief fillable
	/// return fields name from database uses to store data
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::MeasureUnit& unit) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::MeasureUnit& unit) noexcept;
	///@}
};

} // namespace core

#endif // MEASUREUNIT_CORE_H
