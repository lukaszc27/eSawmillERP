#ifndef Tax_H
#define Tax_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QDataStream>


namespace core {
///
/// \brief The Tax struct
/// base object to manage Tax data on SQL server side
/// this object represent one row from Taxs table
///
struct CORE_EXPORT Tax : public eloquent::Model<Tax> {
	explicit Tax();

	static QString tableName() {
		return "VATs";
	}

	///@{
	/// getters and setters
	double value() const		{ return get("Value").toDouble(); }
	void setValue(double val)	{ set("Value", val); }
	///@}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Tax& tax) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Tax& tax) noexcept;
	///@}
};

} // namespace core

#endif // Tax_H
