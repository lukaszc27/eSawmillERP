#ifndef USER_H
#define USER_H

#include "core_global.h"

#include "contact.h"
#include "company.h"
#include "dbo/role.hpp"
#include "dbobj.hpp"
#include "eloquent/builder.hpp"

namespace core {

struct CORE_EXPORT User : public eloquent::Model<User> {
	explicit User();

	static QString tableName() {
		return "Users";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	// getters and setters
	void setName(const QString& name)			{ set("Name", name.toUpper()); }
	void setPassword(const QString& password)	{ set("Password", password); }
	void setToken(const QString& token)			{ set("Token", token); }
	void setFirstName(const QString& firstName)	{ set("FirstName", firstName.toUpper()); }
	void setSurName(const QString& surname)		{ set("SurName", surname.toUpper()); }

	QString password() const					{ return get("Password").toString(); }
	QString name() const						{ return get("Name").toString(); }
	QString token() const						{ return get("Token").toString(); }
	QString firstName() const					{ return get("FirstName").toString(); }
	QString surName() const						{ return get("SurName").toString(); }

	core::Contact& contact() { return _contact; }
	core::Contact contact() const { return _contact; }
	void setContact(const core::Contact& contact);
	void setContact(core::eloquent::DBIDKey contactId);

	core::Company& company() { return _company; }
	core::Company company() const { return _company; }
	void setCompany(const core::Company& company);
	void setCompany(core::eloquent::DBIDKey companyId);

	///@{
	/// user roles
	core::RoleCollection& roles()		{ return _roles; }
	core::RoleCollection roles() const	{ return _roles; }

	/// @brief add new roles
	void assignRole(const core::RoleCollection& roles);
	void assignRole(const core::Role& role);

	/// @brief check if user has assigned specific role
	bool hasRole(const core::eloquent::DBIDKey& roleId) const;
	bool hasRole(core::Role::SystemRole systemRole) const;
	bool hasRole(const core::Role& role) const;
	///@}

	///@{
	/// override operators
	/// @brief check do user object is correct
	explicit operator bool() const noexcept {
		return id() > 0 && get("ContactId").toInt() > 0;
	}

	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::User& user) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::User& user) noexcept;
	///@}

private:
	core::Contact _contact;
	core::Company _company;
	core::RoleCollection _roles;	///< assigned roles into user
};

} // namespace core


#endif // USER_H
