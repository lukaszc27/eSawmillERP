#ifndef CITY_MODEL_HPP
#define CITY_MODEL_HPP

#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QVariant>
#include <QDataStream>


namespace core {

struct CORE_EXPORT City : public eloquent::Model<City> {
	///@{
	/// ctors
	explicit City();
	///@}

	static QString tableName() {
		return "Cities";
	}

	///
	/// \brief fillable
	/// return all fields from database uses in model
	/// \return
	///
	QStringList fillable() const final override;

	/// getters and setters
	void setName(const QString& name)	{ set("Name", name.toUpper()); }
	QString name() const				{ return get("Name").toString(); }

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::City& city) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::City& city) noexcept;
	///@}
};

} // namespace core

#endif // CITY_HPP
