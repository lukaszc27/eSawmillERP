#ifndef SALEINVOICEITEM_HPP
#define SALEINVOICEITEM_HPP

#include "core_global.h"
#include "dbo/orderElements.h"
#include "dbo/article.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "article.h"


namespace core {

struct CORE_EXPORT SaleInvoiceItem : public core::eloquent::Model<SaleInvoiceItem> {
	///@{
	/// ctors
	explicit SaleInvoiceItem();
	virtual ~SaleInvoiceItem() = default;
	///@}

	static QString tableName()
		{ return "SaleInvoiceItems"; }

	// getters and setters
	QString name() const		{ return get("Name").toString(); }
	int quantity() const		{ return get("Quantity").toInt(); }
	QString measureUnit() const { return get("MeasureUnit").toString(); }
	double price() const		{ return get("Price").toDouble(); }
	double rebate() const		{ return get("Rebate").toDouble(); }
	double value() const		{ return get("Value").toDouble(); }
	QString warehouse() const	{ return get("Warehouse").toString(); }
	double articleId() const	{ return get("ArticleId").toDouble(); }
	core::eloquent::DBIDKey invoiceId() const { return get("InvoiceId").toInt(); }

	void setName(const QString& name)					{ set("Name", name.toUpper()); }
	void setQuantity(int qty)							{ set("Quantity", qty); }
	void setMeasureUnit(const QString& unit)			{ set("MeasureUnit", unit.toUpper()); }
	void setPrice(double price)						{ set("Price", price); }
	void setRebate(double rebate)						{ set("Rebate", rebate); }
	void setValue(double value)						{ set("Value", value); }
	void setWarehouse(const QString& warehouse)		{ set("Warehouse", warehouse.toUpper()); }
	void setArticle(core::eloquent::DBIDKey articleId)	{ set("ArticleId", articleId); }

	void setInvoice(core::eloquent::DBIDKey invoiceId)	{ set("InvoiceId", invoiceId); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	CORE_EXPORT friend QDataStream& operator<<(QDataStream&, const core::SaleInvoiceItem&) noexcept;
	CORE_EXPORT friend QDataStream& operator>>(QDataStream&, core::SaleInvoiceItem&) noexcept;
};

/////////////////////////////////////////////////////////////
namespace adapter {

/// @brief OrderElementSaleInvoiceItem struct
/// convert order element into sale invoice item
struct CORE_EXPORT OrderElementSaleInvoiceItem : public SaleInvoiceItem {
	///@{
	/// ctors
	/// create sale invoice item from order wood element
	explicit OrderElementSaleInvoiceItem(const core::order::Element& element, double additionalLength = 0) noexcept;
	///@}
};

struct CORE_EXPORT SaleInvoiceItemFromArticle : public SaleInvoiceItem {
	explicit SaleInvoiceItemFromArticle(const core::Article& article) noexcept;
};

} // namespace adapter
/////////////////////////////////////////////////////////////

} // namespace core

#endif // SALEINVOICEITEM_HPP
