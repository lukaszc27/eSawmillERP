#ifndef AUTH_H
#define AUTH_H

#include "core_global.h"
#include "user.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

namespace core
{

class CORE_EXPORT Auth
{
public:
    Auth() {};

    /**
     * @brief attempt - Logowanie użytkownika w programie przy użyciu nazwy użytkownika oraz hasła
     * @param userName - nazwa użytkownika
     * @param password - hasło
     * @return true/false
     */
    static bool attempt(const QString &userName,const QString &password);
	static bool attempt(const User& user) {
		return Auth::attempt(user.name(), user.password());
	}

    /**
     * @brief logout - wylogowanie aktualnego użytkownika
     */
    static void logout();

    /**
     * @brief status - zwraca stan użytkownika (czy zalogowany czy nie)
     * @return true/false
     */
    static bool status();

    /**
     * @brief user zwraca aktualnego zalogowanego użytkownika
     * @return
     */
	static User user(bool forceUpdate = false);
};

}

#endif // AUTH_H
