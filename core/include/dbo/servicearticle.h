#ifndef SERVICEARTICLE_H
#define SERVICEARTICLE_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
//#include "service.h"
#include "article.h"

namespace core {
namespace service {

struct CORE_EXPORT Article : public eloquent::Model<Article> {
	///@{
	explicit Article();
	///@}

	static QString tableName() {
		return "ServiceArticle";
	}

//	core::Service service() const;
	core::Article article() const;

	core::eloquent::DBIDKey serviceId() const	{ return get("ServiceId").toInt(); }
	core::eloquent::DBIDKey articleId() const	{ return get("ArticleId").toInt(); }
	double quantity() const						{ return get("Quantity").toInt(); }

	void setServiceId(const core::eloquent::DBIDKey& id)
		{ set("ServiceId", id); }
	void setArticleId(const core::eloquent::DBIDKey& id)
		{ set("ArticleId", id); }
	void setQuantity(double qty)
		{ set("Quantity", qty); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::service::Article& article) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::service::Article& article) noexcept;
	///@}
};

} // namespace service
} // namespace core
#endif // SERVICEARTICLE_H
