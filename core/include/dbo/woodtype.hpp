#ifndef WOODTYPE_HPP
#define WOODTYPE_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QDataStream>

namespace core {

struct CORE_EXPORT WoodType : public eloquent::Model<WoodType> {
	explicit WoodType();

	static QString tableName() {
		return "WoodTypes";
	}

	QString name() const				{ return get("Name").toString(); }
	void setName(const QString& name)	{ set("Name", name.toUpper()); }
	double factor() const				{ return get("Factor").toDouble(); }
	void setFactor(double factor)		{ set("Factor", factor); }
	double woodBark() const				{ return get("WoodBark").toDouble(); }
	void setWoodBark(double woodBark)	{ set("WoodBark", woodBark); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::WoodType& woodType) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::WoodType& woodType) noexcept;
	///@}

	operator bool() const noexcept {
		return id() > 0 && !name().isEmpty();
	}
};

} // namespace core

#endif // WOODTYPE_HPP
