#ifndef CONTRACTOR_H
#define CONTRACTOR_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QString>
#include <QTextStream>
#include "contact.h"
#include "company.h"


namespace core {

///
/// \brief The Contractor struct
/// represent one row from Contractors table
/// base object describe Contractor
///
struct CORE_EXPORT Contractor : public eloquent::Model<Contractor> {
	///@{
	/// ctors
	explicit Contractor();
	///@}

	static QString tableName() {
		return "Contractors";
	}

	///
	/// \brief fillable
	/// return all db fields uses to store data
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief destroyRecord
	/// method executed by ORM framework by internal mechanism
	/// when user want to destroy record in database
	/// \param model
	/// \return
	///
	bool destroyRecord(const Model<Contractor>& model) override;

	///
	/// \brief contact get data about contractor contact
	/// from database or cahce (if exist in cache)
	/// \return contact information
	///
	core::Contact& contact() { return _contact; }
	core::Contact  contact() const { return _contact; }
	void setContact(const core::Contact& contact);
	void setContact(core::eloquent::DBIDKey contactId);

	///@{
	/// getters and setters
	QString name() const							{ return get("Name").toString(); }
	void setName(const QString& name)				{ set("Name", name.toUpper()); }

	QString NIP() const								{ return get("NIP").toString(); }
	void setNIP(const QString& nip)					{ set("NIP", nip.toUpper()); }

	QString REGON() const							{ return get("REGON").toString(); }
	void setREGON(const QString& regon)				{ set("REGON", regon.toUpper()); }

	QString PESEL() const							{ return get("PESEL").toString(); }
	void setPESEL(const QString& pesel)				{ set("PESEL", pesel.toUpper()); }

	bool provider() const							{ return get("Provider").toBool(); }
	void setProvider(bool provider)					{ set("Provider", provider); }

	QString personName() const						{ return get("PersonName").toString(); }
	void setPersonName(const QString& name)			{ set("PersonName", name.toUpper()); }

	QString personSurname() const					{ return get("PersonSurname").toString(); }
	void setPersonSurname(const QString& surname)	{ set("PersonSurname", surname.toUpper()); }

	bool blocked() const							{ return get("Blocked").toBool(); }
	void setBlocked(bool blocked)					{ set("Blocked", blocked); }

	core::eloquent::DBIDKey comarchId() const		{ return get("ComarchId").toInt(); }
	void setComarchId(int id)						{ set("ComarchId", id); }

	core::eloquent::DBIDKey parentId() const		{ return get("ParentId").toInt(); }
	void setParentId(const core::eloquent::DBIDKey& parentId) { set("ParentId", parentId); }
	///@}


	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Contractor& contactor) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Contractor& contractor) noexcept;
	///@}

	///
	/// \brief fromCompany construct contractor object from comapny
	/// \param company
	/// \return
	///
	static Contractor fromCompany(const Company& company);

	///
	/// \brief toString - convert object to string
	/// \return
	///
	friend CORE_EXPORT QTextStream& operator<<(QTextStream& out, const core::Contractor& contractor) noexcept;

	///
	/// \brief getDisplayName
	/// simple function to return contractor name
	/// + person first name and surname when contractor don't have name
	/// + if contractor will have name field return name
	/// \return
	///
	QString getDisplayName() const noexcept;

protected:
	core::Contact _contact;	///< store contact object
};

} // namespace core

#endif // CONTRACTOR_H
