#ifndef SERVICEELEMENT_H
#define SERVICEELEMENT_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"

#include <QList>
#include "../dbo/woodtype.hpp"

namespace core {
namespace service {

struct CORE_EXPORT Element : public eloquent::Model<Element> {
	///@{
	/// ctors
	explicit Element();
	///@}

	static QString tableName() {
		return "ServiceElements";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// getters and setters
	double diameter() const		{ return get("Diameter").toDouble(); }
	double length()	const		{ return get("Length").toDouble(); }
	bool isRotated() const		{ return get("IsRotated").toBool(); }
	core::eloquent::DBIDKey serviceId() const { return get("ServiceId").toInt(); }

	void setDiameter(double diameter)	{ set("Diameter", diameter); }
	void setLength(double length)		{ set("Length", length); }
	void setRotated(bool rotated)		{ set("IsRotated", rotated); }
	void setServiceId(const core::eloquent::DBIDKey& id) { set("ServiceId", id); }

	core::WoodType& woodType()			{ return _woodType; }
	core::WoodType  woodType() const	{ return _woodType; }
	void setWoodType(core::eloquent::DBIDKey woodTypeId);
	void setWoodType(const core::WoodType& woodType);
	///@}

	///
	/// \brief stere
	/// calculate total stere for wood
	/// \return
	///
	double stere() const;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::service::Element& element) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::service::Element& element) noexcept;
	///@}

private:
	core::WoodType _woodType;
};

} // namespace service
} // namespace core

#endif // SERVICEELEMENT_H
