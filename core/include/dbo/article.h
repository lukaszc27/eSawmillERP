#ifndef ARTICLES_CORE_H
#define ARTICLES_CORE_H

#include "../core_global.h"
#include "articleType.h"
#include "contractor.h"
#include "measureunit_core.hpp"
#include "dbobj.hpp"
#include "../eloquent/model.hpp"
#include "../eloquent/builder.hpp"
#include "user.h"
#include "tax.h"
#include "wareHouse.h"

#include <QString>
#include <QTextStream>
#include <QtXml>


namespace core {
///
/// \brief The Article struct
/// dbo object describe one record from Articles table
///
struct CORE_EXPORT Article : public eloquent::Model<Article> {
	///@{
	/// ctors
	explicit Article();
	///@}

	static QString tableName() {
		return "Articles";
	}

	///
	/// \brief fillable
	/// database fields uses in model
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief destroyRecord
	/// make special method for destroy article, and remove before allassigned articles from orders and services
	/// \param model - dbo article model to remove
	/// \return boolean value
	///
	bool destroyRecord(const Model<Article>& model) override;

	///@{
	/// getters and setters for fields in model
	QString name() const						{ return get("Name").toString(); }
	QString barCode() const						{ return get("BarCode").toString(); }
	double pricePurchaseNetto() const			{ return get("PricePurchaseNetto").toDouble(); }
	double pricePurchaseBrutto() const			{ return get("PricePurchaseBrutto").toDouble(); }
	double priceSaleNetto() const				{ return get("PriceSaleNetto").toDouble(); }
	double priceSaleBrutto() const				{ return get("PriceSaleBrutto").toDouble(); }
	double quantity() const						{ return get("Quantity").toDouble(); }

	void setName(const QString& name)			{ set("Name", name); }
	void setBarCode(const QString& barcode)		{ set("BarCode", barcode); }
	void setPricePurchaseNetto(double price)	{ set("PricePurchaseNetto", price); }
	void setPricePurchaseBrutto(double price)	{ set("PricePurchaseBrutto", price); }
	void setPriceSaleNetto(double price)		{ set("PriceSaleNetto", price); }
	void setPriceSaleBrutto(double price)		{ set("PriceSaleBrutto", price); }
	void setQuantity(double qty)				{ set("Quantity", qty); }

	///
	/// \brief artcielType get current assigned article type model
	/// \return reference to ArticleType object
	ArticleType& artcielType() { return _articleType; }
	ArticleType  articleType() const { return _articleType; }
	///
	/// \brief setArticleType - assign existing model to current article object
	/// \param type - dbo object
	void setArticleType(const ArticleType& type);
	///
	/// \brief setArticleType - set id in main model and get article type from database
	/// and store in memory
	/// \param typeId - article type id
	void setArticleType(core::eloquent::DBIDKey typeId);

	///
	/// \brief provider - get current assigned contractor object
	/// \return reference to Contractor object
	Contractor& provider() { return _provider; }
	Contractor  provider() const { return _provider; }
	///
	/// \brief setProvider - assign existing model to current article object
	/// \param provider - valid dbo object
	void setProvider(const Contractor& provider);
	///
	/// \brief setProvider - set id in main model and get contractor form database
	/// \param providerId - contractor id
	void setProvider(core::eloquent::DBIDKey providerId);

	///
	/// \brief warehouse get current assigned warehouse object
	/// \return reference to Warehouse dbo object
	Warehouse& warehouse() { return _warehouse; }
	Warehouse  warehouse() const { return _warehouse; }
	///
	/// \brief setWarehouse - assign new valid dbo object
	/// \param warehouse - object to assign
	void setWarehouse(const Warehouse& warehouse);
	///
	/// \brief setWarehouse - set current warehouse dbo object from db
	/// \param warehouseId warehouse identyficator in db
	void setWarehouse(core::eloquent::DBIDKey warehouseId);

	///
	/// \brief saleTax - get current assigned Tax object to calculate sale price
	/// \return - valid dbo Tax object
	Tax& saleTax() { return _saleTax; }
	Tax  saleTax() const { return _saleTax; }
	///
	/// \brief purchaseTax - get current assigned Tax object to calculate purchase price
	/// \return valid dbo Tax object
	Tax& purchaseTax() { return _purchaseTax; }
	Tax  purchaseTax() const { return _purchaseTax; }
	///
	/// \brief setSaleTax - assign Tax object to article model
	/// \param Tax - valid object to assign
	void setSaleTax(const core::Tax& Tax);
	void setSaleTax(core::eloquent::DBIDKey saleTaxId);
	///
	/// \brief setPurchaseTax - the same situation but for purchase Tax object
	/// \param Tax look up ;)
	void setPurchaseTax(const core::Tax& Tax);
	void setPurchaseTax(core::eloquent::DBIDKey purchaseTaxId);

	///
	/// \brief company - get current assigned company object
	/// \return valid dbo Company object (reference to object)
	Company& company() { return _company; }
	Company  company() const { return _company; }
	///
	/// \brief setCompany - set current valid company object
	/// \param company object to assign to model
	void setCompany(const Company& company);
	///
	/// \brief setCompany - set current valid company object
	/// and get info from database
	/// \param companyId - valid company identificator
	void setCompany(core::eloquent::DBIDKey companyId);

	///
	/// \brief user return valid user object who added article to system
	/// \return valid user dbo (reference)
	User& user() { return _user; }
	User user() const { return _user; }
	///
	/// \brief setUser
	/// \param user
	void setUser(const User& user);
	void setUser(core::eloquent::DBIDKey userId);

	///
	/// \brief unit
	/// \return
	MeasureUnit& unit() { return _measureUnit; }
	MeasureUnit unit() const { return _measureUnit; }
	///
	/// \brief setUnit
	/// \param unit
	void setUnit(const MeasureUnit& unit);
	void setUnit(core::eloquent::DBIDKey unitId);
	///@}

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Article& article) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Article& article) noexcept;
	///@}

protected:
	ArticleType _articleType;
	Contractor	_provider;
	Warehouse	_warehouse;
	Tax			_purchaseTax;
	Tax			_saleTax;
	Company		_company;
	User		_user;
	MeasureUnit	_measureUnit;
};

} // namespace core

#endif // ARTICLES_CORE_H
