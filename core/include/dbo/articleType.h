#ifndef ARTICLETYPECORE_H
#define ARTICLETYPECORE_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QtXml>


namespace core {

struct CORE_EXPORT ArticleType : public eloquent::Model<ArticleType> {
	explicit ArticleType();

	static QString tableName() {
		return "ArticleTypes";
	}

	///@{
	/// getters and setters
	void setName(const QString& name)	{ set("Name", name.toUpper()); }
	QString name() const				{ return get("Name").toString(); }
	///@}

	///
	/// \brief fillable
	/// columns from database uses in model
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::ArticleType& type) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::ArticleType& type) noexcept;
	///@}
};

} // namespace core

#endif // ARTICLETYPECORE_H
