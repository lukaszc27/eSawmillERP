#ifndef PURCHASEINVOICECORRECTION_HPP
#define PURCHASEINVOICECORRECTION_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "purchaseinvoice.hpp"


namespace core {

struct CORE_EXPORT PurchaseInvoiceCorrection : public eloquent::Model<PurchaseInvoiceCorrection> {
	///@{
	/// ctors
	explicit PurchaseInvoiceCorrection();
	virtual ~PurchaseInvoiceCorrection() = default;
	///@}

	///
	/// \brief tableName
	/// return table name where objects are stored
	/// \return
	///
	static QString tableName()
		{ return "PurchaseInvoiceCorrections"; }

	///
	/// \brief invoice get orignal invocie
	/// \param forceUpdate
	/// \return
	///
	PurchaseInvoice invoice(bool forceUpdate = false) const;
	void setInvoice(int invoiceId) { set("InvoiceId", invoiceId); }
	void setInvoice(const PurchaseInvoice& invoice) { setInvoice(invoice.id()); }

	///
	/// \brief correctionInvoice get correction invoice
	/// \param forceUpdate
	/// \return
	///
	PurchaseInvoice correctionInvoice(bool forceUpdate = false) const;
	void setCorrectionInvoice(int invoiceId) { set("CorrectionId", invoiceId); }
	void setCorrectionInvoice(const PurchaseInvoice& invoice) { setInvoice(invoice.id()); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;
};

} // namespace core

#endif // PURCHASEINVOICECORRECTION_HPP
