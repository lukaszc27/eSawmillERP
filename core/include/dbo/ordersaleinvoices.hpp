#ifndef ORDERSALEINVOICES_H
#define ORDERSALEINVOICES_H

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "order.h"
#include "saleinvoice.hpp"
#include <QDate>

namespace core {

struct CORE_EXPORT OrderSaleInvoices : public eloquent::Model<OrderSaleInvoices> {
	///@{
	/// ctors
	explicit OrderSaleInvoices();
	explicit OrderSaleInvoices(const Order& order, const SaleInvoice& invoice);
	virtual ~OrderSaleInvoices() = default;
	///@}

	static QString tableName() { return "OrderSaleInvoices"; }

	// getters and setters
	void setOrder(eloquent::DBIDKey orderId)	 { set("OrderId", orderId); }
	void setOrder(const Order& order)			 { setOrder(order.id()); }
	Order order(bool forceUpdate = false) const;

	void setInvoice(eloquent::DBIDKey invoiceId) { set("InvoiceId", invoiceId); }
	void setInvoice(core::SaleInvoice& invoice)  { setInvoice(invoice.id()); }
	SaleInvoice invoice(bool forceUpdate = false) const;

	QDate createdDate() const { return get("CreatedDate").toDate(); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;
};

} // namespace core

#endif // ORDERSALEINVOICES_H
