#ifndef ORDERARTICLE_H
#define ORDERARTICLE_H

#include "core_global.h"
#include "article.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QList>


namespace core {
namespace order {

struct CORE_EXPORT Article : public eloquent::Model<Article> {
	///@{
	/// ctors
	explicit Article();
	///@}

	static QString tableName() {
		return "OrderArticles";
	}

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief article
	/// return orginal article from Articles database
	/// \return
	///
	core::Article article() const;

	// getters and setters
	double quantity() const { return get("Quantity").toDouble(); }
	core::eloquent::DBIDKey orderId() const { return get("OrderId").toInt(); }
	core::eloquent::DBIDKey articleId() const { return get("ArticleId").toInt(); }

	void setQuantity(double qty)							{ set("Quantity", qty); }
	void setOrderId(const core::eloquent::DBIDKey& id)		{ set("OrderId", id); }
	void setArticleId(const core::eloquent::DBIDKey& id)	{ set("ArticleId", id); }

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::order::Article& article) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::order::Article& article) noexcept;
	///@}

};

} // namespace order
} // namespace core


#endif // ORDERARTICLE_H
