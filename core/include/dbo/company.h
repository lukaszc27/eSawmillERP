#ifndef COMPANY_H
#define COMPANY_H

#include "core_global.h"
#include "contact.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QObject>
#include <QTextStream>
#include <QDataStream>


namespace core {

struct CORE_EXPORT Company : public eloquent::Model<Company> {
	///@{
	/// ctors
	explicit Company();
	///@}

	static QString tableName() {
		return "Companies";
	}

	///
	/// \brief fillable
	/// list of database fields uses in model
	/// \return
	///
	QStringList fillable() const final override;

	bool destroyRecord(const Model<Company>& model) override;

	// getters and setters
	QString name() const			{ return get("Name").toString(); }
	QString personName() const		{ return get("PersonName").toString(); }
	QString personSurname() const	{ return get("PersonSurname").toString(); }
	QString NIP() const				{ return get("NIP").toString(); }
	QString REGON() const			{ return get("REGON").toString(); }
	QString KRS() const				{ return get("KRS").toString(); }

	void setName(const QString& name)					{ set("Name", name.toUpper()); }
	void setPersonName(const QString& personName)		{ set("PersonName", personName.toUpper()); }
	void setPersonSurname(const QString& personSurname) { set("PersonSurname", personSurname.toUpper()); }
	void setNIP(const QString& nip)						{ set("NIP", nip.toUpper()); }
	void setREGON(const QString& regon)					{ set("REGON", regon.toUpper()); }
	void setKRS(const QString& krs)						{ set("KRS", krs.toUpper()); }

	core::Contact& contact() { return _contact; }
	core::Contact contact() const { return _contact; }
	void setContact(const core::Contact& contact);
	void setContact(core::eloquent::DBIDKey contactId);

	friend CORE_EXPORT QTextStream& operator<<(QTextStream& out, const core::Company& company) noexcept;

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Company& comapny) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Company& comapny) noexcept;
	///@}

private:
	core::Contact _contact;
};

} // namespace core

#endif // COMPANY_H
