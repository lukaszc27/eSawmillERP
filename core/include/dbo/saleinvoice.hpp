#ifndef SALEINVOICE_HPP
#define SALEINVOICE_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "contractor.h"
#include "wareHouse.h"
#include "saleinvoiceitem.hpp"
#include <QDate>
#include <QCache>


namespace core {
//struct CORE_EXPORT SaleInvoiceItem;
struct CORE_EXPORT SaleInvoice : public eloquent::Model<SaleInvoice> {
	///@{
	/// ctors
	explicit SaleInvoice();
	virtual ~SaleInvoice() = default;
	///@}

	static QString tableName() { return "SaleInvoices"; }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	bool createRecord(const Model<SaleInvoice>& model) override final;

	// getters and setters
	QString prefix() const				{ return get("Prefix").toString(); }
	QString number() const				{ return get("Number").toString(); }
	QDate dateOfIssue() const			{ return get("DateOfIssue").toDate(); }
	QDate dateOfSale() const			{ return get("DateOfSale").toDate(); }
	double rebate() const				{ return get("Rebate").toDouble(); }
	QString payment() const				{ return get("Payment").toString(); }
	int paymentDeadlineDays() const		{ return get("PaymentDeadlineDays").toInt(); }
	QDate paymentDeadlineDate() const	{ return get("PaymentDeadlineDate").toDate(); }
	double nettoPrice() const			{ return get("NettoPrice").toDouble(); }
	double totalPrice() const			{ return get("TotalPrice").toDouble(); }
	double paidPrice() const			{ return get("PaidPrice").toDouble(); }
	double toPaidPrice() const			{ return get("ToPaidPrice").toDouble(); }
	bool inBuffer() const				{ return get("Buffer").toBool(); }
	bool wasPaid() const				{ return get("WasPaid").toBool(); }

	void setPrefix(const QString& prefix)			{ set("Prefix", prefix.toUpper()); }
	void setNumber(const QString& number)			{ set("Number", number); }
	void setDateOfIssue(const QDate& date)			{ set("DateOfIssue", date); }
	void setDateOfSale(const QDate& date)			{ set("DateOfSale", date); }
	void setRebate(float rebate)					{ set("Rebate", rebate); }
	void setPayment(const QString& payment)			{ set("Payment", payment); }
	void setPaymentDeadlineDays(int days)			{ set("PaymentDeadlineDays", days); }
	void setPaymentDeadlineDate(const QDate& date)	{ set("PaymentDeadlineDate", date); }
	void setNettoPrice(double price)				{ set("NettoPrice", price); }
	void setTotalPrice(double price)				{ set("TotalPrice", price); }
	void setPaidPrice(double price)					{ set("PaidPrice", price); }
	void setToPaidPrice(double price)				{ set("ToPaidPrice", price); }
	void existInBuffer(bool exist)					{ set("Buffer", exist); }
	void setPaid(bool paid)							{ set("WasPaid", paid); }

	// get all items assign to invoice
//	QList<SaleInvoiceItem> items() const {
//		return hasMany<core::SaleInvoiceItem>("SaleInvoiceItems", "InvoiceId");
//	}
	QList<core::SaleInvoiceItem> items() const					{ return _items; }
	void setItems(const QList<core::SaleInvoiceItem>& items)	{ _items = items; }

	///
	/// \brief setWarehouse
	/// set existing warehouse object (object must be valid)
	/// \param warehouse
	///
	void setWarehouse(const core::Warehouse& warehouse);
	///
	/// \brief setWarehouse
	/// find warehouse by id and assign to model
	/// \param warehouseId
	///
	void setWarehouse(core::eloquent::DBIDKey warehouseId);
	///
	/// \brief warehouse
	/// return current assigned warehouse
	/// \return
	///
	core::Warehouse& warehouse() { return _warehouse; }
	core::Warehouse  warehouse() const { return _warehouse; }

	///
	/// \brief setContractor
	/// set existing contractor object (object must be valid)
	/// \param contractor
	///
	void setContractor(const core::Contractor& contractor);
	///
	/// \brief setContractor
	/// find contractor by id and assign to model
	/// \param contractorId
	///
	void setContractor(core::eloquent::DBIDKey contractorId);
	///
	/// \brief contractor
	/// return current assigned contractor
	/// \return
	///
	core::Contractor& contractor() { return _contractor; }
	core::Contractor  contractor() const { return _contractor; }

	CORE_EXPORT friend QDataStream& operator<<(QDataStream&, const core::SaleInvoice&) noexcept;
	CORE_EXPORT friend QDataStream& operator>>(QDataStream&, core::SaleInvoice& invoice) noexcept;

protected:
	core::Warehouse _warehouse;		///< assigned warehouse object
	core::Contractor _contractor;	///< assigned contractor object
	QList<core::SaleInvoiceItem> _items;
};

//////////////////////////////////////////////////////////////
namespace adapter {

/// @brief OrderSaleInvoice
/// construct sale invoice object from order
struct CORE_EXPORT OrderSaleInvoice : public SaleInvoice {
	///@{
	/// ctors
	explicit OrderSaleInvoice(const core::Order& order) noexcept;
	///@}
};

} // namespace adapter
//////////////////////////////////////////////////////////////
} // namespace core

#endif // SALEINVOICE_HPP
