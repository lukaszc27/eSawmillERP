#ifndef ROLE_HPP
#define ROLE_HPP

#include "../core_global.h"
#include "../eloquent/model.hpp"
#include "../eloquent/builder.hpp"
#include <QDataStream>


namespace core {

struct CORE_EXPORT Role : public core::eloquent::Model<Role> {
	explicit Role();

	static QString tableName() {
		return "Roles";
	}

	virtual QStringList fillable() const override;

	// getters and setters
	QString name() const				{ return get("Name").toString(); }
	void setName(const QString& name)	{ set("Name", name.toUpper()); }

	operator bool() const noexcept {
		return id() > 0 && !name().isEmpty();
	}
	bool operator==(const Role& other) const noexcept {
		return id() == other.id();
	}

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Role& role) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Role& role) noexcept;
	///@}

	///
	/// \brief The SystemRole enum
	/// default system roles existing in system
	///
	enum SystemRole {
		Administrator	= 1,
		Worker			= 2,
		Operator		= 3,
		Sawer			= 4,
		Driver			= 5
	};
};
using RoleCollection = core::eloquent::Collection<Role>;


///////////////////////////////////////////////////////////////////////
///
/// \brief The UserRole struct
/// many to many relation between users and roles
///
struct CORE_EXPORT UserRole : public core::eloquent::Model<UserRole> {
	explicit UserRole();
	explicit UserRole(const std::pair<core::eloquent::DBIDKey, core::eloquent::DBIDKey>& item);
	explicit UserRole(
		const core::eloquent::DBIDKey& userId,
		const core::eloquent::DBIDKey& roleId
	);

	static QString tableName() {
		return "UsersRoles";
	}
	virtual QStringList fillable() const override;

	// getters and setters
	core::eloquent::DBIDKey userId() const { return get("UserId").toInt(); }
	core::eloquent::DBIDKey roleId() const { return get("RoleId").toInt(); }

	void setUser(const core::eloquent::DBIDKey& userId) { set("UserId", userId); }
	void setRole(const core::eloquent::DBIDKey& roleId) { set("RoleId", roleId); }

	// override bool operator
	explicit operator bool() const noexcept {
		return id() > 0 && userId() > 0 && roleId() > 0;
	}

	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::UserRole& userRole) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::UserRole& userRole) noexcept;
};

} // namespace core

#endif // ROLE_HPP
