#ifndef CONTACT_H
#define CONTACT_H

#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include <QTextStream>
#include <QDataStream>

namespace core {

///
/// \brief The Contact struct
/// struct describe one record from Contacts table
///
struct CORE_EXPORT Contact : public eloquent::Model<Contact> {
	///@{
	/// ctors
	explicit Contact();
	///@}

	///
	/// \brief tableName
	/// table name in db uses by model
	/// \return
	///
	static QString tableName() {
		return "Contacts";
	}

	///
	/// \brief fillable
	/// return database fields uses in model
	/// \return
	///
	QStringList fillable() const final override;

	///@{
	/// getters and setters
	QString city() const			{ return get("City").toString(); }
	QString country() const			{ return get("Country").toString(); }
	QString IsoCountry() const		{ return get("IsoCountry").toString(); }
	QString state() const			{ return get("State").toString(); }
	QString postCode() const		{ return get("ZipCode").toString(); }
	QString postOffice() const		{ return get("PostOffice").toString(); }
	QString street() const			{ return get("Street").toString(); }
	QString homeNumber() const		{ return get("HomeNumber").toString(); }
	QString localNumber() const		{ return get("LocalNumber").toString(); }
	QString phone() const			{ return get("Phone").toString(); }
	QString email() const			{ return get("Email").toString(); }
	QString url() const				{ return get("Url").toString(); }

	void setCity(const QString& city)				{ set("City", city.toUpper()); }
	void setCountry(const QString& country)			{ set("Country", country.toUpper()); }
	void setIsoCountry(const QString& iso)			{ set("IsoCountry", iso.toUpper()); }
	void setState(const QString& state)				{ set("State", state.toUpper()); }
	void setPostCode(const QString& postCode)		{ set("ZipCode", postCode.toUpper()); }
	void setPostOffice(const QString& postOffice)	{ set("PostOffice", postOffice.toUpper()); }
	void setStreet(const QString& street)			{ set("Street", street.toUpper()); }
	void setHomeNumber(const QString& number)		{ set("HomeNumber", number.toUpper()); }
	void setLocalNumber(const QString& number)		{ set("LocalNumber", number.toUpper()); }
	void setPhone(const QString& phone)				{ set("Phone", phone); }
	void setEmail(const QString& email)				{ set("Email", email.toUpper()); }
	void setUrl(const QString& url)					{ set("Url", url.toUpper()); }
	///@}


	/// \brief operator bool
	/// check correction contact object
	explicit operator bool() const noexcept {
		return id() > 0;
	}

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Contact& contact) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Contact& contact) noexcept;
	///@}

	///
	/// \details serialization object, used to print obj in documents
	/// \param out
	/// \param contact
	/// \return
	///
	friend CORE_EXPORT QTextStream& operator<<(QTextStream& out, const core::Contact& contact) noexcept;
};

} // namespace core

#endif // CONTACT_H
