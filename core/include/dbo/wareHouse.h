#ifndef WAREHOUSE_H
#define WAREHOUSE_H


#include "core_global.h"
#include "dbobj.hpp"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"

#include <QString>
#include <QDomElement>
#include <QDataStream>


namespace core {
///
/// \brief The Warehouse struct
/// basic Warehouse model used to manage data in SQL server
/// and store information in memory
/// extends standard eloquent::Model (add some function specifited to warehouses)
///
struct CORE_EXPORT Warehouse : public eloquent::Model<Warehouse> {
	explicit Warehouse();

	static QString tableName()
		{ return "Warehouses"; }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief destroyRecord
	/// override virtual method in Model object
	/// to execute specifited operation before warehouse will be removed
	/// \param model - warehouse model describe record who will be removed
	/// \return boolean value (true if will be success)
	///
	bool destroyRecord(const Model<Warehouse>& model) override;

	///@{
	/// getters and setters
	QString name() const							{ return get("Name").toString(); }
	void setName(const QString& name)				{ set("Name", name.toUpper()); }

	QString shortcut() const						{ return get("Shortcut").toString(); }
	void setShortcut(const QString& shortcut)		{ set("Shortcut", shortcut.toUpper()); }

	QString description() const						{ return get("Description").toString(); }
	void setDescription(const QString& description) { set("Description", description.toUpper()); }
	///@}

	///@{
	/// serialization and deserialization
	friend CORE_EXPORT QDataStream& operator<<(QDataStream& stream, const core::Warehouse& warehouse) noexcept;
	friend CORE_EXPORT QDataStream& operator>>(QDataStream& stream, core::Warehouse& warehouse) noexcept;
	///@}
};

} // namespace core

#endif // WAREHOUSE_H
