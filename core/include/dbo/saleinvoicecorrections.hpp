#ifndef SALEINVOICECORRECTIONS_HPP
#define SALEINVOICECORRECTIONS_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "saleinvoice.hpp"

namespace core {

struct CORE_EXPORT SaleInvocieCorrections : public eloquent::Model<SaleInvocieCorrections> {
	///@{
	/// ctors
	explicit SaleInvocieCorrections();
	virtual ~SaleInvocieCorrections() = default;
	///@}

	static QString tableName() { return "SaleInvoiceCorretions"; }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	///
	/// \brief invoice - return main invoice (corrected invoice)
	/// \param forceUpdate
	/// \return
	///
	core::SaleInvoice invoice(bool forceUpdate = false) const;
	void setInvoice(int invoiceId) { set("InvoiceId", invoiceId); }
	void setInvoice(const SaleInvoice& invoice) { setInvoice(invoice.id()); }

	///
	/// \brief correctionInvoice - return new invoice after correction
	/// \param forceUpdate
	/// \return
	///
	core::SaleInvoice correctionInvoice(bool forceUpdate = false) const;
	void setCorrectionInvoice(int invoiceId) { set("CorrectionId", invoiceId); }
	void setCorrectionInvoice(const SaleInvoice& invoice) { setCorrectionInvoice(invoice.id()); }

	///
	/// \brief createdDate return date when user was created correction for invoice
	/// \return QDate object
	///
	QDate createdDate() const { return get("CreatedDate").toDate(); }
};

} // namespace core

#endif // SALEINVOICECORRECTIONS_HPP
