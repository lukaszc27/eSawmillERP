#ifndef PURCHASEINVOICEITEM_HPP
#define PURCHASEINVOICEITEM_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
//#include "purchaseinvoice.hpp"
#include "article.h"


namespace core {

struct CORE_EXPORT PurchaseInvoiceItem : public eloquent::Model<PurchaseInvoiceItem> {
	///@{
	/// ctors
	explicit PurchaseInvoiceItem();
	virtual ~PurchaseInvoiceItem() = default;
	///@}

	static QString tableName() { return "PurchaseInvoiceItems"; }

	// getters and setters
	QString name() const						{ return get("Name").toString(); }
	void setName(const QString& name)			{ set("Name", name.toUpper()); }

	int quantity() const						{ return get("Quantity").toInt(); }
	void setQuantity(int qty)					{ set("Quantity", qty); }

	QString measureUnit() const					{ return get("MeasureUnit").toString(); }
	void setMeasureUnit(const QString& unit)	{ set("MeasureUnit", unit.toUpper()); }

	int rebate() const							{ return get("Rebate").toInt(); }
	void setRebate(int rebate)					{ return set("Rebate", rebate); }

	double price() const						{ return get("Price").toDouble(); }
	void setPrice(double price)					{ set("Price", price); }

	double value() const						{ return get("Value").toDouble(); }
	void setValue(double val)					{ set("Value", val); }

	QString warehouse() const					{ return get("Warehouse").toString(); }
	void setWarehouse(const QString& name)		{ set("Warehouse", name.toUpper()); }

	void setInvoice(core::eloquent::DBIDKey id) { set("InvoiceId", id); }
	core::eloquent::DBIDKey invoiceId() const { return get("InvoiceId").toInt(); }

	///
	/// \brief orginalArticle return orginal article existing in warehouse
	/// assigned to this position
	/// \param forceUpdate will be true if you want to get fresh data from db
	/// \return Database article model
	///
	core::Article orginalArticle(bool forceUpdate = false) const;
	void setOrginalArticle(int id)					{ set("ArticleId", id); }
	void setOrginalArticle(core::Article& article)	{ setOrginalArticle(article.id()); }
	core::eloquent::DBIDKey articleId() const			{ return get("ArticleId").toInt(); }

	///
	/// \brief fillable
	/// \return
	///
	QStringList fillable() const final override;

	CORE_EXPORT friend QDataStream& operator<<(QDataStream&, const core::PurchaseInvoiceItem&) noexcept;
	CORE_EXPORT friend QDataStream& operator>>(QDataStream&, core::PurchaseInvoiceItem&) noexcept;
};

}

#endif // PURCHASEINVOICEITEM_HPP
