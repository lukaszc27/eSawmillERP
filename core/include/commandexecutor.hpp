#ifndef COMMANDEXECUTOR_HPP
#define COMMANDEXECUTOR_HPP

#include "core_global.h"
#include "abstracts/abstractcommand.hpp"
#include <stack>
#include <memory>
#include <forward_list>

namespace core {

/// @brief CommandExecutor
/// global object to execute commands sent by user
/// this object can undo last executed commands
/// and schould be used to communicate GUI layer with buisness layer
class CORE_EXPORT CommandExecutor final {
public:
	explicit CommandExecutor();

	CommandExecutor(const CommandExecutor&) = delete;
	CommandExecutor& operator=(const CommandExecutor&) = delete;

	/// @brief instance
	/// @return reference to singelton
	static CommandExecutor& instance();

	/// @brief execute
	/// execute command sent by user and when the command can be undo,
	/// adding it onto stack
	/// @param command 
	/// @return true - when function will be done correctly (when command return true)
	bool execute(std::unique_ptr<core::abstracts::AbstractCommand> command);

	/// @brief canUndo function
	/// @return true when user can undo last executed operation
	bool canUndo();

	/// @brief undo function
	/// undo last executed command
	/// @return
	bool undo();


	/////////////////////////////////////////////////////////
	/// \brief The Listener interface
	/// use it when you want to listen changes in excutor object
	struct Listener {
		/// @brief stackChanged
		/// inform listeners do user can undo last executed operation
		virtual void stackChanged(bool canUndo) = 0;
		virtual void commandExecuted() = 0;
	};

	/// @brief attachListener - add new listener as a object to recive notifications
	void attachListener(Listener* listener) {
		_listeners.push_front(listener);
	}
	/// @brief detachListener - remove object from listeners list
	void detachListener(Listener* listener) {
		_listeners.remove(listener);
	}
	/////////////////////////////////////////////////////////

private:
	template <typename Function, typename... Args>
	void notify(Function func, Args... args)
	{
		for (auto* listener : _listeners)
			((*listener).*func)(std::forward<Args>(args)...);
	}

private:
	std::stack<std::unique_ptr<core::abstracts::AbstractCommand>> _commands;
	std::forward_list<Listener*> _listeners;
};

} // namespace core

#endif // COMMANDEXECUTOR_HPP
