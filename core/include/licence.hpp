#ifndef LICENCE_HPP
#define LICENCE_HPP

#include <QString>
#include <QDate>
#include "dbo/contact.h"
#include "dbo/company.h"
#include <QDomElement>
#include <QDomDocument>
#include "core_global.h"


namespace core {
/**
 * @brief The Licence class
 * parsuje plik licencji oraz wyciąga z niego
 * wszystkie dane potrzebne do dalszego wykożystania w programie
 */
class CORE_EXPORT Licence
{
public:
	Licence();
	Licence(const QString& name);
	~Licence();

	///
	/// \brief load - wczytuje plik o podanej nazwie oraz stara się wyciągnąć wszystkie informacje
	/// \param name - nazwa pliku do odczytu
	/// \return true w przypadku powodzenia
	///
	bool load(const QString& name);

	void setBuyDate(QDate date) { mBuyDate = date; }
	void setExpiryDate(QDate date) { mExpiryDate = date; }

	bool isOrderModuleActive() const { return mOrderActive; }
	bool isServiceModuleActive() const { return mServiceActive; }
	bool isArticleModuleActive() const { return mArticleActive; }

	QDate buyDate() const { return mBuyDate; }
	QDate expiryDate() const { return mExpiryDate; }

//	QString companyName() const { return mCompanyName; }
//	QString personName() const { return mPersonName; }
//	QString personSurname() const { return mPersonSurname; }
//	QString NIP() const { return mNip; }
	core::Contact customerContact() const { return mCustomerContact; }
	core::Contact dealerContact() const { return mDealerContact; }

	core::Company customer() const { return mCustomer; }
	core::Company dealer() const { return mDealer; }

private:
	/**
	 * @brief parseNode - odczytuje informacje o kontahencie lub osobie wystawiającej dokument licencji
	 * @param element - element dostawcy lub kontrahenta (XML node)
	 * @param company - wskaźnik na obiekt w którym będą zapisane dane firmowe
	 * @param contact - wskaźnik na obiekt w którym będa zapisane dane kontaktowe
	 * @return bool ( true w przypadku powodzenia odczytania danych )
	 */
	bool parseNode(QDomElement& element, core::Company* company, core::Contact* contact);

	QDate mBuyDate;		// data zakupu licencji
	QDate mExpiryDate;	// termin ważności licencji

	bool mOrderActive;		// aktywność modułu zamówień
	bool mServiceActive;	// aktywność modułu usług
	bool mArticleActive;	// aktywność modułu artykułów

	core::Company mCustomer;
	core::Company mDealer;

	core::Contact mCustomerContact;
	core::Contact mDealerContact;
};

} // namespace eSawmill

#endif // LICENCE_HPP
