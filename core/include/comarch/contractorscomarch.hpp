#ifndef CONTRACTORSCOMARCH_HPP
#define CONTRACTORSCOMARCH_HPP

#include "core_global.h"
#include "eloquent/model.hpp"
#include "eloquent/builder.hpp"
#include "dbo/contact.h"
#include "dbo/contractor.h"
#include <QSqlDatabase>

namespace core::comarch {

class CORE_EXPORT Contractor : public core::eloquent::Model<Contractor> {
public:
	explicit Contractor();
	virtual ~Contractor() = default;

	static QString tableName() { return "Kontrahenci"; }
	core::eloquent::Builder<Contractor> builder() const override;
	bool createRecord(const Model<Contractor>& model) override;

	///
	/// \brief contact - returned full used contact model
	/// who can by gived to other widget to automatic import data
	/// \return eloquent model
	///
	Contact contact();

	///
	/// \brief contractor - returned full used contractor model
	/// \return eloquent model
	///
	core::Contractor contractor();

	void setName(const QString &name, int index = 0) {
		switch (index) {
		case 0: set("Knt_Nazwa1", name); break;
		case 1: set("Knt_Nazwa2", name); break;
		case 2: set("Knt_Nazwa3", name); break;
		}
	}
	void setPhone(const QString &phone,int index = 0) {
		switch (index) {
		case 0: set("Knt_Telefon1", phone); break;
		case 1: set("Knt_Telefon2", phone); break;
		case 2: set("Knt_Telefon3", phone); break;
		}
	}

	void setCode(const QString &code) { set("Knt_Kod", code); }
	void setGroup(const QString &group) { set("Knt_Grupa", group); }
	void setReceiver(bool receiver) { set("Knt_Rodzaj_Odbiorca", receiver); }
	void setProvider(bool provider) { set("Knt_Rodzaj_Dostawca", provider); }
	void setCompetition(bool competition) { set("Knt_Rodzaj_Konkurencja", competition); }
	void setPartner(bool partner) { set("Knt_Rodzaj_Partner", partner); }
	void setPotentialCustomer(bool potentialCustomer) { set("Knt_Rodzaj_Potencilny", potentialCustomer); }
	void setNip(const QString &nip) { set("Knt_Nip", nip); }
	void setNip(const QString &nip, const QString &code) { setNip(nip); setNipCode(code); }
	void setNipCode(const QString &code) { set("Knt_NipKraj", code);}
	void setRegon(const QString& regon) { set("Knt_Regon", regon); }
	void setDocumentId(const QString &documentId) { set("Knt_DokumentTozsamosci", documentId); }
	void setCountry(const QString &country) { set("Knt_Kraj", country); }
	void setCountry(const QString &country, const QString &iso) { setCountry(country); setCountryIso(iso); }
	void setCountryIso(const QString& iso) { set("Knt_KrajISO", iso); }
	void setCity(const QString &city) { set("Knt_Miasto", city); }
	void setPostOffice(const QString &postOffice) { set("Knt_Poczta", postOffice); }
	void setPostCode(const QString &postCode) { set("Knt_KodPocztowy", postCode); }
	void setStreet(const QString &street) { set("Knt_Ulica", street); }
	void setSmsPhone(const QString &phone) { set("Knt_TelefonSms", phone); }
	void setState(const QString &state) { set("Knt_Wojewodztwo", state); }
	void setHomeNumber(const QString &homeNumber) { set("Knt_NrDomu", homeNumber); }
	void setLocalNumber(const QString &localNumber) { set("Knt_NrLokalu", localNumber); }
	void setFax(const QString &fax) { set("Knt_Fax", fax); }
	void setEmail(const QString &email) { set("Knt_Email", email); }
	void setUrl(const QString &url) { set("Knt_URL", url); }
	void setPesel(const QString &pesel) { set("Knt_Pesel", pesel); }

	QString code() const { return get("Knt_Kod").toString(); }
	QString group() const {return get("Knt_Grupa").toString(); }
	bool receiver() const { return get("Knt_Rodzaj_Odbiorca").toBool(); }
	bool provider() const { return get("Knt_Rodzaj_Dostawca").toBool(); }
	bool competition() const { return get("Knt_Rodzaj_Konkurencja").toBool(); }
	bool potentialCustomer() const { return get("Knt_Rodzaj_Potencialny").toBool(); }
	QString nip() const { return get("Knt_Nip").toString(); }
	QString nipCode() const { return get("Knt_NipKraj").toString(); }
	QString regon() const { return get("Knt_Regon").toString(); }
	QString documentId() const { return get("Knt_DokumentTozsamosci").toString(); }
	QString country() const { return get("Knt_Kraj").toString(); }
	QString countryISO() const { return get("Knt_KrajISO").toString(); }
	QString city() const { return get("Knt_Miasto").toString(); }
	QString postOffice() const { return get("Knt_Poczta").toString(); }
	QString postCode() const { return get("Knt_KodPocztowy").toString(); }
	QString street() const { return get("Knt_Ulica").toString(); }
	QString smsPhone() const { return get("Knt_TelefonSms").toString(); }
	QString state() const { return get("Knt_Wojewodztwo").toString(); }
	QString homeNumber() const { return get("Knt_NrDomu").toString(); }
	QString localNumber() const { return get("Knt_NrLokalu").toString(); }
	QString fax() const { return get("Knt_Fax").toString(); }
	QString email() const { return get("Knt_Email").toString(); }
	QString url() const { return get("Knt_URL").toString(); }
	QString pesel() const { return get("Knt_Pesel").toString(); }

	QString name(int index = 0) const {
		switch (index) {
		case 0: return get("Knt_Nazwa1").toString(); break;
		case 1: return get("Knt_Nazwa2").toString(); break;
		case 2: return get("Knt_Nazwa3").toString(); break;
		}
		return QString();
	}
	QString phone(int index = 0) const {
		switch (index) {
		case 0: return get("Knt_Telefon1").toString(); break;
		case 1: return get("Knt_Telefon2").toString(); break;
		case 2: return get("Knt_Telefon3").toString(); break;
		}
		return QString();
	}
};

} // namespace core

#endif // CONTRACTORSCOMARCH_HPP
