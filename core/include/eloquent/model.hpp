#ifndef MODEL_HPP
#define MODEL_HPP

#include "core_global.h"
#include <QMap>
#include <QVariant>
#include <QTextStream>
#include <QJsonObject>
#include <QList>
#include <QHash>
#include "../dbobj.hpp"

namespace core {
namespace eloquent {

// class declaration
//template <typename T> struct Model;

// basic types uses by ORM
using DBIDKey = int;
// invalid db key
constexpr int NullDBIDKey = 0;
// collection of models
template <typename T>
using Collection = QList<T>;


///
/// base dbo model
/// contains methods to manage data direct in database by builder object
/// builder object is SQL query provider for specific database engine
///
template <typename T>
class CORE_EXPORT Builder;

template <typename T>
struct CORE_EXPORT Model : public core::DbObject {
	friend class Builder<T>;

	///@{
	/// ctors
	explicit Model()
		: DbObject{}
		, mBuilder {T::tableName()}
	{
		setId(NullDBIDKey);
	}

	explicit Model(Model<T> const& other)
		: DbObject{}
		, mBuilder{T::tableName()}
	{
		mData = other.mData;
		mDirty.clear();
	}

	virtual ~Model() = default;
	///@}

	///
	/// \brief fillable
	/// return all uses fields in model
	/// where data from model will be stored in db
	/// \return
	///
	virtual QStringList fillable() const {
		return QStringList{};
	}

	///
	/// \brief hidden
	/// override this method in models
	/// if you want to hidden field in exported data (e.g JSON)
	/// \return
	///
	virtual QStringList hidden() const {
		return QStringList{};
	}

	virtual Collection<T> getAllRecords() {
		return builder().get();
	}
	virtual T findRecord(DBIDKey id) {
		return builder().where("Id", id).first();
	}
	virtual bool updateRecord(const Model<T>& model) {
		Q_ASSERT(model.id() > 0);
		return builder().update(model);
	}
	virtual bool createRecord(const Model<T>& model) {
		return builder().insert(model);
	}
	virtual bool destroyRecord(const Model<T>& model) {
		Q_ASSERT(model.id() > 0);
		return builder().where("Id", model.id()).destroy();
	}

	virtual bool save() {
		if (get("Id").isValid() && id() > 0)
			return updateRecord(*this);
		else
			return createRecord(*this);
	}
	virtual bool destroy() {
		Q_ASSERT(id() > 0);
		return destroyRecord(*this);
	}
	virtual bool create() {
		return createRecord(*this);
	}

	virtual eloquent::DBIDKey maxId() const {
		return builder().maxId();
	}
	virtual eloquent::DBIDKey lastInsertedId() const {
		return builder().lastInsertedId();
	}

	///
	/// @brief set set new key
	/// if you want to store data in db
	/// the key must be column name
	/// @param key
	/// @param value
	///
	void set(const QString &key, const QVariant &value) noexcept
		{ mData[key] = value; }
	void setDirty(const QString& key, const QVariant& value) noexcept
	{
		mDirty[key] = value;
	}
	virtual void makeDirty() noexcept
		{ mDirty.clear(); }

	///
	/// @brief get
	/// get value from key (use it to read data from model)
	/// @param key
	/// @return
	///
	QVariant get(const QString &key) const {
//#ifdef _DEBUG
//		bool exist = std::any_of(fillable().begin(), fillable().end(),
//			[&key](const QString& name)->bool{
//				return name == key;
//		});
//		if (!exist)
//			qDebug() << "Brak pola: " << key << " w obiekcie: " << tableName();
//		Q_ASSERT(exist);
//#endif
		return mData[key];
	}
	QVariant getDirty(const QString& key) const
		{ return mDirty[key]; }

	///
	/// @brief removeKey
	/// @param key
	///
	int removeKey(const QString &key)
		{ return mData.remove(key); }

	///
	/// @brief builder
	/// @return
	///
	virtual Builder<T> builder() const {
		return mBuilder;
		//return Builder<T>(tableName());
	}

	///
	/// \brief toString
	/// \return
	///
	virtual QString toString() const
		{ return QString(); }

	QVariant operator [](const char* key) const
		{ return get(key); }
	QVariant& operator [](const char* key)
		{ return mData[key]; }

	DBIDKey id() const
		{ return get("Id").toUInt(); }
	void setId(DBIDKey id)
		{ set("Id", id); }

	bool isValid() const {
		return id() != 0 && mData.size() > 0;
	}
	operator bool() const
		{ return isValid(); }

public:
	///
	/// \brief toJson convert object to JSON
	/// use by server to serialise object before send to clients
	/// \return valid JSON object ready to use
	///
	QJsonObject toJson() const {
		const QStringList& hiddenFields = hidden();
		QJsonObject obj;
		for (const QString& key : mData.keys()) {
			bool exist = std::any_of(hiddenFields.begin(), hiddenFields.end(),
				[&key](const QString& val)->bool{
					return val == key;
			});
			if (exist)
				continue;	// skip fields if exist in hidden group

			obj.insert(key, QJsonValue::fromVariant(mData[key]));
		}
		return obj;
	}
	static T fromJson(const QJsonObject& object) {
		T model {};
		for (const QString& key : object.keys())
			model.set(key, object.value(key).toVariant());
		return model;
	}

	///
	/// \brief createOrUpdate
	/// \param model
	/// \return
	///
	bool createOrUpdate(const T& model) {
		if (model.id() > 0)
			return updateRecord(model);
		else return createRecord(model);
	}

protected:
	template <typename R = Model<T>>
	R belongsTo(
		const QString& tableName,
		const QString &localKey,
		const QString &foreignKey = QString("Id")) const
	{
		Q_UNUSED(tableName);
		Q_ASSERT(get(localKey).toInt() > 0);
		Builder<R> builder(tableName);
		return builder.where(foreignKey, this->get(localKey)).first();
	}

	template <typename R = Model<T>>
	QList<R> hasMany(
		const QString& tableName,
		const QString &foreignKey,
		const QString &localKey = QString("Id")) const
	{
		Q_UNUSED(tableName);
		Q_ASSERT(get(localKey).toInt() > 0);
		Builder<R> builder(tableName);
		return builder.where(foreignKey, this->get(localKey)).get();
	}

	QHash<QString, QVariant> mData;
	QHash<QString, QVariant> mDirty;

	///
	/// \brief keys
	/// return all keys uses in model
	/// \return
	///
	QStringList keys() const
		{ return mData.keys(); }

private:
	Builder<T> mBuilder;
};

} // namespace eloquent
} // namespace core


#endif // MODEL_HPP
