#ifndef BUILDER_HPP
#define BUILDER_HPP

#include "core_global.h"
#include <QString>
#include <QVariant>
#include <QList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include "model.hpp"


namespace core {
namespace eloquent {

template <typename T>
class CORE_EXPORT Builder {
public:
	///@{
	/// ctors
	explicit Builder(
			const QString &tableName,
			const QSqlDatabase &db = QSqlDatabase::database(),
			const QString ns = "dbo")
		: mDatabase {db}
		, mTableClause {tableName}
		, mNs {ns}
		, mLimit {0}
	{
	}
	virtual ~Builder() = default;
	///@}

	bool insert(const Model<T>& model) {
		bool insertID = (model.get("Id").isValid() && model.get("Id").toInt() > 0);
		Model<T> tmp {model};

		// jeśli nie dodajemy id to usuwamy klucz z modelu
		// aby uniknąć błędu SQL
		if (!insertID)
			tmp.removeKey("Id");

		// sprawdzenie kluczy z listą fillable
		// tylko klucze z listy fillable wchodzą w skład bazy danych
		const auto& fillable = T().fillable();
		for (const QString& key : model.keys()) {
			bool exist = std::any_of(fillable.begin(), fillable.end(), [&key](const QString& item)->bool{
				return item == key;
			});
			if (!exist)
				tmp.removeKey(key);
		}

		QStringList keys, values;
		for (const QString& key : tmp.keys()) {
			keys.append(Builder::escapeName(key));
			values.append(Builder::escapeValue(tmp.get(key)));
		}

		QString queryString = QString("INSERT INTO %1 (%2) VALUES (%3)")
			.arg(escapeTable(mTableClause))
			.arg(keys.join(", "))
			.arg(values.join(", "));

#if _DEBUG
		qDebug() << queryString;
#endif
		if (insertID) {
			QSqlQuery query {mDatabase};
			QString identityQuery = QString("SET IDENTITY_INSERT %1 ON").arg(escapeTable(mTableClause));
			if (!query.exec(identityQuery))
				return false;
		}
		QSqlQuery query {mDatabase};
		if (!query.exec(queryString)) {
#ifdef _DEBUG
			qCritical() << query.lastError().text();
#endif
			return false;
		}
		if (insertID) {
			QString identityQuery = QString("SET IDENTITY_INSERT %1 OFF").arg(escapeTable(mTableClause));
			query.clear();
			if (!query.exec(identityQuery))
				return false;
		}
		return true;
	} // insert

	bool update(const Model<T>& model) {
		if (!model.get("Id").isValid()) {
			qCritical() << "You have to set record id to can update it";
			return false;
		}
		const QStringList& fields = T().fillable();
		QStringList sets;
		for (const QString& key : model.keys()) {
			bool exist = std::any_of(fields.begin(), fields.end(), [&key](const QString& name)->bool{
				return key == name;
			});
			if (exist && (key != "Id") && (model.get(key) != model.getDirty(key))) {
				sets.append(QString("%1 = %2")
					.arg(escapeName(key))
					.arg(escapeValue(model.get(key))));
			}
		}
		if (sets.empty()) {
			// nothing to change
			qDebug() << "Nothing to change for " << T::tableName();
			return true;
		}

		QString queryString = QString("UPDATE %1 SET %2 WHERE [Id] = %3")
			.arg(escapeTable(mTableClause))
			.arg(sets.join(", "))
			.arg(model.get("Id").toUInt());

#ifdef _DEBUG
		qDebug() << queryString;
#endif
		QSqlQuery query(mDatabase);
		if (!query.exec(queryString)) {
			qCritical() << query.lastError().text();
			return false;
		}
		return true;
	}

	Builder& where(const QString &key,
				   const QString &op,
				   const QVariant &value,
				   const QString &boolean = "AND")
	{
		if (mWhereClause.size())
			mWhereClause += QString(" %1 ").arg(boolean);

		mWhereClause += QString("%1 %2 %3")
						.arg(escapeName(key))
						.arg(op.toUpper())
						.arg(escapeValue(value));

		return *this;
	}

	Builder& where(const QString &key, const QVariant &value)
		{ return where(key, "=", value); }

	Builder& limit(int limit) {
		mLimit = limit;
		return *this;
	}

	Builder& orderBy(const QString& col, const QString& type = "DESC") {
		if (!mOrderByClause.isEmpty())
			return *this;

		mOrderByClause = QString("ORDER BY %1 %2")
			.arg(escapeName(col), type);

		return *this;
	}

	QList<T> get(const QString &columns = "*") {
		QString queryString;
		if (mLimit) {
			queryString = QString("SELECT TOP %1 %2 FROM %3")
				.arg(mLimit)
				.arg(columns)
				.arg(escapeTable(mTableClause));
		} else {
			queryString = QString("SELECT %1 FROM %2")
				.arg(columns)
				.arg(escapeTable(mTableClause));
		}

		if (!mWhereClause.isEmpty())
			queryString += QString(" WHERE %1").arg(mWhereClause);

		if (!mOrderByClause.isEmpty())
			queryString += QString(" %1").arg(mOrderByClause);

#ifdef _DEBUG
		qDebug() << queryString;
#endif
		QSqlQuery query(mDatabase);
		if (!query.exec(queryString)) {
#ifdef _DEBUG
			qCritical() << query.lastError().text();
#endif
			return QList<T>();
		}

		QSqlRecord record = query.record();
		QList<T> list;

		const QStringList& fields = T().fillable();
		while (query.next()) {
			T model {};
			for (int i = 0; i < record.count(); i++) {
				const QString fieldName = record.fieldName(i);
				bool exist = std::any_of(fields.begin(), fields.end(), [&fieldName](const QString& name)->bool{
					return fieldName == name;
				});
				if (!exist)
					continue;	// skip if field not exist in fillable list

				model.set(fieldName, query.value(i));
				model.setDirty(fieldName, query.value(i));
			}
			list.append(model);
		}
		return list;
	}

	T first(const QString &columns = "*") {
		Collection<T> list = limit(1).get(columns);
		return list.size() ? list.first() : T {};
	}

	bool destroy() {
		QString queryString = QString("DELETE FROM %1")
			.arg(escapeTable(mTableClause));

		if (mWhereClause.size())
			queryString += QString(" WHERE %1").arg(mWhereClause);

#ifdef _DEBUG
		qDebug() << queryString;
#endif

		QSqlQuery query(mDatabase);
		if (!query.exec(queryString)) {
			qCritical() << query.lastError().text();
			return false;
		}
		return true;
	}

	///@{
	/// only on refactoring time
	/// remove at end of code refactoring
//	Builder operator->() const noexcept { return *this; }
	///@}

	///
	/// \brief database - return used QSqlDatabase instance where all
	/// queries will be exec
	/// \return QSqlDatabase
	///
	QSqlDatabase database() const { return mDatabase; }

	bool beginTransaction() {
		return true;

		QSqlQuery query {mDatabase};
		bool result = query.exec("BEGIN TRANSACTION");
#if _DEBUG
		if (result)
			qDebug() << query.lastQuery();
		else qCritical() << query.lastError().text();
#endif
		return result;
	}

	bool commitTransaction() {
		return true;

		QSqlQuery query {mDatabase};
		bool result = query.exec("COMMIT TRANSACTION");

#if _DEBUG
		if (result)
			qDebug() << query.lastQuery();
		else qCritical() << query.lastError().text();
#endif
		return result;
	}
	bool rollbackTransaction() {
		return true;

		QSqlQuery query {mDatabase};
		bool result = query.exec("ROLLBACK TRANSACTION");

#if _DEBUG
		if (result)
			qDebug() << query.lastQuery();
		else qCritical() << query.lastError().text();
#endif
		return result;
	}

	DBIDKey maxId() const {
		const QString queryString = QString("SELECT MAX(Id) AS [Id] FROM %1").arg(escapeTable(mTableClause));
		QSqlQuery query {mDatabase};
		bool result = query.exec(queryString) && query.first();

#if _DEBUG
		if (result)
			qDebug() << query.lastQuery();
		else qCritical() << query.lastError().text();
#else
		Q_UNUSED(result);
#endif
		return query.value("Id").toInt();
	}
	DBIDKey lastInsertedId() const {
		const QString queryString = QString("SELECT IDENT_CURRENT('%1') AS Id").arg(mTableClause);
		QSqlQuery query {mDatabase};
		bool result = query.exec(queryString) && query.first();

#if _DEBUG
		if (result)
			qDebug() << query.lastQuery();
		else qCritical() << query.lastError();
#endif
		return result ? query.value("Id").value<DBIDKey>() : -1;
	}

	///
	/// \brief currentDatabase
	/// \return current instance to connection with database
	///
	QSqlDatabase& currentDatabase() { return mDatabase; }

private:
	QSqlDatabase mDatabase;
	QString mTableClause;
	QString mWhereClause;
	QString mOrderByClause;
	QString mNs;
	int mLimit;

	inline QString escapeTable(const QString& name) const
		{ return QString("[%1].[%2]").arg(mNs, name); }

	inline QString escapeName(const QString& name) const
		{ return QString("[%1]").arg(name); }

	QString escapeValue(const QVariant& data) const {
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
		switch (data.typeId()) {
		case QMetaType::Type::Bool:
			return QString::number(data.toBool() ? 1 : 0);

		case QMetaType::Type::Int:
			return QString::number(data.toInt());

		case QMetaType::Type::Double:
			return QString::number(data.toDouble());

		case QMetaType::Type::QDate:
			return QString("'%1'").arg(data.toDate().toString("yyyy-MM-dd"));

		default:
			return QString("'%1'").arg(data.toString());
		}
#elif QT_VERSION >= QT_VERSION_CHECK(5, 12 ,0)
		//
		// deprecated version only for support win7 and win8
		//
		switch (data.type()) {
		case QVariant::Type::Invalid:
			return QString::fromLatin1("NULL");

		case QVariant::Type::Bool:
			return QString::number(data.toBool() ? 1 : 0);

		case QVariant::Type::Int:
			return QString::number(data.toInt());

		case QVariant::Type::Double:
			return QString::number(data.toDouble());

		case QVariant::Type::Date:
			return QString("'%1'").arg(data.toDate().toString("yyyy-MM-dd"));

		default:
			return QString("'%1'").arg(data.toString());
		}
#endif
	}
};

} // namespace eloquent
} // namespace core


#endif // BUILDER_HPP
