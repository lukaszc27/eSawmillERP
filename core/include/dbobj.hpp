#ifndef DBOBJ_HPP
#define DBOBJ_HPP

#include <QObject>
#include "core_global.h"

namespace core {

/**
 * @brief The DbObject class
 * klasa bazowa dla wszystkich obiektów pracujących w bazie danych
 */
class CORE_EXPORT DbObject
{
public:
	explicit DbObject() { setChecked(false); }
	~DbObject() {}

	inline void setChecked(bool checked) { mIsChecked = checked; }

	inline bool isChecked() const { return mIsChecked; }

protected:
	bool mIsChecked;
};

}

#endif // DBOBJ_HPP
