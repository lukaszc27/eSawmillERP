#ifndef GLOBALDATA_HPP
#define GLOBALDATA_HPP

#include "core_global.h"
#include "repository/abstractrepository.hpp"
#include "logs/abstractlogger.hpp"

namespace core {

class CommandExecutor;

///
/// \brief The GlobalData struct
/// object share global data (e.g repositories, configurations, app name and version...)
///
struct CORE_EXPORT GlobalData {
	///
	/// \brief instance
	/// \return
	///
	static GlobalData& instance();

	///
	/// \brief commandExecutor
	/// get current command executor
	/// (implements command pattern)
	/// this object can execute core::abstracts::AbstracCommand interface
	/// \return
	///
	CommandExecutor& commandExecutor() const;

	///
	/// \brief repository
	/// \return
	///
	repository::AbstractRepository& repository() const;

	///
	/// \brief logger
	/// get default logger uses in app
	/// \return
	///
	core::logs::AbstractLogger& logger() const;

	///
	/// \brief currentApplicationVersion
	/// get current application version
	/// \return
	///
	QVersionNumber currentApplicationVersion() const;
};

}

#endif // GLOBALDATA_HPP
