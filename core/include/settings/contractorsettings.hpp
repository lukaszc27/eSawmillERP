#ifndef CONTRACTORSETTINGS_HPP
#define CONTRACTORSETTINGS_HPP

#include "../core_global.h"
#include "abstractsettings.hpp"

namespace core::settings {

class CORE_EXPORT ContractorSettings final : public core::settings::AbstractSettings {
	Q_OBJECT

public:
	explicit ContractorSettings(QObject* parent = nullptr);
	~ContractorSettings() {}

	static ContractorSettings& instance();

protected:
	virtual QString group() const override;
};

} // namespace core::settings

#endif // CONTRACTORSETTINGS_HPP
