#ifndef SALEINVOICESETTINGS_HPP
#define SALEINVOICESETTINGS_HPP

#include "../../core_global.h"
#include "../abstractsettings.hpp"

namespace core::settings::invoices {

class CORE_EXPORT SaleInvoiceSettings final : public core::settings::AbstractSettings {
	Q_OBJECT

public:
	SaleInvoiceSettings(QObject* parent = nullptr);
	virtual ~SaleInvoiceSettings() = default;

	///
	/// \brief instance - return instance for this object
	/// this method make singelton
	/// \return
	///
	static SaleInvoiceSettings& instance();

protected:
	virtual QString group() const override;
};

} // namespace core::settings::invoices

#endif // SALEINVOICESETTINGS_HPP
