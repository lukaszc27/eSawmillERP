#ifndef PURCHASEINVOICESETTINGS_HPP
#define PURCHASEINVOICESETTINGS_HPP

#include "../../core_global.h"
#include "../abstractsettings.hpp"


namespace core::settings::invoices {

class CORE_EXPORT PurchaseInvoiceSettings final : public core::settings::AbstractSettings {
	Q_OBJECT

public:
	PurchaseInvoiceSettings(QObject* parent = nullptr);
	virtual ~PurchaseInvoiceSettings() = default;

	///
	/// \brief instance
	/// \return
	///
	static PurchaseInvoiceSettings& instance();

protected:
	///
	/// \brief group - group name in settings file
	/// \return
	///
	virtual QString group() const override;
};

} // namespace core::settings::invoices

#endif // PURCHASEINVOICESETTINGS_HPP
