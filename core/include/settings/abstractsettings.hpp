#ifndef ABSTRACTSETTINGS_HPP
#define ABSTRACTSETTINGS_HPP

#include "../core_global.h"
#include <QSettings>
#include <QWidget>

namespace core::settings {

class CORE_EXPORT AbstractSettings : public QSettings {
public:
	explicit AbstractSettings(QObject* parent = nullptr);
	virtual ~AbstractSettings() = default;

protected:
	///
	/// \brief group name in settings file
	/// \return
	///
	virtual QString group() const = 0;
};

} // namespace core::settings

#endif // ABSTRACTSETTINGS_HPP
