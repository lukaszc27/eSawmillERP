#ifndef APPSETTINGS_HPP
#define APPSETTINGS_HPP

#include "../core_global.h"
#include <QSettings>
#include <QVersionNumber>
#include "ordersettings.hpp"
#include <QMainWindow>
#include "invoices/saleinvoicesettings.hpp"
#include "invoices/purchaseinvoicesettings.hpp"
#include "contractorsettings.hpp"


namespace core::settings {

///
/// \brief The AuthSettings struct
/// struct store information to connect to db
///
struct CORE_EXPORT AuthInformation {
	AuthInformation()
		: driver {}
		, host {}
		, databaseName {}
		, userName {}
		, password {}
		, trustedConnection {false}
		, windowsAuthorization {false}
	{
	}

	QString driver;
	QString host;
	QString databaseName;
	QString userName;
	QString password;
	bool trustedConnection;
	bool windowsAuthorization;

	///
	/// \brief connectionString - return connection string to connect to db
	/// \return
	///
	QString connectionString() const;
};

enum class RepositoryMode
{
	Database	= 0,	///< direct connection to database
	Remote		= 1		///< connect to remote service
};

///
/// \brief The AppSettings class
/// main class to manage configuration in application
///
class CORE_EXPORT AppSettings final : public QSettings {
	Q_OBJECT
public:
	AppSettings(QObject* parent = nullptr);
	virtual ~AppSettings() = default;

	///
	/// \brief instance
	/// \return
	///
	static AppSettings& instance();

	///
	/// \brief applicationVersion return main app version
	/// \return
	///
	QVersionNumber applicationVersion() const;

	RepositoryMode repositoryMode() const noexcept {
		const auto type = this->value("DataProvider").toInt();
		return static_cast<RepositoryMode>(type);
	}

	///
	/// \brief order
	/// app configuration for order module
	/// \return
	///
	static OrderSettings& order() {
		return OrderSettings::instance();
	}

	///
	/// \brief saleInvoiceSettings
	/// app configuration for sale invocie widget group
	/// \return
	///
	static invoices::SaleInvoiceSettings& saleInvoice() {
		return invoices::SaleInvoiceSettings::instance();
	}

	///
	/// \brief purchaseInvocieSettings
	/// \return
	///
	static invoices::PurchaseInvoiceSettings& purchaseInvocie() {
		return invoices::PurchaseInvoiceSettings::instance();
	}

	///
	/// \brief contractorSettings
	/// \return
	///
	static ContractorSettings& contractor() {
		return ContractorSettings::instance();
	}

	///
	/// \brief saveMainWindowGeometry
	/// read and save main window geometry
	/// \param window object to save/restore geometry
	///
	void saveMainWindowGeometry(const QMainWindow* window);
	void readMainWindowGeometry(QMainWindow* window);

	void saveWidgetGeometry(const QWidget* widget, const QString& group);
	void readWidgetGeometry(QWidget* widget, const QString& group);

	///
	/// \brief saveAuthDialogGeometry
	/// read and save auth dialog geometry
	/// \param widget
	///
	void saveAuthDialogGeometry(const QWidget* widget);
	void readAuthDialogGeometry(QWidget* widget);

	AuthInformation readDatabaseAuthInformation();
	void writeDatabaseAuthInformation(const AuthInformation& info);
};

} // namespace core::settings

#endif // APPSETTINGS_HPP
