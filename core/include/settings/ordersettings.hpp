#ifndef ORDERSETTINGS_HPP
#define ORDERSETTINGS_HPP

#include <QColor>
#include "abstractsettings.hpp"
#include "../core_global.h"


namespace core::settings {

///
/// \brief The OrderSettings class
/// store global configuration for orders module
///
class CORE_EXPORT OrderSettings : public AbstractSettings {
protected:
	QString group() const { return "Order/"; }

public:
	OrderSettings(QObject* parent = nullptr);
	virtual ~OrderSettings() = default;

	///
	/// \brief instance
	/// \return
	///
	static OrderSettings& instance();

	///
	/// \brief defaultPrice get default price store in main app settings
	/// \return
	///
	double defaultPrice() const;
	void setDefaultPrice(double price);

	///
	/// \brief defaultPlannedPrice additional price for palnned items
	/// \return
	///
	double defaultPlannedPrice() const;
	void setDefaultPlannedPrice(double price);

	///
	/// \brief deadlineAfterRemind
	/// \return
	///
	bool deadlineAfterRemind() const;
	void setDeadlineAfterRemind(bool remind);
	QColor deadlineAfterRemindColor() const;
	void setDeadlineAfterRemindColor(const QColor& color);

	///
	/// \brief deadlineBeforeRemind
	/// \return
	///
	bool deadlineBeforeRemind() const;
	void setDeadlineBeforeRemind(bool remind);
	QColor deadlineBeforeRemindColor() const;
	void setDeadlineBefoteRemindColor(const QColor& color);
	///
	/// \brief markByPriority highlight orders by type of priority
	/// \return
	///
	bool markByPriority() const;
	void setMarkByPriority(bool enable);

	///
	/// \brief realisedColor color to mark realised orders
	/// \return
	///
	QColor realisedColor() const;
	void setRealisedColor(const QColor& color);

	///
	/// \brief daysToWarning maximum days to remind (if order wasn't realised)
	/// \return
	///
	int daysToWarning() const;
	void setDaysToWarning(int days);

	///
	/// \brief documentPrefix this characters will be start order number
	/// \return
	///
	QString documentPrefix() const;
	void setDocumentPrefix(const QString& prefix);

	///
	/// \brief minimumRealisedTime minimum number of days to realised one order
	/// \return
	///
	int minimumRealisedTime() const;
	void setMinimumRealisedTime(double days);

	///
	/// \brief defaultItemQuantity
	/// return default quantity for item
	/// used in dialog to add eleemnts to order
	/// \return
	///
	int defaultItemQuantity() const;

	///
	/// \brief defaultItemName
	/// return default item name
	/// used in dialog to add element to order
	/// \return
	///
	QString defaultItemName() const;
};

} // namespace core::settings

#endif // ORDERSETTINGS_HPP
