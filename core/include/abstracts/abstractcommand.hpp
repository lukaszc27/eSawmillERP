#ifndef ABSTRACTCOMMAND_HPP
#define ABSTRACTCOMMAND_HPP

#include "../core_global.h"
#include "../repository/abstractrepository.hpp"

namespace core::abstracts {

/// @brief AbstractCommand interface
/// base class for all commands existing in system
/// is has some pure virtual methods to override in next clasess
struct CORE_EXPORT AbstractCommand {
	explicit AbstractCommand(QWidget* parent = nullptr)
		: _parent {parent}
	{
	}

	virtual ~AbstractCommand() = default;

	/// @brief execute command in command executor main object
	/// this function wil be running when user make action in GUI layer
	/// @param repository 
	/// @return 
	virtual bool execute(core::repository::AbstractRepository& repository) = 0;

	/// @brief canUndo function
	/// @return true - operation from execute command can be undo
	virtual bool canUndo() const {
		return false;
	}

	/// @brief undo function
	/// undo all operation executed in command by execute function
	/// @param repository current repository in use
	/// @return true - when all operation will be undo
	virtual bool undo(core::repository::AbstractRepository& repository) {
		Q_UNUSED(repository);
		return false;
	}

	/// @brief overrideCursor function
	/// return true when mouse cursor schould be override
	/// to simulate waiting
	virtual bool overrideCursor() const { return true; }

protected:
	QWidget* _parent;	///< pointer to the widget from which the command was sent

	/// @brief parent method
	/// return pointer to parent widget uses by interactive dialogs with users
	QWidget* parent() const { return _parent; }
};

} // namespace core::abstracts

#endif // ABSTRACTCOMMAND_HPP
