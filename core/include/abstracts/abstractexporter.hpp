#ifndef ABSTRACTEXPORTER_H
#define ABSTRACTEXPORTER_H

#include "../core_global.h"
#include "abstractmodel.h"
#include "eloquent/model.hpp"

#include "../core_global.h"
#include <QObject>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QIODevice>
#include <QDomElement>


namespace core::abstracts {

///
/// \brief The AbstractExporter class
/// primary abstract class to implement
/// import and export data in app
///
template <typename T>
class AbstractExporter {
public:
	/// ctors
	explicit AbstractExporter() {
	}

	virtual bool exportData(QIODevice& device) = 0;
	virtual bool importData(QIODevice& device) = 0;

	///
	/// convert data to XML nodes
	///
	virtual QDomElement toDomNode(QDomDocument& document) {
		Q_UNUSED(document);
		return QDomElement();
	};

	///
	/// \brief fromDomNode
	/// \param element
	///
	virtual void fromDomNode(const QDomElement& element) {
		Q_UNUSED(element);
	}

	///
	/// \brief setData setter for mData field
	/// \param data
	///
	void setData(const QList<T>& elements) {
		mData = elements;
		notifyDataChanged();
	}
	void setData(const T& model) {
		mData.clear();
		mData.append(model);
		notifyDataChanged();
	}
	void setData(core::models::AbstractModel<T>* model) { mData = model->items(); }

	///
	/// \brief data - getter for mData field
	/// \return reference to map with data to export/import
	///
	QList<T>& data()	{ return mData; }

protected:
	QList<T> mData;

	///
	/// \brief notifyDataChanged
	/// notify when data will be changed
	///
	virtual void notifyDataChanged() {}
};

} // namespace

#endif // ABSTRACTEXPORTER_H
