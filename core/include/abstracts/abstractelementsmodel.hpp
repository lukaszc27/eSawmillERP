#ifndef ABSTRACTELEMENTSMODEL_HPP
#define ABSTRACTELEMENTSMODEL_HPP

#include "core_global.h"
#include "abstractmodel.h"
#include <QDomDocument>
#include <QDomElement>
#include <functional>


namespace core::models {

template <typename T>
class AbstractElementsModel : public core::models::AbstractModel<T> {
public:
//	explicit AbstractElementsModel(QObject* parent = nullptr)
//		: core::models::AbstractModel<T>{parent}
//		, mId {0}
//		, mPrice {0}
//		, mTax {0}
//		, mRebate {0}
//		, mSpareLength {0}
//		, mOnPriceChanged {nullptr}
//		, mOnTaxChanged {nullptr}
//		, mOnRebateChanged {nullptr}
//		, mOnSpareLengthChanged {nullptr}
//	{
//	}

	explicit AbstractElementsModel(QObject* parent = nullptr)
		: AbstractModel<T> {parent}
		, mPrice {0}
		, mTax {0}
		, mRebate {0}
		, mSpareLength {0}
		, mOnPriceChanged {nullptr}
		, mOnTaxChanged {nullptr}
		, mOnRebateChanged {nullptr}
		, mOnSpareLengthChanged {nullptr}
	{
//		mId = id;
	}
	virtual ~AbstractElementsModel() = default;

	///
	/// \brief totalCubature - return sum of all elements cubature
	/// \return double
	///
	virtual double totalCubature() const = 0;

	///
	/// \brief totalPlannedCubature - return sum of cubature only planned elements
	/// \return double
	///
	virtual double totalPlannedCubature() const {
		return 0.0f;
	}

	///
	/// \brief importFromXML - import elements to model from QDomDocument
	/// \param doc QDomDocument object
	///
	virtual bool importFromXML(const QDomDocument* doc) = 0;

	///
	/// \brief exportToXML - export all elements from list to XML format
	/// \return QDomDocuemnt
	///
	virtual QDomDocument exportToXML() = 0;

	///
	/// \brief toDomElement - make DOM tree form all elements in list
	/// used in export to XML file
	/// \return
	///
	virtual QDomElement toDomElement() const = 0;
	virtual void fromDomElement(QDomElement& element) = 0;


	// getters
	double price() const { return mPrice; }
	double Tax() const { return mTax; }
	double rebate() const { return mRebate; }
	double spareLength() const { return mSpareLength; }
//	core::eloquent::DBIDKey id() const { return mId; }

	// setters
	virtual void setPrice(double price) {
		this->beginResetModel();
		mPrice = price;
		notifyPriceValueChanged(price);
		this->endResetModel();
	}
	virtual void setTax(double Tax) {
		this->beginResetModel();
		mTax = Tax;
		notifyTaxValueChanged(Tax);
		this->endResetModel();
	}
	virtual void setRebate(double rebate) {
		this->beginResetModel();
		mRebate = rebate;
		notifyRebateValueChanged(rebate);
		this->endResetModel();
	}
	virtual void setSpareLength(double value) {
		this->beginResetModel();
		mSpareLength = value;
		notifySpareLengthChanged(value);
		this->endResetModel();
	}
//	virtual void setId(core::eloquent::DBIDKey id) { mId = id; }

	///
	/// \brief onPriceChanged
	/// assign function to run when price will be changed
	/// \param func - method to execute
	///
	void onPriceChanged(std::function<void(double)> func) {
		Q_ASSERT(func);
		mOnPriceChanged = func;
	}

	///
	/// \brief onTaxChanged
	/// assign function to run when tax will be changed
	/// \param func - method to execute
	///
	void onTaxChanged(std::function<void(double)> func) {
		Q_ASSERT(func);
		mOnTaxChanged = func;
	}

	///
	/// \brief onRebateChanged
	/// assign function to execute when rebate will be changed
	/// \param func
	///
	void onRebateChanged(std::function<void(double)> func) {
		Q_ASSERT(func);
		mOnRebateChanged = func;
	}

	void onSpareLengthChanged(std::function<void(double)> func) {
		Q_ASSERT(func);
		mOnSpareLengthChanged = func;
	}

private:
//	core::eloquent::DBIDKey mId;
//	T& _object;
	double mPrice;
	double mTax;
	double mRebate;
	double mSpareLength;

	std::function<void(double)> mOnPriceChanged;		///< this function will be running every time when price was update
	std::function<void(double)> mOnTaxChanged;			///< running every time when tax will be change
	std::function<void(double)> mOnRebateChanged;		///< running every time when rebate will be change
	std::function<void(double)> mOnSpareLengthChanged;	///< this function will be running every time when spare length was change

protected:

	///
	/// \brief priceValueChanged
	/// this method is running after notifyDataChanged when
	/// price must be update
	/// \param actual price (after changed)
	///
	virtual void notifyPriceValueChanged(double price) {
		if (mOnPriceChanged)
			mOnPriceChanged(price);
	}

	///
	/// \brief notifyTaxValueChanged
	/// \param newTax - actual tax value
	///
	virtual void notifyTaxValueChanged(double tax) {
		if (mOnTaxChanged)
			mOnTaxChanged(tax);
	}

	///
	/// \brief notifyRebateValueChanged
	/// \param newRebate - actual rebate
	///
	virtual void notifyRebateValueChanged(double rebate) {
		if (mOnRebateChanged)
			mOnRebateChanged(rebate);
	}

	///
	/// \brief notifySpareLengthChanged
	/// \param newSpareLength - actual spare length
	///
	virtual void notifySpareLengthChanged(double spareLength) {
		if (mOnSpareLengthChanged)
			mOnSpareLengthChanged(spareLength);
	}
};

} // namespace

#endif // ABSTRACTELEMENTSMODEL_HPP
