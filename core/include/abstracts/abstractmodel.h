#ifndef ABSTRACTMODEL_H
#define ABSTRACTMODEL_H

#include "../core_global.h"
#include <QAbstractTableModel>
#include <QList>
#include <functional>
#include "../eloquent/model.hpp"


namespace core::models {

///
/// \brief The AbstractModel class
/// basic abstract class for all models in app
/// it supply basic virtual methods to manage data in model (e.g insert/remove)
///
/// this class was add 31 of May 2020
///
template <typename T>
class AbstractModel : public QAbstractTableModel {
public:
	///@{
	/// ctors
	AbstractModel(QObject* parent = nullptr)
		: QAbstractTableModel {parent}
		, mReadOnly {false}
	{
	}
	virtual ~AbstractModel() = default;
	///@}

	///@{
	/// basic Qt framework methods witch has to overrides to display data in model
	///
	/// \brief rowCount - return row count in model
	/// is it size of mList returned by size() method
	/// \param index
	/// \return
	///
	int rowCount(const QModelIndex &index) const override {
		Q_UNUSED(index);
		return mList.size();
	}

	///
	/// \brief columnCount return column count in model
	/// is it size of headerData() list returned by size() method
	/// \param index
	/// \return
	///
	int columnCount(const QModelIndex &index) const override {
		Q_UNUSED(index);
		return headerData().size();
	}
	///@}

	///@{
	///
	/// \brief createOrUpdate - DEPRECATED
	/// create or update existing item in model and database
	/// this method are deprecated and schould be removed in next refactoring time
	/// \deprecated schould be removed in next refactoring time
	/// \param element
	///
	virtual void createOrUpdate(T element) = 0;

	///
	/// \brief destroy - DEPRECATED
	/// destroy item from model and in database
	/// this method are deprecated and schould be removed in next refactoring time
	/// \deprecated schould be removed in next refactoring time
	/// \param element
	///
	virtual void destroy(T element) = 0;
	///@}

	///
	/// \brief get
	/// get items from source (e.g database/file)
	///
	virtual void get() = 0;

	///
	/// \brief hasSelectedItems
	/// check if user check rows in model
	/// \deprecated schould be remove in next refactoring time
	/// \return
	///
	bool hasSelectedItems() const {
		bool checked {false};
		for (const auto& item : mList) {
			if (item.isChecked()) {
				checked = true;
				break;
			}
		}
		return checked;
	}

	///
	/// \brief deleteSelectedItems
	/// remove all marked item as selected
	/// (has set checked() flag)
	/// \deprecated schould be removed in next refactoring time
	///
	virtual void deleteSelectedItems() {
		beginResetModel();
		QList<T> tmp = mList;
		mList.clear();
		for (auto& item : tmp) {
			if (!item.isChecked())
				mList.append(item);
		}
		notifyDataChanged();
		endResetModel();
	}

	///
	/// \brief insert
	/// add new item to list so that can be display in model
	/// \param element
	///
	virtual void insert(T element) {
		beginResetModel();
		mList.append(element);
		notifyDataChanged();
		endResetModel();
	}
	virtual void insert(QList<T> elements) {
		beginResetModel();
		mList.append(elements);
		notifyDataChanged();
		endResetModel();
	}

	///
	/// \brief items
	/// return list uses to display data in model
	/// \return
	///
	QList<T>& items()		{ return mList; }	// return reference to list
	QList<T> items() const	{ return mList; }

	///
	/// \brief clear - clear main list in model
	///
	void clear() {
		beginResetModel();
		mList.clear();
		notifyDataChanged();
		endResetModel();
	}

	///
	/// \brief headerData
	/// return titles for columns shown in header row
	/// \return list with titles
	///
	virtual QStringList headerData() const = 0;

	///
	/// \brief totalPrice
	/// return total costs for elements in model
	/// uses in order/service items model and articles model
	/// \return price in PLN
	///
	virtual double totalPrice() {
		return 0;
	}

	void setReadOnly(bool readonly) {
		beginResetModel();
		mReadOnly = readonly;
		endResetModel();
	}
	bool readOnly() const { return mReadOnly; }
	core::eloquent::DBIDKey parentObjectId() const { return mParentObjectId; }
	void setParentObjectId(core::eloquent::DBIDKey parentId) { mParentObjectId = parentId; }

	void startInsertData() { beginResetModel(); }
	void endInsertData() { endResetModel(); }

	void onDataChanged(std::function<void()> func) {
		Q_ASSERT(func);
		mOnDataChaged = func;
	}

protected:
	QList<T> mList;								///< list uses to display data
	bool mReadOnly;								///< mark model as read only
	core::eloquent::DBIDKey mParentObjectId;	///< if model is sb assigned to other data structure (example order elements in create order dialog)
	std::function<void()> mOnDataChaged;		///< method to execute when data in model will be changed

	///
	/// \brief notifyDataChanged
	/// this mmethod is running every time when data in models will be change
	///
	virtual void notifyDataChanged() {
		if (mOnDataChaged)
			mOnDataChaged();
	}
};

} // namespace core::models

#endif // ABSTRACTMODEL_H
