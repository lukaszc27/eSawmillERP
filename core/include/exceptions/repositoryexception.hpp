#ifndef REPOSITORYEXCEPTION_HPP
#define REPOSITORYEXCEPTION_HPP

#include "../core_global.h"
#include <QException>

namespace core::exceptions {

///
/// @brief The RepositoryException class
/// exceptions uses with repositories in app
/// throws always when are mistake on repository side
/// and you want to show info for user
///
class CORE_EXPORT RepositoryException : public QException {
public:
	explicit RepositoryException();
	virtual ~RepositoryException() = default;

	void raise() const override;
	RepositoryException* clone() const override;
};

} // namespace core::exceptions

#endif // REPOSITORYEXCEPTION_HPP
