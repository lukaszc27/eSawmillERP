#ifndef USERALREADYLOGGEDINEXCEPTION_HPP
#define USERALREADYLOGGEDINEXCEPTION_HPP

#include "../core_global.h"
#include <QException>

namespace core::exceptions {

///
/// @brief The UserAlreadyLoggedinException class
/// the exception wil be thrown on login time (when the user is logged in on other device)
///
class CORE_EXPORT UserAlreadyLoggedinException : public QException {
public:
	explicit UserAlreadyLoggedinException();
	virtual ~UserAlreadyLoggedinException() = default;

	void raise() const override;
	UserAlreadyLoggedinException* clone() const override;

	void setHostName(const QString& hostName);
	QString hostName() const;

private:
	QString _hostName;
};

}

#endif // USERALREADYLOGGEDINEXCEPTION_HPP
