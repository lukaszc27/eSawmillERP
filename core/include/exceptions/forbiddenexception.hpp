#ifndef FORBIDDENEXCEPTION_HPP
#define FORBIDDENEXCEPTION_HPP

#include "../core_global.h"
#include "../eloquent/model.hpp"
#include <QException>
#include <QTime>

namespace core::exceptions {

///
/// \brief The ForbiddenException class
/// this exception is thrown every time by lock or unlock method
/// when user want to save resource before editing
///
class CORE_EXPORT ForbiddenException : public QException {
public:
	explicit ForbiddenException(core::eloquent::DBIDKey userId, const QTime& time);
	virtual ~ForbiddenException() = default;

	void raise() const override;
	ForbiddenException* clone() const override;

	///@{
	/// getters and setters
	inline void setUserId(const core::eloquent::DBIDKey& userId) { _userId = userId; }
	inline void setTime(const QTime& time) { _time = time; }

	inline core::eloquent::DBIDKey userId() const { return _userId; }
	inline QTime time() const { return _time; }
	///@}

private:
	core::eloquent::DBIDKey _userId;
	QTime _time;
};

} // namespace core::exceptions

#endif // FORBIDDENEXCEPTION_HPP
