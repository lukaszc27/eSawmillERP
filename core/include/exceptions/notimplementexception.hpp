#ifndef NOTIMPLEMENTEXCEPTION_H
#define NOTIMPLEMENTEXCEPTION_H

#include "../core_global.h"
#include <QException>

namespace core::exceptions {

///
/// \brief The NotImplementException class
/// the exceptions uses in repositores (in remote repository)
/// ans is thrown when server didn't have implement current running method
///
class CORE_EXPORT NotImplementException : public QException {
public:
	explicit NotImplementException();
	virtual ~NotImplementException() = default;

	void raise() const override;
	NotImplementException* clone() const override;
};

} // namespace core::exceptions;

#endif // NOTIMPLEMENTEXCEPTION_H
