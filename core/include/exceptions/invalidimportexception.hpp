#ifndef INVALIDIMPORTEXCEPTION_HPP
#define INVALIDIMPORTEXCEPTION_HPP

#include "../core_global.h"
#include <QException>

namespace core::exceptions {

///
/// \brief The InvalidImportException class
/// exception thrown by import/export actions
/// in all allowed modules
///
class CORE_EXPORT InvalidImportException : public QException {
	QString _message;

public:
	explicit InvalidImportException();
	explicit InvalidImportException(const QString& msg);
	virtual ~InvalidImportException() = default;

	QString message() const { return _message; }

	void raise() const override;
	InvalidImportException* clone() const override;
};

} // namespace core::exceptions

#endif // INVALIDIMPORTEXCEPTION_HPP
