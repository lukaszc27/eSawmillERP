#ifndef DEVICEUNREGISTEREDEXCEPTION_HPP
#define DEVICEUNREGISTEREDEXCEPTION_HPP

#include "../core_global.h"
#include <QException>


namespace core::exceptions {

///
/// @brief The DeviceUnregisteredException class
/// exceptions throws every time (on startup app process and user login)
/// when from service will be reveived information aboutt unregistered device (on server side)
///
class CORE_EXPORT DeviceUnregisteredException : public QException {
public:
	explicit DeviceUnregisteredException();
	virtual ~DeviceUnregisteredException() = default;

	void raise() const override;
	DeviceUnregisteredException* clone() const override;
};

}

#endif // DEVICEUNREGISTEREDEXCEPTION_HPP
