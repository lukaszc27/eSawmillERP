#ifndef WOODTYPEDELEGATE_HPP
#define WOODTYPEDELEGATE_HPP

#include "../core_global.h"
#include "../models/woodtypemodel.hpp"
#include <QStyledItemDelegate>

namespace core::delegates {

class CORE_EXPORT WoodTypeDelegate : public QStyledItemDelegate {
	Q_OBJECT

public:
	WoodTypeDelegate(QObject* parent = nullptr);
	~WoodTypeDelegate() {}

	QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
};

} // namespace core

#endif // WOODTYPEDELEGATE_HPP
