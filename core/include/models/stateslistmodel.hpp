#ifndef STATESLISTMODEL_HPP
#define STATESLISTMODEL_HPP

#include "../core_global.h"
#include <QStringListModel>


namespace core::models {

/**
 * @brief The StatesListModel class
 * list of all states in Poland
 * is used in dialog to add new contractor (to local db or comarch)
 */
class CORE_EXPORT StatesListModel : public QStringListModel {
	Q_OBJECT

public:
	StatesListModel(QObject* parent = nullptr);
	~StatesListModel();
};

} // namespace core


#endif // STATESLISTMODEL_HPP
