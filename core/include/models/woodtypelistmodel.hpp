#ifndef WOODTYPELISTMODEL_HPP
#define WOODTYPELISTMODEL_HPP

#include "../core_global.h"
#include "../dbo/woodtype.hpp"
#include <QAbstractListModel>
#include <QList>


namespace core::models {

class CORE_EXPORT WoodTypeListModel : public QAbstractListModel {
//	Q_OBJECT

public:
	WoodTypeListModel(QObject* parent = nullptr);

	Qt::ItemFlags flags(const QModelIndex &index) const override;
	int rowCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	enum Roles {
		Id = Qt::UserRole + 1
	};

	/**
	 * @brief get - pobiera dane z bazy
	 * oraz zapisuje je w mList
	 */
	void get();

private:
	QList<core::WoodType> mList;
};

} // namespace core

#endif // WOODTYPELISTMODEL_HPP
