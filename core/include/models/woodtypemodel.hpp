#ifndef WOODTYPEMODEL_HPP
#define WOODTYPEMODEL_HPP

#include "../core_global.h"
#include "../abstracts/abstractmodel.h"
#include "dbo/woodtype.hpp"


namespace core::models {

///
/// \brief The WoodTypeModel class
/// model presenting data about wood types
///
class CORE_EXPORT WoodTypeModel : public core::models::AbstractModel<core::WoodType> {
public:
	explicit WoodTypeModel(QObject* parent = nullptr);
	virtual ~WoodTypeModel() = default;

	///
	/// \brief The Columns enum
	/// name of columns (indexes)
	///
	enum Columns {
		Name,		///< wood type name
		Factor,		///< price factor
		WoodBark	///< woodbark size in centimeters
	};

	///
	/// \brief The Roles enum
	/// additional roles used in model
	///
	enum Roles {
		Id = Qt::UserRole + 1
	};

	///@{
	/// methods used by Qt framework to manage data
	Qt::ItemFlags flags(const QModelIndex &index) const override;

	QStringList headerData() const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

	///
	/// \brief insertRows - insert new rows to model
	/// \param row - number of row after will be created new rows
	/// \param count - count of rows to add
	/// \param parent (optional)
	/// \return true if operation will be success
	///
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
	///@}

	///
	/// \brief destroy remove wood type object from model and repository
	/// \param woodId - object id to remove
	///
	void destroy(core::eloquent::DBIDKey woodId);

	///@{
	/// specialised method from AbstractModel
	/// this methods are deprecated, in future should avoid this methods because can be removed in next refactoring
	void createOrUpdate(const core::WoodType model) override;
	void destroy(const core::WoodType model) override;
	void get() override;
	///@}
};

} // namespace core

#endif // WOODTYPEMODEL_HPP
