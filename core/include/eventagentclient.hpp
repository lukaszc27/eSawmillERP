#ifndef EVENTAGENTCLIENT_HPP
#define EVENTAGENTCLIENT_HPP

#include "core_global.h"
#include <QThread>
#include <QMutex>
#include <QHostAddress>
#include <QUdpSocket>
#include <queue>
//#include "providers/controlcenterclient.hpp"
#include <unordered_set>
#include <providers/protocol.hpp>
namespace core {

///
/// \brief The EventAgentClient class
/// client for event agent subservice to recived notifications from service
///
class CORE_EXPORT EventAgentClient : public QThread {
	Q_OBJECT

public:
	explicit EventAgentClient(const QHostAddress& address, quint16 port, QObject* parent = nullptr);
	virtual ~EventAgentClient();

	void makeConnection();

	///@{
	static EventAgentClient* createSingelton(const QHostAddress& address, quint16 port, QObject* parent = nullptr);
	static void destroySingelton();
	static EventAgentClient* instance();
	///@}

protected:
	virtual void run() override;

private:
	QUdpSocket* m_socket;
	QHostAddress m_address;
	quint16 m_port;
	QMutex m_mutex;
	std::queue<core::providers::agent::Event> m_events;

private Q_SLOTS:
	void readPendingDatagrams();

Q_SIGNALS:
	void notifyRepositoryEventReceived(const core::providers::agent::RepositoryEvent& event);
	void notifyServerStatusEventReceived(const core::providers::agent::ServerStatusEvent& event);
	void notifyError(QAbstractSocket::SocketError error);
};

} // namespace core

Q_DECLARE_METATYPE(core::providers::agent::ServerStatusEvent);
Q_DECLARE_METATYPE(core::providers::agent::RepositoryEvent);
Q_DECLARE_METATYPE(core::providers::agent::Event);

#endif // EVENTAGENTCLIENT_HPP
