#ifndef ABSTRACTVERIFICATION_HPP
#define ABSTRACTVERIFICATION_HPP

#include "../core_global.h"
#include "notification.hpp"
#include <QList>


namespace core::verifications {

///
/// \brief The AbstractVerification class
/// abstract (base) class for all verifications in app
///
class CORE_EXPORT AbstractVerification {
public:
	//
	// ctor
	AbstractVerification();
	virtual ~AbstractVerification() = default;

	// remove copy ctor and operator
	// only one copy can exist in memory
	AbstractVerification(const AbstractVerification& verification) = delete;
	AbstractVerification& operator=(const AbstractVerification&) = delete;

	///
	/// \brief execute test and add notifications to list
	/// if everything is ok set 'valid' flag on true
	///
	virtual void execute() = 0;
	void operator()() { execute(); }

	///
	/// \brief notifications get all notifications after test
	/// \return
	///
	QList<Notification> notifications() const				{ return mNotifications; }
	void addNotification(const Notification& notification)	{ mNotifications.append(notification); }
	void addNotification(const QString& msg, const Notification::Type& type);

	bool isValid() const { return mValid; }
	operator bool() const { return isValid(); }

protected:
	bool mValid;
	QList<Notification> mNotifications;
};

} // namespace core::verifications

#endif // ABSTRACTVERIFICATION_HPP
