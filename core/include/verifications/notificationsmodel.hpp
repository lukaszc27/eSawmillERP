#ifndef NOTIFICATIONSMODEL_HPP
#define NOTIFICATIONSMODEL_HPP

#include "core_global.h"
#include "notification.hpp"
#include <QAbstractTableModel>


namespace core::verifications {

class CORE_EXPORT NotificationsModel : public QAbstractTableModel {
	Q_OBJECT

public:
	NotificationsModel(QObject* parent = nullptr);
	virtual ~NotificationsModel() = default;

	// columns identificators used in model
	//
	enum Columns {
		DateTime,
		Type,
		Message
	};

	Qt::ItemFlags flags(const QModelIndex& index) const override;
	int rowCount(const QModelIndex& index) const override;
	int columnCount(const QModelIndex& index) const override;

	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	///
	/// \brief appendNotifications - add new item to model
	/// \param notifications
	///
	void appendNotifications(const QList<Notification>& notifications);

private:
	QList<Notification> mNotifications;
};

} // namespace core::verifications

#endif // NOTIFICATIONSMODEL_HPP
