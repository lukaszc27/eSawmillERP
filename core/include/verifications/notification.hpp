#ifndef NOTIFICATION_HPP
#define NOTIFICATION_HPP

#include "../core_global.h"
#include <QString>
#include <QDateTime>


namespace core::verifications {

struct CORE_EXPORT Notification
{
	///
	/// \brief The Type enum
	/// notification types
	///
	enum class Type : short {
		Information,
		Warning,
		Error
	};

	///
	/// \brief Notification - ctor
	/// \param msg - message for user
	/// \param type - notification type
	///
	Notification();
	Notification(const QString& msg, Type type = Type::Information);

	// getters and setters
	QString message() const		{ return mMsg; }
	QDateTime dateTime() const	{ return mDateTime; }
	Type type() const			{ return mType; }

	void setMessage(const QString& msg, const Type& type);
	void setMessage(const QString& message)		{ mMsg = message; }
	void setDateTime(const QDateTime& dateTime) { mDateTime = dateTime; }
	void setType(const Type& type)				{ mType = type; }

private:
	Type mType;
	QString mMsg;
	QDateTime mDateTime;
};

} // namesapce core::verifications

#endif // NOTIFICATION_HPP
