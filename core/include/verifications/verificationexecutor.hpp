#ifndef VERIFICATIONEXECUTOR_HPP
#define VERIFICATIONEXECUTOR_HPP

#include "../core_global.h"
#include "abstractverification.hpp"
#include <QList>


namespace core::verifications {

class CORE_EXPORT VerificationExecutor {
public:
	VerificationExecutor();
	virtual ~VerificationExecutor();

	VerificationExecutor(const VerificationExecutor&) = delete;
	VerificationExecutor& operator=(const VerificationExecutor&) = delete;

	void addVerification(core::verifications::AbstractVerification* verification);

	bool runTests(bool showSummary = true);
	bool operator()(bool showSummary) { return runTests(showSummary); }
	bool isValid() const { return mTestsPassed; }

private:
	bool mTestsPassed;
	QList<core::verifications::AbstractVerification*> mVerifications;
};

} // namespace core::verifications

#endif // VERIFICATIONEXECUTOR_HPP
