#ifndef VERIFICATIONSWIDGET_HPP
#define VERIFICATIONSWIDGET_HPP

#include "../core_global.h"
#include <QDialog>
#include <QTableView>
#include <QPushButton>
#include <QCloseEvent>
#include "notificationsmodel.hpp"


namespace core::verifications {

class CORE_EXPORT VerificationsWidget : public QDialog {
	Q_OBJECT

public:
	VerificationsWidget(QWidget* parent = nullptr);
	virtual ~VerificationsWidget() = default;

	void appendNotifications(const QList<Notification>& notifications) { mNotificationsModel->appendNotifications(notifications); }

private:
	void createWidgets();
	void createModels();
	void createConnections();

	QPushButton* mCloseButton;
	QTableView* mTableView;
	NotificationsModel* mNotificationsModel;
};

} // namespace core::verifications

#endif // VERIFICATIONSWIDGET_HPP
