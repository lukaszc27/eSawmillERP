#include "repository/abstractwarehouserepository.hpp"

namespace core::repository {

void AbstractWarehouseRepository::removeFromCache(const core::eloquent::DBIDKey& warehouseId) {
	Q_ASSERT(warehouseId > 0);

	auto mem = std::find_if(m_cache.begin(), m_cache.end(),
		[&warehouseId](const std::pair<core::eloquent::DBIDKey, core::Warehouse>& item)->bool {
			return item.first == warehouseId;
	});
	if (mem != m_cache.end())
		m_cache.erase(mem);
}

void AbstractWarehouseRepository::updateCache(const core::eloquent::DBIDKey& warehouseId) {
	Q_ASSERT(warehouseId > 0);

	// remove existing warehouse from cache
	removeFromCache(warehouseId);

	// now schould get new warehouse from data provider
	getWarehouse(warehouseId);
}

} // namespace core::repository
