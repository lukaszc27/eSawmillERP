#include "repository/abstractarticlerepository.hpp"

namespace core::repository {

void AbstractArticleRepository::updateCache(const core::eloquent::DBIDKey& articleId) {
	Q_ASSERT(articleId > 0);

	// first remove from cache
	removeFromCache(articleId);

	// schould get article from data provider
	getArticle(articleId);
}

void AbstractArticleRepository::removeFromCache(const eloquent::DBIDKey& articleId) {
	Q_ASSERT(articleId > 0);

	auto itr = m_articleCache.begin();
	{
		QReadLocker locker(&m_articleCacheLock);
		itr = std::find_if(m_articleCache.begin(), m_articleCache.end(),
			[&articleId](const std::pair<core::eloquent::DBIDKey, core::Article>& item)->bool {
				return item.first == articleId;
		});
	}

	QWriteLocker locker(&m_articleCacheLock);
	if (itr != m_articleCache.end())
		m_articleCache.erase(itr);
}

} // namespace core::repository
