#include "repository/remote/remotewoodtyperepository.hpp"
#include "providers/controlcenterclient.hpp"
#include <QWriteLocker>
#include <QReadLocker>

namespace core::repository::remote {

QList<WoodType> RemoteWoodTypeRepository::getAll() {
	QList<core::WoodType> woods;

	QReadLocker read_locker(&m_cache_lock);
	if (m_cache.size() == 0) {
		read_locker.unlock();

		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getAllWoodsType(woods) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (!success)
			return QList<core::WoodType>{};

		{ // update cache memory
			QWriteLocker locker(&m_cache_lock);
			for (const auto& wood : woods)
				m_cache[wood.id()] = wood;
		}
	} else {
		// get woods from cache
		read_locker.relock();
		for (const auto& [id, type] : m_cache)
			woods.append(type);
	}
	return woods;
}

bool RemoteWoodTypeRepository::create(WoodType& wood) {
	Q_ASSERT(wood.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	core::WoodType obj {wood};
	bool success = client->createWoodType(obj) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		// update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_cache[obj.id()] = obj;
	}
	return success;
}

bool RemoteWoodTypeRepository::update(WoodType& wood) {
	Q_ASSERT(wood.id() > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->updateWoodType(wood) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_cache[wood.id()] = wood;
	}
	return success;
}

bool RemoteWoodTypeRepository::destroy(eloquent::DBIDKey& key) {
	Q_ASSERT(key > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->destroyWoodType(key) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// remove from cache
		QWriteLocker locker(&m_cache_lock);
		auto mem = std::find_if(m_cache.begin(), m_cache.end(), Comparator{key});
		if (mem != m_cache.end())
			m_cache.erase(mem);
	}
	return success;
}

WoodType RemoteWoodTypeRepository::get(eloquent::DBIDKey id) {
	Q_ASSERT(id > 0);
	{ // first check cache memory
		QReadLocker locker(&m_cache_lock);
		auto itr = std::find_if(m_cache.begin(), m_cache.end(), Comparator{id});
		if (itr != m_cache.end())
			return itr->second;
	}

	// get from service
	auto* client = core::providers::ControlCenterClient::instance();
	core::WoodType wood;
	bool success = client->getWoodType(id, wood) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_cache[wood.id()] = wood;
	}
	return wood;
}

} // namespace core::repository::remote
