#include "repository/remote/remotewarehouserepository.hpp"
#include "dbo/wareHouse.h"
#include "providers/controlcenterclient.hpp"
#include "exceptions/notimplementexception.hpp"
#include "globaldata.hpp"
#include "providers/protocol.hpp"

namespace agent = core::providers::agent;

namespace core::repository::remote {

QList<Warehouse> RemoteWarehouseRepository::getAll() {
	QList<core::Warehouse> warehouses;
	if (m_cache.size() == 0) {
		auto* client = core::providers::ControlCenterClient::instance();
		warehouses.clear();
		if (client->getAllWarehouses(warehouses) != core::providers::ControlCenterClient::StatusCode::Ok)
			throw exceptions::NotImplementException();

		// update cache memory
		for (const auto& item : warehouses)
			m_cache[item.id()] = item;
	} else {
		// get warehouses from cache memory
		for (const auto& [id, warehouse] : m_cache)
			warehouses.append(warehouse);
	}

	return warehouses;
}

Warehouse RemoteWarehouseRepository::getWarehouse(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);

	// before get warehouse via server
	// check mem cache
	auto mem = std::find_if(m_cache.begin(), m_cache.end(),
		[&warehouseId](const std::pair<core::eloquent::DBIDKey, core::Warehouse>& item)->bool{
			return item.first == warehouseId;
	});
	if (mem != m_cache.end())
		return mem->second;	// return warehouse from cache

	auto* client = core::providers::ControlCenterClient::instance();
	core::Warehouse warehouse;
	if (client->getWarehouse(warehouseId, warehouse) != core::providers::ControlCenterClient::StatusCode::Ok)
		throw core::exceptions::NotImplementException();

	// update cache memory and return warehouse object
	m_cache[warehouseId] = warehouse;
	return warehouse;
}

bool RemoteWarehouseRepository::create(Warehouse& warehouse) {
	Q_ASSERT(warehouse.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	core::Warehouse obj {warehouse};
	bool success = client->createWarehouse(obj) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		// if object will be create in database
		// we have to update cache memory
		Q_ASSERT(obj.id() > 0);
		m_cache[obj.id()] = obj;
	}
	return success;
}

bool RemoteWarehouseRepository::update(Warehouse& warehouse) {
	Q_ASSERT(warehouse.id() > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	core::Warehouse obj {warehouse};
	bool success = client->updateWarehouse(obj) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// if object will be updated on server side
		// we have to update cache memory
		Q_ASSERT(obj.id() > 0);
		m_cache[obj.id()] = obj;
	}
	return success;
}

bool RemoteWarehouseRepository::destroy(const eloquent::DBIDKey& warehouseId) {
	Q_ASSERT(warehouseId > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->destroyWarehouse(warehouseId) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// if object will be destroy on server side
		// we have to remove it on client side
		auto mem = std::find_if(m_cache.begin(), m_cache.end(),
			[&warehouseId](const std::pair<core::eloquent::DBIDKey, core::Warehouse>& item)->bool{
				return item.first == warehouseId;
		});
		if (mem != m_cache.end())
			m_cache.erase(mem);
		else {
			// whe we can't find warehouse object in cache memory
			// we have to return false because in other else user still can see removed warehouse
			// when we return false in this place, user schould see informaction about error
			return false;
		}
	}
	return success;
}

QList<Article> RemoteWarehouseRepository::getArticles(eloquent::DBIDKey warehouseId) {
	// in this repository we don't update cache for articles assigned to warehouse
	// if you want to cache articles you scholud use dedicated ArticleRepository
	Q_ASSERT(warehouseId > 0);
	QList<core::Article> articles;
	auto* client = core::providers::ControlCenterClient::instance();
	if (client->getArticlesForWarehouse(warehouseId, articles) != core::providers::ControlCenterClient::StatusCode::Ok)
		throw core::exceptions::NotImplementException();

	return articles;
}

bool RemoteWarehouseRepository::lock(const eloquent::DBIDKey& warehouseId) {
	Q_ASSERT(warehouseId > 0);
	try {
		core::GlobalData::instance().logger().information(QString::asprintf("Blokowanie magazynu do edycji (%d)", warehouseId));
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::Warehouse,
			warehouseId,
			userRepository.autorizeUser().id()
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Blokada dostępu do magazynu o id: %d", warehouseId)
				: QString::asprintf("Nie można zablokować magazynu o id: %d", warehouseId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteWarehouseRepository::unlock(const eloquent::DBIDKey& warehouseId) {
	Q_ASSERT(warehouseId > 0);
	try {
		core::GlobalData::instance().logger().information(QString::asprintf("Odblokowanie magazynu (%d)", warehouseId));
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::Warehouse,
			warehouseId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Odblokowanie dostępu do magazynu o id: %d", warehouseId)
				: QString::asprintf("Nie można odblokować dostępu do magazynu o id: %d", warehouseId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}


} // namespace core::repository::remote
