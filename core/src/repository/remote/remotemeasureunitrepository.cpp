#include "repository/remote/remotemeasureunitrepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "exceptions/notimplementexception.hpp"

namespace core::repository::remote {

QList<MeasureUnit> RemoteMeasureUnitRepository::getAll() {
	if (m_cache.size() == 0) {
		QList<core::MeasureUnit> units;
		auto* client = core::providers::ControlCenterClient::instance();
		core::providers::ControlCenterClient::StatusCode statusCode = client->getAllMeasureUnits(units);
		if (statusCode == core::providers::ControlCenterClient::StatusCode::Ok) {
			// update cache memory
			for (const auto& unit : units)
				m_cache[unit.id()] = unit;
			return units;
		}

		if (statusCode == core::providers::ControlCenterClient::StatusCode::NotImplemented)
			throw exceptions::NotImplementException {};
	} else {
		// return units from cache memory
		QList<core::MeasureUnit> units;
		for (auto& mem : m_cache)
			units.append(mem.second);
		return units;
	}
	return QList<core::MeasureUnit>{};
}

MeasureUnit RemoteMeasureUnitRepository::get(eloquent::DBIDKey unitId) {
	Q_ASSERT(unitId > 0);
	// if we found unit in cache memory, return the found object
	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&unitId](const std::pair<core::eloquent::DBIDKey, core::MeasureUnit>& item)->bool{
			return item.first == unitId;
	});
	if (itr != m_cache.end())
		return itr->second;

	core::MeasureUnit unit;
	auto* client = core::providers::ControlCenterClient::instance();
	auto statusCode = client->getMeasureUnit(unitId, unit);

	if (statusCode == core::providers::ControlCenterClient::StatusCode::Ok) {
		m_cache[unit.id()] = unit;	// update cache memory
		return unit;
	}

	if (statusCode == core::providers::ControlCenterClient::StatusCode::NotImplemented)
		throw exceptions::NotImplementException {};

	return core::MeasureUnit{};
}

} // namespace core::repository::remote
