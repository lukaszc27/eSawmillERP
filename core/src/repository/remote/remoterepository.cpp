#include "repository/remote/remoterepository.hpp"
#include "repository/remote/remoteuserrepository.hpp"
#include "repository/remote/remotecompanyrepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "repository/remote/remotewarehouserepository.hpp"
#include "repository/remote/remotetaxrepository.hpp"
#include "repository/remote/remotewoodtyperepository.hpp"
#include "repository/remote/remotearticlerepository.hpp"
#include "repository/remote/remotemeasureunitrepository.hpp"
#include "repository/remote/remotecontractorrepository.hpp"
#include "repository/remote/remoteinvoicerepository.hpp"
#include "repository/remote/remoteorderrepository.hpp"
#include "repository/remote/remoteservicerepository.hpp"
#include "repository/remote/remotecityrepository.hpp"
#include "globaldata.hpp"
#include "exceptions/repositoryexception.hpp"
#include "exceptions/deviceunregisteredexception.hpp"
#include <QEventLoop>
#include <QSettings>
#include <QHostAddress>

namespace core::repository::remote {

bool RemoteRepository::connect(QObject* parent) {
	auto* client = core::providers::ControlCenterClient::createSingelton(parent);
	QSettings settings;
	client->connectToHost(QHostAddress(
		settings.value("remote/serviceHost", QVariant("127.0.0.1")).toString()),
		settings.value("remote/servicePort", 27015).toInt()
	);

	auto& logger = core::GlobalData::instance().logger();
	if (!client->waitForConnected()) {
		logger.warning("Nie nawiązano połączenia z usługą ProductionCenter");
		throw core::exceptions::RepositoryException{};
	}

	if (client->registerDevice() != core::providers::ControlCenterClient::StatusCode::Accepted) {
		logger.warning("Nie udało się zarejestrować aplikacji w usłudze ProductionCenter");
		throw core::exceptions::DeviceUnregisteredException();
	}
	return true;
}

bool RemoteRepository::disconnect() {
	auto* client = core::providers::ControlCenterClient::instance();
//#if defined(_DEBUG) && defined(NO_REGISTER_DEVICE)
	if (client->unregisterDevice() != core::providers::ControlCenterClient::StatusCode::Ok) {
		core::GlobalData::instance().logger().warning("Nie udało się wyrejestrować urządzenia w usłudze ProductionCenter");
		return false;
	}
//#endif
	client->close();
	delete client;

	return true;
}

AbstractUserRepository& RemoteRepository::userRepository() const {
	static RemoteUserRepository userRepository;
	return userRepository;
}

AbstractCompanyRepository& RemoteRepository::companiesRepository() const {
	static RemoteCompanyRepository companiesRepository;
	return companiesRepository;
}

AbstractWarehouseRepository& RemoteRepository::warehousesRepository() const {
	static RemoteWarehouseRepository warehouseRepository;
	return warehouseRepository;
}

AbstractTaxRepository& RemoteRepository::TaxRepository() const {
	static RemoteTaxRepository taxRepository;
	return taxRepository;
}

AbstractWoodTypeRepository& RemoteRepository::woodTypeRepository() const {
	static RemoteWoodTypeRepository woodTypeRepository;
	return woodTypeRepository;
}

AbstractArticleRepository& RemoteRepository::articleRepository() const {
	static RemoteArticleRepository articleRepository;
	return articleRepository;
}

AbstractMeasureUnitRepository& RemoteRepository::measureUnitRepository() const {
	static RemoteMeasureUnitRepository measureUnitRepository;
	return measureUnitRepository;
}

AbstractContractorRepository& RemoteRepository::contractorRepository() const {
	static RemoteContractorRepository contractorRepository;
	return contractorRepository;
}

AbstractInvoiceRepository& RemoteRepository::invoiceRepository() const {
	static RemoteInvoiceRepository invoiceRepository;
	return invoiceRepository;
}

AbstractOrderRepository& RemoteRepository::orderRepository() const {
	static RemoteOrderRepository orderRepository;
	return orderRepository;
}

AbstractServiceRepository& RemoteRepository::serviceRepository() const {
	static RemoteServiceRepository serviceRepository;
	return serviceRepository;
}

AbstractCityRepository& RemoteRepository::cityRepository() const {
	static RemoteCityRepository cityRepository;
	return cityRepository;
}

} // namespace core::repository::remote
