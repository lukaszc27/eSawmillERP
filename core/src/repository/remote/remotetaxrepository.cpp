#include "repository/remote/remotetaxrepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "exceptions/repositoryexception.hpp"

namespace core::repository::remote {

QList<Tax> RemoteTaxRepository::getAll() {
	QList<core::Tax> taxes;
	if (m_cache.size() == 0) {
		auto* client = core::providers::ControlCenterClient::instance();
		if (client->getAllTaxes(taxes) != core::providers::ControlCenterClient::StatusCode::Ok)
			throw core::exceptions::RepositoryException();

		// store taxes in cache
		for (const auto& tax : taxes)
			m_cache[tax.id()] = tax;
	} else {
		// get taxes from cache
		for (const auto& [id, tax] : m_cache)
			taxes.append(tax);
	}

	return taxes;
}

Tax RemoteTaxRepository::get(eloquent::DBIDKey TaxId) {
	Q_ASSERT(TaxId > 0);
	auto itr = std::find_if(m_cache.begin(), m_cache.end(), [&TaxId](const std::pair<core::eloquent::DBIDKey, core::Tax>& item){
		return item.first == TaxId;
	});
	if (itr != m_cache.end())
		return itr->second;	// return from cache memory

	// tax object not exist in cache memory, do we have to add now
	auto* client = core::providers::ControlCenterClient::instance();
	core::Tax tax;
	bool success = client->getTaxById(TaxId, tax) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success)
		m_cache[tax.id()] = tax;

	return tax;
}

Tax RemoteTaxRepository::getByValue(double value) {
	auto itr = std::find_if(m_cache.begin(), m_cache.end(), [&value](const std::pair<core::eloquent::DBIDKey, core::Tax>& item){
		return item.second.value() == value;
	});
	if (itr != m_cache.end())
		return itr->second;	// return from cache memory

	auto* client = core::providers::ControlCenterClient::instance();
	core::Tax tax;
	bool success = client->getTaxByValue(value, tax) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success)
		m_cache[tax.id()] = tax;

	return tax;
}

bool RemoteTaxRepository::create(core::Tax& tax) {
	Q_ASSERT(tax.id() == 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->createTax(tax) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		Q_ASSERT(tax.id() > 0);
		m_cache[tax.id()] = tax;	// if save operation will be successfull update cache memory
	}

	return success;
}

bool RemoteTaxRepository::update(const core::Tax& tax) {
	Q_ASSERT(tax.id() > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->updateTax(tax) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success)
		m_cache[tax.id()] = tax;	// update cache memory

	return success;
}

bool RemoteTaxRepository::destroy(eloquent::DBIDKey taxId) {
	Q_ASSERT(taxId > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->destroyTax(taxId) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// update cache memory
		auto itr = std::find_if(m_cache.begin(), m_cache.end(),
			[&taxId](const std::pair<core::eloquent::DBIDKey, core::Tax>& item){
				return item.first == taxId;
		});
		// if found remove from cache
		if (itr != m_cache.end())
			m_cache.erase(itr);
		else {
			// if not found we have to return false
			// because end user must have information about it
			// because can still can see deleted taxes
			return false;
		}
	}
	return success;
}

} // namespace core::repository::remote
