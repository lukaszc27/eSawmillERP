#include "repository/remote/remoteservicerepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "globaldata.hpp"
#include "repository/remote/remotearticlerepository.hpp"
#include "providers/protocol.hpp"
//#include "repository/remote/remotewoodtyperepository.hpp"

namespace agent = core::providers::agent;

namespace core::repository::remote {

Service RemoteServiceRepository::get(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);
	{// first check cache memory
		QReadLocker locker(&m_cache_lock);
		auto itr = std::find_if(m_services_cache.begin(), m_services_cache.end(), Comparator{serviceId});
		if (itr != m_services_cache.end())
			return itr->second;
	}

	// and if service not exist in cache, we have to get them from db server
	auto* client = core::providers::ControlCenterClient::instance();
	core::Service service;
	auto code = client->getService(serviceId, service);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		// update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_services_cache[serviceId] = service;
	}
	return service;
}

QList<Service> RemoteServiceRepository::getAll() {
	QList<core::Service> services;
	if (m_services_cache.size() == 0) {
		// get services from db
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->getServices(services);
		if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
			// update cache
			QWriteLocker locker(&m_cache_lock);
			for (auto& service : services) {
				Q_ASSERT(service.id() > 0);
				m_services_cache[service.id()] = service;
			}
		}
	} else {
		// get services from cache
		QReadLocker locker(&m_cache_lock);
		for (const auto& [id, service] : m_services_cache)
			services.append(service);
	}
	return services;
}

bool RemoteServiceRepository::create(Service& service) {
	Q_ASSERT(service.id() == 0);

	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->createService(service);
	if (code == core::providers::ControlCenterClient::StatusCode::Created) {
		Q_ASSERT(service.id() > 0);

		// update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_services_cache[service.id()] = service;
		return true;
	}
	return false;
}

bool RemoteServiceRepository::update(const Service& service) {
	Q_ASSERT(service.id() > 0);
	core::Service& serviceTmp = const_cast<Service&>(service);

	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->updateService(serviceTmp);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		Q_ASSERT(serviceTmp.id() > 0);
		// update cache memory
		updateServiceInCache(serviceTmp.id());
		return true;
	}
	return false;
}

QList<service::Element> RemoteServiceRepository::getServiceElements(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);
	const auto& service = get(serviceId);
	return service.elements();
}

QList<Article> RemoteServiceRepository::getServiceArticles(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);
	auto articleRepository = core::repository::remote::RemoteArticleRepository{};
	const auto& service = get(serviceId);

	QList<core::Article> articles;
	for (const auto& clip : service.articles()) {
		Q_ASSERT(clip.articleId() > 0);
		auto article = articleRepository.getArticle(clip.articleId());
		article.setQuantity(clip.quantity());
		articles.append(article);
	}
	return articles;
}

bool RemoteServiceRepository::destroy(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->destroyService(serviceId);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		// remove service from cache memory
		QWriteLocker locker(&m_cache_lock);
		auto itr = std::find_if(m_services_cache.begin(), m_services_cache.end(), Comparator {serviceId});
		if (itr != m_services_cache.end()) {
			m_services_cache.erase(itr);
		}

		return true;
	}
	return false;
}

QDate RemoteServiceRepository::getFirstAvaiableDeadline() {
	if (m_services_cache.size() == 0)
		getAll();	// get all services to cache

	QDate avaiable = QDate::currentDate();
	QReadLocker locker(&m_cache_lock);
	for (const auto& [id, service] : m_services_cache) {
		if (service.endDate() > avaiable)
			avaiable = service.endDate();
	}
	return avaiable;
}

bool RemoteServiceRepository::lock(const eloquent::DBIDKey& serviceId) {
	Q_ASSERT(serviceId > 0);
	try {
		auto& userrepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::Service,
			serviceId,
			userrepository.autorizeUser().id()
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Zablokowano zlecenie o id: %d do edycji", serviceId)
				: QString::asprintf("Nie można zablokować zlecenia do edycji (id: %d)", serviceId));

		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteServiceRepository::unlock(const eloquent::DBIDKey& serviceId) {
	Q_ASSERT(serviceId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::Service,
			serviceId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Odblokowanie dostępu do usługi o id: %d", serviceId)
				: QString::asprintf("Nie można odblokować dostępu do usługi o id: %d", serviceId));

		return success;
	} catch (...) {
		throw;
	}
	return false;
}

}
