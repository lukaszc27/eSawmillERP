#include "repository/remote/remoteuserrepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "globaldata.hpp"
#include "providers/protocol.hpp"

namespace agent = core::providers::agent;

namespace core::repository::remote {

bool RemoteUserRepository::login(const QString& userName, const QString& password) {
	if (userName.isEmpty() && password.isEmpty())
		return false;

	const auto& client = core::providers::ControlCenterClient::instance();
	mCurrentUser = client->loginUser(userName, password);
	if (mCurrentUser) {
		m_usersCache[mCurrentUser.id()] = mCurrentUser;
		return true;
	}
	return false;
}

void RemoteUserRepository::logout() {
	if (mCurrentUser.id() <= 0)
		return;

	try {
		auto* client = core::providers::ControlCenterClient::instance();
		client->logoutUser(mCurrentUser.id());
		mCurrentUser.setId(-1);
	} catch (...) {
		core::GlobalData::instance().logger().warning("Nie udało się wylogować użytkownika z usługi ProductionCenter");
		throw;
	}
}

QList<User> RemoteUserRepository::getAllUsers() {
	QList<User> users;
	if (m_usersCache.size() == 0) {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getAllUsers(users) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// update cache memory
			for (const auto& user : users)
				m_usersCache[user.id()] = user;
		}
	} else {
		// get users from cache memory
		for (const auto& [id, user] : m_usersCache)
			users.append(user);
	}
	return users;
}

User RemoteUserRepository::getUser(const eloquent::DBIDKey& userId) {
	auto itr = std::find_if(m_usersCache.begin(), m_usersCache.end(), Comparator{userId});
	if (itr != m_usersCache.end())
		return itr->second;

	auto* client = core::providers::ControlCenterClient::instance();
	core::User user;
	if (client->getUser(userId, user) == core::providers::ControlCenterClient::StatusCode::Ok) {
		m_usersCache[user.id()] = user;
		return user;
	}
	return core::User{};
}

bool RemoteUserRepository::create(User& user) {
	/// CACHE MEMORY IS NOT UPDATED IN CREATE METHOD !!!!
	auto* client = core::providers::ControlCenterClient::instance();
	if (client->createUser(user) == core::providers::ControlCenterClient::StatusCode::Created) {
		Q_ASSERT(user.id() > 0);
		m_usersCache[user.id()] = user;
		return true;
	}

	return false;
}

bool RemoteUserRepository::update(User& user) {
	Q_ASSERT(user.id() > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	if (client->updateUser(user) == core::providers::ControlCenterClient::StatusCode::Ok) {
		updateCache(user.id());
		return true;
	}
	return false;
}

bool RemoteUserRepository::destroy(eloquent::DBIDKey& key) {
	Q_ASSERT(key > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	if (client->destroyUser(key) == core::providers::ControlCenterClient::StatusCode::Ok) {
		return removeFromCache(key);
	}

	return false;
}

bool RemoteUserRepository::lock(const eloquent::DBIDKey& userId) {
	try {
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->lock(
			agent::Resource::User,
			userId,
			userRepository.autorizeUser().id());

		return code == core::providers::ControlCenterClient::StatusCode::Accepted;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteUserRepository::unlock(const eloquent::DBIDKey& userId) {
	Q_ASSERT(userId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->unlock(agent::Resource::User, userId);
		return code == core::providers::ControlCenterClient::StatusCode::Accepted;
	} catch (...) {
		throw;
	}
	return false;
}

RoleCollection RemoteUserRepository::getAllRoles() {
	core::RoleCollection roles;
	if (m_roles_cache.empty()) {
		// get roles from data provider
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getUserRoles(roles) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// update cache memory
			for (const auto& role : roles) {
				m_roles_cache[role.id()] = role;
			}
		}
	} else {
		// get roles from cache memory
		for (const auto& [id, role] : m_roles_cache) {
			roles.append(role);
		}
	}
	return roles;
}

} // namespace core::repository::remote
