#include "repository/remote/remotecontractorrepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "providers/protocol.hpp"
#include "globaldata.hpp"
#include <QReadLocker>
#include <QWriteLocker>


namespace agent = core::providers::agent;;

namespace core::repository::remote {

Contractor RemoteContractorRepository::get(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	{ // first check cache memory
		QReadLocker locker(&m_cache_lock);
		auto itr = std::find_if(m_cache.begin(), m_cache.end(), Comparator{contractorId});
		if (itr != m_cache.end()) {
			return itr->second;	// return contractor from cache memory
		}
	}

	core::Contractor contractor;
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->getContractor(contractorId, contractor) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// if we have to get contractor from remote server
		// now we schould add not existing (on client side) contractor to cache memory
		QWriteLocker locker(&m_cache_lock);
		m_cache[contractorId] = contractor;
	}
	return contractor;
}

QList<Contractor> RemoteContractorRepository::getAll() {
	QList<core::Contractor> contractors;

	QReadLocker read_locker(&m_cache_lock);
	if (m_cache.size() == 0) {
		read_locker.unlock();

		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getAllContractors(contractors) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// update cache memory
			QWriteLocker write_locker(&m_cache_lock);
			for (const auto& contractor : contractors)
				m_cache[contractor.id()] = contractor;
		}
	} else {
		// return contractor list from cache
		read_locker.relock();
		for (const auto& [id, contractor] : m_cache)
			contractors.append(contractor);
	}
	return contractors;
}

bool RemoteContractorRepository::create(Contractor& contractor) {
	Q_ASSERT(contractor.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->createContractor(contractor) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		Q_ASSERT(contractor.id() > 0 && contractor["ContactId"].toInt() > 0);	// in this moment id should be valid

		// if create operation will be success we should update cache memory
		QWriteLocker locker(&m_cache_lock);
		m_cache[contractor.id()] = contractor;
	}
	return success;
}

bool RemoteContractorRepository::update(const Contractor& contractor) {
	Q_ASSERT(contractor.id() > 0 && contractor["ContactId"].toInt() > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	core::Contractor obj {contractor};
	bool success = client->updateContractor(obj) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(contractor.id() > 0 && contractor["ContactId"].toInt() > 0);

		QWriteLocker locker(&m_cache_lock);
		m_cache[contractor.id()] = contractor;
	}
	return success;
}

bool RemoteContractorRepository::destroy(const eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->destroyContractor(contractorId) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// remove contractor from cache memory
		removeFromCache(contractorId);
	}
	return success;
}

bool RemoteContractorRepository::lock(const eloquent::DBIDKey& contractorId) {
	Q_ASSERT(contractorId > 0);
	try {
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::Contractor,
			contractorId,
			userRepository.autorizeUser().id()
		) == providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Zablokowano dostęp do kontrahenta o id: %d", contractorId)
				: QString::asprintf("Nie można zablokować kontrahenta o id: %d", contractorId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteContractorRepository::unlock(const eloquent::DBIDKey& contractorId) {
	Q_ASSERT(contractorId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::Contractor,
			contractorId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Odblokowanie dostępu do kontrahenta o id: %d", contractorId)
				: QString::asprintf("Nie odblokowano dostępu do kontrahenta o id: %d", contractorId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

} // namespace core::reposiotry::remote
