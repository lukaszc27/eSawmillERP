#include "repository/remote/remotearticlerepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "exceptions/notimplementexception.hpp"
#include "globaldata.hpp"
#include "providers/protocol.hpp"
#include <QReadLocker>
#include <QWriteLocker>

namespace agent = core::providers::agent;

namespace core::repository::remote {

QList<Article> RemoteArticleRepository::getAll(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);

	QList<core::Article> articles;
	if (m_articleCache.size() == 0) {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getArticlesForWarehouse(warehouseId, articles) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// update cache memory
			QWriteLocker locker(&m_articleCacheLock);
			for (const auto& article : articles)
				m_articleCache[article.id()] = article;
		}
	} else {
		// load data from cache
		QReadLocker locker(&m_articleCacheLock);
		for (const auto& [articleId, article] : m_articleCache) {
			// load from cache only articles from gived warehouse identificator
			if (article.get("WarehouseId").toInt() == warehouseId)
				articles.append(article);
		}
	}
	return articles;
}

Article RemoteArticleRepository::getArticle(eloquent::DBIDKey articleId) {
	Q_ASSERT(articleId > 0);
	{ // first check cache memory
		QReadLocker locker(&m_articleCacheLock);
		auto mem = std::find_if(m_articleCache.begin(), m_articleCache.end(), Comparator{articleId});
		if (mem != m_articleCache.end())
			return mem->second;
	}

	auto* client = core::providers::ControlCenterClient::instance();
	core::Article article;
	bool success = client->getArticle(articleId, article) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// update cache memory
		QWriteLocker locker(&m_articleCacheLock);
		m_articleCache[articleId] = article;
	}
	return article;
}

bool RemoteArticleRepository::createArticle(Article& article) {
	Q_ASSERT(article.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->createArticle(article) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		QWriteLocker locker(&m_articleCacheLock);
		m_articleCache[article.id()] = article;
	}
	return success;
}

bool RemoteArticleRepository::destroyArticle(eloquent::DBIDKey articleId) {
	Q_ASSERT(articleId > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->destroyArticle(articleId) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		removeFromCache(articleId);
	}
	return success;
}

bool RemoteArticleRepository::updateArticle(const Article& article) {
	Q_ASSERT(article.id() > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	core::Article toUpdate {article};
	bool success = client->updateArticle(toUpdate) == providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(toUpdate.id() > 0);

		QWriteLocker locker(&m_articleCacheLock);
		m_articleCache[toUpdate.id()] = toUpdate;
	}
	return success;
}

QList<ArticleType> RemoteArticleRepository::getArticleTypes() {
	QList<core::ArticleType> types;
	if (m_articleTypeCache.size() == 0) {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getArticleTypes(types) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// add types to cache memory
			QWriteLocker locker(&m_articleTypeLock);
			for (const auto& type : types)
				m_articleTypeCache[type.id()] = type;
		} else {
			throw exceptions::NotImplementException();
		}
	} else {
		// load data from cache memory
		QReadLocker locker(&m_articleTypeLock);
		for (const auto& [typeId, type] : m_articleTypeCache)
			types.append(type);
	}
	return types;
}

ArticleType RemoteArticleRepository::getArticleType(eloquent::DBIDKey typeId) {
	Q_ASSERT(typeId > 0);
	{ // first find article type in cache memory
		QReadLocker locker(&m_articleTypeLock);
		auto mem = std::find_if(m_articleTypeCache.begin(), m_articleTypeCache.end(),
			[&typeId](const std::pair<core::eloquent::DBIDKey, core::ArticleType>& item){
				return item.first == typeId;
		});
		if (mem != m_articleTypeCache.end())
			return mem->second;
	}

	auto* client = core::providers::ControlCenterClient::instance();
	core::ArticleType type;
	bool success = client->getArticleType(typeId, type) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		// update cache memory
		QWriteLocker locker(&m_articleTypeLock);
		m_articleTypeCache[typeId] = type;
	}
	return type;
}

bool RemoteArticleRepository::lock(const eloquent::DBIDKey& articleId) {
	Q_ASSERT(articleId > 0);
	try {
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->lock(
			agent::Resource::Article,
			articleId,
			userRepository.autorizeUser().id());

		return code == core::providers::ControlCenterClient::StatusCode::Accepted;
	} catch (...) {
		throw;	// rethrown all exceptions
	}
	return false;
}

bool RemoteArticleRepository::unlock(const eloquent::DBIDKey& articleId) {
	Q_ASSERT(articleId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->unlock(agent::Resource::Article, articleId);
		return code == core::providers::ControlCenterClient::StatusCode::Accepted;
	} catch (...) {
		throw;
	}
	return false;
}

} // namespace core::repository::remote

