#include "repository/remote/remoteorderrepository.hpp"
#include "repository/remote/remotearticlerepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "globaldata.hpp"
#include "providers/protocol.hpp"
#include <QMap>
#include <QReadWriteLock>
//#include "repository/remote/remotewoodtyperepository.hpp"
//#include "repository/remote/remotecontractorrepository.hpp"

namespace agent = core::providers::agent;

namespace core::repository::remote {

QList<Order> RemoteOrderRepository::getAllOrders() {
	QList<core::Order> orders;
	if (m_orders_cache.empty()) {
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->getOrders(orders);
		if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
			QWriteLocker locker(&m_orders_cache_lock);
			for (const auto& item : orders) {
				m_orders_cache[item.id()] = item;
			}
		}
	} else {
		// retrive cache memory
		QReadLocker locker(&m_orders_cache_lock);
		for (const auto& [id, order] : m_orders_cache) {
			orders.append(order);
		}
	}
	return orders;
}

QList<order::Element> RemoteOrderRepository::getOrderElements(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	const auto& order = getOrder(orderId);
	return order.elements();
}

QList<Article> RemoteOrderRepository::getArticlesForOrder(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);

	const auto& order = getOrder(orderId);
	auto articleRepository = core::repository::remote::RemoteArticleRepository{};

	QList<core::Article> articles;
	for (const auto& clip : order.articles()) {
		auto article = articleRepository.getArticle(clip.articleId());
		article.setQuantity(clip.quantity());
		articles.append(article);
	}
	return articles;
}

Order RemoteOrderRepository::getOrder(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	{// first check cache memory
		QReadLocker locker(&m_orders_cache_lock);
		auto itr = std::find_if(m_orders_cache.begin(), m_orders_cache.end(), Comparator{orderId});
		if (itr != m_orders_cache.end())
			return itr->second;	// return order from cache memory
	}

	// order not exist in cache memory
	// se we have to get it from service
	// and save in cache
	core::Order order;
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->getOrder(orderId, order);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		QWriteLocker locker(&m_orders_cache_lock);
		m_orders_cache[order.id()] = order;
	}
	return order;
}

bool RemoteOrderRepository::create(Order& order) {
	Q_ASSERT(order.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->createOrder(order);

	if (code == core::providers::ControlCenterClient::StatusCode::Created) {
		Q_ASSERT(order.id() > 0);
		QWriteLocker locker(&m_orders_cache_lock);
		m_orders_cache[order.id()] = order;
		return true;
	}
	return false;
}

bool RemoteOrderRepository::update(Order& order) {
	Q_ASSERT(order.id() > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->updateOrder(order);

	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		QWriteLocker locker(&m_orders_cache_lock);
		m_orders_cache[order.id()] = order;
		return true;
	}
	return false;
}

bool RemoteOrderRepository::destroy(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->destroyOrder(orderId);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		// remove from cache memory
		removeFromCache(orderId);
		return true;
	}
	return false;
}

QDate RemoteOrderRepository::getFirstAvaiableDeadline() {
	// in remote mode we try use maximim cache memory to limit transfer data via network
	if (m_orders_cache.empty())
		getAllOrders();

	QDate date = QDate::currentDate();
	QReadLocker locker(&m_orders_cache_lock);
	for (const auto& [id, order] : m_orders_cache) {
		if (order.endDate() > date)
			date = order.endDate();
	}
	return date;
}

bool RemoteOrderRepository::lock(const eloquent::DBIDKey& orderId) {
	Q_ASSERT(orderId > 0);
	try {
		auto& repository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::Order,
			orderId,
			repository.autorizeUser().id()
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Zablokowano dostęp do zamówienia o id: %d", orderId)
				: QString::asprintf("Nie można zablokować zamówienia o id: %d", orderId));

		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteOrderRepository::unlock(const eloquent::DBIDKey& orderId) {
	Q_ASSERT(orderId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::Order,
			orderId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Odblokowano zamówienia o id: %d", orderId)
				: QString::asprintf("Nie można odblokować zammówienia o id: %d", orderId));

		return success;
	} catch (...) {
		throw;
	}
	return false;
}

} // namespace core::repository::remote
