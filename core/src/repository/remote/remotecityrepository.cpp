#include "repository/remote/remotecityrepository.hpp"
#include "providers/controlcenterclient.hpp"


namespace core::repository::remote {

City RemoteCityRepository::get(const eloquent::DBIDKey& cityId) {
	Q_ASSERT(cityId > 0);
	// first check cache memory
	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&cityId](const std::pair<core::eloquent::DBIDKey, core::City>& item)->bool {
			return item.first == cityId;
	});
	if (itr != m_cache.end())
		return itr->second;

	// city not exist in cache se we need get it from service and update cache
	auto* client = core::providers::ControlCenterClient::instance();
	core::City city;
	bool success = client->getCity(cityId, city) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(city.id() > 0);
		m_cache[city.id()] = city;
	}
	return city;
}

QList<City> RemoteCityRepository::getAll() {
	QList<core::City> cities;
	if (m_cache.size() == 0) {
		// get cities from service
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getCities(cities) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			// update cache memory
			for (const auto& city : cities) {
				m_cache[city.id()] = city;
			}
		}
	} else {
		// get cities from cache
		for (const auto& [id, city] : m_cache) {
			cities.append(city);
		}
	}
	return cities;
}

bool RemoteCityRepository::create(City& city) {
	Q_ASSERT(city.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->createCity(city) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		// update cache
		Q_ASSERT(city.id() > 0);
		m_cache[city.id()] = city;
	}
	return success;
}

} // namespace core::repository::remote
