#include "repository/remote/remoteinvoicerepository.hpp"
#include "providers/controlcenterclient.hpp"
#include "globaldata.hpp"
#include "providers/protocol.hpp"
//#include "repository/remote/remotewarehouserepository.hpp"
//#include "repository/remote/remotecontractorrepository.hpp"
//#include "exceptions/repositoryexception.hpp"

namespace agent = core::providers::agent;

namespace core::repository::remote {

QList<SaleInvoice> RemoteInvoiceRepository::getAllSaleInvoices() {
	QList<core::SaleInvoice> invoices;
	if (m_saleInvoicesCache.empty()) {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->getSaleInvoices(invoices) == core::providers::ControlCenterClient::StatusCode::Ok;
		if (success) {
			for (const auto& invoice : invoices) {
				Q_ASSERT(invoice.id() > 0);
				m_saleInvoicesCache[invoice.id()] = invoice;
			}
		}
	} else {
		for (const auto& [id, invoice] : m_saleInvoicesCache) {
			invoices.append(invoice);
		}
	}
	return invoices;
}

SaleInvoice RemoteInvoiceRepository::getSaleInvoice(eloquent::DBIDKey saleInvoiceId){
	Q_ASSERT(saleInvoiceId > 0);
	{
		// first check cache memory
		auto mem = std::find_if(m_saleInvoicesCache.begin(), m_saleInvoicesCache.end(), Comparator{saleInvoiceId});
		if (mem != m_saleInvoicesCache.end()) {
			return mem->second;
		}
	}
	// invoice not exist in cache memory
	// so in this place we schould get it from service
	// and update local cache
	auto* client = core::providers::ControlCenterClient::instance();
	core::SaleInvoice invoice;
	bool success = client->getSaleInvoice(saleInvoiceId, invoice) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(invoice.id() > 0);
		m_saleInvoicesCache[invoice.id()] = invoice;
	}
	return invoice;
}

bool RemoteInvoiceRepository::hasSaleInvoiceCorrection(eloquent::DBIDKey saleInvoiceId, bool useCache) {
	Q_UNUSED(useCache);
	{
		auto found = m_saleInvoiceCorrections.find(saleInvoiceId);
		if (found != m_saleInvoiceCorrections.end())
			return true;
	}
	return false;
}

SaleInvoice RemoteInvoiceRepository::getCorrectionForSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	{
		// first check cache memory
		auto correctionId = m_saleInvoiceCorrections.find(saleInvoiceId);
		if (correctionId != m_saleInvoiceCorrections.end()) {
			auto found = std::find_if(m_saleInvoicesCache.begin(), m_saleInvoicesCache.end(), Comparator{correctionId->first});
			if (found != m_saleInvoicesCache.end())
				return found->second;
		}
	}

	auto* client = core::providers::ControlCenterClient::instance();
	core::SaleInvoice correction;
	bool success = client->getSaleInvoiceCorrection(saleInvoiceId, correction) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(correction.id() > 0);
		m_saleInvoicesCache[correction.id()] = correction;
		m_saleInvoiceCorrections[saleInvoiceId] = correction.id();
	}
	return correction;
}

bool RemoteInvoiceRepository::hasSaleInvoiceAssignedOrder(eloquent::DBIDKey saleInvoiceId, bool useCache) {
	Q_ASSERT(false);

//	static QMap<core::eloquent::DBIDKey, bool> cache;
//	Q_ASSERT(saleInvoiceId > 0);
//
//	if (useCache && cache.contains(saleInvoiceId))
//		return cache[saleInvoiceId];	// return from cache
//
//	auto* client = core::providers::ControlCenterClient::instance();
//	QMap<QString, QVariant> attributes;
//	bool success = client->getSaleInvoiceAttributes(saleInvoiceId, attributes) == core::providers::ControlCenterClient::StatusCode::Ok;
//	if (success) {
//		bool val = attributes["hasSaleInvoiceAssignedOrder"].toBool();
//		cache[saleInvoiceId] = val;
//		return val;
//	}
	return false;
}

Order RemoteInvoiceRepository::getAssignedOrderForSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_UNUSED(saleInvoiceId);
	return core::Order{};
}

bool RemoteInvoiceRepository::postSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->postSaleInvoice(saleInvoiceId) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		auto found = std::find_if(m_saleInvoicesCache.begin(), m_saleInvoicesCache.end(), Comparator{saleInvoiceId});
		if (found != m_saleInvoicesCache.end()) {
			core::SaleInvoice& invoice = found->second;
			invoice.existInBuffer(false);
		}
	}
	return success;
}

bool RemoteInvoiceRepository::createSaleInvoice(SaleInvoice& invoice) {
	Q_ASSERT(invoice.id() == 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->createSaleInvoice(invoice) == core::providers::ControlCenterClient::StatusCode::Created;
	if (success) {
		Q_ASSERT(invoice.id() > 0 && !invoice.number().isEmpty());
		m_saleInvoicesCache[invoice.id()] = invoice;
	}
	return success;
}

bool RemoteInvoiceRepository::updateSaleInvoice(SaleInvoice& invoice) {
	Q_ASSERT(invoice.id() > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->updateSaleInvoice(invoice) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(invoice.id() > 0 && !invoice.number().isEmpty());
		m_saleInvoicesCache[invoice.id()] = invoice;
	}
	return success;
}

bool RemoteInvoiceRepository::markSaleInvoiceAsCorrectionDocument(eloquent::DBIDKey orginalInvoiceId, eloquent::DBIDKey correctionInvoiceId) {
	Q_ASSERT(orginalInvoiceId > 0 && correctionInvoiceId > 0);

	if (m_saleInvoiceCorrections.find(orginalInvoiceId) != m_saleInvoiceCorrections.end())
		return false;

	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->markSaleInvoiceAsCorrectionDocument(orginalInvoiceId, correctionInvoiceId)
			== core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		m_saleInvoiceCorrections[orginalInvoiceId] = correctionInvoiceId;
	}
	return success;
}

QList<SaleInvoiceItem> RemoteInvoiceRepository::getSaleInvoiceItems(eloquent::DBIDKey saleInvoiceId){
	Q_ASSERT(saleInvoiceId > 0);

	const auto& invoice = getSaleInvoice(saleInvoiceId);
	return invoice.items();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// methods for purchase invoices
/////////////////////////////////////////////////////////////////////////////////////////////////////////

QList<PurchaseInvoice> RemoteInvoiceRepository::getAllPurchaseInvoices() {
	QList<core::PurchaseInvoice> invoices;
	if (m_purchaseInvoicesCache.empty()) {
		auto* client = core::providers::ControlCenterClient::instance();
		auto code = client->getPurchaseInvoices(invoices);
		if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
			for (const auto& invoice : invoices) {
				Q_ASSERT(invoice.id() > 0);
				m_purchaseInvoicesCache[invoice.id()] = invoice;
			}
		}
	} else {
		for (const auto& [id, invoice] : m_purchaseInvoicesCache)
			invoices.append(invoice);
	}
	return invoices;
}

PurchaseInvoice RemoteInvoiceRepository::getPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);
	{
		// first check memory cache
		auto found = std::find_if(m_purchaseInvoicesCache.begin(), m_purchaseInvoicesCache.end(), Comparator{purchaseInvoiceId});
		if (found != m_purchaseInvoicesCache.end()) {
			return found->second;
		}
	}
	core::PurchaseInvoice invoice;
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->getPurchaseInvoice(purchaseInvoiceId, invoice);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		// update cache memory
		m_purchaseInvoicesCache[invoice.id()] = invoice;
	}
	return invoice;
}

bool RemoteInvoiceRepository::hasPurchaseInvoiceCorrection(eloquent::DBIDKey purchaseInvoideId, bool useCache) {
	Q_ASSERT(false);
	Q_ASSERT(purchaseInvoideId > 0);
	{
		auto found = m_purchaseInvoiceCorrections.find(purchaseInvoideId);
		if (found != m_purchaseInvoiceCorrections.end())
			return true;
	}
	return false;
//	static QHash<core::eloquent::DBIDKey, bool> cache;
//
//	if (useCache && cache.contains(purchaseInvoideId))
//		return cache[purchaseInvoideId];
//
//	auto* client = core::providers::ControlCenterClient::instance();
//	QMap<QString, QVariant> attributes;
//	auto code = client->getPurchaseInvoiceAttributes(purchaseInvoideId, attributes);
//	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
//		bool val = attributes["hasPurchaseInvoiceCorrection"].toBool();
//		cache[purchaseInvoideId] = val;	// update cache
//		return val;
//	}
//	return false;
}

PurchaseInvoice RemoteInvoiceRepository::getCorrectionForPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);
	{
		auto correction = m_purchaseInvoiceCorrections.find(purchaseInvoiceId);
		if (correction != m_purchaseInvoiceCorrections.end()) {
			auto found = std::find_if(m_purchaseInvoicesCache.begin(), m_purchaseInvoicesCache.end(), Comparator{correction->first});
			if (found != m_purchaseInvoicesCache.end())
				return found->second;
		}
	}
	auto* client = core::providers::ControlCenterClient::instance();
	core::PurchaseInvoice invoice;
	auto success = client->getPurchaseInvoiceCorrection(purchaseInvoiceId, invoice) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(invoice.id() > 0);
		m_purchaseInvoicesCache[invoice.id()] = invoice;
		m_purchaseInvoiceCorrections[purchaseInvoiceId] = invoice.id();
	}
	return invoice;
}

QList<PurchaseInvoiceItem> RemoteInvoiceRepository::getPurchaseInvoiceItems(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(false);
	Q_ASSERT(purchaseInvoiceId > 0);
	QList<core::PurchaseInvoiceItem> items;
	return items;
//	if (m_purchaseInvoiceItemsCache.size() == 0) {
//		// get items from service
//		auto* client = core::providers::ControlCenterClient::instance();
//		auto code = client->getPurchaseInvoiceItems(purchaseInvoiceId, items);
//		if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
//			// add items to cache memory
//			for (const auto& item : items)
//				m_purchaseInvoiceItemsCache[item.id()] = item;
//		}
//	} else {
//		// return items from cache memory
//		for (const auto& [id, item] : m_purchaseInvoiceItemsCache) {
//			if (item.get("InvoiceId").toInt() == purchaseInvoiceId)
//				items.append(item);
//		}
//	}
}

bool RemoteInvoiceRepository::postPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);

	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->postPurchaseInvoice(purchaseInvoiceId);
	if (code == core::providers::ControlCenterClient::StatusCode::Ok) {
		// update cache memory
		auto mem = std::find_if(m_purchaseInvoicesCache.begin(), m_purchaseInvoicesCache.end(), Comparator{purchaseInvoiceId});
		if (mem != m_purchaseInvoicesCache.end()) {
			core::PurchaseInvoice& invoice = mem->second;
			invoice.existInBuffer(false);
			return true;
		}
	}
	return false;
}

bool RemoteInvoiceRepository::createPurchaseInvoice(core::PurchaseInvoice& invoice) {
	Q_ASSERT(invoice.id() == 0);
	auto* client = core::providers::ControlCenterClient::instance();
	auto code = client->createPurchaseInvoice(invoice);
	if (code == core::providers::ControlCenterClient::StatusCode::Created) {
		Q_ASSERT(invoice.id() > 0 && !invoice.number().isEmpty());
		// update cache memory
		m_purchaseInvoicesCache[invoice.id()] = invoice;
		return true;
	}
	return false;
}

bool RemoteInvoiceRepository::updatePurchaseInvoice(core::PurchaseInvoice& invoice) {
	Q_ASSERT(invoice.id() > 0);
	auto* client = core::providers::ControlCenterClient::instance();
	bool success = client->updatePurchaseInvoice(invoice) == core::providers::ControlCenterClient::StatusCode::Ok;
	if (success) {
		Q_ASSERT(invoice.id() > 0 && !invoice.number().isEmpty());
		m_purchaseInvoicesCache[invoice.id()] = invoice;
	}
	return success;
}

bool RemoteInvoiceRepository::lockPurchaseInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_ASSERT(invoiceId > 0);
	try {
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::PurchaseInvoice,
			invoiceId,
			userRepository.autorizeUser().id()
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				 ? QString::asprintf("Zablokowano dostęp do faktury zakupu o id: %d", invoiceId)
				 : QString::asprintf("Nie można zablokować dostępu do faktury zakupu o id: %d", invoiceId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteInvoiceRepository::unlockPurchaseInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_ASSERT(invoiceId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::PurchaseInvoice,
			invoiceId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				 ? QString::asprintf("Odblokowano dostęp do faktury zakupu o id: %d", invoiceId)
				 : QString::asprintf("Nie można odblokować faktury zakupu o id: %d", invoiceId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteInvoiceRepository::lockSaleInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_ASSERT(invoiceId > 0);
	try {
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->lock(
			agent::Resource::SaleInvoice,
			invoiceId,
			userRepository.autorizeUser().id()
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Zablokowano dostęp do faktury sprzedaży o id: %d", invoiceId)
				: QString::asprintf("Nie można zablokować dostępu do faktury sprzedaży o id: %d", invoiceId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

bool RemoteInvoiceRepository::unlockSaleInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_ASSERT(invoiceId > 0);
	try {
		auto* client = core::providers::ControlCenterClient::instance();
		bool success = client->unlock(
			agent::Resource::SaleInvoice,
			invoiceId
		) == core::providers::ControlCenterClient::StatusCode::Accepted;

		core::GlobalData::instance().logger()
			.information(success
				? QString::asprintf("Odblokowano dostęp do faktury sprzedaży o id: %d", invoiceId)
				: QString::asprintf("Nie można odblokować dostępu do faktury sprzedaży o id: %d", invoiceId));
		return success;
	} catch (...) {
		throw;
	}
	return false;
}

} // namespace core::repository::remote
