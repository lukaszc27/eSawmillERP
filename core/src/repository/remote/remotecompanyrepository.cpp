#include "repository/remote/remotecompanyrepository.hpp"
#include "providers/controlcenterclient.hpp"


namespace core::repository::remote {

bool RemoteCompanyRepository::update(Company& company) {
	auto* client = core::providers::ControlCenterClient::instance();
	if (client->updateCompany(company) == core::providers::ControlCenterClient::StatusCode::Ok) {
		m_cache[company.id()] = company;	// update cache after updated object on server side
		return true;
	}

	return false;
}

bool RemoteCompanyRepository::create(Company& company) {
	auto* client = core::providers::ControlCenterClient::instance();
	if (client->createCompany(company) == core::providers::ControlCenterClient::StatusCode::Created) {
		Q_ASSERT(company.id() > 0);
		m_cache[company.id()] = company;
		return true;
	}

	return false;
}

Company RemoteCompanyRepository::getCompany(eloquent::DBIDKey companyId) {
	Q_ASSERT(companyId > 0);

	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&companyId](const std::pair<core::eloquent::DBIDKey, core::Company>& item)->bool {
			return item.first == companyId;
	});
	if (itr != m_cache.end())
		return itr->second;

	auto* client = core::providers::ControlCenterClient::instance();
	core::Company company;
	if (client->getCompany(companyId, company) == core::providers::ControlCenterClient::StatusCode::Ok) {
		m_cache[company.id()] = company;	// update cache memory
		return company;
	}

	return core::Company {};	// return empty object
}

} // namespace core::repository::remote
