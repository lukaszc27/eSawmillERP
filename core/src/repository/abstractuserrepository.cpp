#include "repository/abstractuserrepository.hpp"

namespace core::repository {

bool AbstractUserRepository::login(const User& user) {
	return login(user.get("Name").toString(), user.get("Password").toString());
}

Role AbstractUserRepository::getRole(const eloquent::DBIDKey& roleId) {
	Q_ASSERT(roleId > 0);
	if (m_roles_cache.empty())
		getAllRoles();

	auto itr = std::find_if(m_roles_cache.begin(), m_roles_cache.end(),
		[&roleId](const std::pair<core::eloquent::DBIDKey, core::Role>& item)->bool {
			return item.first == roleId;
	});
	if (itr != m_roles_cache.end())
		return itr->second;

	// return invalid object if not exist in cache memory
	return core::Role{};
}

void AbstractUserRepository::updateCache(const eloquent::DBIDKey& userId) {
	Q_ASSERT(userId > 0);

	removeFromCache(userId);
	// schould get user object from service
	getUser(userId);
}

bool AbstractUserRepository::removeFromCache(const eloquent::DBIDKey& userId) {
	Q_ASSERT(userId > 0);

	auto itr = std::find_if(m_usersCache.begin(), m_usersCache.end(),
		[&userId](const std::pair<core::eloquent::DBIDKey, core::User>& item)->bool {
			return item.first == userId;
	});
	if (itr != m_usersCache.end()) {
		m_usersCache.erase(itr);
		return true;
	}
	return false;
}

} // namespace core::repository
