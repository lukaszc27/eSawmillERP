#include "repository/abstractcompanyrepository.hpp"

namespace core::repository {

void AbstractCompanyRepository::updateCache(const core::eloquent::DBIDKey& companyId) {
	Q_ASSERT(companyId > 0);

	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&companyId](const std::pair<core::eloquent::DBIDKey, core::Company>& item)->bool{
			return item.first == companyId;
	});
	if (itr != m_cache.end())
		m_cache.erase(itr);

	// schould get company object from service
	getCompany(companyId);
}

Company AbstractCompanyRepository::getByNIPNumber(const QString& nip) {
	if (nip.isEmpty())
		throw std::invalid_argument("NIP number is empty");

	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&nip](const std::pair<core::eloquent::DBIDKey, core::Company>& item)->bool {
			return item.second.NIP().trimmed() == nip.trimmed();
	});

	return itr != m_cache.end()
		? itr->second
		: core::Company{};
}

}
