#include "repository/abstractinvoicerepository.hpp"

namespace core::repository {

void AbstractInvoiceRepository::removeSaleInvoiceFromCache(const core::eloquent::DBIDKey& saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);

	auto found = std::find_if(m_saleInvoicesCache.begin(), m_saleInvoicesCache.end(), Comparator{saleInvoiceId});
	if (found != m_saleInvoicesCache.end()) {
		m_saleInvoicesCache.erase(found);
	}
}

void AbstractInvoiceRepository::updateSaleInvoiceCache(const eloquent::DBIDKey& saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);

	// first remove all items from cache
	removeSaleInvoiceFromCache(saleInvoiceId);

	// now schould get it from the data provider
	getSaleInvoice(saleInvoiceId);
}

void AbstractInvoiceRepository::removePurchaseInvoiceFromCache(const eloquent::DBIDKey& purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);

	auto found = std::find_if(m_purchaseInvoicesCache.begin(), m_purchaseInvoicesCache.end(), Comparator{purchaseInvoiceId});
	if (found != m_purchaseInvoicesCache.end()) {
		m_purchaseInvoicesCache.erase(found);
	}
}

void AbstractInvoiceRepository::updatePurchaseInvoiceCache(const eloquent::DBIDKey& purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);

	// first remove all item from cache
	removePurchaseInvoiceFromCache(purchaseInvoiceId);

	// refresh cache memory
	getPurchaseInvoice(purchaseInvoiceId);
}

} // namespace core::repository
