#include "repository/abstractcityrepository.hpp"

namespace core::repository {

bool AbstractCityRepository::hasCity(const QString& name) {
	return std::any_of(m_cache.begin(), m_cache.end(),
		[&name](const std::pair<core::eloquent::DBIDKey, core::City>& item)->bool {
			return item.second.name() == name;
	});
}

} // namespace core::reposito
