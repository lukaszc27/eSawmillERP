#include "repository/abstractrepository.hpp"

namespace core::repository {

void AbstractRepository::loadRepositories(std::shared_ptr<QProgressDialog>& dialog) {
	// first set maximim reositores number
	// now we schould load (in correct order):
	// + contractors repository					[1]
	// + warehouses - and articles for each		[2]
	// + invoices (sales and purchases)			[3,4]
	// + orders									[5]
	// + services								[6]
	constexpr int repositoresToLoadCount {6}; // ^

	// load current value from dialog
	int currentValue {0};
	if (dialog) {
		currentValue = dialog->value();
		// update maximum value in dialog
		dialog->setMaximum(currentValue + repositoresToLoadCount);
	}

	{ // load contractors repository
		if (dialog)
			dialog->setLabelText(QObject::tr("Wczytywanie kontrahentów..."));

		contractorRepository().getAll();
		if (dialog)
			dialog->setValue(++currentValue);
	} // loaded contractors end

	{ // load warehouses and articles
		if (dialog)
			dialog->setLabelText(QObject::tr("Wczytywanie dostępnych magazynów..."));

		const QList<core::Warehouse>& warehouses = warehousesRepository().getAll();
		if (dialog) {
			// update max value in dialog (add warehouse list size)
			dialog->setMaximum(dialog->maximum() + warehouses.size());
			dialog->setValue(++currentValue);
		}

		// load articles
		for (const auto& warehouse : warehouses) {
			Q_ASSERT(warehouse.id() > 0);

			if (dialog)
				dialog->setLabelText(QObject::tr("Wczytywanie artykułów dla magazynu: %1...").arg(warehouse.name()));

			articleRepository().getAll(warehouse.id());
			if (dialog)
				dialog->setValue(++currentValue);
		}
	} // loaded warehouses end

	{ // load invoices
		if (dialog)
			dialog->setLabelText(QObject::tr("Wczytywanie faktur sprzedaży..."));

		invoiceRepository().getAllSaleInvoices();
		if (dialog) {
			dialog->setValue(++currentValue);
			dialog->setLabelText(QObject::tr("Wczytywanie faktur zakupu..."));
		}

		invoiceRepository().getAllPurchaseInvoices();
		if (dialog)
			dialog->setValue(++currentValue);
	} // loaded invoices end

	{ // load orders
		if (dialog)
			dialog->setLabelText(QObject::tr("Wczytywanie zamówień..."));

		orderRepository().getAllOrders();
		if (dialog)
			dialog->setValue(++currentValue);
	} // loaded orders end

	{ // load services
		if (dialog)
			dialog->setLabelText(QObject::tr("Wczytywanie usług..."));

		serviceRepository().getAll();
		if (dialog)
			dialog->setValue(++currentValue);
	} // loaded services end
}

}
