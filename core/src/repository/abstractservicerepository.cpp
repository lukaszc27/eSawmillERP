#include "repository/abstractservicerepository.hpp"

namespace core::repository {

void AbstractServiceRepository::updateServiceInCache(const eloquent::DBIDKey& serviceId) {
	{
		QWriteLocker locker{&m_cache_lock};
		auto itr = std::find_if(m_services_cache.begin(), m_services_cache.end(), Comparator{serviceId});
		if (itr != m_services_cache.end())
			m_services_cache.erase(itr);
	}
	get(serviceId);
}

void AbstractServiceRepository::removeServiceFromCache(const eloquent::DBIDKey& serviceId) {
	Q_ASSERT(serviceId > 0);

	auto service = m_services_cache.end();
	{
		QReadLocker locker(&m_cache_lock);
		service = std::find_if(m_services_cache.begin(), m_services_cache.end(), Comparator {serviceId});
	}

	QWriteLocker locker(&m_cache_lock);
	if (service != m_services_cache.end()) {
		m_services_cache.erase(service);
	}
}

} // namespace core::repository
