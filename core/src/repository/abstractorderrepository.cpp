#include "repository/abstractorderrepository.hpp"

namespace core::repository {

void AbstractOrderRepository::updateOrderInCache(const core::eloquent::DBIDKey& orderId) {
	Q_ASSERT(orderId > 0);

	removeFromCache(orderId);
	getOrder(orderId);
}

void AbstractOrderRepository::removeFromCache(const eloquent::DBIDKey& orderId) {
	Q_ASSERT(orderId > 0);

	auto order = m_orders_cache.end();
	{
		QReadLocker read_locker(&m_orders_cache_lock);
		order = std::find_if(m_orders_cache.begin(), m_orders_cache.end(), Comparator{orderId});
		read_locker.unlock();
	}

	QWriteLocker write_lock(&m_orders_cache_lock);
	if (order != m_orders_cache.end()) {
		m_orders_cache.erase(order);
	}
}

} // namespace core::repository
