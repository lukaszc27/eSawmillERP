#include "repository/database/dbwoodtyperepository.hpp"

namespace core::repository::database {

QList<WoodType> WoodTypeRepository::getAll() {
	const QList<core::WoodType> woods = core::WoodType().getAllRecords();
	return woods;
}

bool WoodTypeRepository::create(WoodType& wood) {
	Q_ASSERT(wood.id() <= 0);

	wood.builder().beginTransaction();
	bool success = wood.create();
	if (success) {
		wood.setId(wood.lastInsertedId());
		wood.builder().commitTransaction();
	}
	else wood.builder().rollbackTransaction();
	return success;
}

bool WoodTypeRepository::update(WoodType& wood) {
	Q_ASSERT(wood.id() > 0);

	wood.builder().beginTransaction();
	bool success = wood.save();
	if (success)
		wood.builder().commitTransaction();
	else wood.builder().rollbackTransaction();
	return success;
}

bool WoodTypeRepository::destroy(eloquent::DBIDKey& key) {
	Q_ASSERT(key > 0);
	core::WoodType wood;
	wood.setId(key);
	wood.builder().beginTransaction();
	bool success = wood.destroy();
	if (success)
		wood.builder().commitTransaction();
	else wood.builder().rollbackTransaction();
	return success;
}

WoodType WoodTypeRepository::get(eloquent::DBIDKey id) {
	return core::WoodType().findRecord(id);
}

} // namespace core::repository::database
