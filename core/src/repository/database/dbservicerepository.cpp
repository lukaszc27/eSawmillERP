#include "repository/database/dbservicerepository.hpp"
#include <QCache>
#include "globaldata.hpp"
#include "dbo/servicearticle.h"
#include "repository/database/dbcontractorrepository.hpp"
#include "repository/database/dbtaxrepository.hpp"
#include "repository/database/dbwoodtyperepository.hpp"
#include "repository/database/dbarticlerepository.hpp"
#include "dbo/serviceelement.h"
#include "dbo/servicearticle.h"


namespace core::repository::database {

Service ServiceRepository::get(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);
	core::Service service;

	service.builder().beginTransaction();
	service = service.findRecord(serviceId);
	prepareService(service);
	service.builder().commitTransaction();
	return service;
}

QList<Service> ServiceRepository::getAll() {
	core::repository::database::ContractorRepository contractorRepository;
	core::repository::database::TaxRepository taxRepository;

	QList<core::Service> services;
	core::Service service;
	service.builder().beginTransaction();
	for (auto obj : service.getAllRecords()) {
		prepareService(obj);
		services.append(obj);
	}
	service.builder().commitTransaction();
	return services;
}

bool ServiceRepository::create(Service& service) {
	service.builder().beginTransaction();
	bool success = service.create();
	if (success) {
		const core::eloquent::DBIDKey serviceID = service.lastInsertedId();
		service.setId(serviceID);
		Q_ASSERT(serviceID > 0);
		if (!service.elements().empty()) {
			for (core::service::Element& el : service.elements()) {
				el.set("ServiceId", serviceID);
				success = el.create();
				if (success) {
					el.setId(el.lastInsertedId());
				} else {
					break;
				}
			}
		}
		if (success && !service.articles().empty()) {
			for (core::service::Article& clip : service.articles()) {
				Q_ASSERT(clip.articleId() > 0);
				clip.setServiceId(serviceID);
				success = clip.create();
				if (success) {
					clip.setId(clip.lastInsertedId());
				} else {
					break;
				}
			}
		}
	}
	if (success)
		service.builder().commitTransaction();
	else service.builder().rollbackTransaction();
	return success;
}

bool ServiceRepository::update(const Service& service) {
	Q_ASSERT(service.id() > 0);	// only in this case will be update, when id is zero cen be running create method in ORM model

	core::Service obj = service;
	obj.builder().beginTransaction();
	bool success = obj.save();
	if (success) {
		{ // before update elements and articles assigned to service, we have to remove all items in db
			core::service::Element().builder().where("ServiceId", service.id()).destroy();
			core::service::Article().builder().where("ServiceId", service.id()).destroy();
		}
		if (service.elements().size() > 0) {
			Q_ASSERT(service.id() > 0);
			for (core::service::Element& el : service.elements()) {
				el.set("ServiceId", service.id());
				success = el.create();
				if (success) {
					el.setId(el.lastInsertedId());
				} else {
					break;
				}
			}
		}
		if (success && service.articles().size() > 0) {
			Q_ASSERT(service.id() > 0);
			for (core::service::Article& clip : service.articles()) {
				Q_ASSERT(clip.id() > 0 && clip.articleId() > 0);
				clip.setServiceId(service.id());
				success = clip.create();
				if (success) {
					clip.setId(clip.lastInsertedId());
				} else {
					break;
				}
			}
		}
	}
	if (success)
		obj.builder().commitTransaction();
	else obj.builder().rollbackTransaction();
	return success;
}

QList<service::Element> ServiceRepository::getServiceElements(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);

	core::service::Element element;
	element.builder().beginTransaction();
	QList<core::WoodType> woods = core::WoodType().getAllRecords();
	QList<core::service::Element> items = element.builder().where("ServiceId", serviceId).get();
	for (auto& item : items) {
		const core::eloquent::DBIDKey woodTypeId = item["WoodTypeId"].toInt();
		Q_ASSERT(woodTypeId > 0);
		auto wood = std::find_if(woods.begin(), woods.end(), [woodTypeId](const core::WoodType& type){
			return type.id() == woodTypeId;
		});
		if (wood != woods.end())
			item.setWoodType(*wood);
		else {
			// item has assigned wood whose don't exist in database
			Q_ASSERT(false);
		}
	}
	element.builder().commitTransaction();
	return items;
}

QList<Article> ServiceRepository::getServiceArticles(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);

	core::service::Article().builder().beginTransaction();
	QList<core::service::Article> serviceArticles = core::service::Article().builder().where("ServiceId", serviceId).get();
	QList<core::Article> articles;
	for (auto& serviceArticle : serviceArticles) {
		const core::eloquent::DBIDKey articleID = serviceArticle.get("ArticleId").toInt();
		Q_ASSERT(articleID > 0);
		const double qty = serviceArticle.get("Quantity").toDouble();
		core::Article article = core::repository::database::ArticleRepository().getArticle(articleID);
		article.setQuantity(qty);
		articles.append(article);
	}
	core::service::Article().builder().commitTransaction();
	return articles;
}

bool ServiceRepository::destroy(eloquent::DBIDKey serviceId) {
	Q_ASSERT(serviceId > 0);

	core::Service service;
	core::service::Element elements;
	core::service::Article articles;

	if (service.builder().where("Id", serviceId).destroy()
		&& elements.builder().where("ServiceId", serviceId).destroy()
		&& articles.builder().where("ServiceId", serviceId).destroy())
	{
		service.builder().commitTransaction();
		return true;
	} else {
		service.builder().rollbackTransaction();
		return false;
	}
}

QDate ServiceRepository::getFirstAvaiableDeadline() {
	const QString queryStr = QString("SELECT MAX(EndDate) AS [EndDate] FROM [%1]").arg(core::Service::tableName());
	QSqlQuery query {QSqlDatabase::database()};
	bool result = query.exec(queryStr) && query.first();
#ifdef _DEBUG
	qDebug() << query.lastQuery();
#endif
	if (!result) {
#if _DEBUG
		qCritical() << query.lastError().text();
#endif
		return QDate::currentDate();
	}

	QDate date = query.value("EndDate").toDate();
	if (date.isNull() || !date.isValid())
		return QDate::currentDate();
	return date;
}

bool ServiceRepository::lock(const eloquent::DBIDKey& serviceId) {
	Q_UNUSED(serviceId);
	return true;
}

bool ServiceRepository::unlock(const eloquent::DBIDKey& serviceId) {
	Q_UNUSED(serviceId);
	return true;
}

void ServiceRepository::prepareService(Service &service) {
	Q_ASSERT(service.id() > 0 && service["ContractorId"].toInt() > 0 && service["VatId"].toInt() > 0);
	auto articles = core::service::Article()
			.builder()
			.where("ServiceId", service.id())
			.get();

	service.setContractor(service["ContractorId"].toInt());
	service.setTax(service["VatId"].toInt());
	service.setElements(getServiceElements(service.id()));
	service.setArticles(articles);
}

} // namespace core::repository::database
