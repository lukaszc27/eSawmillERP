#include "repository/database/dbcityrepository.hpp"

namespace core::repository::database {

City CityRepository::get(const eloquent::DBIDKey& cityId) {
	Q_ASSERT(cityId > 0);

	if (cityId <= 0)
		throw std::invalid_argument("id cannot be less or equal zero!");

	// first check cache memory
	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&cityId](const std::pair<core::eloquent::DBIDKey, core::City>& item)->bool {
			return item.first == cityId;
	});
	if (itr != m_cache.end())
		return itr->second;

	// city don't exist in cache memory
	// se we have to get it from database and add to cache
	core::City city = core::City().findRecord(cityId);
	if (city.id() > 0) {
		// add valid city object to cache memory
		m_cache[city.id()] = city;
	}
	return city;
}

QList<City> CityRepository::getAll() {
	QList<core::City> cities;
	if (m_cache.size() == 0) {
		// cache memory is empty
		cities = core::City().getAllRecords();
		for (const auto& city : cities) {
			Q_ASSERT(city.id() > 0);
			if (city.id() == 0)
				continue;

			m_cache[city.id()] = city;
		}
	} else {
		// load cities from cache memory
		for (auto& [id, city] : m_cache) {
			cities.append(city);
		}
	}
	return cities;
}

bool CityRepository::create(City& city) {
	Q_ASSERT(city.id() == 0);
	core::City().builder().beginTransaction();
	bool success = city.create();
	if (success) {
		core::eloquent::DBIDKey cityId = core::City().lastInsertedId();
		Q_ASSERT(cityId > 0);
		city.setId(cityId);
		core::City().builder().commitTransaction();

		// now we can add new city object into cache memory
		m_cache[cityId] = city;
	} else {
		core::City().builder().rollbackTransaction();
	}
	return success;
}

}
