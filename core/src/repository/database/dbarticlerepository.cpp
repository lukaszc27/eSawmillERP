#include "repository/database/dbarticlerepository.hpp"
#include "repository/database/dbwarehouserepository.hpp"
#include "globaldata.hpp"
#include <QCache>
#include <exception>

namespace core::repository::database {

QList<Article> ArticleRepository::getAll(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);

	core::repository::database::WarehouseRepository repository;
	return repository.getArticles(warehouseId);
}

Article ArticleRepository::getArticle(eloquent::DBIDKey articleId) {
	Q_ASSERT(articleId > 0);

	core::Article article;
	article.builder().beginTransaction();
	article = article.findRecord(articleId);
	article.setArticleType(article.get("ArticleTypeId").toInt());
	article.setProvider(article.get("ProviderId").toInt());
	article.setWarehouse(article.get("WarehouseId").toInt());
	article.setPurchaseTax(article.get("PurchaseVatId").toInt());
	article.setSaleTax(article.get("SaleVatId").toInt());
	article.setCompany(article.get("CompanyId").toInt());
	article.setUser(article.get("UserAddId").toInt());
	article.setUnit(article.get("UnitId").toInt());
	article.builder().commitTransaction();
	return article;
}

bool ArticleRepository::createArticle(Article& article) {
	article.builder().beginTransaction();
	bool success = article.create();
	if (success) {
		article.setId(article.lastInsertedId());
		article.builder().commitTransaction();
	} else {
		article.builder().rollbackTransaction();
	}
	return success;
}

bool ArticleRepository::updateArticle(const Article& article) {
	core::Article obj {article};
	Q_ASSERT(obj.id() > 0);

	obj.builder().beginTransaction();
	bool success = obj.save();
	if (success)
		obj.builder().commitTransaction();
	else obj.builder().rollbackTransaction();
	return success;
}

bool ArticleRepository::destroyArticle(eloquent::DBIDKey articleId) {
	Q_ASSERT(articleId > 0);
	core::Article article;
	article.setId(articleId);
	article.builder().beginTransaction();
	bool success = article.destroy();
	if (success)
		article.builder().commitTransaction();
	else article.builder().rollbackTransaction();
	return success;
}

QList<ArticleType> ArticleRepository::getArticleTypes() {
	QList<core::ArticleType> articleTypes = core::ArticleType().getAllRecords();
	return articleTypes;
}

ArticleType ArticleRepository::getArticleType(eloquent::DBIDKey typeId) {
	Q_ASSERT(typeId > 0);
	return static_cast<core::ArticleType>(core::ArticleType().findRecord(typeId));
}

bool ArticleRepository::lock(const eloquent::DBIDKey& articleId) {
	Q_UNUSED(articleId);
	return true;
}

bool ArticleRepository::unlock(const eloquent::DBIDKey& articleId) {
	Q_UNUSED(articleId);
	return true;
}

} // namespace core::repository::database
