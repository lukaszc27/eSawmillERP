#include "repository/database/dbcontractorrepository.hpp"

namespace core::repository::database {

Contractor ContractorRepository::get(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	core::Contractor contractor;
	contractor.builder().beginTransaction();
	contractor = static_cast<core::Contractor>(contractor.findRecord(contractorId));
	contractor.setContact(contractor.get("ContactId").toInt());
	contractor.builder().commitTransaction();

	return contractor;
}

QList<Contractor> ContractorRepository::getAll() {
	core::Contractor c {};
	QList<core::Contractor> contractors;
	c.builder().beginTransaction();
	contractors = core::Contractor().getAllRecords();
	for (auto& contractor : contractors) {
		contractor.setContact(contractor["ContactId"].toInt());
	}
	c.builder().commitTransaction();
	return contractors;
}

bool ContractorRepository::create(Contractor& contractor) {
	Q_ASSERT(contractor.id() == 0 && contractor.contact().id() == 0);

	core::Contact contact;
	contact.builder().beginTransaction();
	bool success = contractor.contact().create();
	if (success) {
		const core::eloquent::DBIDKey contactID = contact.builder().lastInsertedId();
		contractor.set("ContactId", contactID);
		contractor.contact().setId(contactID);
		success = contractor.create();
	}

	if (success) {
		const core::eloquent::DBIDKey contractorID = core::Contractor().lastInsertedId();
		contractor.setId(contractorID);
		Q_ASSERT(contractor.id() > 0 && contractor.contact().id() > 0);
		contact.builder().commitTransaction();
	} else {
		contact.builder().rollbackTransaction();
	}
	return success;
}

bool ContractorRepository::update(const Contractor& contractor) {
	Q_ASSERT(contractor.id() > 0);
	Q_ASSERT(contractor.contact().id() > 0);

	core::Contact contact;
	contact.builder().beginTransaction();
	bool success = contractor.contact().save();
	if (success) {
		Q_ASSERT(contractor.contact().id() == contractor.get("ContactId").toInt());	// just in case
		core::Contractor c {contractor};
		success = c.save();
	}

	if (success)
		contact.builder().commitTransaction();
	else contact.builder().rollbackTransaction();
	return success;
}

bool ContractorRepository::destroy(const eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	core::Contractor contractor;
	contractor.builder().beginTransaction();
	contractor = static_cast<core::Contractor>(core::Contractor().findRecord(contractorId));
	bool success = contractor.destroy();
	if (success)
		contractor.builder().commitTransaction();
	else contractor.builder().rollbackTransaction();
	return success;
}

bool ContractorRepository::lock(const eloquent::DBIDKey& contractorId) {
	Q_UNUSED(contractorId);
	return true;
}

bool ContractorRepository::unlock(const eloquent::DBIDKey& contractorId) {
	Q_UNUSED(contractorId);
	return true;
}

} // namespace core::repository::database
