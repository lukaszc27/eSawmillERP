#include "repository/database/dbinvoicerepository.hpp"
#include "repository/database/dbwarehouserepository.hpp"
#include "repository/database/dbcontractorrepository.hpp"
#include "dbo/saleinvoicecorrections.hpp"
#include "dbo/purchaseinvoicecorrection.hpp"
#include "dbo/ordersaleinvoices.hpp"
#include <QCache>
#include <QHash>

namespace core::repository::database {

QList<SaleInvoice> InvoiceRepository::getAllSaleInvoices() {
	QCache<core::eloquent::DBIDKey, core::Warehouse> warehousesCahce;
	QCache<core::eloquent::DBIDKey, core::Contractor> contractorsCache;

	core::repository::database::WarehouseRepository warehouseRepository;
	core::repository::database::ContractorRepository contractorRepository;

	QList<core::SaleInvoice> invoices;
	core::SaleInvoice invoice;
	invoice.builder().beginTransaction();
	for (const auto& model : invoice.getAllRecords()) {
		core::SaleInvoice obj {model};
		const core::eloquent::DBIDKey warehouseID = obj.get("WarehouseId").toInt();
		if (!warehousesCahce.contains(warehouseID)) {
			core::Warehouse* warehouse = new core::Warehouse {};
			*warehouse = warehouseRepository.getWarehouse(warehouseID);
			warehousesCahce.insert(warehouseID, warehouse);
		}
		const core::eloquent::DBIDKey contractorID = obj.get("ContractorId").toInt();
		if (!contractorsCache.contains(contractorID)) {
			core::Contractor* contractor = new core::Contractor {};
			*contractor = contractorRepository.get(contractorID);
			contractorsCache.insert(contractorID, contractor);
		}
		obj.setContractor(*contractorsCache[contractorID]);
		obj.setWarehouse(*warehousesCahce[warehouseID]);
		invoices.append(obj);
	}
	invoice.builder().commitTransaction();

	return invoices;
}

SaleInvoice InvoiceRepository::getSaleInvoice(eloquent::DBIDKey invoiceId) {
	Q_ASSERT(invoiceId > 0);

	QCache<core::eloquent::DBIDKey, core::Warehouse> warehousesCahce;
	QCache<core::eloquent::DBIDKey, core::Contractor> contractorsCache;

	core::repository::database::WarehouseRepository warehouseRepository;
	core::repository::database::ContractorRepository contractorRepository;

	core::SaleInvoice invoice;
	invoice.builder().beginTransaction();
	invoice = static_cast<core::SaleInvoice>(core::SaleInvoice().findRecord(invoiceId));
	const core::eloquent::DBIDKey warehouseID = invoice.get("WarehouseId").toInt();
	if (!warehousesCahce.contains(warehouseID)) {
		core::Warehouse* warehouse = new core::Warehouse {};
		*warehouse = warehouseRepository.getWarehouse(warehouseID);
		warehousesCahce.insert(warehouseID, warehouse);
	}
	const core::eloquent::DBIDKey contractorID = invoice.get("ContractorId").toInt();
	if (!contractorsCache.contains(contractorID)) {
		core::Contractor* contractor = new core::Contractor {};
		*contractor = contractorRepository.get(contractorID);
		contractorsCache.insert(contractorID, contractor);
	}
	invoice.setContractor(*contractorsCache[contractorID]);
	invoice.setWarehouse(*warehousesCahce[warehouseID]);
	return invoice;
}

bool InvoiceRepository::hasSaleInvoiceCorrection(eloquent::DBIDKey saleInvoiceId, bool useCache) {
	Q_ASSERT(saleInvoiceId > 0);
	static QHash<core::eloquent::DBIDKey, bool> cache;
	if (useCache && cache.contains(saleInvoiceId)) {
		return cache[saleInvoiceId];
	}
	core::SaleInvocieCorrections corrections;
	bool hasCorrections = corrections.builder().where("InvoiceId", saleInvoiceId).get().size() > 0;
	cache.insert(saleInvoiceId, hasCorrections);
	return cache[saleInvoiceId];
}

SaleInvoice InvoiceRepository::getCorrectionForSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	core::SaleInvocieCorrections correction;
	correction = correction.builder().where("InvoiceId", saleInvoiceId).orderBy("CreatedDate").first();
	return getSaleInvoice(correction.get("CorrectionId").toInt());
}

bool InvoiceRepository::hasSaleInvoiceAssignedOrder(eloquent::DBIDKey saleInvoiceId, bool useCache) {
	Q_ASSERT(saleInvoiceId > 0);
	static QHash<core::eloquent::DBIDKey, bool> cache;
	if (useCache && cache.contains(saleInvoiceId)) {
		return cache[saleInvoiceId];
	}
	core::OrderSaleInvoices osi;
	bool hasOrder = osi.builder().where("InvoiceId", saleInvoiceId).orderBy("CreatedDate").get().size() > 0;
	cache.insert(saleInvoiceId, hasOrder);
	return cache[saleInvoiceId];
}

Order InvoiceRepository::getAssignedOrderForSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	core::OrderSaleInvoices osi;
	osi = osi.builder().where("InvoiceId", saleInvoiceId).orderBy("CreatedDate").first();
	return osi.order();
}

bool InvoiceRepository::postSaleInvoice(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	core::SaleInvoice invoice;
	invoice.setId(saleInvoiceId);
	invoice.set("Buffer", false);
	invoice.builder().beginTransaction();
	bool success = invoice.save();
	if (success)
		invoice.builder().commitTransaction();
	else invoice.builder().rollbackTransaction();
	return success;
}

bool InvoiceRepository::createSaleInvoice(SaleInvoice& invoice) {
	invoice.builder().beginTransaction();
	bool success = invoice.create();
	if (success) {
		const core::eloquent::DBIDKey invoiceID = invoice.builder().lastInsertedId();
		Q_ASSERT(invoiceID > 0);
		invoice.setId(invoiceID);
		for (core::SaleInvoiceItem& item : invoice.items()) {
			item.setInvoice(invoiceID);
			if (!item.create()) {
				success = false;
				break;
			} else {
				auto id = item.lastInsertedId();
				Q_ASSERT(id > 0);
				item.setId(id);
			}
		}
	}
	if (success) {
		invoice.builder().commitTransaction();
	} else {
		invoice.builder().rollbackTransaction();
	}
	return success;
}

bool InvoiceRepository::updateSaleInvoice(SaleInvoice& invoice) {
	Q_ASSERT(invoice.id() > 0);

	invoice.builder().beginTransaction();
	bool success = invoice.save();
	if (success && !invoice.items().empty()) {
		// list of items was created before, now we can edit data in db
		// by deleting all items attached to editing invoice and recreated it
		if (core::SaleInvoiceItem saleInvoiceItem{};
			!saleInvoiceItem.builder().where("InvoiceId", invoice.id()).destroy())
		{
			success = false;
		}
		// in next step we will recteare all items in db
		for (core::SaleInvoiceItem& item : invoice.items()) {
			item.setInvoice(invoice.id());
			if (!item.create()) {
				success = false;
				break;
			} else {
				// store new id for item
				auto id = item.lastInsertedId();
				Q_ASSERT(id > 0);
				item.setId(id);
			}
		}
	}
	if (success) {
		invoice.builder().commitTransaction();
	} else {
		invoice.builder().rollbackTransaction();
	}
	return success;
}

bool InvoiceRepository::markSaleInvoiceAsCorrectionDocument(eloquent::DBIDKey orignalInvoiceId, eloquent::DBIDKey correctionInvoiceId) {
	Q_ASSERT(orignalInvoiceId > 0);
	Q_ASSERT(correctionInvoiceId > 0);

	core::SaleInvocieCorrections correction;
	correction.builder().beginTransaction();
	correction.setCorrectionInvoice(orignalInvoiceId);
	correction.setInvoice(correctionInvoiceId);
	bool success = correction.create();
	if (success)
		correction.builder().commitTransaction();
	else correction.builder().rollbackTransaction();
	return success;
}

QList<SaleInvoiceItem> InvoiceRepository::getSaleInvoiceItems(eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);

	core::SaleInvoiceItem saleInvoiceItem;
	saleInvoiceItem.builder().beginTransaction();
	QList<core::SaleInvoiceItem> items;

	items = saleInvoiceItem.builder().where("InvoiceId", saleInvoiceId).get();
	saleInvoiceItem.builder().commitTransaction();
	return items;
}

///////////////////////////////////////////////////////////////////////////////////////
/// PURCHASE INVOICES METHODS GROUP
///////////////////////////////////////////////////////////////////////////////////////

QList<PurchaseInvoice> InvoiceRepository::getAllPurchaseInvoices() {
	QCache<core::eloquent::DBIDKey, core::Warehouse> warehousesCache;
	QCache<core::eloquent::DBIDKey, core::Contractor> contractorsCache;

	core::repository::database::WarehouseRepository warehouseRepository;
	core::repository::database::ContractorRepository contractorRepository;

	QList<core::PurchaseInvoice> invoices;
	core::PurchaseInvoice invoice;
	invoice.builder().beginTransaction();
	for (const auto& model : invoice.getAllRecords()) {
		core::PurchaseInvoice obj {model};
		const core::eloquent::DBIDKey warehouseId = model.get("WarehouseId").toInt();
		if (!warehousesCache.contains(warehouseId)) {
			core::Warehouse* warehouse = new core::Warehouse {};
			*warehouse = warehouseRepository.getWarehouse(warehouseId);
			warehousesCache.insert(warehouseId, warehouse);
		}
		const core::eloquent::DBIDKey contractorId = model.get("ContractorId").toInt();
		if (!contractorsCache.contains(contractorId)) {
			core::Contractor* contractor = new core::Contractor {};
			*contractor = contractorRepository.get(contractorId);
			contractorsCache.insert(contractorId, contractor);
		}
		obj.setContractor(*contractorsCache[contractorId]);
		obj.setWarehouse(*warehousesCache[warehouseId]);
		invoices.append(obj);
	}
	invoice.builder().commitTransaction();
	return invoices;
}

PurchaseInvoice InvoiceRepository::getPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);

	QCache<core::eloquent::DBIDKey, core::Warehouse> warehousesCache;
	QCache<core::eloquent::DBIDKey, core::Contractor> contractorsCache;

	core::repository::database::WarehouseRepository warehouseRepository;
	core::repository::database::ContractorRepository contractorRepository;

	core::PurchaseInvoice invoice;
	invoice.builder().beginTransaction();
	invoice = invoice.findRecord(purchaseInvoiceId);
	const core::eloquent::DBIDKey warehouseId = invoice.get("WarehouseId").toInt();
	if (!warehousesCache.contains(warehouseId)) {
		core::Warehouse* warehouse = new core::Warehouse {};
		*warehouse = warehouseRepository.getWarehouse(warehouseId);
		warehousesCache.insert(warehouseId, warehouse);
	}
	const core::eloquent::DBIDKey contractorId = invoice.get("ContractorId").toInt();
	if (!contractorsCache.contains(contractorId)) {
		core::Contractor* contractor = new core::Contractor {};
		*contractor = contractorRepository.get(contractorId);
		contractorsCache.insert(contractorId, contractor);
	}
	invoice.setContractor(*contractorsCache[contractorId]);
	invoice.setWarehouse(*warehousesCache[warehouseId]);
	invoice.builder().commitTransaction();
	return invoice;
}

bool InvoiceRepository::hasPurchaseInvoiceCorrection(eloquent::DBIDKey purchaseInvoiceId, bool useCache) {
	Q_ASSERT(purchaseInvoiceId > 0);
	static QHash<core::eloquent::DBIDKey, bool> cache;
	if (useCache && cache.contains(purchaseInvoiceId)) {
		return cache[purchaseInvoiceId];
	}
	core::PurchaseInvoiceCorrection corrections;
	bool hasCorrection = corrections.builder().where("InvoiceId", purchaseInvoiceId).get().size() > 0;
	cache.insert(purchaseInvoiceId, hasCorrection);
	return cache[purchaseInvoiceId];
}

core::PurchaseInvoice InvoiceRepository::getCorrectionForPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);
	core::PurchaseInvoiceCorrection correction;
	correction = correction.builder().where("InvoiceId", purchaseInvoiceId).orderBy("CreatedDate").first();
	return getPurchaseInvoice(correction.get("CorrectionId").toInt());
}

bool InvoiceRepository::postPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);
	core::PurchaseInvoice invoice;
	invoice.setId(purchaseInvoiceId);
	invoice.set("Buffer", false);
	invoice.builder().beginTransaction();
	bool success = invoice.save();
	if (success)
		invoice.builder().commitTransaction();
	else invoice.builder().rollbackTransaction();
	return success;
}

QList<PurchaseInvoiceItem> InvoiceRepository::getPurchaseInvoiceItems(eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);

	core::PurchaseInvoiceItem purchaseInvoiceItem;
	purchaseInvoiceItem.builder().beginTransaction();
	QList<core::PurchaseInvoiceItem> items = purchaseInvoiceItem.builder().where("InvoiceId", purchaseInvoiceId).get();
	purchaseInvoiceItem.builder().commitTransaction();
	return items;
}

bool InvoiceRepository::createPurchaseInvoice(core::PurchaseInvoice& invoice) {
	Q_ASSERT(invoice.id() == 0);
	core::PurchaseInvoice().builder().beginTransaction();
	bool success = invoice.create();
	if (success) {
		core::eloquent::DBIDKey invoiceId = core::PurchaseInvoice().builder().lastInsertedId();
		Q_ASSERT(invoiceId > 0);
		if (success && !invoice.items().empty()) {
			for (auto& item : invoice.items()) {
				item.set("InvoiceId", invoiceId);
				success = item.create();
				if (!success) {
					break;
				} else {
					// store id for new invoice item
					auto id = item.lastInsertedId();
					Q_ASSERT(id > 0);
					item.setId(id);
				}
			}
		}
	}
	if (success) {
		core::PurchaseInvoice().builder().commitTransaction();
	} else {
		core::PurchaseInvoice().builder().rollbackTransaction();
	}
	return success;
}

bool InvoiceRepository::updatePurchaseInvoice(core::PurchaseInvoice& invoice) {
	Q_ASSERT(invoice.id() > 0);
	core::PurchaseInvoice().builder().beginTransaction();
	bool success = invoice.save();
	if (success && !invoice.items().empty()) {
		for (auto& item : invoice.items()) {
			item.set("InvoiceId", invoice.id());
			success = item.save();
			if (!success) {
				break;
			} else {
				// store id for updated item
				auto id = item.lastInsertedId();
				Q_ASSERT(id > 0);
				item.setId(id);
			}
		}
	}
	
	if (success) {
		core::PurchaseInvoice().builder().commitTransaction();
	} else {
		core::PurchaseInvoice().builder().rollbackTransaction();
	}
	return success;
}

bool InvoiceRepository::lockPurchaseInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_UNUSED(invoiceId);
	return true;
}

bool InvoiceRepository::unlockPurchaseInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_UNUSED(invoiceId);
	return true;
}

bool InvoiceRepository::lockSaleInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_UNUSED(invoiceId);
	return true;
}

bool InvoiceRepository::unlockSaleInvoice(const eloquent::DBIDKey& invoiceId) {
	Q_UNUSED(invoiceId);
	return true;
}

} // namespace core::repository::database
