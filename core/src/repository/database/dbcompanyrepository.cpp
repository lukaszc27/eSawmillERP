#include "repository/database/dbcompanyrepository.hpp"
#include <exception>
#include <dbo/user.h>

namespace core::repository::database {

bool CompanyRepository::update(Company& company) {
	bool success {false};
	core::Contact& contact = company.contact();
	contact.builder().beginTransaction();
	if (contact.save()) {
		if (company.save()) {
			Q_ASSERT(company.id() > 0 && company.contact().id() > 0);
			m_cache[company.id()] = company;	// update cache memory
			success = true;
		}
	}
	if (!success) {
		contact.builder().rollbackTransaction();
		return false;
	}

	contact.builder().commitTransaction();
	return true;
}

bool CompanyRepository::create(Company& company) {
	Q_ASSERT(company.id() == 0);

	bool success {false};
	core::Contact& contact = company.contact();
	contact.builder().beginTransaction();
	if (contact.save()) {
		company.set("ContactId", contact.lastInsertedId());
		if (company.create()) {
			company.setId(company.lastInsertedId());
			Q_ASSERT(company.id() > 0 && company.contact().id() > 0);
			m_cache[company.id()] = company;
			success = true;
		}
	}
	if (!success) {
		contact.builder().rollbackTransaction();
		return false;
	}

	contact.builder().commitTransaction();
	return true;
}

Company CompanyRepository::getCompany(eloquent::DBIDKey companyId) {
	Q_ASSERT(companyId > 0);

	// first check cache memory
	auto&& comparator = Comparator{companyId};
	auto mem = std::find_if(m_cache.begin(), m_cache.end(), comparator);
	if (mem != m_cache.end())
		return mem->second;

	core::Company company;
	company.builder().beginTransaction();
	company = company.findRecord(companyId);
	company.setContact(company.get("ContactId").toUInt());
	company.builder().commitTransaction();

	// add company into cache memory
	Q_ASSERT(company.id() > 0 && company.contact().id() > 0);
	m_cache[companyId] = company;

	return company;
}

} // namespace core::repository::database
