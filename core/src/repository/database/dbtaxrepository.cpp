#include "repository/database/dbtaxrepository.hpp"
#include <QCache>

namespace  core::repository::database {

QList<Tax> TaxRepository::getAll() {
	QList<core::Tax> taxs = core::Tax().getAllRecords();
	return taxs;
}

Tax TaxRepository::get(eloquent::DBIDKey taxId) {
	Q_ASSERT(taxId > 0);
	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&taxId](const std::pair<core::eloquent::DBIDKey, core::Tax>& item)->bool {
			return item.first == taxId;
	});
	if (itr != m_cache.end())
		return itr->second;

	// get tax from database
	core::Tax tax = core::Tax().findRecord(taxId);
	m_cache[taxId] = tax;

	return tax;
}

Tax TaxRepository::getByValue(double value) {
	// first find tax in cache
	auto itr = std::find_if(m_cache.begin(), m_cache.end(),
		[&value](const std::pair<core::eloquent::DBIDKey, core::Tax>& item)->bool {
			return item.second.value() == value;
	});
	if (itr != m_cache.end())
		return itr->second;

	// if tax not exist in cache, we have to get it from database
	core::Tax tax = core::Tax().builder().where("Value", value).first();
	Q_ASSERT(tax.id() > 0);
	m_cache[tax.id()] = tax;

	return tax;
}

bool TaxRepository::create(core::Tax& tax) {
	Q_ASSERT(tax.id() == 0);
	tax.builder().beginTransaction();
	bool success = tax.create();
	if (success) {
		auto id = tax.lastInsertedId();
		Q_ASSERT(id > 0);
		tax.setId(id);
		tax.builder().commitTransaction();

		// update cache
		m_cache[id] = tax;
	} else {
		tax.builder().rollbackTransaction();
	}
	return success;
}

bool TaxRepository::update(const core::Tax& tax) {
	core::Tax obj {tax};
	Q_ASSERT(obj.id() > 0);
	obj.builder().beginTransaction();
	bool success = obj.save();
	if (success) {
		obj.builder().commitTransaction();
		m_cache[obj.id()] = obj;
	} else {
		obj.builder().rollbackTransaction();
	}
	return success;
}

bool TaxRepository::destroy(eloquent::DBIDKey key) {
	Q_ASSERT(key > 0);
	core::Tax tax;
	tax.setId(key);
	tax.builder().beginTransaction();
	bool success = tax.destroy();
	if (success) {
		tax.builder().commitTransaction();
		// remove from cache memory
		auto itr = std::find_if(m_cache.begin(), m_cache.end(),
			[&key](const std::pair<core::eloquent::DBIDKey, core::Tax>& item)->bool {
				return item.first == key;
		});
		if (itr != m_cache.end())
			m_cache.erase(itr);
	} else {
		tax.builder().rollbackTransaction();
	}
	return success;
}

} // core::repository::database
