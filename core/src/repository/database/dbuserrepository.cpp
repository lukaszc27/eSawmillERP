#include "repository/database/dbuserrepository.hpp"
#include "eloquent/builder.hpp"
#include "eloquent/model.hpp"
#include "dbo/role.hpp"
#include <exception>
#include <stdexcept>
#include <QCache>
#include "repository/database/dbcompanyrepository.hpp"


namespace core::repository::database {

bool UserRepository::login(const QString& userName, const QString& password) {
	if (userName.isEmpty() || password.isEmpty())
		throw std::invalid_argument("invalid user name or password");

	// first check cache
	auto mem = std::find_if(m_usersCache.begin(), m_usersCache.end(),
		[&userName, &password](const std::pair<core::eloquent::DBIDKey, core::User>& item)->bool {
			return item.second.name() == userName && item.second.password() == password;
	});
	if (mem != m_usersCache.end()) {
		// now check roles
		const auto& [userId, user] = *mem;
		if (!user.hasRole(core::Role::Operator)) {
			// to login in system user must have operator role
			return false;
		}

		mCurrentUser = getUser(userId);
		return true;
	}

	// if user dont exist in cache we need find in remote service
	core::User user {};
	user = user.builder().where("Name", userName).where("Password", password).first();
	if (!user)
		return false;

	{ // now check role
		QList<core::UserRole> assigned_roles = core::UserRole().builder().where("UserId", user.id()).get();
		bool exist = std::any_of(assigned_roles.begin(), assigned_roles.end(),
			[](const core::UserRole& user_role)->bool {
				return user_role.roleId() == core::Role::SystemRole::Operator;
		});
		if (!exist)
			return false;	// user don't have operator role
	}

	// if user will be logged create all user object from data in db
	user = getUser(user.id());
	mCurrentUser = user;

	return true;
}

void UserRepository::logout() {
	mCurrentUser.setId(-1);
	// stub!
}

QList<User> UserRepository::getAllUsers() {
	QList<core::User> users {};
	if (m_usersCache.size() == 0) {
		auto&& companyRepository = core::repository::database::CompanyRepository{};
		// convert Collection to users list
		for (auto& item : core::User().getAllRecords()) {
			core::User user {item};
			user.setContact(user.get("ContactId").toUInt());
			core::eloquent::DBIDKey companyID = user.get("CompanyId").toUInt();
			Q_ASSERT(companyID > 0);
			user.setCompany(companyRepository.getCompany(companyID));

			// load assigned roles
			QList<core::UserRole> userRoles = core::UserRole().builder().where("UserId", user.id()).get();
			auto& roles = user.roles();
			for (const auto& userRole : userRoles) {
				roles.append(getRole(userRole.roleId()));
			}

			// add user to global list
			users.append(user);

			// update cache memory
			m_usersCache[user.id()] = user;
		}
	} else {
		// get users from cache
		for (const auto& [id, user] : m_usersCache) {
			users.append(user);
		}
	}
	return users;
}

User UserRepository::getUser(const eloquent::DBIDKey& userId) {
	Q_ASSERT(userId > 0);
	auto&& comparator = Comparator{userId};
	auto mem = std::find_if(m_usersCache.begin(), m_usersCache.end(), comparator);
	if (mem != m_usersCache.end())
		return mem->second;

	core::User user {};
	user.builder().beginTransaction();
	user = user.findRecord(userId);
	user.setContact(user.get("ContactId").toUInt());
	const core::Company& company = core::repository::database::CompanyRepository().getCompany(user.get("CompanyId").toUInt());
	user.setCompany(company);

	// load roles for user
	auto& roles = user.roles();
	QList<core::UserRole> user_roles = core::UserRole().builder().where("UserId", userId).get();
	for (const auto& user_role : user_roles) {
		roles.append(getRole(user_role.roleId()));
	}

	user.builder().commitTransaction();

	// update cache memory
	m_usersCache[user.id()] = user;

	return user;
}

bool UserRepository::create(User& user) {
	core::Contact& contact = user.contact();
	Q_ASSERT(user.id() == 0 && contact.id() == 0 && user.get("CompanyId").toUInt() > 0);

	bool success {false};
	contact.builder().beginTransaction();
	if (contact.create()) {
		const core::eloquent::DBIDKey contactID = contact.lastInsertedId();
		Q_ASSERT(contactID > 0);
		contact.setId(contactID);
		user.set("ContactId", contactID);
		success = user.create();
		if (success) {
			const core::eloquent::DBIDKey userId = user.lastInsertedId();
			Q_ASSERT(userId > 0);
			user.setId(userId);

			// create roles for user
			auto& roles = user.roles();
			Q_ASSERT(roles.size() > 0);	// roles list cannot be empty
			for (const auto& role : roles) {
				Q_ASSERT(role.id() > 0);
				core::UserRole userRole (userId, role.id());
				if (!userRole.create()) {
					success = false;
					break;
				}
			}

			// update cache
			if (success) {
				m_usersCache[user.id()] = user;
			}
		}
	}
	if (!success) {
		contact.builder().rollbackTransaction();
	} else {
		contact.builder().commitTransaction();
	}

	return success;
}

bool UserRepository::update(core::User& user) {
	core::Contact& contact = user.contact();
	Q_ASSERT(user.id() > 0 && contact.id() > 0);

	bool success {false};
	contact.builder().beginTransaction();
	// don't update password when is empty (user must have password)
	if (user.password().isEmpty())
		user.removeKey("Password");

	success = contact.save() && user.save();
	if (success) {
		// now recreate roles for user
		auto& roles = user.roles();
		Q_ASSERT(roles.size() > 0);	// roles list cannot be empty
		QList<core::UserRole> userRoleRelation = core::UserRole().builder().where("UserId", user.id()).get();
		// first remove unneeded roles
		for (auto& relation : userRoleRelation) {
			bool exist = std::any_of(roles.begin(), roles.end(),
				[&relation](const core::Role& role)->bool {
					return role.id() == relation.roleId();
			});
			if (exist)
				continue;

			// role has been remove by user
			if (!relation.destroy()) {
				success = false;
				break;
			}
		}

		// now create roles whom has been created by user on editing dialog
		for (auto& role : roles) {
			bool exist = std::any_of(userRoleRelation.begin(), userRoleRelation.end(),
				[&role](const core::UserRole& userRole)->bool {
					return userRole.roleId() == role.id();
			});
			if (exist)
				continue;

			// role has been created by user
			Q_ASSERT(role.id() > 0);
			core::UserRole user_role{user.id(), role.id()};
			if (!user_role.create()) {
				success = false;
				break;
			}
		}
	}

	if (success) {
		user.builder().commitTransaction();
		// update cache memory
		Q_ASSERT(user.id() > 0 && user.contact().id() > 0);
		m_usersCache[user.id()] = user;
	} else {
		user.builder().rollbackTransaction();
	}

	return success;
}

bool UserRepository::destroy(eloquent::DBIDKey& key) {
	if (key <= 0)
		throw std::invalid_argument("user key is invalid");

	core::User user;
	user.setId(key);
	user.builder().beginTransaction();
	bool success = user.builder().where("Id", key).destroy();
	if (success) {
		user.builder().commitTransaction();

		// remove user object from cache memory
		auto comparator = Comparator{key};	// RVO
		auto mem = std::find_if(m_usersCache.begin(), m_usersCache.end(), comparator);
		if (mem != m_usersCache.end())
			m_usersCache.erase(mem);
	} else {
		user.builder().rollbackTransaction();
	}
	return success;
}

RoleCollection UserRepository::getAllRoles() {
	RoleCollection roles;
	if (m_roles_cache.size() == 0) {
		// get roles from database and update cache memory
		roles = core::Role().getAllRecords();
		for (const auto& role : roles)
			m_roles_cache[role.id()] = role;
	} else {
		// get roles from cache memory
		for (const auto& [id, role] : m_roles_cache)
			roles.append(role);
	}
	return roles;
}


} // namespace core::repository::database
