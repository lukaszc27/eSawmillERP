#include "repository/database/dbwarehouserepository.hpp"
#include "dbo/wareHouse.h"
#include <QCache>
#include "repository/database/databaserepository.hpp"


namespace core::repository::database {

QList<Warehouse> WarehouseRepository::getAll() {
	QList<core::Warehouse> warehouses = core::Warehouse().getAllRecords();
	return warehouses;
}

Warehouse WarehouseRepository::getWarehouse(eloquent::DBIDKey id) {
	Q_ASSERT(id > 0);
	core::Warehouse warehouse;
//	warehouse.builder().beginTransaction();
	warehouse = static_cast<core::Warehouse>(warehouse.findRecord(id));
//	warehouse.builder().commitTransaction();
	return warehouse;
}

bool WarehouseRepository::create(Warehouse& warehouse) {
	core::Warehouse w = warehouse;
	w.builder().beginTransaction();
	bool success = w.create();
	if (!success)
		warehouse.builder().rollbackTransaction();
	else {
		warehouse.setId(warehouse.builder().lastInsertedId());
		warehouse.builder().commitTransaction();
	}
	return success;
}

bool WarehouseRepository::update(Warehouse& warehouse) {
	core::Warehouse w = warehouse;
	// on edit action id field must be valid value
	// in other case object will be created
	Q_ASSERT(w.id() > 0);
	w.builder().beginTransaction();
	bool success = w.save();
	if (!success)
		w.builder().rollbackTransaction();
	else w.builder().commitTransaction();
	return success;
}

bool WarehouseRepository::destroy(const eloquent::DBIDKey& warehouseId) {
	// to remove object id must be valid
	Q_ASSERT(warehouseId > 0);

	core::Warehouse warehouse;
	warehouse.setId(warehouseId);
	warehouse.builder().beginTransaction();
	bool success = warehouse.destroy();
	if (!success)
		warehouse.builder().rollbackTransaction();
	else warehouse.builder().commitTransaction();
	return success;
}

QList<Article> WarehouseRepository::getArticles(eloquent::DBIDKey warehouseId) {
	QCache<core::eloquent::DBIDKey, Tax> TaxsCache;
	QCache<core::eloquent::DBIDKey, MeasureUnit> unitsCache;
	QCache<core::eloquent::DBIDKey, Contractor> providersCache;
	QCache<core::eloquent::DBIDKey, ArticleType> articleTypesCache;
	QCache<core::eloquent::DBIDKey, Company> companiesCache;
	QCache<core::eloquent::DBIDKey, User> usersCache;

//	core::Article article;
	core::Article().builder().beginTransaction();
	// first get warehouse object
	const core::Warehouse warehouse = getWarehouse(warehouseId);

	// get all articles assigned to warehouse
	QList<core::Article> articles = core::Article().builder().where("WarehouseId", warehouseId).get();
//	QList<core::Article> articles;
	auto repository = core::repository::database::DatabaseRepository();
	for (core::Article& article : articles) {
		// cache article type
		const core::eloquent::DBIDKey articleTypeId = article.get("ArticleTypeId").toInt();
		if (articleTypeId > 0 && !articleTypesCache.contains(articleTypeId)) {
			core::ArticleType* articleType = new core::ArticleType {};
			*articleType = repository.articleRepository().getArticleType(articleTypeId);
			articleTypesCache.insert(articleTypeId, articleType);
		}
		const core::eloquent::DBIDKey saleTaxId = article.get("SaleVatId").toUInt();
		if (saleTaxId > 0 && !TaxsCache.contains(saleTaxId)) {
			core::Tax* Tax = new core::Tax {};
			*Tax = repository.TaxRepository().get(saleTaxId);
			TaxsCache.insert(saleTaxId, Tax);
		}
		const core::eloquent::DBIDKey purchaseTaxId = article.get("PurchaseVatId").toUInt();
		if (purchaseTaxId > 0 && !TaxsCache.contains(purchaseTaxId)) {
			core::Tax* Tax = new core::Tax {};
			*Tax = repository.TaxRepository().get(purchaseTaxId);
			TaxsCache.insert(purchaseTaxId, Tax);
		}
		const core::eloquent::DBIDKey unitId = article.get("UnitId").toUInt();
		if (unitId > 0 && !unitsCache.contains(unitId)) {
			core::MeasureUnit* unit = new core::MeasureUnit {};
			*unit = repository.measureUnitRepository().get(unitId);
			unitsCache.insert(unitId, unit);
		}
		const core::eloquent::DBIDKey providerId = article.get("ProviderId").toUInt();
		if (providerId > 0 && !providersCache.contains(providerId)) {
			core::Contractor* contractor = new core::Contractor {};
			*contractor = repository.contractorRepository().get(providerId);
			providersCache.insert(providerId, contractor);
		}
		const core::eloquent::DBIDKey companyId = article.get("CompanyId").toUInt();
		if (companyId > 0 && !companiesCache.contains(companyId)) {
			core::Company* company = new core::Company {};
			*company = repository.companiesRepository().getCompany(companyId);
			companiesCache.insert(companyId, company);
		}
		const core::eloquent::DBIDKey userId = article.get("UserAddId").toUInt();
		if (userId > 0 && !usersCache.contains(userId)) {
			core::User* user = new core::User {};
			*user = repository.userRepository().getUser(userId);
			usersCache.insert(userId, user);
		}
		article.setArticleType(*articleTypesCache[articleTypeId]);
		article.setProvider(*providersCache[providerId]);
		article.setWarehouse(warehouse);
		article.setPurchaseTax(*TaxsCache[purchaseTaxId]);
		article.setSaleTax(*TaxsCache[saleTaxId]);
		article.setCompany(*companiesCache[companyId]);
		article.setUnit(*unitsCache[unitId]);
		article.setUser(*usersCache[userId]);
	}
	core::Article().builder().commitTransaction();
	return articles;
}

bool WarehouseRepository::lock(const eloquent::DBIDKey& warehouseId) {
	Q_UNUSED(warehouseId);
	return true;
}

bool WarehouseRepository::unlock(const eloquent::DBIDKey& warehouseId) {
	Q_UNUSED(warehouseId);
	return true;
}

} // namespace core::repository::database
