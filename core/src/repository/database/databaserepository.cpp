#include "repository/database/databaserepository.hpp"
#include "repository/database/dbuserrepository.hpp"
#include "repository/database/dbcompanyrepository.hpp"
#include "repository/database/dbwarehouserepository.hpp"
#include "repository/database/dbtaxrepository.hpp"
#include "repository/database/dbwoodtyperepository.hpp"
#include "repository/database/dbarticlerepository.hpp"
#include "repository/database/dbmeasureunitrepository.hpp"
#include "repository/database/dbcontractorrepository.hpp"
#include "repository/database/dbinvoicerepository.hpp"
#include "repository/database/dborderrepository.hpp"
#include "repository/database/dbservicerepository.hpp"
#include "repository/database/dbcityrepository.hpp"
#include "database.h"
#include "globaldata.hpp"

namespace core::repository::database {

bool DatabaseRepository::connect(QObject* parent) {
	Q_UNUSED(parent);
	try {
		core::Database::connect();
	} catch (...) {
		core::GlobalData::instance().logger().critical("Nie można połączyć sie z serwerem bazy danych!");
		// in this place we can't show present message to user
		// only save information about error in application logs
		// and return false, in other place will be show information about error
		return false;
	}
	return true;
}

bool DatabaseRepository::disconnect() {
	core::Database::disconnect();
	core::GlobalData::instance().logger().information("Zamknięto połączenia z bazą danych");
	return true;
}

AbstractUserRepository& DatabaseRepository::userRepository() const {
	static UserRepository userRepository;
	return userRepository;
}

AbstractCompanyRepository& DatabaseRepository::companiesRepository() const {
	static CompanyRepository companyRepository;
	return companyRepository;
}

AbstractWarehouseRepository& DatabaseRepository::warehousesRepository() const {
	static WarehouseRepository warehouseRepository;
	return warehouseRepository;
}

AbstractTaxRepository& DatabaseRepository::TaxRepository() const {
	static core::repository::database::TaxRepository taxRepository;
	return taxRepository;
}

AbstractWoodTypeRepository& DatabaseRepository::woodTypeRepository() const {
	static WoodTypeRepository woodTypeRepository;
	return woodTypeRepository;
}

AbstractArticleRepository& DatabaseRepository::articleRepository() const {
	static ArticleRepository articleRepository;
	return articleRepository;
}

AbstractMeasureUnitRepository& DatabaseRepository::measureUnitRepository() const {
	static MeasureUnitRepository measureUnitRepository;
	return measureUnitRepository;
}

AbstractContractorRepository& DatabaseRepository::contractorRepository() const {
	static ContractorRepository contractorRepository;
	return contractorRepository;
}

AbstractInvoiceRepository& DatabaseRepository::invoiceRepository() const {
	static InvoiceRepository invoiceRepository;
	return invoiceRepository;
}

AbstractOrderRepository& DatabaseRepository::orderRepository() const {
	static OrderRepository orderRepository;
	return orderRepository;
}

AbstractServiceRepository& DatabaseRepository::serviceRepository() const {
	static ServiceRepository serviceRepository;
	return serviceRepository;
}

AbstractCityRepository& DatabaseRepository::cityRepository() const {
	static CityRepository cityRepository;
	return cityRepository;
}

} // namespace core::repository::database
