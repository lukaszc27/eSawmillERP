#include "repository/database/dborderrepository.hpp"
#include "globaldata.hpp"
#include "repository/database/dbcontractorrepository.hpp"
#include "repository/database/dbtaxrepository.hpp"
#include "repository/database/dbwoodtyperepository.hpp"
#include "repository/database/dbarticlerepository.hpp"
#include "dbo/orderarticle.h"
#include "dbo/article.h"
#include <QCache>

namespace core::repository::database {

QList<Order> OrderRepository::getAllOrders() {
	QCache<core::eloquent::DBIDKey, core::Contractor> contractorsCache;
	core::repository::database::ContractorRepository contractorRepository {};
	core::repository::database::TaxRepository taxRepository {};

	QList<core::Order> orders;
	core::Order().builder().beginTransaction();
	for (auto order : core::Order().getAllRecords()) {
		Q_ASSERT(order.id() > 0);
		const core::eloquent::DBIDKey contractorId = order.get("ContractorId").toInt();
		if (!contractorsCache.contains(contractorId)) {
			core::Contractor* contractor = new core::Contractor {};
			*contractor = contractorRepository.get(contractorId);
			contractorsCache.insert(contractorId, contractor);
		}
		const core::eloquent::DBIDKey taxId = order.get("VatId").toInt();
		order.setContractor(*contractorsCache[contractorId]);
		order.setTax(taxRepository.get(taxId));
		order.setElements(getOrderElements(order.id()));
		order.setArticles(core::order::Article().builder().where("OrderId", order.id()).get());
		orders.append(order);
	}

	core::Order().builder().commitTransaction();
	return orders;
}

QList<order::Element> OrderRepository::getOrderElements(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	core::repository::database::WoodTypeRepository woodRepository {};
	QCache<core::eloquent::DBIDKey, core::WoodType> woodsCache;

	core::order::Element element;
	element.builder().beginTransaction();
	QList<core::order::Element> elements = element.builder().where("OrderId", orderId).get();
	for (auto& el : elements) {
		const core::eloquent::DBIDKey woodId = el.get("WoodTypeId").toInt();
		if (!woodsCache.contains(woodId)) {
			core::WoodType* wood = new core::WoodType {};
			*wood = woodRepository.get(woodId);
			woodsCache.insert(woodId, wood);
		}
		el.setWoodType(*woodsCache[woodId]);
	}
	element.builder().commitTransaction();

	return elements;
}

Order OrderRepository::getOrder(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);

	core::Order order;
	order.builder().beginTransaction();
	order = order.findRecord(orderId);
	order.setContractor(order.get("ContractorId").toInt());
	order.setTax(order.get("VatId").toInt());
	order.setElements(getOrderElements(orderId));
	order.setArticles(core::order::Article().builder().where("OrderId", order.id()).get());
	order.builder().commitTransaction();
	return order;
}

QList<Article> OrderRepository::getArticlesForOrder(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);

	core::repository::database::ArticleRepository articleRepository {};
	core::order::Article().builder().beginTransaction();
	const QList<core::order::Article> orderArticles = core::order::Article().builder().where("OrderId", orderId).get();
	QList<core::Article> articles;
	for (const auto& orderArticle : orderArticles) {
		const core::eloquent::DBIDKey articleID = orderArticle.get("ArticleId").toInt();
		const double qty = orderArticle.get("Quantity").toDouble();
		core::Article article = articleRepository.getArticle(articleID);
		article.setQuantity(qty);
		articles.append(article);
	}
	core::order::Article().builder().commitTransaction();
	return articles;
}

bool OrderRepository::create(Order& order) {
	core::Order().builder().beginTransaction();
	bool success = order.create();
	if (success) {
		const core::eloquent::DBIDKey orderID = order.lastInsertedId();
		Q_ASSERT(orderID > 0);
		order.setId(orderID);

		if (auto& elements = order.elements(); !elements.empty()) {
			for (core::order::Element& el : elements) {
				el.set("OrderId", orderID);
				success = el.create();
				if (success) {
					el.setId(el.lastInsertedId());
					Q_ASSERT(el.id() > 0);
				} else {
					// error on saving data
					break;
				}
			}
		}
		if (auto& articles = order.articles(); success && !articles.empty()) {
			for (auto& clip : articles) {
				Q_ASSERT(clip.articleId() > 0);

				clip.setOrderId(orderID);
				success = clip.create();
				if (success)
					clip.setId(clip.lastInsertedId());
				else break;
			}
		}
	}

	if (success)
		core::Order().builder().commitTransaction();
	else core::Order().builder().rollbackTransaction();
	return success;
}

bool OrderRepository::update(Order& order) {
	Q_ASSERT(order.id() > 0);
	order.builder().beginTransaction();
	bool success = order.save();
	if (success) {
		{ // before update elements and articles assigned to order, we have to remove all items from db
			core::order::Element el;
			el.builder().where("OrderId", order.id()).destroy();
			core::order::Article article;
			article.builder().where("OrderId", order.id()).destroy();
		}

		if (auto& elements = order.elements(); success && !elements.empty()) {
			for (core::order::Element& element : elements) {
				element.set("OrderId", order.id());
				success = element.create();
				if (success) {
					if (element.id() == 0)	// update ID field for newset added items
						element.setId(element.lastInsertedId());

					Q_ASSERT(element.id() > 0);
				} else {
					// error on db operation
					break;
				}
			}
			if (auto& articles = order.articles(); success && !articles.empty()) {
				for (core::order::Article& clip : articles) {
					Q_ASSERT(clip.articleId() > 0);
					clip.setOrderId(order.id());
					success = clip.create();
					if (success) {
						if (clip.id() == 0)
							clip.setId(clip.lastInsertedId());

						Q_ASSERT(clip.id() > 0);
					} else break;
				}
			}
		}
	}

	if (success)
		order.builder().commitTransaction();
	else order.builder().rollbackTransaction();
	return success;
}

bool OrderRepository::destroy(eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	core::Order order;
	core::order::Element element;
	core::order::Article article;

	order.builder().beginTransaction();
	if (order.builder().where("Id", orderId).destroy()
		|| element.builder().where("OrderId", orderId).destroy()
		|| article.builder().where("OrderId", orderId).destroy())
	{
		order.builder().commitTransaction();
		return true;
	} else {
		order.builder().rollbackTransaction();
		return false;
	}
}

QDate OrderRepository::getFirstAvaiableDeadline() {
	const QString queryStr {QString("SELECT MAX(EndDate) AS [EndDate] FROM [%1]").arg(core::Order::tableName())};
	QSqlQuery query {queryStr, QSqlDatabase::database()};
	bool result = query.exec(queryStr) && query.first();
#if _DEBUG
	qDebug() << query.lastQuery();
	if (!result) {
		qCritical() << query.lastError().text();
	}
#endif //_DEBUG

	if (!result)
		return QDate::currentDate();

	QDate date = query.value("EndDate").toDate();
	if (date.isNull() || !date.isValid())
		return QDate::currentDate();
	return date;
}

bool OrderRepository::lock(const eloquent::DBIDKey& userId) {
	Q_UNUSED(userId);
	return true;
}

bool OrderRepository::unlock(const eloquent::DBIDKey& userId) {
	Q_UNUSED(userId);
	return true;
}

} // namespace core::repository::database
