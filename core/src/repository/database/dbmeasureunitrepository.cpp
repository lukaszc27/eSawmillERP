#include "repository/database/dbmeasureunitrepository.hpp"

namespace core::repository::database {

QList<MeasureUnit> MeasureUnitRepository::getAll() {
//	const QList<core::eloquent::Model> models = core::MeasureUnit().getAllRecords();
	QList<core::MeasureUnit> units = core::MeasureUnit().getAllRecords();
//	for (const auto& model : models)
//		units.append(model);

	return units;
}

MeasureUnit MeasureUnitRepository::get(eloquent::DBIDKey unitId) {
	Q_ASSERT(unitId > 0);
	return core::MeasureUnit().findRecord(unitId);
}

} // namespace core::repository::database
