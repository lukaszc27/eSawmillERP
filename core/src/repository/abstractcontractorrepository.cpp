#include "repository/abstractcontractorrepository.hpp"
#include <QMutexLocker>
#include <QWriteLocker>


namespace core::repository {

void AbstractContractorRepository::updateCache(const eloquent::DBIDKey& contractorId) {
	Q_ASSERT(contractorId > 0);

	removeFromCache(contractorId);
	get(contractorId);
}

void AbstractContractorRepository::removeFromCache(const eloquent::DBIDKey& contractorId) {
	Q_ASSERT(contractorId > 0);

	QWriteLocker locker(&m_cache_lock);
	auto itr = std::find_if(m_cache.begin(), m_cache.end(), Comparator{contractorId});
	if (itr != m_cache.end())
		m_cache.erase(itr);
}

bool AbstractContractorRepository::checkDoExistNipNumberForContractor(const QString& nip) {
	if (nip.isEmpty())
		throw std::invalid_argument("nip cannot be empty");

	QReadLocker locker(&m_cache_lock);
	if (m_cache.size() == 0)
		getAll();

	return std::any_of(m_cache.begin(), m_cache.end(),
		[&nip](const std::pair<core::eloquent::DBIDKey, core::Contractor>& item)->bool{
			return item.second.NIP() == nip;
	});
}

//////////////////////////////////////////////////////////////////////////////////////
AbstractContractorRepository::Comparator::Comparator(eloquent::DBIDKey contractorId)
	: _contractorId {contractorId}
{
}

bool AbstractContractorRepository::Comparator::operator()(const std::pair<eloquent::DBIDKey, Contractor>& item) const noexcept {
	return item.first == _contractorId;
}
//////////////////////////////////////////////////////////////////////////////////////

}

