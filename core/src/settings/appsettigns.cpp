#include "settings/appsettings.hpp"
#include <QSettings>
#include <QTextStream>
#include <exception>


namespace core::settings {

QString AuthInformation::connectionString() const {
	if (driver.isEmpty()
		|| host.isEmpty()
		|| databaseName.isEmpty()
		|| userName.isEmpty()
		|| password.isEmpty())
	{
		throw std::exception();
	}

	QString connectionString;
	QTextStream out {&connectionString};

	out << "DRIVER={" << driver << "};"
		<< "SERVER={" << host << "};"
		<< "DATABASE={" << databaseName << "};";

	if (!windowsAuthorization) {
		out << "UID={" << userName << "};"
			<< "PWD={" << password << "};";
	}

	if (trustedConnection)
		out << "TRUSTED_CONNECTION=YES";
	else out << "TRUSTED_CONNECTION=NO";

	return connectionString;
}

AppSettings::AppSettings(QObject* parent)
	: QSettings {parent}
{
}

AppSettings& AppSettings::instance() {
	static core::settings::AppSettings settings;
	return settings;
}

QVersionNumber AppSettings::applicationVersion() const {
	return QVersionNumber {1, 0, 0};
}

AuthInformation AppSettings::readDatabaseAuthInformation() {
	QSettings settings;
	AuthInformation authSettings;
	authSettings.driver = settings.value("database/driver", "ODBC Driver 17 for SQL Server").toString();
	authSettings.host = settings.value("database/host").toString();
	authSettings.databaseName = settings.value("database/databaseName", "SAWMILL").toString();
	authSettings.password = settings.value("database/userPassword").toString();
	authSettings.userName = settings.value("database/userName").toString();
	authSettings.trustedConnection = settings.value("database/trustedConnection").toBool();
	authSettings.windowsAuthorization = settings.value("database/windowsAuthorization").toBool();
	return authSettings;
}

void AppSettings::writeDatabaseAuthInformation(const AuthInformation& info) {
	QSettings settings;
	settings.setValue("database/driver", info.driver);
	settings.setValue("database/host", info.host);
	settings.setValue("database/userName", info.userName);
	settings.setValue("database/userPassword", info.password);
	settings.setValue("database/trustedConnection", info.trustedConnection);
	settings.setValue("database/windowsAuthorization", info.windowsAuthorization);
}

} // namespace core::settings
