#include "settings/invoices/purchaseinvoicesettings.hpp"

namespace core::settings::invoices {

PurchaseInvoiceSettings::PurchaseInvoiceSettings(QObject* parent)
	: core::settings::AbstractSettings {parent}
{
}

PurchaseInvoiceSettings& PurchaseInvoiceSettings::instance()
{
	static PurchaseInvoiceSettings settings;
	return settings;
}

QString PurchaseInvoiceSettings::group() const {
	return "PurchaseInvoice/";
}

} // namespace core::settings::invoices
