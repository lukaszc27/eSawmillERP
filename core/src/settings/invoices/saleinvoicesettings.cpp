#include "settings/invoices/saleinvoicesettings.hpp"

namespace core::settings::invoices {

SaleInvoiceSettings::SaleInvoiceSettings(QObject* parent)
	: core::settings::AbstractSettings {parent}
{
}

SaleInvoiceSettings& SaleInvoiceSettings::instance()
{
	static SaleInvoiceSettings settings;
	return settings;
}

QString SaleInvoiceSettings::group() const {
	return "SaleInvoice/";
}

} // namespace core::settings::invoices
