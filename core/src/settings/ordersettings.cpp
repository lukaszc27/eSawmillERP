#include "settings/ordersettings.hpp"
#include <QWidget>

namespace core::settings {

OrderSettings::OrderSettings(QObject* parent)
	: AbstractSettings {parent}
{
}

OrderSettings &OrderSettings::instance() {
	static core::settings::OrderSettings settings;
	return settings;
}

double OrderSettings::defaultPrice() const {
	return value(group() + "DefaultPrice", 1).toDouble();
}

double OrderSettings::defaultPlannedPrice() const {
	return value(group() + "DefaultPlannedPrice", 0).toDouble();
}

void OrderSettings::setDefaultPrice(double price) {
	setValue(group() + "DefaultPrice", price);
}

void OrderSettings::setDefaultPlannedPrice(double price) {
	setValue(group() + "DefaultPlannedPrice", price);
}

bool OrderSettings::deadlineAfterRemind() const {
	return value(group() + "DeadlineAfterRemind", true).value<bool>();
}

void OrderSettings::setDeadlineAfterRemind(bool remind) {
	setValue(group() + group() + "DeadlineAfterRemind", remind);
}

bool OrderSettings::deadlineBeforeRemind() const {
	return value(group() + "DeadlineBeforeRemind", true).value<bool>();
}

void OrderSettings::setDeadlineBeforeRemind(bool remind) {
	setValue(group() + "DeadlineBeforeRemind", remind);
}

bool OrderSettings::markByPriority() const {
	return value(group() + "MarkByPriority", true).value<bool>();
}

void OrderSettings::setMarkByPriority(bool enable) {
	setValue(group() + "MarkByPriority", enable);
}

QColor OrderSettings::deadlineAfterRemindColor() const {
	return value(group() + "DeadlineAfterRemindColor").value<QColor>();
}

void OrderSettings::setDeadlineAfterRemindColor(const QColor& color) {
	setValue(group() + "DeadlineAfterRemindColor", color);
}

QColor OrderSettings::deadlineBeforeRemindColor() const {
	return value(group() + "DeadlineBeforeRemindColor").value<QColor>();
}

void OrderSettings::setDeadlineBefoteRemindColor(const QColor& color) {
	setValue(group() + "DeadlineBeforeRemindColor", color);
}

QColor OrderSettings::realisedColor() const {
	return value(group() + "RealisedColor").value<QColor>();
}

void OrderSettings::setRealisedColor(const QColor& color) {
	setValue(group() + "RealisedColor", color);
}

int OrderSettings::daysToWarning() const {
	return value(group() + "DaysToWarning").value<int>();
}

void OrderSettings::setDaysToWarning(int days) {
	setValue(group() + "DaysToWarning", days);
}

QString OrderSettings::documentPrefix() const {
	return value(group() + "DocumentPrefix").value<QString>();
}

void OrderSettings::setDocumentPrefix(const QString& prefix) {
	setValue(group() + "DocumentPrefix", prefix);
}

int OrderSettings::minimumRealisedTime() const {
	return value(group() + "MinimumRealisedTime", 3).value<int>();
}

void OrderSettings::setMinimumRealisedTime(double days) {
	setValue(group() + "MinimumRealisedTime", days);
}

int OrderSettings::defaultItemQuantity() const {
	return value(group() + "DefaultItemQuantity", 1).toInt();
}

QString OrderSettings::defaultItemName() const {
	return value(group() + "DefaultItemName", tr("Belka")).toString();
}

} // namespace core::settings
