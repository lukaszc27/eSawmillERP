#include "settings/abstractsettings.hpp"

namespace core::settings {

AbstractSettings::AbstractSettings(QObject* parent)
	: QSettings {parent}
{
}

} // namespace core::settings
