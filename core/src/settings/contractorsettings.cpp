#include "settings/contractorsettings.hpp"
#include <settings/abstractsettings.hpp>

namespace core::settings {

ContractorSettings::ContractorSettings(QObject* parent)
	: core::settings::AbstractSettings {parent}
{
}

ContractorSettings& ContractorSettings::instance()
{
	static ContractorSettings settings;
	return settings;
}

QString ContractorSettings::group() const {
	return "ContractorManager/";
}

} // namespace core::settings
