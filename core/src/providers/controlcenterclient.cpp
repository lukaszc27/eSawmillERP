#include "providers/controlcenterclient.hpp"
#include <QSysInfo>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDataStream>
#include <QTextStream>
#include <QEventLoop>
#include <QTimer>
#include <iostream>
#include <cstring>

#include "exceptions/forbiddenexception.hpp"
#include "exceptions/repositoryexception.hpp"
#include "exceptions/useralreadyloggedinexception.hpp"
#include "exceptions/deviceunregisteredexception.hpp"
#include "providers/protocol.hpp"

namespace proto = core::providers::protocol;

namespace core::providers {

namespace {

template <typename Object>
QByteArray as_bytes(const Object& obj)
{
	QByteArray arr{};
	QDataStream out{&arr, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);
	out << obj;

	return arr;
}

} // namespace

ControlCenterClient::ControlCenterClient(QObject* parent)
	: QTcpSocket {parent}
{
	stream.setDevice(this);
	stream.setVersion(QDataStream::Version::Qt_5_0);

	connect(this, &QTcpSocket::readyRead, this, &ControlCenterClient::readPendindData, Qt::QueuedConnection);
}

static ControlCenterClient* _controlCenterClientSingelton;
ControlCenterClient* ControlCenterClient::createSingelton(QObject* parent) {
	if (_controlCenterClientSingelton == nullptr) {
		_controlCenterClientSingelton = new ControlCenterClient(parent);
		return _controlCenterClientSingelton;
	}
	return _controlCenterClientSingelton;
}

void ControlCenterClient::destroySingelton() {
	if (_controlCenterClientSingelton != nullptr) {
		delete _controlCenterClientSingelton;
		_controlCenterClientSingelton = nullptr;
	}
}

ControlCenterClient* ControlCenterClient::instance() {
	if (_controlCenterClientSingelton == nullptr) {
		throw std::bad_alloc();
	}
	return _controlCenterClientSingelton;
}

void ControlCenterClient::readPendindData()
{
	while (bytesAvailable() > 0)
	{
		pendingData_.append(readAll());

		if (pendingData_.size() < sizeof(proto::MessageHeader))
			continue;

		QDataStream in{&pendingData_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::MessageHeader header{};
		in >> header;

		const auto crc = qChecksum(pendingData_.data() + header.headerSize, header.dataSize);
		if (crc != header.crc)
			continue;

		if ((pendingData_.size() - header.headerSize) < header.dataSize)
			continue;

		const auto size = pendingData_.size() - header.headerSize;

		responsePayload_.clear();
		responsePayload_.resize(size);
		std::memcpy(responsePayload_.data(), pendingData_.data() + header.headerSize, size);
		pendingData_.clear();

		Q_EMIT dataReady();
	}
}

bool ControlCenterClient::waitForReadyData(int msecs)
{
	bool timeout {false};

	QEventLoop loop{this};
	connect(this, &ControlCenterClient::dataReady, &loop, &QEventLoop::quit);

	//QTimer timer{this};
	//timer.setSingleShot(true);
	//timer.setInterval(msecs);

	//connect(&timer, &QTimer::timeout, this, [&] {
	//	timeout = true;
	//	loop.quit();
	//});
	//timer.start();

	loop.exec();
	return !timeout;
}

ControlCenterClient::StatusCode ControlCenterClient::registerDevice() {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QByteArray buf{};
	QDataStream out{&buf, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);

	out << QSysInfo::machineUniqueId();
	out << QSysInfo::machineHostName();


	proto::MessageHeader header{};
	header.type = proto::msg::RegisterDevice;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	write(as_bytes(header));
	write(buf);
	flush();

	if (waitForReadyData())
	{
		QDataStream in{&responsePayload_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::DeviceStatus status{};
		in >> status;

		return status.registered
				? StatusCode::Accepted
				: StatusCode::Unauthorized;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::unregisterDevice()
{
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream out {&buf, QIODevice::WriteOnly};
		out.setVersion(QDataStream::Version::Qt_5_0);

		out << QSysInfo::machineUniqueId();

		proto::MessageHeader header{};
		header.type = proto::msg::UnregisterDevice;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream in {&responsePayload_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::DeviceStatus status{};
		in >> status;

		if (!status.registered)
		{
			m_uniqueDeviceId.clear();
			return StatusCode::Ok;
		}
		else
		{
			return StatusCode::NotFound;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request to server
//		stream << static_cast<std::uint32_t>(Protocol::MsgUnregisterDevice)
//			   << QSysInfo::machineUniqueId();

//		//		flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 8)
//				continue;

//			std::uint32_t msg;
//			if (stream >> msg; msg == Protocol::MsgDeviceStatus) {
//				stream >> msg;
//				if (msg == Protocol::MsgSuccess) {
//					m_uniqueDeviceId.clear();
//					return StatusCode::Ok;
//				}
//				return StatusCode::NotFound;
//			} else if (msg == Protocol::MsgMethodNotImplement) {
//				return StatusCode::NotImplemented;
//			}
//		}
//	} while(!stream.commitTransaction());

//	// waiting for response has been too long
}

ControlCenterClient::StatusCode ControlCenterClient::lock(
		agent::Resource resource,
		core::eloquent::DBIDKey resourceId,
		core::eloquent::DBIDKey userId)
{
	qDebug() << "CCC:lock";
	return StatusCode::Accepted;

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	Q_ASSERT(userId > 0 && resourceId > 0);
	{
		stream << static_cast<std::uint32_t>(Protocol::MsgLockResource)
			   << userId
			   << resourceId
			   << static_cast<int>(resource);

		flush();
	}

	if (waitForReadyRead()) {
		do {
			std::uint32_t msg;
			stream.startTransaction();
			stream >> msg;
			if (msg == Protocol::MsgSuccess) {
				return StatusCode::Accepted;
			} else if (msg == Protocol::MsgFail) {
				// resource has been blocked by other user
				core::eloquent::DBIDKey uid;
				QTime time;
				stream >> uid >> time;
				throw core::exceptions::ForbiddenException(userId, time);
			} else {
				throw core::exceptions::RepositoryException();
			}
		} while (!stream.commitTransaction());
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::unlock(agent::Resource resource, eloquent::DBIDKey resourceId) {
	qDebug() << "CCC::unlock";
	return StatusCode::Accepted;

	Q_ASSERT(resourceId > 0);
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		stream << static_cast<std::uint32_t>(Protocol::MsgUnlockResource)
			   << resourceId
			   << static_cast<int>(resource);

		flush();
	}
	if (waitForReadyRead()) {
		do {
			std::uint32_t msg {0};

			stream.startTransaction();
			stream >> msg;
			if (msg == Protocol::MsgSuccess) {
				return StatusCode::Accepted;
			} else if(msg == Protocol::MsgFail) {
				return StatusCode::NotFound;
			} else {
				throw core::exceptions::RepositoryException();
			}
		} while (!stream.commitTransaction());
	}
	return StatusCode::InternalServerError;
}

core::User ControlCenterClient::loginUser(const QString& userName, const QString& password) {
	if (!isOpen())
		return core::User{};

	{ // send request
		QByteArray buf{};
		QDataStream out{&buf, QIODevice::WriteOnly};
		out.setVersion(QDataStream::Version::Qt_5_0);

		out << userName << password << QSysInfo::machineUniqueId();

		proto::MessageHeader header{};
		header.type = proto::msg::LoginUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.crc = qChecksum(buf.constData(), buf.size());
		header.dataSize = buf.size();

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream in {&responsePayload_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::UserStatus status{};
		in >> status;

		switch (status)
		{
		case proto::UserStatus::Rejected:
			throw core::exceptions::DeviceUnregisteredException{};

		case proto::UserStatus::AlreadyLogged:
		{
			QString hostName;
			in >> hostName;

			core::exceptions::UserAlreadyLoggedinException ex{};
			ex.setHostName(hostName);
			throw ex;
		}

		case proto::UserStatus::Accepted:
		{
			core::User user{};
			in >> user;
			return user;
		}

		default:
			return core::User{};
		} // switch
	}
	return core::User{};
}

void ControlCenterClient::logoutUser(const eloquent::DBIDKey& userId)
{
	if (userId <= 0)
		throw std::invalid_argument("user id cannot be less or equal zero!");
	else if (!isOpen())
		throw std::bad_alloc();

	{ // send request
		QByteArray buf{};
		QDataStream out{&buf, QIODevice::WriteOnly};
		out.setVersion(QDataStream::Version::Qt_5_0);

		out << userId;

		proto::MessageHeader header{};
		header.type = proto::msg::LogoutUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream in{&responsePayload_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::UserStatus status{};
		in >> status;

		switch (status)
		{
		case proto::UserStatus::Unlogged:
			qDebug() << "User successfully unlogged from service";
			break;

		default:
			break;
		}
	}
}

ControlCenterClient::StatusCode ControlCenterClient::getAllUsers(QList<User>& users)
{
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request to service
		QByteArray buf{};

		proto::MessageHeader header{};
		header.type = proto::msg::GetAllUsers;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream in(&responsePayload_, QIODevice::ReadOnly);
		in.setVersion(QDataStream::Version::Qt_5_0);

		in >> users;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getUser(const core::eloquent::DBIDKey& userId, core::User& user)
{
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	if (userId <= 0)
		throw std::invalid_argument("userid can't be less or equal zero!");

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);

		writer << userId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::UserStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::UserStatus::Accepted:
			reader >> user;
			return StatusCode::Ok;

		default:
			return StatusCode::InternalServerError;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createUser(User& user) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << user;

		proto::MessageHeader header{};
		header.type = proto::msg::CreateUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> user;
			return StatusCode::Created;

		default:
			return StatusCode::InternalServerError;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateUser(User& user) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	Q_ASSERT(user.id() > 0);

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << user;

		proto::MessageHeader header{};
		header.type = proto::msg::UpdateUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> user;
			return StatusCode::Ok;

		default:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyUser(eloquent::DBIDKey userId) {
	if (userId <= 0)
		throw std::invalid_argument("userid cannot be less or equal zero!");

	Q_ASSERT(userId > 0);

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << userId;

		proto::MessageHeader header{};
		header.type = proto::msg::DestroyUser;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::InternalServerError;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getUserRoles(RoleCollection& roles) {
	qDebug() << "CCC::getUserRoles";

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetUserRoles;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);
		reader >> roles;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getCompany(core::eloquent::DBIDKey companyId, core::Company& company) {
	qDebug() << "CCC::getCompany";

	if (companyId < 0)
		throw std::invalid_argument("companyId can not by less or equal zero!");

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);

		writer << companyId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetCompany;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> company;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createCompany(Company& company) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray payload{};
		QDataStream writer{&payload, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << company;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.type = proto::msg::CreateCompany;
		header.dataSize = payload.size();
		header.crc = qChecksum(payload.constData(), payload.size());

		write(as_bytes(header));
		write(payload);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> company;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateCompany(Company& company) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray payload{};
		QDataStream writer{&payload, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << company;

		proto::MessageHeader header{};
		header.type = proto::msg::UpdateCompany;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = payload.size();
		header.crc = qChecksum(payload.constData(), payload.size());

		write(as_bytes(header));
		write(payload);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> company;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getAllMeasureUnits(QList<MeasureUnit>& units) {
	qDebug() << "CCC::getAllMeasureUnits";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetMeasureUnits;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> units;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getMeasureUnit(eloquent::DBIDKey unitId, core::MeasureUnit& unit) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << unitId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetMeasureUnit;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> unit;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getAllWarehouses(QList<Warehouse>& warehouses) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetAllWarehouses;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> warehouses;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getWarehouse(eloquent::DBIDKey warehouseId, Warehouse& warehouse) {
	Q_ASSERT(warehouseId > 0);

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << warehouseId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetWarehouse;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> warehouse;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createWarehouse(Warehouse& warehouse) {
	Q_ASSERT(warehouse.id() == 0);
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << warehouse;

		proto::MessageHeader header{};
		header.type = proto::msg::CreateWarehouse;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> warehouse;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request to server
//		stream << static_cast<std::uint32_t>(Protocol::MsgCreateWarehouse) << warehouse;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			std::uint32_t status {0};
//			if (bytesAvailable() < 4)
//				continue;

//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> warehouse;
//				return StatusCode::Created;
//			}
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateWarehouse(Warehouse& warehouse) {
	Q_ASSERT(warehouse.id() > 0);
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << warehouse;

		proto::MessageHeader header{};
		header.type = proto::msg::UpdateWarehouse;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> warehouse;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request
//		stream << static_cast<std::uint32_t>(Protocol::MsgUpdateWarehouse) << warehouse;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> warehouse;
//				return StatusCode::Ok;
//			}
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyWarehouse(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << warehouseId;

		proto::MessageHeader header{};
		header.type = proto::msg::DestroyWarehouse;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request to server
//		stream << static_cast<std::uint32_t>(Protocol::MsgDestroyWarehouse) << warehouseId;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				return StatusCode::Ok;
//			}
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getArticlesForWarehouse(eloquent::DBIDKey warehouseId, QList<Article>& articles) {
	Q_ASSERT(warehouseId > 0);
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << warehouseId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetArticles;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> articles;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request to server
//		stream << static_cast<std::uint32_t>(Protocol::MsgGetArticlesForWarehouse)
//			   << warehouseId;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> articles;
//				return StatusCode::Ok;
//			}
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getArticleTypes(QList<core::ArticleType>& types) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetArticleTypes;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> types;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getArticleType(eloquent::DBIDKey typeId, ArticleType& articleType) {
	Q_ASSERT(typeId > 0);

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << typeId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.type = proto::msg::GetArticleType;
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> articleType;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getArticle(eloquent::DBIDKey articleId, Article& article) {
	Q_ASSERT(articleId > 0);

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << articleId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.type = proto::msg::GetArticle;
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> article;

		case proto::OperationStatus::Fail:
			return StatusCode::NotFound;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateArticle(Article& article) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << article;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::UpdateArticle;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> article;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createArticle(Article& article) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << article;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::CreateArticle;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> article;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyArticle(eloquent::DBIDKey articleId) {
	if (articleId < 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << articleId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::DestroyArticle;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getAllTaxes(QList<Tax>& taxes) {
	qDebug() << "CCC::getAllTaxes";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;
		header.type = proto::msg::GetAllTaxes;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);
		reader >> taxes;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getTaxById(eloquent::DBIDKey id, core::Tax& tax) {
	if (id <= 0)
		throw std::invalid_argument("tax id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << id;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::GetTaxById;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> tax;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::NotFound;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getTaxByValue(double value, Tax& tax) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << value;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::GetTaxByValue;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> tax;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::NotFound;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createTax(Tax& tax) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	Q_ASSERT(tax.id() == 0);

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << tax;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::CreateTax;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> tax;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateTax(const core::Tax& tax) {
	if (tax.id() <= 0)
		throw std::invalid_argument("id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << tax;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::UpdateTax;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
		{
			core::Tax t{};
			reader >> t;
		}
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyTax(eloquent::DBIDKey id) {
	if (id <= 0)
		throw std::invalid_argument("id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << id;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::DestroyTax;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getAllWoodsType(QList<core::WoodType>& woods) {
	qDebug() << "CCC::getAllWoodsType";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		proto::MessageHeader header{};
		header.type = proto::msg::GetWoodTypes;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> woods;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getWoodType(eloquent::DBIDKey id, WoodType& type) {
	qDebug() << "CCC::getWoodType";

	if (id <= 0)
		throw std::invalid_argument("id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << id;

		proto::MessageHeader header{};
		header.type = proto::msg::GetWoodType;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> type;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createWoodType(WoodType& wood) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << wood;

		proto::MessageHeader header{};
		header.type = proto::msg::CreateWoodType;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> wood;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateWoodType(WoodType& wood) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << wood;

		proto::MessageHeader header{};
		header.type = proto::msg::UpdateWoodType;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> wood;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyWoodType(eloquent::DBIDKey woodId) {
	if (woodId <= 0)
		throw std::invalid_argument("wood id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << woodId;

		proto::MessageHeader header{};
		header.type = proto::msg::DestroyWoodType;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getAllContractors(QList<Contractor>& contractors) {
	qDebug() << "ControlCenterClient::getAllContractors";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetAllContractors;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream in{&responsePayload_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		in >> contractors;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getContractor(eloquent::DBIDKey id, Contractor& contractor) {
	qDebug() << "CCC::getContractor";

	if (id <= 0)
		throw std::invalid_argument("id cannot be equal or less than zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << id;

		proto::MessageHeader header{};
		header.type = proto::msg::GetContractor;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> contractor;
			return StatusCode::Ok;

		default:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateContractor(Contractor& contractor) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << contractor;

		proto::MessageHeader header{};
		header.type = proto::msg::UpdateContractor;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;
		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> contractor;
			return StatusCode::Ok;

		default:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createContractor(Contractor& contractor) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << contractor;

		proto::MessageHeader header{};
		header.type = proto::msg::CreateContractor;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> contractor;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyContractor(eloquent::DBIDKey id) {
	if (id <= 0)
		throw std::invalid_argument("id cannot be equal or less than zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << id;

		proto::MessageHeader header{};
		header.type = proto::msg::DestroyContractor;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getSaleInvoices(QList<SaleInvoice>& invoices) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;
		header.type = proto::msg::GetAllSaleInvoices;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> invoices;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request
//		stream << static_cast<std::uint32_t>(Protocol::MsgGetAllSaleInvoices);
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> invoices;
//				return StatusCode::Ok;
//			}
//		}
//	} while(!stream.commitTransaction());
//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getSaleInvoice(eloquent::DBIDKey id, SaleInvoice& invoice) {
	if (id <= 0)
		throw std::invalid_argument("id cannot be less or equal zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{
		stream << static_cast<std::uint32_t>(Protocol::MsgGetSaleInvoice) << id;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateSaleInvoice(SaleInvoice& invoice) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // send request
		stream << static_cast<std::uint32_t>(Protocol::MsgUpdateSaleInvoice) << invoice;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createSaleInvoice(SaleInvoice& invoice) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request
//		stream << static_cast<std::uint32_t>(Protocol::MsgCreateService) << invoice;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> invoice;
//				return StatusCode::Created;
//			}
//		}
//	} while(!stream.commitTransaction());
//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getSaleInvoiceAttributes(eloquent::DBIDKey id, QMap<QString, QVariant>& map) {
	if (id <= 0)
		throw std::invalid_argument("id cannot be equal or less than zero!");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::markSaleInvoiceAsCorrectionDocument(
		eloquent::DBIDKey orginalId, eloquent::DBIDKey correctionId)
{
	if (orginalId <=0 || correctionId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // prepare request
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgMarkSaleInvoiceAsCorrectionDocument)
			   << orginalId << correctionId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getSaleInvoiceCorrection(eloquent::DBIDKey orginalId, SaleInvoice& invoice) {
	if (orginalId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // prepare request
		stream << static_cast<std::uint32_t>(Protocol::MsgGetSaleInvoiceCorrection)
			   << orginalId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::postSaleInvoice(eloquent::DBIDKey invoiceId) {
	if (invoiceId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // prepare request
		stream << static_cast<std::uint32_t>(Protocol::MsgPostSaleInvoice)
			   << invoiceId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getPurchaseInvoices(QList<PurchaseInvoice>& invoices) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;
		header.type = proto::msg::GetAllPurchaseInvoices;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);
		reader >> invoices;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // prepare request
//		stream << static_cast<std::uint32_t>(Protocol::MsgGetAllPurchaseInvoices);
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status {0};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> invoices;
//				return StatusCode::Ok;
//			}
//		}
//	} while(!stream.commitTransaction());
//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getPurchaseInvoice(eloquent::DBIDKey invoiceId, PurchaseInvoice& invoice) {
	if (invoiceId < 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // send request
		stream << static_cast<std::uint32_t>(Protocol::MsgGetPurchaseInvoice)
			   << invoiceId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());
	return StatusCode::BadRequest;
}

ControlCenterClient::StatusCode ControlCenterClient::getPurchaseInvoiceItems(eloquent::DBIDKey id, QList<PurchaseInvoiceItem>& items) {
	if (id <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // prepare request
		stream << static_cast<std::uint32_t>(Protocol::MsgGetItemsForPurchaseInvoice)
			   << id;
		//flush();
	}
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			stream.startTransaction();
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> items;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getPurchaseInvoiceAttributes(eloquent::DBIDKey id, QMap<QString, QVariant>& map) {
	if (id <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::postPurchaseInvoice(eloquent::DBIDKey purchaseInvoiceId) {
	if (purchaseInvoiceId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{ // send request
		stream << static_cast<std::uint32_t>(Protocol::MsgPostPurchaseInvoice)
			   << purchaseInvoiceId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			std::uint32_t status {0};
			if (bytesAvailable() < 4)
				continue;

			if (stream >> status; status == Protocol::MsgSuccess)
				return StatusCode::Ok;
		}
	} while(!stream.commitTransaction());

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getPurchaseInvoiceCorrection(eloquent::DBIDKey orginalId, PurchaseInvoice& invoice) {
	if (orginalId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{
		stream << static_cast<std::uint32_t>(Protocol::MsgGetPurchaseInvoiceCorrection)
			   << orginalId;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				if (bytesAvailable() < sizeof(PurchaseInvoice))
					continue;

				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updatePurchaseInvoice(PurchaseInvoice& invoice) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{
		stream << static_cast<std::uint32_t>(Protocol::MsgUpdatePurchaseInvoice)
			   << invoice;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			std::uint32_t status {0};
			if (bytesAvailable() < 4)
				continue;

			if (stream >> status; status == Protocol::MsgSuccess) {
				if (bytesAvailable() < sizeof(PurchaseInvoice))
					continue;

				stream >> invoice;
				return StatusCode::Ok;
			}
		}
	} while(!stream.commitTransaction());

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createPurchaseInvoice(core::PurchaseInvoice& invoice) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	QDataStream stream {this};
	stream.setVersion(QDataStream::Version::Qt_5_0);

	{
		stream << static_cast<std::uint32_t>(Protocol::MsgCreatePurchaseInvoice)
			   << invoice;
		//flush();
	}

	stream.startTransaction();
	do {
		if (waitForReadyRead()) {
			if (bytesAvailable() < 4)
				continue;

			std::uint32_t status {0};
			if (stream >> status; status == Protocol::MsgSuccess) {
				stream >> invoice;
				return StatusCode::Created;
			}
		}
	} while(!stream.commitTransaction());

	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getOrder(eloquent::DBIDKey orderId, Order& order) {
	if (orderId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << orderId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::GetOrder;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> order;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getOrders(QList<Order>& orders) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;
		header.type = proto::msg::GetAllOrders;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		reader >> orders;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateOrder(Order& order) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << order;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::UpdateOrder;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> order;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createOrder(Order& order) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << order;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::CreateOrder;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> order;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyOrder(eloquent::DBIDKey orderId) {
	if (orderId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << orderId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::DestroyOrder;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getService(eloquent::DBIDKey serviceId, Service& service) {
	if (serviceId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << serviceId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::GetService;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> service;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getServices(QList<Service>& services) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;
		header.type = proto::msg::GetAllServices;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);
		reader >> services;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::destroyService(eloquent::DBIDKey serviceId) {
	if (serviceId <= 0)
		throw std::invalid_argument("");
	else if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << serviceId;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::DestroyService;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::updateService(Service& service) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << service;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::UpdateService;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> service;
			return StatusCode::Ok;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createService(Service& service) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << service;

		proto::MessageHeader header{};
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());
		header.type = proto::msg::CreateServiceResponse;

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> service;
			return StatusCode::Created;

		case proto::OperationStatus::Fail:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getCities(QList<City>& cities) {
	qDebug() << "CCC::getCities";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	{ // send request
		proto::MessageHeader header{};
		header.type = proto::msg::GetAllCities;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = 0;
		header.crc = 0;

		write(as_bytes(header));
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);
		reader >> cities;
		return StatusCode::Ok;
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request to server
//		stream << static_cast<std::uint32_t>(Protocol::MsgGetCities);
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			std::uint32_t status {};
//			if (bytesAvailable() < 4)
//				continue;

//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> cities;
//				return StatusCode::Ok;
//			} else if (status == MsgMethodNotImplement)
//				return StatusCode::NotImplemented;
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::getCity(const core::eloquent::DBIDKey& cityId, City& city) {
	qDebug() << "CCC::getCity";

	if (!isOpen())
		return StatusCode::ServiceUnavaiable;
	if (cityId <= 0)
		throw std::invalid_argument("id cannot be equal or less than zero");

	{ // send request
		QByteArray payload{};
		QDataStream writer{&payload, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << cityId;

		proto::MessageHeader header{};
		header.type = proto::msg::GetCity;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = payload.size();
		header.crc = qChecksum(payload.constData(), payload.size());

		write(as_bytes(header));
		write(payload);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader{&responsePayload_, QIODevice::ReadOnly};
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;
		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> city;
			return StatusCode::Ok;

		default:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request
//		stream << static_cast<std::uint32_t>(Protocol::MsgGetCity)
//			   << cityId;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status{};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> city;
//				return StatusCode::Ok;
//			} else if (status == Protocol::MsgMethodNotImplement)
//				return StatusCode::NotImplemented;
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

ControlCenterClient::StatusCode ControlCenterClient::createCity(City& city) {
	if (!isOpen())
		return StatusCode::ServiceUnavaiable;

	Q_ASSERT(city.id() == 0);

	{ // send request
		QByteArray buf{};
		QDataStream writer{&buf, QIODevice::WriteOnly};
		writer.setVersion(QDataStream::Version::Qt_5_0);
		writer << city;

		proto::MessageHeader header{};
		header.type = proto::msg::CreateCity;
		header.headerSize = sizeof(proto::MessageHeader);
		header.dataSize = buf.size();
		header.crc = qChecksum(buf.constData(), buf.size());

		write(as_bytes(header));
		write(buf);
		flush();
	}

	if (waitForReadyData())
	{
		QDataStream reader(&responsePayload_, QIODevice::ReadOnly);
		reader.setVersion(QDataStream::Version::Qt_5_0);

		proto::OperationStatus status{};
		reader >> status;

		switch (status)
		{
		case proto::OperationStatus::Successfull:
			reader >> city;
			return StatusCode::Ok;

		default:
			return StatusCode::BadRequest;
		}
	}
	return StatusCode::InternalServerError;
//	QDataStream stream {this};
//	stream.setVersion(QDataStream::Version::Qt_5_0);

//	{ // send request
//		stream << static_cast<std::uint32_t>(Protocol::MsgCreateCity)
//			   << city;
//		//flush();
//	}

//	stream.startTransaction();
//	do {
//		if (waitForReadyRead()) {
//			if (bytesAvailable() < 4)
//				continue;

//			std::uint32_t status{};
//			if (stream >> status; status == Protocol::MsgSuccess) {
//				stream >> city;
//				return StatusCode::Created;
//			} else if (status == Protocol::MsgMethodNotImplement)
//				return StatusCode::NotImplemented;
//		}
//	} while(!stream.commitTransaction());

//	return StatusCode::InternalServerError;
}

} // namespace core::providers
