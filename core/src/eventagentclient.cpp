#include "eventagentclient.hpp"
#include "globaldata.hpp"
#include <QNetworkDatagram>
#include <QNetworkProxy>
#include <QNetworkInterface>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QMutexLocker>
#include <QAbstractSocket>
#include <QDataStream>

namespace core {

EventAgentClient::EventAgentClient(const QHostAddress& address, quint16 port, QObject* parent)
	: QThread{parent}
	, m_address {address}
	, m_port {port}
{
	m_socket = new QUdpSocket(this);
}

EventAgentClient::~EventAgentClient() {
	if (isRunning())
		this->exit();
}

void EventAgentClient::makeConnection() {
	connect(m_socket, &QAbstractSocket::errorOccurred, this, &EventAgentClient::notifyError);

	m_socket->setProxy(QNetworkProxy::NoProxy);
	m_socket->bind(QHostAddress::AnyIPv4, m_port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);

	if (m_socket->state() == QUdpSocket::SocketState::BoundState) {
		m_socket->joinMulticastGroup(m_address);

#ifdef _DEBUG
		qDebug() << "Otwarto połączenie dla wymiany danych o zdarzeniach";
		qDebug() << "Address: " << m_address.toString() << "\t"
				 << "Port: " << m_socket->localPort();
#endif
	} else {
		qWarning() << m_socket->errorString();
		Q_EMIT notifyError(QAbstractSocket::SocketAccessError);
	}
	connect(m_socket, &QUdpSocket::readyRead, this, &EventAgentClient::readPendingDatagrams);
}

static EventAgentClient* _singeltonInstance = nullptr;
EventAgentClient* EventAgentClient::createSingelton(const QHostAddress& address, quint16 port, QObject* parent) {
	if (_singeltonInstance == nullptr)
		_singeltonInstance = new EventAgentClient(address, port, parent);

	return _singeltonInstance;
}

void EventAgentClient::destroySingelton() {
	if (_singeltonInstance != nullptr)
		delete _singeltonInstance;
}

EventAgentClient* EventAgentClient::instance() {
	return _singeltonInstance;
}

void EventAgentClient::run() {
	while (!isInterruptionRequested()) {
		QMutexLocker locker (&m_mutex);
		if (!m_events.empty())
		{
			auto& event = m_events.front();
			const std::uint32_t crc = qChecksum(event.payload, event.payloadSize);
			if (crc == event.crc)
			{
				switch (event.type)
				{
				case core::providers::agent::EventType::RepositorySync:
				{
					QDataStream in{&event.payload, QIODevice::ReadOnly};
					core::providers::agent::RepositoryEvent repoEvent;
					in >> repoEvent;

					Q_EMIT notifyRepositoryEventReceived(repoEvent);
				} break;

				case core::providers::agent::EventType::ServerStatus:
					QDataStream in{&event.payload, QIODevice::ReadOnly};
					core::providers::agent::ServerStatusEvent statusEvent;
					in >> statusEvent;

					Q_EMIT notifyServerStatusEventReceived(statusEvent);
					break;
				}
			}

			m_events.pop();
		}
	}
}

void EventAgentClient::readPendingDatagrams() {
	while (m_socket->hasPendingDatagrams()) {
		const QNetworkDatagram datagram = m_socket->receiveDatagram();
		core::providers::agent::Event event{};
		QByteArray data = datagram.data();
		QDataStream in(&data, QIODevice::ReadOnly);
		in >> event;

		QMutexLocker locker(&m_mutex);
		m_events.push(event);
	}
}

} // namespace core
