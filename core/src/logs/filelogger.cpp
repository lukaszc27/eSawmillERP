#include "logs/filelogger.hpp"
#include <QDir>
#include <QDate>
#include <QTime>
#include <exception>
#include <QMutexLocker>
#include <QTextStream>
#include <QDebug>

namespace core::logs {

FileLogger::FileLogger() {
	QDir dir = QDir::currentPath();
	if (!dir.exists("logs")) {
		dir.mkdir("logs");
		dir.cd("logs");	// go into logs
	}

	const QString filename = QString("%1/%2.log").arg(dir.path(), QDate::currentDate().toString("ddMMyyyy"));
	_file = new QFile(filename);
	if (!_file->open(QFile::WriteOnly | QFile::Append))
		throw std::bad_exception();
}

FileLogger::~FileLogger() {
	if (_file->isOpen() || _file != nullptr) {
		_file->close();
		_file = nullptr;
	}
}

FileLogger::operator bool() {
	return _file != nullptr && _file->isOpen();
}

void FileLogger::log(const QString& message, Type type) {
	if (!(*this))
		return;	// FileLogger object is unavaiable

	QTextStream out{_file};
	const QString date = QDate::currentDate().toString();
	const QString time = QTime::currentTime().toString();

	QStringList levels = QStringList() << "Informacja" << "Ostrzeżenie" << "Błąd" << "DEBUG" << "TRACE";
	int levelIdx = static_cast<int>(type);

	out << date << ';' << time << ';' << levels[levelIdx] << ';' << message << '\n';

	if (type == Type::Debug || type == Type::Trace)
		qDebug() << message;
}

} // namespace core::logs
