#include "exceptions/invalidimportexception.hpp"

namespace core::exceptions {

InvalidImportException::InvalidImportException()
	: QException {}
	, _message {""}
{
}

InvalidImportException::InvalidImportException(const QString& msg)
	: InvalidImportException {}
{
	_message = msg;
}

void InvalidImportException::raise() const {
	throw *this;
}

InvalidImportException* InvalidImportException::clone() const {
	return new InvalidImportException {*this};
}

} // namespace core::exceptions
