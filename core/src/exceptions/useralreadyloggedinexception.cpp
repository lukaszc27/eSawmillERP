#include "exceptions/useralreadyloggedinexception.hpp"

namespace core::exceptions {

UserAlreadyLoggedinException::UserAlreadyLoggedinException()
	: QException {}
{
}

void UserAlreadyLoggedinException::raise() const {
	throw *this;
}

UserAlreadyLoggedinException* UserAlreadyLoggedinException::clone() const {
	return new UserAlreadyLoggedinException {*this};
}

void UserAlreadyLoggedinException::setHostName(const QString& hostName) {
	_hostName = hostName;
}

QString UserAlreadyLoggedinException::hostName() const {
	return _hostName;
}

}
