#include "exceptions/forbiddenexception.hpp"

namespace core::exceptions {

ForbiddenException::ForbiddenException(eloquent::DBIDKey userId, const QTime& time)
	: QException {}
	, _userId {userId}
	, _time {time}
{
}

void ForbiddenException::raise() const {
	throw *this;
}

ForbiddenException* ForbiddenException::clone() const {
	return new ForbiddenException {*this};
}

} // namespace core::exceptions
