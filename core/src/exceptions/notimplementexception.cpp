#include "exceptions/notimplementexception.hpp"

namespace core::exceptions {

NotImplementException::NotImplementException()
	: QException{}
{
}

void NotImplementException::raise() const {
	throw *this;
}

NotImplementException* NotImplementException::clone() const {
	return new NotImplementException{*this};
}

} // namespace core::exceptions
