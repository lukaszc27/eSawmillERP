#include "exceptions/deviceunregisteredexception.hpp"

namespace core::exceptions {

DeviceUnregisteredException::DeviceUnregisteredException()
	: QException {}
{
}

void DeviceUnregisteredException::raise() const {
	throw *this;
}

DeviceUnregisteredException* DeviceUnregisteredException::clone() const {
	return new DeviceUnregisteredException {*this};
}

} // namespace core::exceptions
