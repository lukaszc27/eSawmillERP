#include "exceptions/repositoryexception.hpp"

namespace core::exceptions {

RepositoryException::RepositoryException()
	: QException{}
{
}

void RepositoryException::raise() const {
	throw *this;
}

RepositoryException* RepositoryException::clone() const {
	return new RepositoryException {*this};
}



} // namespace core::repository
