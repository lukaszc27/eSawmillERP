#include "database.h"
#include "simplecrypt.hpp"
#include "dbo/tax.h"
#include "dbo/wareHouse.h"
#include "dbo/service.h"
#include "dbo/order.h"
#include "dbo/company.h"
#include "dbo/orderElements.h"
#include "dbo/orderarticle.h"
#include "dbo/serviceelement.h"
#include "dbo/servicearticle.h"
#include <settings/appsettings.hpp>
#include <QFile>
#include <QIODevice>
#include <QDataStream>
#include <QSqlError>
#include <QSqlDatabase>
#include <QtXml>
#include <QStringBuilder>
#include <QSettings>


namespace core
{

/**
 * @brief Database::connect
 * Nawiązuje połączenie z serwerem SQL
 * dane potrzebne do nawiązania połączenia pobrane z pliku
 */
void Database::connect() {
	// create connection string and
	// connect to eSawmill database
	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	QSettings settings;
	{
		QSqlDatabase database = QSqlDatabase::addDatabase("QODBC");
		database.setDatabaseName(core::settings::AppSettings::instance().readDatabaseAuthInformation().connectionString());
		if (!database.open())
			throw QString(database.lastError().text());
	}

	// connect to Comarch ERP Optima database
	// if user want to integrate this app
	// with Comarch
	if (settings.value("comarch/allowToConnect").toBool()) {
		qInfo() << "Connect to Comarch ERP Optima database";

		// build connection string for comarch db
		const QString driver = settings.value("comarch/driver").toString();
		const QString host = settings.value("comarch/host").toString();
		const QString database = settings.value("comarch/database").toString();
		const QString username = settings.value("comarch/userName").toString();
		const QString password = crypt.decryptToString(settings.value("comarch/userPassword").toString());

		QString cs = QString("DRIVER={%1};SERVER=%2;DATABASE=%3;")
				.arg(driver, host, database);

		if (!settings.value("comarch/windowsAuth").toBool()) {
			// user isn't check option Windows auth, so user have to give
			// username and password
			cs = cs.append("UID=%1;PWD=%2;").arg(username, password);
		}

		QString trustedConnection = "";
		if (settings.value("comarch/trustedConnection").toBool())
			trustedConnection = "YES";
		else trustedConnection = "NO";

		cs = cs.append("Trusted_Connection=%1;").arg(trustedConnection);

		QSqlDatabase comarchDatabase = QSqlDatabase::addDatabase("QODBC", "comarch");
		comarchDatabase.setDatabaseName(cs);

		if (!comarchDatabase.open()) {
			throw QString("Nie można nawiązać połączenia z bazą danych Comarch ERP!\n"
						  "ConnectionString: %1").arg(cs);
		}
	}
}

void Database::disconnect() {
	// close connection with Comarch database if exist
	if (auto db = QSqlDatabase::database("comarch"); db.isOpen())
		db.close();

	// close main db connection
	if (auto db = QSqlDatabase::database(); db.isOpen())
		db.close();
}

void Database::writeConfig(const QString &driver,
						   const QString &host,
						   const QString &userName,
						   const QString &password,
						   const QString &dbName,
						   bool trustedConnection)
{
	// store inforamtion about connection in QSettings
	// it's better than manually create file with connection string
	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	QSettings settings;
	settings.setValue("database/driver", driver);
	settings.setValue("database/host", host);
	settings.setValue("database/userName", userName);
	settings.setValue("database/userPassword", crypt.encryptToString(password));
	settings.setValue("database/name", dbName);
	settings.setValue("database/trustedConnection", trustedConnection);
}

bool Database::restore(const QString fileName) {
	const QSqlDatabase db = QSqlDatabase::database();	// default db connection for sawmill database
	QSqlQuery query(db);

	if (!query.exec("USE [master]")) {
		qDebug() << query.lastError().text();
		return false;
	}

	QSettings settings;
	QString queryString = QString("RESTORE DATABASE [%1] FROM DISK = '%2' WITH REPLACE;")
			.arg(settings.value("database/name").toString(), fileName);

	if (!query.exec(queryString)) {
		qDebug() << query.lastError().text();
		return false;
	}

	return query.exec(QString("USE [%1]").arg(settings.value("database/name").toString()));
}

bool Database::createBackup(const QString &fileName,
		const QString &name,
		const QString &description) {
	QSqlDatabase db = QSqlDatabase::database();
	QSqlQuery q(db);
	QSettings settings;

	const QString dbName = settings.value("database/name").toString();
	const QString query = QString("BACKUP DATABASE [%1] "
							"TO DISK = '%2' "
							"WITH FORMAT, "
							"MEDIANAME = 'SQLServerBackup', "
							"MEDIADESCRIPTION = '%3', "
							"NAME = '%4' ").arg(dbName, fileName, description, name);

	if (!q.exec(query)) {
		return false;
	}
	return true;
}

/**
 * @brief Database::Database
 */
Database::Database()
{
    this->connect();
}

/**
 * @brief Database::~Database
 */
Database::~Database()
{
}

} // namespace core
