#include "network/regon/regon.hpp"
#include <QMessageBox>
#include <QRegularExpression>


namespace core::network {

Regon::Regon(QObject* parent)
	: QObject(parent)
	, mUserIsLogged(false)
	, mToken("")
{
}

Regon::Request::Request()
	: QNetworkRequest()
{
	setHeader(QNetworkRequest::ContentTypeHeader, "application/soap+xml");
	setUrl(QUrl(Regon::host()));
}

Regon::~Regon() {}

QByteArray Regon::host() {
	return QByteArray("https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc");
}

bool Regon::login(const QByteArray &userKey)
{
	QString data;
	QTextStream out(&data, QIODevice::WriteOnly);

	out << "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ns=\"http://CIS/BIR/PUBL/2014/07\">";

	/// make header
	out << "<soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">";
	out << "<wsa:To>" << host() << "</wsa:To>";
	out << "<wsa:Action>" << "http://CIS/BIR/PUBL/2014/07/IUslugaBIRzewnPubl/Zaloguj" << "</wsa:Action>";
	out << "</soap:Header>";

	/// make body
	out << "<soap:Body>";

	out << "<ns:Zaloguj>"
		<< "<ns:pKluczUzytkownika>" << userKey << "</ns:pKluczUzytkownika>"
		<<" </ns:Zaloguj>";

	out << "</soap:Body>";
	out << "</soap:Envelope>";

	// create request to send
	Request request;
	QNetworkAccessManager* nam = new QNetworkAccessManager(this);
	connect(nam, &QNetworkAccessManager::finished, this, &Regon::processLoginRequest);

	// send post request
	nam->post(request, data.toStdString().c_str());

	return true;
}

void Regon::processLoginRequest(QNetworkReply *reply) {
	if (reply->error() != QNetworkReply::NetworkError::NoError) {
		QMessageBox::critical(nullptr, tr("Błąd sieci"), reply->errorString());
		return;
	}

	const QByteArray data = reply->readAll();
	QByteArray token = "";
	QRegularExpression regexp("<ZalogujResult>([\\s\\S]+)</ZalogujResult>\\B");
	if (QRegularExpressionMatch match = regexp.match(data); match.hasMatch()) {
		token = match.captured(1).toStdString().c_str();
		mUserIsLogged = true;
		mToken = token;

		emit userIsLogged(token);
	}
}

void Regon::logout()
{
	if (mUserIsLogged == false && mToken.size() == 0) {
		return;
	}

	QString data;
	QTextStream out(&data, QIODevice::WriteOnly);

	out << "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ns=\"http://CIS/BIR/PUBL/2014/07\">";

	/// make header
	out << "<soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">";
	out << "<wsa:To>" << host() << "</wsa:To>";
	out << "<wsa:Action>" << "http://CIS/BIR/PUBL/2014/07/IUslugaBIRzewnPubl/Wyloguj" << "</wsa:Action>";
	out << "</soap:Header>";

	/// make body
	out << "<soap:Body>";

	out << "<ns:Wyloguj>"
		<< "<ns:pIdentyfikatorSesji>" << mToken << "</ns:pIdentyfikatorSesji>"
		<< "</ns:Wyloguj>";

	out << "</soap:Body>";
	out << "</soap:Envelope>";

	// send request to server
	Request request;
	QNetworkAccessManager* nam = new QNetworkAccessManager(this);
	connect(nam, &QNetworkAccessManager::finished, this, &Regon::processLogoutRequest);

	nam->post(request, data.toStdString().c_str());
}

void Regon::processLogoutRequest(QNetworkReply *reply) {
	if (reply->error() != QNetworkReply::NetworkError::NoError) {
		QMessageBox::critical(nullptr, tr("Błąd sieci"), reply->errorString());
		return;
	}

	const QByteArray data = reply->readAll();
	QRegularExpression regexp("<WylogujResult>([\\S\\s]+)</WylogujResult>\\B");
	if (QRegularExpressionMatch match = regexp.match(data); match.hasMatch()) {
		const QString result = match.captured(1);
		if (result.indexOf("true") != -1) {
			emit userIsLogout();
		}
	}
}

void Regon::searchEntities(const QString& nip) {
	if (mUserIsLogged == false && mToken.size() == 0) {
		qWarning() << "Nie jestes zalogowany, wiec nie moszesz szukac podmiotow w bazie REGON";
		return;
	}

	QString data;
	QTextStream out(&data, QIODevice::WriteOnly);

	out << "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ns=\"http://CIS/BIR/PUBL/2014/07\" xmlns:dat=\"http://CIS/BIR/PUBL/2014/07/DataContract\">";

	/// make header
	out << "<soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">";
	out << "<wsa:To>" << host() << "</wsa:To>";
	out << "<wsa:Action>" << "http://CIS/BIR/PUBL/2014/07/IUslugaBIRzewnPubl/DaneSzukajPodmioty" << "</wsa:Action>";
	out << "</soap:Header>";

	/// make body
	out << "<soap:Body>";

	out << "<ns:DaneSzukajPodmioty>"
		<< "<ns:pParametryWyszukiwania>"
		<< "<dat:Nip>" << nip << "</dat:Nip>"
		<< "</ns:pParametryWyszukiwania>"
		<< "</ns:DaneSzukajPodmioty>";

	out << "</soap:Body>";
	out << "</soap:Envelope>";


	// create request and send to server
	Request request;
	request.setRawHeader(QStringLiteral("sid").toStdString().c_str(), mToken.toStdString().c_str());

	QNetworkAccessManager* nam = new QNetworkAccessManager(this);
	connect(nam, &QNetworkAccessManager::finished, this, &Regon::processSearchRequest);

	nam->post(request, data.toStdString().c_str());
}

void Regon::processSearchRequest(QNetworkReply* reply) {
	if (reply->error() != QNetworkReply::NetworkError::NoError) {
		QMessageBox::critical(nullptr, tr("Błąd sieci"), reply->errorString());
		return;
	}

	const QByteArray data = reply->readAll();
	// replace bad characters
	QString response = QString::fromUtf8(data);
	response = response.replace(QRegularExpression("&lt;"), "<")
			.replace(QRegularExpression("&gt;"), ">")
			.remove(QRegularExpression("&#xD;"));

	QRegularExpression regexp("<DaneSzukajPodmiotyResult>([\\S\\s]+)</DaneSzukajPodmiotyResult>\\B");
	if (QRegularExpressionMatch match = regexp.match(response); match.hasMatch()) {
		const QString documentBody = match.captured(1);
		QDomDocument doc;
		QString errorMessage;
		if (!doc.setContent(documentBody, &errorMessage)) {
			qCritical() << "Nie mozna bylo utworzyc kontekstu dokumentu XML";
		}

		QDomElement rootElement = doc.firstChildElement("root");
		QDomElement first = rootElement.elementsByTagName("dane").at(0).toElement();

#ifdef DEBUG
		qInfo() << "Nazwa: " << first.firstChildElement("Nazwa").text() << '\n'
				<< "Regon: " << first.firstChildElement("Regon").text() << '\n'
				<< "Wojewodztwo: " << first.firstChildElement("Wojewodztwo").text() << '\n'
				<< "Miejscowosc: " << first.firstChildElement("Miejscowosc").text() << '\n'
				<< "Kod pocztowy: " << first.firstChildElement("KodPocztowy").text() << '\n'
				<< "Miejscowosc poczty: " << first.firstChildElement("MiejscowoscPoczty").text();
#endif

		emit finished(first);
		return;
	}
	QDomElement emptyElement;
	emit finished(emptyElement);
}

} // namespace core
