#include "licence.hpp"
#include <QFile>
#include <QDomDocument>
#include <QDomElement>
#include <QSettings>
#include <QDebug>

#include "simplecrypt.hpp"
#include <QMessageBox>


namespace core {

Licence::Licence()
    : mBuyDate(1996, 8, 6)		// moje urodziny ;)
    , mExpiryDate(1998, 7, 23)	// Oli urodziny
    , mOrderActive(false)
    , mServiceActive(false)
    , mArticleActive(false)
{
    QSettings settings;
    if (!load(settings.value("app/licence").toString()))
        return;
}

Licence::Licence(const QString& name) {
    if (!load(name))
        return;
}

Licence::~Licence() {}

bool Licence::load(const QString &name) {
	Q_UNUSED(name);
    bool active(true);

#ifndef DEBUG
    QFile file(name);
    if (!file.open(QFile::ReadOnly | QFile::Text))
        return false;

    core::SimpleCrypt crypto;
    crypto.setKey(0x0c2ad4a4acb9f023);

    QByteArray data = crypto.decryptToByteArray(file.readAll());
    if (crypto.lastError() != core::SimpleCrypt::ErrorNoError) {
        switch (crypto.lastError()) {
        case core::SimpleCrypt::ErrorUnknownVersion:
			QMessageBox::warning(nullptr, "Odczyt licencji", "Nie znana wersja pliku");
            break;

        case core::SimpleCrypt::ErrorIntegrityFailed:
			QMessageBox::warning(nullptr, "Odczyt licencji", "Błąd integralności klucza!");
            break;

        case core::SimpleCrypt::ErrorNoKeySet:
			QMessageBox::warning(nullptr, "Odczyt licencji", "Nie ustawiono klucza do szyfrowania/odszyfrowania licencji");
            break;

        case core::SimpleCrypt::ErrorNoError:
            qDebug() << "ErrorNoError";
            break;
        }
    }

    QDomDocument* doc = new QDomDocument;
    if (!doc->setContent(data)) {
        active = false;

		QMessageBox::critical(nullptr, "Licencja", "Plik licencji ma niewłaściwy format");
    }
    else file.close();

    QDomElement root = doc->firstChildElement("licence");
    if (!root.isElement()) {
        active = false;
		QMessageBox::critical(0, "Licencja",
                                        "Nie znaleziono głónego obiektu danych w pliku licencji");
    }

    setBuyDate(QDate::fromString(root.attribute("buyDate"), "dd/MM/yyyy"));
    setExpiryDate(QDate::fromString(root.attribute("expiryDate"), "dd/MM/yyyy"));

    if (QDate::currentDate() < buyDate()) {
        active = false;
		QMessageBox::critical(nullptr, "Licencja",
                                        "Data zakupu licencji jest nie zgodna z datą systemową");
    }

    bool infiniteLicence = root.hasAttribute("infinite") ? root.attribute("infinite").toUInt() >= 1 : false;

    // w przypadku licencji o nie skończonym okresie wsparcia
    // datą wygaśnięcia licencji jest data dzisiejsza plus 10 lat
    if (infiniteLicence) setExpiryDate(QDate::currentDate().addYears(10));

    if (!infiniteLicence) {
        if (QDate::currentDate() > expiryDate()) {
            active = false;
			QMessageBox::information(nullptr, "Licencja",
                                               "Zakończył się termin ważności licencji na oprogramowanie eSawmill. "
                                               "W celu dalszej możliwości korzystania z oprogramowania w sposób legalny "
                                               "prosimy o kontakt z dostawcą");
        }
    }

    QDomElement components = root.firstChildElement("components");
    if (components.hasAttributes()) {
        mOrderActive = components.hasAttribute("orders") ? components.attribute("orders").toUInt() >= 1 && active : false;
        mServiceActive = components.hasAttribute("services") ? components.attribute("services").toUInt() >= 1 && active : false;
        mArticleActive = components.hasAttribute("article") ? components.attribute("article").toUInt() >= 1 && active : false;
    }

    QDomElement customer = root.firstChildElement("customer");
    if (customer.hasChildNodes()) {
        parseNode(customer, &mCustomer, &mCustomerContact);
    }

    QDomElement dealer = root.firstChildElement("dealer");
    if (dealer.hasChildNodes()) {
        parseNode(dealer, &mDealer, &mDealerContact);
    }
#endif
    return active;
}

bool Licence::parseNode(QDomElement &element, Company *company, Contact *contact)
{
    const QString companyName = element.firstChildElement("CompanyName").text();
    const QString personName = element.firstChildElement("PersonName").text();
    const QString personSurname = element.firstChildElement("PersonSurname").text();
    const QString nip = element.firstChildElement("NIP").text();
    const QString regon = element.firstChildElement("REGON").text();
    const QString pesel = element.firstChildElement("PESEL").text();

//    company->setName(companyName);
//    company->setPersonName(personName);
//    company->setPersonSurname(personSurname);
//    company->setNIP(nip);
//    company->setREGON(regon);
    company->set("Name", companyName);
    company->set("PersonName", personName);
    company->set("PersonSurname", personSurname);
    company->set("NIP", nip);
    company->set("REGON", regon);

    const QDomElement address = element.firstChildElement("Address");
    const QString city = address.firstChildElement("City").text();
    const QString street = address.firstChildElement("Street").text();
    const QString homeNumber = address.firstChildElement("HomeNumber").text();
    const QString postCode = address.firstChildElement("PostCode").text();
    const QString postOffice = address.firstChildElement("PostOffice").text();
    const QString phone = address.firstChildElement("Phone").text();
    const QString email = address.firstChildElement("Email").text();

//    contact->setCity(city);
//    contact->setStreet(street);
//    contact->setHomeNumber(homeNumber);
//    contact->setZipCode(postCode);
//    contact->setEmail(email);
//    contact->setPhone(phone);
    contact->set("City", city);
    contact->set("Street", street);
    contact->set("HomeNumber", homeNumber);
    contact->set("ZipCode", postCode);
    contact->set("Email", email);
    contact->set("Phone", phone);

    return true;
}

} // namespace eSawmill
