#include "globaldata.hpp"
#include "repository/database/databaserepository.hpp"
#include "repository/remote/remoterepository.hpp"
#include "logs/filelogger.hpp"
#include "commandexecutor.hpp"
#include <QSettings>

namespace core {

GlobalData& GlobalData::instance() {
	static GlobalData globalData;
	return globalData;
}

CommandExecutor& GlobalData::commandExecutor() const {
	return CommandExecutor::instance();
}

repository::AbstractRepository& GlobalData::repository() const {
	QSettings settings;
	static const int provider = settings.value("DataProvider").toInt();
	if (provider == 1) {
		// user use remote repository provider
		static repository::remote::RemoteRepository remoteRepository {};
		return remoteRepository;
	}

	// default always return database provider
	static repository::database::DatabaseRepository databaseRepository {};
	return databaseRepository;
}

logs::AbstractLogger& GlobalData::logger() const {
	static core::logs::FileLogger fileLogger{};
	return fileLogger;
}

QVersionNumber GlobalData::currentApplicationVersion() const {
	return QVersionNumber {2, 1, 0};
}

}
