#include "models/stateslistmodel.hpp"

namespace core::models {

StatesListModel::StatesListModel(QObject* parent)
	: QStringListModel(parent)
{
	setStringList(QStringList()
				  << tr("Małopolskie")
				  << tr("Podkarpackie")
				  << tr("Świętokrzyskie")
				  << tr("Śląskie"));
}

StatesListModel::~StatesListModel() {}

} // namespace core
