#include "models/woodtypemodel.hpp"
#include "dbo/woodtype.hpp"
#include <globaldata.hpp>

namespace core::models {

WoodTypeModel::WoodTypeModel(QObject* parent)
	: core::models::AbstractModel<core::WoodType>{parent}
{
	get();
}

Qt::ItemFlags WoodTypeModel::flags(const QModelIndex &index) const {
	auto flags = core::models::AbstractModel<core::WoodType>::flags(index);
	flags |= Qt::ItemIsEnabled | Qt::ItemIsEditable;
	return flags;
}

QStringList WoodTypeModel::headerData() const {
	return QStringList()
		<< tr("Nazwa")
		<< tr("Mnożnik ceny")
		<< tr("Potrącenie na korę");
}

QVariant WoodTypeModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (orientation == Qt::Horizontal) {
		switch (role) {
		case Qt::DisplayRole:
			return headerData().at(section);
			break;
		}
	}
	else if (orientation == Qt::Vertical) {
		switch (role) {
		case Qt::DisplayRole:
			return section + 1;
			break;
		}
	}
	return QVariant();
}

QVariant WoodTypeModel::data(const QModelIndex &index, int role) const {
	int row = index.row(),
		col = index.column();

	core::WoodType current = mList[row];
	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Name: return current.name();
		case Factor: return current.factor();
		case WoodBark: return QString("%1 cm").arg(current.woodBark());
		}
		break;

	case Roles::Id:
		return mList.at(row).id();
		break;
	}

	return QVariant();
}

bool WoodTypeModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	int row = index.row(),
		col = index.column();

	core::WoodType& woodType = mList[row];
	switch (role) {
	case Qt::EditRole: {
		switch (col) {
		case Name:
			woodType.setName(value.toString());
			break;

		case Factor:
			woodType.setFactor(value.toDouble());
			break;

		case WoodBark:
			woodType.setWoodBark(value.toDouble());
			break;
		}
	} break;
	}
	core::GlobalData::instance().repository().woodTypeRepository().update(woodType);
	QAbstractItemModel::setData(index, value, role);
	return true;
}

void WoodTypeModel::createOrUpdate(const core::WoodType model) {
	Q_UNUSED(model);
}

void WoodTypeModel::destroy(const core::WoodType model) {
	Q_UNUSED(model);
}

void WoodTypeModel::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance().repository().woodTypeRepository().getAll();
		endResetModel();
	} catch (...) {
		throw; // rethrow all exceptions
	}
}

bool WoodTypeModel::insertRows(int row, int count, const QModelIndex &parent) {
	// always add new row to end of list, so I don't need it
	Q_UNUSED(row);
	Q_UNUSED(parent);

	if (count <= 0)
		return false;

	beginInsertRows(parent, mList.size(), mList.size() + 1);
	for (int i = 0; i < count; i++) {
		core::WoodType model;
		model.set("Name", "");
		model.set("Factor", 1);		// default options
		model.set("WoodBark", 3);	// default options
		core::GlobalData::instance().repository().woodTypeRepository().create(model);
	}
	// when finish added new rows (in database)
	// get all data from db to local list
	get();

	endInsertRows();
	return true;
}

void WoodTypeModel::destroy(eloquent::DBIDKey woodId) {
	Q_ASSERT(woodId > 0);
	try {
		core::GlobalData::instance().repository().woodTypeRepository().destroy(woodId);
	}
	catch(...) {
		throw;	// rethrow all exceptions
	}
}

} // namespace core
