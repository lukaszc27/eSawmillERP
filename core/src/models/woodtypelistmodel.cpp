#include "models/woodtypelistmodel.hpp"
#include <globaldata.hpp>

namespace core::models {

WoodTypeListModel::WoodTypeListModel(QObject* parent)
	: QAbstractListModel(parent)
{
	get();
}

Qt::ItemFlags WoodTypeListModel::flags(const QModelIndex &index) const
{
	auto flags = QAbstractListModel::flags(index);
	flags |= Qt::ItemIsEnabled;

	return flags;
}

int WoodTypeListModel::rowCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mList.size();
}

QVariant WoodTypeListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(section);

	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Horizontal)
			return QString("Rodzaje drewna");
		break;
	}
	return QVariant();
}

QVariant WoodTypeListModel::data(const QModelIndex &index, int role) const {
	int row = index.row();
	const core::WoodType& current = mList[row];
	switch (role) {
	case Qt::DisplayRole:
		return current.name();
		break;

	case Roles::Id: return mList.at(row).id();
	}

	return QVariant();
}

void WoodTypeListModel::get() {
	beginResetModel();
	mList = core::GlobalData::instance().repository().woodTypeRepository().getAll();
	endResetModel();
}

} // namespace core
