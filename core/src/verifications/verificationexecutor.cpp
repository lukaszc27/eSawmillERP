#include "verifications/verificationexecutor.hpp"
#include "verifications/verificationswidget.hpp"
#include <QList>
#include <QDebug>
#include <memory>


namespace core::verifications {

VerificationExecutor::VerificationExecutor()
	: mTestsPassed {false}
{
}

VerificationExecutor::~VerificationExecutor()
{
	if (mVerifications.size() > 0) {
		for (int i {0}; i < mVerifications.size(); ++i)
			delete mVerifications[i];
	}
}

void VerificationExecutor::addVerification(AbstractVerification* verification)
{
	if (verification != nullptr)
		mVerifications.append(verification);
}

bool VerificationExecutor::runTests(bool showSummary)
{
#if _DEBUG
	qDebug() << "Uruchomienie testów weryfikacyjnych";
	qDebug() << Q_FUNC_INFO;
#endif

	int testFailed {0};
	for (int i {0}; i < mVerifications.size(); ++i) {
		mVerifications.at(i)->execute();
		if (!mVerifications.at(i)->isValid())
			++testFailed;
	}
	mTestsPassed = testFailed == 0;

	if (!mTestsPassed && showSummary) {
		std::unique_ptr<core::verifications::VerificationsWidget> widget = std::make_unique<core::verifications::VerificationsWidget>();
		for (int i {0}; i < mVerifications.size(); ++i)
			widget->appendNotifications(mVerifications.at(i)->notifications());

		widget->exec();
	}

	return mTestsPassed;
}

} // namespace core::verifications
