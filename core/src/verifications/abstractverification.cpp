#include "verifications/abstractverification.hpp"

namespace core::verifications {

AbstractVerification::AbstractVerification()
	: mValid {false}
{
}

void AbstractVerification::addNotification(const QString& msg, const Notification::Type& type)
{
	Notification notification;
	notification.setDateTime(QDateTime::currentDateTime());
	notification.setMessage(msg);
	notification.setType(type);
	addNotification(notification);
}

} // namespace core::verifications
