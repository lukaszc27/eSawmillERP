#include "verifications/notification.hpp"

namespace core::verifications {

Notification::Notification()
{
}

void Notification::setMessage(const QString& msg, const Type& type)
{
	setMessage(msg);
	setType(type);
}

} // namespace core::verifications
