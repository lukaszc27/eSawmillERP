#include "verifications/notificationsmodel.hpp"
#include <QIcon>

namespace core::verifications {

verifications::NotificationsModel::NotificationsModel(QObject* parent)
	: QAbstractTableModel {parent}
{
}

Qt::ItemFlags NotificationsModel::flags(const QModelIndex& index) const
{
	return QAbstractTableModel::flags(index) | Qt::ItemIsEnabled;
}

int NotificationsModel::rowCount(const QModelIndex& index) const
{
	Q_UNUSED(index);
	return mNotifications.size();
}

int NotificationsModel::columnCount(const QModelIndex& index) const
{
	Q_UNUSED(index);
	return 3;
}

QVariant NotificationsModel::data(const QModelIndex& index, int role) const
{
	const int row = index.row();
	const int col = index.column();
	const Notification currentItem = mNotifications.at(row);

	switch(role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::DateTime: return currentItem.dateTime();
		case Columns::Type: {
			switch (currentItem.type()) {
			case Notification::Type::Information:	return tr("Informacja");
			case Notification::Type::Error:			return tr("Błąd");
			case Notification::Type::Warning:		return tr("Ostrzeżenie");
			}
		} break;
		case Columns::Message: return currentItem.message();
		}
		break;

	case Qt::DecorationRole:
		if (index.column() == Columns::Type) {
			switch (currentItem.type()) {
			case Notification::Type::Information:	return QIcon(":/icons/information");
			case Notification::Type::Error:			return QIcon(":/icons/cancel");
			case Notification::Type::Warning:		return QIcon(":/icons/warning");
			}
		}
		break;
	};
	return QVariant {};
}

QVariant NotificationsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	const QStringList headers = QStringList() << tr("Data") << tr("Rodzaj") << tr("Wiadomość");

	switch (role) {
	case Qt::DisplayRole:
		if (orientation == Qt::Vertical)
			return section + 1;

		return headers.at(section);
		break;
	}
	return QVariant {};
}

void NotificationsModel::appendNotifications(const QList<Notification>& notifications)
{
	beginResetModel();
	mNotifications.append(notifications);
	endResetModel();
}

} // namespace core::verifications
