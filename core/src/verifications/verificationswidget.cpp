#include "verifications/verificationswidget.hpp"
//#include "settings/appsettings.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>

namespace core::verifications {

VerificationsWidget::VerificationsWidget(QWidget* parent)
	: QDialog {parent}
{
	setWindowTitle(tr("Podsumowanie weryfikacji"));
	setMinimumSize(QSize(640, 320));

	createWidgets();	
	createModels();
	createConnections();
}

void VerificationsWidget::createWidgets()
{
	mTableView = new QTableView {this};
	mTableView->setCornerButtonEnabled(false);
	mTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
	mTableView->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
	mTableView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
	mTableView->setShowGrid(false);
	mCloseButton = new QPushButton {QIcon(":/icons/exit"), tr("Zamknij"), this};

	QHBoxLayout* buttonsLayout = new QHBoxLayout {};
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mCloseButton);

	QVBoxLayout* mainLayout = new QVBoxLayout {this};
	mainLayout->addWidget(mTableView);
	mainLayout->addSpacing(4);
	mainLayout->addLayout(buttonsLayout);
}

void VerificationsWidget::createModels()
{
	mNotificationsModel = new NotificationsModel {this};
	mTableView->setModel(mNotificationsModel);
}

void VerificationsWidget::createConnections()
{
	connect(mCloseButton, &QPushButton::clicked, this, &VerificationsWidget::close);
}

} // namespace core::verifications
