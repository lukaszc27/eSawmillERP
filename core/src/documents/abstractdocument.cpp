#include "documents/abstractdocument.h"
#include "dbo/measureunit_core.hpp"
#include "globaldata.hpp"
#include <QTextStream>
#include <QDate>
#include <QSettings>


namespace core::documents {

AbstractDocument::AbstractDocument(core::Company company,
								   core::Contractor contractor,
								   QObject *parent)
	: QTextDocument(parent)
	, mCompany(company)
	, mContractor(contractor)
{
	setDocumentMargin(10);
}

void AbstractDocument::setCompany(const core::Company company) {
	mCompany = company;
	emit companyChanged();
}

void AbstractDocument::setContractor(const core::Contractor contractor) {
	mContractor = contractor;
	emit contractorChanged();
}

bool AbstractDocument::generate() {
	QString html;
	QTextStream out(&html);

	out << "<style type=\"text/css\">"
		<< "h2 {"
		<< "margin-bottom: 4px;"
		<< "}"
		<< "h4 {"
		<< "margin-top: 4px;"
		<< "}"
		<< "table, tr, th, td {"
		<< "border-collapse: collapse;"
		<< "}"
		<< "table {"
		<< "border-width: 1px;"
		<< "width: 100%;"
		<< "}"
		<< "th, td {"
		<< "text-align:center;"
		<< "}"
		<< "th {"
		<< "background-color: #e6e6e6;"
		<< "font-size: 10pt;"
		<< "}"
		<< "</style>";

	QSettings settings;
	out << "<body>"
		   "<table width=\"100%\" border=\"0\">"
		   "<tr>"
		   "<td style=\"text-align:left;\">"
		   //"<img src=\"" << settings.value("Document/banner").toString()<< "\" alt=\"Banner\" width=\"40%\" />"
		<< mCompany << "</td>"
		<< "<td style=\"text-align:right;\">" << mCompany.contact().city() << " dn. " << QDate::currentDate().toString("dd/MM/yyyy")
		<< "</td>"
		   "</tr>";

	if (mContractor) {
		out << "<tr>"
			   "<td width=\"60%\"></td>"
			   "<td style=\"text-align:left;\"><strong>Nabywca:</strong><br/>" << mContractor << "</td>"
			   "</tr>";
	} else {
		out << "<tr></tr>";
	}
	out << "</table>";

	if (!mTitle.isEmpty()) out << "<h2 align=\"Center\">" << mTitle.toUpper() << "</h2>"; else return false;
	if (!mSubtitle.isEmpty()) out << "<h4 align=\"Center\">" << mSubtitle.toUpper() << "</h4>";

	// umieszcza treść dokumentu
	out << "<br/>" << body();

	out << "<p style=\"text-align: left; color: #8a8a8a; font-size: 8pt;\">"
		<< applicationVersion() << "</p>";
	out << "</body>";

	html.replace('\n', "<br/>");
	setHtml(html);

	return true;
}

QString AbstractDocument::articlesTable(QList<core::Article> articles, double *totalArticlesPrice) {
	QString html;
	QTextStream out(&html);

	out << "<table width=\"100%\">"
		<< "<thead>"
		<< "<tr>"
		<< "<th width=\"8%\">" << tr("Lp") << "</td>"
		<< "<th width=\"40%\">" << tr("Nazwa") << "</th>"
		<< "<th>" << tr("Ilość") << "</th>"
		<< "<th>" << tr("Jm") << "</th>"
		<< "<th>" << tr("Cena NETTO") << "</th>"
		<< "<th>" << tr("VAT") << "</th>"
		<< "<th>" << tr("Tax Wartość") << "</th>"
		<< "<th>" << tr("Cena BRUTTO") << "</th>"
		<< "<th>" << tr("Wartość BRUTTO") << "</th>"
		<< "</tr>"
		<< "</thead>"
		<< "<tbody>";

	int index = 0;
	for (auto article : articles) {
		double articlePrice = article.get("PriceSaleBrutto").toDouble() * article.get("Quantity").toDouble();
		double TaxValue = (articlePrice * article.saleTax().value()) / 100;

		TaxValue = QString::number(TaxValue, 'g', 3).toDouble();
		articlePrice = QString::number(articlePrice, 'g', 3).toDouble();
		*totalArticlesPrice += articlePrice;

		out << "<tr>"
			<< "<td>" << ++index << "</td>"
			<< "<td style=\"text-align:left;\">" << article.name() << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", article.quantity()) << "</td>"
			<< "<td>" << article.unit().shortName() << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", article.priceSaleNetto()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", article.saleTax().value()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", TaxValue) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", article.priceSaleBrutto()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", articlePrice) << "</td>"
			<< "</tr>";
	}

	/// wiersz podsumuwujący tabelę artykułów
	out << "<tr style=\"font-weight:bold;\">"
		<< "<td colspan=\"8\"style=\"text-align:right;\">" << tr("RAZEM") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", *totalArticlesPrice) << "</td>"
		<< "</tr>";

	out << "</tbody>"
		<< "</table>";

	return html;
}

QString AbstractDocument::elementsTable(double* price) {
	Q_UNUSED(price);
	return QString();
}

QString AbstractDocument::applicationVersion()
{
	// wyświetlenie nazwy programu z jakiego dokonano wydruku
	QString appName = tr("eSawmill ERP ");
#if defined (Q_OS_LINUX)
	appName.append(tr("dla Linux"));
#elif defined (Q_OS_WIN)
	appName.append(tr("dla Windows"));
#endif

	appName.append(tr(" ver. "));
	appName.append(core::GlobalData::instance().currentApplicationVersion().toString());
	return tr("Wydrukowano z programu %1").arg(appName);
}

} // namespace eSawmill
