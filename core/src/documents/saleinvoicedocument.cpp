#include "documents/saleinvoicedocument.hpp"
//#include "../dbo/saleinvoiceitem.hpp"


namespace core::documents {

SaleInvoiceDocument::SaleInvoiceDocument(QObject* parent)
	: AbstractInvoice<core::SaleInvoice, core::SaleInvoiceItem> {parent}
{
}

QString SaleInvoiceDocument::createTableOfItems(double *totalPrice)
{
	QString html;
	QTextStream out(&html);

	out << "<tr><td colspan='3'>";
	out << "<table width='1' border='1'>"
		<< "<tr>"
		<< "<th>" << tr("Lp") << "</th>"
		<< "<th width='45%'>" << tr("Nazwa") << "</th>"
		<< "<th>" << tr("Ilość") << "</th>"
		<< "<th>" << tr("jm") << "</th>"
		<< "<th>" << tr("Cena") << "</th>"
		<< "<th>" << tr("Wartość") << "</th>"
		<< "</tr>";

	int index {1};
	for (core::SaleInvoiceItem item : invoiceItems()) {
		double value = item.price() * item.quantity();
		out << "<tr>"
			<< "<td style='text-align:center;'>" << index++ << "</td>"
			<< "<td>" << item.name() << "</td>"
			<< "<td style='text-align:center;'>" << item.quantity() << "</td>"
			<< "<td style='text-align:center;'>" << item.measureUnit() << "</td>"
			<< "<td style='text-align:center;'>" << item.price() << "</td>"
			<< "<td style='text-align:center;'>" << value << "</td>";

		*totalPrice += value;
	}

	out << "</td></tr>";

	return html;
}

} // namespace core::documents
