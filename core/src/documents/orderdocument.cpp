#include "documents/orderdocument.h"
#include "dbo/orderarticle.h"
#include "settings/appsettings.hpp"
#include "globaldata.hpp"

namespace core::documents {

//OrderDocument::OrderDocument(core::Company company,
//							 core::Contractor contractor,
//							 core::Order order,
//							 QObject *parent)
//	: AbstractDocument(company, contractor, parent)
//	, mOrder(order)
//{
//	//	mElements = order.elements();
//	//	mArticles = order.articles();
//}

//OrderDocument::OrderDocument(core::Company company,
//							 core::Contractor contractor,
//							 core::Order order,
//							 QList<core::order::Element> elements,
//							 QList<core::Article> articles,
//							 QObject* parent)
//	: AbstractDocument(company, contractor, parent)
//	, mOrder(order)
//	, mElements(elements)
//	, mArticles(articles)
//{
//}
OrderDocument::OrderDocument(const core::Company& company,const core::Order& order, QObject* parent)
	: AbstractDocument {company, order.contractor(), parent}
	, mOrder {order}
{
}

QString OrderDocument::body()
{
	QString html;
	QTextStream out(&html);

	const core::settings::OrderSettings& settings = core::settings::AppSettings::order();
	double planedPrice = settings.defaultPlannedPrice();

	out << "<table width=\"100%\">"
		<< "<thead>"
		<< "<tr style=\"font-size:10px;\">"
		<< "<th>" << tr("Lp") << "</th>"
		<< "<th>" << tr("Szerokość") << "<br/>[cm]" << "</th>"
		<< "<th>" << tr("Wysokość") << "<br/>[cm]" << "</th>"
		<< "<th>" << tr("Długość") << "<br/>[m]" << "</th>"
		<< "<th>" << tr("Ilość") << "<br/>[szt]" << "</th>"
		<< "<th>" << tr("Kubatura") << "<br/>[m<sup>3</sup>]" << "</th>"
		<< "<th>" << tr("Cena") << "<br/>[PLN]" << "</th>"
		<< "<th>" << tr("Wartość") << "<br/>[PLN/m<sup>3</sup>]" << "</th>";

	if (mOrder.Tax().value() > 0) {
		out << "<th>" << tr("VAT") << "<br/>[%]" << "</th>"
			<< "<th>" << tr("VAT Wartość") << "<br/>[PLN]" << "</th>"
			<< "<th>" << tr("Cena") << "<br/>[PLN/m<sup>3</sup>]";
	}
	out << "</tr>"
		<< "</thead>"
		<< "<tbody>";

	const double orderPrice = mOrder.price();
	const double Tax = mOrder.Tax().value() / 100.f;
	double totalOrderPrice {0},
	totalOrderPriceWithTax {0},
	totalOrderStere {0},
	totalPlannedStere {0};	// całkowita ilość m3 drewna do strugania

	int index {0};
	for (const core::order::Element& element : mOrder.elements()) {
		const double factor = element.woodType().factor();
		double price = orderPrice * factor;

		double elementPrice = element.stere() * price;
		totalOrderPrice += elementPrice;	// całkowita cena za elementy bez uwzględnia Tax
		totalOrderStere += element.stere();	// sumuje ilość m3 zamówienia

		if (element.planned())
			totalPlannedStere += element.stere();

		out << "<tr>"
			<< "<td>" << ++index << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", element.width()) << "</th>"
			<< "<td>" << QString::asprintf("%0.2f", element.height()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", element.length()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", element.quantity()) << "</td>"
			<< "<td>" << QString::asprintf("%0.4f", element.stere()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", price) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", elementPrice) << "</td>";

		if (Tax > 0) {
			double TaxValue = elementPrice * Tax;
			totalOrderPriceWithTax += (elementPrice + TaxValue);

			out << "<td>" << QString::asprintf("%0.2f", Tax * 100) << "</td>"
				<< "<td>" << QString::asprintf("%0.2f", TaxValue) << "</td>"
				<< "<td>" << QString::asprintf("%0.2f", elementPrice + TaxValue) << "</td>";
		}
		out << "</tr>";
	}
	/// wyświetlenie wiersza podsumuwującego wszystkie elementy
	/// zaokrągalnie wartości do przystępnej wersji liczbowej
	//	totalOrderPrice = QString::number(totalOrderPrice, 'g', 3).toDouble();
	//	totalOrderPriceWithTax = QString::number(totalOrderPriceWithTax, 'g', 3).toDouble();

	out << "<tr style=\"font-weight:bold;\">"
		<< "<td colspan=\"5\" style=\"text-align:right;\">" << tr("RAZEM") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalOrderStere) << "</td>"
		<< "<td>" << "X" << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalOrderPrice) << "</td>";

	if (Tax > 0) {
		out << "<td>" << QString::asprintf("%0.2f", Tax * 100) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", totalOrderPrice * Tax) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", totalOrderPriceWithTax) << "</td>";
	}
	out << "</tr>";

	out << "</tbody>"
		<< "</table>";

	double articlesTotalPrice = 0;
	if (mOrder.articles().count() > 0) {
		out << "<h3 align=\"center\">" << tr("ARTYKUŁY") << "</h3>";

		auto& repository = core::GlobalData::instance().repository().articleRepository();
		QList<core::Article> articles;
		for (const auto& clip : mOrder.articles()) {
			auto article = repository.getArticle(clip.articleId());
			article.setQuantity(clip.quantity());
			articles.append(article);
		}
		out << articlesTable(articles, &articlesTotalPrice);
	}
	double totalPrice = totalOrderPriceWithTax > 0 ? totalOrderPriceWithTax : totalOrderPrice;

	/// tabelka podsumuwująca zamówienia
	out << "<h3 align=\"center\">" << tr("PODSUMOWANIE") << "</h3>";
	out << "<table width=\"100%\">"
		<< "<thead>"
		<< "<tr>"
		<< "<th width=\"50%\">" << tr("Nazwa") << "</th>"
		<< "<th>" << tr("Kubatura") << "<br/>[m<sup>3</sup>]" << "</th>"
		<< "<th>" << tr("Cena") << "<br/>[PLN/m<sup>3</sup>]" << "</th>"
		<< "<th>" << tr("Wartość") << "<br/>[PLN]" << "</th>"
		<< "</tr>"
		<< "</thead>"
		<< "<body>";

	out << "<tr>"
		<< "<td style=\"text-align:left;\">" << tr("Elementy konstrukcyjne") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalOrderStere) << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", mOrder.price()) << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalPrice) << "</td>"
		<< "</tr>";

	double totalPlannedPrice = totalPlannedStere * planedPrice;
	totalPrice += totalPlannedPrice;	// dodanie wartości drewna struganego
	out << "<tr>"
		<< "<td style=\"text-align:left;\">" << tr("Struganie") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalPlannedStere) << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", planedPrice) << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalPlannedPrice) << "</td>"
		<< "</tr>";

	totalPrice += articlesTotalPrice;	// do ceny całkowitej za zamówienie doliczenie kosztów artykułów
	out << "<tr>"
		<< "<td colspan=\"3\" style=\"text-align:left;\">" << tr("Artykuły") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", articlesTotalPrice) << "</td>"
		<< "</tr>";

	out << "<tr style=\"font-weight:bold;\">"
		<< "<td colspan=\"3\" style=\"text-align:right;\">" << tr("Razem do zapłaty") << "</td>"
		<< "<td>" << QString::asprintf("%0.2f", totalPrice) << "</td>"
		<< "</tr>";

	out << "</tbody>"
		<< "</table>";

	// jeśli dano rabat uwzględnienie go dla całości zamówienia
	double rebate = mOrder.rebate();
	rebate = (100 - rebate) / 100.f;
	if (mOrder.rebate() > 0) {
		out << "<h4>" << tr("Wartość: ") << QString::asprintf("%0.2f", totalPrice) << " PLN" << "</h4>"
			<< "<h4>" << tr("Rabat: ") << QString::asprintf("%0.2f", mOrder.rebate()) << " %" << "</h4>";
	}
	out << "<h3>" << tr("Do zapłaty: ") << QString::asprintf("%0.2f", totalPrice * rebate) << " PLN" << "</h3>";

	if (mOrder.description().length() > 0) {
		out << "<hr />"
			<< "<span>" << mOrder.description().toHtmlEscaped() << "</span>";
	}

	return html;
}

} // namespace eSawmill
