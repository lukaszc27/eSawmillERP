#include "documents/purchaseinvoicedocument.hpp"
#include <QTextStream>
//#include <dbo/purchaseinvoiceitem.hpp>


namespace core::documents {

PurchaseInvoiceDocument::PurchaseInvoiceDocument(QObject* parent)
	: AbstractInvoice<core::PurchaseInvoice, core::PurchaseInvoiceItem>{parent}
{
}

QString PurchaseInvoiceDocument::createTableOfItems(double* totalValue)
{
	QString html;
	QTextStream out(&html);

	out << "<tr>"
		<< "<table width='100%' border='1'>";

	out << "<tr>"
		   "<th>" << tr("Lp") << "</th>"
		   "<th>" << tr("Nazwa") << "</th>"
		   "<th>" << tr("Ilość") << "</th>"
		   "<th>" << tr("jm") << "</th>"
		   "<th>" << tr("Cena") << "</th>"
		   "<th>" << tr("Wartość") << "</th>"
		   "</tr>";

	int index {1};
	for (core::PurchaseInvoiceItem item : invoiceItems()) {
		const double value { item.quantity()*item.price() };
		*totalValue += value;
		out << "<tr>"
			   "<td style='text-align:center;'>" << index++ << "</td>"
			   "<td>" << item.name() << "</td>"
			   "<td style='text-align:center;'>" << item.quantity() << "</td>"
			   "<td style='text-align:center;'>" << item.measureUnit() << "</td>"
			   "<td style='text-align:center;'>" << item.price() << "</td>"
			   "<td style='text-align:center;'>" << value << "</td>"
			   "</tr>";
	}

	out << "</table>"
		<< "</tr>";

	return html;
}

}
