#include "documents/servicedocument.h"
#include "dbo/serviceelement.h"
#include "dbo/servicearticle.h"
#include <QSettings>
#include <QDebug>
#include <globaldata.hpp>


namespace core::documents {

ServiceDocument::ServiceDocument(core::Company company,
								 core::Contractor contractor,
								 core::Service service,
								 QObject *parent)
	: AbstractDocument(company, contractor, parent)
	, mService(service)
{
}

//ServiceDocument::ServiceDocument(core::Company company,
//								 core::Contractor contractor,
//								 core::Service service,
//								 QList<core::service::Element> elements,
//								 QList<core::Article> articles,
//								 QObject* parent)
//	: AbstractDocument(company, contractor, parent)
//	, mService(service)
//	, mElements(elements)
//	, mArticles(articles)
//{
//}

QString ServiceDocument::body()
{
	double totalElementsPrice {0}, totalArticlesPrice {0};
	QString html;
	QTextStream out(&html);

	if (mService.elements().size() > 0) {
		out << this->elementsTable(&totalElementsPrice);
	}
	if (mService.articles().size() > 0) {
		out << "<h3 align=\"center\">" << tr("ARTYKUŁY") << "</h3>";
		QList<core::Article> articles{};
		for (auto const& clip : mService.articles())
		{
			auto article = core::GlobalData::instance()
					.repository()
					.articleRepository()
					.getArticle(clip.articleId());

			article.setQuantity(clip.quantity());
			articles.append(std::move(article));
		}
		out << this->articlesTable(articles, &totalArticlesPrice);
	}
	{
		double totalPrice {totalElementsPrice + totalArticlesPrice};
		if (double rebate = mService.rebate(); rebate > 0) {
			rebate = (100.0f - rebate) / 100.0f;

			// to service was add rebate so we have to inform end user about it
			out << "<h4>" << tr("Wartość: ") << QString::asprintf("%0.2f ", totalPrice) << tr("PLN") << "</h4>"
				<< "<h3>" << tr("Rabat: ") << QString::asprintf("%0.2f ", mService.rebate()) << tr("%") << "</h3>";

			totalPrice *= rebate;
		}
		out << "<h3>" << tr("Do zapłaty: ") << QString::asprintf("%0.2f ", totalPrice) << tr("PLN") << "</h3>";
		if (QString description = mService.descripion(); !description.isEmpty()) {
			out << "<hr />"
				<< "<span>" << description.toHtmlEscaped() << "</span>";
		}
	}
	return html;
}

QString ServiceDocument::elementsTable(double* elementsPrice) {
	QSettings settings;
	double pricePerRotatedStere = settings.value("Service/rotatedPrice").toDouble();
	double pricePerStere = mService.price();

	QString html;
	QTextStream out {&html};

	out << "<table width=\"100%\">"
		<< "<thead>"
		<< "<tr>"
		<< "<th>Lp</th>"
		<< "<th>" << tr("Średnica") << "<br/>[cm]</th>"
		<< "<th>" << tr("Długość") << "<br/>[m]</th>"
		<< "<th>" << tr("Kubatura") << "<br/>[m<sup>3</sup>]</th>"
		<< "<th>" << tr("Cena") << "<br/>[PLN/m<sup>3</sup>]</th>"
		<< "<th>" << tr("Wartość") << "<br/>[PLN]</th>";

	if (mService.Tax().value() > 0) {
		out << "<th>" << tr("VAT") << "<br/>[%]</th>"
			<< "<th>" << tr("VAT wartość") << "<br/>[PLN]</th>"
			<< "<th>" << tr("Cena") << "<br/>[PLN]</th>";
	}

	out << "</tr>"
		<< "</thead>"
		<< "<tbody>";

	int index {1};
	double totalStere {0};
	double totalRotatedStere {0};
	double totalTaxPrice {0};		// total price if tax is more than 0

	for (const auto& el : mService.elements()) {
		double woodFactor = el.woodType().factor();
		out << "<tr>"
			<< "<td>" << index++ << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", el.diameter()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", el.length()) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", el.stere()) << "</td>";

		double price {0}, currentPrice {0};
		if (el.isRotated()) {
			price = pricePerRotatedStere * woodFactor;
			currentPrice = price * el.stere();
			totalRotatedStere += el.stere();
		} else {
			price = pricePerStere * woodFactor;
			currentPrice = price * el.stere();
			totalStere += el.stere();

		}
		*elementsPrice += currentPrice;

		out << "<td>" << QString::asprintf("%0.2f", price) << "</td>"
			<< "<td>" << QString::asprintf("%0.2f", currentPrice) << "</td>";

		const double tax = mService.Tax().value() / 100.0f;
		if (tax > 0) {
			out << "<td>" << QString::asprintf("%0.2f", tax * 100.f) << "</td>"
				<< "<td>" << QString::asprintf("%0.2f", tax * currentPrice) << "</td>";

			double fullPrice = currentPrice + (tax * currentPrice);
			totalTaxPrice += fullPrice;
			out << "<td>" << fullPrice << "</td>";
		}
		out << "</tr>";
	}
	// at end add summary row
	out << "<tr>"
		<< "<td colspan=\"3\" style=\"text-align:right;font-weight:bold;\">" << tr("RAZEM") <<"</td>"
		<< "<td style=\"font-weight:bold;\">" << QString::asprintf("%0.2f", totalStere + totalRotatedStere) << "</td>"
		<< "<td style=\"font-weight:bold;\">" << tr("X") << "</td>"
		<< "<td style=\"font-weight:bold;\">" << QString::asprintf("%0.2f", *elementsPrice) << "</td>";

	if (mService.Tax().value() > 0) {
		const double tax = mService.Tax().value() / 100.0f;
		out << "<td style=\"font-weight:bold;\">" << tax * 100.0f << "</td>"
			<< "<td style=\"font-weight:bold;\">" << QString::asprintf("%0.2f", *elementsPrice * tax) << "</td>"
			<< "<td style=\"font-weight:bold;\">" << QString::asprintf("%0.2f", totalTaxPrice) << "</td>";

		*elementsPrice = totalTaxPrice;
	}
	out << "</tr>"
		<< "</tbody>"
		<< "</table>";

	return html;
}

} // namespace eSawmill
