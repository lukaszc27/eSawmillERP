#include "commandexecutor.hpp"
#include "globaldata.hpp"
#include <QApplication>

namespace core {

CommandExecutor::CommandExecutor()
{
}

CommandExecutor& CommandExecutor::instance() {
	static CommandExecutor commandExecutor;
	return commandExecutor;
}

bool CommandExecutor::execute(std::unique_ptr<abstracts::AbstractCommand> command) {
	if (!command)
		return false;

	if (command->overrideCursor()) {
		QApplication::setOverrideCursor(Qt::WaitCursor);
	}

	bool success = command->execute(core::GlobalData::instance().repository());
	if (success)
	{
		notify(&Listener::commandExecuted);
		if (command->canUndo()) {
			_commands.push(std::move(command));
			notify(&Listener::stackChanged, canUndo());
		}
	}
	QApplication::restoreOverrideCursor();

	return success;
}

bool CommandExecutor::canUndo() {
	return _commands.size() > 0;
}

bool CommandExecutor::undo() {
	if (_commands.empty())
		return false;	// the stack is empty

	if (auto command = std::move(_commands.top()); command) {
		_commands.pop();
		notify(&Listener::stackChanged, canUndo());

		const bool success = command->undo(core::GlobalData::instance().repository());
		notify(&Listener::commandExecuted);

		return success;
	}
	return false;
}

} // namespace core
