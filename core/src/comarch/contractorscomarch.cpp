#include "comarch/contractorscomarch.hpp"
#include <QtSql>
#include <eloquent/builder.hpp>


namespace core::comarch {


//Contractor::Contractor(const core::eloquent::Model &model)
//	: core::eloquent::Model(model)
//{
//}
Contractor::Contractor()
	: core::eloquent::Model<Contractor>{}
{
}

//QString Contractor::tableName() const {
//	return "Kontrahenci";
//}

core::eloquent::Builder<Contractor> Contractor::builder() const {
	return core::eloquent::Builder<Contractor>(tableName(), QSqlDatabase::database("comarch"), "CDN");
}

bool Contractor::createRecord(const core::eloquent::Model<Contractor>& model)
{
	Model<Contractor> contractor {model};

	QSqlQuery q(builder().database());
	q.setForwardOnly(true);
	q.prepare("EXEC [CDN].[Med_DodajKontrahenta] "
			  ":optimaId, "
			  ":kod, "
			  ":ean, "
			  ":nazwa1, "
			  ":nazwa2, "
			  ":nazwa3, "
			  ":kraj, "
			  ":wojewodztwo, "
			  ":powiat, "
			  ":gmina, "
			  ":ulica, "
			  ":nrDomu, "
			  ":nrLokalu, "
			  ":miasto, "
			  ":kodPocztowy, "
			  ":poczta, "
			  ":adres2, "
			  ":nipKraj, "
			  ":nipE, "
			  ":nip, "
			  ":regon, "
			  ":pesel, "
			  ":telefon, "
			  ":opis, "
			  ":osobaOdbierajaca, "
			  ":opeId, "
			  ":TS_Mod");

	q.bindValue(":optimaId", 0);	// ponieważ dodajemy nowego kontrahenta
	q.bindValue(":kod", contractor.get("Knt_Kod").toString());
	q.bindValue(":ean", "");
	q.bindValue(":nazwa1", contractor.get("Knt_Nazwa1").toString());
	q.bindValue(":nazwa2", contractor.get("Knt_Nazwa2").toString());
	q.bindValue(":nazwa3", contractor.get("Knt_Nazwa3").toString());
	q.bindValue(":kraj", contractor.get("Knt_Kraj").toString());
	q.bindValue(":wojewodztwo", contractor.get("Knt_Knt_Wojewodztwo").toString());
	q.bindValue(":powiat", "");
	q.bindValue(":gmina", "");
	q.bindValue(":ulica", contractor.get("Knt_Ulica").toString());
	q.bindValue(":nrDomu", contractor.get("Knt_NrDomu").toString());
	q.bindValue(":nrLokalu", contractor.get("Knt_NrLokalu").toString());
	q.bindValue(":miasto", contractor.get("Knt_Miasto").toString());
	q.bindValue(":kodPocztowy", contractor.get("Knt_KodPocztowy").toString());
	q.bindValue(":poczta", contractor.get("Knt_Poczta").toString());
	q.bindValue(":adres2", "");
	q.bindValue(":nipKraj", contractor.get("Knt_NipKraj").toString());
	q.bindValue(":nipE", "");
	q.bindValue(":nip", contractor.get("Knt_Nip").toString());
	q.bindValue(":regon", contractor.get("Knt_Regon").toString());
	q.bindValue(":pesel", contractor.get("Knt_Pesel").toString());
	q.bindValue(":telefon", contractor.get("Knt_Telefon1").toString());
	q.bindValue(":opis", "");
	q.bindValue(":osobaOdbierajaca", "");
	q.bindValue(":opeId", 1);	// id użytkownika optimy dodającego kontrahenta (pierwszy z brzegu :D)
	q.bindValue(":TS_Mod", QDateTime::currentDateTime());

#ifdef DEBUG
	if (!q.exec()) {
		qCritical() << q.lastError().text();
		return false;
	}

	return true;
#else
	return q.exec();
#endif
}

core::Contact Contractor::contact() {
	core::Contact contact;
	contact.setCountry(country());
	contact.setIsoCountry(countryISO());
	contact.setCity(city());
	contact.setState(state());
	contact.setPostCode(postCode());
	contact.setPostOffice(postOffice());
	contact.setStreet(street());
	contact.setHomeNumber(homeNumber());
	contact.setLocalNumber(localNumber());
	contact.setPhone(phone());
	contact.setEmail(email());
	contact.setUrl(url());
	//model.set("Country", country());
	//model.set("IsoCountry", countryISO());
	//model.set("City", city());
	//model.set("State", state());
	//model.set("ZipCode", postCode());
	//model.set("PostOffice", postOffice());
	//model.set("Street", street());
	//model.set("HomeNumber", homeNumber());
	//model.set("LocalNumber", localNumber());
	//model.set("Phone", phone());
	//model.set("Email", email());
	//model.set("Url", url());
	return contact;
}

core::Contractor Contractor::contractor() {
	core::Contractor contractor;
	contractor.setName(name());
	contractor.setPersonName("");
	contractor.setPersonSurname("");
	contractor.setNIP(nip());
	contractor.setREGON(regon());
	contractor.setPESEL(pesel());
	contractor.setProvider(provider());
	//model.set("Name", name());
	//model.set("PersonName", "");
	//model.set("PersonSurname", "");
	//model.set("NIP", nip());
	//model.set("REGON", regon());
	//model.set("PESEL", pesel());
	//model.set("Provider", provider());
	return contractor;
}

} // namespace core

