#include "dbo/servicearticle.h"

namespace core {
namespace service {

Article::Article()
	: core::eloquent::Model<core::service::Article>{}
{
}

//core::Service Article::service() const {
//	return belongsTo<core::Service>("Services", "ServiceId");
//}

core::Article Article::article() const {
	return belongsTo<core::Article>("Articles", "ArticleId");
}

QStringList Article::fillable() const {
	return QStringList()
			<< "Id"
			<< "ServiceId"
			<< "ArticleId"
			<< "Quantity";
}

QDataStream& operator<<(QDataStream& stream, const core::service::Article& article) noexcept {
	stream << article.id() << article.serviceId() << article.articleId() << article.quantity();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::service::Article& article) noexcept {
	core::eloquent::DBIDKey id, serviceId, articleId;
	double qty;

	stream >> id >> serviceId >> articleId >> qty;

	article.setId(id);
	article.setServiceId(serviceId);
	article.setArticleId(articleId);
	article.setQuantity(qty);

	return stream;
}

} // namespace service
} // namespace core
