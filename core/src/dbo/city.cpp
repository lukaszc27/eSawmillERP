#include "dbo/city.hpp"

namespace core {

City::City()
	: core::eloquent::Model<core::City>{}
{
}

QStringList City::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name";
}

QDataStream& operator<<(QDataStream& stream, const core::City& city) noexcept {
	stream << city.id() << city.name();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::City& city) noexcept {
	core::eloquent::DBIDKey id;
	QString name;

	stream >> id >> name;

	city.setId(id);
	city.setName(name);

	return stream;
}

} // namespace core
