#include "dbo/service.h"
#include "dbo/serviceelement.h"
#include "dbo/servicearticle.h"
#include "eloquent/builder.hpp"
#include "globaldata.hpp"
#include <QVariant>
#include <QSettings>
#include <QDebug>
#include "repository/database/dbcontractorrepository.hpp"
#include "repository/database/dbtaxrepository.hpp"


namespace core {

Service::Service()
	: core::eloquent::Model<core::Service>{}
{
}

bool Service::createRecord(const Model<Service>& model)
{
	const core::Service lastService = findRecord(maxId());

	int number = 0;
	if (QString fullNumber = lastService.get("Number").toString(); !fullNumber.isEmpty()) {
		QStringList parts = fullNumber.split(' ');
		// parts[0] - prefix (UG)
		// parts[1] - number XX/MM/yyyy
		QStringList values = parts[1].trimmed().split('/');

		number = values[1].trimmed().toInt() == QDate::currentDate().month()
				 ? values[0].trimmed().toInt() : 0;
	}

	QSettings settings;
	const QDate& currentDate = QDate::currentDate();
	QString serviceNumber = QString::asprintf("%s %d/%.2d/%d",
			settings.value("Service/prefix", "UG").toString().toStdString().c_str(),
			++number,
			currentDate.month(),
			currentDate.year());

	auto& service = const_cast<Model<Service>&>(model);
	service.set("Number", serviceNumber);

	return Model::createRecord(service);
}

bool Service::destroyRecord(const Model<Service>& model) {
	core::service::Element element;
	core::service::Article article;

	return element.builder().where("ServiceId", model.id()).destroy() &&
			article.builder().where("ServiceId", model.id()).destroy() &&
			Model<Service>::destroyRecord(model);
}

void Service::setContractor(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	set("ContractorId", contractorId);
	_contractor = core::repository::database::ContractorRepository().get(contractorId);
}

void Service::setContractor(const Contractor& contractor) {
	set("ContractorId", contractor.id());
	_contractor = contractor;
}

void Service::setTax(eloquent::DBIDKey taxId) {
	Q_ASSERT(taxId > 0);
	set("VatId", taxId);
	_tax = core::repository::database::TaxRepository().get(taxId);
}

void Service::setTax(const core::Tax& tax) {
	set("VatId", tax.id());
	_tax = tax;
}

void Service::setArticles(const QList<Article>& articles) {
	_articles_clip.clear();
	for (const auto& article : articles) {
		core::service::Article art;
		art.setArticleId(article.id());
		art.setQuantity(article.quantity());
	}
}

QStringList Service::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Number"
		<< "AddDate"
		<< "EndDate"
		<< "IsRealised"
		<< "Rebate"
		<< "Priority"
		<< "Price"
		<< "Description"
		<< "VatId"
		<< "ContractorId"
		<< "UserAddId"
		<< "CompanyId";
}

QDataStream& operator<<(QDataStream& stream, const core::Service& service) noexcept {
	stream << service.id()
		   << service.name()
		   << service.number()
		   << service.addDate()
		   << service.endDate()
		   << service.isRealised()
		   << service.rebate()
		   << static_cast<int>(service.priority())
		   << service.price()
		   << service.descripion()
		   << service.Tax()
		   << service.contractor()
		   << service.elements()
		   << service.articles();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Service& service) noexcept {
	core::eloquent::DBIDKey id;
	QString name, number, description;
	QDate addDate, endDate;
	bool realised;
	double price, rebate;
	int priority;
	core::Contractor contractor;
	core::Tax tax;
	QList<core::service::Element> elements;
	QList<core::service::Article> articles;

	stream >> id
		>> name
		>> number
		>> addDate
		>> endDate
		>> realised
		>> rebate
		>> priority
		>> price
		>> description
		>> tax
		>> contractor
		>> elements
		>> articles;

	service.setId(id);
	service.setName(name);
	service.setNumber(number);
	service.setAddDate(addDate);
	service.setEndDate(endDate);
	service.setRealised(realised);
	service.setRebate(rebate);
	service.setPriority(static_cast<core::Service::Priority>(priority));
	service.setPrice(price);
	service.setDescription(description);
	service.setTax(tax);
	service.setContractor(contractor);
	service.setElements(elements);
	service.setArticles(articles);

	return stream;
}

} // namespace core
