#include "dbo/contact.h"
#include <QVariant>
#include <QDebug>


namespace core {

Contact::Contact()
	: core::eloquent::Model<core::Contact>{}
{
}

QStringList Contact::fillable() const {
	return QStringList()
		<< "Id"
		<< "Country"
		<< "IsoCountry"
		<< "State"
		<< "City"
		<< "ZipCode"
		<< "PostOffice"
		<< "Street"
		<< "HomeNumber"
		<< "LocalNumber"
		<< "Phone"
		<< "Email"
		<< "Url";
}

QDataStream& operator<<(QDataStream& stream, const core::Contact& contact) noexcept {
	stream << contact.id()
		<< contact.country()
		<< contact.IsoCountry()
		<< contact.state()
		<< contact.city()
		<< contact.postCode()
		<< contact.postOffice()
		<< contact.street()
		<< contact.homeNumber()
		<< contact.localNumber()
		<< contact.phone()
		<< contact.email()
		<< contact.url();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Contact& contact) noexcept {
	core::eloquent::DBIDKey id;
	QString country, isoCountry, state, city, postCode, postOffice, street, homeNumber, localNumber, phone, email, url;

	stream >> id
		>> country
		>> isoCountry
		>> state
		>> city
		>> postCode
		>> postOffice
		>> street
		>> homeNumber
		>> localNumber
		>> phone
		>> email
		>> url;

	contact.setId(id);
	contact.setCountry(country);
	contact.setIsoCountry(isoCountry);
	contact.setState(state);
	contact.setCity(city);
	contact.setPostCode(postCode);
	contact.setPostOffice(postOffice);
	contact.setStreet(street);
	contact.setHomeNumber(homeNumber);
	contact.setLocalNumber(localNumber);
	contact.setPhone(phone);
	contact.setEmail(email);
	contact.setUrl(url);

	return stream;
}

QTextStream& operator<<(QTextStream& out, const core::Contact& contact) noexcept {
	if (!contact.city().isEmpty())
		out << contact.city() << " ";

	if (!contact.street().isEmpty())
		out << contact.street() << " ";

	if (!contact.homeNumber().isEmpty())
		out << contact.homeNumber();

	if (!contact.localNumber().isEmpty())
		out << "/" << contact.localNumber();

	out << "\n";

	if (!contact.state().isEmpty())
		out << contact.state();

	if (!contact.country().isEmpty())
		out << ", " << contact.country();

	out << "\n";

	if (!contact.phone().isEmpty())	out << "Tel.:" << contact.phone() << "\n";
	if (!contact.email().isEmpty()) out << "Email: " << contact.email() << "\n";
	if (!contact.url().isEmpty())	out << contact.url() << "\n";

	return out;
}

} // namespace core
