#include "dbo/wareHouse.h"
#include "dbo/article.h"
#include "eloquent/builder.hpp"
#include <QVariant>

namespace core
{

Warehouse::Warehouse()
	: core::eloquent::Model<core::Warehouse>{}
{
}

bool Warehouse::destroyRecord(const Model<Warehouse>& model) {
	return core::Article().builder().where("WarehouseId", model.id()).destroy()
			&& Model<Warehouse>::destroyRecord(model);
}

QStringList Warehouse::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Shortcut"
		<< "Description";
}

QDataStream& operator<<(QDataStream& stream, const core::Warehouse& warehouse) noexcept {
	stream << warehouse.id() << warehouse.name() << warehouse.shortcut() << warehouse.description();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Warehouse& warehouse) noexcept {
	core::eloquent::DBIDKey id;
	QString name, shortcut, description;

	stream >> id >> name >> shortcut >> description;

	warehouse.setId(id);
	warehouse.setName(name);
	warehouse.setShortcut(shortcut);
	warehouse.setDescription(description);

	return stream;
}

} // namespace core
