#include "dbo/auth.h"
#include <QVariant>
#include <QtXml>
#include <QCache>
#include "eloquent/model.hpp"


namespace core
{
quint32 mUserId = 0;
bool mUserLogin = false;

bool Auth::attempt(const QString &userName, const QString &password)
{
	QSqlDatabase db = QSqlDatabase::database();
	if (db.isOpen()) {
		QSqlQuery q;
		q.prepare("SELECT TOP 1 Id FROM [Users] WHERE [Name]=? AND [Password]=?");
		q.bindValue(0, userName);
		q.bindValue(1, password);

		mUserId = 0;
		mUserLogin = false;

		if (q.exec() && q.first()) {
			mUserId = q.value(0).toUInt();
			mUserLogin = true;

			return true;
		}
	}
	return false;
}

void Auth::logout() {
	mUserId = 0;
	mUserLogin = false;
}

bool Auth::status() {
	return (mUserId != 0 && mUserLogin);
}

User Auth::user(bool forceUpdate) {
	static QCache<int, core::User> cache;

	if (!forceUpdate || cache.contains(mUserId)) {
		if (forceUpdate) cache.clear();

		core::User* user = new core::User;
		*user = user->findRecord(mUserId);
		cache.insert(mUserId, user);
	}

	return *cache[mUserId];
}

}; // namespace core
