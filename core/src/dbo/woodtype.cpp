#include "dbo/woodtype.hpp"

namespace core {

WoodType::WoodType()
	: core::eloquent::Model<core::WoodType>{}
{
}

QStringList WoodType::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Factor"
		<< "WoodBark";
}

QDataStream& operator<<(QDataStream& stream, const core::WoodType& woodType) noexcept {
	stream << woodType.id()
		   << woodType.name()
		   << woodType.factor()
		   << woodType.woodBark();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::WoodType& woodType) noexcept {
	core::eloquent::DBIDKey id;
	QString name;
	double factor, woodBark;

	stream >> id >> name >> factor >> woodBark;

	woodType.setId(id);
	woodType.setName(name);
	woodType.setFactor(factor);
	woodType.setWoodBark(woodBark);

	return stream;
}

} // namespace core
