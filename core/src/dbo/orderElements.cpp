#include "dbo/orderElements.h"
#include "eloquent/builder.hpp"
#include "dbo/woodtype.hpp"
#include "dbo/order.h"
#include <QtSql>
#include "globaldata.hpp"


namespace core::order {

Element::Element()
	: core::eloquent::Model<core::order::Element>{}
{
}

core::Order Element::order(bool forceUpdate) const {
	static QCache<core::eloquent::DBIDKey, core::Order> cache;
	core::eloquent::DBIDKey id = get("OrderId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		core::Order* order = new core::Order();
		*order = belongsTo<core::Order>("Orders", "OrderId");
		cache.insert(id, order);
	}
	return *cache[id];
}

double Element::stere(double sparedLength) const {
	Q_ASSERT(sparedLength >= 0);
	double width = this->width() / 100.f,
			height = this->height() / 100.f,
			length = this->length(),
			qty = this->quantity();

	// add additional length
	length += sparedLength;

	double stere = (width * height * length) * qty;
	return stere;
}

void Element::setWoodType(const WoodType& woodtype) {
	set("WoodTypeId", woodtype.id());
	_woodType = woodtype;
}

void Element::setWoodType(eloquent::DBIDKey woodTypeId) {
	Q_ASSERT(woodTypeId > 0);
	set("WoodTypeId", woodTypeId);
	_woodType = core::GlobalData::instance().repository().woodTypeRepository().get(woodTypeId);
}

QStringList Element::fillable() const {
	return QStringList()
		<< "Id"
		<< "OrderId"
		<< "Width"
		<< "Height"
		<< "Length"
		<< "Quantity"
		<< "Planed"
		<< "WoodTypeId"
		<< "Name";
}

QDataStream& operator<<(QDataStream& stream, const core::order::Element& element) noexcept {
	stream << element.id()
		   << element.name()
		   << element.orderId()
		   << element.width()
		   << element.height()
		   << element.length()
		   << element.quantity()
		   << element.planned()
		   << element.woodType();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::order::Element& element) noexcept {
	core::eloquent::DBIDKey id, orderId;
	QString name;
	double width, height, length, quantity;
	bool planned;
	core::WoodType woodType;

	stream >> id
		>> name
		>> orderId
		>> width
		>> height
		>> length
		>> quantity
		>> planned
		>> woodType;

	element.setId(id);
	element["OrderId"] = orderId;
	element.setName(name);
	element.setWidth(width);
	element.setHeight(height);
	element.setLength(length);
	element.setQuantity(quantity);
	element.setPlanned(planned);
	element.setWoodType(woodType);

	return stream;
}

} // namespace
