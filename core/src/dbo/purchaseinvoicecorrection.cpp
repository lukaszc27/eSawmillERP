#include "dbo/purchaseinvoicecorrection.hpp"
#include <QCache>

namespace core {

PurchaseInvoiceCorrection::PurchaseInvoiceCorrection()
	: core::eloquent::Model<core::PurchaseInvoiceCorrection>{}
{
}

PurchaseInvoice PurchaseInvoiceCorrection::invoice(bool forceUpdate) const
{
	static QCache<int, PurchaseInvoice> cache;
	int id = get("InvoiceId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		core::PurchaseInvoice* invoice = new core::PurchaseInvoice();
		*invoice = invoice->findRecord(id);
		cache.insert(id, invoice);
	}
	return *cache[id];
}

PurchaseInvoice PurchaseInvoiceCorrection::correctionInvoice(bool forceUpdate) const
{
	static QCache<int, PurchaseInvoice> cache;
	int id = get("CorrectionId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		PurchaseInvoice* invoice = new PurchaseInvoice();
		*invoice = invoice->findRecord(id);
		cache.insert(id, invoice);
	}
	return *cache[id];
}

QStringList PurchaseInvoiceCorrection::fillable() const {
	return QStringList()
		<< "Id"
		<< "InvoiceId"
		<< "CorrectionId"
		<< "CreatedDate";
}

} // namespace core
