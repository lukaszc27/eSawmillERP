#include "dbo/company.h"
#include "eloquent/builder.hpp"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QCache>
#include "dbo/user.h"


namespace core {

Company::Company()
	: core::eloquent::Model<core::Company>{}
{
}

bool Company::destroyRecord(const Model<Company>& model) {
	// destroy contact and user object form database before remove company
	return core::Contact().builder().where("Id", model.get("ContactId").toInt()).destroy()
			&& core::User().builder().where("CompanyId", model.id()).destroy()
			&& Model::destroyRecord(model);
}

void Company::setContact(const Contact& contact) {
	set("ContactId", contact.id());
	_contact = contact;
}

void Company::setContact(eloquent::DBIDKey contactId) {
	Q_ASSERT(contactId > 0);
	set("ContactId", contactId);
	_contact = belongsTo<core::Contact>("Contacts", "ContactId");
}

QTextStream& operator<<(QTextStream& out, const core::Company& company) noexcept {
	if (!company.name().isEmpty())
		out << company.name() << "\n";

	if (!company.personName().isEmpty() && !company.personSurname().isEmpty())
		out << company.personName() << " " << company.personSurname() << "\n";

	out << company.contact();
	if (!company.NIP().isEmpty()) out << "NIP: " << company.NIP() << "\n";

	return out;
}

QStringList Company::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "PersonName"
		<< "PersonSurname"
		<< "NIP"
		<< "REGON"
		<< "KRS"
		<< "ContactId";
}

QDataStream& operator<<(QDataStream& stream, const core::Company& company) noexcept {
	stream << company.id()
		   << company.name()
		   << company.personName()
		   << company.personSurname()
		   << company.NIP()
		   << company.REGON()
		   << company.KRS()
		   << company.contact();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Company& company) noexcept {
	core::eloquent::DBIDKey id;
	QString name, personName, personSurname, nip, regon, krs;
	core::Contact contact;

	stream >> id >> name >> personName >> personSurname >> nip >> regon >> krs >> contact;

	company.setId(id);
	company.setName(name);
	company.setPersonName(personName);
	company.setPersonSurname(personSurname);
	company.setNIP(nip);
	company.setREGON(regon);
	company.setKRS(krs);
	company.setContact(contact);

	return stream;
}


} // namespace core
