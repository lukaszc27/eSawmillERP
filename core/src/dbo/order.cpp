#include "dbo/order.h"
#include "dbo/orderarticle.h"
#include "eloquent/builder.hpp"
#include "dbo/ordersaleinvoices.hpp"
#include "repository/database/dbcontractorrepository.hpp"
#include "repository/database/dbtaxrepository.hpp"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QSettings>
#include <QDebug>
#include <QHash>

namespace core {

Order::Order()
	: core::eloquent::Model<core::Order>{}
{
}

bool Order::createRecord(const Model<core::Order>& model)
{
	if (!model.get("Number").toString().isEmpty()) {
		// the numer is set
		return Model<Order>::createRecord(model);
	}
	// pobranie ostatnio dodanego zamówienia
	// w celu odczytania ostatnio nadanego numeru
	const core::Order lastOrder = findRecord(maxId());

	int number {0};
	if (QString fullNumber = lastOrder.number(); !fullNumber.isEmpty()) {
		QStringList parts = fullNumber.split(' ');
		// parts[0] => prefix
		// parts[1] => data
		QStringList values = parts[1].trimmed().split('/');
		number = values[1].trimmed().toInt() == QDate::currentDate().month() ? values[0].trimmed().toInt() : 0;
	}

	QSettings settings;
	const QDate currentDate = QDate::currentDate();
	QString orderNumber = QString("%1 %2/%3/%4")
			.arg(settings.value("Orders/prefix", "ZM").toString())
			.arg(++number)
			.arg(currentDate.month())
			.arg(currentDate.year());

	// ustawienie numeru zamówienia przed wpisaniem do bazy
	auto& order = const_cast<Model<Order>&>(model);
	order.set("Number", orderNumber);
	return Model<Order>::createRecord(order);
}

bool Order::destroyRecord(const Model<Order>& model) {
	// before destroyed order
	// remove all elements and articles assign to the order
	core::order::Element element;
	core::order::Article article;

	return element.builder().where("OrderId", model.id()).destroy()
			&& article.builder().where("OrderId", model.id()).destroy()
			&& Model<Order>::destroyRecord(model);
}

void Order::setContractor(const Contractor& contractor) {
	set("ContractorId", contractor.id());
	_contractor = contractor;
}

void Order::setContractor(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	set("ContractorId", contractorId);
	_contractor = core::repository::database::ContractorRepository().get(contractorId);
}

void Order::setTax(const core::Tax& Tax) {
	set("VatId", Tax.id());
	_tax = Tax;
}

void Order::setTax(eloquent::DBIDKey taxId) {
	Q_ASSERT(taxId > 0);
	set("VatId", taxId);
	_tax = core::Tax().findRecord(taxId);
}

void Order::setArticles(const QList<Article>& articles) {
	_articles_clip.clear();
	for (const auto& article : articles) {
		core::order::Article clip{};
		clip.setArticleId(article.id());
		clip.setQuantity(article.quantity());
		_articles_clip.append(clip);
	}
}

bool Order::hasInvoices(bool forceUpdate) const {
	static QHash<eloquent::DBIDKey, bool> cache;
	if (forceUpdate || !cache.contains(id())) {
		core::OrderSaleInvoices invoices;
		cache.insert(id(), invoices.builder().where("OrderId", id()).get().size() > 0);
	}
	return cache[id()];
}

SaleInvoice Order::invoice(bool forceUpdate) const {
	core::OrderSaleInvoices orderSaleInvoices;
	orderSaleInvoices = orderSaleInvoices.builder().where("OrderId", id()).orderBy("CreatedDate").first();
	return orderSaleInvoices.invoice(forceUpdate);
}

QStringList Order::fillable() const {
	return QStringList()
		<< "Id"
		<< "Number"
		<< "Name"
		<< "AddDate"
		<< "EndDate"
		<< "IsRealised"
		<< "Rebate"
		<< "Priority"
		<< "Price"
		<< "Description"
		<< "SpareLength"
		<< "ContractorId"
		<< "UserAddId"
		<< "CompanyId"
		<< "VatId";
}

QDataStream& operator<<(QDataStream& stream, const core::Order& order) noexcept {
	stream << order.id()
		   << order.number()
		   << order.name()
		   << order.addDate()
		   << order.endDate()
		   << order.isRealised()
		   << order.rebate()
		   << order.priority()
		   << order.price()
		   << order.description()
		   << order.spareLength()
		   << order.contractor()
		   << order.Tax()
		   << order.userAddId()
		   << order.companyId()
		   << order.elements()
		   << order.articles();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Order& order) noexcept {
	core::eloquent::DBIDKey id, userAddId, companyId;
	QString name, description, number;
	double price, spareLength, rebate;
	int priority;
	bool isRealised;
	QDate addDate, endDate;
	core::Contractor contractor;
	core::Tax tax;
	QList<core::order::Element> elements;
	QList<core::order::Article> articles;

	stream >> id
		>> number
		>> name
		>> addDate
		>> endDate
		>> isRealised
		>> rebate
		>> priority
		>> price
		>> description
		>> spareLength
		>> contractor
		>> tax
		>> userAddId
		>> companyId
		>> elements
		>> articles;

	order.setId(id);
	order.setName(name);
	order.setNumber(number);
	order.setAddDate(addDate);
	order.setEndDate(endDate);
	order.setRealised(isRealised);
	order.setRebate(rebate);
	order.setPriority(priority);
	order.setPrice(price);
	order.setDescription(description);
	order.setSpareLength(spareLength);
	order.setContractor(contractor);
	order.setTax(tax);
	order.setUser(userAddId);
	order.setCompany(companyId);
	order.setElements(elements);
	order.setArticles(articles);

	return stream;
}

} // namespace core
