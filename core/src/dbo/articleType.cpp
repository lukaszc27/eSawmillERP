#include "dbo/articleType.h"
#include <QVariant>
#include <QDataStream>

namespace core
{

ArticleType::ArticleType()
	: core::eloquent::Model<ArticleType>{}
{
}

QStringList ArticleType::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name";
}

QDataStream& operator<<(QDataStream& stream, const core::ArticleType& type) noexcept {
	stream << type.id() << type.name();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::ArticleType& type) noexcept {
	core::eloquent::DBIDKey typeId;
	QString name;

	stream >> typeId >> name;

	type.setId(typeId);
	type.setName(name);

	return stream;
}

} // namespace core
