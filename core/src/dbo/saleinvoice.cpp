#include "dbo/saleinvoice.hpp"
#include "eloquent/builder.hpp"
#include "dbo/saleinvoicecorrections.hpp"
#include "dbo/ordersaleinvoices.hpp"
#include <QMap>

namespace core {

SaleInvoice::SaleInvoice()
	: core::eloquent::Model<core::SaleInvoice>{}
{
}

bool SaleInvoice::createRecord(const Model<SaleInvoice>& model) {
	Model<core::SaleInvoice>& invoice = const_cast<Model<core::SaleInvoice>&>(model);
	if (invoice.get("Number").toString().isEmpty()) {
		const core::SaleInvoice lastInvoice = findRecord(maxId());
		int number {0};
		if (QString fullNumber = lastInvoice.number(); !fullNumber.isEmpty()) {
			QStringList parts = fullNumber.split(' ');
			// part[0] => prefix
			// part[1] => date
			QStringList data = parts[1].trimmed().split('/');
			number = data[1].trimmed().toInt() == QDate::currentDate().month() ? data[0].trimmed().toInt() : 0;
		}

		QDate date = QDate::currentDate();
//		QString nextNumber = QString("%1 %2/%3/%4")
//			.arg(prefix())
//			.arg(++number)
//			.arg(date.month())
//			.arg(date.year());
		QString nextNumber = QString::asprintf("%s %d/%2d/%d",
			prefix().toStdString().data(), ++number, date.month(), date.year());

		invoice.set("Number", nextNumber);
	}

	return Model::createRecord(invoice);
}

void SaleInvoice::setWarehouse(const Warehouse& warehouse) {
	set("WarehouseId", warehouse.id());
	_warehouse = warehouse;
}

void SaleInvoice::setWarehouse(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);
	set("WarehouseId", warehouseId);
	_warehouse = core::Warehouse().findRecord(warehouseId);
}

void SaleInvoice::setContractor(const Contractor& contractor) {
	set("ContractorId", contractor.id());
	_contractor = contractor;
}

void SaleInvoice::setContractor(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	set("ContractorId", contractorId);
	_contractor = core::Contractor().findRecord(contractorId);
}

QStringList SaleInvoice::fillable() const {
	return QStringList()
		<< "Id"
		<< "Number"
		<< "ContractorId"
		<< "WarehouseId"
		<< "DateOfIssue"
		<< "DateOfSale"
		<< "Rebate"
		<< "Payment"
		<< "PaymentDeadlineDays"
		<< "PaymentDeadlineDate"
		<< "NettoPrice"
		<< "TotalPrice"
		<< "PaidPrice"
		<< "ToPaidPrice"
		<< "Buffer"
		<< "WasPaid";
}

QDataStream& operator<<(QDataStream& stream, const core::SaleInvoice& invoice) noexcept {
	stream << invoice.id()
		   << invoice.number()
		   << invoice.contractor()
		   << invoice.warehouse()
		   << invoice.dateOfIssue()
		   << invoice.dateOfSale()
		   << invoice.rebate()
		   << invoice.paymentDeadlineDays()
		   << invoice.paymentDeadlineDate()
		   << invoice.nettoPrice()
		   << invoice.totalPrice()
		   << invoice.paidPrice()
		   << invoice.toPaidPrice()
		   << invoice.inBuffer()
		   << invoice.wasPaid();

	return stream;
}
QDataStream& operator>>(QDataStream& stream, core::SaleInvoice& invoice) noexcept {
	core::eloquent::DBIDKey invoiceId {0};
	QString number;
	core::Contractor contractor;
	core::Warehouse warehouse;
	QDate dateOfIssue;
	QDate dateOfSale;
	double rebate;
	int paymentDeadlineDays;
	QDate paymentDeadlineDate;
	double nettoPrice;
	double totalPrice;
	double toPaidPrice;
	double paidPrice;
	bool inBuffer;
	bool wasPaid;

	stream >> invoiceId
		>> number
		>> contractor
		>> warehouse
		>> dateOfIssue
		>> dateOfSale
		>> rebate
		>> paymentDeadlineDays
		>> paymentDeadlineDate
		>> nettoPrice
		>> totalPrice
		>> paidPrice
		>> toPaidPrice
		>> inBuffer
		>> wasPaid;

	invoice.setId(invoiceId);
	invoice.setNumber(number);
	invoice.setContractor(contractor);
	invoice.setWarehouse(warehouse);
	invoice.setDateOfIssue(dateOfIssue);
	invoice.setDateOfSale(dateOfSale);
	invoice.setRebate(rebate);
	invoice.setPaymentDeadlineDays(paymentDeadlineDays);
	invoice.setPaymentDeadlineDate(paymentDeadlineDate);
	invoice.setNettoPrice(nettoPrice);
	invoice.setTotalPrice(totalPrice);
	invoice.setToPaidPrice(toPaidPrice);
	invoice.setPaidPrice(paidPrice);
	invoice.existInBuffer(inBuffer);
	invoice.setPaid(wasPaid);

	return stream;
}

//////////////////////////////////////////////////////////////
namespace adapter {

OrderSaleInvoice::OrderSaleInvoice(const Order& order) noexcept {
	setContractor(order.contractor());
	setDateOfSale(QDate::currentDate());
	setDateOfIssue(QDate::currentDate());
	setRebate(order.rebate());
	setPrefix(QObject::tr("FA"));
}

} // namespace adapter
//////////////////////////////////////////////////////////////
} // namespace core
