#include "dbo/tax.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDomNodeList>
#include <QDomNode>
#include <QDebug>


namespace core
{

Tax::Tax()
	: core::eloquent::Model<core::Tax>{}
{
}

QStringList Tax::fillable() const {
	return QStringList()
		<< "Id"
		<< "Value";
}

QDataStream& operator<<(QDataStream& stream, const core::Tax& tax) noexcept {
	stream << tax.id() << tax.value();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Tax& tax) noexcept {
	core::eloquent::DBIDKey id;
	double value;

	stream >> id >> value;

	tax.setId(id);
	tax.setValue(value);

	return stream;
}

} // namespace core
