#include "dbo/article.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDebug>
#include <QDataStream>
#include "eloquent/builder.hpp"
#include "dbo/orderarticle.h"
#include "dbo/servicearticle.h"

namespace core {

Article::Article()
	: core::eloquent::Model<Article>{}
{
}

bool Article::destroyRecord(const Model<Article>& model) {
	// remove articles form services and orders when user remve master article
	core::order::Article orderArticles;
	core::service::Article serviceArticles;

	return orderArticles.builder().where("ArticleId", model.id()).destroy()
			&& serviceArticles.builder().where("ArticleId", model.id()).destroy()
			&& Model<Article>::destroyRecord(model);
}

void Article::setArticleType(const ArticleType& type) {
	set("ArticleTypeId", type.id());
	_articleType = type;
}

void Article::setArticleType(eloquent::DBIDKey typeId) {
	Q_ASSERT(typeId > 0);
	set("ArticleTypeId", typeId);
	_articleType = belongsTo<core::ArticleType>("ArticleTypes", "ArticleTypeId");
}

void Article::setProvider(const Contractor& provider) {
	set("ProviderId", provider.id());
	_provider = provider;
}

void Article::setProvider(eloquent::DBIDKey providerId) {
	Q_ASSERT(providerId > 0);
	set("ProviderId", providerId);
	_provider = belongsTo<core::Contractor>("Contractors", "ProviderId");
}

void Article::setWarehouse(const Warehouse& warehouse) {
	set("WarehouseId", warehouse.id());
	_warehouse = warehouse;
}

void Article::setWarehouse(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);
	set("WarehouseId", warehouseId);
	_warehouse = belongsTo<core::Warehouse>("Warehouses", "WarehouseId");
}

void Article::setSaleTax(const core::Tax& Tax) {
	set("SaleVatId", Tax.id());
	_saleTax = Tax;
}

void Article::setSaleTax(eloquent::DBIDKey saleTaxId) {
	Q_ASSERT(saleTaxId > 0);
	set("SaleVatId", saleTaxId);
	_saleTax = belongsTo<core::Tax>("VATs", "SaleVatId");
}

void Article::setPurchaseTax(const core::Tax& Tax) {
	set("PurchaseVatId", Tax.id());
	_purchaseTax = Tax;
}

void Article::setPurchaseTax(eloquent::DBIDKey purchaseTaxId) {
	Q_ASSERT(purchaseTaxId > 0);
	set("PurchaseVatId", purchaseTaxId);
	_purchaseTax = belongsTo<core::Tax>("VATs", "PurchaseVatId");
}

void Article::setCompany(const Company& company) {
	set("CompanyId", company.id());
	_company = company;
}

void Article::setCompany(eloquent::DBIDKey companyId) {
	Q_ASSERT(companyId);
	set("CompanyId", companyId);
	_company = belongsTo<core::Company>("Companies", "CompanyId");
}

void Article::setUser(const User& user) {
	set("UserAddId", user.id());
	_user = user;
}

void Article::setUser(eloquent::DBIDKey userId) {
	Q_ASSERT(userId > 0);
	set("UserAddId", userId);
	_user = belongsTo<core::User>("Users", "UserAddId");
}

void Article::setUnit(const MeasureUnit& unit) {
	set("UnitId", unit.id());
	_measureUnit = unit;
}

void Article::setUnit(eloquent::DBIDKey unitId) {
	Q_ASSERT(unitId > 0);
	set("UnitId", unitId);
	_measureUnit = belongsTo<core::MeasureUnit>("UnitMeasure", "UnitId");
}

QStringList Article::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "BarCode"
		<< "ArticleTypeId"
		<< "ProviderId"
		<< "WarehouseId"
		<< "PurchaseVatId"
		<< "SaleVatId"
		<< "PricePurchaseNetto"
		<< "PricePurchaseBrutto"
		<< "PriceSaleNetto"
		<< "PriceSaleBrutto"
		<< "Quantity"
		<< "CompanyId"
		<< "UserAddId"
		<< "UnitId";
}

QDataStream& operator<<(QDataStream& stream, const core::Article& article) noexcept {
	stream << article.id()
		   << article.name()
		   << article.barCode()
		   << article.articleType()
		   << article.provider()
		   << article.warehouse()
		   << article.purchaseTax()
		   << article.saleTax()
		   << article.pricePurchaseNetto()
		   << article.pricePurchaseBrutto()
		   << article.priceSaleBrutto()
		   << article.priceSaleNetto()
		   << article.quantity()
		   << article.unit()
		   << article.company()	// to remove from model and DB
		   << article.user();	// to remove from model and DB

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Article& article) noexcept {
	core::eloquent::DBIDKey articleId {0};
	QString name, barCode;
	core::ArticleType articleType;
	core::Contractor provider;
	core::Warehouse warehouse;
	core::Tax purchaseTax, saleTax;
	double pricePurchaseBrutto, pricePurchaseNetto;
	double priceSaleBrutto, priceSaleNetto;
	double quantity;
	core::MeasureUnit unit;
	core::User user;
	core::Company company;

	stream >> articleId
		>> name
		>> barCode
		>> articleType
		>> provider
		>> warehouse
		>> purchaseTax
		>> saleTax
		>> pricePurchaseNetto
		>> pricePurchaseBrutto
		>> priceSaleBrutto
		>> priceSaleNetto
		>> quantity
		>> unit
		>> company
		>> user;

	article.setId(articleId);
	article.setName(name);
	article.setBarCode(barCode);
	article.setArticleType(articleType);
	article.setProvider(provider);
	article.setWarehouse(warehouse);
	article.setPurchaseTax(purchaseTax);
	article.setPricePurchaseBrutto(pricePurchaseBrutto);
	article.setPricePurchaseNetto(pricePurchaseNetto);
	article.setSaleTax(saleTax);
	article.setPriceSaleNetto(priceSaleNetto);
	article.setPriceSaleBrutto(priceSaleBrutto);
	article.setQuantity(quantity);
	article.setUnit(unit);
	article.setCompany(company);
	article.setUser(user);

	return stream;
}

} // namespace core
