#include "dbo/purchaseinvoice.hpp"
#include <QDebug>
#include <QCache>
#include <QMap>
#include "dbo/purchaseinvoicecorrection.hpp"
#include "eloquent/builder.hpp"
#include "globaldata.hpp"


namespace core {

PurchaseInvoice::PurchaseInvoice()
	: core::eloquent::Model<core::PurchaseInvoice>{}
{
}

bool PurchaseInvoice::createRecord(const Model<PurchaseInvoice>& model) {
	Model<core::PurchaseInvoice>& invoice = const_cast<Model<PurchaseInvoice>&>(model);

	// get last inserted document number
	// and generate number for next document
	if (invoice.get("Number").toString().isEmpty()) {
		const core::PurchaseInvoice lastInvoice = findRecord(maxId());
		int number {0};
		if (QString fullNumber = lastInvoice.number(); !fullNumber.isEmpty()) {
			QStringList parts = fullNumber.split(' ');
			// part[0] => prefix
			// part[1] => date
			QStringList data = parts[1].trimmed().split('/');
			number = data[1].trimmed().toInt() == QDate::currentDate().month() ? data[0].trimmed().toInt() : 0;
		}

		QDate date = QDate::currentDate();
		QString nextNumber = QString("%1 %2/%3/%4")
			.arg(prefix())
			.arg(++number)
			.arg(date.month())
			.arg(date.year());

		invoice.set("Number", nextNumber);
	}

	return Model::createRecord(invoice);
}

void PurchaseInvoice::setContractor(const Contractor& contractor) {
	set("ContractorId", contractor.id());
	_contractor = contractor;
}

void PurchaseInvoice::setContractor(eloquent::DBIDKey contractorId) {
	Q_ASSERT(contractorId > 0);
	set("ContractorId", contractorId);
	_contractor = core::GlobalData::instance().repository().contractorRepository().get(contractorId);
}

void PurchaseInvoice::setWarehouse(const Warehouse& warehouse) {
	set("WarehouseId", warehouse.id());
	_warehouse = warehouse;
}

void PurchaseInvoice::setWarehouse(eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);
	set("WarehouseId", warehouseId);
	_warehouse = core::GlobalData::instance().repository().warehousesRepository().getWarehouse(warehouseId);
}

QStringList PurchaseInvoice::fillable() const {
	return QStringList()
		<< "Id"
		<< "Number"
		<< "ForeignNumber"
		<< "DateOfReceipt"
		<< "ContractorId"
		<< "WarehouseId"
		<< "DateOfIssue"
		<< "DateOfPurchase"
		<< "Rebate"
		<< "Payment"
		<< "PaymentDeadlineDays"
		<< "PaymentDeadlineDate"
		<< "NettoPrice"
		<< "TotalPrice"
		<< "PaidPrice"
		<< "ToPaidPrice"
		<< "Buffer"
		<< "WasPaid";
}

QDataStream& operator<<(QDataStream& stream, const core::PurchaseInvoice& invoice) noexcept {
	stream << invoice.id()
		   << invoice.number()
		   << invoice.foreignNumber()
		   << invoice.dateOfReceipt()
		   << invoice.contractor()
		   << invoice.dateOfIssue()
		   << invoice.dateOfPurchase()
		   << invoice.rebate()
		   << invoice.payment()
		   << invoice.paymentDeadlineDays()
		   << invoice.paymentDeadlineDate()
		   << invoice.nettoPrice()
		   << invoice.totalPrice()
		   << invoice.paidPrice()
		   << invoice.toPaidPrice()
		   << invoice.inBuffer()
		   << invoice.wasPaid();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::PurchaseInvoice& invoice) noexcept {
	core::eloquent::DBIDKey invoiceId {0};
	QString number;
	QString foreignNumber;
	QDate dateOfReceipt;
	core::Contractor contractor;
	QDate dateOfIssue;
	QDate dateOfPurchase;
	double rebate;
	QString payment;
	int paymentDeadlineDays;
	QDate paymentDeadlineDate;
	double nettoPrice;
	double totalPrice;
	double paidPrice;
	double toPaidPrice;
	bool inBuffer;
	bool wasPaid;

	stream >> invoiceId
		>> number
		>> foreignNumber
		>> dateOfReceipt
		>> contractor
		>> dateOfIssue
		>> dateOfPurchase
		>> rebate
		>> payment
		>> paymentDeadlineDays
		>> paymentDeadlineDate
		>> nettoPrice
		>> totalPrice
		>> paidPrice
		>> toPaidPrice
		>> inBuffer
		>> wasPaid;

	invoice.setId(invoiceId);
	invoice.setNumber(number);
	invoice.setForeignNumber(foreignNumber);
	invoice.setDateOfReceipt(dateOfReceipt);
	invoice.setContractor(contractor);
	invoice.setDateOfIssue(dateOfIssue);
	invoice.setDateOfPurchase(dateOfPurchase);
	invoice.setRebate(rebate);
	invoice.setPayment(payment);
	invoice.setPaymentDeadlineDays(paymentDeadlineDays);
	invoice.setPaymentDeadlineDate(paymentDeadlineDate);
	invoice.setNettoPrice(nettoPrice);
	invoice.setTotalPrice(totalPrice);
	invoice.setPaidPrice(paidPrice);
	invoice.setToPaidPrice(toPaidPrice);
	invoice.existInBuffer(inBuffer);
	invoice.setPaid(wasPaid);

	return stream;
}

//Contractor PurchaseInvoice::contractor(bool forceUpdate) const
//{
//	static QCache<int, core::Contractor> cache;
//	int id = get("ContractorId").toInt();

//	if (forceUpdate || !cache.contains(id)) {
//		if (forceUpdate)
//			cache.clear();

//		core::Contractor* con = new core::Contractor();
//		*con = core::Contractor().findRecord(id);
//		cache.insert(id, con);
//	}

//	return *cache[id];
//}

//Warehouse PurchaseInvoice::warehouse(bool forceUpdate) const
//{
//	static QCache<int, core::Warehouse> cache;
//	int id = get("WarehouseId").toInt();

//	if (forceUpdate || !cache.contains(id)) {
//		if (forceUpdate && !cache.isEmpty())
//			cache.clear();

//		core::Warehouse* warehouse = new core::Warehouse();
//		*warehouse = core::eloquent::Model::find<core::Warehouse>(id);
//		cache.insert(id, warehouse);
//	}

//	return *cache[id];
//}

//bool PurchaseInvoice::post()
//{
//	set("Buffer", false);
//	return save();
//}

//bool PurchaseInvoice::hasCorrection(bool forceUpdate) const
//{
//	static QMap<int, bool> cache;
//	if (forceUpdate || !cache.contains(id())) {
//		core::PurchaseInvoiceCorrection correction;
//		bool exist = correction.builder().where("InvoiceId", id()).orderBy("CreatedDate").get().size() > 0;
//		cache.insert(id(), exist);
//	}
//	return cache[id()];
//}

//eloquent::Model PurchaseInvoice::correctionDocument(bool forceUpdate) const
//{
//	PurchaseInvoiceCorrection correction;
//	correction = correction.builder().where("InvoiceId", id()).orderBy("CreatedDate").first();
//	return correction.correctionInvoice(forceUpdate);
//}

} // namespace core
