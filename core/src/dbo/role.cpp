#include "dbo/role.hpp"

namespace core {

Role::Role()
	: core::eloquent::Model<Role>{}
{
}

QStringList Role::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name";
}

QDataStream& operator<<(QDataStream& stream, const core::Role& role) noexcept {
	stream << role.id() << role.name();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Role& role) noexcept {
	core::eloquent::DBIDKey id;
	QString name;

	stream >> id >> name;

	role.setId(id);
	role.setName(name);

	return stream;
}


////////////////////////////////////////////////////////////

UserRole::UserRole()
	: core::eloquent::Model<UserRole>{}
{
}

UserRole::UserRole(const std::pair<eloquent::DBIDKey, eloquent::DBIDKey>& item) {
	setUser(item.first);
	setRole(item.second);
}

UserRole::UserRole(const eloquent::DBIDKey& userId, const eloquent::DBIDKey& roleId) {
	setUser(userId);
	setRole(roleId);
}

QStringList UserRole::fillable() const {
	return QStringList()
		<< "Id"
		<< "UserId"
		<< "RoleId";
}

QDataStream& operator<<(QDataStream& stream, const core::UserRole& userRole) noexcept {
	stream << userRole.id() << userRole.userId() << userRole.roleId();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::UserRole& userRole) noexcept {
	core::eloquent::DBIDKey id, userId, roleId;

	stream >> id >> userId >> roleId;

	userRole.setId(id);
	userRole.setUser(userId);
	userRole.setRole(roleId);

	return stream;
}

} // namespace core
