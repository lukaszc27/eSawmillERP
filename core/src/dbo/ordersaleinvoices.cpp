#include "dbo/ordersaleinvoices.hpp"
#include <QCache>


namespace core {

OrderSaleInvoices::OrderSaleInvoices()
	: core::eloquent::Model<core::OrderSaleInvoices>{}
{
}

OrderSaleInvoices::OrderSaleInvoices(const Order& order, const SaleInvoice& invoice)
	: OrderSaleInvoices {}
{
	setOrder(order);
	setInvoice(invoice);
}

Order OrderSaleInvoices::order(bool forceUpdate) const
{
	static QCache<eloquent::DBIDKey, Order> cache;
	eloquent::DBIDKey id = get("OrderId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		Order* order = new Order {};
		*order = belongsTo<core::Order>("Orders", "OrderId");
		cache.insert(id, order);
	}
	return *cache[id];
}

SaleInvoice OrderSaleInvoices::invoice(bool forceUpdate) const
{
	static QCache<eloquent::DBIDKey, SaleInvoice> cache;
	eloquent::DBIDKey id = get("InvoiceId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		SaleInvoice* invoice = new SaleInvoice {};
		*invoice = belongsTo<core::SaleInvoice>("SaleInvoices", "InvoiceId");
		invoice->setPrefix(invoice->number().split(' ').at(0));
		cache.insert(id, invoice);
	}
	return *cache[id];
}

QStringList OrderSaleInvoices::fillable() const {
	return QStringList()
		<< "Id"
		<< "OrderId"
		<< "InvoiceId"
		<< "CreatedDate";
}

} // namespace core
