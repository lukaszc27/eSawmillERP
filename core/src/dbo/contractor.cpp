#include "dbo/contractor.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QStringBuilder>
#include <QCache>

#include "eloquent/builder.hpp"
#include "dbo/order.h"
#include "dbo/service.h"
#include "dbo/article.h"


namespace core
{

Contractor::Contractor()
	: core::eloquent::Model<Contractor>{}
{
}

bool Contractor::destroyRecord(const Model<Contractor>& model) {
	core::Order orders;
	core::Service services;
	core::Article articles;

	return orders.builder().where("ContractorId", model.id()).destroy()
			&& services.builder().where("ContractorId", model.id()).destroy()
			&& articles.builder().where("ProviderId", model.id()).destroy()
			&& Model<Contractor>::destroyRecord(model);
}

void Contractor::setContact(const Contact& contact) {
	set("ContactId", contact.id());
	_contact = contact;
}

void Contractor::setContact(eloquent::DBIDKey contactId) {
	set("ContactId", contactId);
	_contact = belongsTo<core::Contact>("Contacts", "ContactId");
}

Contractor Contractor::fromCompany(const Company &company) {
	Contractor contractor;
	contractor.setName(company.name());
	contractor.setPersonName(company.personName());
	contractor.setPersonSurname(company.personSurname());
	contractor.setNIP(company.NIP());
	contractor.setREGON(company.REGON());
	contractor.setContact(company.contact());

	return contractor;
}

QString Contractor::getDisplayName() const noexcept {
	if (name().isEmpty()) {
		return QString("%1 %2")
			.arg(personName())
			.arg(personSurname());
	}
	return name();
}

QStringList Contractor::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "NIP"
		<< "REGON"
		<< "PESEL"
		<< "Provider"
		<< "PersonName"
		<< "PersonSurname"
		<< "ParentId"
		<< "Blocked"
		<< "ComarchId"
		<< "ContactId";
}

QDataStream& operator<<(QDataStream& stream, const core::Contractor& contractor) noexcept {
	stream << contractor.id()
		   << contractor.name()
		   << contractor.NIP()
		   << contractor.REGON()
		   << contractor.PESEL()
		   << contractor.provider()
		   << contractor.personName()
		   << contractor.personSurname()
		   << contractor.parentId()
		   << contractor.blocked()
		   << contractor.comarchId()
		   << contractor.contact();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::Contractor& contractor) noexcept {
	core::eloquent::DBIDKey id, parentId, comarchId;
	QString name, nip, regon, pesel, personName, personSurname;
	bool provider, blocked;
	core::Contact contact;

	stream >> id
		>> name
		>> nip
		>> regon
		>> pesel
		>> provider
		>> personName
		>> personSurname
		>> parentId
		>> blocked
		>> comarchId
		>> contact;

	contractor.setId(id);
	contractor.setName(name);
	contractor.setREGON(regon);
	contractor.setPESEL(pesel);
	contractor.setNIP(nip);
	contractor.setProvider(provider);
	contractor.setPersonName(personName);
	contractor.setPersonSurname(personSurname);
	contractor.setParentId(parentId);
	contractor.setBlocked(blocked);
	contractor.setComarchId(comarchId);
	contractor.setContact(contact);

	return stream;
}

QTextStream& operator<<(QTextStream& out, const core::Contractor& contractor) noexcept{
	if (!contractor.name().isEmpty())
		out << contractor.name() << "\n";

	if (!contractor.personName().isEmpty() && !contractor.personSurname().isEmpty())
		out << contractor.personName() << " " << contractor.personSurname() << "\n";

	out << contractor.contact();

	if (!contractor.NIP().isEmpty())	out << "NIP: " << contractor.NIP() << "\n";
	if (!contractor.PESEL().isEmpty())	out << "PESEL: " << contractor.PESEL() << "\n";
	if (!contractor.REGON().isEmpty())	out << "REGON: " << contractor.REGON() << "\n";

	return out;
}

} // namepsace core
