#include "dbo/measureunit_core.hpp"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDataStream>


namespace core {

MeasureUnit::MeasureUnit()
	: core::eloquent::Model<core::MeasureUnit>{}
{
}

QStringList MeasureUnit::fillable() const {
	return QStringList()
		<< "Id"
		<< "ShortName"
		<< "FullName";
}

QDataStream& operator<<(QDataStream& stream, const core::MeasureUnit& unit) noexcept {
	stream << unit.id() << unit.shortName() << unit.fullName();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::MeasureUnit& unit) noexcept {
	core::eloquent::DBIDKey id;
	QString shortName, fullName;

	stream >> id >> shortName >> fullName;

	unit.setId(id);
	unit.setShortName(shortName);
	unit.setFullName(fullName);

	return stream;
}

} // namespace core
