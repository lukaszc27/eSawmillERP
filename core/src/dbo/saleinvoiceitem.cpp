#include "dbo/saleinvoiceitem.hpp"
#include <QCache>


namespace core {

SaleInvoiceItem::SaleInvoiceItem()
	: core::eloquent::Model<core::SaleInvoiceItem>{}
{
}

QStringList SaleInvoiceItem::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Quantity"
		<< "MeasureUnit"
		<< "Price"
		<< "InvoiceId"
		<< "Rebate"
		<< "Value"
		<< "Warehouse"
		<< "ArticleId";
}

QDataStream& operator<<(QDataStream& stream, const core::SaleInvoiceItem& item) noexcept {
	stream << item.id()
		   << item.name()
		   << item.quantity()
		   << item.measureUnit()
		   << item.invoiceId()
		   << item.price()
		   << item.rebate()
		   << item.value()
		   << item.warehouse()
		   << item.articleId();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::SaleInvoiceItem& item) noexcept {
	core::eloquent::DBIDKey itemId {0};
	core::eloquent::DBIDKey invoiceId {0};
	core::eloquent::DBIDKey articleId {0};
	QString warehouse;
	QString unit;
	QString name;
	double qty;
	double price;
	double rebate;
	double value;

	stream >> itemId
		>> name
		>> qty
		>> unit
		>> invoiceId
		>> price
		>> rebate
		>> value
		>> warehouse
		>> articleId;

	item.setId(itemId);
	item.setQuantity(qty);
	item.setMeasureUnit(unit);
	item.setInvoice(invoiceId);
	item.setPrice(price);
	item.setRebate(rebate);
	item.setValue(value);
	item.setWarehouse(warehouse);
	item.setArticle(articleId);

	return stream;
}

/////////////////////////////////////////////////////////////
namespace adapter {

OrderElementSaleInvoiceItem::OrderElementSaleInvoiceItem(const order::Element& element, double additionalLength) noexcept {
	QString name = QString("%1 %2X%3X%4 - %5")
		.arg(element.name())
		.arg(element.width())
		.arg(element.height() + additionalLength)
		.arg(element.length())
		.arg(element.woodType().name());

	if (element.planned())
		name += QObject::tr(" (STRUGANY)");

	setName(name);
	setQuantity(element.quantity());
	setMeasureUnit(QObject::tr("SZT"));
	setRebate(0);
}

SaleInvoiceItemFromArticle::SaleInvoiceItemFromArticle(const Article& article) noexcept {
	double value = article.priceSaleBrutto() * article.quantity();

	setName(article.name());
	setQuantity(article.quantity());
	setMeasureUnit(article.unit().shortName());
	setPrice(article.priceSaleBrutto());
	setRebate(0);
	setValue(value);
}

} // namespace adapter
/////////////////////////////////////////////////////////////
} // namespace core
