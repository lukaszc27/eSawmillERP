#include "dbo/purchaseinvoiceitem.hpp"
#include <QCache>


namespace core {

PurchaseInvoiceItem::PurchaseInvoiceItem()
	: core::eloquent::Model<core::PurchaseInvoiceItem>{}
{
}

core::Article PurchaseInvoiceItem::orginalArticle(bool forceUpdate) const {
	static QCache<int, core::Article> cache;
	int id = get("ArticleId").toUInt();

	if (forceUpdate || !cache.contains(id)) {
		if (forceUpdate) cache.clear();

		core::Article* article = new core::Article;
		*article = belongsTo<core::Article>("Articles", "ArticleId");
		cache.insert(id, article);
	}

	return *cache[id];
}

QStringList PurchaseInvoiceItem::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Quantity"
		<< "MeasureUnit"
		<< "Rebate"
		<< "Price"
		<< "Value"
		<< "Warehouse"
		<< "InvoiceId"
		<< "ArticleId";
}

QDataStream& operator<<(QDataStream& stream, const core::PurchaseInvoiceItem& item) noexcept {
	stream << item.id()
		   << item.name()
		   << item.quantity()
		   << item.measureUnit()
		   << item.rebate()
		   << item.price()
		   << item.value()
		   << item.warehouse()
		   << item.invoiceId()
		   << item.articleId();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::PurchaseInvoiceItem& item) noexcept {
	core::eloquent::DBIDKey itemId {0};
	core::eloquent::DBIDKey invoiceId {0};
	core::eloquent::DBIDKey articleId {0};
	QString name;
	QString warehouse;
	QString unit;
	double rebate;
	double price;
	double qty;
	double value;

	stream >> itemId
		>> name
		>> qty
		>> unit
		>> rebate
		>> price
		>> value
		>> warehouse
		>> invoiceId
		>> articleId;

	item.setId(itemId);
	item.setName(name);
	item.setQuantity(qty);
	item.setMeasureUnit(unit);
	item.setRebate(rebate);
	item.setPrice(price);
	item.setValue(value);
	item.setWarehouse(warehouse);
	item.setInvoice(invoiceId);
	item.setOrginalArticle(articleId);

	return stream;
}

}
