#include "dbo/user.h"
#include "dbo/article.h"

#include <QCryptographicHash>
#include <QVariant>
#include <QDebug>
#include <QCache>
#include <QDataStream>

namespace core {

User::User()
	: core::eloquent::Model<core::User>{}
{
}

void User::setContact(const Contact& contact) {
	set("ContactId", contact.id());
	_contact = contact;
}

void User::setContact(eloquent::DBIDKey contactId) {
	Q_ASSERT(contactId > 0);
	set("ContactId", contactId);
	_contact = belongsTo<core::Contact>("Contacts", "ContactId");
}

void User::setCompany(const Company& company) {
	set("CompanyId", company.id());
	_company = company;
}

void User::setCompany(eloquent::DBIDKey companyId) {
	Q_ASSERT(companyId > 0);
	set("CompanyId", companyId);
	_company = belongsTo<core::Company>("Companies", "CompanyId");
}

void User::assignRole(const RoleCollection& roles) {
	_roles = roles;
}

void User::assignRole(const Role& role) {
	auto itr = std::find(_roles.begin(), _roles.end(), role);
	if (itr != _roles.end())
		return;	// the role exist for this user

	_roles.append(role);
}

bool User::hasRole(const eloquent::DBIDKey& roleId) const {
	Q_ASSERT(roleId > 0);
	return std::any_of(_roles.begin(), _roles.end(), [roleId](const core::Role& role)->bool {
		return role.id() == roleId;
	});
}

bool User::hasRole(Role::SystemRole systemRole) const {
	return hasRole(static_cast<core::eloquent::DBIDKey>(systemRole));
}

bool User::hasRole(const Role& role) const {
	return hasRole(role.id());
}

QStringList User::fillable() const {
	return QStringList()
		<< "Id"
		<< "Name"
		<< "Password"
		<< "Token"
		<< "FirstName"
		<< "SurName"
		<< "ContactId"
		<< "CompanyId";
}

QDataStream& operator<<(QDataStream& stream, const core::User& user) noexcept {
	stream << user.id()
		   << user.name()
		   << user.password()
		   << user.token()
		   << user.firstName()
		   << user.surName()
		   << user.contact()
		   << user.company()
		   << user.roles();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::User& user) noexcept {
	core::eloquent::DBIDKey id;
	QString name, password, token, firstName, surName;
	core::Contact contact;
	core::Company company;
	core::RoleCollection roles;

	stream >> id
		>> name
		>> password
		>> token
		>> firstName
		>> surName
		>> contact
		>> company
		>> roles;

	user.setId(id);
	user.setName(name);
	user.setPassword(password);
	user.setToken(token);
	user.setFirstName(firstName);
	user.setSurName(surName);
	user.setContact(contact);
	user.setCompany(company);
	user.assignRole(roles);

	return stream;
}

} // namespace core
