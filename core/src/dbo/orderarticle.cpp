#include "dbo/orderarticle.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>


namespace core {
namespace order {

Article::Article()
	: core::eloquent::Model<core::order::Article>{}
{
}

core::Article Article::article() const {
	return belongsTo<core::Article>("Articles", "ArticleId");
}

QStringList Article::fillable() const {
	return QStringList()
		<< "Id"
		<< "OrderId"
		<< "ArticleId"
		<< "Quantity";
}

QDataStream& operator<<(QDataStream& stream, const core::order::Article& article) noexcept {
	stream << article.id()
		   << article.orderId()
		   << article.articleId()
		   << article.quantity();
	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::order::Article& article) noexcept {
	core::eloquent::DBIDKey id, orderId, articleId;
	double qty;

	stream >> id
		>> orderId
		>> articleId
		>> qty;

	article.setId(id);
	article.setOrderId(orderId);
	article.setArticleId(articleId);
	article.setQuantity(qty);

	return stream;
}

} // namespace order
} // namespace core
