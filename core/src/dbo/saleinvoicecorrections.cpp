#include "dbo/saleinvoicecorrections.hpp"
#include <QCache>


namespace core {

SaleInvocieCorrections::SaleInvocieCorrections()
	: core::eloquent::Model<core::SaleInvocieCorrections>{}
{
}

SaleInvoice SaleInvocieCorrections::invoice(bool forceUpdate) const
{
	static QCache<int, SaleInvoice> cache;
	int id = get("InvoiceId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		SaleInvoice* invoice = new SaleInvoice();
		*invoice = invoice->findRecord(id);
		cache.insert(id, invoice);
	}
	return *cache[id];
}

SaleInvoice SaleInvocieCorrections::correctionInvoice(bool forceUpdate) const
{
	static QCache<int, SaleInvoice> cache;
	int id = get("CorrectionId").toInt();

	if (forceUpdate || !cache.contains(id)) {
		SaleInvoice* invoice = new SaleInvoice();
		*invoice = invoice->findRecord(id);
		cache.insert(id, invoice);
	}
	return *cache[id];
}

QStringList SaleInvocieCorrections::fillable() const {
	return QStringList()
		<< "Id"
		<< "InvoiceId"
		<< "CorrectionId"
		<< "CreatedDate";
}

}
