#include "dbo/serviceelement.h"
#include "globaldata.hpp"
#include "repository/database/dbwoodtyperepository.hpp"

namespace core::service {

Element::Element()
	: core::eloquent::Model<core::service::Element>{}
{
}

void Element::setWoodType(eloquent::DBIDKey woodTypeId) {
	Q_ASSERT(woodTypeId > 0);
	set("WoodTypeId", woodTypeId);
	_woodType = core::repository::database::WoodTypeRepository().get(woodTypeId);
}

void Element::setWoodType(const WoodType& woodType) {
	set("WoodTypeId", woodType.id());
	_woodType = woodType;
}

double Element::stere() const {
	double R = (diameter() / 2.f) / 100.f;	// calculate radius from diameter and convert centimeters to meters
	double Pp = M_PI * (R*R);

	return Pp * length();
}

QStringList Element::fillable() const {
	return QStringList()
		<< "Id"
		<< "ServiceId"
		<< "Diameter"
		<< "Length"
		<< "IsRotated"
		<< "WoodTypeId";
}

QDataStream& operator<<(QDataStream& stream, const core::service::Element& element) noexcept {
	stream << element.id()
		   << element.serviceId()
		   << element.diameter()
		   << element.length()
		   << element.isRotated()
		   << element.woodType();

	return stream;
}

QDataStream& operator>>(QDataStream& stream, core::service::Element& element) noexcept {
	core::eloquent::DBIDKey id, serviceId;
	double diameter, length;
	bool rotated;
	core::WoodType wood;

	stream >> id
		>> serviceId
		>> diameter
		>> length
		>> rotated
		>> wood;

	element.setId(id);
	element.setServiceId(serviceId);
	element.setDiameter(diameter);
	element.setLength(length);
	element.setRotated(rotated);
	element.setWoodType(wood);

	return stream;
}

} // namespace core::service
