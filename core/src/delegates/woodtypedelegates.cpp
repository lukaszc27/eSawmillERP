#include "delegates/woodtypedelegate.hpp"
#include <QLineEdit>
#include <QDoubleSpinBox>


namespace core::delegates {

WoodTypeDelegate::WoodTypeDelegate(QObject* parent)
	: QStyledItemDelegate(parent)
{
}

QWidget *WoodTypeDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
	Q_UNUSED(option);

	switch (index.column()) {
	case core::models::WoodTypeModel::Name:
		return new QLineEdit(parent);

	case core::models::WoodTypeModel::Factor:
	case core::models::WoodTypeModel::WoodBark:
		return new QDoubleSpinBox(parent);
	}
	return nullptr;
}

void WoodTypeDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
	switch (index.column()) {
	case core::models::WoodTypeModel::Name:
	{
		QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
		lineEdit->setText(index.data().toString());
	} break;

	case core::models::WoodTypeModel::Factor:
	{
		QDoubleSpinBox* spinbox = qobject_cast<QDoubleSpinBox*>(editor);
		spinbox->setValue(index.data().toDouble());
	} break;

	case core::models::WoodTypeModel::WoodBark:
	{
		QDoubleSpinBox* spinBox = qobject_cast<QDoubleSpinBox*>(editor);
		auto list = index.data().toString().split(' ');
		spinBox->setValue(list[0].toDouble());
	} break;
	}
}

void WoodTypeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
	switch (index.column()) {
	case core::models::WoodTypeModel::Name:
	{
		QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
		model->setData(index, lineEdit->text().toUpper());
	} break;

	case core::models::WoodTypeModel::Factor:
	case core::models::WoodTypeModel::WoodBark:
	{
		QDoubleSpinBox* spinBox = qobject_cast<QDoubleSpinBox*>(editor);
		model->setData(index, spinBox->value());
	} break;
	}
}

} // namespace core
