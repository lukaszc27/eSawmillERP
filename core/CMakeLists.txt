add_library(core-lib)
target_sources(core-lib
    PRIVATE
        "include/commandexecutor.hpp"
        "include/database.h"
        "include/dbobj.hpp"
        "include/eventagentclient.hpp"
        "include/globaldata.hpp"
        "include/licence.hpp"
        "include/simplecrypt.hpp"
        #
        "src/commandexecutor.cpp"
        "src/database.cpp"
        "src/dbobj.cpp"
        "src/eventagentclient.cpp"
        "src/globaldata.cpp"
        "src/licence.cpp"
        "src/simplecrypt.cpp"
        
        # abstracts directory
        "include/abstracts/abstractcommand.hpp"
        "include/abstracts/abstractexporter.hpp"
        "include/abstracts/abstractmodel.h"
        "include/abstracts/abstractelementsmodel.hpp"
        "src/abstracts/abstractexporter.cpp"

        # comarch directory
        "include/comarch/contractorscomarch.hpp"
        "src/comarch/contractorscomarch.cpp"

        # dbo directory
        "include/dbo/article.h"
        "include/dbo/articleType.h"
        "include/dbo/auth.h"
        "include/dbo/city.hpp"
        "include/dbo/company.h"
        "include/dbo/contact.h"
        "include/dbo/contractor.h"
        "include/dbo/measureunit_core.hpp"
        "include/dbo/order.h"
        "include/dbo/orderElements.h"
        "include/dbo/orderarticle.h"
        "include/dbo/ordersaleinvoices.hpp"
        "include/dbo/purchaseinvoicecorrection.hpp"
        "include/dbo/purchaseinvoice.hpp"
        "include/dbo/purchaseinvoiceitem.hpp"
        "include/dbo/role.hpp"
        "include/dbo/saleinvoice.hpp"
        "include/dbo/saleinvoicecorrections.hpp"
        "include/dbo/saleinvoiceitem.hpp"
        "include/dbo/service.h"
        "include/dbo/servicearticle.h"
        "include/dbo/serviceelement.h"
        "include/dbo/tax.h"
        "include/dbo/user.h"
        "include/dbo/wareHouse.h"
        "include/dbo/woodtype.hpp"
        #
        "src/dbo/article.cpp"
        "src/dbo/articleType.cpp"
        "src/dbo/auth.cpp"
        "src/dbo/city.cpp"
        "src/dbo/company.cpp"
        "src/dbo/contact.cpp"
        "src/dbo/contractor.cpp"
        "src/dbo/measureunit_core.cpp"
        "src/dbo/order.cpp"
        "src/dbo/orderElements.cpp"
        "src/dbo/orderarticle.cpp"
        "src/dbo/ordersaleinvoices.cpp"
        "src/dbo/purchaseinvoicecorrection.cpp"
        "src/dbo/purchaseinvoiceitem.cpp"
        "src/dbo/purchaseinvoice.cpp"
        "src/dbo/role.cpp"
        "src/dbo/saleinvoice.cpp"
        "src/dbo/saleinvoiceitem.cpp"
        "src/dbo/saleinvoicecorrections.cpp"
        "src/dbo/service.cpp"
        "src/dbo/servicearticle.cpp"
        "src/dbo/serviceelement.cpp"
        "src/dbo/tax.cpp"
        "src/dbo/user.cpp"
        "src/dbo/wareHouse.cpp"
        "src/dbo/woodtype.cpp"

        # delegates directory
        "include/delegates/woodtypedelegate.hpp"
        "src/delegates/woodtypedelegates.cpp"

        # documents directory
        "src/documents/abstractdocument.cpp"
        "src/documents/purchaseinvoicedocument.cpp"
        "src/documents/servicedocument.cpp"
        "src/documents/orderdocument.cpp"
        "src/documents/saleinvoicedocument.cpp"
        #
        "include/documents/abstractdocument.h"
        "include/documents/orderdocument.h"
        "include/documents/saleinvoicedocument.hpp"
        "include/documents/abstractinvoice.hpp"
        "include/documents/purchaseinvoicedocument.hpp"
        "include/documents/servicedocument.h"

        # eloquent directory
        "include/eloquent/model.hpp"
        "include/eloquent/builder.hpp"

        # exceptions directory
        "include/exceptions/deviceunregisteredexception.hpp"
        "include/exceptions/notimplementexception.hpp"
        "include/exceptions/forbiddenexception.hpp"
        "include/exceptions/repositoryexception.hpp"
        "include/exceptions/invalidimportexception.hpp"
        "include/exceptions/useralreadyloggedinexception.hpp"

	    "src/exceptions/deviceunregisteredexception.cpp"
	    "src/exceptions/forbiddenexception.cpp"
	    "src/exceptions/invalidimportexception.cpp"
	    "src/exceptions/notimplementexception.cpp"
	    "src/exceptions/repositoryexception.cpp"
	    "src/exceptions/useralreadyloggedinexception.cpp"

        # logs directory
        "include/logs/abstractlogger.hpp"
        "include/logs/filelogger.hpp"
        "src/logs/filelogger.cpp"

        # models directory
        "include/models/woodtypelistmodel.hpp"
        "include/models/stateslistmodel.hpp"
        "include/models/woodtypemodel.hpp"
        #
        "src/models/woodtypelistmodel.cpp"
        "src/models/woodtypemodel.cpp"
        "src/models/stateslistmodel.cpp"

        # network directory
        "include/network/regon/regon.hpp"
        "src/network/regon/regon.cpp"

        # providers directory
        "include/providers/protocol.hpp"
        "include/providers/controlcenterclient.hpp"
        #
        "src/providers/controlcenterclient.cpp"

        # repository directory
        "include/repository/abstractarticlerepository.hpp"
        "include/repository/abstractcontractorrepository.hpp"
        "include/repository/abstractorderrepository.hpp"
        "include/repository/abstractservicerepository.hpp"
        "include/repository/abstractwarehouserepository.hpp"
        "include/repository/abstractcityrepository.hpp"
        "include/repository/abstractinvoicerepository.hpp"
        "include/repository/abstractrepository.hpp"
        "include/repository/abstracttaxrepository.hpp"
        "include/repository/abstractwoodtyperepository.hpp"
        "include/repository/abstractcompanyrepository.hpp"
        "include/repository/abstractmeasureunitrepository.hpp"
        "include/repository/abstractrepositorylocker.hpp"
        "include/repository/abstractuserrepository.hpp"
        #
        "src/repository/abstractarticlerepository.cpp"
        "src/repository/abstractcompanyrepository.cpp"
        "src/repository/abstractinvoicerepository.cpp"
        "src/repository/abstractrepository.cpp"
        "src/repository/abstractuserrepository.cpp"
        "src/repository/abstractcityrepository.cpp"
        "src/repository/abstractcontractorrepository.cpp"
        "src/repository/abstractorderrepository.cpp"
        "src/repository/abstractservicerepository.cpp"
        "src/repository/abstractwarehouserepository.cpp"
        # database subdirectory
        "include/repository/database/databaserepository.hpp"
        "include/repository/database/dbcityrepository.hpp"
        "include/repository/database/dbcontractorrepository.hpp"
        "include/repository/database/dbmeasureunitrepository.hpp"
        "include/repository/database/dbservicerepository.hpp"
        "include/repository/database/dbuserrepository.hpp"
        "include/repository/database/dbwoodtyperepository.hpp"
        "include/repository/database/dbarticlerepository.hpp"
        "include/repository/database/dbcompanyrepository.hpp"
        "include/repository/database/dbinvoicerepository.hpp"
        "include/repository/database/dborderrepository.hpp"
        "include/repository/database/dbtaxrepository.hpp"
        "include/repository/database/dbwarehouserepository.hpp"
        #
        "src/repository/database/databaserepository.cpp"
        "src/repository/database/dbcityrepository.cpp"
        "src/repository/database/dbcontractorrepository.cpp"
        "src/repository/database/dbmeasureunitrepository.cpp"
        "src/repository/database/dbservicerepository.cpp"
        "src/repository/database/dbuserrepository.cpp"
        "src/repository/database/dbwoodtyperepository.cpp"
        "src/repository/database/dbarticlerepository.cpp"
        "src/repository/database/dbcompanyrepository.cpp"
        "src/repository/database/dbinvoicerepository.cpp"
        "src/repository/database/dborderrepository.cpp"
        "src/repository/database/dbtaxrepository.cpp"
        "src/repository/database/dbwarehouserepository.cpp"

        # remote subdirectory
        "include/repository/remote/remotearticlerepository.hpp"
        "include/repository/remote/remotecompanyrepository.hpp"
        "include/repository/remote/remoteinvoicerepository.hpp"
        "include/repository/remote/remoteorderrepository.hpp"
        "include/repository/remote/remoteservicerepository.hpp"
        "include/repository/remote/remoteuserrepository.hpp"
        "include/repository/remote/remotewoodtyperepository.hpp"
        "include/repository/remote/remotecityrepository.hpp"
        "include/repository/remote/remotecontractorrepository.hpp"
        "include/repository/remote/remotemeasureunitrepository.hpp"
        "include/repository/remote/remoterepository.hpp"
        "include/repository/remote/remotetaxrepository.hpp"
        "include/repository/remote/remotewarehouserepository.hpp"
        #
        "src/repository/remote/remotearticlerepository.cpp"
        "src/repository/remote/remotecompanyrepository.cpp"
        "src/repository/remote/remoteinvoicerepository.cpp"
        "src/repository/remote/remoteorderrepository.cpp"
        "src/repository/remote/remoteservicerepository.cpp"
        "src/repository/remote/remoteuserrepository.cpp"
        "src/repository/remote/remotewoodtyperepository.cpp"
        "src/repository/remote/remotecityrepository.cpp"
        "src/repository/remote/remotecontractorrepository.cpp"
        "src/repository/remote/remotemeasureunitrepository.cpp"
        "src/repository/remote/remoterepository.cpp"
        "src/repository/remote/remotetaxrepository.cpp"
        "src/repository/remote/remotewarehouserepository.cpp"

        # settings directory
	"include/settings/contractorsettings.hpp"
	"include/settings/ordersettings.hpp"
        "include/settings/abstractsettings.hpp"
        "include/settings/appsettings.hpp"
        "include/settings/ordersettings.hpp"
        "include/settings/invoices/saleinvoicesettings.hpp"
        "include/settings/invoices/purchaseinvoicesettings.hpp"
        #
        "src/settings/contractorsettings.cpp"
        "src/settings/abstractsettings.cpp"
        "src/settings/appsettigns.cpp"
        "src/settings/ordersettings.cpp"
        "src/settings/invoices/purchaseinvoicesettings.cpp"
        "src/settings/invoices/saleinvoicesettings.cpp"

        # verification directory
        "include/verifications/abstractverification.hpp"
        "include/verifications/notification.hpp"
        "include/verifications/notificationsmodel.hpp"
        "include/verifications/verificationexecutor.hpp"
        "include/verifications/verificationswidget.hpp"
        #
        "src/verifications/abstractverification.cpp"
        "src/verifications/notification.cpp"
        "src/verifications/notificationsmodel.cpp"
        "src/verifications/verificationexecutor.cpp"
        "src/verifications/verificationswidget.cpp"
)
target_link_libraries(core-lib
    Qt::Core
    Qt::Sql
    Qt::Gui
    Qt::Xml
    Qt::Widgets
    Qt::Network
)
target_compile_definitions(core-lib PRIVATE CORE_LIBRARY)
target_include_directories(core-lib
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
)
install(TARGETS core-lib)
