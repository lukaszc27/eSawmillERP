
CREATE DATABASE [SAWMILL_TEST];
GO
USE [SAWMILL_TEST];
GO

CREATE TABLE [Contacts] (
    [Id]          BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [City]        NVARCHAR(64) NOT NULL,
    [ZipCode]     VARCHAR(16) NOT NULL,
    [Street]      NVARCHAR(64) NOT NULL,
    [HomeNumber]  VARCHAR(8) NOT NULL,
    [Phone]       VARCHAR(16) NOT NULL UNIQUE,
    [Email]       VARCHAR(32)
);
GO

ALTER TABLE [Contacts] ADD
	[Country]		NVARCHAR(255) DEFAULT 'POLSKA',
	[IsoCountry]	VARCHAR(2) DEFAULT 'PL',
	[PostOffice]	NVARCHAR(255),
	[LocalNumber]	NVARCHAR(16),
	[State]			NVARCHAR(255),
	[Url]			NVARCHAR(255)
GO

CREATE TABLE [Companies] (
    [Id]              BIGINT  IDENTITY(1, 1) PRIMARY KEY,
    [Name]            NVARCHAR(128) NOT NULL,
    [PersonName]      NVARCHAR(64) NOT NULL,
    [PersonSurname]   NVARCHAR(64) NOT NULL,
    [NIP]             VARCHAR(16) NOT NULL UNIQUE,
    [REGON]           VARCHAR(16),
    [KRS]             VARCHAR(16),
    [ContactId]       BIGINT,

    FOREIGN KEY (ContactId) REFERENCES Contacts(Id) ON DELETE CASCADE
);
GO

 ---- użytkownicy
CREATE TABLE [Users] (
    [Id]          BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]        NVARCHAR(64) NOT NULL,
    [Password]    NVARCHAR(64) NOT NULL,
    [Token]       TEXT,
    [ContactId]   BIGINT,
    [CompanyId]   BIGINT,
    [FirstName]     NVARCHAR(64) NULL DEFAULT NULL,
    [SurName]       NVARCHAR(64) NULL DEFAULT NULL,

    FOREIGN KEY (ContactId) REFERENCES Contacts(Id) ON DELETE CASCADE,
    FOREIGN KEY (CompanyId) REFERENCES Companies(Id)
);
GO

---- role użytkownika
CREATE TABLE [Roles] (
    [Id]        BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]      nVARCHAR(64) NOT NULL
);
GO
-- wartości początkowe
INSERT INTO [Roles] ([Name]) VALUES 
    ('ADMINISTRATOR'),
    ('PRACOWNIK'),
    ('OPERATOR'),
    ('PILARZ'),
    ('KIEROWCA')
GO

CREATE TABLE [UsersRoles] (
    [Id]        BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [UserId]    BIGINT NOT NULL,
    [RoleId]    BIGINT NOT NULL,

    FOREIGN KEY (UserId) REFERENCES Users(Id) ON DELETE CASCADE,
    FOREIGN KEY (RoleId) REFERENCES Roles(Id) ON DELETE CASCADE
);
GO

CREATE TABLE [ArticleTypes] (
    [Id]      BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]    NVARCHAR(32) NOT NULL
);
GO

CREATE TABLE [VATs] (
    [Id]        BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Value]     DECIMAL NOT NULL
);
GO

CREATE TABLE [Contractors] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]          NVARCHAR(128) NOT NULL,
    [NIP]           VARCHAR(32),
    [REGON]         VARCHAR(32),
    [PESEL]         VARCHAR(32),
    [Provider]      BIT DEFAULT 0,
    [ContactId]     BIGINT,
    [PersonName]    NVARCHAR(64),
    [PersonSurname] NVARCHAR(64),
    [ParentId]	    BIGINT DEFAULT 0,	-- id kontrahenta nadrzędnego
	[Blocked]	    BIT DEFAULT 0,  	-- czy dany kontrahent został zablokowany
    [ComarchId]     BIGINT DEFAULT NULL

    FOREIGN KEY (ContactId) REFERENCES Contacts(Id) ON DELETE CASCADE
);
GO


CREATE TABLE [Warehouses] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]          NVARCHAR(63) NOT NULL,
    [Shortcut]      NVARCHAR(16),
    [Description]   TEXT
);
GO

CREATE TABLE [UnitMeasure] (
    [Id]        BIGINT PRIMARY KEY IDENTITY(1, 1),
    [ShortName] NVARCHAR(16) NOT NULL,
    [FullName]  NVARCHAR(255)
)
GO

CREATE TABLE [Cities] (
	[Id]	BIGINT IDENTITY(1, 1) PRIMARY KEY,
	[Name]	NVARCHAR(255) NOT NULL
);
GO

CREATE TABLE [WoodTypes] (
	[Id]		BIGINT IDENTITY(1, 1) PRIMARY KEY,
	[Name]		NVARCHAR(128),
	[Factor]	FLOAT DEFAULT 1.00,
    [WoodBark]  FLOAT DEFAULT 0
);
GO

CREATE TABLE [OrderElementNames] (
	[Id]	BIGINT IDENTITY(1,1) PRIMARY KEY,
	[Name]	NVARCHAR(128)
)
GO

CREATE TABLE [Articles] (
    [Id]                   BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Name]                 VARCHAR(128) NOT NULL,
    [BarCode]              VARCHAR(32),
    [ArticleTypeId]        BIGINT,
    [ProviderId]           BIGINT,
    [WarehouseId]          BIGINT,
    [PurchaseVatId]        BIGINT,
    [SaleVatId]            BIGINT,
    [PricePurchaseNetto]   FLOAT DEFAULT 0,
    [PricePurchaseBrutto]  FLOAT DEFAULT 0,
    [PriceSaleNetto]       FLOAT DEFAULT 0,
    [PriceSaleBrutto]      FLOAT DEFAULT 0,
    [Quantity]             FLOAT NOT NULL DEFAULT 1.00,
    [CompanyId]            BIGINT,
    [UserAddId]            BIGINT,
    [UnitId]               BIGINT

    FOREIGN KEY (ArticleTypeId) REFERENCES ArticleTypes(Id),
    FOREIGN KEY (ProviderId) REFERENCES Contractors(Id),
    FOREIGN KEY (SaleVatId) REFERENCES VATs(Id),
    FOREIGN KEY (PurchaseVatId) REFERENCES VATs(Id),
    FOREIGN KEY (WarehouseId) REFERENCES Warehouses(Id),
    FOREIGN KEY (UnitId) REFERENCES UnitMeasure(Id)
);
GO

INSERT INTO ArticleTypes ([Name]) VALUES 
    ('TOWAR'),
    ('USŁUGA'),
    ('PRODUKT');

INSERT INTO VATs ([Value]) VALUES
    (0),
    (3),
    (4),
    (5),
    (6),
    (7),
    (8),
    (22),
    (23);
GO

CREATE TABLE [Orders] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [Number]        NVARCHAR(32) DEFAULT NULL,
    [ContractorId]  BIGINT,
    [UserAddId]     BIGINT,
    [CompanyId]     BIGINT,
    [Name]          NVARCHAR(128) NOT NULL,
    [AddDate]       SMALLDATETIME DEFAULT GetDate(),
    [EndDate]       SMALLDATETIME NOT NULL,
    [IsRealised]    BIT DEFAULT 0,
    [Rebate]        FLOAT DEFAULT 0,
    [Priority]      INT DEFAULT 1,
    [VatId]         BIGINT,
    [Price]         FLOAT NOT NULL,
    [Description]   TEXT,
    [SpareLength] FLOAT DEFAULT 0,

    FOREIGN KEY (ContractorId) REFERENCES Contractors(Id),
    FOREIGN KEY (VatId) REFERENCES VATs(Id),
    FOREIGN KEY (UserAddId) REFERENCES Users(Id),
    FOREIGN KEY (CompanyId) REFERENCES Companies(Id)
);
GO

CREATE TABLE [OrderElements] (
    [Id]        BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [OrderId]   BIGINT,
    [Width]     FLOAT NOT NULL,
    [Height]    FLOAT NOT NULL,
    [Length]    FLOAT NOT NULL,
    [Quantity]  FLOAT NOT NULL,
    [Planed]    BIT DEFAULT 0,
    [WoodTypeId]	BIGINT,
    [Name]          NVARCHAR(128)

    FOREIGN KEY (OrderId) REFERENCES Orders(Id) ON DELETE CASCADE,
    FOREIGN KEY (WoodTypeId) REFERENCES WoodTypes(Id) ON DELETE SET NULL
);
GO

CREATE TABLE [Services] (
    [Id]            BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [ContractorId]  BIGINT,
    [UserAddId]     BIGINT,
    [CompanyId]     BIGINT,
    [Number]        NVARCHAR(32) DEFAULT NULL,
    [Name]          NVARCHAR(128) NOT NULL,
    [AddDate]       SMALLDATETIME DEFAULT GETDATE(),
    [EndDate]       SMALLDATETIME NOT NULL,
    [IsRealised]    BIT DEFAULT 0,
    [Rebate]        FLOAT DEFAULT 0,
    [Priority]      INT DEFAULT 1,
    [VatId]         BIGINT,
    [Price]         FLOAT NOT NULL,
    [Description]   TEXT,

    FOREIGN KEY (ContractorId) REFERENCES Contractors(Id),
    FOREIGN KEY (VatId) REFERENCES VATs(Id),
    FOREIGN KEY (UserAddId) REFERENCES Users(Id),
    FOREIGN KEY (CompanyId) REFERENCES Companies(Id)
);
GO

CREATE TABLE [ServiceElements] (
    [Id]           BIGINT IDENTITY(1, 1) PRIMARY KEY,
    [ServiceId]    BIGINT,
    [Diameter]     FLOAT NOT NULL,
    [Length]       FLOAT NOT NULL,
    [IsRotated]    BIT DEFAULT 0,
    [WoodTypeId]    BIGINT,

    FOREIGN KEY (WoodTypeId) REFERENCES WoodTypes(Id) ON DELETE SET NULL,
    FOREIGN KEY (ServiceId) REFERENCES Services(Id) ON DELETE CASCADE
);
GO

CREATE TABLE [OrderArticles] (
    [Id]        BIGINT PRIMARY KEY IDENTITY(1, 1),
    [OrderId]   BIGINT,
    [ArticleId] BIGINT,
    [Quantity]  FLOAT NOT NULL DEFAULT 1.00

    FOREIGN KEY (OrderId) REFERENCES Orders(Id) ON DELETE CASCADE,
    FOREIGN KEY (ArticleId) REFERENCES Articles(Id) ON DELETE SET NULL
);
GO

CREATE TABLE [ServiceArticle] (
    [Id]        BIGINT PRIMARY KEY IDENTITY(1, 1),
    [ServiceId] BIGINT,
    [ArticleId] BIGINT,
    [Quantity]  FLOAT NOT NULL DEFAULT 1.00,

    FOREIGN KEY(ServiceId) REFERENCES Services(Id) ON DELETE CASCADE,
    FOREIGN KEY(ArticleId) REFERENCES Articles(Id) ON DELETE CASCADE
);
GO



--
-- DODANIE PODSTAWOWYCH WARTOŚCI DO BAZY
--
INSERT INTO [UnitMeasure] ([ShortName], [FullName]) VALUES
    ('szt', 'Sztuka'),
    ('kg', 'Kilogram'),
    ('t', 'Tona'),
    ('m3', 'Metr sześcienny'),
    ('m2', 'Metr kwadratowy'),
    ('mb', 'Metr bierzący');

INSERT INTO [Cities] ([Name]) VALUES
	('GORLICE'),
	('JASŁO'),
	('NOWY SĄCZ'),
	('KRAKÓW'),
	('WIELICZKA');

INSERT INTO [WoodTypes] ([Name], [Factor]) VALUES
	('JODŁA', 1),
	('ŚWIERK', 1),
	('SOSNA', 1.2),
	('MODRZEW', 1.3);
