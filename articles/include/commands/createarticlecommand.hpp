#ifndef CREATEARTICLECOMMAND_HPP
#define CREATEARTICLECOMMAND_HPP

#include "../articles_global.h"
#include <repository/abstractrepository.hpp>
#include <abstracts/abstractcommand.hpp>
#include <dbo/article.h>

namespace eSawmill::articles::commands {

class ARTICLES_EXPORT CreateArticleCommand final : public core::abstracts::AbstractCommand {
public:
	explicit CreateArticleCommand(const core::Article& article);
	virtual ~CreateArticleCommand() = default;

	bool execute(core::repository::AbstractRepository& repository) override;
	bool undo(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }

private:
	core::Article article_;
	core::eloquent::DBIDKey insertedArticleId_;
};

} // namespace eSawmill::articles::commands

#endif // CREATEARTICLECOMMAND_HPP
