#ifndef REMOVEARTICLECOMMAND_HPP
#define REMOVEARTICLECOMMAND_HPP

#include "../articles_global.h"
#include <repository/abstractrepository.hpp>
#include <abstracts/abstractcommand.hpp>
#include <dbo/article.h>

namespace eSawmill::articles::commands {

class ARTICLES_EXPORT RemoveArticleCommand final : public core::abstracts::AbstractCommand {
public:
	explicit RemoveArticleCommand(core::eloquent::DBIDKey articleId);
	virtual ~RemoveArticleCommand() = default;

	bool execute(core::repository::AbstractRepository& repository) override;
	bool undo(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }

private:
	core::eloquent::DBIDKey articleId_;
	core::Article removedArticle_;
};

} // namespace eSawmill::articles::commands

#endif // REMOVEARTICLECOMMAND_HPP
