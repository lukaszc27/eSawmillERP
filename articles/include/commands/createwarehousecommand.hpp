#ifndef CREATEWAREHOUSECOMMAND_HPP
#define CREATEWAREHOUSECOMMAND_HPP

#include "../articles_global.h"
#include <abstracts/abstractcommand.hpp>
#include <repository/abstractrepository.hpp>
#include <dbo/wareHouse.h>

namespace eSawmill::articles::commands {

class ARTICLES_EXPORT CreateWarehouseCommand final : public core::abstracts::AbstractCommand {
public:
	explicit CreateWarehouseCommand(const core::Warehouse& warehouse);
	virtual ~CreateWarehouseCommand() = default;

	bool execute(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }
	bool undo(core::repository::AbstractRepository& repository) override;

private:
	core::Warehouse warehouse_;
};

} // namespace eSawmill::articles::commands

#endif // CREATEWAREHOUSECOMMAND_HPP
