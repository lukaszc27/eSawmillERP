#ifndef UPDATEARTICLECOMMAND_HPP
#define UPDATEARTICLECOMMAND_HPP

#include "../articles_global.h"
#include <dbo/article.h>
#include <repository/abstractrepository.hpp>
#include <abstracts/abstractcommand.hpp>

namespace eSawmill::articles::commands {

class ARTICLES_EXPORT UpdateArticleCommand final : public core::abstracts::AbstractCommand {
public:
	explicit UpdateArticleCommand(const core::Article& article);
	virtual ~UpdateArticleCommand() = default;

	bool execute(core::repository::AbstractRepository& repository) override;
	bool undo(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }

private:
	core::Article article_;
	core::Article beforeUpdateArticle_;
};

} // namespace eSawmill::articles::commands

#endif // UPDATEARTICLECOMMAND_HPP
