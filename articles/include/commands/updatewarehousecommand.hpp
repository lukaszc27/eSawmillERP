#ifndef UPDATEWAREHOUSECOMMAND_HPP
#define UPDATEWAREHOUSECOMMAND_HPP

#include "../articles_global.h"
#include <repository/abstractrepository.hpp>
#include <abstracts/abstractcommand.hpp>
#include <dbo/wareHouse.h>


namespace eSawmill::articles::commands {

class ARTICLES_EXPORT UpdateWarehouseCommand final : public core::abstracts::AbstractCommand {
public:
	explicit UpdateWarehouseCommand(const core::Warehouse& warehouse);
	virtual ~UpdateWarehouseCommand() = default;

	bool execute(core::repository::AbstractRepository& repository) override;
	bool undo(core::repository::AbstractRepository& repository) override;
	bool canUndo() const override { return true; }

private:
	core::Warehouse warehouse_;
	core::Warehouse lastWarehouse_;	// warehouse object before editing
};

} // namespace eSawmill::articles::commands

#endif // UPDATEWAREHOUSECOMMAND_HPP
