#ifndef ARTICLEWIDGET_H
#define ARTICLEWIDGET_H

#include "../articles_global.h"
#include "dbo/service.h"

#include <QWidget>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>
#include <QSortFilterProxyModel>

// moduł widgets
#include <pushbutton.h>
#include <tableview.h>
#include <abstracts/abstractmodel.h>
#include <dbo/article.h>
#include <dbo/order.h>


namespace eSawmill::articles::widgets {

///
/// \brief The ArticleWidget class
/// widget to display articles assigned to order or service
///
class ARTICLES_EXPORT ArticleWidget : public QWidget {
	Q_OBJECT

public:
	ArticleWidget(core::models::AbstractModel<core::Article>* model, QWidget* parent = nullptr);

	///
	/// \brief setOrder
	/// assign valid order or service
	/// \param orderId
	///
	void setOrder(core::eloquent::DBIDKey orderId);

	///
	/// \brief loadService
	/// load articles and other nessesary information for service
	/// used in service creator only
	/// \param service
	///
	void load(const core::Service& service);

	///
	/// \brief load
	/// load articles and other nessesary information for order
	/// \param order
	///
	void load(const core::Order& order);

	/**
	 * @brief articles
	 * @return - lista elementów w zamówieniu
	 */
	QList<core::Article> items() const { return mModel->items(); }

	/**
	 * @brief insert dodaje artykuły do widgetu
	 * @param articles
	 */
	void insert(QList<core::Article> articles);

	/**
	 * @brief setReadOnly - ustwia widget w tryb prezentacji danych
	 * aby uniemożliwić użytkownikowi zmianę danych
	 * @param readonly
	 */
	void setReadOnly(bool readonly);

	/**
	 * @brief totalPrice - całkowita cena artykułów dodanych do listy
	 * @return
	 */
	double totalPrice() const { return mModel->totalPrice(); }

private:
	void createWidgets();
	void createConnections();
	void createModels();

	QComboBox* mArticleColumnSelector;
	QLineEdit* mArticleSearchLine;
	QPushButton* mArticleSearchButton;
	QLabel* mTotalArticlesLabel;
	::widgets::TableView* mArticleView;
	::widgets::PushButton* mArticleAddButton;
	::widgets::PushButton* mArticleRemoveButton;

	core::models::AbstractModel<core::Article>* mModel;
	QSortFilterProxyModel* mFilterModel;

private Q_SLOTS:
	/**
	 * @brief addArticle_clicked
	 * dodawanie nowego artykułu do listy zamówienia
	 */
	void addArticle_clicked();

	/**
	 * @brief filterArticle_clicked
	 * filtorwanie elementów po określonej kolumnie
	 * oraz określonym wzorcu
	 */
	void filterArticle_clicked();

	/**
	 * @brief removeArticle_clicked
	 * usuwanie artykułów z misty
	 */
	void removeArticle_clicked();

protected:
	///
	/// \brief notifyArticlesChanged
	/// executed every time when articles will be changed in widget
	/// e.g by adding or remove actions
	///
	virtual void notifyArticlesChanged();

Q_SIGNALS:
	/**
	 * @brief articlesChanged
	 * sygnał emitowany w przypadku edycji listy artykułów w dialogu
	 */
	void articlesChanged();
};

} // namespace eSawmill


#endif // ARTICLEWIDGET_H
