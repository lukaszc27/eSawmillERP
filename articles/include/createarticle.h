#ifndef CREATEARTICLE_H
#define CREATEARTICLE_H

#include "articles_global.h"

#include <QDialog>
#include <QGroupBox>
#include <QLineEdit>
#include <QComboBox>
#include <QDoubleSpinBox>

#include <widgets/contractorselector.h>
#include <pushbutton.h>					// moduł widgets
#include <dbo/article.h>


namespace eSawmill::articles {

class ARTICLES_EXPORT CreateArticle : public QDialog {
	Q_OBJECT

public:
	explicit CreateArticle(QWidget* parent = nullptr);
	explicit CreateArticle(core::eloquent::DBIDKey id, QWidget* parent = nullptr);
	virtual ~CreateArticle() = default;

	/**
	 * @brief article
	 * @return core::Article
	 *
	 * Zwraca obiekt Article utworzony na podstawie danych
	 * wprowadzonych do dialogu
	 */
	core::Article article() const;

private:
	//// Kontrolki
	QGroupBox* mGeneralGroup;
	QGroupBox* mPriceGroup;

	::widgets::PushButton* mAcceptButton;
	::widgets::PushButton* mRejectButton;
	eSawmill::contractors::widgets::ContractorSelector* mProviderSelector;

	// grupa ogólne
	QLineEdit* mName;
	QLineEdit* mBarcode;
	QComboBox* mArticleType;
	QComboBox* mWarehouse;
	QComboBox* mMeasureUnit;
	QSpinBox* mQuantity;

	// grupa ceny
	QDoubleSpinBox* mPricePurchaseNetto;
	QDoubleSpinBox* mPricePurchaseBrutto;
	QDoubleSpinBox* mPriceSaleNetto;
	QDoubleSpinBox* mPriceSaleBrutto;
	QComboBox* mPurchaseTax;
	QComboBox* mSaleTax;

	//// Funkcje
	void createWidgets();
	void createConnections();
	void createModels();
	void createValidators();

	QWidget* createGeneralWidget();
	QWidget* createPriceWidget();

private Q_SLOTS:
	/**
	 * @brief pricePurchaseNetto_changed
	 * zmiana wartości netto ceny zakupu
	 * obliczenie ceny brutto zakupu uwzględniając Tax zakupu
	 */
	void pricePurchaseNetto_changed(double value);

	/**
	 * @brief priceSaleNetto_changed
	 * zmiana wartości netto ceny sprzedarzy
	 * obliczanie wartości brutto ceny sprzedaży
	 * z uwzględnieniem Tax-u
	 */
	void priceSaleNetto_changed(double value);

	void checkBarcode();

private:
	// id aktualnie edytowanego przedmiotu
	unsigned int mCurrentArticleId;
};

} // namespace eSawmill

#endif // CREATEARTICLE_H
