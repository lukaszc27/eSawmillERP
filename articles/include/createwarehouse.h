#ifndef CREATEWAREHOUSE_H
#define CREATEWAREHOUSE_H

#include "articles_global.h"

#include <QDialog>
#include <QLineEdit>
#include <QPlainTextEdit>

#include "pushbutton.h"	// moduł widgets
#include "dbo/wareHouse.h"

namespace eSawmill::articles {

///
/// \brief The CreateWarehouse class
/// dialog to create new warehouse
///
class ARTICLES_EXPORT CreateWarehouse : public QDialog {
	Q_OBJECT

public:
	CreateWarehouse(QWidget* parent = nullptr);
	CreateWarehouse(core::eloquent::DBIDKey id, QWidget* parent = nullptr);
	virtual ~CreateWarehouse() = default;

	///@{
	/// methods to get data from dialog in safe mode
	inline QString fullName() const { return mFullName->text().toUpper(); }
	inline QString shortName() const { return mShortName->text().toUpper(); }
	inline QString description() const { return mDescription->document()->toPlainText().toUpper(); }
	///@}

	///
	/// \brief warehouse
	/// return valid Warehouse object created from data entered by user
	/// \return valid warehouse object from data entered by user in dialog
	///
	core::Warehouse warehouse();

private:
	///@{
	/// widgets (controls) in dialog
	QLineEdit* mFullName;
	QLineEdit* mShortName;
	QPlainTextEdit* mDescription;
	widgets::PushButton* mAcceptButton;
	widgets::PushButton* mRejectButton;
	///@}

	unsigned int mWarehouseId;	///< id loaded object (when warehouse is editing)

	void createWidgets();
};

} // namespace eSawmill

#endif // CREATEWAREHOUSE_H
