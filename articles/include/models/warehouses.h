#ifndef WAREHOUSES_H
#define WAREHOUSES_H

#include "../articles_global.h"
#include "dbo/wareHouse.h"
#include <abstracts/abstractmodel.h>
#include <QList>
#include <commandexecutor.hpp>


namespace eSawmill::articles::models {

///
/// \brief The Warehouses class
/// base models to present data in warehouse manager
///
class ARTICLES_EXPORT Warehouses
		: public core::models::AbstractModel<core::Warehouse>
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	Warehouses(QObject* parent = nullptr);
	virtual ~Warehouses();

	///@{
	/// standards ovedrride methods used by Qt framework
	/// to present data
	int rowCount(const QModelIndex &index) const override;
	int columnCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override { return mHeaders; }
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

	///@{
	/// specifited methods to get data from repository and manage by model
	/// it is deprecated and can be removed in next app versions (when I will make code refactoring ;) )
	void createOrUpdate(core::Warehouse warehouse) override;
	void get() override;
	void destroy(core::Warehouse element) override;
	///@}


	/// column names in model
	enum Columns {
		FullName,
		ShortName,
		Description
	};

	/// additional roles used by model
	enum Roles {
		WarehouseId = Qt::UserRole + 1
	};

private:
	// CommandExecutor::Listener implementation
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;

private:
	QStringList mHeaders;
};

} // namespace eSawmill

#endif // WAREHOUSES_H
