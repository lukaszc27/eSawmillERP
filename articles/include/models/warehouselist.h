#ifndef WAREHOUSELIST_H
#define WAREHOUSELIST_H

#include <QAbstractListModel>
#include <QList>
#include "../articles_global.h"
#include <dbo/wareHouse.h>
#include <commandexecutor.hpp>


namespace eSawmill::articles::models {

///
/// \brief The WarehouseList class
/// simple model to present list of warehouses name
/// can present full name, short name, and full and short name
/// in format full name (short name)
///
class ARTICLES_EXPORT WarehouseList
		: public QAbstractListModel
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	enum class Format;	// only declaration implementation below

	///@{
	/// ctors
	explicit WarehouseList(
		QObject* parent = nullptr,
		Format format = Format::FullName
	);
	virtual ~WarehouseList();
	///@}

	///@{
	/// methods used by Qt framework to present data
	int rowCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

	///
	/// \brief get
	/// get data from repository
	///
	void get();

	///
	/// \brief The Roles enum
	/// additional roles used in model
	///
	enum Roles {
		Id = Qt::UserRole + 1	///< warehouse id
	};

	///
	/// \brief The Format enum
	/// type to format warehouse name
	///
	enum class Format {
		All,		///< show two names (full and short) in model
		FullName,	///< show only full name
		ShortName	///< show only short name
	};

private:
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;

private:
	QList<core::Warehouse> mList;	///< list of elements to present
	Format mFormat;					///< current presenting name type
};

} // namespace eSawmill

#endif // WAREHOUSELIST_H
