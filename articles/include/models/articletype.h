#ifndef ARTICLESTYPE_H
#define ARTICLESTYPE_H

#include "../articles_global.h"

#include <QAbstractListModel>
#include <QList>
#include <dbo/articleType.h>

namespace eSawmill::articles::models {

///
/// \brief The ArticleType class
/// represent model object to present all article types
///
class ARTICLES_EXPORT ArticleType : public QAbstractListModel {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit ArticleType(QObject* parent = nullptr);
	virtual ~ArticleType() = default;
	///@}

	///@{
	/// methods specifited to Qt framework
	int rowCount(const QModelIndex &index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

	///
	/// \brief get - get data from repository
	///
	void get();

	///
	/// \brief The Roles enum
	/// additional roles used by model
	///
	enum Roles {
		Id = Qt::UserRole + 1		///< type id
	};

private:
	QList<core::ArticleType> mList;	///< list of elements to present
};

} // namespace eSawmill

#endif // ARTICLESTYPE_H
