#ifndef ARTICLESMODEL_H
#define ARTICLESMODEL_H

#include "articles_global.h"
#include <abstracts/abstractmodel.h>
#include <dbo/article.h>	// moduł core
#include <commandexecutor.hpp>

namespace eSawmill::articles::models {

///
/// \brief The Articles class
/// main article models to present grid in article manager dialog
///
class ARTICLES_EXPORT Articles
		: public core::models::AbstractModel<core::Article>
		, private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	explicit Articles(QObject* parent = nullptr);
	virtual ~Articles();

	///@{
	/// methods used by Qt framework to display data in model
	int rowCount(const QModelIndex &index) const override;
	int columnCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QStringList headerData() const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

	///
	/// \brief The Columns enum
	/// column indexes in model
	///
	enum Columns {
		Name,
		BarCode,
		Quantity,
		MeasureUnit,
		Provider,
		PricePurchaseNetto,
		PricePurchaseBrutto,
		PurchaseTax,
		PriceSaleNetto,
		PriceSaleBrutto,
		SaleTax
	};

	///
	/// \brief The Roles enum
	/// additional roles used in data methods to get data from model
	///
	enum Roles {
		Id = Qt::UserRole + 1	///< artile id in row
	};

	///@{
	/// THIS METHODS IS DEPRECATED AND CAN BE REMOVED ON NEXT CODE RECACTORING
	///
	/// \brief createOrUpdate create new article or update existing
	/// \deprecated
	/// \param article
	///
	void createOrUpdate(core::Article article) override;

	///
	/// \brief get
	/// load item list to disply from repository to model
	///
	void get() override;

	///
	/// \brief destroy - remove existing article from system
	/// \deprecated
	/// \param article
	///
	void destroy(core::Article article) override;
	///@}

private:
	// CommandExecutor::Listener implementation
	virtual void stackChanged(bool empty) override {};
	virtual void commandExecuted() override;

public Q_SLOTS:

	///
	/// \brief setCurrentWarehouse
	/// active display articles from warehouse
	/// \param id - warehouse identificator from which articles will be loaded
	///
	void setCurrentWarehouse(unsigned int id) {
		mWarehouseId = id;
		emit warehouseChanged(id);
	}

private:
	///
	/// \brief currentWarehouse
	/// \return current used warehouse identificator
	///
	unsigned int currentWarehouse() const { return mWarehouseId; }

	unsigned int mWarehouseId;			///< current warehouse identificator
Q_SIGNALS:
	///
	/// \brief warehouseChanged (signal)
	/// this signal is emitted when user change current selected warehouse
	/// \param id - identificator new use warehouse
	///
	void warehouseChanged(int id);
};

} // namespace eSawmill

#endif // ARTICLESMODEL_H
