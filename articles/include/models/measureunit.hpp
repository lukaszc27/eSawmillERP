#ifndef MEASUREUNIT_HPP
#define MEASUREUNIT_HPP

#include "articles_global.h"
#include "dbo/measureunit_core.hpp"

#include <QAbstractListModel>
#include <QList>


namespace eSawmill::articles::models {

///
/// \brief The MeasureUnit class
/// model present data in list (only in one column)
///
class ARTICLES_EXPORT MeasureUnit : public QAbstractListModel {
	Q_OBJECT

public:
	///
	/// \brief The ModelType enum
	/// type of data presentation
	///
	enum class ModelType {
		ShortName,			///< present only short name
		FullName,			///< present only full name
		FullAndShortName	///< present short and full name
	};

	///@{
	/// ctors
	MeasureUnit(const ModelType& type, QObject* parent = nullptr);
	MeasureUnit(QObject* parent = nullptr);
	virtual ~MeasureUnit();
	///@}

	///@{
	/// methods used by Qt framework
	int rowCount(const QModelIndex& index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	///@}

	///
	/// \brief get
	/// get data to display from repository
	///
	void get();

	///
	/// \brief The Roles enum
	/// additional roles used in model
	///
	enum Roles {
		Id = Qt::UserRole + 1,
		ShortName,
		FullName,
	};

private:
	QList<core::MeasureUnit> mList;	///< list of elements to display in model
	ModelType mType;				///< presentation type
};

} // namespace eSawmill

#endif // MEASUREUNIT_HPP
