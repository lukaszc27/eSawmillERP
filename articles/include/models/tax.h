#ifndef TaxMODEL_H
#define TaxMODEL_H

#include "../articles_global.h"
#include <QAbstractListModel>
#include <QList>

#include <dbo/tax.h>	// moduł core


namespace eSawmill::articles::models {

///
/// \brief The Tax class
/// model to present information in list about tax registered in system
/// it's bad name in future we schould rename to 'Tax'
///
class ARTICLES_EXPORT Tax : public QAbstractListModel {
	Q_OBJECT

public:
	Tax(QObject* parent = nullptr);

	int rowCount(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

	void get();

	enum Roles {
		Id = Qt::UserRole + 1
	};

private:
	QList<core::Tax> mList;
};

} // namespace eSawmill

#endif // Tax_H
