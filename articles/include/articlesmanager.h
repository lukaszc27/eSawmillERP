#ifndef ARTICLESMANAGER_H
#define ARTICLESMANAGER_H


#include <QDialog>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QSortFilterProxyModel>
#include <tableview.h>	// moduł widgets
#include <pushbutton.h>	// moduł widgets
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>
#include "models/articles.h"


namespace agent = core::providers::agent;

namespace eSawmill::articles {

///
/// @brief The ArticlesManager class
/// dialog to manage articles existing in system
///
class ARTICLES_EXPORT ArticlesManager : public QDialog {
	Q_OBJECT

public:
	ArticlesManager(QWidget* parent = nullptr, bool selectorMode = false);

	///
	/// @brief article
	/// get current selected article on list
	/// \return
	///
	core::Article article() const;

public Q_SLOTS:
	///
	/// @brief handleRepositoryNotify
	/// update main articles model when repository will be changed
	/// @param event
	///
	void handleRepositoryNotify(const agent::RepositoryEvent& event);

private:
	///@{
	/// additional controls
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemakeButton;
	::widgets::PushButton* mRemoveButton;
	::widgets::PushButton* mCloseButton;

	QComboBox* mColumnSelector;
	QLineEdit* mSearchLine;
	QPushButton* mSearchButton;
	QComboBox* mWarehouseSelector;
	///@}

	///@{
	void createWidgets();
	void createConnections();
	void createModels();
	///@}

	///@{
	/// models used in model
	models::Articles* mArticles;
	QSortFilterProxyModel* mFilterModel;
	///@}

	///@{
	/// variables
	bool mSelectorMode;			///< flag status describe type of manager (selector or standard manager)
	unsigned int mArticleId;	///< id current selected article
	///@}

private Q_SLOTS:
	///
	/// @brief addArticle_Clicked
	/// reaction on add button click signal
	///
	void addArticle_Clicked();

	///
	/// @brief removeArticle_Clicked
	/// reaction on remove button click signal
	///
	void removeArticle_Clicked();

	///
	/// @brief editArticle_ReturnPressed reaction on double clicked on article list
	/// @param index
	///
	void editArticle_ReturnPressed(const QModelIndex &index);

	///
	/// @brief filterRecords
	/// @details filter presenting data by categories (columns)
	///
	void filterRecords();
};

} // namespace eSawmill

#endif // ARTICLESMANAGER_H
