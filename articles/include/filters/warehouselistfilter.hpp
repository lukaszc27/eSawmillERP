#ifndef WAREHOUSELISTFILTER_HPP
#define WAREHOUSELISTFILTER_HPP

#include "../articles_global.h"
#include <QSortFilterProxyModel>


namespace eSawmill::articles::filters {

class WarehouseListFilter : public QSortFilterProxyModel {
	Q_OBJECT

public:
	explicit WarehouseListFilter(QObject* parent = nullptr);
	virtual ~WarehouseListFilter() = default;
};

} // namespace eSawmill::articles::filters

#endif // WAREHOUSELISTFILTER_HPP
