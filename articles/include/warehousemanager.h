#ifndef WAREHOUSEMANAGER_H
#define WAREHOUSEMANAGER_H

#include "articles_global.h"
#include <QDialog>

#include "tableview.h"	// moduł widget
#include "pushbutton.h" // moduł widget
#include "models/warehouses.h"
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill::articles {

///
/// \brief The WarehouseManager class
/// main warehouse manager used by user to create/edit or remove warehouses from system
///
class ARTICLES_EXPORT WarehouseManager : public QDialog {
	Q_OBJECT

public:
	explicit WarehouseManager(QWidget* parent = nullptr);
	virtual ~WarehouseManager() = default;

public Q_SLOTS:
	void handleRepositoryNotify(const agent::RepositoryEvent& event);

private:
	///@{
	/// widgets in dialog
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemakeButton;
	::widgets::PushButton* mCloseButton;
	///@}

	models::Warehouses* mWarehouses;	///< warehouse models used by manager

	///@{
	/// create widgets, models and connections between widgets in dialog
	void createWidgets();
	void createModels();
	void createConnections();
	///@}

private Q_SLOTS:
	/// \brief addButton_Clicked
	/// action for add new warehouse by button
	void addButton_Clicked();

	/// \brief editButton_Clicked
	/// action for edit existing warehouse by button
	void editButton_Clicked();

	/// \brief edit_DoubleClick
	/// edit existing warehouse by double clicked in list
	/// \param index
	void edit_DoubleClick(const QModelIndex& index);

private:
	///
	/// \brief editWarehouse
	/// methods for editing warehouse
	/// \param warehouseId
	///
	void editWarehouse(core::eloquent::DBIDKey warehouseId);
};

} // namespace eSawmill


#endif // WAREHOUSEMANAGER_H
