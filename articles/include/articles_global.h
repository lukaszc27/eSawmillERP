#ifndef ARTICLES_GLOBAL_H
#define ARTICLES_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ARTICLES_LIBRARY)
#  define ARTICLES_EXPORT Q_DECL_EXPORT
#else
#  define ARTICLES_EXPORT Q_DECL_IMPORT
#endif

#endif // ARTICLES_GLOBAL_H
