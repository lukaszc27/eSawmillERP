#include "articlesmanager.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScopedPointer>

#include "models/warehouselist.h"
#include "createarticle.h"
#include "commands/createarticlecommand.hpp"
#include "commands/updatearticlecommand.hpp"
#include "commands/removearticlecommand.hpp"

#include <messagebox.h>
#include <globaldata.hpp>
#include <commandexecutor.hpp>
#include <exceptions/forbiddenexception.hpp>
#include <logs/abstractlogger.hpp>
#include <repository/abstractrepositorylocker.hpp>
#include <memory>

namespace eSawmill::articles {

ArticlesManager::ArticlesManager(QWidget* parent, bool selectorMode)
	: QDialog(parent)
	, mSelectorMode(selectorMode)
	, mArticleId(0)
{
	core::GlobalData::instance().logger().information("Otwarto menadżera artykułów");
    createWidgets();
    createConnections();
    createModels();

	if (selectorMode) {
		setWindowTitle(tr("Wybór artykułu"));
		mRemakeButton->setText(tr("Wybierz"));
		mRemoveButton->setDisabled(true);	// on select mode user can't remove article from list
	}
	else {
		setWindowTitle(tr("Artykuły"));
	}
}

core::Article ArticlesManager::article() const {
	if (mSelectorMode) {
		Q_ASSERT(mArticleId > 0);
		return core::GlobalData::instance().repository().articleRepository().getArticle(mArticleId);
	}
	return core::Article {};
}

void ArticlesManager::handleRepositoryNotify(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::Article) {
		const auto& selection = mTableView->selectionModel()->selection();
		mArticles->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::SelectionFlag::Select);
	}
}

void ArticlesManager::createWidgets()
{
    mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemakeButton = new ::widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
	mCloseButton = new ::widgets::PushButton(tr("Zamknij"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));
    mColumnSelector = new QComboBox(this);
    mWarehouseSelector =  new QComboBox(this);
    mSearchLine = new QLineEdit(this);
	mSearchButton = new QPushButton(QIcon(":/icons/find"), tr("Szukaj"), this);

	mSearchButton->setDefault(true);

    QHBoxLayout* searchLayout = new QHBoxLayout;
    searchLayout->addWidget(mColumnSelector);
    searchLayout->addWidget(mSearchLine);
    searchLayout->addWidget(mSearchButton);
    searchLayout->addWidget(mWarehouseSelector);

    QHBoxLayout* buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(mAddButton);
    buttonLayout->addWidget(mRemakeButton);
    buttonLayout->addWidget(mRemoveButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(mCloseButton);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(searchLayout);
	mainLayout->addSpacing(12);
    mainLayout->addWidget(mTableView);
	mainLayout->addSpacing(24);
    mainLayout->addLayout(buttonLayout);
}

void ArticlesManager::createConnections()
{
    connect(mCloseButton, &::widgets::PushButton::clicked, this, &ArticlesManager::close);
    connect(mAddButton, &::widgets::PushButton::clicked, this, &ArticlesManager::addArticle_Clicked);
    connect(mRemoveButton, &::widgets::PushButton::clicked, this, &ArticlesManager::removeArticle_Clicked);
	connect(mTableView, &::widgets::TableView::returnPressed, this, &ArticlesManager::editArticle_ReturnPressed);
	connect(mSearchButton, &QPushButton::clicked, this, &ArticlesManager::filterRecords);
	connect(mSearchLine, &QLineEdit::returnPressed, this, &ArticlesManager::filterRecords);
	connect(mTableView, &::widgets::TableView::doubleClicked, this, &ArticlesManager::editArticle_ReturnPressed);

	connect(mWarehouseSelector, QOverload<int>::of(&QComboBox::activated), this, [=](int index){
		Q_UNUSED(index);
		mArticles->setCurrentWarehouse(mWarehouseSelector->currentData(models::WarehouseList::Id).toUInt());
	});

	// edit button
	connect(mRemakeButton, &::widgets::PushButton::clicked, this, [&]{
		editArticle_ReturnPressed(mTableView->currentIndex());
	});
}

void ArticlesManager::createModels()
{
    mWarehouseSelector->setModel(new models::WarehouseList(this));
    mArticles = new models::Articles(this);
	mFilterModel = new QSortFilterProxyModel(this);
	mFilterModel->setSourceModel(mArticles);
	mTableView->setModel(mFilterModel);

	mColumnSelector->addItems(mArticles->headerData());
}

//---------------------------------------------
// slots
void ArticlesManager::addArticle_Clicked()
{
	core::GlobalData::instance().logger().information("Dodawanie nowego artykułu");
	try {
    QScopedPointer<CreateArticle> dialog(new CreateArticle(this));
		if (dialog->exec() == QDialog::Accepted) {
			auto command = std::make_unique<commands::CreateArticleCommand>(dialog->article());
			if (!core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Błąd podczas tworzenia nowego artykułu"));

				return;
			}
			mArticles->get();
		}
	} catch (std::exception&) {
		core::GlobalData::instance().logger().warning("Błąd zapisywania obiektu artykułu");
		::widgets::MessageBox::critical(this, tr("Błąd operacji"),
			tr("Podczas operacji tworzenia nowego artykułu wystąpiły błędy!"));
	}
}

void ArticlesManager::removeArticle_Clicked()
{
	if (!mTableView->selectionModel()->hasSelection()) {
		::widgets::MessageBox::warning(this, tr("Informacja"),
			tr("Nie wybrano artykułu do usunięcia"));
		return;
	}

	try {
		const auto articleId = static_cast<core::eloquent::DBIDKey>(
			mTableView->selectionModel()->currentIndex().data(models::Articles::Roles::Id).toUInt());
		Q_ASSERT(articleId > 0);

		auto& repository = core::GlobalData::instance().repository().articleRepository();
		core::repository::ResourceLocker locker {articleId, repository};

		auto ret = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Czy na pewno chcesz usunąć wybrany artykuł?"), "",
			QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::Yes) {
			auto command = std::make_unique<commands::RemoveArticleCommand>(articleId);
			if (!core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
				::widgets::MessageBox::critical(this, tr("Błąd"),
					tr("Błąd podczas usuwania artykułu.\n"
					   "Zmiany w bazie nie zostały zapisane!"));
				return;
			}
			mArticles->get();
		}
	} catch (core::exceptions::ForbiddenException&) {
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Artykuł jest w tym momencie edytowany przez innego użytkownika, poczekaj na zakończenie edycji aby wykonać tą akcję na tym artykule"));
	}
}

void ArticlesManager::editArticle_ReturnPressed(const QModelIndex &index) {
	Q_UNUSED(index);
	if (!mSelectorMode) {
		try {
			if (!mTableView->selectionModel()->hasSelection()) {
				::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
					tr("Nie zaznaczono artykułu do edycji"));
				return;
			}

			core::eloquent::DBIDKey articleId = mTableView->selectionModel()->currentIndex().data(models::Articles::Roles::Id).toUInt();
			Q_ASSERT(articleId > 0);

			auto& repository = core::GlobalData::instance().repository().articleRepository();
			core::repository::ResourceLocker locker {articleId, repository};

			QPointer<CreateArticle> dialog(new CreateArticle{articleId, this});
			if (dialog->exec() == QDialog::Accepted) {
				auto command = std::make_unique<commands::UpdateArticleCommand>(dialog->article());
				if (!core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
					::widgets::MessageBox::critical(this, tr("Błąd"),
						tr("Błąd podczas edycji istniejącego artykułu.\n"
						   "Dane w bazie nie zostały zapisane!"));
					return;
				}
				mArticles->get();
			}
		} catch (core::exceptions::ForbiddenException& ex) {
			Q_UNUSED(ex);
			::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
				tr("Artykuł jest w tym momencie edytowany przez innego użytkownika, poczekaj na zakończenie edycji"));
		}
	} else {
		// choosen article mode
		mArticleId = mTableView->currentIndex().data(static_cast<int>(models::Articles::Roles::Id)).toUInt();
		core::GlobalData::instance().logger().information(QString::asprintf("Wybór artykułu (id: %d)", mArticleId));
		auto& repository = core::GlobalData::instance().repository().articleRepository();
		core::Article currentArticle = repository.getArticle(mArticleId);
		accept();
	}
}

void ArticlesManager::filterRecords()
{
	if (!mSearchLine->text().isEmpty()) {
		mFilterModel->setFilterRegularExpression(QString("(%1)[A-Za-z0-9]*").arg(mSearchLine->text().toUpper()));
		mFilterModel->setFilterKeyColumn(mColumnSelector->currentIndex());
	}
	else {
		mFilterModel->setFilterRegularExpression("");
		mFilterModel->invalidate();
	}
}

} // namespace eSawmill
