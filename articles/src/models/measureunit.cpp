#include "models/measureunit.hpp"
#include "messagebox.h"
#include <globaldata.hpp>

namespace eSawmill::articles::models {

MeasureUnit::MeasureUnit(const ModelType &type, QObject *parent)
	: MeasureUnit {parent}
{
	mType = type;
}

MeasureUnit::MeasureUnit(QObject* parent)
	: QAbstractListModel {parent}
	, mType {ModelType::FullAndShortName}
{
	get();
}

MeasureUnit::~MeasureUnit() {}

int MeasureUnit::rowCount(const QModelIndex &index) const {
	Q_UNUSED(index);

	return mList.size();
}

QVariant MeasureUnit::data(const QModelIndex &index, int role) const {
	int row = index.row();
	core::MeasureUnit unit = mList.at(row);

	switch (role) {
	case Qt::DisplayRole:
		switch (mType) {
		case ModelType::FullAndShortName:
			return QString("%1 (%2)").arg(unit.fullName(), unit.shortName());

		case ModelType::FullName:
			return unit.fullName();

		case ModelType::ShortName:
			return unit.shortName();
		}
		break;

	case Roles::Id:
		return unit.id();

	case Roles::ShortName:
		return unit.shortName();

	case Roles::FullName:
		return unit.fullName();
	}

	return QVariant {};
}

QVariant MeasureUnit::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (role == Qt::DisplayRole && section == 0)
			return QString("ShortName");
		break;

	case Qt::Vertical:
		break;
	}

	return QVariant {};
}

void MeasureUnit::get() {
	try {
		beginResetModel();
		mList.clear();
		mList = core::GlobalData::instance().repository().measureUnitRepository().getAll();
		endResetModel();
	}
	catch (const QString msg) {
		::widgets::MessageBox::critical(nullptr, tr("SQL Error"), msg);
	}
}

} // namespace eSawmill
