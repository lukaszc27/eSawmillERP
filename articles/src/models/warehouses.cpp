#include "models/warehouses.h"
#include <messagebox.h>
#include <globaldata.hpp>

namespace eSawmill::articles::models {

Warehouses::Warehouses(QObject* parent)
	: core::models::AbstractModel<core::Warehouse>{parent}
{
	core::GlobalData::instance().commandExecutor().attachListener(this);
	mHeaders << tr("Nazwa") << tr("Skrót") << tr("Opis");

	get();
}

Warehouses::~Warehouses()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);
}

int Warehouses::rowCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return mList.size();
}

int Warehouses::columnCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return mHeaders.size();
}

QVariant Warehouses::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (role == Qt::DisplayRole)
			return mHeaders.at(section);
		break;

	case Qt::Vertical: break;
	}

	return QVariant();
}

QVariant Warehouses::data(const QModelIndex &index, int role) const {
	int row{0}, col{0};
	if (index.isValid()) {
		row = index.row();
		col = index.column();
	}
	const core::Warehouse& current = mList.at(row);

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::FullName: return current.name();
		case Columns::ShortName: return current.shortcut();
		case Columns::Description: return current.description();
		}
		break;

	case Qt::ForegroundRole:
		if (col == Columns::Description)
			return QColor(Qt::darkGray);
		break;

	case Roles::WarehouseId:
		return mList.at(row).id();
		break;
	}

	return QVariant();
}

void Warehouses::createOrUpdate(core::Warehouse warehouse) {
	Q_UNUSED(warehouse);
}

void Warehouses::get() {
	beginResetModel();
	try {
		mList.clear();	// remove old data from model
		mList = core::GlobalData::instance()
			.repository()
			.warehousesRepository()
			.getAll();
	} catch (...) {
		throw;
	}
	endResetModel();
}

void Warehouses::destroy(core::Warehouse warehouse) {
	Q_UNUSED(warehouse);
}

void Warehouses::commandExecuted()
{
	get();
}

} // namespace eSawmill
