#include "models/tax.h"
#include <messagebox.h>
#include <globaldata.hpp>
#include <exceptions/repositoryexception.hpp>


namespace eSawmill::articles::models {

Tax::Tax(QObject* parent)
	: QAbstractListModel(parent)
{
	get();
}

int Tax::rowCount(const QModelIndex &index) const
{
	if (index.isValid())
		return 0;

	return mList.size();
}

QVariant Tax::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal) {
		switch (role) {
		case Qt::DisplayRole:
			if (section == 0)
				return QString("Wartość");
			break;
		}
	}
	return QVariant();
}

QVariant Tax::data(const QModelIndex &index, int role) const {
	const core::Tax& current = mList[index.row()];
	switch (role) {
	case Qt::DisplayRole:
		return current.value();
		break;

	case Roles::Id:
		return current.id();
		break;
	}
	return QVariant();
}

void Tax::get() {
	beginResetModel();
	mList.clear();
	try {
		mList = core::GlobalData::instance().repository().TaxRepository().getAll();
	} catch (const QString &msg) {
		Q_UNUSED(msg);
		throw core::exceptions::RepositoryException();
	}
	endResetModel();
}

} // namespace eSawmill
