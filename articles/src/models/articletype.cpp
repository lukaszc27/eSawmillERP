#include "models/articletype.h"
#include <messagebox.h>
#include <globaldata.hpp>

namespace eSawmill::articles::models {

ArticleType::ArticleType(QObject* parent)
	: QAbstractListModel(parent)
{
	get();
}

int ArticleType::rowCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return mList.size();
}

QVariant ArticleType::headerData(int section, Qt::Orientation orientation, int role) const {
	if (orientation == Qt::Horizontal) {
		switch (role) {
		case Qt::DisplayRole:
			if (section == 0)
				return QString(tr("Typ artykułu"));
			break;
		}
	}
	return QVariant();
}

QVariant ArticleType::data(const QModelIndex &index, int role) const {
	switch (role) {
	case Qt::DisplayRole:
		return mList.at(index.row()).name();
		break;

	case Roles::Id:
		return mList.at(index.row()).id();
		break;
	}
	return QVariant {};
}

void ArticleType::get() {
	mList.clear();
	try {
		beginResetModel();
		mList.clear();
		mList = core::GlobalData::instance().repository().articleRepository().getArticleTypes();
		endResetModel();
	}
	catch (const QString &msg) {
		::widgets::MessageBox::critical(0, tr("Błąd"),
			tr("Podczas pobierania danych z bazy wystąpiły błędy"), msg);
	}
}

} // namespace eSawmill
