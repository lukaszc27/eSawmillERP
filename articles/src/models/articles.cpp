#include "models/articles.h"
#include "dbo/contractor.h"
#include "eloquent/builder.hpp"
#include <messagebox.h>
#include <globaldata.hpp>

namespace eSawmill::articles::models {

Articles::Articles(QObject* parent)
	: core::models::AbstractModel<core::Article>(parent)
	, mWarehouseId(1)
{
	core::GlobalData::instance().commandExecutor().attachListener(this);
	get();

	// load articles after warehouse chage
	connect(this, QOverload<int>::of(&Articles::warehouseChanged), this, [this](int warehouse){
		Q_UNUSED(warehouse);
		get();
	});
}

Articles::~Articles()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);
}

QStringList Articles::headerData() const {
	return QStringList()
			<< tr("Nazwa")
			<< tr("Kod kereskowy")
			<< tr("Ilość")
			<< tr("jm")
			<< tr("Dostawca")
			<< tr("Cena zakupu netto")
			<< tr("Cena zakupu brutto")
			<< tr("VAT zakupu")
			<< tr("Cena sprzedaży netto")
			<< tr("Cena sprzedaży brutto")
			<< tr("VAT sprzedaży");
}

int Articles::rowCount(const QModelIndex &index) const
{
	if (index.isValid())
		return 0;

	return items().size();
}

int Articles::columnCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return headerData().size();
}

QVariant Articles::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (role == Qt::DisplayRole)
			return headerData().at(section);
		break;

	case Qt::Vertical:
		if (role == Qt::DisplayRole)
			return QString::number(section + 1);
		break;
	}
	return QVariant {};
}

QVariant Articles::data(const QModelIndex &index, int role) const {
	int row = 0, col = 0;
	if (index.isValid()) {
		row = index.row();
		col = index.column();
	}
	core::Article current = items().at(row);

	switch (role) {
	case Qt::DisplayRole:
		switch (col) {
		case Columns::Name: return current.name();
		case Columns::BarCode: return current.barCode();
		case Columns::Quantity: return current.quantity();
		case Columns::MeasureUnit: return current.unit().shortName();
		case Columns::Provider: {
			core::Contractor provider = current.provider();
			if (provider.name().isEmpty()) {
				return QString("%1 %2")
						.arg(provider.personName(),
							provider.personSurname());
			}
			return provider.name();
		}
		case Columns::PurchaseTax: return current.purchaseTax().value();
		case Columns::SaleTax: return current.saleTax().value();
		case Columns::PricePurchaseNetto: return current.pricePurchaseNetto();
		case Columns::PricePurchaseBrutto: return current.pricePurchaseBrutto();
		case Columns::PriceSaleNetto: return current.priceSaleNetto();
		case Columns::PriceSaleBrutto: return current.priceSaleBrutto();
		}
		break;

	case Qt::ForegroundRole:
		if (current.quantity() <= 0)
			return QBrush(Qt::gray);
		break;

	case Roles::Id:
		return current.id();
	}
	return QVariant();
}

void Articles::createOrUpdate(core::Article article) {
	Q_UNUSED(article);
}

void Articles::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance().repository().articleRepository().getAll(currentWarehouse());
		endResetModel();
	}
	catch (const QString& msg) {
		Q_UNUSED(msg);
		// now in models we don't show information (message box)
		// only mark error in log file and rethrow exception up
		core::GlobalData::instance().logger().warning("Błąd pobierania artykułów");
		throw;
	}
}

void Articles::destroy(core::Article article) {
	Q_UNUSED(article);
}

void Articles::commandExecuted()
{
	get();
}

} // namespace eSawmill
