#include "models/warehouselist.h"
#include <QMessageBox>
#include <globaldata.hpp>

namespace eSawmill::articles::models {

WarehouseList::WarehouseList(QObject* parent, Format format)
	: QAbstractListModel {parent}
	, mFormat {format}
{
	core::GlobalData::instance().commandExecutor().attachListener(this);
	get();
}

WarehouseList::~WarehouseList()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);
}

void WarehouseList::get() {
	try {
		beginResetModel();
		mList = core::GlobalData::instance().repository().warehousesRepository().getAll();
		endResetModel();
	}
	catch (const QString& msg) {
		QMessageBox::critical(0, tr("Błąd"), msg);
	}
}

void WarehouseList::commandExecuted()
{
	get();
}

int WarehouseList::rowCount(const QModelIndex &index) const {
	if (index.isValid())
		return 0;

	return mList.size();
}

QVariant WarehouseList::headerData(int section, Qt::Orientation orientation, int role) const {
	switch (orientation) {
	case Qt::Horizontal:
		if (section == 0 && role == Qt::DisplayRole)
			return tr("Magazyny");
		break;

	case Qt::Vertical: break;
	}
	return QVariant();
}

QVariant WarehouseList::data(const QModelIndex &index, int role) const {
	core::Warehouse current = mList.at(index.row());

	switch (role) {
	case Qt::DisplayRole: {
		if (mFormat == Format::All)
			return QString("%1 (%2)").arg(current.name(), current.shortcut());
		else if (mFormat == Format::FullName)
			return current.name();
		else if (mFormat == Format::ShortName)
			return current.shortcut();
	} break; // Qt::DisplayRole
	case Roles::Id:
		return mList.at(index.row()).id();
		break;
	}
	return QVariant();
}

} // namespace eSawmill
