#include "warehousemanager.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScopedPointer>

#include <messagebox.h>
#include "createwarehouse.h"
//#include "dbo/wareHouse.h"
#include "filters/warehouselistfilter.hpp"
#include "commands/createwarehousecommand.hpp"
#include "commands/updatewarehousecommand.hpp"

#include <globaldata.hpp>
#include <commandexecutor.hpp>
#include <exceptions/forbiddenexception.hpp>
#include <repository/abstractrepositorylocker.hpp>

namespace eSawmill::articles {

WarehouseManager::WarehouseManager(QWidget* parent)
	: QDialog(parent)
{
	core::GlobalData::instance().logger().information("Otwarto menadżera magazynów");
	setWindowTitle(tr("Magazyny"));
	createWidgets();
	createModels();
	createConnections();
}

void WarehouseManager::handleRepositoryNotify(const agent::RepositoryEvent& event) {
	if (event.resourceType == agent::Resource::Warehouse) {
		const auto& selection = mTableView->selectionModel()->selection();
		mWarehouses->get();
		mTableView->selectionModel()->select(selection, QItemSelectionModel::Select);
	}
}

void WarehouseManager::createWidgets() {
	mTableView = new widgets::TableView(this);
	mAddButton = new widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemakeButton = new widgets::PushButton(tr("Popraw"), QKeySequence("F2"), this, QIcon(":/icons/edit"));
	mCloseButton = new widgets::PushButton(tr("Zamknij"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(mAddButton);
	buttonLayout->addWidget(mRemakeButton);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mCloseButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTableView);
	mainLayout->addLayout(buttonLayout);
}

void WarehouseManager::createModels() {
	mWarehouses = new models::Warehouses(this);
	auto filter = new filters::WarehouseListFilter(this);
	filter->setSourceModel(mWarehouses);

	mTableView->setModel(filter);
}

void WarehouseManager::createConnections() {
	connect(mAddButton, &widgets::PushButton::clicked, this, &WarehouseManager::addButton_Clicked);
	connect(mRemakeButton, &widgets::PushButton::clicked, this, &WarehouseManager::editButton_Clicked);
	connect(mTableView, &widgets::TableView::doubleClicked, this, &WarehouseManager::edit_DoubleClick);
	connect(mCloseButton, &widgets::PushButton::clicked, this, &WarehouseManager::reject);
}

void WarehouseManager::addButton_Clicked() {
	QPointer<CreateWarehouse> dialog(new CreateWarehouse(this));
	if (dialog->exec() == QDialog::Accepted) {
		auto command = std::make_unique<commands::CreateWarehouseCommand>(dialog->warehouse());
		if (core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
			mWarehouses->get();
		}
	}
}

void WarehouseManager::editButton_Clicked() {
	if (!mTableView->selectionModel()->selectedRows().size()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Przed wykonaniem tej operacji musisz zaznaczyć który wiersz edytujesz"));
		return;
	}
	edit_DoubleClick(mTableView->currentIndex());
}

void WarehouseManager::edit_DoubleClick(const QModelIndex& index) {
	core::eloquent::DBIDKey id = index.data(eSawmill::articles::models::Warehouses::Roles::WarehouseId).toInt();
	Q_ASSERT(id > 0);
	if (id <= 0) {
		::widgets::MessageBox::warning(this, tr("Błąd"),
			tr("Nieprawidłowy identyfikator magazynu!\n"
			   "Operacja edycji nie może być kontynuowana"));
		return;
	}
	editWarehouse(id);
}

void WarehouseManager::editWarehouse(core::eloquent::DBIDKey warehouseId) {
	Q_ASSERT(warehouseId > 0);	// id can't be less or equal zero!!!
	try {
		auto& repository = core::GlobalData::instance().repository().warehousesRepository();
		core::repository::ResourceLocker locker(warehouseId, repository);

		QPointer<CreateWarehouse> dialog(new CreateWarehouse(warehouseId, this));
		if (dialog->exec() == CreateWarehouse::Accepted) {
			auto command = std::make_unique<commands::UpdateWarehouseCommand>(dialog->warehouse());
			if (core::GlobalData::instance().commandExecutor().execute(std::move(command))) {
				mWarehouses->get();
			}
		}
	} catch (core::exceptions::ForbiddenException& ex) {
		Q_UNUSED(ex);
		::widgets::MessageBox::warning(this, tr("Blokada dostępu"),
			tr("Wybrany magazyn jest edytowany w tym momencie przez innego użytkownika, poczekaj na zakończenie edycji"));
	} catch (...) {
		core::GlobalData::instance().logger().critical("Podczas edycji magazynu wystąpił nieoczekiwany błąd");
	}
}

} // namespace eSawmill
