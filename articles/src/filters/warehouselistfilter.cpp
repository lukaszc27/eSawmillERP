#include "filters/warehouselistfilter.hpp"

namespace eSawmill::articles::filters {

WarehouseListFilter::WarehouseListFilter(QObject* parent)
	: QSortFilterProxyModel {parent}
{
}

} // namespace eSawmill::articles::filters
