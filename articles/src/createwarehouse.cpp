#include "createwarehouse.h"
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <globaldata.hpp>

namespace eSawmill::articles {

CreateWarehouse::CreateWarehouse(QWidget *parent)
	: QDialog(parent)
	, mWarehouseId(0)
{
	createWidgets();

	connect(mAcceptButton, &widgets::PushButton::clicked, this, &CreateWarehouse::accept);
	connect(mRejectButton, &widgets::PushButton::clicked, this, &CreateWarehouse::reject);

	setWindowTitle(tr("Tworzenie magazynu"));
}

CreateWarehouse::CreateWarehouse(int id, QWidget *parent)
	: CreateWarehouse(parent)
{
	mWarehouseId = id;

	setWindowTitle(tr("Aktualizacja magazynu"));

	const core::Warehouse warehouse = core::GlobalData::instance().repository().warehousesRepository().getWarehouse(id);
	mFullName->setText(warehouse.name());
	mShortName->setText(warehouse.shortcut());
	mDescription->document()->setPlainText(warehouse.description());
}

core::Warehouse CreateWarehouse::warehouse()
{
	core::Warehouse w;
	w.setName(mFullName->text());
	w.setShortcut(mShortName->text());
	w.setDescription(mDescription->document()->toPlainText());
	w.setId(mWarehouseId);
	return w;
}

void CreateWarehouse::createWidgets()
{
	mFullName = new QLineEdit(this);
	mShortName = new QLineEdit(this);
	mDescription = new QPlainTextEdit(this);
	mAcceptButton = new widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mAcceptButton);
	buttonLayout->addWidget(mRejectButton);

	QFormLayout* formLayout = new QFormLayout;
	formLayout->addRow(tr("Pełna nazwa"), mFullName);
	formLayout->addRow(tr("Iniciały"), mShortName);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(formLayout);
	mainLayout->addWidget(mDescription);
	mainLayout->addLayout(buttonLayout);
}

}
