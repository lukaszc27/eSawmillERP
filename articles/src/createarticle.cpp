#include "createarticle.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>

#include "models/articletype.h"
#include "models/tax.h"
#include "models/warehouselist.h"
#include "models/measureunit.hpp"
#include <dbo/auth.h>					/// logowanie w programie
#include <messagebox.h>
#include <globaldata.hpp>

namespace eSawmill::articles {

CreateArticle::CreateArticle(QWidget* parent)
	: QDialog {parent}
	, mCurrentArticleId {0}
{
	setWindowTitle(tr("Dodawanie artykułu"));

	createWidgets();
	createValidators();
	createConnections();
	createModels();
}

CreateArticle::CreateArticle(core::eloquent::DBIDKey id, QWidget* parent)
	: CreateArticle {parent}
{
	if (id < 0)
		throw std::invalid_argument("id can't be less or equal zero!!!");

	mCurrentArticleId = id;

	setWindowTitle(tr("Aktualizacja artykułu"));

	const core::Article article = core::GlobalData::instance().repository().articleRepository().getArticle(id);
	mName->setText(article.name());
	mBarcode->setText(article.barCode());
	mProviderSelector->selectContractor(article.provider());
	mQuantity->setValue(article.quantity());
	mPricePurchaseNetto->setValue(article.pricePurchaseNetto());
	mPricePurchaseBrutto->setValue(article.pricePurchaseBrutto());
	mPriceSaleNetto->setValue(article.priceSaleNetto());
	mPriceSaleBrutto->setValue(article.priceSaleBrutto());
	mSaleTax->setCurrentText(QString::number(article.saleTax().value()));
	mPurchaseTax->setCurrentText(QString::number(article.purchaseTax().value()));
	mArticleType->setCurrentText(article.articleType().name());
	mMeasureUnit->setCurrentText(QString("%1 (%2)").arg(article.unit().fullName(), article.unit().shortName()));
	mWarehouse->setCurrentText(article.warehouse().name());
}

void CreateArticle::createWidgets()
{
	mGeneralGroup = new QGroupBox(tr("Ogólne"), this);
	mPriceGroup = new QGroupBox(tr("Cena"), this);
	mAcceptButton = new ::widgets::PushButton(tr("Zatwierdź"), QKeySequence("F10"), this, QIcon(":/icons/add"));
	mRejectButton = new ::widgets::PushButton(tr("Anuluj"), QKeySequence("ESC"), this, QIcon(":/icons/cancel"));

	QVBoxLayout* generalLoyout = new QVBoxLayout(mGeneralGroup);
	generalLoyout->addWidget(createGeneralWidget());

	QVBoxLayout* priceLayout = new QVBoxLayout(mPriceGroup);
	priceLayout->addWidget(createPriceWidget());

	QHBoxLayout* groupLayout = new QHBoxLayout;
	groupLayout->addWidget(mGeneralGroup);
	groupLayout->addSpacing(6);
	groupLayout->addWidget(mPriceGroup);

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mAcceptButton);
	buttonLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(groupLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonLayout);
}

void CreateArticle::createModels()
{
	mArticleType->setModel(new models::ArticleType(this));
	mPurchaseTax->setModel(new models::Tax(this));
	mSaleTax->setModel(new models::Tax(this));
	mWarehouse->setModel(new models::WarehouseList(this));
	mMeasureUnit->setModel(new models::MeasureUnit(this));
}

void CreateArticle::createValidators()
{
	QRegularExpressionValidator* barcodeValidator = new QRegularExpressionValidator(QRegularExpression("[0-9]{8,14}"), this);
	mBarcode->setValidator(barcodeValidator);

	QRegularExpressionValidator* nameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9- ]+"), this);
	mName->setValidator(nameValidator);
}

void CreateArticle::createConnections()
{
	connect(mAcceptButton, &::widgets::PushButton::clicked, this, &CreateArticle::accept);
	connect(mRejectButton, &::widgets::PushButton::clicked, this, &CreateArticle::reject);

	connect(mPricePurchaseNetto, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
			this, &CreateArticle::pricePurchaseNetto_changed);
	connect(mPriceSaleNetto, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
			this, &CreateArticle::priceSaleNetto_changed);

	connect(mBarcode, &QLineEdit::editingFinished, this, &CreateArticle::checkBarcode);
}

QWidget* CreateArticle::createGeneralWidget()
{
	QWidget* widget = new QWidget(this);

	mName = new QLineEdit(widget);
	mBarcode = new QLineEdit(widget);
	mArticleType = new QComboBox(widget);
	mProviderSelector = new eSawmill::contractors::widgets::ContractorSelector(this);
	mWarehouse = new QComboBox(widget);
	mMeasureUnit = new QComboBox(widget);
	mQuantity = new QSpinBox(this);

	QFormLayout* formLayout = new QFormLayout(widget);
	formLayout->addRow(tr("Nazwa"), mName);
	formLayout->addRow(tr("Kod kreskowy"), mBarcode);
	formLayout->addRow(tr("Typ artykułu"), mArticleType);
	formLayout->addRow(tr("Dostawca"), mProviderSelector);
	formLayout->addRow(tr("Magazyn"), mWarehouse);
	formLayout->addRow(tr("Ilość"), mQuantity);
	formLayout->addRow(tr("Jednostka miary"), mMeasureUnit);

	return widget;
}

QWidget* CreateArticle::createPriceWidget()
{
	QWidget* widget = new QWidget(this);

	mPurchaseTax = new QComboBox(widget);
	mSaleTax = new QComboBox(widget);
	mPricePurchaseNetto = new QDoubleSpinBox(widget);
	mPricePurchaseBrutto = new QDoubleSpinBox(widget);
	mPriceSaleNetto = new QDoubleSpinBox(widget);
	mPriceSaleBrutto = new QDoubleSpinBox(widget);

	for (auto& priceField : findChildren<QDoubleSpinBox*>()) {
		priceField->setMaximum(100000);
	}

	auto createLayoutWithLabel = [widget](QWidget* field, const QString& label = "PLN") -> QHBoxLayout* {
		QHBoxLayout* layout = new QHBoxLayout;
		layout->addWidget(field);
		layout->addWidget(new QLabel(label, widget));
		return layout;
	};

	QFormLayout* formLayout = new QFormLayout(widget);
	formLayout->addRow(tr("VAT zakupu"), createLayoutWithLabel(mPurchaseTax, "%"));
	formLayout->addRow(tr("VAT sprzedaży"), createLayoutWithLabel(mSaleTax, "%"));
	formLayout->addRow(tr("Cena zakupu netto"), createLayoutWithLabel(mPricePurchaseNetto));
	formLayout->addRow(tr("Cena zakupu brutto"), createLayoutWithLabel(mPricePurchaseBrutto));
	formLayout->addRow(tr("Cena sprzedaży netto"), createLayoutWithLabel(mPriceSaleNetto));
	formLayout->addRow(tr("Cena sprzedaży brutto"), createLayoutWithLabel(mPriceSaleBrutto));

	return widget;
}

void CreateArticle::priceSaleNetto_changed(double value)
{
	const auto& tax = core::GlobalData::instance()
		.repository()
		.TaxRepository()
		.get(mSaleTax->currentData(models::Tax::Id).toInt());

	double taxVal = (100.f + tax.value()) / 100.f;
	mPriceSaleBrutto->setValue(taxVal * value);
}

void CreateArticle::checkBarcode()
{
	const QString& text = mBarcode->text();
	const int length = text.length();

	// zamiana tekstu na liczby
	int* barcode = new int[length];
	int sum {0};
	for (int i {0}; i < length; i++)
		barcode[i] = text.at(i).unicode() - QChar('0').unicode();

	for (int i = 0; i < length - 1; i++) {
		if ((i + 1) % 2 == 0) {
			// pozycje parzyste
			sum += barcode[i];
		}
		else {
			// pozycje nie parzyste
			// mnożenie razy 3
			sum += barcode[i] * 3;
		}
	}
	int CRC = 10 - (sum % 10);
	if (CRC != barcode[length - 1]) {
		::widgets::MessageBox::warning(this, tr("Błąd EAN"),
			tr("Wprowadzony Kod kreskowy jest nieprawidłowy!"));

		mBarcode->selectAll();
		mBarcode->setFocus();
	}

	delete[] barcode;
}

void CreateArticle::pricePurchaseNetto_changed(double value)
{
	const auto& tax = core::GlobalData::instance()
		.repository()
		.TaxRepository()
		.get(mPurchaseTax->currentData(models::Tax::Id).toInt());
	double taxVal = (100.f + tax.value()) / 100.f;
	mPricePurchaseBrutto->setValue(taxVal * value);
}

core::Article CreateArticle::article() const
{
	auto& repo = core::GlobalData::instance().repository();
	core::Article article {};
	article.setId(mCurrentArticleId);
	article.setName(mName->text());
	article.setBarCode(mBarcode->text());
	article.setProvider(mProviderSelector->contractor());
	article.setQuantity(mQuantity->value());
	article.setPriceSaleBrutto(mPriceSaleBrutto->value());
	article.setPriceSaleNetto(mPriceSaleNetto->value());
	article.setPricePurchaseBrutto(mPricePurchaseBrutto->value());
	article.setPricePurchaseNetto(mPricePurchaseNetto->value());
	article.setUser(repo.userRepository().autorizeUser());
	article.setCompany(repo.userRepository().autorizeUser().company());
	article.setSaleTax(repo.TaxRepository().get(mSaleTax->currentData(models::Tax::Id).toUInt()));
	article.setPurchaseTax(repo.TaxRepository().get(mPurchaseTax->currentData(models::Tax::Id).toUInt()));
	article.setUnit(repo.measureUnitRepository().get(mMeasureUnit->currentData(models::MeasureUnit::Id).toUInt()));
	article.setWarehouse(repo.warehousesRepository().getWarehouse(mWarehouse->currentData(models::WarehouseList::Id).toUInt()));
	article.setArticleType(repo.articleRepository().getArticleType(mArticleType->currentData(models::ArticleType::Id).toUInt()));
	return article;
}

} // namespace eSawmill
