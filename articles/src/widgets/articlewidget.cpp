#include "widgets/articlewidget.h"
#include "articlesmanager.h"
#include <dbo/service.h>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScopedPointer>
#include <QInputDialog>

#include <dbo/orderarticle.h>
#include <messagebox.h>
#include <globaldata.hpp>


namespace eSawmill::articles::widgets {

ArticleWidget::ArticleWidget(core::models::AbstractModel<core::Article>* model, QWidget *parent)
    : QWidget(parent)
{
    mModel = model;
//	connect(model, &core::models::AbstractModel::listDataChanged, this, &ArticleWidget::articlesChanged);

    createWidgets();
    createModels();
    createConnections();

	//    emit articlesChanged();
}

void ArticleWidget::setOrder(core::eloquent::DBIDKey orderId) {
	Q_ASSERT(orderId > 0);
	mModel->setParentObjectId(orderId);
	mModel->get();
}

void ArticleWidget::load(const core::Order& order) {
	Q_ASSERT(order.id() > 0);
	auto& repository = core::GlobalData::instance().repository().orderRepository();
	QList<core::Article> articles = repository.getArticlesForOrder(order.id());
	mModel->insert(articles);
	notifyArticlesChanged();
}

void ArticleWidget::load(const core::Service& service) {
	Q_ASSERT(service.id() > 0);
	auto& repository = core::GlobalData::instance().repository().serviceRepository();
	QList<core::Article> articles = repository.getServiceArticles(service.id());
	mModel->insert(articles);
	notifyArticlesChanged();
}

void ArticleWidget::insert(QList<core::Article> articles)
{
    mModel->insert(articles);
	notifyArticlesChanged();
}

void ArticleWidget::setReadOnly(bool readonly)
{
    mArticleAddButton->setEnabled(!readonly);
    mArticleRemoveButton->setEnabled(!readonly);
    mArticleView->setEnabled(!readonly);
}

void ArticleWidget::createWidgets()
{
    mArticleColumnSelector = new QComboBox(this);
    mArticleSearchLine = new QLineEdit(this);
    mArticleSearchButton = new QPushButton(QIcon(":/icons/find"), tr("Szukaj"), this);
    mArticleView = new ::widgets::TableView(this);
    mArticleAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
    mArticleRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("DEL"), this, QIcon(":/icons/trash"));
    mTotalArticlesLabel = new QLabel(this);

    mTotalArticlesLabel->setAlignment(Qt::AlignRight);

    QHBoxLayout* findLayout = new QHBoxLayout;
    findLayout->addWidget(mArticleColumnSelector);
    findLayout->addWidget(mArticleSearchLine);
    findLayout->addWidget(mArticleSearchButton);

    QHBoxLayout* buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(mArticleAddButton);
    buttonsLayout->addWidget(mArticleRemoveButton);
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(mTotalArticlesLabel);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(findLayout);
    mainLayout->addWidget(mArticleView);
    mainLayout->addLayout(buttonsLayout);
}

void ArticleWidget::createConnections()
{
	connect(mArticleAddButton, &::widgets::PushButton::clicked, this, &ArticleWidget::addArticle_clicked);
	connect(mArticleSearchButton, &QPushButton::clicked, this, &ArticleWidget::filterArticle_clicked);
	connect(mArticleSearchLine, &QLineEdit::returnPressed, this, &ArticleWidget::filterArticle_clicked);
	connect(mArticleRemoveButton, &::widgets::PushButton::clicked, this, &ArticleWidget::removeArticle_clicked);
}

void ArticleWidget::createModels() {
	mFilterModel = new QSortFilterProxyModel(this);
	mFilterModel->setSourceModel(mModel);

	mArticleView->setModel(mFilterModel);
	mArticleColumnSelector->addItems(mModel->headerData());
}

void ArticleWidget::addArticle_clicked() {
	QScopedPointer<eSawmill::articles::ArticlesManager> manager(new eSawmill::articles::ArticlesManager(this, true));
	if (manager->exec() == QDialog::Accepted) {
		int qty = QInputDialog::getInt(this, tr("Ilość"), tr("Ilość w zamówieniu"), 1, 0, 1000, 1, nullptr, Qt::Sheet);

		core::Article article = manager->article();
		article.setQuantity(qty);
		mModel->insert(article);
		notifyArticlesChanged();
	}
}

void ArticleWidget::filterArticle_clicked()
{
    if (!mArticleSearchLine->text().isEmpty()) {
        mFilterModel->setFilterKeyColumn(mArticleColumnSelector->currentIndex());
		mFilterModel->setFilterRegularExpression(QString("(%1)[0-9A-Za-z]*").arg(mArticleSearchLine->text().toUpper()));
    }
    else {
		mFilterModel->setFilterRegularExpression("");
        mFilterModel->invalidate();
    }
}

void ArticleWidget::removeArticle_clicked() {
	auto rows = mArticleView->selectionModel()->selectedRows();
	if (rows.length() <= 0) {
		::widgets::MessageBox::information(this, "Informacja",
			"Nie zaznaczono obiektów do usunięcia");
		return;
	}

	int result = ::widgets::MessageBox::question(this, "Pytanie",
		QString("Zaznaczono %1 elementów. Czy chcesz je usunąć z listy?").arg(rows.length()), "",
		QMessageBox::Yes | QMessageBox::No);

	if (result == QMessageBox::Yes) {
		for (auto row : rows)
			mModel->setData(row, true, Qt::CheckStateRole);

		// remove selected items from list
		mModel->deleteSelectedItems();
		notifyArticlesChanged();
	}
}

void ArticleWidget::notifyArticlesChanged() {
	double total {0};
	for (auto& item : mModel->items()) {
		total += (item.priceSaleBrutto() * item.quantity());
	}
	mTotalArticlesLabel->setText(QString::asprintf("Wartość: %0.2f PLN", total));
	mTotalArticlesLabel->setVisible(total > 0);
	Q_EMIT articlesChanged();
}

} // namespace eSawmill
