#include "commands/removearticlecommand.hpp"
#include <repository/abstractrepositorylocker.hpp>

namespace eSawmill::articles::commands {

RemoveArticleCommand::RemoveArticleCommand(core::eloquent::DBIDKey articleId)
	: articleId_ {articleId}
{
}

bool RemoveArticleCommand::execute(core::repository::AbstractRepository& repository) {
	Q_ASSERT(articleId_ > 0);
	core::repository::ResourceLocker locker {articleId_, repository.articleRepository()};

	removedArticle_ = repository
		.articleRepository()
		.getArticle(articleId_);

	return repository
		.articleRepository()
			.destroyArticle(articleId_);
}

bool RemoveArticleCommand::undo(core::repository::AbstractRepository& repository) {
	removedArticle_.setId(core::eloquent::NullDBIDKey);

	return repository
		.articleRepository()
		.createArticle(removedArticle_);
}

} // namespace eSawmill::articles::commands
