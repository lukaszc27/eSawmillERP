#include "commands/createarticlecommand.hpp"

namespace eSawmill::articles::commands {

CreateArticleCommand::CreateArticleCommand(const core::Article& article)
	: article_ {article}
	, insertedArticleId_ {core::eloquent::NullDBIDKey}
{
}

bool CreateArticleCommand::execute(core::repository::AbstractRepository& repository) {
	bool success = repository.articleRepository().createArticle(article_);
	if (success) {
		insertedArticleId_ = article_.id();
		Q_ASSERT(insertedArticleId_ > 0);
	}
	return success;
}

bool CreateArticleCommand::undo(core::repository::AbstractRepository& repository) {
	return repository.articleRepository().destroyArticle(insertedArticleId_);
}

} // namespace eSawmill::articles::commands
