#include "commands/updatearticlecommand.hpp"

namespace eSawmill::articles::commands {

UpdateArticleCommand::UpdateArticleCommand(const core::Article& article)
	: article_ {article}
{
}

bool UpdateArticleCommand::execute(core::repository::AbstractRepository& repository) {
	Q_ASSERT(article_.id() > 0);
	beforeUpdateArticle_ = repository
		.articleRepository()
		.getArticle(article_.id());

	beforeUpdateArticle_.makeDirty();

	return repository
		.articleRepository()
		.updateArticle(article_);
}

bool UpdateArticleCommand::undo(core::repository::AbstractRepository& repository) {
	return repository
		.articleRepository()
		.updateArticle(beforeUpdateArticle_);
}

} // namespace eSawmill::articless:commands
