#include "commands/createwarehousecommand.hpp"

namespace eSawmill::articles::commands {

CreateWarehouseCommand::CreateWarehouseCommand(const core::Warehouse& warehouse)
	: warehouse_{warehouse}
{
}

bool CreateWarehouseCommand::execute(core::repository::AbstractRepository& repository) {
	bool success = repository.warehousesRepository().create(warehouse_);
	Q_ASSERT(warehouse_.id() > 0);

	return success;
}

bool CreateWarehouseCommand::undo(core::repository::AbstractRepository& repository) {
	Q_ASSERT(warehouse_.id() > 0);
	return repository.warehousesRepository().destroy(warehouse_.id());
}



} // namespace eSawmill::articles::commands
