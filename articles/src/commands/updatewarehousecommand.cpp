#include "commands/updatewarehousecommand.hpp"

namespace eSawmill::articles::commands {

UpdateWarehouseCommand::UpdateWarehouseCommand(const core::Warehouse& warehouse)
	: warehouse_{warehouse}
{
}

bool UpdateWarehouseCommand::execute(core::repository::AbstractRepository& repository) {
	Q_ASSERT(warehouse_.id() > 0);
	lastWarehouse_ = repository.warehousesRepository().getWarehouse(warehouse_.id());
	lastWarehouse_.makeDirty();
	return repository.warehousesRepository().update(warehouse_);
}

bool UpdateWarehouseCommand::undo(core::repository::AbstractRepository& repository) {
	return repository.warehousesRepository().update(lastWarehouse_);
}

} // namespace eSawmill::articles::commands
