#ifndef STATUSBAR_HPP
#define STATUSBAR_HPP

#include <QStatusBar>
#include <QLabel>
#include <QPushButton>


namespace eSawmill {

class StatusBar : public QStatusBar {
	Q_OBJECT

public:
	explicit StatusBar(QWidget* parent = nullptr);

	inline void setStatusMessage(const QString& msg) { mStatusTip->setText(msg); }
	inline QString statusMessage() const { return mStatusTip->text(); }

	inline void setRegisterFor(const QString& msg) { mRegisterFor->setText(tr("Zarejestrowano dla: %1").arg(msg)); }

private:
	QLabel* mStatusTip;
	QLabel* mRegisterFor;
	QPushButton* mUpdateButton;
};

}

#endif // STATUSBAR_HPP
