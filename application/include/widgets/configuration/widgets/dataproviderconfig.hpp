#ifndef DATAPROVIDERCONFIG_H
#define DATAPROVIDERCONFIG_H

#include <configurationwidget.h>
#include <QStackedWidget>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QCheckBox>
#include <QPushButton>
#include <QSpinBox>

namespace eSawmill::config {

class DatabaseConnectionConfig;
class RemoteConnectionConfig;

///
/// \brief The DataProviderConfig class
/// widget uses to configure type of connection
/// to data provider displaied in main app configuration window
///
class DataProviderConfig : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	explicit DataProviderConfig(QWidget* parent = nullptr);
	virtual ~DataProviderConfig() = default;

	virtual void initialize() override;
	virtual void save() override;

	enum DataProvider {
		Database = 0,
		Remote
	};

private:
	void createAllWidgets();
	void createConnections();

	QRadioButton* mRemoteConnection;
	QRadioButton* mDatabaseConnection;
	QStackedWidget* mStacked;
	QLabel* mInfoFrame;

	// additional configuration widgets
	DatabaseConnectionConfig* mDatabaseConfig;
	RemoteConnectionConfig* mRemoteConfig;
};


///
/// \brief The DatabaseConnectionConfig class
/// widget to configure connection string to database
///
class DatabaseConnectionConfig : public QWidget {
	Q_OBJECT

public:
	explicit DatabaseConnectionConfig(QWidget* parent = nullptr);
	virtual ~DatabaseConnectionConfig() = default;

	/// load settings
	void load();

	/// save settings
	void save();

private:
	///
	/// \brief createAllWidgets
	/// create all nessesary widgets
	/// for database connection configuration
	///
	void createAllWidgets();
	void createConnections();

	QLineEdit* mDriverName;
	QLineEdit* mHost;
	QLineEdit* mDatabaseName;
	QLineEdit* mUserName;
	QLineEdit* mPassword;
	QCheckBox* mTrustedConnection;
	QCheckBox* mWindowsAuth;
};

///
/// \brief The RemoteConnectionConfig class
/// widget uses to configure connection with
/// remote service used to coperate between others system users
///
class RemoteConnectionConfig : public QWidget {
	Q_OBJECT

public:
	explicit RemoteConnectionConfig(QWidget* parent);
	virtual ~RemoteConnectionConfig() = default;

	/// load configuration
	void load();

	/// save configuration
	void save();

private:
	void createAllWidgets();
	void createConnections();

	QLineEdit* mServiceAddress;
	QSpinBox* mServicePort;
	QLineEdit* mEventAgentAddress;
	QSpinBox* mEventAgentPort;
};

} // namespace eSawmill::config

#endif // DATAPROVIDERCONFIG_H
