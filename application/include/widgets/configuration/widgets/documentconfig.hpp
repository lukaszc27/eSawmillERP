#ifndef DOCUMENTCONFIG_HPP
#define DOCUMENTCONFIG_HPP

#include <configurationwidget.h>
#include <QPixmap>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>


namespace eSawmill {
namespace config {

class DocumentConfig : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	explicit DocumentConfig(QWidget *parent = nullptr);

	void initialize() override;
	void save() override;

private:
	QLabel* mBannerView;
	QPushButton* mBrowseButton;
	QLineEdit* mOrderPrefix;
	QLineEdit* mServicePrefix;

private Q_SLOTS:
	/**
	 * @brief browseButton_clicked
	 * wczytuje banner z dysku i przedstawia go podglądzie
	 */
	void browseButton_clicked();
};

} // namespace config
} // namespace eSawmill

#endif // DOCUMENTCONFIG_HPP
