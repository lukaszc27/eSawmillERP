#ifndef COMARCHCONFIGWIDGET_HPP
#define COMARCHCONFIGWIDGET_HPP

#include <configurationwidget.h>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <simplecrypt.hpp>


namespace eSawmill {
namespace config {
namespace integrations {

///
/// \brief The ComarchWidget class
/// subwidget for main integration widget
/// in this place user can configure how
/// to connected to Comarch ERP Optima database
/// and other configuration for Comarch app
///
class ComarchConfigWidget : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	ComarchConfigWidget(QWidget* parent = nullptr);
	virtual ~ComarchConfigWidget() {}

	void initialize() override;
	void save() override;

private:
	QCheckBox* mAccessToIntergate;
	QCheckBox* mTrustedConnection;
	QCheckBox* mWindowsAuth;
	QLineEdit* mDriverName;
	QLineEdit* mHostName;
	QLineEdit* mDatabaseName;
	QLineEdit* mUserName;
	QLineEdit* mUserPassword;
	QPushButton* mTestButton;

private Q_SLOTS:
	///
	/// \brief testConnectionHandle
	/// create test connection to SQL server
	/// with data from field
	///
	void testConnectionHandle();
};

} // namespace integrations
} // namespace config
} // namespace eSawmill

#endif // COMARCHCONFIGWIDGET_HPP
