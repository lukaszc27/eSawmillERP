#ifndef PRICECONFIGWIDGET_HPP
#define PRICECONFIGWIDGET_HPP

#include <configurationwidget.h>
#include <QDoubleSpinBox>
#include <tableview.h>
#include <pushbutton.h>
#include <models/woodtypemodel.hpp>


namespace eSawmill {
namespace config {

///
/// \brief The PriceConfigWidget class
/// the widget allow to manage information about
/// default price used in system to calculate
///
class PriceConfigWidget : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	PriceConfigWidget(QWidget* parent = nullptr);
	virtual ~PriceConfigWidget() = default;

	///
	/// \brief initialize
	/// get data from config file and set to fields
	///
	void initialize() override;

	///
	/// \brief save
	/// save data to condif file when user close config dialog
	///
	void save() override;

private:
	void createAllWidgets();
	void createModels();
	void createConnections();

	QDoubleSpinBox* mOrderPrice;
	QDoubleSpinBox* mOrderPlannedPrice;
	QDoubleSpinBox* mServicePrice;
	QDoubleSpinBox* mServiceRotatedPrice;
	::widgets::TableView* mTableView;
	::widgets::PushButton* mAddButton;
	::widgets::PushButton* mRemoveButton;

	///@{
	/// models used to present data for user
	/// it can be connect to view widget
	core::models::WoodTypeModel* mWoodTypeModel;
	///@}

private Q_SLOTS:
	///
	/// \brief addNewWoodTypeHandle
	/// add new wood type object to list
	///
	void addNewWoodTypeHandle();

	///
	/// \brief removeWoodTypeHandle
	/// remove one record of wood type from list and database
	///
	void removeWoodTypeHandle();
};

} // namespace config
} // namespace eSawmill

#endif // PRICECONFIGWIDGET_HPP
