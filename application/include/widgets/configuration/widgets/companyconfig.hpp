#ifndef COMPANYCONFIG_HPP
#define COMPANYCONFIG_HPP

#include <configurationwidget.h>
#include <QLineEdit>
#include <QComboBox>
#include <QToolButton>


namespace eSawmill {
namespace config {

class CompanyConfig : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	CompanyConfig(QWidget* parent = nullptr);

	void initialize() override;
	void save() override;

private:
	void createAllWidgets();
	void createAllValidators();

	QLineEdit* mCompanyName;
	QLineEdit* mFirstName;
	QLineEdit* mSurName;
	QLineEdit* mNip;
	QLineEdit* mRegon;
	QLineEdit* mKrs;
	QToolButton* mGusButton;

	QLineEdit* mCountry;
	QLineEdit* mCountryIso;
	QLineEdit* mCity;
	QLineEdit* mPostOffice;
	QLineEdit* mStreet;
	QLineEdit* mHomeNumber;
	QLineEdit* mLocalNumber;
	QLineEdit* mPostCode;
	QLineEdit* mPhone;
	QLineEdit* mEmail;
	QComboBox* mState;
	QLineEdit* mUrl;

	///
	/// \brief showCityDictionaryWidget
	/// show dialog where user can select city name
	/// and the name will set in field
	/// \param field - where city name will be set
	///
	void showCityDictionaryWidget(QLineEdit* field);

private Q_SLOTS:
	void checkNipNumber();

	///
	/// \brief gusButtonHandle
	/// search data in GUS database (integration with API GUS)
	///
	void gusButtonHandle();
};

} // namespace cofnig
} // namespace eSawmill

#endif // COMPANYCONFIG_HPP
