#ifndef INTEGRATIONSCONFIG_HPP
#define INTEGRATIONSCONFIG_HPP

#include <configurationwidget.h>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QTabWidget>


namespace eSawmill {
namespace config {

class IntegrationsConfig : public eSawmill::widgets::ConfigurationWidget {
	Q_OBJECT

public:
	IntegrationsConfig(QWidget* parent = nullptr);
	virtual ~IntegrationsConfig() = default;

private:
	QTabWidget* mTabWidget;
};

}
}

#endif // INTEGRATIONSCONFIG_HPP
