#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QDialog>
#include <QTabWidget>
#include <QPushButton>
#include <QLayout>
#include <QListWidget>
#include <QStackedWidget>
#include <eventagentclient.hpp>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill {
namespace config {

class MainConfiguration : public QDialog {
	Q_OBJECT

public:
	MainConfiguration(QWidget* parent = nullptr);

public Q_SLOTS:
	void accept() override;

private:
	void createWidgets();
	void createConnections();

	QListWidget* mListWidget;
	QStackedWidget* mStackedWidget;
	QPushButton* mAcceptButton;
	QPushButton* mRejectButton;

Q_SIGNALS:
	void notifyRepositoryChanged(const agent::RepositoryEvent& event);
};

} // namespace config
} // namespace eSawmill

#endif // CONFIGURATION_H
