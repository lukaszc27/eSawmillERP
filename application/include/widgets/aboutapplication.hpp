#ifndef ABOUTAPPLICATION_HPP
#define ABOUTAPPLICATION_HPP

#include <QtGlobal>
#include <QDialog>
#include <pushbutton.h>

namespace eSawmill {

#ifdef USE_QT6
#include <QQuickWidget>


class AboutApplication : public QDialog {
	Q_OBJECT

public:
	explicit AboutApplication(QWidget* parent = nullptr);
	virtual ~AboutApplication() = default;

private:
	QQuickWidget* mQuickWidget;
	::widgets::PushButton* mCloseButton;
};

#else

class AboutApplication final : public QDialog {
Q_OBJECT

public:
	explicit AboutApplication(QWidget* parent = nullptr);
	virtual ~AboutApplication() = default;
};

#endif

} // namespace eSawmill

#endif // ABOUTAPPLICATION_HPP
