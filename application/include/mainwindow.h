#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QToolBar>
#include <QSystemTrayIcon>

//#include <QWebEngineView>
#include "verifications/verificationexecutor.hpp"
#include "logs/abstractlogger.hpp"
#include "authdialog.h"		/// dialog autoryzacji (moduł account)
#include "statusbar.hpp"
#include "contractormanager.h"
#include "warehousemanager.h"
#include "createarticle.h"
#include "articlesmanager.h"
#include "ordersmanager.h"
#include "servicemanager.hpp"
#include <providers/controlcenterclient.hpp>
#include <eventagentclient.hpp>
#include <commandexecutor.hpp>
#include <eventswidget.hpp>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace eSawmill {

/**
 * @brief The MainWindow class
 * Klasa okna głównego aplikacji
 */
class MainWindow final : public QMainWindow,
						 public core::verifications::VerificationExecutor,
						 private core::CommandExecutor::Listener {
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	virtual ~MainWindow();

	///@{
	/// implementation core::CommandExecutor::Listener interface
	void stackChanged(bool canUndo) override;
	void commandExecuted() override {};
	///@}

protected:
	void closeEvent(QCloseEvent* event) override;

private:
	// funkcje
	void createWidgets();
	void createMenus();
	void createActions();
	void createConnections();
	void createDockWidgets();

	///
	/// \brief registerVerifications
	/// added verifications that will be check after login user to app
	///
	void registerVerifications();

	/**
	 * @brief loginUserWithExistProfile
	 * loguje użytkownika zapisanego w profilu logowania
	 * @return true - jeśli użytkownik zostanie zalogowany
	 */
	bool loginUserWithExistProfile();

	// akcje
	QAction* mContractorsAction;
	QAction* mArticlesAction;
	QAction* mWarehouseAction;
	QAction* mOrdersAction;
	QAction* mServiceAction;
	QAction* mConfigurationAction;
	QAction* mBackupAction;
	QAction* mExitAction;
	QAction* mAuthAction;
	QAction* mAboutQt;
	QAction* mAboutApp;
	QAction* mSalesInvoicesAction;
	QAction* mUndoAction;

	// menu
	QMenu* mWarehouseMenu;
	QMenu* mOrdersMenu;
	QMenu* mMainMenu;
	QMenu* mHelpMenu;
	QMenu* mViewMenu;
	QMenu* mEditMenu;

	// kontrolki
	QToolBar* mAuthToolBar;
	QToolBar* mGeneralToolBar;
	QToolBar* mMainToolBar;
//	QWebEngineView* mWebView;
	StatusBar* mStatusBar;
	QSystemTrayIcon* mSystemTray;

	// additional widgets and docking widgets
	eSawmill::account::AuthDialog* mAuthDialog;
	::widgets::EventsWidget* mEventsWidget;

private Q_SLOTS:
	void authenticateUserHandle();
	void userIsLogin();

	void contractorActionHandle();
	void articleActionHandle();
	void warehouseActionHandle();
	void orderActionHandle();
	void serviceActionHandle();
	void backupActionHandle();
	void configurationActionHandle();
	void aboutQtHandle();
	void aboutApplicationHandle();
	void salesInvoicesManagerHandle();
	void purchaseInvoicesManagerHandle();
	void undoActionHandle();

	///
	/// \brief notifyEventReceived
	/// slots running when new events will be recived from event agent service
	/// \param event
	///
	void notifyEventReceived(const agent::RepositoryEvent& event);
	void notifyServerStatusReceived(const agent::ServerStatusEvent& event);

	///
	/// \brief handleSocketErrors
	/// handle all errors in communication with control center service
	/// \param err
	///
	void handleSocketErrors(QAbstractSocket::SocketError err);

	///
	/// \brief loadRepositoryData
	/// load all nessesary repositores (e.g contractors, articles, orders, services, invoices)
	/// into cache memory after users are logged
	///
	void loadRepositoryData();

private:
	/**
	 * @brief parseLicenceFile
	 * sprawdza czy plik licencji istnieje we wskazanej lokalizacji
	 * oraz wygiąga dane zapisane w licencji tj. dostępne moduły aplikacji
	 * oraz informacje o licencjonobiorcy
	 */
	void parseLicenceFile();

	void createEventAgent();
	void destroyEventAgent();


	bool mOrderModuleActive;
	bool mServiceModuleActive;
	bool mArticleModuleActive;

	core::logs::AbstractLogger& logger_;

Q_SIGNALS:
	///
	/// \brief notifyRepositoryChanged
	/// signal emitted by main window when data in repository will be changed
	/// \param evnet
	///
	void notifyRepositoryChanged(const agent::RepositoryEvent& evnet);
};

}

#endif // MAINWINDOW_H
