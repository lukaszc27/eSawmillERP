#ifndef RESTORE_HPP
#define RESTORE_HPP

#include <QWizardPage>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>


namespace eSawmill {
namespace BackupWizard {
namespace Pages {

class RestorePage : public QWizardPage {
	Q_OBJECT

public:
	explicit RestorePage(QWidget* parent = nullptr);

private:
	/**
	 * @brief createAllWidgets
	 */
	void createAllWidgets();

	QLabel* mLocationLabel;
	QLineEdit* mLocation;
	QPushButton* mBrowseButton;
	QPushButton* mRestoreButton;

private Q_SLOTS:
	void browseButtonHandle();
	void restoreButtonHandle();
};

} // namespace Pages
} // namespace BackupWizard
} // namespace eSawmill

#endif // RESTORE_HPP
