#ifndef INTRO_HPP
#define INTRO_HPP

#include <QWizardPage>
#include <QRadioButton>


namespace eSawmill {
namespace BackupWizard {
namespace Pages {

/**
 * @brief The IntroPage class
 * strona pozwalająca wybrać czy chcemy utworzyć kopię
 * bezpieczeństwa czy odczytać istniejącą
 */
class IntroPage : public QWizardPage {
	Q_OBJECT

public:
	explicit IntroPage(QWidget* parent = nullptr);

	int nextId() const override;

private:
	/**
	 * @brief createAllWidgets
	 * tworzy wszystkie widgety wykożystywane w oknie
	 */
	void createAllWidgets();

	QRadioButton* mCreateBackup;
	QRadioButton* mRestoreBackup;
};

} // namespace Pages
} // namespace BackupWizard
} // namespace eSawmill
#endif // INTRO_HPP
