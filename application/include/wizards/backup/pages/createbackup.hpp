#ifndef CREATEBACKUP_HPP
#define CREATEBACKUP_HPP

#include <QWizardPage>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>


namespace eSawmill {
namespace BackupWizard {
namespace Pages {

/**
 * @brief The CreateBackup class
 * tworzenie kopii bezpieczeństwa ustawień programu
 * oraz bazy danych (SQL Server)
 */
class CreateBackup : public QWizardPage {
	Q_OBJECT

public:
	explicit CreateBackup(QWidget* parent = nullptr);

	void initializePage() override;
	void cleanupPage() override;
	int nextId() const override { return -1; }

private:
	void createAllWidgets();
	void createConnections();

	QLineEdit* mName;
	QLineEdit* mFilename;
	QLineEdit* mLocation;
	QPlainTextEdit* mDescription;
	QPushButton* mBrowseButton;
	QPushButton* mCreateButton;

private Q_SLOTS:
	/**
	 * @brief createButtonHandle
	 * tworzy kopię bezpieczeństwa
	 */
	void createButtonHandle();

	/**
	 * @brief locationButtonHandle
	 * wybór lokalizacji pliku kopii bezpieczeństwa
	 */
	void locationButtonHandle();
};

} // namespace Pages
} // namespace BackupWizard
} // namespace eSawmill

#endif // CREATEBACKUP_HPP
