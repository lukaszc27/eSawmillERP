#ifndef BACKUPWIZARD_HPP
#define BACKUPWIZARD_HPP

#include <QWizard>

namespace eSawmill {
namespace BackupWizard {

/**
 * @brief The BackupWizard class
 * kreator kopii bezpieczeństwa, umożliwia odczyt
 * danych z kopii oraz przywrucenie danych
 * z poprzednio utworzonych kopii
 */
class BackupWizard : public QWizard {
	Q_OBJECT

public:
	explicit BackupWizard(QWidget* parent = nullptr);

	enum WizardPages {
		Intro = 0,
		CreateBackup,
		RestoreBackup,
	};
};

} // namespace BackupWizard
} // namespace eSawmill

#endif // BACKUPWIZARD_HPP
