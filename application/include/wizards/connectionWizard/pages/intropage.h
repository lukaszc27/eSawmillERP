#ifndef INTROPAGE_CREATECONNECTION_H
#define INTROPAGE_CREATECONNECTION_H

#include <QWizardPage>
#include <QLabel>

namespace eSawmill {
namespace ConnectionWizard {

/**
 * Strona informacyjna dialogu pierwszego uruchomienia
 * aplikacji, informuje jakie dane powinien przygotować
 * użytkownik w celu prawidłowej konfiguracji połączenia
 * z serwerem MS SQL
 */
class IntroPage : public QWizardPage {
	Q_OBJECT

public:
	IntroPage(QWidget* parent = nullptr);

private:
	void createWidgets();

	QLabel* mWelocomeText;
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // INTROPAGE_CREATECONNECTION_H
