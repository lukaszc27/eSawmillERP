#ifndef ENDPAGE_H
#define ENDPAGE_H

#include <QWizardPage>


namespace eSawmill {
namespace ConnectionWizard {

class EndPage : public QWizardPage {
	Q_OBJECT

public:
	EndPage(QWidget* parent = nullptr);
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // ENDPAGE_H
