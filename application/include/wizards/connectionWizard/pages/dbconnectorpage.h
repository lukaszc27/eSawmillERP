#ifndef DBCONNECTORPAGE_H
#define DBCONNECTORPAGE_H

#include <QWizardPage>
#include <QGroupBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QRadioButton>


namespace eSawmill {
namespace ConnectionWizard {

class DbConnectorPage : public QWizardPage
{
public:
	DbConnectorPage(QWidget* parent = nullptr);

	virtual int nextId() const override;
	virtual void initializePage() override;
	virtual bool validatePage() override;

private:
	QGroupBox* mAuthTypeGroup;
	QRadioButton* mSqlServerAuth;
	QRadioButton* mWindowsAuth;
	QLineEdit* mDriverName;
	QLineEdit* mHostName;
	QLineEdit* mDatabaseName;
	QLineEdit* mUserName;
	QLineEdit* mUserPassword;
	QCheckBox* mTrustedConnection;
	QPushButton* mTestConnButton;

	void createWidgets();

	// Zmienne
	bool bTestConnectionComplete;

private Q_SLOTS:
	void testConnectionButton_Click();
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // DBCONNECTORPAGE_H
