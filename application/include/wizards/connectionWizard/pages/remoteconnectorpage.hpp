#ifndef REMOTECONNECTORPAGE_HPP
#define REMOTECONNECTORPAGE_HPP

#include <QWizardPage>
#include <QLineEdit>
#include <QSpinBox>


namespace eSawmill::ConnectionWizard {

class RemoteConnectorPage : public QWizardPage {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit RemoteConnectorPage(QWizard* parent = nullptr);
	virtual ~RemoteConnectorPage() = default;
	///@}

	virtual void initializePage() override;
	virtual int nextId() const override;
	virtual bool validatePage() override;

private:
	void createAllWidgets();

	QLineEdit* mServiceAddress;
	QLineEdit* mEventAgentAddress;
	QSpinBox* mServicePort;
	QSpinBox* mEventAgentPort;
};

} // eSawmill::ConnectionWizard

#endif // REMOTECONNECTORPAGE_HPP
