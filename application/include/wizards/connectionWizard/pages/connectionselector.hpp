#ifndef CONNECTIONSELECTOR_HPP
#define CONNECTIONSELECTOR_HPP

#include <QWizardPage>
#include <QRadioButton>


namespace eSawmill::ConnectionWizard {

///
/// \brief The ConnectionSelector class
/// page in the intro wizard where users can select prefered connection
///
class ConnectionSelectorPage : public QWizardPage {
	Q_OBJECT

public:
	///@{
	/// ctors
	explicit ConnectionSelectorPage(QWidget* parent = nullptr);
	virtual ~ConnectionSelectorPage() = default;
	///@}

	virtual int nextId() const override;
	virtual bool validatePage() override;

private:
	void createAllWidgets();

	QRadioButton* mDatabaseConnection;
	QRadioButton* mRemoteConnection;
};

} // namespace eSawmill::ConnectionWizard

#endif // CONNECTIONSELECTOR_HPP
