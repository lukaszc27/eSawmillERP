#ifndef COMPANYPAGE_H
#define COMPANYPAGE_H

#include <QWizardPage>
#include <QLineEdit>


namespace eSawmill {
namespace ConnectionWizard {

class CompanyPage : public QWizardPage {
	Q_OBJECT

public:
	CompanyPage(QWidget* parent = nullptr);

	bool validatePage() override;

private:
	QLineEdit* mName;
	QLineEdit* mNip;
	QLineEdit* mRegon;
	QLineEdit* mKrs;

	void createWidgets();
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // COMPANYPAGE_H
