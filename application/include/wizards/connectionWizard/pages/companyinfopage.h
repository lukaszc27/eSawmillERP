#ifndef COMPANYINFOPAGE_H
#define COMPANYINFOPAGE_H

#include <QWizardPage>
#include <QLabel>
#include <QRadioButton>


namespace eSawmill {
namespace ConnectionWizard {

/**
 * Strona umożliwiająca wybór czy tworzyć nowe konto firmowe.
 * Jeśli użytkownik posiada konto firmowe i nie ma konieczności tworzenia
 * kolejnego jest możliwość przejścia na koniec dialogu i rozpoczęcia
 * pracy w programie eSawmill ERP
 */
class CompanyInfoPage : public QWizardPage {
	Q_OBJECT

public:
	CompanyInfoPage(QWidget* parent = nullptr);

	int nextId() const override;

private:
	QLabel* mInfoLabel;
	QRadioButton* mAccountExistRadioButton;
	QRadioButton* mCreateAccountButton;

	void createWidgets();
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // COMPANYINFOPAGE_H
