#ifndef AUTHPAGE_H
#define AUTHPAGE_H

#include <QWizardPage>
#include <QLineEdit>


namespace eSawmill {
namespace ConnectionWizard {

class AuthPage : public QWizardPage {
	Q_OBJECT

public:
	AuthPage(QWidget* parent = nullptr);

	bool validatePage() override;
	void cleanupPage() override;

private:
	QLineEdit* mUserName;
	QLineEdit* mPassword;
	QLineEdit* mConfirmPassword;
	QLineEdit* mFirstname;
	QLineEdit* mSurname;

	void createWidgets();
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // AUTHPAGE_H
