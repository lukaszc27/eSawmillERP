#ifndef CONTACTPAGE_H
#define CONTACTPAGE_H

#include <QWizardPage>
#include <QLineEdit>
#include <QComboBox>


namespace eSawmill {
namespace ConnectionWizard {

class ContactPage : public QWizardPage {
	Q_OBJECT

public:
	ContactPage(QWidget* parent = nullptr);

	bool validatePage() override;

private:
	QLineEdit* mCountry, *mCountryIso;
	QLineEdit* mCity;
	QComboBox* mState;
	QLineEdit* mPostOffice, *mPostCode;
	QLineEdit* mStreet, *mHomeNumber, *mLocalNumber;
	QLineEdit* mPhone;
	QLineEdit* mEmail, *mUrl;

	void createWidgets();
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // CONTACTPAGE_H
