#ifndef CREATECONNECTIONWIZARD_H
#define CREATECONNECTIONWIZARD_H

#include <QWizard>


namespace eSawmill {
namespace ConnectionWizard {

/**
 * Dialog wyświetlany podczas pierwszego startu aplikacji
 * umożliwia połączenie aplikacji z serserem baz danych
 * oraz tworzy wymagany plik do połączenia podczas następnych
 * uruchomień aplikacji
 */
class CreateConnectionWizard : public QWizard {
	Q_OBJECT

public:
	CreateConnectionWizard(QWidget* parent = nullptr);

	/**
	 * @brief indeksty stron
	 */
	enum Pages {
		Intro,
		ConnectionSelector,
		DbConnection,
		RemoteConnection,
		AccountInfo,
		Contact,
		Company,
		Auth,
		End
	};
};

} // namespace ConnectionWizard
} // namespace eSawmill

#endif // CREATECONNECTIONWIZARD_H
