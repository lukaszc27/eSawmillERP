import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    ColumnLayout {
        anchors.fill: parent

        Text {
            text: qsTr('eSawmill ERP')
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pointSize: 24
            Layout.fillWidth: true
        }
        Text {
            text: qsTr('Rozwiązania informatyczne dla tartaku')
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 16
            font.weight: Font.DemiBold
            color: 'gray'
            Layout.fillWidth: true
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            anchors.margins: 12

            Item {
                width: parent.width / 2
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom

                Text {
                    anchors.fill: parent
                    text: qsTr('eSawmill ERP jest systemem do zarządzania produkcją w przedsiębiorstwach obróbki drewna.')
                    wrapMode: Text.WordWrap
                }
            }
            Item {
                width: parent.width / 2
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom

                Text {
                    text: qsTr('Zarejestrowano dla')
                    font.weight: Font.DemiBold
                }
            }
        }
    }
}
