#include "widgets/configuration/widgets/integrations/comarchconfigwidget.hpp"
#include <QFormLayout>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QStringBuilder>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSettings>
#include <messagebox.h>


namespace eSawmill {
namespace config {
namespace integrations {

ComarchConfigWidget::ComarchConfigWidget(QWidget *parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	mDriverName = new QLineEdit(this);
	mHostName = new QLineEdit(this);
	mDatabaseName = new QLineEdit(this);
	mUserName = new QLineEdit(this);
	mUserPassword = new QLineEdit(this);
	mAccessToIntergate = new QCheckBox(tr("Zezwól na integrację eSawmill ERP z Comarch ERP Optima"), this);
	mTrustedConnection = new QCheckBox(tr("Trusted Connection"), this);
	mWindowsAuth = new QCheckBox(tr("Autoryzacja Windows"), this);
	mTestButton = new QPushButton(tr("Test połączenia"), this);

	mDriverName->setPlaceholderText(tr("ODBC Driver 17 for SQL Server"));
	mHostName->setPlaceholderText(tr("localhost"));
	mDatabaseName->setPlaceholderText(tr("CDN_OPTIMA"));
	mUserName->setPlaceholderText(tr("SA"));
	mUserPassword->setPlaceholderText(tr("Hasło"));
	mUserPassword->setEchoMode(QLineEdit::Password);

	mTestButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDatabaseName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mHostName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* layout = new QFormLayout(this);
	layout->addRow(mAccessToIntergate);
	layout->addRow(mWindowsAuth);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Expanding));
	layout->addRow(tr("Sterownik"), mDriverName);
	layout->addRow(tr("Host"), mHostName);
	layout->addRow(tr("Baza danych"), mDatabaseName);
	layout->addRow(tr("Użytkownik"), mUserName);
	layout->addRow(tr("Hasło"), mUserPassword);
	layout->addItem(new QSpacerItem(0, 6, QSizePolicy::Expanding));
	layout->addRow(mTrustedConnection);
	layout->addItem(new QSpacerItem(0, 12, QSizePolicy::Expanding));
	layout->addWidget(mTestButton);

	connect(mAccessToIntergate, &QCheckBox::stateChanged, this, [&](int state){
		// enable or disable all input fields
		QList<QLineEdit*> fields = findChildren<QLineEdit*>();
		for (QLineEdit* field : fields) {
			field->setEnabled(state);
		}
		mWindowsAuth->setEnabled(state);
		mTrustedConnection->setEnabled(state);
		mTestButton->setEnabled(state);
	});

	connect(mWindowsAuth, &QCheckBox::stateChanged, this, [&](int state){
		// if user set windows auth
		// disable user name and password fields
		mUserName->setEnabled(!state);
		mUserPassword->setEnabled(!state);
	});
	connect(mTestButton, &QPushButton::clicked, this, &ComarchConfigWidget::testConnectionHandle);

	mAccessToIntergate->stateChanged(0);
}

void ComarchConfigWidget::initialize()
{
	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	QSettings settings;
	mDriverName->setText(settings.value("comarch/driver").toString());
	mHostName->setText(settings.value("comarch/host").toString());
	mDatabaseName->setText(settings.value("comarch/database").toString());
	mUserName->setText(settings.value("comarch/userName").toString());
	mUserPassword->setText(crypt.decryptToString(settings.value("comarch/userPassword").toString()));
	mAccessToIntergate->setChecked(settings.value("comarch/allowToConnect").toBool());
	mWindowsAuth->setChecked(settings.value("comarch/windowsAuth").toBool());
	mTrustedConnection->setChecked(settings.value("comarch/trustedConnection").toBool());
}

void ComarchConfigWidget::save()
{
	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	const QString password = crypt.encryptToString(mUserPassword->text());

	QSettings settings;
	settings.setValue("comarch/driver", mDriverName->text());
	settings.setValue("comarch/host", mHostName->text());
	settings.setValue("comarch/database", mDatabaseName->text());
	settings.setValue("comarch/userName", mUserName->text());
	settings.setValue("comarch/userPassword", password);
	settings.setValue("comarch/allowToConnect", mAccessToIntergate->isChecked());
	settings.setValue("comarch/windowsAuth", mWindowsAuth->isChecked());
	settings.setValue("comarch/trustedConnection", mTrustedConnection->isChecked());
}

void ComarchConfigWidget::testConnectionHandle()
{
	QString sb;
	sb = sb.append("DRIVER={%1};").arg(mDriverName->text().trimmed());
	sb = sb.append("SERVER=%1;").arg(mHostName->text().trimmed());
	sb = sb.append("DATABASE=%1;").arg(mDatabaseName->text().trimmed());

	if (!mWindowsAuth->isChecked())
		sb = sb.append("UID=%1;PWD=%2;").arg(mUserName->text(), mUserPassword->text());

	if (mTrustedConnection->isChecked())
		sb = sb.append("Trusted_Connection=YES");

	QSqlDatabase db = QSqlDatabase::addDatabase("QODBC", "comarch");
	db.setDatabaseName(sb);

	if (db.open()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
										   tr("Połączenie z bazą Comarch ERP Optima nawiązane prawidłowo"));
	}
	else {
		::widgets::MessageBox::critical(this, tr("Błąd"), db.lastError().text());
	}
}

} // namespace integrations
} // namespace config
} // namespace eSawmill
