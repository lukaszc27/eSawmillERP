#ifndef COMPANYCONFIG_CPP
#define COMPANYCONFIG_CPP

#include "widgets/configuration/widgets/companyconfig.hpp"
#include <dbo/company.h>
#include <dbo/contact.h>
#include <messagebox.h>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QLabel>
#include <models/stateslistmodel.hpp>
#include <dialogs/citydictionarydialog.hpp>
#include <network/regon/regon.hpp>
#include <globaldata.hpp>

namespace eSawmill {
namespace config {

CompanyConfig::CompanyConfig(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	setWindowTitle(tr("Konfiguracja firmy"));
	createAllWidgets();
	createAllValidators();

	// create models
	mState->setModel(new core::models::StatesListModel(this));

	connect(mNip, &QLineEdit::editingFinished, this, &CompanyConfig::checkNipNumber);

	// this widget can edit only users who have admin role
	if (!core::GlobalData::instance().repository().userRepository().autorizeUser().hasRole(core::Role::SystemRole::Administrator)) {
		setEnabled(false);
	}
}

void CompanyConfig::initialize()
{
	try {
		core::repository::AbstractUserRepository& userRepository = core::GlobalData::instance().repository().userRepository();
		core::Company company = core::GlobalData::instance()
			.repository()
			.companiesRepository()
			.getCompany(userRepository.autorizeUser().company().id());

		const core::Contact& contact = company.contact();
		mCompanyName->setText(company.name());
		mFirstName->setText(company.personName());
		mSurName->setText(company.personSurname());
		mNip->setText(company.NIP());
		mRegon->setText(company.REGON());
		mKrs->setText(company.KRS());
		mCity->setText(contact.city());
		mStreet->setText(contact.street());
		mHomeNumber->setText(contact.homeNumber());
		mPostCode->setText(contact.postCode());
		mPhone->setText(contact.phone());
		mEmail->setText(contact.email());
		mCountryIso->setText(contact.IsoCountry());
		mCountry->setText(contact.country());
		mPostOffice->setText(contact.postOffice());
		mLocalNumber->setText(contact.localNumber());
		mState->setCurrentText(contact.state());
		mUrl->setText(contact.url());
	}
	catch (std::exception& ex) {
		::widgets::MessageBox::critical(this, tr("Błąd"),
			tr("Podczas pobierania informacji o firmie wystąpiły nieoczekiwane błędy"), ex.what());
		core::GlobalData::instance().logger().warning("Błąd pobierania informacji o firmie");
	}
}

void CompanyConfig::save()
{
	try {
		core::repository::AbstractUserRepository& userRepository = core::GlobalData::instance().repository().userRepository();
		core::Company company = userRepository.autorizeUser().company();
		company.setName(mCompanyName->text());
		company.setPersonName(mFirstName->text());
		company.setPersonSurname(mSurName->text());
		company.setNIP(mNip->text());
		company.setREGON(mRegon->text());
		company.setKRS(mKrs->text());

		core::Contact contact = userRepository.autorizeUser().contact();
		contact.setCountry(mCountry->text());
		contact.setIsoCountry(mCountryIso->text());
		contact.setCity(mCity->text());
		contact.setStreet(mStreet->text());
		contact.setHomeNumber(mHomeNumber->text());
		contact.setLocalNumber(mLocalNumber->text());
		contact.setPostCode(mPostCode->text());
		contact.setPostOffice(mPostOffice->text());
		contact.setPhone(mPhone->text());
		contact.setEmail(mEmail->text());
		contact.setState(mState->currentText());
		contact.setUrl(mUrl->text());

		company.setContact(contact);
		if (!core::GlobalData::instance().repository().companiesRepository().update(company)) {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Błąd zapisywania danych firmowych!\n"
				   "Wprowadzone zmiany nie zostały zapisane"));
		}
	}
	catch (const QString msg) {
		::widgets::MessageBox::critical(this, tr("SQL Syntax Error"), tr("Podczas edycji danych firmowych wystąpiły nieoczekiwane błędy"), msg);
	}
}

void CompanyConfig::createAllWidgets()
{
	mCountry = new QLineEdit(this);
	mCountryIso = new QLineEdit(this);
	mPostOffice = new QLineEdit(this);
	mState = new QComboBox(this);
	mLocalNumber = new QLineEdit(this);
	mCompanyName = new QLineEdit(this);
	mFirstName = new QLineEdit(this);
	mSurName = new QLineEdit(this);
	mNip = new QLineEdit(this);
	mRegon = new QLineEdit(this);
	mKrs = new QLineEdit(this);
	mCity = new QLineEdit(this);
	mStreet = new QLineEdit(this);
	mHomeNumber = new QLineEdit(this);
	mPostCode = new QLineEdit(this);
	mPhone = new QLineEdit(this);
	mEmail = new QLineEdit(this);
	mUrl = new QLineEdit(this);
	QPushButton* cityButton = new QPushButton(tr("Miasto"), this);
	QPushButton* postButton = new QPushButton(tr("Poczta"), this);
	mGusButton = new QToolButton(this);

	mGusButton->setText("GUS");
	mGusButton->setMaximumWidth(32);
	mCountryIso->setMaximumWidth(48);
	mHomeNumber->setMaximumWidth(75);
	mLocalNumber->setMaximumWidth(75);

	mGusButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mFirstName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mSurName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mNip->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mRegon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mKrs->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mCity->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPostOffice->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountry->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountryIso->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPostCode->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mHomeNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mLocalNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mState->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mState->setEditable(true);

	QGroupBox* companyGroup = new QGroupBox(tr("Dane firmowe"), this);
	QFormLayout* companyForm = new QFormLayout(companyGroup);
	QHBoxLayout* nipLayout = new QHBoxLayout;
	nipLayout->addWidget(mNip);
	nipLayout->addWidget(mGusButton);

	companyForm->addRow(tr("Nazwa firmy"), mCompanyName);
	companyForm->addRow(tr("Imię"), mFirstName);
	companyForm->addRow(tr("Nazwisko"), mSurName);
	companyForm->addRow(tr("NIP"), nipLayout);
	companyForm->addRow(tr("REGON"), mRegon);
	companyForm->addRow(tr("KRS"), mKrs);

	QGroupBox* contactGroup = new QGroupBox(tr("Dane teleadresowe"), this);
	contactGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

	QHBoxLayout* countryLayout = new QHBoxLayout;
	countryLayout->addWidget(mCountryIso);
	countryLayout->addWidget(mCountry);

	QHBoxLayout* homeNumberLayout = new QHBoxLayout;
	homeNumberLayout->addWidget(mStreet);
	homeNumberLayout->addSpacing(12);
	homeNumberLayout->addWidget(mHomeNumber);
	homeNumberLayout->addWidget(new QLabel("/", this));
	homeNumberLayout->addWidget(mLocalNumber);

	QFormLayout* cl = new QFormLayout(contactGroup);
	cl->addRow(tr("Kraj"), countryLayout);
	cl->addRow(tr("Województwo"), mState);
	cl->addRow(cityButton, mCity);
	cl->addRow(tr("Ulica"), homeNumberLayout);
	cl->addRow(postButton, mPostOffice);
	cl->addRow(tr("Kod pocztowy"), mPostCode);
	cl->addRow(tr("Telefon"), mPhone);
	cl->addRow(tr("E-Mail"), mEmail);
	cl->addRow(tr("URL"), mUrl);

	QHBoxLayout* mainLayout = new QHBoxLayout(this);
	mainLayout->addWidget(companyGroup);
	mainLayout->addSpacing(12);
	mainLayout->addWidget(contactGroup);

	// creact connections for city and post buttons
	connect(cityButton, &QPushButton::clicked, this, [&](){
		showCityDictionaryWidget(mCity);
	});

	connect(postButton, &QPushButton::clicked, this, [&](){
		showCityDictionaryWidget(mPostOffice);
	});
	connect(mGusButton, &QPushButton::clicked, this, &CompanyConfig::gusButtonHandle);
}

void CompanyConfig::createAllValidators()
{
	// maski wprowadzania chnych
	mPostCode->setInputMask("99-999;_");
	mNip->setInputMask("9999999999;_");
	mPhone->setInputMask("+99 999999999;_");
	mPhone->setText("48");

	// walidatory pól tekstowych
	QRegularExpressionValidator* stringValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z]+"), this);
	QRegularExpressionValidator* companyNameValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9\\s\"-]*"), this);
	QRegularExpressionValidator* regonValidator = new QRegularExpressionValidator(QRegularExpression("[0-9]{9,14}"), this);
	QRegularExpressionValidator* streetValidator = new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9 ]+"), this);
	QRegularExpressionValidator* emailValidator = new QRegularExpressionValidator(QRegularExpression("^[A-Za-z0-9]+@[A-Za-z]+.[A-Za-z]{2,3}$"), this);
	QRegularExpressionValidator* homeNumberValidator = new QRegularExpressionValidator(QRegularExpression("[0-9]+[A-Za-z]?"), this);

	mFirstName->setValidator(stringValidator);
	mSurName->setValidator(stringValidator);
	mCompanyName->setValidator(companyNameValidator);
	mStreet->setValidator(streetValidator);
	mHomeNumber->setValidator(homeNumberValidator);
	mEmail->setValidator(emailValidator);
	mRegon->setValidator(regonValidator);
}

void CompanyConfig::showCityDictionaryWidget(QLineEdit *field)
{
	::widgets::dialogs::CityDictionaryDialog* dialog = new ::widgets::dialogs::CityDictionaryDialog(this);
	if (dialog->exec() == QDialog::Accepted) {
		field->setText(dialog->city());
	}
}

void CompanyConfig::gusButtonHandle()
{
	core::network::Regon* regon = new core::network::Regon(this);
	regon->login(core::network::Regon::userKey());
	connect(regon, &core::network::Regon::userIsLogged, [regon, this](const QByteArray token){
		Q_UNUSED(token);

		setCursor(Qt::WaitCursor);
		QList<QLineEdit*> fields = findChildren<QLineEdit*>();
		for (auto field : fields) {
			field->setEnabled(false);
		}
		regon->searchEntities(mNip->text().trimmed());
	});

	connect(regon, &core::network::Regon::userIsLogout, [this](){
		setCursor(Qt::ArrowCursor);
		QList<QLineEdit*> fields = findChildren<QLineEdit*>();
		for (auto field : fields) {
			field->setEnabled(true);
		}
	});

	connect(regon, &core::network::Regon::finished, [regon, this](QDomElement &element){
		if (element.hasChildNodes()) {
			mCompanyName->setText(element.firstChildElement("Nazwa").text().toUpper());
			mRegon->setText(element.firstChildElement("Regon").text());
			mState->setCurrentText(element.firstChildElement("Wojewodztwo").text().toUpper());
			mCity->setText(element.firstChildElement("Miejscowosc").text().toUpper());
			mPostCode->setText(element.firstChildElement("KodPocztowy").text());
			mHomeNumber->setText(element.firstChildElement("NrNieruchomosci").text());
			mLocalNumber->setText(element.firstChildElement("NrLokalu").text());
			mPostOffice->setText(element.firstChildElement("MiejscowoscPoczty").text().toUpper());
		}

		regon->logout();
	});
}

void CompanyConfig::checkNipNumber()
{
	/// sprawdzanie poprawności numeru NIP opiera się o standardowy algorytm
	/// 1. Pomnożyć cyfry 1-9 przez następujące wagi: 6, 5, 7, 2, 3, 4, 5, 6, 7
	/// 2. Zsumować wyniki mnożenia
	/// 3. Obliczyć resztę z dzielenia sumy przez 11 (modulo 11)
	/// 4. Reszta z dzielenia ma być różna od 10 !!!

	const QString text = mNip->text();
	int values[9] = {0};
	const int weights[] = { 6, 5, 7, 2, 3, 4, 5, 6, 7};

	int sum = 0;
	for (int i = 0; i < 9; i++) {
		values[i] = (text.at(i).unicode() - QChar('0').unicode());

		sum += values[i] * weights[i];
	}

	if ((sum % 11) == 10) {
		::widgets::MessageBox::warning(this, tr("Numer NIP"),
									   tr("Wprowadzony numer NIP jest nieprawidłowy!"));
		mNip->selectAll();
		mNip->setFocus(Qt::FocusReason::ActiveWindowFocusReason);
	}
}

} // namespace config
} // namespace eSawmill

#endif // COMPANYCONFIG_CPP
