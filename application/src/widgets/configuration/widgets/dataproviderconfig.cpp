#include "widgets/configuration/widgets/dataproviderconfig.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QSpacerItem>
#include <QSettings>
#include <QRegularExpressionValidator>
#include <cmath>

namespace eSawmill::config {

DataProviderConfig::DataProviderConfig(QWidget *parent)
	: eSawmill::widgets::ConfigurationWidget {parent}
{
	setWindowTitle(tr("Konfiguracja źródła danych"));

	createAllWidgets();
	createConnections();
}

void DataProviderConfig::initialize() {
	// load configurations and update widgets
	QSettings settings;
	if (settings.value("DataProvider").toInt() < 0)
		return;

	mDatabaseConfig->load();
	mRemoteConfig->load();

	if (settings.value("DataProvider", DataProvider::Database).toInt() == DataProvider::Database) {
		// client use database connection
		mDatabaseConnection->setChecked(true);
		mStacked->setCurrentWidget(mDatabaseConfig);
	} else if (settings.value("DataProvider", DataProvider::Database).toInt() == DataProvider::Remote) {
		// client use remote connection (via production center service)
		mRemoteConnection->setChecked(true);
		mStacked->setCurrentWidget(mRemoteConfig);
	}
}

void DataProviderConfig::save() {
	QSettings settings;
	if (mDatabaseConnection->isChecked()) {
		settings.setValue("DataProvider", static_cast<int>(DataProvider::Database));
		mDatabaseConfig->save();
	} else {
		settings.setValue("DataProvider", static_cast<int>(DataProvider::Remote));
		mRemoteConfig->save();
	}
}

void DataProviderConfig::createAllWidgets() {
	mRemoteConnection = new QRadioButton(tr("Połączenie wielostanowiskowe"), this);
	mDatabaseConnection = new QRadioButton(tr("Połączenie jednostanowiskowe"), this);
	mStacked = new QStackedWidget(this);
	mInfoFrame = new QLabel(this);

	mInfoFrame->setStyleSheet("border 1px solid black");
	mDatabaseConfig = new DatabaseConnectionConfig(this);
	mRemoteConfig = new RemoteConnectionConfig(this);
	mStacked->addWidget(mDatabaseConfig);
	mStacked->addWidget(mRemoteConfig);

	QHBoxLayout* connectionTypeLayout = new QHBoxLayout{};
	connectionTypeLayout->addWidget(mDatabaseConnection);
	connectionTypeLayout->addSpacing(24);
	connectionTypeLayout->addWidget(mRemoteConnection);
	connectionTypeLayout->addStretch(1);

	// create layout for widgets
	QVBoxLayout* mainLayout = new QVBoxLayout {this};
	mainLayout->addLayout(connectionTypeLayout);
	mainLayout->addWidget(mStacked, 1);
	mainLayout->addWidget(mInfoFrame);
}

void DataProviderConfig::createConnections() {
	connect(mRemoteConnection, &QRadioButton::clicked, this, [this](bool checked)->void {
		if (checked) {
			mStacked->setCurrentWidget(mRemoteConfig);
			mInfoFrame->setText(tr("Połączenie z usługą umożliwiającą współpracę między wieloma "
								   "użytkownikami systemu"));
		}
	});

	connect (mDatabaseConnection, &QRadioButton::clicked, this, [this](bool checked)->void {
		if (checked) {
			mStacked->setCurrentWidget(mDatabaseConfig);
			mInfoFrame->setText(tr("Połączenie bezpośrednio z bazą danych. Zalecane przy pracy "
								   "jednego użytkownika w systemie."));
		}
	});
}

///////////////////////////////////////////////////////////////////////////////

DatabaseConnectionConfig::DatabaseConnectionConfig(QWidget *parent)
	: QWidget {parent}
{
	createAllWidgets();
	createConnections();
}

void DatabaseConnectionConfig::load() {
	QSettings settings;
	mDriverName->setText(settings.value("database/driver").toString());
	mHost->setText(settings.value("database/host").toString());
	mDatabaseName->setText(settings.value("database/name").toString());
	mUserName->setText(settings.value("database/userName").toString());
	mPassword->setText(settings.value("database/userPassword").toString());
	mTrustedConnection->setChecked(settings.value("database/trustedConnection").toBool());
	mWindowsAuth->setChecked(settings.value("database/windowsAuthorization").toBool());
}

void DatabaseConnectionConfig::save() {
	QSettings settings;
	settings.setValue("database/driver", mDriverName->text());
	settings.setValue("database/host", mHost->text());
	settings.setValue("database/name", mDatabaseName->text());
	settings.setValue("database/userName", mUserName->text());
	settings.setValue("database/userPassword", mPassword->text());
	settings.setValue("database/trustedConnection", mTrustedConnection->isChecked());
	settings.setValue("database/windowsAuthorization", mWindowsAuth->isChecked());
}

void DatabaseConnectionConfig::createAllWidgets() {
	mDriverName			= new QLineEdit(this);
	mHost				= new QLineEdit(this);
	mDatabaseName		= new QLineEdit(this);
	mUserName			= new QLineEdit(this);
	mPassword			= new QLineEdit(this);
	mTrustedConnection	= new QCheckBox(tr("Trusted Connection"), this);
	mWindowsAuth		= new QCheckBox(tr("Autoryzacja Windows"), this);

	mHost->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDatabaseName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPassword->setEchoMode(QLineEdit::EchoMode::PasswordEchoOnEdit);

	QFormLayout* mainLayout = new QFormLayout(this);
	mainLayout->addRow(tr("Sterownik"), mDriverName);
	mainLayout->addRow(tr("Host"), mHost);
	mainLayout->addRow(tr("Nazwa bazy"), mDatabaseName);
	mainLayout->addRow(tr("Użytkownik"), mUserName);
	mainLayout->addRow(tr("Hasło"), mPassword);
	mainLayout->addItem(new QSpacerItem(0, 12, QSizePolicy::Expanding, QSizePolicy::Fixed));
	mainLayout->addWidget(mTrustedConnection);
	mainLayout->addWidget(mWindowsAuth);
	mainLayout->addItem(new QSpacerItem(0, 12, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

void DatabaseConnectionConfig::createConnections() {
	connect(mWindowsAuth, &QCheckBox::stateChanged, this, [this](int state)->void {
		mUserName->setDisabled(state == Qt::Checked);
		mPassword->setDisabled(state == Qt::Checked);
	});
}

///////////////////////////////////////////////////////////////////////////////

RemoteConnectionConfig::RemoteConnectionConfig(QWidget *parent)
	: QWidget(parent)
{
	createAllWidgets();
	createConnections();
}

void RemoteConnectionConfig::load() {
	QSettings settings;
	mServiceAddress->setText(settings.value("remote/serviceHost", QVariant("127.0.0.1")).toString());
	mServicePort->setValue(settings.value("remote/servicePort", 27015).toInt());
	mEventAgentAddress->setText(settings.value("remote/eventAgent", QVariant("127.0.0.1")).toString());
	mEventAgentPort->setValue(settings.value("remote/eventAgentPort", 5050).toInt());
}

void RemoteConnectionConfig::save() {
	QSettings settings;
	settings.value("remote/serviceHost", mServiceAddress->text());
	settings.value("remote/servicePort", mServicePort->value());
	settings.value("remote/eventAgent", mEventAgentAddress->text());
	settings.value("remote/eventAgentPort", mEventAgentPort->value());
}

void RemoteConnectionConfig::createAllWidgets() {
	mServiceAddress		= new QLineEdit(this);
	mServicePort		= new QSpinBox(this);
	mEventAgentAddress	= new QLineEdit(this);
	mEventAgentPort		= new QSpinBox(this);
	QLabel* servicePortLabel = new QLabel(tr("Port"), this);
	QLabel* eventAgentPortLabel = new QLabel(tr("Port"), this);

	servicePortLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	eventAgentPortLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mEventAgentPort->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mServicePort->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* serviceLayout = new QHBoxLayout{};
	serviceLayout->addWidget(mServiceAddress);
	serviceLayout->addSpacing(8);
	serviceLayout->addWidget(servicePortLabel);
	serviceLayout->addWidget(mServicePort);

	mServiceAddress->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mEventAgentAddress->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mServicePort->setRange(0, std::pow(2, 16));
	mEventAgentPort->setRange(0, std::pow(2, 16));

	QHBoxLayout* eventAgentLayout = new QHBoxLayout{};
	eventAgentLayout->addWidget(mEventAgentAddress);
	eventAgentLayout->addSpacing(8);
	eventAgentLayout->addWidget(eventAgentPortLabel);
	eventAgentLayout->addWidget(mEventAgentPort);

	QFormLayout* formLayout = new QFormLayout{this};
	formLayout->addRow(tr("Adres usługi ProductionCenter"), serviceLayout);
	formLayout->addRow(tr("Adres usługi powiadomień"), eventAgentLayout);
	formLayout->addItem(new QSpacerItem(0, 12, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

void RemoteConnectionConfig::createConnections() {
}

} // namespace eSawmill::config
