#include "widgets/configuration/widgets/documentconfig.hpp"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QGroupBox>
#include <QFileDialog>
#include <QSettings>
#include <messagebox.h>


namespace eSawmill {
namespace config {

DocumentConfig::DocumentConfig(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget (parent)
{
	mBrowseButton = new QPushButton(tr("Przeglądaj"), this);
	mBannerView = new QLabel(this);
	mBannerView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	mOrderPrefix = new QLineEdit(this);
	mServicePrefix = new QLineEdit(this);

	QGroupBox* bannerGroup = new QGroupBox(tr("Banner"), this);
	QHBoxLayout* bannerLayout = new QHBoxLayout(bannerGroup);
	bannerLayout->addWidget(mBannerView);
	bannerLayout->addWidget(mBrowseButton);

	QGroupBox* orderGroup = new QGroupBox(tr("Zamówienia"), this);
	QFormLayout* orderLayout = new QFormLayout(orderGroup);
	orderLayout->addRow(tr("Prefix"), mOrderPrefix);

	QGroupBox* serviceGroup = new QGroupBox(tr("Usługi"), this);
	QFormLayout* serviceLayout = new QFormLayout(serviceGroup);
	serviceLayout->addRow(tr("Prefix"), mServicePrefix);

	QHBoxLayout* groupsLayout = new QHBoxLayout;
	groupsLayout->addWidget(orderGroup);
	groupsLayout->addWidget(serviceGroup);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(bannerGroup);
	mainLayout->addLayout(groupsLayout);


	connect(mBrowseButton, &QPushButton::clicked, this, &DocumentConfig::browseButton_clicked);
}

void DocumentConfig::initialize()
{
	QSettings settings;
	QPixmap pixmap;
	if (pixmap.load(settings.value("Document/banner").toString())) {
		mBannerView->setPixmap(pixmap);
	}

	mOrderPrefix->setText(settings.value("Orders/prefix", "ZM").toString());
	mServicePrefix->setText(settings.value("Service/prefix", "UG").toString());
}

void DocumentConfig::save()
{
	QSettings settings;
	settings.setValue("Orders/prefix", mOrderPrefix->text().toUpper());
	settings.setValue("Service/prefix", mServicePrefix->text().toUpper());
}

void DocumentConfig::browseButton_clicked()
{
	const QString fileName = QFileDialog::getOpenFileName(this, tr("Wczytywanie banneru"), QDir::currentPath(), "*.*");
	if (!fileName.isEmpty()) {
		QPixmap pixmap;
		if (pixmap.load(fileName)) {
			mBannerView->setPixmap(pixmap);

			QSettings settings;
			settings.setValue("Document/banner", fileName);
		}
		else {
			::widgets::MessageBox::critical(this, tr("Błąd ładowania banneru"),
											tr("Nie można było wczytać wskazanego banneru do pamięci"));
		}
	}
}

} // namespace config
} // namespace eSawmill
