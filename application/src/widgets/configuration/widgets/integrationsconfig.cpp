#include "widgets/configuration/widgets/integrationsconfig.hpp"
#include <QFormLayout>
#include <QVBoxLayout>
#include "widgets/configuration/widgets/integrations/comarchconfigwidget.hpp"


namespace eSawmill {
namespace config {

IntegrationsConfig::IntegrationsConfig(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	setWindowTitle(tr("Integracje"));

	mTabWidget = new QTabWidget(this);
	mTabWidget->addTab(new integrations::ComarchConfigWidget(this), tr("Comarch ERP"));

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mTabWidget);
}

} // namespace config
} // namesapce eSawmill
