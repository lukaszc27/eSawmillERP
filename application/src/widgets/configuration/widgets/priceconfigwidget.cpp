#include "widgets/configuration/widgets/priceconfigwidget.hpp"
#include <QFormLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QItemSelectionModel>
#include <delegates/woodtypedelegate.hpp>
#include <messagebox.h>
#include <dbo/woodtype.hpp>
//#include <dbo/auth.h>
#include <globaldata.hpp>
#include <settings/appsettings.hpp>


namespace eSawmill {
namespace config {

PriceConfigWidget::PriceConfigWidget(QWidget* parent)
	: eSawmill::widgets::ConfigurationWidget(parent)
{
	setWindowTitle(tr("Cennik"));
	setWindowIcon(QIcon(":/icons/cash"));

	createAllWidgets();
	createModels();
	createConnections();

	if (!core::GlobalData::instance().repository().userRepository().autorizeUser().hasRole(core::Role::SystemRole::Administrator)) {
		// this widget can edit only users who are admin
		setEnabled(false);
	}
}

void PriceConfigWidget::initialize() {
	const core::settings::OrderSettings& orderSettings = core::settings::AppSettings::order();
	mOrderPrice->setValue(orderSettings.defaultPrice());
	mOrderPlannedPrice->setValue(orderSettings.defaultPlannedPrice());
	mServicePrice->setValue(orderSettings.value("Service/defaultPrice").toDouble());
	mServiceRotatedPrice->setValue(orderSettings.value("Service/rotatedPrice").toDouble());
}

void PriceConfigWidget::save() {
	core::settings::OrderSettings& orderSettings = core::settings::AppSettings::order();
	orderSettings.setDefaultPrice(mOrderPrice->value());
	orderSettings.setDefaultPlannedPrice(mOrderPlannedPrice->value());
	orderSettings.setValue("Service/defaultPrice", mServicePrice->value());
	orderSettings.setValue("Service/rotatedPrice", mServiceRotatedPrice->value());
}

void PriceConfigWidget::createAllWidgets()
{
	mOrderPrice = new QDoubleSpinBox(this);
	mOrderPlannedPrice = new QDoubleSpinBox(this);
	mServicePrice = new QDoubleSpinBox(this);
	mServiceRotatedPrice = new QDoubleSpinBox(this);
	mTableView = new ::widgets::TableView(this);
	mAddButton = new ::widgets::PushButton(tr("Dodaj"), QKeySequence("INS"), this, QIcon(":/icons/add"));
	mRemoveButton = new ::widgets::PushButton(tr("Usuń"), QKeySequence("Del"), this, QIcon(":/icons/trash"));

	mTableView->setItemDelegate(new core::delegates::WoodTypeDelegate(this));

	QFormLayout* form = new QFormLayout;
	form->addRow(new QLabel(tr("<b>Zamówienia</b>"), this));
	form->addRow(tr("Cena 1m<sup>3</sup> elementów konstrukcyjnych"), createRow(mOrderPrice, "PLN"));
	form->addRow(tr("Cena 1m<sup>3</sup> struganych elementów"), createRow(mOrderPlannedPrice, "PLN"));
	form->addItem(new QSpacerItem(0, 12, QSizePolicy::Fixed));

	form->addRow(new QLabel(tr("<b>Usługi/Zlecenia</b>"), this));
	form->addRow(tr("Cena za 1m<sup>3</sup>"), createRow(mServicePrice, "PLN"));
	form->addRow(tr("Cena za 1m<sup>3</sup> (kantowane)"), createRow(mServiceRotatedPrice, "PLN"));

	QVBoxLayout* buttonsLayout = new QVBoxLayout;
	buttonsLayout->addWidget(mAddButton);
	buttonsLayout->addWidget(mRemoveButton);
	buttonsLayout->addStretch(1);

	QHBoxLayout* woodTypesLayout = new QHBoxLayout;
	woodTypesLayout->addWidget(mTableView);
	woodTypesLayout->addLayout(buttonsLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(form);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(woodTypesLayout);

	// set maximum value to all fields QDoubleSpinBox
	QList<QDoubleSpinBox*> allSpinsBox = findChildren<QDoubleSpinBox*>();
	for (auto spinBox : allSpinsBox) {
		spinBox->setMaximum(10000);
		spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	}
}

void PriceConfigWidget::createModels() {
	mWoodTypeModel = new core::models::WoodTypeModel(this);
	mTableView->setModel(mWoodTypeModel);
}

void PriceConfigWidget::createConnections() {
	connect(mAddButton, &::widgets::PushButton::clicked, this, &PriceConfigWidget::addNewWoodTypeHandle);
	connect(mRemoveButton, &::widgets::PushButton::clicked, this, &PriceConfigWidget::removeWoodTypeHandle);
}

void PriceConfigWidget::addNewWoodTypeHandle() {
	mWoodTypeModel->insertRows(0, 1);
}

void PriceConfigWidget::removeWoodTypeHandle() {
	QItemSelectionModel* selectionModel = mTableView->selectionModel();
	if (selectionModel->selectedRows().size() > 0) {
		int msgResult = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Znaleziono zaznaczone elementy na liście (%1) czy na pewno chcesz je usunąć?")
			.arg(selectionModel->selectedRows().size()), "",
			QMessageBox::Yes | QMessageBox::No);

		if (msgResult == QMessageBox::Yes) {
			for (auto index : selectionModel->selectedRows()) {
				const core::eloquent::DBIDKey id = index.data(core::models::WoodTypeModel::Id).toInt();
				mWoodTypeModel->destroy(id);
			}
		}
	}
	else {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Przed wykonaniem operacji usuń musisz zaznaczyć które wiersze mają zostać usunięte"));
	}
}

} // namespace config
} // namespace eSawmill
