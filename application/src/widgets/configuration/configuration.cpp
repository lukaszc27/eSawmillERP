#include "widgets/configuration/configuration.h"
#include "widgets/configuration/widgets/documentconfig.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QList>

#include <widgets/orderconfig.h>
#include <widgets/servicesconfig.hpp>
#include <widgets/userconfig.hpp>
#include "widgets/configuration/widgets/companyconfig.hpp"
#include "widgets/configuration/widgets/priceconfigwidget.hpp"
#include "widgets/configuration/widgets/integrationsconfig.hpp"
#include "widgets/configuration/widgets/dataproviderconfig.hpp"

namespace eSawmill {
namespace config {

MainConfiguration::MainConfiguration(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle(tr("Ustawienia"));

	createWidgets();
	createConnections();

	QList<QWidget*> widgets;
	widgets.append(new eSawmill::config::DataProviderConfig(this));
	widgets.append(new config::CompanyConfig(this));
	widgets.append(new account::widgets::UserConfig(this));
	widgets.append(new config::PriceConfigWidget(this));
	widgets.append(new orders::widgets::config::Orders(this));
	widgets.append(new widgets::Services(this));
	widgets.append(new config::IntegrationsConfig(this));

	for (auto* widget : widgets) {
		mStackedWidget->addWidget(widget);

		// add item in left menu
		QListWidgetItem* item = new QListWidgetItem(mListWidget);
		item->setText(widget->windowTitle());
		item->setIcon(widget->windowIcon());

		mListWidget->addItem(item);
	}

	// wczytywanie ustawień przeznaczonych do edycji´
	QList<eSawmill::widgets::ConfigurationWidget*> widgetsToLoadConfig = findChildren<eSawmill::widgets::ConfigurationWidget*>();
	for (auto* widget : widgetsToLoadConfig) {
		connect(this, &MainConfiguration::notifyRepositoryChanged,
			widget, &eSawmill::widgets::ConfigurationWidget::repositoryChanged);

		widget->initialize();
	}
}

void MainConfiguration::accept()
{
	QList<eSawmill::widgets::ConfigurationWidget*> widgets = findChildren<eSawmill::widgets::ConfigurationWidget*>();
	for (auto* widget : widgets)
		widget->save();

	QDialog::accept();
}

void MainConfiguration::createWidgets()
{
	mAcceptButton = new QPushButton(tr("OK"), this);
	mRejectButton = new QPushButton(tr("Anuluj"), this);
	mStackedWidget = new QStackedWidget(this);
	mListWidget = new QListWidget(this);

	mAcceptButton->setIcon(QIcon(":/icons/accept"));
	mRejectButton->setIcon(QIcon(":/icons/reject"));

	mListWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	mStackedWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	QHBoxLayout* stackLayout = new QHBoxLayout;
	stackLayout->addWidget(mListWidget);
	stackLayout->addWidget(mStackedWidget);

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(mAcceptButton);
	buttonLayout->addWidget(mRejectButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(stackLayout);
	mainLayout->addSpacing(12);
	mainLayout->addLayout(buttonLayout);
}

void MainConfiguration::createConnections()
{
	connect(mAcceptButton, &QPushButton::clicked, this, &MainConfiguration::accept);
	connect(mRejectButton, &QPushButton::clicked, this, &MainConfiguration::reject);
	connect(mListWidget, QOverload<int>::of(&QListWidget::currentRowChanged), mStackedWidget, &QStackedWidget::setCurrentIndex);
}

} // namespace config
} // namespace eSawmill
