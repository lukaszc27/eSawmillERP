#include "widgets/aboutapplication.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>

namespace eSawmill {

#ifdef USE_QT6
#include <QQmlContext>

AboutApplication::AboutApplication(QWidget* parent)
	: QDialog {parent}
{
	mQuickWidget = new QQuickWidget(this);
	mQuickWidget->setSource(QUrl("qrc:/qml/aboutapplication"));
	mQuickWidget->setResizeMode(QQuickWidget::ResizeMode::SizeRootObjectToView);
	connect(mQuickWidget, &QQuickWidget::statusChanged, this, [](QQuickWidget::Status status) {
		qDebug() << "Status changed: " << status;
	});

	mCloseButton = new ::widgets::PushButton(tr("OK"), QKeySequence("F10"), this, QIcon(":/icons/accept"));
	connect(mCloseButton, &::widgets::PushButton::clicked, this, &AboutApplication::accept);

	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->setContentsMargins(8, 8, 8, 8);
	buttonsLayout->addStretch(1);
	buttonsLayout->addWidget(mCloseButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->setContentsMargins(0, 0, 0, 0);
	mainLayout->addWidget(mQuickWidget, 1);
	mainLayout->addLayout(buttonsLayout);
}

#else

AboutApplication::AboutApplication(QWidget* parent)
	: QDialog{parent}
{
}

#endif

} // namespace eSawmill
