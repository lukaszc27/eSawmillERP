#include "wizards/connectionWizard/pages/remoteconnectorpage.hpp"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QHostAddress>
#include <messagebox.h>
#include <QSettings>

namespace eSawmill::ConnectionWizard {

RemoteConnectorPage::RemoteConnectorPage(QWizard* parent)
	: QWizardPage {parent}
{
	setTitle(tr("Połączenie ze usługą produkcjną"));
	setSubTitle(tr("Połączenie z usługą ProductionCenter w celu wymiany danych w systemie eSawmillERP"));

	createAllWidgets();

	registerField("ServiceAddress", mServiceAddress);
	registerField("ServicePort", mServicePort);
	registerField("EventAgentAddress", mEventAgentAddress);
	registerField("EventAgentPort", mEventAgentPort);
}

void RemoteConnectorPage::initializePage() {
	QSettings settings;
	mServiceAddress->setText(settings.value("remote/serviceHost").toString());
	mServicePort->setValue(settings.value("remote/serviceHostPort").toInt());
	mEventAgentAddress->setText(settings.value("remote/eventAgent").toString());
	mEventAgentPort->setValue(settings.value("remote/eventAgentPort").toInt());
}

int RemoteConnectorPage::nextId() const {
	return static_cast<int>(CreateConnectionWizard::Pages::AccountInfo);
}

bool RemoteConnectorPage::validatePage() {
	unsigned int servicePort = mServicePort->value();
	unsigned int eventAgentPort = mEventAgentPort->value();

	if (servicePort == eventAgentPort) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Wprowadzone porty dla serwera produkcyjnego oraz powiadomień są takie same!"));
		return false;
	}

	if (QHostAddress eventAgentAddress = QHostAddress(mEventAgentAddress->text()); !eventAgentAddress.isMulticast()) {
		::widgets::MessageBox::warning(this, tr("Ostrzeżenie"),
			tr("Wprowadzony adres usługi powiadomień nie należy do grupy adresów multicast!\n"
				"Przypisz prawidłowy adres z zakresu 239.0.0.0 - 240.255.255.255"));
		return false;
	}

	if (QHostAddress serviceAddress = QHostAddress(mServiceAddress->text()); serviceAddress.isLoopback()) {
		auto ret = ::widgets::MessageBox::warning(
			this, tr("Ostrzeżenie"),
			tr("Wprowadzony adres serwera należy do grupy adresów pętli zwrotnej czy chcesz kontynuować?"),
			tr(""),
			QMessageBox::Yes | QMessageBox::No);

		if (ret == QMessageBox::No)
			return false;
	}
	return true;
}

void RemoteConnectorPage::createAllWidgets() {
	mServiceAddress = new QLineEdit(this);
	mServicePort = new QSpinBox(this);
	mEventAgentAddress = new QLineEdit(this);
	mEventAgentPort = new QSpinBox(this);

	mServicePort->setRange(0, 65535);
	mEventAgentPort->setRange(0, 65535);

	QLabel* description = new QLabel(this);
	description->setText(tr("Wskaż adres hosta na którym działa serwer produkcyjny (ProductionCenter Server)"));
	description->setWordWrap(true);

	QHBoxLayout* serviceAddressLayout = new QHBoxLayout{};
	serviceAddressLayout->addWidget(mServiceAddress, 1);
	serviceAddressLayout->addSpacing(8);
	serviceAddressLayout->addWidget(new QLabel(tr("Port"), this), 0, Qt::AlignRight);
	serviceAddressLayout->addWidget(mServicePort, 0);
	serviceAddressLayout->addStretch(1);

	QHBoxLayout* eventAgentLayout = new QHBoxLayout{};
	eventAgentLayout->addWidget(mEventAgentAddress, 1);
	eventAgentLayout->addSpacing(8);
	eventAgentLayout->addWidget(new QLabel(tr("Port"), this), 0, Qt::AlignRight);
	eventAgentLayout->addWidget(mEventAgentPort, 0);
	eventAgentLayout->addStretch(1);

	QFormLayout* mainLayout = new QFormLayout {this};
	mainLayout->addItem(new QSpacerItem(0, 12, QSizePolicy::Expanding, QSizePolicy::Fixed));
	mainLayout->addRow(description);
	mainLayout->addItem(new QSpacerItem(0, 24, QSizePolicy::Expanding, QSizePolicy::Fixed));
	mainLayout->addRow(tr("Serwer produkcyjny"), serviceAddressLayout);
	mainLayout->addRow(tr("Usługa powiadomień"), eventAgentLayout);
	mainLayout->addItem(new QSpacerItem(0, 1, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

} // namespace eSawmill::ConnectionWizard
