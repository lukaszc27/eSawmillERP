#include "wizards/connectionWizard/pages/connectionselector.hpp"
#include <QLabel>
#include <QVBoxLayout>
#include "wizards/connectionWizard/createconnectionwizard.h"


namespace eSawmill::ConnectionWizard {

ConnectionSelectorPage::ConnectionSelectorPage(QWidget* parent)
	: QWizardPage {parent}
{
	setTitle(tr("Rodzaj pracy aplikacji"));
	setSubTitle(tr("Tryb połączenia do dostawcy danych (tryb pracy jednostanowiskowy lub wielostanowiskowy"));

	createAllWidgets();

	registerField("DatabaseConnection", mDatabaseConnection);
}

int ConnectionSelectorPage::nextId() const {
	if (mRemoteConnection->isChecked())
		return static_cast<int>(CreateConnectionWizard::Pages::RemoteConnection);

	return static_cast<int>(CreateConnectionWizard::Pages::DbConnection);
}

bool ConnectionSelectorPage::validatePage() {
	return mDatabaseConnection->isChecked()
			|| mRemoteConnection->isChecked();
}

void ConnectionSelectorPage::createAllWidgets() {
	mDatabaseConnection = new QRadioButton(tr("Praca jednostanowiskowa"), this);
	mRemoteConnection = new QRadioButton(tr("Praca wielostanowiskowa"), this);

	QLabel* description = new QLabel(this);
	description->setWordWrap(true);
	description->setText(tr("Wybierz rodzaj pracy aplikacji klienckiej. Wszystkie aplikacje działające w systemie powinny pracować w tym samym trybie aby zachować spójność danych."
					 "W przypadku pracy kulku użytkowników zalecany jest wybór pracy wielostanowiskowej."));

	QVBoxLayout* layout = new QVBoxLayout {this};
	layout->addWidget(description);
	layout->addSpacing(12);
	layout->addWidget(mDatabaseConnection);
	layout->addWidget(mRemoteConnection);
	layout->addStretch(1);
}

} // namespace eSawmill::ConnectionWizard

