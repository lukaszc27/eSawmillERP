#include "wizards/connectionWizard/pages/authpage.h"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <QLabel>
#include <QFormLayout>


namespace eSawmill {
namespace ConnectionWizard {

AuthPage::AuthPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Dane do logowania w systemie eSawmill ERP"));
	createWidgets();

	registerField("ERP_UserName", mUserName);
	registerField("ERP_Password", mPassword);
	registerField("Firstname", mFirstname);
	registerField("Surname", mSurname);
}

void AuthPage::createWidgets()
{
	mUserName = new QLineEdit(this);
	mPassword = new QLineEdit(this);
	mConfirmPassword = new QLineEdit(this);
	mFirstname = new QLineEdit(this);
	mSurname = new QLineEdit(this);

	mPassword->setEchoMode(QLineEdit::Password);
	mConfirmPassword->setEchoMode(QLineEdit::Password);

	QFormLayout* mainLayout = new QFormLayout(this);
	mainLayout->addRow(tr("Nazwa użytkownika"), mUserName);
	mainLayout->addRow(tr("Hasło"), mPassword);
	mainLayout->addRow(tr("Powtórz hasło"), mConfirmPassword);
	mainLayout->addItem(new QSpacerItem(0, 3));
	mainLayout->addRow(new QLabel("Dane dotyczące użytkownika", this));
	mainLayout->addRow("Imię", mFirstname);
	mainLayout->addRow("Nazwisko", mSurname);
}

bool AuthPage::validatePage()
{
	return !mUserName->text().isEmpty() &&
			!mPassword->text().isEmpty() &&
			(mPassword->text() == mConfirmPassword->text());
}

void AuthPage::cleanupPage()
{
	mPassword->setText("");
	mConfirmPassword->setText("");
}

}
}
