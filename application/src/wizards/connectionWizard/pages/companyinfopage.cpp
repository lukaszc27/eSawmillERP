#include "wizards/connectionWizard/pages/companyinfopage.h"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <QVBoxLayout>


namespace eSawmill {
namespace ConnectionWizard {

CompanyInfoPage::CompanyInfoPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("eSawmill ERP - Zaczynamy!"));
	setSubTitle(tr("Konto firmowe w programie eSawmill EPR"));

	createWidgets();

	registerField("CreateAccount", mCreateAccountButton);
}

void CompanyInfoPage::createWidgets()
{
	mInfoLabel = new QLabel(this);
	mAccountExistRadioButton = new QRadioButton(tr("Posiadam konto w programie eSawmill ERP"), this);
	mCreateAccountButton = new QRadioButton(tr("Utwórz konto firmowe w programie eSawmill ERP"), this);

	mInfoLabel->setText(tr("Prawdopodobnie jest to twoje pierwsze uruchomienie aplikacji "
						   "eSawmill ERP. Aby móc kożystać z programu bez żadnych ograniczeń "
						   "konieczne będzie utworzenie konta firmowego. Jeśli kożystałeś "
						   "wcześniej z aplikacji na tym komputerze najprawdopodobniej "
						   "posiadasz już konto i nie ma potrzeby zakładania nowego profilu firmowego."
						   "\n\n"
						   "Wybierz jedną z opcji"));

	mInfoLabel->setWordWrap(true);
	mCreateAccountButton->setChecked(true);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mInfoLabel);
	mainLayout->addWidget(mCreateAccountButton);
	mainLayout->addWidget(mAccountExistRadioButton);
}

int CompanyInfoPage::nextId() const
{
	if (mAccountExistRadioButton->isChecked())
		return CreateConnectionWizard::Pages::End;

	return CreateConnectionWizard::Pages::Contact;
}

} // namespace ConnectionWizard
} // namespace eSawmill
