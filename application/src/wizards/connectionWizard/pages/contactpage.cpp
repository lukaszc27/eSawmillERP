#include "wizards/connectionWizard/pages/contactpage.h"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <models/stateslistmodel.hpp>
#include <QLabel>
#include <QHBoxLayout>
#include <QFormLayout>

namespace eSawmill {
namespace ConnectionWizard {

ContactPage::ContactPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle("Firmowe dane teleadresowe");
	createWidgets();

	registerField("Country", mCountry);
	registerField("CountryIso", mCountryIso);
	registerField("City", mCity);
	registerField("State", mState);
	registerField("Street", mStreet);
	registerField("PostCode", mPostCode);
	registerField("PostOffice", mPostOffice);
	registerField("HomeNumber", mHomeNumber);
	registerField("LocalNumber", mLocalNumber);
	registerField("Phone", mPhone);
	registerField("Email", mEmail);
	registerField("Url", mUrl);
}

void ContactPage::createWidgets()
{
	mCountry = new QLineEdit(this);
	mCountryIso = new QLineEdit(this);
	mCity = new QLineEdit(this);
	mState = new QComboBox(this);
	mPostOffice = new QLineEdit(this);
	mPostCode = new QLineEdit(this);
	mStreet = new QLineEdit(this);
	mHomeNumber = new QLineEdit(this);
	mLocalNumber = new QLineEdit(this);
	mPhone = new QLineEdit(this);
	mEmail = new QLineEdit(this);
	mUrl = new QLineEdit(this);

	mCountryIso->setMaximumWidth(36);
	mCountryIso->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCountry->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPostCode->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mHomeNumber->setMaximumWidth(64);
	mLocalNumber->setMaximumWidth(64);
	mHomeNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mLocalNumber->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mPhone->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mState->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mState->setEditable(true);
	mState->setModel(new core::models::StatesListModel(this));

	// set input mask for fields
	mCountryIso->setInputMask("AA;_");
	mPostCode->setInputMask("99-999;_");
	mPhone->setInputMask("+99 999999999;_");

	// set default values
	mCountry->setText("Polska");
	mCountryIso->setText("PL");
	mPhone->setText("48");

	QFormLayout* left = new QFormLayout;
	left->addRow("Kraj", mCountry);
	left->addRow("Miasto", mCity);
	left->addRow("Poczta", mPostOffice);
	left->addRow("Ulica", mStreet);
	left->addRow("Telefon", mPhone);

	QHBoxLayout* homeNumberLayout = new QHBoxLayout;
	homeNumberLayout->addWidget(mHomeNumber);
	homeNumberLayout->addWidget(new QLabel("/", this));
	homeNumberLayout->addWidget(mLocalNumber);
	homeNumberLayout->addStretch(1);

	QFormLayout* right = new QFormLayout;
	right->addRow("Kraj ISO", mCountryIso);
	right->addRow("Województwo", mState);
	right->addRow("Kod pocztowy", mPostCode);
	right->addRow("", homeNumberLayout);
	right->addRow("E-Mail", mEmail);
	right->addRow("URL", mUrl);

	QHBoxLayout* mainLayout = new QHBoxLayout(this);
	mainLayout->addLayout(left);
	mainLayout->addSpacing(6);
	mainLayout->addLayout(right);
}

bool ContactPage::validatePage()
{
	bool countryInvalid = !mCountry->text().isEmpty();
	bool countryIsoInvalid = !mCountryIso->text().isEmpty();
	bool cityInvalid = !mCity->text().isEmpty();
	bool stateInvalid = !mState->currentText().isEmpty();
	bool streetInvalid = !mStreet->text().isEmpty();
	bool homeNumberInvalid = !mHomeNumber->text().isEmpty() || !mLocalNumber->text().isEmpty();
	bool phoneInvalid = !mPhone->text().isEmpty() && mPhone->text().length() > 3;

	mCountry->setProperty("invalid", countryInvalid);
	mCountryIso->setProperty("invalid", countryIsoInvalid);
	mCity->setProperty("invalid", cityInvalid);
	mState->setProperty("invalid", stateInvalid);
	mStreet->setProperty("invalid", streetInvalid);
	mHomeNumber->setProperty("invalid", homeNumberInvalid);
	mPhone->setProperty("invalid", phoneInvalid);

	return countryInvalid && countryIsoInvalid && cityInvalid && stateInvalid &&
			streetInvalid && homeNumberInvalid && phoneInvalid;
}

}
}
