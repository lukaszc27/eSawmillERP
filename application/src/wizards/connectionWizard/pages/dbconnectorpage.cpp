#include "wizards/connectionWizard/pages/dbconnectorpage.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QMessageBox>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSysInfo>
#include "wizards/connectionWizard/createconnectionwizard.h"

#include <messagebox.h>	// moduł widgets
#include <simplecrypt.hpp>


namespace eSawmill {
namespace ConnectionWizard {

DbConnectorPage::DbConnectorPage(QWidget* parent)
	: QWizardPage(parent)
	, bTestConnectionComplete(false)
{
	this->setTitle(tr("Dane autoryzaji do serwera MS SQL"));
	this->createWidgets();

	registerField("DriverName", mDriverName);
	registerField("HostName", mHostName);
	registerField("DatabaseName", mDatabaseName);
	registerField("UserName", mUserName);
	registerField("UserPassword", mUserPassword);
	registerField("TrustedConnection", mTrustedConnection);

	connect(mTestConnButton, &QPushButton::clicked,
			this, &DbConnectorPage::testConnectionButton_Click);
}

int DbConnectorPage::nextId() const {
	return static_cast<int>(CreateConnectionWizard::Pages::AccountInfo);
}

void DbConnectorPage::createWidgets()
{
	mAuthTypeGroup = new QGroupBox(tr("Sposób autoryzacji"), this);
	mSqlServerAuth = new QRadioButton(tr("Autoryzacja SQL"), this);
	mWindowsAuth = new QRadioButton(tr("Autoryzacja Windows"), this);

	QHBoxLayout* authGroupLayout = new QHBoxLayout(mAuthTypeGroup);
	authGroupLayout->addWidget(mSqlServerAuth);
	authGroupLayout->addSpacing(32);
	authGroupLayout->addWidget(mWindowsAuth);
	authGroupLayout->addStretch(1);

	mDriverName = new QLineEdit(this);
	mHostName = new QLineEdit(this);
	mDatabaseName = new QLineEdit(this);
	mUserName = new QLineEdit(this);
	mUserPassword = new QLineEdit(this);
	mTrustedConnection = new QCheckBox(tr("Trusted Connection"), this);

	mHostName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDatabaseName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserName->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserPassword->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mUserPassword->setEchoMode(QLineEdit::Password);

	mTestConnButton = new QPushButton(QIcon(":/icons/database"), tr("Test połączenia"), this);
	mTestConnButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QFormLayout* inputFormLayout = new QFormLayout;
	inputFormLayout->addRow(tr("Sterownik ODBC"), mDriverName);
	inputFormLayout->addRow(tr("Host"), mHostName);
	inputFormLayout->addRow(tr("Daza danych"), mDatabaseName);
	inputFormLayout->addRow(tr("Użytkownik"), mUserName);
	inputFormLayout->addRow(tr("Hasło"), mUserPassword);
	inputFormLayout->addItem(new QSpacerItem(0, 3));
	inputFormLayout->addWidget(mTrustedConnection);
	inputFormLayout->addItem(new QSpacerItem(0, 8));
	inputFormLayout->addWidget(mTestConnButton);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mAuthTypeGroup);
	mainLayout->addLayout(inputFormLayout);
}

void DbConnectorPage::initializePage()
{
	mDriverName->setPlaceholderText(tr("ODBC Driver 17 for SQL Server"));
	mHostName->setPlaceholderText(tr("localhost"));
	mDatabaseName->setPlaceholderText(tr("SAWMILL"));
	mUserName->setPlaceholderText(tr("SA"));
	mUserPassword->setPlaceholderText(tr("Hasło"));

	// set data from config if dialog is open in next time
	QSettings settings;
	mDriverName->setText(settings.value("database/driver", "ODBC Driver 17 for SQL Server").toString());
	mHostName->setText(settings.value("database/host", "localhost").toString());
	mDatabaseName->setText(settings.value("database/name").toString());
	mUserName->setText(settings.value("database/userName", "SA").toString());

	// decrypt password from settings
	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	mUserPassword->setText(crypt.decryptToString(settings.value("database/userPassword").toString()));

#ifdef Q_OS_WINDOWS
	mWindowsAuth->setChecked(true);
	mDriverName->setEnabled(true);
	mDatabaseName->setEnabled(true);
//	mUserName->setEnabled(false);
//	mUserPassword->setEnabled(false);
	mTrustedConnection->setEnabled(true);

	mHostName->setText(QString("%1\\SQLEXPRESS").arg(QSysInfo::machineHostName()));
	mTrustedConnection->setChecked(true);
#else
	mSqlServerAuth->setChecked(true);
	mWindowsAuth->setDisabled(true);
#endif
}

bool DbConnectorPage::validatePage()
{
	if (mWindowsAuth->isChecked())
		return true;

	return mSqlServerAuth->isChecked() && !mDriverName->text().isEmpty() && !mDatabaseName->text().isEmpty() &&
			!mUserName->text().isEmpty() && !mUserPassword->text().isEmpty() && bTestConnectionComplete == true;
}

void DbConnectorPage::testConnectionButton_Click()
{
	QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
	QString connectionString;
	if (mSqlServerAuth->isChecked()) {
		// połączenie z systemów Linux oraz macOS
		connectionString = QString("DRIVER={%1};SERVER=%2;DATABASE=%3;UID=%4;PWD=%5;")
				.arg(mDriverName->text(),
					 mHostName->text(),
					 mDatabaseName->text(),
					 mUserName->text(),
					 mUserPassword->text());

		if (mTrustedConnection->isChecked())
			connectionString.append("Trusted_Connection=Yes;");
		else connectionString.append("Trusted_Connection=No;");
	}
	else {
		// połączenie z systemu Windows
		connectionString = QString("DRIVER={%1};Server=%2;Database=%3;")
				.arg(mDriverName->text(),
					 mHostName->text(),
					 mDatabaseName->text());

		if (mTrustedConnection->isChecked())
			connectionString.append("Trusted_Connection=Yes;");
		else connectionString.append("Trusted_Connection=No;");
	}

	db.setDatabaseName(connectionString);
	if (db.isValid() && db.open()) {
		::widgets::MessageBox::information(this, tr("Sukces"),
			tr("Połączenie z serwerem Microsoft SQL Server zostało nawiązanie prawidłowo"));
		bTestConnectionComplete = true;
	}
	else {
		::widgets::MessageBox::critical(this, tr("Błąd"),
										tr("Błąd nawiązywania połączenia z serwerem Microsoft SQL Server"),
										db.lastError().text());
		bTestConnectionComplete = false;
	}

	// zamykamy połączenie z bazą danych
	if (db.isOpen())
		db.close();
}

} // namespace ConnectionWizard
} // namespace eSawmill
