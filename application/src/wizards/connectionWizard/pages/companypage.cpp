#include "wizards/connectionWizard/pages/companypage.h"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <QLabel>
#include <QFormLayout>


namespace eSawmill {
namespace ConnectionWizard {

CompanyPage::CompanyPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Dane firmowe"));

	createWidgets();

	registerField("CompanyName", mName);
	registerField("NIP", mNip);
	registerField("REGON", mRegon);
	registerField("KRS", mKrs);
}

void CompanyPage::createWidgets()
{
	mName = new QLineEdit(this);
	mNip = new QLineEdit(this);
	mRegon = new QLineEdit(this);
	mKrs = new QLineEdit(this);

	mNip->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mRegon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mKrs->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	mNip->setInputMask("9999999999;_");

	QFormLayout* formLayout = new QFormLayout(this);
	formLayout->addRow(tr("Nazwa"), mName);
	formLayout->addRow(tr("NIP"), mNip);
	formLayout->addRow(tr("REGON"), mRegon);
	formLayout->addRow(tr("KRS"), mKrs);
}

bool CompanyPage::validatePage()
{
	return !mName->text().isEmpty() &&
			!mNip->text().isEmpty();
}

}
}
