#include "wizards/connectionWizard/pages/endpage.h"
#include <QLabel>
#include <QVBoxLayout>


namespace eSawmill {
namespace ConnectionWizard {

EndPage::EndPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Konfiguracja programu eSawmill dobiegła końca"));

	QLabel* label = new QLabel(this);
	label->setText(tr("Program eSawmill ERP jest gotowy do użycia. Dziękujemy za wybranie nasej aplikacji."
					  "Wiecej informacji uzyskasz w dziale pomocy lub na stronie internetowej."));

	label->setWordWrap(true);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(label);
}

}
}
