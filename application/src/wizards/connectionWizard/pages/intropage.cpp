#include "wizards/connectionWizard/pages/intropage.h"
#include <QTextStream>
#include <QVBoxLayout>


namespace eSawmill {
namespace ConnectionWizard {

IntroPage::IntroPage(QWidget* parent)
	: QWizardPage(parent)
{
	this->setTitle(tr("eSawmill ERP - Zapraszamy!"));

	this->createWidgets();
}

void IntroPage::createWidgets()
{
	mWelocomeText = new QLabel(this);

	QString welocomeText;
	QTextStream out(&welocomeText);

	out << tr("Jest to prawdopodobnie twoje pierwsze uruchomienie aplikacji\r\neSawmill ERP ")
		<< tr("i nie posiadasz jeszcze skonfigurowanego połączenia z\r\nserwerem bazy danych. ")
		<< tr("Aby ta czynność zakończyła się szybko\r\npowodzeniem proszę przygotuj następujące dane:\r\n\n")
		<< tr("\r\nNazwa sterownika ODBC\r\nNazwa użytkownika\r\nHasło\r\nNazwa bazy danych z którą będziesz się łączył")
		<< tr("\r\n\n\nA następnie przejdź do następnego kroku");

	mWelocomeText->setText(welocomeText);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(mWelocomeText);
}

} // namespace ConnectionWizard
} // namespace eSawmill
