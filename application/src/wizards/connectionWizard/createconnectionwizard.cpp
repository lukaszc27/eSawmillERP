#include "wizards/connectionWizard/createconnectionwizard.h"
#include "wizards/connectionWizard/pages/contactpage.h"
#include "wizards/connectionWizard/pages/companypage.h"
#include "wizards/connectionWizard/pages/authpage.h"
#include "wizards/connectionWizard/pages/intropage.h"
#include "wizards/connectionWizard/pages/dbconnectorpage.h"
#include "wizards/connectionWizard/pages/companyinfopage.h"
#include "wizards/connectionWizard/pages/endpage.h"
#include "wizards/connectionWizard/pages/connectionselector.hpp"
#include "wizards/connectionWizard/pages/remoteconnectorpage.hpp"

namespace eSawmill {
namespace ConnectionWizard {

CreateConnectionWizard::CreateConnectionWizard(QWidget* parent)
	: QWizard(parent)
{
	setPage(Pages::Intro, new IntroPage(this));
	setPage(Pages::ConnectionSelector, new ConnectionSelectorPage(this));
	setPage(Pages::DbConnection, new DbConnectorPage(this));
	setPage(Pages::RemoteConnection, new RemoteConnectorPage(this));
	setPage(Pages::AccountInfo, new CompanyInfoPage(this));
	setPage(Pages::Contact, new ContactPage(this));
	setPage(Pages::Company, new CompanyPage(this));
	setPage(Pages::Auth, new AuthPage(this));
	setPage(Pages::End, new EndPage(this));

	button(QWizard::WizardButton::CancelButton)->setIcon(QIcon(":/icons/cancel"));
	button(QWizard::WizardButton::CancelButton)->setText(tr("Anuluj"));
	button(QWizard::WizardButton::NextButton)->setIcon(QIcon(":/icons/next"));
	button(QWizard::WizardButton::NextButton)->setText(tr("Dalej"));
	button(QWizard::WizardButton::BackButton)->setIcon(QIcon(":/icons/back"));
	button(QWizard::WizardButton::BackButton)->setText(tr("Wstecz"));
}

} // namespace ConnectionWizard
} // namespace eSawmill
