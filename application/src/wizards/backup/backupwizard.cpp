#include "wizards/backup/backupwizard.hpp"
#include "wizards/backup/pages/intro.hpp"
#include "wizards/backup/pages/createbackup.hpp"
#include "wizards/backup/pages/restore.hpp"


namespace eSawmill {
namespace BackupWizard {

BackupWizard::BackupWizard(QWidget* parent)
	: QWizard(parent)
{
	setWindowTitle(tr("Kopia bezpieczeństwa"));

	setPage(WizardPages::Intro, new Pages::IntroPage(this));
	setPage(WizardPages::CreateBackup, new Pages::CreateBackup(this));
	setPage(WizardPages::RestoreBackup, new Pages::RestorePage(this));
}

} // namespace BackupWizard
} // namespace eSawmill
