#include "wizards/backup/pages/intro.hpp"
#include "wizards/backup/backupwizard.hpp"
#include <QVBoxLayout>


namespace eSawmill {
namespace BackupWizard {
namespace Pages {

IntroPage::IntroPage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Kreator kopii bezpieczeństwa"));
	setSubTitle(tr("Wybierz co chcesz zrobić"));

	createAllWidgets();
}

int IntroPage::nextId() const
{
	if (mCreateBackup->isChecked())
		return eSawmill::BackupWizard::BackupWizard::WizardPages::CreateBackup;

	return eSawmill::BackupWizard::BackupWizard::WizardPages::RestoreBackup;
}

void IntroPage::createAllWidgets()
{
	mCreateBackup = new QRadioButton(tr("Utwórz kopię bezpieczeństwa"), this);
	mRestoreBackup = new QRadioButton(tr("Przywróć dane z kopii bezpieczeństwa"), this);

	mCreateBackup->setChecked(true);

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addStretch(1);
	mainLayout->addWidget(mCreateBackup);
	mainLayout->addWidget(mRestoreBackup);
	mainLayout->addStretch(2);
}

} // namespace Pages
} // namespace BackupWizard
} // namespace eSawmill
