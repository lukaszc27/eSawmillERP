#include "wizards/backup/pages/restore.hpp"
#include <database.h>
#include <messagebox.h>

#include <QSettings>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLayoutItem>
#include <QGroupBox>
#include <QFileDialog>
#include <messagebox.h>


namespace eSawmill::BackupWizard::Pages {

RestorePage::RestorePage(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Kreator kopii bezpieczeństwa"));
	setSubTitle(tr("Odczytaj dane z kopii bezpieczeństwa"));

	createAllWidgets();

	connect(mBrowseButton, &QPushButton::clicked, this, &RestorePage::browseButtonHandle);
	connect(mRestoreButton, &QPushButton::clicked, this, &RestorePage::restoreButtonHandle);
}

void RestorePage::createAllWidgets()
{
	mLocation = new QLineEdit(this);
	mBrowseButton = new QPushButton("...", this);
	mRestoreButton = new QPushButton("Odczytaj dane", this);

	mLocation->setReadOnly(true);
	mBrowseButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mBrowseButton->setMaximumWidth(36);
	mRestoreButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	QHBoxLayout* locationLayout = new QHBoxLayout;
	locationLayout->addWidget(mLocation);
	locationLayout->addWidget(mBrowseButton);

	QFormLayout* layout = new QFormLayout(this);
	layout->addRow("Plik kopii bezpieczeństwa", locationLayout);
	layout->addItem(new QSpacerItem(0, 6));
	layout->addWidget(mRestoreButton);
}

void RestorePage::browseButtonHandle()
{
	QSettings settings;
	const QString filename = QFileDialog::getOpenFileName(this, "Odczyt pliku kopii bezpieczeństwa",
														  settings.value("database/backupPath").toString(),
														  "BAK (*.bak);;BAC (*.bac);;Wszystkie pliki (*.*)");
	if (!filename.isEmpty()) {
		mLocation->setText(filename);
	}
}

void RestorePage::restoreButtonHandle()
{
	if (mLocation->text().isEmpty()) {
		::widgets::MessageBox::warning(this, "Ostrzeżenie", "Nie można odczytać kopii bezpieczeństwa ponieważ plik nie został jeszcze wskazany");
		return;
	}

	if (!core::Database::restore(mLocation->text())) {
		::widgets::MessageBox::critical(this, "Bład kopii bezpieczeństwa", "Odtwarzanie danych z kpoii bezpieczeństwa nie może być ukończone z powodu błędów!");
		return;
	}

	::widgets::MessageBox::information(this, "Przywracanie kopii bezpieczeństwa",
									   "Baza danych została przuwrucona z kopii bezpieczeństwa");
}

} // namespace
