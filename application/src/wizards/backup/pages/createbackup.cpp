#include "wizards/backup/pages/createbackup.hpp"
#include <messagebox.h>
#include <database.h>

#include <QFormLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QApplication>
#include <QSpacerItem>
#include <QFileSystemModel>
#include <QCompleter>


namespace eSawmill {
namespace BackupWizard {
namespace Pages {

CreateBackup::CreateBackup(QWidget* parent)
	: QWizardPage(parent)
{
	setTitle(tr("Kreator kopii bezpieczeństwa"));
	setSubTitle(tr("Tworzenie kopii bezpieczeństwa"));

	createAllWidgets();
	createConnections();
}

void CreateBackup::initializePage() {
	QSettings settings;
	mLocation->setText(settings.value("database/backupPath").toString());
}

void CreateBackup::cleanupPage() {
	QSettings settings;
	settings.setValue("database/backupPath", mLocation->text());
}

void CreateBackup::createAllWidgets()
{
	mName = new QLineEdit(this);
	mDescription = new QPlainTextEdit(this);
	mLocation = new QLineEdit(this);
	mBrowseButton = new QPushButton("...", this);
	mFilename = new QLineEdit(this);
	mCreateButton = new QPushButton("Utwórz", this);

	mBrowseButton->setMaximumWidth(36);
	mBrowseButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mCreateButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	mDescription->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	QHBoxLayout* locationLayout = new QHBoxLayout;
	locationLayout->addWidget(mLocation);
	locationLayout->addWidget(mBrowseButton);

	auto model = new QFileSystemModel(this);
	model->setRootPath(QString());

	QCompleter* locationCompleter = new QCompleter(this);
	locationCompleter->setModel(model);
	locationCompleter->setCompletionMode(QCompleter::CompletionMode::PopupCompletion);
	mLocation->setCompleter(locationCompleter);

	const QString filename = QString("eSawmill_backup_%1_%2.bak")
			.arg(QDate::currentDate().toString("ddMMyyyy"), QTime::currentTime().toString("hhmmss"));

	mFilename->setText(filename);
	mName->setText("Kopia bezpieczeństwa bazy eSawmillERP");

	QFormLayout* layout = new QFormLayout(this);
	layout->addRow("Nazwa", mName);
	layout->addRow("Opis", mDescription);
	layout->addRow("Nazwa pliku", mFilename);
	layout->addRow("Lokalizacja", locationLayout);
	layout->addItem(new QSpacerItem(0, 8));
	layout->addRow("", mCreateButton);
}

void CreateBackup::createConnections()
{
	connect(mBrowseButton, &QPushButton::clicked, this, &CreateBackup::locationButtonHandle);
	connect(mCreateButton, &QPushButton::clicked, this, &CreateBackup::createButtonHandle);
}

void CreateBackup::createButtonHandle()
{
	QString path;
	if (mLocation->text().endsWith('/'))
		path = mLocation->text();
	else {
		path = mLocation->text();
		path.append('/');
	}

	path.append(mFilename->text());

	if (core::Database::createBackup(path, mName->text(), mDescription->document()->toPlainText())) {
		::widgets::MessageBox::information(this, "Informacja",
			"Kopa bezpieczeństwa została utworzona");
	}
	else {
		::widgets::MessageBox::critical(this, "Błąd",
			"Tworzenie kopii bezpieczeństwa nie powiodło się. "
			"Prawdopodobnie nie posiadasz uprawnień do zapisu w wybranym katalogu");
	}
}

void CreateBackup::locationButtonHandle()
{
	const QString path = QFileDialog::getExistingDirectory(this,
		"Wskaż folder do zapisu", QDir::currentPath());

	if (path.length())
		mLocation->setText(path);
}

} // namespace Pages
} // namespace BackupWizard
} // namespace eSawmill
