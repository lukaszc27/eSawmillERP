#include "mainwindow.h"

#include <QTimer>
#include <QMessageBox>
#include <QMenu>
#include <QMenuBar>
#include <QtXml>
#include <QApplication>
#include <QThread>
#include <QDockWidget>
#include <QProgressDialog>
#include <QShortcut>
#include <QPointer>
#include <simplecrypt.hpp>
#include <settings/appsettings.hpp>
#include <verifications/unrealisedordersverification.hpp>
#include <verifications/saleinvociesverification.hpp>
#include <verifications/purchaseinvoicesverification.hpp>
#include "contractormanager.h"
#include "widgets/configuration/configuration.h"
#include "wizards/backup/backupwizard.hpp"
#include "widgets/aboutapplication.hpp"
#include "sales/salesinvoicesmanager.hpp"
#include "purchase/purchaseinvoicesmanager.hpp"
#include <dbo/auth.h>
#include <messagebox.h>
#include <simplecrypt.hpp>
#include <globaldata.hpp>
#include <repository/database/databaserepository.hpp>
#include <eventagentclient.hpp>
#include <unordered_map>

namespace eSawmill {

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, mOrderModuleActive(false)
	, mServiceModuleActive(false)
	, mArticleModuleActive(false)
	, logger_{core::GlobalData::instance().logger()}
{
	setWindowIcon(QIcon(":/icons/appicon.ico"));

	/// create system tray if is supported by target platform
	if (QSystemTrayIcon::isSystemTrayAvailable()) {
		logger_.debug("Create system tray");
		mSystemTray = new QSystemTrayIcon(QIcon(":/icons/appicon.ico"), this);
		mSystemTray->setToolTip(tr("eSawmill Event System"));
		mSystemTray->show();
	} else {
		qInfo() << "Docelowa platforma nie wspiera powiadomień przez system tray";
	}

	createEventAgent();

	createActions();
	createMenus();
	createConnections();
	createWidgets();
	createDockWidgets();

	parseLicenceFile();
	registerVerifications();

	// at the end attach listener
	core::GlobalData::instance().commandExecutor().attachListener(this);

	QTimer::singleShot(200, this, &MainWindow::authenticateUserHandle);
}

MainWindow::~MainWindow()
{
	core::GlobalData::instance().commandExecutor().detachListener(this);

	destroyEventAgent();
}

void MainWindow::stackChanged(bool canUndo) {
	if (mUndoAction != nullptr)
		mUndoAction->setEnabled(canUndo);
}

void MainWindow::notifyEventReceived(const agent::RepositoryEvent& event) {
	{ // skip current loggin user at notification
		auto& userRepository = core::GlobalData::instance().repository().userRepository();
		const core::User& user = userRepository.autorizeUser();
		if (user.id() > 0 && event.userId == user.id())
			return;	// don't show events from my server operation
	}

	//////////////////////////////////////////////////////////
	// update cache memory
	if (event.operation == agent::ResourceOperation::Created ||
		event.operation == agent::ResourceOperation::Updated)
	{
		Q_ASSERT(event.resourceId > 0);
		switch (event.resourceType) {
		case agent::Resource::Contractor: {
			auto& repository = core::GlobalData::instance().repository().contractorRepository();
			repository.updateCache(event.resourceId);
		} break;

		case agent::Resource::Article: {
			auto& repository = core::GlobalData::instance().repository().articleRepository();
			repository.updateCache(event.resourceId);
		} break;

		case agent::Resource::Warehouse: {
			auto& repository = core::GlobalData::instance().repository().warehousesRepository();
			repository.updateCache(event.resourceId);
		} break;

		case agent::Resource::SaleInvoice: {
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			repository.updateSaleInvoiceCache(event.resourceId);
		} break;

		case agent::Resource::PurchaseInvoice: {
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			repository.updatePurchaseInvoiceCache(event.resourceId);
		} break;

		case agent::Resource::Order: {
			auto& repository = core::GlobalData::instance().repository().orderRepository();
			repository.updateOrderInCache(event.resourceId);
		} break;

		case agent::Resource::Service: {
			auto& repository = core::GlobalData::instance().repository().serviceRepository();
			repository.updateServiceInCache(event.resourceId);
		} break;

		case agent::Resource::User: {
			auto& repository = core::GlobalData::instance().repository().userRepository();
			repository.updateCache(event.resourceId);
		} break;

		case agent::Resource::Company:
			break;
		} // switch
	}

	if (event.operation == agent::ResourceOperation::Removed) {
		Q_ASSERT(event.resourceId > 0);
		switch (event.resourceType) {
		case agent::Resource::Contractor: {
			auto& repository = core::GlobalData::instance().repository().contractorRepository();
			repository.removeFromCache(event.resourceId);
		} break;

		case agent::Resource::Article: {
			auto& repository = core::GlobalData::instance().repository().articleRepository();
			repository.removeFromCache(event.resourceId);
		} break;

		case agent::Resource::Warehouse: {
			auto& repository = core::GlobalData::instance().repository().warehousesRepository();
			repository.removeFromCache(event.resourceId);
		} break;

		case agent::Resource::SaleInvoice: {
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			repository.removeSaleInvoiceFromCache(event.resourceId);
		} break;

		case agent::Resource::PurchaseInvoice: {
			auto& repository = core::GlobalData::instance().repository().invoiceRepository();
			repository.removePurchaseInvoiceFromCache(event.resourceId);
		} break;

		case agent::Resource::Order: {
			auto& repository = core::GlobalData::instance().repository().orderRepository();
			repository.removeFromCache(event.resourceId);
		} break;

		case agent::Resource::Service: {
			auto& repository = core::GlobalData::instance().repository().serviceRepository();
			repository.removeServiceFromCache(event.resourceId);
		} break;

		case agent::Resource::User: {
				auto& repository = core::GlobalData::instance().repository().userRepository();
				repository.removeFromCache(event.resourceId);
		} break;

		case agent::Resource::Company:
			break;
		} // switch
	}
	//////////////////////////////////////////////////////////


	if (event.operation == agent::ResourceOperation::Logged
		|| event.operation == agent::ResourceOperation::Logout)
	{
		core::User loggedUser = core::GlobalData::instance().repository().userRepository().getUser(event.userId);
		mSystemTray->showMessage(tr("eSawmillERP zdarzenia"),
			event.operation == agent::ResourceOperation::Logged
				? tr("%1 %2 zalogował się do systemu").arg(loggedUser.firstName(), loggedUser.surName())
				: tr("%1 %2 wylogował się z systemu").arg(loggedUser.firstName(), loggedUser.surName()));
	}

	static std::unordered_map<agent::ResourceOperation, QString> operationsName;
	operationsName[agent::ResourceOperation::Logged] = tr("Zalogowano");
	operationsName[agent::ResourceOperation::Logout] = tr("Wylogowano");
	operationsName[agent::ResourceOperation::Created] = tr("Utworzono");
	operationsName[agent::ResourceOperation::Updated] = tr("Zaaktualizowano");
	operationsName[agent::ResourceOperation::Removed] = tr("Usunięto");

	static std::unordered_map<agent::Resource, QString> resourcesName;
	resourcesName[agent::Resource::Article] = tr("artykuł");
	resourcesName[agent::Resource::Company] = tr("firmę");
	resourcesName[agent::Resource::Contractor] = tr("kontrahenta");
	resourcesName[agent::Resource::Order] = tr("zamówienie");
	resourcesName[agent::Resource::PurchaseInvoice] = tr("fakturę zakupu");
	resourcesName[agent::Resource::SaleInvoice] = tr("fakturę sprzedaży");
	resourcesName[agent::Resource::Service] = tr("uslugę");
	resourcesName[agent::Resource::User] = tr("użytkownika");
	resourcesName[agent::Resource::Warehouse] = tr("magazyn");
	resourcesName[agent::Resource::WoodType] = tr("rodzaj drewna");

	if (event.operation != agent::ResourceOperation::Logged &&
		event.operation != agent::ResourceOperation::Logout)
	{
		mSystemTray->showMessage(tr("eSawmillERP zdarzenia"),
			QString("%1 %2").arg(operationsName[event.operation], resourcesName[event.resourceType]));
	}

	Q_EMIT notifyRepositoryChanged(event);
}

void MainWindow::notifyServerStatusReceived(const agent::ServerStatusEvent& event)
{
	//qDebug() << "Notify server status received";
}

bool MainWindow::loginUserWithExistProfile() {
	QSettings settings;
	if (settings.value("profile/rememberme").toBool() == false)
		return false;

	core::SimpleCrypt crypt(core::SimpleCrypt::DefaultKey);
	const QString username = settings.value("profile/username").toString();
	const QString userpass = crypt.decryptToString(settings.value("profile/userpass").toString());
	if (!core::GlobalData::instance().repository().userRepository().login(username, userpass)) {
		::widgets::MessageBox::warning(this, "Błąd",
			"Nie można zalogować użytkownika na podstawie danych odczytanych z profilu!");
		return false;
	}

#ifndef DEBUG
	if (core::Licence licence; core::Auth::user().company().get("NIP").toString() != licence.customer().get("NIP").toString()) {
		::widgets::MessageBox::critical(this, "Konfilkt pliku licencji",
			"Aktualnie zalogowana firma posiada inny NIP niż NIP na który została wystawiona licencja "
			"na użytkowanie aplikacji eSawmill. Dalsze kożystanie z aplikacji będzie nie możliwe");

		QTimer::singleShot(100, qApp, &QApplication::quit);
	}
#endif
	return true;
}

void MainWindow::authenticateUserHandle()
{
	if (loginUserWithExistProfile())
	{
		logger_.debug("Authenticate user from existing profile");
		userIsLogin();
	}
	else
	{
		logger_.debug("Show user authenticate dialog");

		QPointer dlg(new account::AuthDialog(this));
		connect(dlg.data(), &account::AuthDialog::rejected, &MainWindow::close);
		connect(dlg.data(), &account::AuthDialog::logged, this, &MainWindow::userIsLogin);

		dlg->exec();
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	// ask before exit
	unsigned int msgRet = ::widgets::MessageBox::information(this,
		tr("Pytanie"),
		tr("Czy napewno chcesz zakończyć pracę z aplikacją eSawmillERP"), tr(""),
		QMessageBox::Yes | QMessageBox::No);

	if (msgRet == QMessageBox::Yes) {
		/// and next disconnect all repositories and unregister device from service
		// logout user from remote service
		auto& repository = core::GlobalData::instance().repository();
		repository.userRepository().logout();	// first logout user

		if (!repository.disconnect()) {
			::widgets::MessageBox::critical(this, tr("Błąd repozytorium"),
				tr("Błąd podczas wyrejestrowywania klienta z usługi Production Center wystąpiły błędy.\n"
				   "Podczas kolejnego uruchomienia aplikacji może wystąpić błąd atoryzacji urządzenia.\n"
				   "Uruchom ponownie serwer produkcji w celu uniknięcia komplikacji w przyszłości"));
		}

		// stop event agent thread at end
		auto* eventAgent = core::EventAgentClient::instance();
		if (eventAgent != nullptr) {
			eventAgent->requestInterruption();
			eventAgent->quit();
		}

		event->accept();
		QMainWindow::closeEvent(event);
	}
	else event->ignore();
}

void MainWindow::createActions()
{
	//// ESAWMILL - GŁÓWNE MENU
	mAuthAction = new QAction(tr("Autoryzacja"), this);
	mAuthAction->setIcon(QIcon(":/icons/key"));
	mAuthAction->setToolTip(tr("Autoryzacja w programie eSawmill"));
	mAuthAction->setStatusTip(mAuthAction->toolTip());

	mConfigurationAction = new QAction(tr("U&stawienia"), this);
	mConfigurationAction->setToolTip(tr("Ustawienia programu eSawmill"));
	mConfigurationAction->setStatusTip(mConfigurationAction->toolTip());
	mConfigurationAction->setIcon(QIcon(":/icons/settings"));

	mBackupAction = new QAction(tr("Kopia\nbezpieczeństwa"), this);
	mBackupAction->setToolTip(tr("Wykonaj lub odczytaj dane z kopii bezpieczeństwa"));
	mBackupAction->setStatusTip(mBackupAction->toolTip());
	mBackupAction->setIcon(QIcon(":/icons/backup-restore"));

	mExitAction = new QAction(tr("Zakończ"), this);
	mExitAction->setToolTip(tr("Zakończ pracę z programem eSawmill"));
	mExitAction->setStatusTip(mExitAction->toolTip());
	mExitAction->setIcon(QIcon(":/icons/exit"));

	//// MAGAZYN
	mWarehouseAction = new QAction(tr("&Magazyny"), this);
	mWarehouseAction->setToolTip(tr("Zarządzanie magazynami oraz artykułami"));
	mWarehouseAction->setStatusTip(mWarehouseAction->toolTip());

	mArticlesAction = new QAction(tr("&Artykuły"), this);
	mArticlesAction->setIcon(QIcon(":/icons/trolley"));
	mArticlesAction->setToolTip(tr("Zarządzanie artykułami (produkrami) w wybranym magazynie"));
	mArticlesAction->setStatusTip(mArticlesAction->toolTip());

	mContractorsAction = new QAction(tr("&Kontrahenci"), this);
	mContractorsAction->setToolTip(tr("Zarządzanie kontrahentami (dostawcami/odbiorcami)"));
	mContractorsAction->setStatusTip(mContractorsAction->toolTip());
	mContractorsAction->setIcon(QIcon(":/icons/contractors"));

	mSalesInvoicesAction = new QAction("Faktury sprzedaży", this);
	mSalesInvoicesAction->setToolTip("Wyświetla faktury sprzedaży");
	mSalesInvoicesAction->setStatusTip(mSalesInvoicesAction->toolTip());

	//// ZAMÓWIENIA
	mOrdersAction = new QAction(tr("Zamó&wienia"), this);
	mOrdersAction->setIcon(QIcon(":/icons/orders"));
	mOrdersAction->setToolTip(tr("Zarządzanie zamówieniami elementów konstrukcyjnych"));
	mOrdersAction->setStatusTip(mOrdersAction->toolTip());

	mServiceAction = new QAction(tr("Usługi"), this);
	mServiceAction->setIcon(QIcon(":/icons/services"));
	mServiceAction->setToolTip(tr("Zarządzanie usługami (zamówienia realizowane z kłód dostarczonych do obróbki)"));
	mServiceAction->setStatusTip(mServiceAction->toolTip());

	//// POMOC
	mAboutApp = new QAction(tr("O progamie..."), this);
	mAboutApp->setToolTip(tr("O programie"));
	mAboutApp->setStatusTip(tr("Wyświetla informacje o programie i licencji"));

	mAboutQt = new QAction(tr("O Qt"), this);
	mAboutQt->setIcon(QIcon(":/icons/qt"));
	mAboutQt->setToolTip(tr("O programie"));
	mAboutQt->setStatusTip(tr("Wyświetla inforacje o framerowku użytym do stworzenia aplikacji"));

	mUndoAction = new QAction(tr("Cofnij"), this);
	mUndoAction->setEnabled(false);
	mUndoAction->setToolTip(tr("Wycofuje ostatnio wprowadzone zmiany"));
	mUndoAction->setStatusTip(tr("Wycofuje ostatnio wprowadzone zmiany"));
}

void MainWindow::createWidgets()
{
//	mWebView = new QWebEngineView(this);
//	mWebView->load(QUrl::fromLocalFile(QApplication::applicationDirPath() + "/page/index.html"));
//	setCentralWidget(mWebView);
	setCentralWidget(new QWidget(this));

	mAuthToolBar = new QToolBar("Auth", this);
	mAuthToolBar->setAllowedAreas(Qt::ToolBarArea::TopToolBarArea);
	mAuthToolBar->setAcceptDrops(false);
	mAuthToolBar->setIconSize(QSize(32, 32));
	mAuthToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

	mAuthToolBar->addAction(mAuthAction);
	mAuthToolBar->addSeparator();
	mAuthToolBar->addAction(mExitAction);
	addToolBar(mAuthToolBar);

	mGeneralToolBar = new QToolBar("General", this);
	mGeneralToolBar->setIconSize(QSize(32, 32));
	mGeneralToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

	mGeneralToolBar->addAction(mArticlesAction);
	mGeneralToolBar->addAction(mContractorsAction);
	mGeneralToolBar->addSeparator();
	mGeneralToolBar->addAction(mOrdersAction);
	mGeneralToolBar->addAction(mServiceAction);
	addToolBar(mGeneralToolBar);

	mMainToolBar = new QToolBar("Main", this);
	mMainToolBar->setIconSize(QSize(32, 32));
	mMainToolBar->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextUnderIcon);

	mMainToolBar->addAction(mBackupAction);
	mMainToolBar->addAction(mConfigurationAction);
	mMainToolBar->addSeparator();
	mMainToolBar->addAction(mExitAction);
	addToolBar(mMainToolBar);

	mGeneralToolBar->setVisible(false);
	mMainToolBar->setVisible(false);

	mStatusBar = new StatusBar(this);
//	mStatusBar->setRegisterFor("FastDrew sp z.o.o");
	setStatusBar(mStatusBar);
}

void MainWindow::createMenus()
{
	QMenu* traderMenu = new QMenu("Handel", this);
	traderMenu->addAction(mSalesInvoicesAction);
	traderMenu->addAction("Faktury zakupu", this, &MainWindow::purchaseInvoicesManagerHandle);

	mWarehouseMenu = new QMenu(tr("Ma&gazyn"), this);
	mWarehouseMenu->setEnabled(false);
	mWarehouseMenu->addAction(mWarehouseAction);
	mWarehouseMenu->addAction(mArticlesAction);
	mWarehouseMenu->addMenu(traderMenu);
	mWarehouseMenu->addSeparator();
	mWarehouseMenu->addAction(mContractorsAction);

	mOrdersMenu = new QMenu(tr("Zamówienia"), this);
	mOrdersMenu->setEnabled(false);
	mOrdersMenu->addAction(mOrdersAction);
	mOrdersMenu->addSeparator();
	mOrdersMenu->addAction(mServiceAction);

	mMainMenu = new QMenu(tr("&eSawmill"), this);
	mMainMenu->addAction(mAuthAction);
	mMainMenu->addSeparator();
	mMainMenu->addAction(mConfigurationAction);
	mMainMenu->addAction(mBackupAction);
	mMainMenu->addSeparator();
	mMainMenu->addAction(mExitAction);

	mHelpMenu = new QMenu(tr("Pomoc"), this);
	mHelpMenu->addAction(mAboutQt);
	mHelpMenu->addAction(mAboutApp);

	mViewMenu = new QMenu(tr("Widok"), this);

	mEditMenu = new QMenu(tr("Edycja"), this);
	mEditMenu->addAction(mUndoAction);

	menuBar()->addMenu(mMainMenu);
	menuBar()->addMenu(mEditMenu);
	menuBar()->addMenu(mViewMenu);
	menuBar()->addMenu(mWarehouseMenu);
	menuBar()->addMenu(mOrdersMenu);
	menuBar()->addMenu(mHelpMenu);
}

void MainWindow::userIsLogin()
{
	removeToolBar(mAuthToolBar);
	mGeneralToolBar->setVisible(true);
	mMainToolBar->setVisible(true);
	mAuthAction->setEnabled(false);

	mOrdersMenu->setEnabled(true);
	mWarehouseMenu->setEnabled(true);

	loadRepositoryData();
	runTests();
}

void MainWindow::createConnections()
{
	connect(mExitAction, &QAction::triggered, this, &MainWindow::close);
	connect(mContractorsAction, &QAction::triggered, this, &MainWindow::contractorActionHandle);
	connect(mArticlesAction, &QAction::triggered, this, &MainWindow::articleActionHandle);
	connect(mWarehouseAction, &QAction::triggered, this, &MainWindow::warehouseActionHandle);
	connect(mOrdersAction, &QAction::triggered, this, &MainWindow::orderActionHandle);
	connect(mServiceAction, &QAction::triggered, this, &MainWindow::serviceActionHandle);
	connect(mBackupAction, &QAction::triggered, this, &MainWindow::backupActionHandle);
	connect(mConfigurationAction, &QAction::triggered, this, &MainWindow::configurationActionHandle);
	connect(mAuthAction, &QAction::triggered, this, &MainWindow::authenticateUserHandle);
	connect(mAboutQt, &QAction::triggered, this, &MainWindow::aboutQtHandle);
	connect(mAboutApp, &QAction::triggered, this, &MainWindow::aboutApplicationHandle);
	connect(mSalesInvoicesAction, &QAction::triggered, this, &MainWindow::salesInvoicesManagerHandle);
	connect(mUndoAction, &QAction::triggered, this, &MainWindow::undoActionHandle);

	// setup global shortcuts
	QShortcut* undoShortcut = new QShortcut(QKeySequence("Ctrl+C"), this);
	connect(undoShortcut, &QShortcut::activated, this, &MainWindow::undoActionHandle);
}

void MainWindow::createDockWidgets() {
	if (auto* repo = dynamic_cast<core::repository::database::DatabaseRepository*>(&core::GlobalData::instance().repository());
		!repo)
	{
		// create event agent widget only when user use connection to remote service (not direct to database)
		mEventsWidget = new ::widgets::EventsWidget(this);
		QDockWidget* dockEventWidget = new QDockWidget(this);
		dockEventWidget->setWindowTitle(mEventsWidget->windowTitle());
		dockEventWidget->setWidget(mEventsWidget);
		addDockWidget(Qt::BottomDockWidgetArea, dockEventWidget);
		dockEventWidget->toggleViewAction()->setToolTip(tr("Historia zdarzeń operacji użytkowników"));
		dockEventWidget->toggleViewAction()->setStatusTip(tr("Wyświetla lub ukrywa okno prezentujące historię zdarzeń operacji użytkowników"));
		mViewMenu->addAction(dockEventWidget->toggleViewAction());

		// add listeners to event agent
		auto* eventAgent = core::EventAgentClient::instance();
		connect(eventAgent, &core::EventAgentClient::notifyRepositoryEventReceived,
				mEventsWidget, &::widgets::EventsWidget::notifyEventReceived, Qt::QueuedConnection);

		// hide unneeded widgets depends on current repository
		auto& cfg = core::settings::AppSettings::instance();
		if (cfg.repositoryMode() == core::settings::RepositoryMode::Database) {
			dockEventWidget->toggleViewAction()->setEnabled(false);
			dockEventWidget->toggleViewAction()->setToolTip(tr("Zdarzenia nie są dostępne przy pracy jednostanowiskowej"));
			dockEventWidget->close();
		}
	}
}

void MainWindow::registerVerifications()
{
	addVerification(new eSawmill::orders::verifications::UnrealisedOrdersVerification {});
	addVerification(new eSawmill::invoices::verifications::SaleInvoicesVerification {});
	addVerification(new eSawmill::invoices::verifications::PurchaseInvoicesVerification {});
}

void MainWindow::articleActionHandle() {
	eSawmill::articles::ArticlesManager* manager = new eSawmill::articles::ArticlesManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
			manager, &eSawmill::articles::ArticlesManager::handleRepositoryNotify);
	manager->show();
}

void MainWindow::contractorActionHandle() {
	eSawmill::contractors::ContractorManager* manager = new eSawmill::contractors::ContractorManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
		manager, &eSawmill::contractors::ContractorManager::notifyRepository);
	manager->show();
}

void MainWindow::warehouseActionHandle() {
	eSawmill::articles::WarehouseManager* manager = new eSawmill::articles::WarehouseManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
			manager, &eSawmill::articles::WarehouseManager::handleRepositoryNotify);
	manager->show();
}

void MainWindow::orderActionHandle() {
	eSawmill::orders::OrdersManager* manager = new eSawmill::orders::OrdersManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
		manager, &eSawmill::orders::OrdersManager::handleRepositoryNotification);
	manager->show();
}

void MainWindow::configurationActionHandle() {
	QScopedPointer<config::MainConfiguration> configurationDialog(new config::MainConfiguration(this));
	connect(this, &MainWindow::notifyRepositoryChanged,
			configurationDialog.get(), &config::MainConfiguration::notifyRepositoryChanged);

	if (configurationDialog->exec() == QDialog::Accepted) {
	}
}

void MainWindow::aboutQtHandle() {
	QMessageBox::aboutQt(this, tr("O Qt"));
}

void MainWindow::aboutApplicationHandle() {
	AboutApplication* aboutApplication = new AboutApplication(this);
	aboutApplication->exec();
}

void MainWindow::salesInvoicesManagerHandle()
{
	eSawmill::invoices::sales::SalesInvoicesManager* manager = new eSawmill::invoices::sales::SalesInvoicesManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
		manager, &eSawmill::invoices::sales::SalesInvoicesManager::handleRepositoryNotification);
	manager->show();
}

void MainWindow::purchaseInvoicesManagerHandle()
{
	eSawmill::invoices::purchase::PurchaseInvoiceManager* manager = new eSawmill::invoices::purchase::PurchaseInvoiceManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
		manager, &eSawmill::invoices::purchase::PurchaseInvoiceManager::handleRepositoryNotification);
	manager->show();
}

void MainWindow::undoActionHandle() {
	auto& commandExecutor = core::GlobalData::instance().commandExecutor();
#ifdef _DEBUG
	if (!commandExecutor.canUndo()) {
		::widgets::MessageBox::information(this, tr("Informacja"),
			tr("Brak poleceń możliwych do cofnięcia"));
		return;
	}
#endif
	if (commandExecutor.canUndo()) {
		auto answer = ::widgets::MessageBox::question(this, tr("Pytanie"),
			tr("Czy chcesz wycofać ostatnio wprowadzone zmiany?"), "", QMessageBox::Yes | QMessageBox::No);

		if (answer == QMessageBox::No)
			return;

		if (!commandExecutor.undo()) {
			::widgets::MessageBox::critical(this, tr("Błąd"),
				tr("Podczas wycofywania zmian ostatniej operacji wystąpiły błędy!\n"
				   "Zmiany nie zostały wycofane."));
		}
	}
	// update UI
	mUndoAction->setEnabled(commandExecutor.canUndo());
}

void MainWindow::handleSocketErrors(QAbstractSocket::SocketError err) {
	qDebug() << Q_FUNC_INFO << "error: " << err;

	switch (err) {
	case QAbstractSocket::SocketError::HostNotFoundError:
	case QAbstractSocket::SocketError::ConnectionRefusedError:
		::widgets::MessageBox::critical(this, tr("ControlCenter - Błąd połączenia"),
			tr("Pod wskazanym adresem nie odnaleziono działającej usługi ControlCenter"));
		break;

	case QAbstractSocket::SocketError::AddressInUseError:
		::widgets::MessageBox::critical(this, tr("ControlCenter - Błąd adresacji"),
			tr("Podany adres jest już używany przez inną aplkację"));
		break;

	case QAbstractSocket::SocketError::SocketAccessError:
		::widgets::MessageBox::critical(this, tr("ControlCenter - Błąd"),
			tr("Brak uprawnień na dostęp do otwarcia połączenia internetowego"));
		break;

	case QAbstractSocket::SocketError::SocketAddressNotAvailableError:
		::widgets::MessageBox::critical(this, tr("ControlCenter - Błąd"),
			tr("Podany adres do połączeń z serwerem produkcyjnym lub usługą powiadomień jest niedostępny"));
		break;

	default:
#if _DEBUG
		::widgets::MessageBox::critical(this, tr("ControlCenter socket error"),
				tr("Nieprzechwycony błąd sieci (%1)").arg(static_cast<long>(err)));
#endif //_DEBUG
		break;
	}
}

void MainWindow::loadRepositoryData() {
	QApplication::setOverrideCursor(Qt::WaitCursor);
	std::shared_ptr<QProgressDialog> progressDialog = std::make_shared<QProgressDialog>(this);
	progressDialog->setMinimum(0);
	progressDialog->setMaximum(0);
	progressDialog->setAutoClose(true);
	progressDialog->show();

	auto& repository = core::GlobalData::instance().repository();
	repository.loadRepositories(progressDialog);
	QApplication::restoreOverrideCursor();
}

void MainWindow::serviceActionHandle() {
	eSawmill::services::ServiceManager* manager = new eSawmill::services::ServiceManager(this);
	connect(this, &MainWindow::notifyRepositoryChanged,
			manager, &eSawmill::services::ServiceManager::handleRepositoryNotification);
	manager->show();
}

void MainWindow::backupActionHandle()
{
	QScopedPointer<BackupWizard::BackupWizard> wizard(new BackupWizard::BackupWizard(this));
	wizard->exec();
}

void MainWindow::parseLicenceFile() {
#ifndef DEBUG
	try {
        core::Licence licence;

		mOrdersAction->setEnabled(licence.isOrderModuleActive());
		mServiceAction->setEnabled(licence.isServiceModuleActive());
		mArticlesAction->setEnabled(licence.isArticleModuleActive());
		mWarehouseAction->setEnabled(licence.isArticleModuleActive());
		mContractorsAction->setEnabled(licence.isOrderModuleActive() ||
									   licence.isServiceModuleActive() ||
									   licence.isArticleModuleActive());

		mStatusBar->setRegisterFor(licence.customer().name());
	}
	catch (const QString msg) {
		::widgets::MessageBox::warning(this, tr("Licencja"), msg);
	}
#else
	mStatusBar->setRegisterFor(tr("Run in debug mode"));
	mOrdersAction->setEnabled(true);
	mServiceAction->setEnabled(true);
	mArticlesAction->setEnabled(true);
	mWarehouseAction->setEnabled(true);
	mContractorsAction->setEnabled(true);
#endif
}

void MainWindow::createEventAgent()
{
	// event agnet is needed only when user use remote connection
	if (auto* repo = dynamic_cast<core::repository::database::DatabaseRepository*>(&core::GlobalData::instance().repository()); repo)
		return;

	try {
		// now create thread to recvied events from production service
		QSettings settings;
		const QString& eventAgentAddress = settings.value("remote/eventAgent", QVariant("127.0.0.1")).toString();
		const int eventAgentPort = settings.value("remote/eventAgentPort", 5050).toInt();

		logger_.debug(QString("Create event agent thread (ip: %1, port: %2)")
			.arg(eventAgentAddress).arg(eventAgentPort));

		auto* eventAgent = core::EventAgentClient::createSingelton(
			QHostAddress(eventAgentAddress),
			eventAgentPort,
			this
		);
		connect(eventAgent, &core::EventAgentClient::notifyRepositoryEventReceived, this, &MainWindow::notifyEventReceived, Qt::QueuedConnection);
		connect(eventAgent, &core::EventAgentClient::notifyServerStatusEventReceived, this, &MainWindow::notifyServerStatusReceived, Qt::QueuedConnection);
		connect(eventAgent, &core::EventAgentClient::notifyError, this, &MainWindow::handleSocketErrors, Qt::QueuedConnection);
		connect(eventAgent, &QThread::finished, eventAgent, &QThread::deleteLater);

		eventAgent->makeConnection();
		eventAgent->start();
	} catch (...) {
		throw;
	}
}

void MainWindow::destroyEventAgent()
{
	auto* eventAgent = core::EventAgentClient::instance();
	if (!eventAgent)
		return;

	eventAgent->requestInterruption();
	eventAgent->msleep(10);
	eventAgent->quit();

	core::EventAgentClient::destroySingelton();

	logger_.debug("Terminate event agent thread");
}

} // namespace

