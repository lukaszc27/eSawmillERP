#include "mainwindow.h"
#include "wizards/connectionWizard/createconnectionwizard.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSettings>

#include <memory>
#include <exception>
#include <messagebox.h>
#include <dbo/contact.h>
#include <dbo/company.h>
#include <dbo/user.h>
#include <simplecrypt.hpp>
#include <settings/appsettings.hpp>
#include <exceptions/repositoryexception.hpp>
#include <globaldata.hpp>
#include <repository/database/databaserepository.hpp>
#include <repository/remote/remotecityrepository.hpp>
#include <exceptions/deviceunregisteredexception.hpp>

using namespace eSawmill;

///
/// @brief startupDialog
/// show first use dialog where user can select communication
/// mode and create first account in system
/// @return
///
bool startupDialog(QObject* parent = nullptr);

///
/// @brief main - entry point
/// @param argc
/// @param argv
/// @return
///
int main(int argc, char *argv[])
{
	auto& logger = core::GlobalData::instance().logger();
	logger.debug("eSawmill app starting");

	qRegisterMetaType<core::providers::agent::Event>("Event");
	qRegisterMetaType<core::providers::agent::RepositoryEvent>("RepositoryEvent");
	qRegisterMetaType<core::providers::agent::ServerStatusEvent>("ServerStatusEvent");

	QApplication app(argc, argv);
    Q_INIT_RESOURCE(resources);

	app.setApplicationName("eSawmillERP");
	app.setApplicationDisplayName("eSawmill ERP (Alfa)");
    app.setOrganizationName("IDsoft");
	app.setOrganizationDomain("idsoft.com");

	auto& cfg = core::settings::AppSettings::instance();
	app.setApplicationVersion(cfg.applicationVersion().toString());

	logger.debug("Initialize command parser");
	QCommandLineOption noProfileOption{"no-profile", "Discard existing profile file"};
	QCommandLineOption noDirectDbConnection {"no-direct-db-conn", "Disable direct connection to database in remote mode"};

	QCommandLineParser parser{};
	parser.addOption(noProfileOption);
	parser.addOption(noDirectDbConnection);
	parser.addHelpOption();
	parser.addVersionOption();
	parser.process(app);

	int returnCode {0};
	try {
		if (!QFile::exists("profile") || parser.isSet(noProfileOption)) {
			if (!startupDialog()) {
				::widgets::MessageBox::critical(nullptr,
					QObject::tr("Błąd tworzenia nowej firmy"),
					QObject::tr("Podczas zapisu danych firmowych nastąpiły nieoczekiwane błędy")
				);
				app.quit();
				return -1;
			}
		}

		logger.debug(QString("Connecting to data repository (%1 repository)")
			.arg(cfg.repositoryMode() == core::settings::RepositoryMode::Database
				 ? "database"
				 : "remote"));

		core::GlobalData::instance().repository().connect();

		if (cfg.repositoryMode() != core::settings::RepositoryMode::Database
			&& !parser.isSet(noDirectDbConnection))
		{
			core::repository::database::DatabaseRepository databaseRepository;
			databaseRepository.connect();
		}

		logger.debug("Create main window");
		MainWindow mainWindow {};
		app.setActiveWindow(&mainWindow);
		mainWindow.show();

		logger.debug("Start main event loop");
		returnCode = app.exec();
	} catch (core::exceptions::RepositoryException&) {
		logger.debug("Cannot connect to data provider");

		::widgets::MessageBox::critical(nullptr, QObject::tr("Błąd"),
			QObject::tr("Nie nawiązano połączenia z dostawcą danych, praca w systemie nie jest możliwa"));

		app.closeAllWindows();
		app.quit();
		return -1;
	} catch (core::exceptions::DeviceUnregisteredException&) {
		logger.debug("Device is not registered on remote service");

		::widgets::MessageBox::critical(nullptr, QObject::tr("Błąd rejestrowania urządzenia"),
			QObject::tr("Nie zarejestrowano urządzenia w usłudze produkcyjnej.\n"
						"Prawdopodobnie uruchamiasz więcej niż jedną kopię aplikacji na jednym stanowisku komputerowym"
						"lub podczas ostatniego urzycia aplikacji nie udało się jej wyrejestrować"));

		app.closeAllWindows();
		app.quit();
		return -1;
	}

	logger.debug("Stop the app");
	return returnCode;
}

bool startupDialog(QObject* parent) {
	std::unique_ptr<eSawmill::ConnectionWizard::CreateConnectionWizard> wizard = std::make_unique<eSawmill::ConnectionWizard::CreateConnectionWizard>();
	if (wizard->exec() == QWizard::Accepted) {
		// store configuration to data provider

		QSettings settings;
		if (bool useDatabase = wizard->field("DatabaseConnection").toBool(); useDatabase) {
			settings.beginGroup("database");
				settings.setValue("driver", wizard->field("DriverName").toString());
				settings.setValue("host", wizard->field("HostName").toString());
				settings.setValue("name", wizard->field("DatabaseName").toString());
				settings.setValue("userName", wizard->field("UserName").toString());
				settings.setValue("userPassword", wizard->field("UserPassword").toString());
				settings.setValue("trustedConnection", wizard->field("TrustedConnection").toBool());
			settings.endGroup();
		} else {
			// user select remote connection option
			settings.beginGroup("remote");
			settings.setValue("eventAgent", wizard->field("EventAgentAddress").toString());
			settings.setValue("eventAgentPort", wizard->field("EventAgentPort").toInt());
			settings.setValue("serviceHost", wizard->field("ServiceAddress").toString());
			settings.setValue("serviceHostPort", wizard->field("ServicePort").toInt());
			settings.endGroup();
		}

		if (wizard->field("CreateAccount").toBool()) {
			// user want to create account
			core::Contact contact {};
			contact.setCountry(wizard->field("Country").toString());
			contact.setIsoCountry(wizard->field("CountryIso").toString());
			contact.setCity(wizard->field("City").toString());
			contact.setState(wizard->field("State").toString());
			contact.setStreet(wizard->field("Street").toString());
			contact.setPostCode(wizard->field("PostCode").toString());
			contact.setPostOffice(wizard->field("PostOffice").toString());
			contact.setHomeNumber(wizard->field("HomeNumber").toString());
			contact.setLocalNumber(wizard->field("LocalNumber").toString());
			contact.setPhone(wizard->field("Phone").toString());
			contact.setEmail(wizard->field("Email").toString());
			contact.setUrl(wizard->field("Url").toString());

			core::Company company {};
			company.setName(wizard->field("CompanyName").toString());
			company.setNIP(wizard->field("NIP").toString());
			company.setREGON(wizard->field("REGON").toString());
			company.setKRS(wizard->field("KRS").toString());
			company.setContact(contact);

			core::User user {};
			user.setName(wizard->field("ERP_UserName").toString());
			user.setPassword(wizard->field("ERP_Password").toString());
			user.setContact(contact);
			user.setCompany(company);
			user.setFirstName(wizard->field("Firstname").toString());
			user.setSurName(wizard->field("Surname").toString());

			try {
				auto& repository = core::GlobalData::instance().repository();
				if (repository.connect(parent)) {
					if (repository.companiesRepository().create(company)) {
						Q_ASSERT(company.id() > 0);
						user.setCompany(company);

						// assign roles for first user
						user.assignRole(repository.userRepository().getRole(core::Role::SystemRole::Administrator));
						user.assignRole(repository.userRepository().getRole(core::Role::SystemRole::Operator));
						user.assignRole(repository.userRepository().getRole(core::Role::SystemRole::Worker));
						if (!repository.userRepository().create(user))
							return false;
					}
				}
				repository.disconnect();
			} catch (...) {
				throw;
			}
		}

		// when all data stored in db
		// create profile file
		QFile file("profile");
		if (file.open(QFile::WriteOnly | QFile::Text))
			file.close();


		return true;
	}

	return false;
}
