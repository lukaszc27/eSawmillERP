#include "statusbar.hpp"

#include <QMessageBox>
#include <QApplication>


namespace eSawmill {

StatusBar::StatusBar(QWidget* parent)
	: QStatusBar(parent)
{
	mStatusTip = new QLabel(this);
	mRegisterFor = new QLabel(this);

	mUpdateButton = new QPushButton(QIcon(":/icons/update"), "", this);
	mUpdateButton->setToolTip(tr("Sprawdź aktualizacje"));
	mUpdateButton->setStatusTip(tr("Sprawdza czy pojawiła się nowa wersja programu eSawmill"));

	QLabel* versionLabel = new QLabel(this);
	versionLabel->setText(tr("Wersja: %1").arg(QApplication::applicationVersion()));

	addPermanentWidget(mStatusTip, 6);
	addPermanentWidget(mRegisterFor);
	addPermanentWidget(new QWidget(this), 1);
	addPermanentWidget(versionLabel);
	addPermanentWidget(new QWidget(this), 1);
	addPermanentWidget(mUpdateButton);

	connect(this, &QStatusBar::messageChanged, this, [&](const QString& msg){
		mStatusTip->setText(msg);
	});
}

} // namespace eSawmill
