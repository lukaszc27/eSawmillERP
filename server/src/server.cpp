#include "server.hpp"
#include "logger.hpp"
#include <iostream>
#include <memory>
#include <settings/appsettings.hpp>
#include <QSqlDatabase>
#include <QSqlError>
#include <QMutexLocker>
#include <repository/database/databaserepository.hpp>
#include <QSettings>

Server::Server(const std::uint16_t port, EventAgent* eventAgent, Logger& logger, QObject* parent)
	: port_{port}
	, m_eventAgent {eventAgent}
	, m_logger {logger}
	, QTcpServer {parent}
	, m_stateTimer {this}
{
		listen(QHostAddress::AnyIPv4, port_);
		connect(this, &Server::newConnection, this, &Server::acceptPendingConnection, Qt::QueuedConnection);

		m_stateTimer.setInterval(500);
		connect(&m_stateTimer, &QTimer::timeout, this, &Server::sendServerState);
		m_stateTimer.start();
}

Server::~Server() {
	if (m_eventAgent != nullptr) {
		m_eventAgent->requestInterruption();
		m_eventAgent->quit();
		m_eventAgent->wait();
	}
}

core::repository::AbstractRepository& Server::repository() {
	/// make and return singelton object
	/// in future in this method we have to add mutex for safe allowed to database by only one process
	static core::repository::database::DatabaseRepository repository;
	return repository;
}

bool Server::lock(const core::eloquent::DBIDKey& resId, const LockHeader& header, std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock) {
	bool exist = std::any_of(lock->begin(), lock->end(),
							 [&resId, &header](const LockPair& item)->bool {
		return item.first == resId && item.second.userId != header.userId;
	});
	if (exist)
		return false;

	lock->insert(std::make_pair(resId, header));
	return true;
}

bool Server::unlock(const core::eloquent::DBIDKey& resId, std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock) {
	auto itr = std::find_if(lock->begin(), lock->end(),
							[&resId](const LockPair& item)->bool {
		return item.first == resId;
	});
	if (itr != lock->end()) {
		lock->erase(itr);
		return true;
	}
	return false;
}

Server::LockHeader Server::getLockHeader(const core::eloquent::DBIDKey& resId, std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock) {
	auto itr = std::find_if(lock->begin(), lock->end(), [&resId](const LockPair& item)->bool {
		return item.first == resId;
	});
	if (itr != lock->end())
		return itr->second;
	return LockHeader{};
}

void Server::removeClient(Client* client) {
	if (client == nullptr)
		return;

	const auto itr = std::find_if(m_clients.begin(), m_clients.end(),
		[&](std::unique_ptr<Client>& c) {
			return c.get() == client;
		});

	if (itr != m_clients.end())
		m_clients.erase(itr);
}

bool Server::registerDevice(const QString& deviceId, const QString& hostName) {
	if (deviceId.isEmpty())
		return false;

	const auto itr = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
								  [uniqueDeviceId = deviceId.toLocal8Bit()](const ClientInformation& info)->bool {
		return info.uniqueDeviceId == uniqueDeviceId;
	});

	if (itr == m_clientsInformation.end()) {
		ClientInformation info;
		info.uniqueDeviceId = deviceId.toLocal8Bit();
		info.hostName = hostName;
		m_clientsInformation.push_back(info);

		return true;
	}
	// error
	// the device has neen registered before
	return false;
}

void Server::unregisterDevice(const QString& deviceId) {
	const auto found = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
									[uniqueDeviceId = deviceId.toLocal8Bit()](const ClientInformation& info)->bool {
		return info.uniqueDeviceId == uniqueDeviceId;
	});

	if (found != m_clientsInformation.end()) {
		// on unregistered time we can remove object
		// the object is not required in future time
		// (user probably closed the app and disconnected from service)
		m_clientsInformation.erase(found);
	} else {
		// we want to unregister device who is not registered yet
		/// TODO: add code for this case
	}
}

bool Server::isDeviceRegistered(const QString& deviceId) const {
	return std::any_of(m_clientsInformation.begin(), m_clientsInformation.end(),
					   [uniqueDeviceId = deviceId.toLocal8Bit()](const ClientInformation& info)->bool {
		return info.uniqueDeviceId == uniqueDeviceId;
	});
}

void Server::releaseAllLocker(const core::eloquent::DBIDKey& userId) {
	{ // release blocking other users
		auto itr = m_lockingUsers.end();
		do {
			itr = std::find_if(m_lockingUsers.begin(), m_lockingUsers.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingUsers.end()) {
				m_lockingUsers.erase(itr);
			}
		} while (itr != m_lockingUsers.end());
	}

	{ // release blocking contractors
		auto itr = m_lockingContractors.end();
		do {
			itr = std::find_if(m_lockingContractors.begin(), m_lockingContractors.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool{
				return item.second.userId == userId;
			});
			if (itr != m_lockingContractors.end()) {
				m_lockingContractors.erase(itr);
			}
		} while (itr != m_lockingContractors.end());
	}

	{ // release blocking articles
		auto itr = m_lockingArticles.end();
		do {
			itr = std::find_if(m_lockingArticles.begin(), m_lockingArticles.end(),
							   [userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingArticles.end()) {
				m_lockingArticles.erase(itr);
			}
		} while (itr != m_lockingArticles.end());
	}

	{ // release blocking warehouses
		auto itr = m_lockingWarehouses.end();
		do {
			itr = std::find_if(m_lockingWarehouses.begin(), m_lockingWarehouses.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingWarehouses.end()) {
				m_lockingWarehouses.erase(itr);
			}
		} while (itr != m_lockingWarehouses.end());
	}

	{ // release locking purchase invoices
		auto itr = m_lockingPurchaseInvoices.end();
		do {
			itr = std::find_if(m_lockingPurchaseInvoices.begin(), m_lockingPurchaseInvoices.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingPurchaseInvoices.end()) {
				m_lockingPurchaseInvoices.erase(itr);
			}
		} while (itr != m_lockingPurchaseInvoices.end());
	}

	{ // release locking sale invoices
		auto itr = m_lockingSaleInvoices.end();
		do {
			itr = std::find_if(m_lockingSaleInvoices.begin(), m_lockingSaleInvoices.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingSaleInvoices.end()) {
				m_lockingSaleInvoices.erase(itr);
			}
		} while (itr != m_lockingSaleInvoices.end());
	}

	{ // release locking orders
		auto itr = m_lockingOrders.end();
		do {
			itr = std::find_if(m_lockingOrders.begin(), m_lockingOrders.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingOrders.end()) {
				m_lockingOrders.erase(itr);
			}
		} while (itr != m_lockingOrders.end());
	}

	{ // release locking services
		auto itr = m_lockingServices.end();
		do {
			itr = std::find_if(m_lockingServices.begin(), m_lockingServices.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingServices.end()) {
				m_lockingServices.erase(itr);
			}
		} while (itr != m_lockingServices.end());
	}

	{ // release locking companies
		auto itr = m_lockingCompanies.end();
		do {
			itr = std::find_if(m_lockingCompanies.begin(), m_lockingCompanies.end(),
							   [&userId](const std::pair<core::eloquent::DBIDKey, LockHeader>& item)->bool {
				return item.second.userId == userId;
			});
			if (itr != m_lockingCompanies.end()) {
				m_lockingCompanies.erase(itr);
			}
		} while (itr != m_lockingCompanies.end());
	}
}

bool Server::isUserLogged(const core::eloquent::DBIDKey& userId) const {
	return std::any_of(m_clientsInformation.begin(), m_clientsInformation.end(),
					   [&userId](const ClientInformation& info)->bool {
		return info.user.id() == userId;
	});
}

bool Server::attachUserToDevice(const QByteArray& deviceId, const core::User& user, Client* client) {
	auto found = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
							  [&deviceId](const ClientInformation& info)->bool {
		return info.uniqueDeviceId == deviceId;
	});

	if (found != m_clientsInformation.end()) {
		found->user = user;
		found->client = client;
		return true;
	}
	return false;
}

bool Server::detachUserFromDevice(const core::User& user) {
	auto found  = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
							   [userId = user.id()](const ClientInformation& info)->bool {
		return info.user.id() == userId;
	});

	if (found != m_clientsInformation.end()) {
		found->user = core::User{};
		found->client = nullptr;

		{ // only for test
			auto itr = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
									[userId = user.id()](const ClientInformation& info) {
				return info.user.id() == userId;
			});
			Q_ASSERT (itr == m_clientsInformation.end());
		}
		return true;
	}
	return false;
}

ClientInformation* Server::getClientInformation(const core::eloquent::DBIDKey& userId) {
	auto found = std::find_if(m_clientsInformation.begin(), m_clientsInformation.end(),
							  [&userId](const ClientInformation& info)->bool {
		return info.user.id() == userId;
	});
	if (found != m_clientsInformation.end())
		return &(*found);

	// return empty object
	return nullptr;
}

void Server::acceptPendingConnection() {
	QTcpSocket* socket = nextPendingConnection();
	if (socket == nullptr)
		return;

	//Client* client = new Client {this, socket, m_logger};
	auto client = std::make_unique<Client>(*this, socket, m_logger);
	connect(socket, &QTcpSocket::readyRead, client.get(), &Client::read, Qt::QueuedConnection);
	connect(socket, &QTcpSocket::errorOccurred, client.get(), &Client::readError);
	connect(socket, &QTcpSocket::disconnected, client.get(), &Client::disconnected, Qt::QueuedConnection);

	m_clients.push_back(std::move(client));
}

void Server::sendServerState()
{
	core::providers::agent::ServerStatusEvent event{};
	event.status = core::providers::agent::ServerStatus::NoError;

	eventAgent()->pullEvent(event);
}

ClientInformation::operator bool() noexcept {
	return !uniqueDeviceId.isEmpty()
			&& !hostName.isEmpty()
			&& user.id() > 0
			&& client != nullptr;
}

bool ClientInformation::operator<(const ClientInformation& other) noexcept {
	return uniqueDeviceId < other.uniqueDeviceId
			&& hostName.length() < other.hostName.length()
			&& user.id() < other.user.id();
}
