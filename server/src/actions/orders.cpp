#include "client.hpp"
#include "server.hpp"
#include <dbo/order.h>
#include <providers/protocol.hpp>
#include <QDataStream>
//#include "logger.hpp"

namespace agent = core::providers::agent;

//void Client::getOrders(QDataStream& stream) {
//	logInformation(QObject::tr("Żądanie doczytania informacji o liście zamówień..."));

//	const auto orders = server()
//						.repository()
//						.orderRepository()
//						.getAllOrders();

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << orders;

//	logInformation(QObject::tr("Wysłano listę %1 zamówień").arg(orders.size()));
//}
void Client::getAllOrders(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get all orders request"));

	const auto orders = server().repository().orderRepository().getAllOrders();

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);
	writer << orders;

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetAllOrdersResponse;

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(orders.size()));
}

void Client::getOrder(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get order request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey orderId{};
	reader >> orderId;

	Q_ASSERT(orderId > 0);
	if (orderId > 0)
	{
		const auto order = server().repository().orderRepository().getOrder(orderId);

		writer << proto::OperationStatus::Successfull
			   << order;

		logInformation(QObject::tr("Sent order data (name: %1)").arg(order.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get order - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetOrderResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::createOrder(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Create order request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Order order{};
	reader >> order;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(order.id() == 0);
	if (order.id() == 0)
	{
		const bool success = server().repository().orderRepository().create(order);
		if (success)
		{
			Q_ASSERT(order.id() > 0);
			writer << proto::OperationStatus::Successfull
				   << order;

			sendEvent(agent::ResourceOperation::Created, agent::Resource::Order, order.id());
			logInformation(QObject::tr("The order has been successfully created (name: %1)").arg(order.name()));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot create order in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create order in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::CreateOrderResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::updateOrder(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Update existing order request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Order order{};
	reader >> order;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(order.id() > 0);
	if (order.id() > 0)
	{
		const bool success = server().repository().orderRepository().update(order);
		if (success)
		{
			Q_ASSERT(order.id() > 0);
			writer << proto::OperationStatus::Successfull
				   << order;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Order, order.id());
			logInformation(QObject::tr("Order has been updated successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot update existing order in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update existing order in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::UpdateOrderResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::destroyOrder(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Remove existing order request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey orderId{};
	reader >> orderId;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(orderId > 0);
	if (orderId > 0)
	{
		const bool success = server().repository().orderRepository().destroy(orderId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;

			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Order, orderId);
			logInformation(QObject::tr("Order as been removed successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove order from database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannnot remove order - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::DestroyOrderResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}
//void Client::getOrder(QDataStream& stream) {
//	core::eloquent::DBIDKey orderId {0};
//	stream >> orderId;

//	Q_ASSERT(orderId > 0);
//	if (orderId <= 0) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		return;
//	}

//	logInformation(QObject::tr("Pobranie informacji o zamówieniu o id: %1").arg(orderId));

//	const auto order = server()
//					   .repository()
//					   .orderRepository()
//					   .getOrder(orderId);

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << order;

//	logInformation(QObject::tr("Wysłano informację o zamówieniu %1").arg(order.name()));
//}

//void Client::updateOrder(QDataStream& stream) {
//	core::Order order;
//	stream >> order;

//	logInformation(QObject::tr("Aktualizacja informacji o zamówieniu: %1").arg(order.name()));
//#ifdef _DEBUG
//	if (order.elements().empty())
//		logInformation(QObject::tr("Pusta lista elementów!"));
//	if (order.articles().empty())
//		logInformation(QObject::tr("Pusta lista artykułów"));
//#endif

//	if (server().repository().orderRepository().update(order)) {
//		Q_ASSERT(order.id() > 0);

//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << order;

//		sendEvent(agent::ResourceOperation::Updated, agent::Resource::Order, order.id());
//		logInformation(QObject::tr("Zmodyfikowano inforamcje o zamówieniu: %1").arg(order.name()));
//	} else {
//		stream << core::providers::Protocol::MsgFail;
//		logWarning(QObject::tr("Podczas modyfikacji informacji o zamówieniu wystąpiły błędy!"));
//	}
//}

//void Client::createOrder(QDataStream& stream) {
//	core::Order order;
//	stream >> order;

//	logInformation(QObject::tr("Tworzenie nowego zamówienia: %1").arg(order.name()));
//#ifdef _DEBUG
//	if (order.elements().empty())
//		logWarning(QObject::tr("Pusta lista elementów!"));
//	if (order.articles().empty())
//		logInformation(QObject::tr("Pusta lista artykułów"));
//#endif

//	if (server().repository().orderRepository().create(order)) {
//		Q_ASSERT(order.id() > 0);

//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << order;

//		sendEvent(agent::ResourceOperation::Created, agent::Resource::Order, order.id());
//		logInformation(QObject::tr("Utworzono zamówienie o id %1").arg(order.id()));
//	} else {
//		stream << core::providers::Protocol::MsgFail;
//		logWarning(QObject::tr("Błąd podczas zapisu zamówienia w bazie danych!"));
//	}
//}

//void Client::destroyOrder(QDataStream& stream) {
//	core::eloquent::DBIDKey orderId {0};
//	stream >> orderId;

//	logInformation(QObject::tr("Usuwanie zamówiena (id: %1)").arg(orderId));

//	Q_ASSERT(orderId > 0);

//	if (server().repository().orderRepository().destroy(orderId)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		sendEvent(agent::ResourceOperation::Removed, agent::Resource::Order, orderId);
//		logInformation(QObject::tr("Usunięto zamówienie (id: %1").arg(orderId));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Błąd podczas usuwania zamówienia (id: %1) z bazy danych!").arg(orderId));
//	}
//}

