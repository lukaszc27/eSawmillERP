#include "client.hpp"
#include "server.hpp"
#include <dbo/article.h>
#include <dbo/articleType.h>
#include <providers/protocol.hpp>
#include <QDataStream>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::getArticleTypes(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get article types request"));

	const auto types = server().repository().articleRepository().getArticleTypes();

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);
	writer << types;

	proto::MessageHeader header{};
	header.type = proto::msg::GetArticleTypesResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(types.size()));
}

void Client::getArticleType(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get article type request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey typeId{};
	reader >> typeId;

	Q_ASSERT(typeId > 0);
	if (typeId > 0)
	{
		const auto type = server().repository().articleRepository().getArticleType(typeId);
		writer << proto::OperationStatus::Successfull
			   << type;

		logInformation(QObject::tr("Sent list of %1 items").arg(typeId));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get type from database - invalid id"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetArticleTypeResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::getArticle(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get article request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey articleId{};
	reader >> articleId;

	Q_ASSERT(articleId > 0);
	if (articleId > 0)
	{
		const auto article = server().repository().articleRepository().getArticle(articleId);
		writer << proto::OperationStatus::Successfull
			   << article;

		logInformation(QObject::tr("Sent article (name: %1)").arg(article.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get article from database"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.type = proto::msg::GetArticleResponse;
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::createArticle(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Create new article request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Article article{};
	reader >> article;

	Q_ASSERT(article.id() == 0);
	if (article.id() == 0)
	{
		const bool success = server().repository().articleRepository().createArticle(article);
		if (success)
		{
			Q_ASSERT(article.id() > 0);
			writer << proto::OperationStatus::Successfull
				   << article;

			sendEvent(agent::ResourceOperation::Created, agent::Resource::Article, article.id());
			logInformation(QObject::tr("Article has been created successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot create article in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create article in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::CreateArticleResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::updateArticle(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Update existing article request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Article article{};
	reader >> article;

	Q_ASSERT(article.id() > 0);
	if (article.id() > 0)
	{
		const bool success = server().repository().articleRepository().updateArticle(article);
		if (success)
		{
			writer << proto::OperationStatus::Successfull
				   << article;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Article, article.id());
			logInformation(QObject::tr("Article information has been changed"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot change article information in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update article in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::UpdateArticleResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::destroyArticle(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Remove existing article request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey articleId{};
	reader >> articleId;

	Q_ASSERT(articleId > 0);
	if (articleId > 0)
	{
		const bool success = server().repository().articleRepository().destroyArticle(articleId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;

			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Article, articleId);
			logInformation(QObject::tr("The article has been removed successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove article from database (id: %1)").arg(articleId));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot remove article - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::DestroyArticleResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}
