#include "client.hpp"
#include "server.hpp"
//#include "logger.hpp"
#include <providers/protocol.hpp>
#include <QDataStream>

namespace agent = core::providers::agent;

//void Client::getContractor(QDataStream& stream) {
//	core::eloquent::DBIDKey contractorId {0};
//	stream >> contractorId;

//	Q_ASSERT(contractorId > 0);
//	if (contractorId <= 0) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		return;
//	}

//	logInformation(QObject::tr("Żądanie pobrania informacji o kontrahencie o id: %1").arg(contractorId));


//	const auto contractor = server()
//							.repository()
//							.contractorRepository()
//							.get(contractorId);

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << contractor;

//	logInformation(QObject::tr("Wysłano informacje o kontrahencie: %1")
//				   .arg(contractor.getDisplayName()));
//}

//void Client::getContractors(QDataStream& stream) {
//	logInformation(QObject::tr("Żądanie pobrania listy kontrahentów..."));

//	const auto contractors = server()
//							 .repository()
//							 .contractorRepository()
//							 .getAll();

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess
//)		   << contractors;

//	logInformation(QObject::tr("Wysłano listę %1 kontrahentów").arg(contractors.size()));
//}

void Client::getContractor(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get contractor request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey contractorId{};
	reader >> contractorId;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(contractorId > 0);
	if (contractorId <= 0)
	{
		logWarning(QObject::tr("Invalid contractor identificator"));
		writer << proto::OperationStatus::Fail;
	}
	else
	{
		const auto contractor = server().repository().contractorRepository().get(contractorId);
		writer << proto::OperationStatus::Successfull
			   << contractor;
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetContractorResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent contractor"));
}

void Client::getAllContractors(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get contractors request"));

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto contractors = server().repository().contractorRepository().getAll();
	writer << contractors;

	proto::MessageHeader header{};
	header.type = proto::msg::GetAllContractorsResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent list of %1 contractors").arg(contractors.size()));
}

void Client::createContractor(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Create new contractor request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Contractor contractor{};
	reader >> contractor;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (server().repository().contractorRepository().create(contractor))
	{
		Q_ASSERT(contractor.id() > 0);
		writer << proto::OperationStatus::Successfull
			   << contractor;

		sendEvent(agent::ResourceOperation::Created, agent::Resource::Contractor, contractor.id());
		logInformation(QObject::tr("The contractor %1 has been created successfully").arg(contractor.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create contractor object in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateContractorResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::updateContractor(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Update contractor request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Contractor contractor{};
	reader >> contractor;

	if (server().repository().contractorRepository().update(contractor))
	{
		Q_ASSERT(contractor.id() > 0);
		writer << proto::OperationStatus::Successfull
			   << contractor;

		sendEvent(agent::ResourceOperation::Updated, agent::Resource::Contractor, contractor.id());
		logInformation(QObject::tr("Information about contractor has been changed successfully"));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot change data about contractor in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::UpdateContractorResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::destroyContractor(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Destroy contractor request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey contractorId{};
	reader >> contractorId;

	Q_ASSERT(contractorId > 0);
	if (contractorId > 0)
	{
		if (server().repository().contractorRepository().destroy(contractorId))
		{
			writer << proto::OperationStatus::Successfull;

			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Contractor, contractorId);
			logInformation(QObject::tr("Contractor has been removed"));
		}
		else
		{
			logWarning(QObject::tr("Cannot remove contractor from database"));
			writer << proto::OperationStatus::Fail;
		}
	}
	else
	{
		logWarning(QObject::tr("Contractor db id is invalid"));
		writer << proto::OperationStatus::Fail;
	}

	proto::MessageHeader header{};
	header.type = proto::msg::DestroyContractorResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

//void Client::createContractor(QDataStream& stream) {
//	core::Contractor contractor;
//	stream >> contractor;

//	logInformation(QObject::tr("Żądanie utworzenia nowego kontrahenta: %1")
//				   .arg(contractor.getDisplayName()));

//	const auto success = server()
//						 .repository()
//						 .contractorRepository()
//						 .create(contractor);

//	if (success) {
//		Q_ASSERT(contractor.id() > 0);
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << contractor;

//		sendEvent(agent::ResourceOperation::Created, agent::Resource::Contractor, contractor.id());
//		logInformation(QObject::tr("Utworzono kontrahenta %1 w bazie danych")
//					   .arg(contractor.getDisplayName()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Podczas tworzenia kontrahenta %1 w bazie wystąpiły błędy!")
//				   .arg(contractor.getDisplayName()));
//	}
//}

//void Client::updateContractor(QDataStream& stream) {
//	core::Contractor contractor;
//	stream >> contractor;

//	logInformation(QObject::tr("Żądanie modyfikacji kontrahenta: %1").arg(contractor.getDisplayName()));

//	const auto success = server()
//						 .repository()
//						 .contractorRepository()
//						 .update(contractor);

//	if (success) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << contractor;

//		sendEvent(agent::ResourceOperation::Updated, agent::Resource::Contractor, contractor.id());
//		logInformation(QObject::tr("Zmodyfikowano kontrahenta %1 w bazie danych").arg(contractor.getDisplayName()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Podczas modyfikacji informacji o kontrahencie %1 wystąpiły błędy")
//				   .arg(contractor.getDisplayName()));
//	}
//}

//void Client::destroyContractor(QDataStream& stream) {
//	core::eloquent::DBIDKey contractorId {0};
//	stream >> contractorId;

//	logInformation(QObject::tr("Żądanie usunięcia kontrahenta o id: %1").arg(contractorId));

//	Q_ASSERT(contractorId > 0);
//	if (contractorId <= 0) {
//		stream << core::providers::Protocol::MsgFail;
//		return;
//	}

//	const auto success = server()
//						 .repository()
//						 .contractorRepository()
//						 .destroy(contractorId);

//	if (success) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		sendEvent(agent::ResourceOperation::Removed, agent::Resource::Contractor, contractorId);
//		logInformation(QObject::tr("Usunięto kontrahenta o id: %1").arg(contractorId));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Podczas usuwania kontrahenta o id %1 wystąpiły błędy!").arg(contractorId));
//	}
//}

