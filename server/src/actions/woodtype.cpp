#include "logger.hpp"
#include "client.hpp"
#include "server.hpp"
#include <providers/protocol.hpp>
#include <QDataStream>


namespace agent = core::providers::agent;

void Client::getAllWoodTypes(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get all woods request"));

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto woods = server().repository().woodTypeRepository().getAll();
	writer << woods;

	proto::MessageHeader header{};
	header.type = proto::msg::GetAllUsersResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent %1 of woods").arg(woods.size()));
}

void Client::getWoodType(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get data about wood type request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey woodId{};
	reader >> woodId;

	Q_ASSERT(woodId > 0);
	if (woodId > 0)
	{
		core::WoodType wood = server().repository().woodTypeRepository().get(woodId);
		writer << proto::OperationStatus::Successfull
			   << wood;

		logInformation(QObject::tr("Sent data about wood (name: %1)").arg(wood.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Invalid wood database id"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetWoodTypeResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::destroyWoodType(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Destroy existing wood request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey woodId{};
	reader >> woodId;

	Q_ASSERT(woodId > 0);
	if (woodId > 0)
	{
		bool success = server().repository().woodTypeRepository().destroy(woodId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;
			sendEvent(agent::ResourceOperation::Removed, agent::Resource::WoodType, woodId);

			logInformation(QObject::tr("Wood type has been removed successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove wood type from database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Invalid wood type database id"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::DestroyWoodTypeResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	sock.write(as_bytes(header));
	sock.write(buf);
}

void Client::createWoodType(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Create new wood type request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::WoodType wood{};
	reader >> wood;

	if (server().repository().woodTypeRepository().create(wood))
	{
		Q_ASSERT(wood.id() > 0);
		writer << proto::OperationStatus::Successfull
			   << wood;

		sendEvent(agent::ResourceOperation::Created, agent::Resource::WoodType, wood.id());
		logInformation(QObject::tr("Wood type has been created successfully"));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create wood type in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateWoodTypeResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	sock.write(as_bytes(header));
	sock.write(buf);
}

void Client::updateWoodType(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Update existing wood type request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::WoodType wood{};
	reader >> wood;

	Q_ASSERT(wood.id() > 0);
	if (server().repository().woodTypeRepository().update(wood))
	{
		writer << proto::OperationStatus::Successfull
			   << wood;

		sendEvent(agent::ResourceOperation::Updated, agent::Resource::WoodType, wood.id());
		logInformation(QObject::tr("Wood type has been updated successfully"));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update wood type object in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::UpdateWoodTypeResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	sock.write(as_bytes(header));
	sock.write(buf);
}

//void Client::getWoodType(QDataStream& stream) {
//	core::eloquent::DBIDKey woodId {0};
//	stream >> woodId;

//	Q_ASSERT(woodId > 0);
//	if (woodId <= 0) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		return;
//	}

//	logInformation(QObject::tr("Żądanie pobrania informacji dla drewna o id: %1").arg(woodId));

//	const auto& wood = server()
//					   .repository()
//					   .woodTypeRepository()
//					   .get(woodId);

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << wood;

//	logInformation(QObject::tr("Wysłano informacje o drewnie %1").arg(wood.name()));
//}

//void Client::destroyWoodType(QDataStream& stream) {
//	core::eloquent::DBIDKey woodId {0};
//	stream >> woodId;

//	Q_ASSERT(woodId > 0);
//	if (woodId <= 0) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		return;
//	}

//	logInformation(QObject::tr("Żądanie usunięcia drewna o id: %1").arg(woodId));

//	if (server().repository().woodTypeRepository().destroy(woodId)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		logInformation(QObject::tr("Usunięto drewno o id: %1").arg(woodId));
//		sendEvent(agent::ResourceOperation::Removed, agent::Resource::WoodType, woodId);
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logInformation(QObject::tr("Podczas usuwania drewna o id %1 wystąpiły błędy").arg(woodId));
//	}
//}

//void Client::createWoodType(QDataStream& stream) {
//	core::WoodType wood;
//	stream >> wood;

//	logInformation(QObject::tr("Żądanie utworzenia nowego rodzaju drewna (%1)").arg(wood.name()));

//	if (server().repository().woodTypeRepository().create(wood)) {
//		Q_ASSERT(wood.id() > 0);
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << wood;

//		sendEvent(agent::ResourceOperation::Created, agent::Resource::WoodType, wood.id());
//		logInformation(QObject::tr("Utworzono nowy rodzaj drewna: %1").arg(wood.name()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Podczas zapisu informacji o drewnie %1 wystąpiły błędy").arg(wood.name()));
//	}
//}

//void Client::updateWoodType(QDataStream& stream) {
//	core::WoodType wood;
//	stream >> wood;

//	logInformation(QObject::tr("Żądanie modyfikacji informacji o drewnie %1").arg(wood.name()));

//	if (server().repository().woodTypeRepository().update(wood)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << wood;

//		sendEvent(agent::ResourceOperation::Updated, agent::Resource::WoodType, wood.id());
//		logInformation(QObject::tr("Zmodyfikowano informacje o drewnie %1").arg(wood.name()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logWarning(QObject::tr("Błąd podczas modyfikacji informacji o drewnie %1").arg(wood.name()));
//	}
//}
