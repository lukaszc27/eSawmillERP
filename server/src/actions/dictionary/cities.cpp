#include "client.hpp"
//#include "logger.hpp"
#include "server.hpp"
#include "providers/protocol.hpp"
#include <dbo/city.hpp>
#include <QDataStream>

void Client::getAllCities(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get cities dictorany request"));

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto cities = server().repository().cityRepository().getAll();
	writer << cities;

	proto::MessageHeader header{};
	header.type = proto::msg::GetAllCities;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent list of %1 cities").arg(cities.size()));
}

void Client::getCity(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get city information request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey cityId{};
	reader >> cityId;

	Q_ASSERT(cityId > 0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (cityId <= 0)
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Invalid city identificator"));
	}
	else
	{
		const auto city = server().repository().cityRepository().get(cityId);
		writer << proto::OperationStatus::Successfull
			   << city;

		logInformation(QObject::tr("Sent city (name: %1)").arg(city.name()));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetCityResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::createCity(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Create new city request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::City city{};
	reader >> city;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (server().repository().cityRepository().create(city))
	{
		writer << proto::OperationStatus::Successfull
			   << city;

		logInformation(QObject::tr("City %1 has been created successfully").arg(city.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;

		logInformation(QObject::tr("Cannot create city in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateCityResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}
