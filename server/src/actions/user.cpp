#include "client.hpp"
#include "server.hpp"
#include "providers/protocol.hpp"
#include <dbo/user.h>
#include <QDataStream>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::loginUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Login user in system request"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	QString userName, password;
	QByteArray deviceId;

	in >> userName >> password >> deviceId;

	const bool success = server().repository().userRepository().login(userName, password);

	proto::UserStatus userStatus{};

	QByteArray payload{};
	QDataStream out{&payload, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);


	if (success)
	{
		m_currentUser = server().repository().userRepository().autorizeUser();

		if (server().isUserLogged(currentUser().id()))
		{
			logWarning("User is logged in on other station in the same time");
			out << proto::UserStatus::AlreadyLogged
				<< server().getClientInformation(currentUser().id())->hostName;
		}
		else
		{
			if (!server().attachUserToDevice(deviceId, currentUser(), this))
			{
				logWarning("Cannot find device for user, propably device haven't been registered correctly");
				out << proto::UserStatus::Rejected;
			}
			else
			{
				logInformation(QObject::tr("User %1 is logged in successfully").arg(currentUser().name()));

				out << proto::UserStatus::Accepted
					<< currentUser();

				// send event
				agent::RepositoryEvent event{};
				event.resourceId = currentUser().id();
				event.userId = currentUser().id();
				event.operation = agent::ResourceOperation::Logged;
				event.resourceType = agent::Resource::User;

				server().eventAgent()->pullEvent(std::move(event));
			}
		}
	}
	else
	{
		logWarning(QObject::tr("Cannot authorize user in remote service"));
		out << proto::UserStatus::Rejected;
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.type = proto::msg::UserStatusResponse;
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::logoutUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Logout user request"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey userId{};
	in >> userId;

	QByteArray payload{};
	QDataStream out{&payload, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);

	if (userId > 0)
	{
		if (currentUser().id() == userId)
		{
			if (server().detachUserFromDevice(currentUser()))
			{
				server().releaseAllLocker(userId);
				logInformation(QObject::tr("User %1 %2 is successfully unlogged from service")
							   .arg(currentUser().firstName(), currentUser().surName()));

				agent::RepositoryEvent event{};
				event.operation = agent::ResourceOperation::Logout;
				event.resourceType = agent::Resource::User;
				event.resourceId = userId;
				event.userId = userId;

				server().eventAgent()->pullEvent(std::move(event));

				out << proto::UserStatus::Unlogged;
			}
			else
			{
				logCritical(QObject::tr("Cannot disconnect user from device (user: %1 device: %2)")
							.arg(currentUser().name(), server().getClientInformation(userId)->hostName));

				out << proto::UserStatus::Rejected;
			}
		}
		else
		{
			logCritical(QObject::tr("The user id is not the same on service side"));
			out << proto::UserStatus::Rejected;
		}
	}
	else
	{
		logCritical(QObject::tr("Invalid user identificator"));
		out << proto::UserStatus::Rejected;
	}

	proto::MessageHeader header{};
	header.type = proto::msg::UserStatusResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::getAllUsers(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get users list request"));

	const auto users = server().repository().userRepository().getAllUsers();
	if (users.empty())
	{
		_logger.debug("No users in database. Nothing to send");
	}

	QByteArray buf{};
	QDataStream out{&buf, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);

	out << users;

	proto::MessageHeader header{};
	header.type = proto::msg::GetAllUsersResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	sock.write(as_bytes(header));
	sock.write(buf);
}

void Client::getUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get user response"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey userId{};
	in >> userId;


	QByteArray payload{};
	QDataStream out{&payload, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(userId > 0);
	if (userId <= 0)
	{
		logCritical(QObject::tr("Invalid user identificator"));
		out << proto::UserStatus::Rejected;
	}
	else
	{
		const auto user = server().repository().userRepository().getUser(userId);
		out << proto::UserStatus::Accepted << user;

		logInformation(QObject::tr("Sent user %1 (%2 %3)")
					   .arg(user.name(), user.firstName(), user.surName()));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetUserResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

//void Client::createUser(QDataStream& stream) {
//	core::User user;
//	stream >> user;

//	logInformation(QString("Żądanie utworzenia nowego użytkownika %1 %2").arg(user.firstName(), user.surName()));
//	if (server().repository().userRepository().create(user)) {
//		Q_ASSERT(user.id() > 0);

//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << user;

//		{ // emit event
//			agent::RepositoryEvent event{};
//			event.operation = agent::ResourceOperation::Created;
//			event.resourceType = agent::Resource::User;
//			event.userId = currentUser().id();
//			event.resourceId = user.id();

//			server().eventAgent()->pullEvent(std::move(event));
//		}
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logInformation(QObject::tr("Podczas tworzenia użytkownika w bazie wystąpiły błędy"));
//	}
//}
void Client::createUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Create new user request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::User user{};
	reader >> user;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (server().repository().userRepository().create(user))
	{
		writer << proto::OperationStatus::Successfull
			   << user;

		logInformation(QObject::tr("User %1 has been created successfully").arg(user.name()));

		agent::RepositoryEvent event{};
		event.operation = agent::ResourceOperation::Created;
		event.resourceType = agent::Resource::User;
		event.userId = currentUser().id();
		event.resourceId = user.id();
		server().eventAgent()->pullEvent(std::move(event));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create new user in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateUserResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::updateUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Update existing user request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::User user{};
	reader >> user;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (server().repository().userRepository().update(user))
	{
		writer << proto::OperationStatus::Successfull
			   << user;

		logInformation(QObject::tr("User %1 has ben updated successfully").arg(user.name()));

		agent::RepositoryEvent event{};
		event.operation = agent::ResourceOperation::Updated;
		event.resourceId = user.id();
		event.resourceType = agent::Resource::User;
		event.userId = currentUser().id();
		server().eventAgent()->pullEvent(std::move(event));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update existing user in database!"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::UpdateUserResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}
//void Client::updateUser(QDataStream& stream) {
//	core::User user;
//	stream >> user;

//	logInformation(QString("Żądanie modyfikacji informacji o użytkowniku: %1 %2").arg(user.firstName(), user.surName()));

//	if (server().repository().userRepository().update(user)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		{ // send event
//			agent::RepositoryEvent event{};
//			event.operation = agent::ResourceOperation::Updated;
//			event.resourceId = user.id();
//			event.resourceType = agent::Resource::User;
//			event.userId = currentUser().id();

//			server().eventAgent()->pullEvent(std::move(event));
//		}
//		logInformation(QObject::tr("Zmodyfikowano informacje o użytkowniku: %1 %2").arg(user.firstName(), user.surName()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logInformation(QObject::tr("Podczas zapisu informacji o użytkowniku %1 %2 w bazie danych wystąpiły błędy")
//					   .arg(user.firstName(), user.surName()));
//	}
//}

//void Client::destroyUser(QDataStream& stream) {
//	core::eloquent::DBIDKey userId;
//	stream >> userId;

//	Q_ASSERT(userId > 0);
//	logInformation(QObject::tr("Żądanie usunięcia użytkownika o id: %1").arg(userId));

//	if (server().repository().userRepository().destroy(userId)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		{ // send event
//			agent::RepositoryEvent event{};
//			event.operation = agent::ResourceOperation::Removed;
//			event.resourceId = userId;
//			event.resourceType = agent::Resource::User;
//			event.userId = currentUser().id();

//			server().eventAgent()->pullEvent(std::move(event));
//		}
//		logInformation(QObject::tr("Usunięto użytkownika (id: %1)").arg(userId));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logInformation(QObject::tr("Błąd podczas usuwania użytkownika (id: %1)").arg(userId));
//	}
//}

void Client::destroyUser(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Destroy existing user request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey userId{};
	reader >> userId;

	Q_ASSERT(userId > 0);

	if (userId > 0 && server().repository().userRepository().destroy(userId))
	{
		writer << proto::OperationStatus::Successfull;
		logInformation(QObject::tr("User has been removed successfully"));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot remove user from database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::DestroyUserResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::getUserRoles(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get user roles requestt"));

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto roles = server().repository().userRepository().getAllRoles();
	writer << roles;

	proto::MessageHeader header{};
	header.type = proto::msg::GetUserRolesResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent list of %1 roles").arg(roles.size()));
}

//void Client::getUserRoles(QDataStream& stream) {
//	logInformation(QObject::tr("Żądanie pobrania listy ról użytkownika..."));

//	const core::RoleCollection roles = server()
//									   .repository()
//									   .userRepository()
//									   .getAllRoles();

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << roles;

//	logInformation(QObject::tr("Wysłano listę %1 ról").arg(roles.size()));
//}
