#include "client.hpp"
#include "server.hpp"
#include "providers/protocol.hpp"
#include <QDataStream>
#include <QSet>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::getAllWarehouses(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get all warehouses list request"));

	const auto warehouses = server().repository().warehousesRepository().getAll();

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);
	writer << warehouses;

	proto::MessageHeader header{};
	header.type = proto::msg::GetAllWarehousesResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(warehouses.size()));
}

void Client::getWarehouse(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get warehouse request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey warehouseId{};
	reader >> warehouseId;

	Q_ASSERT(warehouseId > 0);
	if (warehouseId > 0)
	{
		const auto warehouse = server().repository().warehousesRepository().getWarehouse(warehouseId);
		writer << proto::OperationStatus::Successfull
			   << warehouse;

		logInformation(QObject::tr("Sent information about warehouse (name: %1)").arg(warehouse.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get warehouse from database - invalid id (id: %1)").arg(warehouseId));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetWarehouseResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::createWarehouse(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Create new warehouse request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Warehouse warehouse{};
	reader >> warehouse;

	// it's nessesary to create new warehouse in database
	// in other case will be executed update command
	Q_ASSERT(warehouse.id() == 0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const bool success = server().repository().warehousesRepository().create(warehouse);
	if (success)
	{
		Q_ASSERT(warehouse.id() > 0);
		writer << proto::OperationStatus::Successfull
			   << warehouse;

		sendEvent(agent::ResourceOperation::Created, agent::Resource::Warehouse, warehouse.id());
		logInformation(QObject::tr("New warehouse was created (name: %1)").arg(warehouse.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create new warehouse in database"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateWarehouseResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::updateWarehouse(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Update existing warehouse request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Warehouse warehouse{};
	reader >> warehouse;

	Q_ASSERT(warehouse.id() > 0);
	if (warehouse.id() > 0)
	{
		const bool success = server().repository().warehousesRepository().update(warehouse);
		if (success)
		{
			writer << proto::OperationStatus::Successfull
				   << warehouse;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Warehouse, warehouse.id());
			logInformation(QObject::tr("Update existing warehouse successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot update warehouse in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update existing warehouse in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::UpdateWarehouseResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::destroyWarehouse(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Remove existing warehouse request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey warehouseId{};
	reader >> warehouseId;

	Q_ASSERT(warehouseId > 0);
	if (warehouseId > 0)
	{
		const bool success = server().repository().warehousesRepository().destroy(warehouseId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;

			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Warehouse, warehouseId);
			logInformation(QObject::tr("Warehouse has been successfully removed"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove warehouse from database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Invalid warehouse id (id: %1)").arg(warehouseId));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::DestroyWarehouseResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::getArticlesForWarehouse(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get articles from warehouse request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey warehouseId{};
	reader >> warehouseId;

	Q_ASSERT(warehouseId > 0);
	if (warehouseId > 0)
	{
		const auto articles = server().repository().warehousesRepository().getArticles(warehouseId);

		writer << proto::OperationStatus::Successfull
			  << articles;

		logInformation(QObject::tr("Sent list of %1 articles from warehouse (id: %2)").arg(articles.size(), warehouseId));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get articles from warehouse - invalid warehouse id (%1)").arg(warehouseId));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetArticlesResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}
