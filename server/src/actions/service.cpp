#include "client.hpp"
#include "server.hpp"
#include <providers/protocol.hpp>
#include <QDataStream>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::getAllServices(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get services request"));

	const auto services = server().repository().serviceRepository().getAll();

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);
	writer << services;

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetAllServicesResponse;

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(services.size()));
}

void Client::getService(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get service request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey serviceId{};
	reader >> serviceId;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(serviceId > 0);
	if (serviceId > 0)
	{
		const auto service = server().repository().serviceRepository().get(serviceId);

		writer << proto::OperationStatus::Successfull
			   << service;

		logInformation(QObject::tr("Sent service (name: %1)").arg(service.name()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get service from database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetServiceResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::destroyService(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Destroy existing service request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey serviceId;
	reader >> serviceId;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(serviceId > 0);
	if (serviceId > 0)
	{
		const bool success = server().repository().serviceRepository().destroy(serviceId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;
			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Service, serviceId);

			logInformation(QObject::tr("Service has been removed successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove service from database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot remove service from database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::DestroyServiceResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::createService(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Create new service request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Service service{};
	reader >> service;

	Q_ASSERT(service.id() == 0);
	if (service.id() == 0)
	{
		const bool success = server().repository().serviceRepository().create(service);
		if (success)
		{
			Q_ASSERT(service.id());
			writer << proto::OperationStatus::Successfull
				   << service;

			sendEvent(agent::ResourceOperation::Created, agent::Resource::Service, service.id());
			logInformation(QObject::tr("Service has been created"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot create service in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot create service in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::CreateServiceResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::updateService(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Update existing service request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::Service service{};
	reader >> service;

	Q_ASSERT(service.id() > 0);
	if (service.id() > 0)
	{
		const bool success = server().repository().serviceRepository().update(service);
		if (success)
		{
			Q_ASSERT(service.id());
			writer << proto::OperationStatus::Successfull
				   << service;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Service, service.id());
			logInformation(QObject::tr("Service has been updated"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot update service in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update service in database - invalid id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::UpdateServiceResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}
