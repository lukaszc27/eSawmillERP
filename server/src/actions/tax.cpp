#include "client.hpp"
#include "server.hpp"
#include <dbo/tax.h>
#include <providers/protocol.hpp>
#include <dbo/tax.h>
#include <QDataStream>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::getAllTaxes(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get taxes request"));

	const auto taxes = server().repository().TaxRepository().getAll();

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);
	writer << taxes;

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.type = proto::msg::GetAllTaxesResponse;
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(taxes.size()));
}

void Client::getTaxById(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get tax by id request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey taxId{};
	reader >> taxId;

	Q_ASSERT(taxId > 0);
	if (taxId > 0)
	{
		const auto tax = server().repository().TaxRepository().get(taxId);
		writer << proto::OperationStatus::Successfull
			   << tax;

		logInformation(QObject::tr("Sent tax (value: %1)").arg(tax.value()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get tax from database - invalid tax id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.type = proto::msg::GetTaxByIdResponse;
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::getTaxByValue(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get tax by value request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	double val{};
	reader >> val;

	Q_ASSERT(val >= 0);
	if (val >= 0)
	{
		const auto tax = server().repository().TaxRepository().getByValue(val);
		writer << proto::OperationStatus::Successfull
			   << tax;

		logInformation(QObject::tr("Sent tax (value: %1)").arg(tax.value()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get tax from database - invalid tax id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetTaxByValueResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::createTax(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Create new tax object request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Tax tax{};
	reader >> tax;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(tax.id() == 0);
	if (tax.id() == 0)
	{
		bool success = server().repository().TaxRepository().create(tax);
		if (success)
		{
			Q_ASSERT(tax.id() > 0);
			writer << proto::OperationStatus::Successfull
				  << tax;

			sendEvent(agent::ResourceOperation::Created, agent::Resource::Tax, tax.id());
			logInformation(QObject::tr("Tax object has been created successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot create tax object in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Tax already exist in database"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::CreateTaxResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::updateTax(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Update existing tax request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Tax tax{};
	reader >> tax;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(tax.id() > 0);
	if (tax.id() > 0)
	{
		const bool success = server().repository().TaxRepository().update(tax);
		if (success)
		{
			writer << proto::OperationStatus::Successfull
				   << tax;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Tax, tax.id());
			logInformation(QObject::tr("The tax object has been updates successfully"));
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot update tax object in database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot update tax object in database - you have to create first"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::UpdateTaxResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

void Client::destroyTax(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Remove tax request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey taxId{};
	reader >> taxId;

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	Q_ASSERT(taxId > 0);
	if (taxId > 0)
	{
		const bool success = server().repository().TaxRepository().destroy(taxId);
		if (success)
		{
			writer << proto::OperationStatus::Successfull;
			logInformation(QObject::tr("The tax has been removed from database"));

			sendEvent(agent::ResourceOperation::Removed, agent::Resource::Tax, taxId);
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot remove tax object from database"));
		}
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot remove object from database - invalid tax id"));
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::DestroyTaxResponse;

	socket.write(as_bytes(header));
	socket.write(buf);
}

//void Client::destroyTax(QDataStream& stream) {
//	core::eloquent::DBIDKey taxId {0};
//	stream >> taxId;

//	Q_ASSERT(taxId > 0);
//	if (taxId <= 0) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		return;
//	}

//	logInformation(QObject::tr("Żądanie usunięcia stawki VAT (id: %1)").arg(taxId));

//	if (server().repository().TaxRepository().destroy(taxId)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);

//		logInformation(QObject::tr("Usunięto stawkę VAT (id: %1)").arg(taxId));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logWarning(QObject::tr("Błąd podczas usuwania stawki VAT z bazy danych (id: %1)").arg(taxId));
//	}
//}

//void Client::createTax(QDataStream& stream) {
//	core::Tax tax;
//	stream >> tax;

//	logInformation(QObject::tr("Żądanie utworzenia stawki VAT %1 %").arg(tax.value()));

//	if (server().repository().TaxRepository().create(tax)) {
//		Q_ASSERT(tax.id() > 0);
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << tax;

//		sendEvent(agent::ResourceOperation::Created, agent::Resource::Tax, tax.id());
//		logInformation(QObject::tr("Utworzono stawkę VAT (id: %1)").arg(tax.id()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
//		logWarning(QObject::tr("Błąd podczas tworzenia obiektu VAT w bazie danych!"));
//	}
//}

//void Client::updateTax(QDataStream& stream) {
//	core::Tax tax;
//	stream >> tax;

//	logInformation(QObject::tr("Żądanie modyfikacji stawki VAT %1 %").arg(tax.value()));

//	if (server().repository().TaxRepository().update(tax)) {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//			   << tax;

//		sendEvent(agent::ResourceOperation::Updated, agent::Resource::Tax, tax.id());
//		logInformation(QObject::tr("Zmodyfikowano stawkę VAT %1 %2 (id: %3)")
//			.arg(tax.value())
//			.arg(tax.id()));
//	} else {
//		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);

//		logWarning(QObject::tr("Podczas modyfikacji stawki VAT w bazie wystąpiły błędy!"));
//	}
//}
