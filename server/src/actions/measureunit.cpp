#include "client.hpp"
#include "server.hpp"
#include <QJsonObject>
#include <QJsonDocument>
#include <dbo/measureunit_core.hpp>
#include <providers/protocol.hpp>
#include <dbo/measureunit_core.hpp>
#include <QDataStream>
#include <QDataStream>

void Client::getMeasureUnits(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get measure units list request"));

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto units = server().repository().measureUnitRepository().getAll();
	writer << units;

	proto::MessageHeader header{};
	header.type = proto::msg::GetMeasureUnitsResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	socket.write(as_bytes(header));
	socket.write(payload);

	logInformation(QObject::tr("Sent list of %1 measure units").arg(units.size()));
}

void Client::getMeasureUnit(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get measure unit request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey unitId{};
	reader >> unitId;

	Q_ASSERT(unitId > 0);
	if (unitId > 0)
	{
		const auto unit = server().repository().measureUnitRepository().get(unitId);
		writer << proto::OperationStatus::Successfull
			   << unit;

		logInformation(QObject::tr("Sent measure unit (name: %1)").arg(unit.fullName()));
	}
	else
	{
		writer << proto::OperationStatus::Fail;
		logWarning(QObject::tr("Cannot get measure unit from database (id: %1)").arg(unitId));
	}

	proto::MessageHeader header{};
	header.type = proto::msg::GetMeasureUnitResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	socket.write(as_bytes(header));
	socket.write(payload);
}
