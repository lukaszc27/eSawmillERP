#include "client.hpp"
#include "server.hpp"
//#include "logger.hpp"
#include <QDataStream>
#include <dbo/company.h>
#include <dbo/contact.h>
#include <providers/protocol.hpp>

namespace agent = core::providers::agent;

namespace {

bool canUserEditCompany(const core::User& user) {
	return user.hasRole(core::Role::Operator) && user.hasRole(core::Role::Administrator);
}

} // namespace

void Client::getCompany(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Get company for logged user request"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	core::eloquent::DBIDKey companyId{};
	in >> companyId;

	Q_ASSERT(companyId > 0);

	const auto company = server().repository().companiesRepository().getCompany(companyId);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	writer << company;

	proto::MessageHeader header{};
	header.type = proto::msg::GetCompanyResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);

	logInformation(QObject::tr("Sent company information: (name: %1)").arg(company.name()));
}

void Client::createCompany(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Create company request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Company company{};
	reader >> company;

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (!canUserEditCompany(currentUser()))
	{
		logWarning(QObject::tr("Current logged user has not have permissions to create or edit comany information"));
		writer << proto::OperationStatus::Fail;
	}
	else
	{
		const bool success = server().repository().companiesRepository().create(company);
		if (success)
		{
			Q_ASSERT(company.id() > 0);
			writer << proto::OperationStatus::Successfull
				   << company;

			sendEvent(agent::ResourceOperation::Created, agent::Resource::Company, company.id());
			logInformation(QObject::tr("Created new company (name: %1, NIP: %2)").arg(company.name(), company.NIP()));
		}
		else
		{
			logWarning(QObject::tr("Durning create company object in database process were errors"));
			writer << proto::OperationStatus::Fail;
		}
	}

	proto::MessageHeader header{};
	header.type = proto::msg::CreateCompanyResponse;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::updateCompany(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Update existing company request"));

	QDataStream reader{&data, QIODevice::ReadOnly};
	reader.setVersion(QDataStream::Version::Qt_5_0);

	core::Company company{};
	reader >> company;

	// it's important to update existing object in db
	Q_ASSERT(company.id() > 0);

	QByteArray payload{};
	QDataStream writer{&payload, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	if (!canUserEditCompany(currentUser()))
	{
		logWarning(QObject::tr("Current logged user has not permissions to create/edit company object in database"));
		writer << proto::OperationStatus::Fail;
	}
	else
	{
		const bool success = server().repository().companiesRepository().update(company);
		if (success)
		{
			logInformation(QObject::tr("Information about company has been changed"));
			writer << proto::OperationStatus::Successfull
				   << company;

			sendEvent(agent::ResourceOperation::Updated, agent::Resource::Company, company.id());
		}
		else
		{
			writer << proto::OperationStatus::Fail;
			logWarning(QObject::tr("Cannot update inforamtion about existing company in database"));
		}
	}

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.type = proto::msg::UpdateCompanyResponse;
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}
