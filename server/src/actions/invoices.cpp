#include "client.hpp"
#include "server.hpp"
#include <QDataStream>
#include <providers/protocol.hpp>
//#include "logger.hpp"

namespace agent = core::providers::agent;

void Client::getSaleInvoice(QDataStream& stream) {
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0) {
		logWarning(QObject::tr("Odczymano nieprawidłowy identyfikator dokumentu sprzedaży!"));

		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		return;
	}

	logInformation(QObject::tr("Pobranie dokumentu sprzedaży o id: %1").arg(invoiceId));

	const auto invoice = server()
						 .repository()
						 .invoiceRepository()
						 .getSaleInvoice(invoiceId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << invoice;

	logInformation(QObject::tr("Wysłano dokument sprzedaży nr %1").arg(invoice.number()));
}

//void Client::getAllSaleInvoices(QDataStream& stream) {
//	logInformation(QObject::tr("Pobranie wszystkich dokumentów sprzedaży"));

//	const auto invoices = server()
//						  .repository()
//						  .invoiceRepository()
//						  .getAllSaleInvoices();

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << invoices;

//	logInformation(QObject::tr("Wysłano listę %1 dokumentów sprzedaży").arg(invoices.size()));
//}
void Client::getAllSaleInvoies(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get list of sale invoices request"));

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto invoices = server().repository().invoiceRepository().getAllSaleInvoices();
	writer << invoices;

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetAllSaleInvoicesResponse;

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent lit of %1 items").arg(invoices.size()));
}

void Client::updateSaleInvoice(QDataStream& stream) {
	core::SaleInvoice invoice;
	stream >> invoice;

	Q_ASSERT(invoice.id() > 0);
	if (invoice.items().empty()) {
		logWarning(QObject::tr("Dokument sprzedaży nie posiada żadnych elementów składnikowych!"));
	}

	const bool success = server()
						 .repository()
						 .invoiceRepository()
						 .updateSaleInvoice(invoice);

	if (success) {
		logInformation(QObject::tr("Zaktualizowano dokument sprzedaży nr %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
			   << invoice;
	} else {
		logWarning(QObject::tr("Błąd podczas aktualizacji dokumentu sprzedaży nr %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
	}
}

void Client::createSaleInvoice(QDataStream& stream) {
	core::SaleInvoice invoice;
	stream >> invoice;

	Q_ASSERT(invoice.id() == 0);
	if (invoice.items().empty()) {
		logWarning(QObject::tr("Dokument sprzedaży nie posiada żadnych elementów składnikowych!"));
	}

	const auto success = server()
						 .repository()
						 .invoiceRepository()
						 .createSaleInvoice(invoice);
	if (success) {
		logInformation(QObject::tr("Utworzono dokument sprzedaży nr %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
			   << invoice;
	} else {
		logWarning(QObject::tr("Błąd podczas tworzenia dokumentu sprzedaży w bazie danych!"));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
	}
}

void Client::createPurchaseInvoice(QDataStream& stream) {
	core::PurchaseInvoice invoice;
	stream >> invoice;

	logInformation(QObject::tr("Tworzenie nowego dokumentu zakupu"));
	if (invoice.items().empty()) {
		logWarning(QObject::tr("Dokument zakupu nie posiada żadnych elementów składnikowych!"));
	}

	if (server().repository().invoiceRepository().createPurchaseInvoice(invoice)) {

		logInformation(QObject::tr("Utworzono dokument zakupu nr: %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
			   << invoice;
	} else {
		logWarning(QObject::tr("Błąd podczas zapisu dokumentu zakupu w bazie danych!"));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
	}
}

void Client::updatePurchaseInvoice(QDataStream& stream) {
	core::PurchaseInvoice invoice;
	stream >> invoice;

	logInformation(QObject::tr("Aktualizacja dokumentu zakupu nr %1").arg(invoice.number()));
	if (invoice.items().empty()) {
		logWarning(QObject::tr("Dokument zakupu nie posiada żadnych elementów składnikowych!"));
	}

	if (server().repository().invoiceRepository().updatePurchaseInvoice(invoice)) {

		logInformation(QObject::tr("Zaktualizowano dokument zakupu nr %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
			   << invoice;
	} else {
		logWarning(QObject::tr("Błąd podczas aktualizacji dokumentu zakupu nr %1").arg(invoice.number()));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
	}
}

void Client::getItemsForSaleInvoice(QDataStream& stream) {
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logCritical(QObject::tr("Odczymano nieprawidłowy identyfikator dokumentu sprzedaży!"));
		return;
	}
	logInformation(QObject::tr("Pobranie elementów dla dokumentu sprzedaży (id: %1)").arg(invoiceId));

	const auto items = server()
					   .repository()
					   .invoiceRepository()
					   .getSaleInvoiceItems(invoiceId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << items;

	logInformation(QObject::tr("Wysłano listę %1 elementów dla dokumentu sprzedaży").arg(items.size()));
}

void Client::markSaleInvoiceAsCorrectionDocument(QDataStream& stream) {
	core::eloquent::DBIDKey orginalId {0};
	core::eloquent::DBIDKey correctionId {0};

	stream >> orginalId >> correctionId;
	Q_ASSERT(orginalId > 0 && correctionId > 0);
	if (orginalId <= 0 || correctionId <= 0) {
		stream << core::providers::Protocol::MsgFail;
		logWarning(QObject::tr("Odebrano nieprawidłowe identyfikatory obiektów dokumentów sprzedaży."));
		return;
	}

	bool success {false};
	{
		success = server()
				  .repository()
				  .invoiceRepository()
				  .markSaleInvoiceAsCorrectionDocument(orginalId, correctionId);
	}
	if (success) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);
		logInformation(QObject::tr("Nadano korektę dla dokumentu sprzedaży (%1 => %2)")
					   .arg(orginalId)
					   .arg(correctionId));
	} else {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logWarning(QObject::tr("Błąd podczas nadawania korekty dla dokumentu sprzedaży (%1 => %2")
				   .arg(orginalId)
				   .arg(correctionId));
	}
}

void Client::getSaleInvoiceCorrectionDocument(QDataStream& stream) {
	core::eloquent::DBIDKey orginalId {0};
	stream >> orginalId;

	Q_ASSERT(orginalId > 0);
	if (orginalId <= 0) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logWarning(QObject::tr("Odebrano nieprawidłowy identyfikator dokumentu sprzedaży!"));
		return;
	}
	logInformation(QObject::tr("Żądanie pobrania dokumentu korekty dla dokumentu sprzedaży o id: %1").arg(orginalId));

	const core::SaleInvoice invoice = server()
									  .repository()
									  .invoiceRepository()
									  .getCorrectionForSaleInvoice(orginalId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << invoice;

	logInformation(QObject::tr("Wysłano dokument sprzedaży nr: %1").arg(invoice.number()));
}

void Client::getPurchaseInvoiceCorrectionDocument(QDataStream& stream) {
	logInformation(QObject::tr("Żądanie pobrania dokumentu korekty dla dokumentu zakupu..."));
	core::eloquent::DBIDKey orginalId {0};

	stream >> orginalId;
	Q_ASSERT(orginalId > 0);
	if (orginalId <= 0) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logWarning(QObject::tr("Odczymano nieprawidłowy identyfikator dokumentu zakupu."));
		return;
	}

	const core::PurchaseInvoice invoice = server()
										  .repository()
										  .invoiceRepository()
										  .getCorrectionForPurchaseInvoice(orginalId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << invoice;

	logInformation(QObject::tr("Wysłano dokument nr %1").arg(invoice.number()));
}

void Client::postSaleInvoice(QDataStream& stream) {
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0) {
		logWarning(QObject::tr("Odebrano nieprawidłowy identyfikator dokuemtnu sprzedaży do zaksięgowania."));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		return;
	}

	logInformation(QObject::tr("Księgowanie dokumentu sprzedaży o id: %1").arg(invoiceId));

	const auto success = server().repository()
						 .invoiceRepository()
						 .postSaleInvoice(invoiceId);

	if (success) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);
	} else {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logWarning(QObject::tr("Podczas księgowania dokumentu sprzedaży o id %1 wystąpiły błędy").arg(invoiceId));
	}
}

void Client::postPurchaseInvoice(QDataStream& stream) {
	logInformation("Żądanie zaksięgowania dokumentu zapupu...");
	
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId >= 0);
	if (invoiceId <= 0) {
		stream << core::providers::Protocol::MsgFail;
		return;
	}
	

	const bool success = server()
						 .repository()
						 .invoiceRepository()
						 .postPurchaseInvoice(invoiceId);

	if (success) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);
		logInformation(QObject::tr("Zaksięgowano dokument zakupu o id: %1").arg(invoiceId));
	} else {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logWarning(QObject::tr("Błąd podczas księgowania dokumentu zakupu o id: %1").arg(invoiceId));
	}
}

void Client::getPurchaseInvoice(QDataStream& stream) {
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0) {
		logWarning(QObject::tr("Otrzymano nieprawidłowy identyfikator dla dokumentu zakupu!"));
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		return;
	}

	logInformation(QObject::tr("Pobranie dokumentu zakupu o id: %1").arg(invoiceId));

	const auto invoice = server()
				   .repository()
				   .invoiceRepository()
				   .getPurchaseInvoice(invoiceId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess);
	logInformation(QObject::tr("Wysłano dokument zakupu nr %1").arg(invoice.number()));
}

void Client::getPurchaseInvoices(QDataStream& stream) {
	logInformation(QObject::tr("Żądanie pobrania listy dokumentów zakupu..."));

	const auto invoices = server()
						  .repository()
						  .invoiceRepository()
						  .getAllPurchaseInvoices();

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << invoices;

	logInformation(QObject::tr("Wysłano listę %1 dokumentów zakupu").arg(invoices.size()));
}

void Client::getAllPurchaseInvoices(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Get list of purhcase invoices request"));

	QByteArray buf{};
	QDataStream writer{&buf, QIODevice::WriteOnly};
	writer.setVersion(QDataStream::Version::Qt_5_0);

	const auto invoices = server().repository().invoiceRepository().getAllPurchaseInvoices();
	writer << invoices;

	proto::MessageHeader header{};
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = buf.size();
	header.crc = qChecksum(buf.constData(), buf.size());
	header.type = proto::msg::GetAllPurchaseInvoicesResponse;

	socket.write(as_bytes(header));
	socket.write(buf);

	logInformation(QObject::tr("Sent list of %1 items").arg(invoices.size()));
}
//void Client::getAllPurchaseInvoices(QDataStream& stream) {
//	logInformation(QObject::tr("Pobranie wszystkich dokumentów zakupu..."));

//	const auto invoices = server()
//						  .repository()
//						  .invoiceRepository()
//						  .getAllPurchaseInvoices();

//	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
//		   << invoices;

//	logInformation(QObject::tr("Wysłano listę %1 dokumentów zakupu").arg(invoices.size()));
//}

void Client::getItemsForPurchaseInvoice(QDataStream& stream) {
	core::eloquent::DBIDKey invoiceId {0};
	stream >> invoiceId;

	Q_ASSERT(invoiceId > 0);
	if (invoiceId <= 0) {
		stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgFail);
		logCritical(QObject::tr("Odczytano nieprawidłowy identyfikator dla dokumentu sprzedaży"));
		return;
	}
	logInformation(QObject::tr("Pobieranie elementów dla dokumentu zakupu (id: %1").arg(invoiceId));

	const auto items = server().repository()
					   .invoiceRepository()
					   .getPurchaseInvoiceItems(invoiceId);

	stream << static_cast<std::uint32_t>(core::providers::Protocol::MsgSuccess)
		   << items;

	logInformation(QObject::tr("Wysłano listę %1 elementów.").arg(items.size()));
}

QJsonObject Client::getHasSaleInvoiceCorrectionAttribute(core::eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	const bool val = server()
			   .repository()
			   .invoiceRepository()
			   .hasSaleInvoiceCorrection(saleInvoiceId);

	QJsonObject attr;
	attr["name"] = "hasSaleInvoiceCorrection";
	attr["value"] = val;

	return attr;
}

QJsonObject Client::getHasPurchaseInvoiceCorrectionAttribute(core::eloquent::DBIDKey purchaseInvoiceId) {
	Q_ASSERT(purchaseInvoiceId > 0);
	const bool val = server()
			   .repository()
			   .invoiceRepository()
			   .hasPurchaseInvoiceCorrection(purchaseInvoiceId);

	QJsonObject attr;
	attr["name"] = "hasPurchaseInvoiceCorrection";
	attr["value"] = val;
	return attr;
}

QJsonObject Client::getHasSaleInvoiceAssignedOrder(core::eloquent::DBIDKey saleInvoiceId) {
	Q_ASSERT(saleInvoiceId > 0);
	bool val = server()
			   .repository()
			   .invoiceRepository()
			   .hasSaleInvoiceAssignedOrder(saleInvoiceId);

	QJsonObject attr;
	attr["name"] = "hasSaleInvoiceAssignedOrder";
	attr["value"] = val;
	return attr;
}
