#include "state_machine.hpp"
#include <iostream>


namespace service {

namespace {
static std::string getStringValue(
	const std::string_view& description,
	const std::string_view& def_value = std::string_view())
{
	std::string val {};
	std::cout << description;
	std::getline(std::cin, val);
	if (val.empty())
		return std::string {def_value};

	return val;
}

static bool getBooleanValue(
	const std::string_view& description,
	bool def_value = false)
{
	bool val = def_value;
	std::string input;
	do {
		std::cout << description;
		std::getline(std::cin, input);
		if (input.empty())
			return def_value;

		if (input.size() > 1)
			continue;

		const unsigned char& ch = input[0];
		if (ch != 'N' && ch != 'n' && ch != 'T' && ch != 't')
			continue;

		val = ch == 'T' || ch == 't';
		break;
	} while (true);
	return val;
}

static std::string_view printBooleanValue(bool val) {
	if (val)
		return std::string_view {"TAK"};

	return std::string_view {"NIE"};
}
}

DatabaseConfgurationPhase::DatabaseConfgurationPhase(StateMachine& machine)
	: AbstractPhase {machine}
{
}

void DatabaseConfgurationPhase::operator()() {
	std::string driver, host, databaseName, userName, userPassword, userConfirmPassword;
	bool trustedConnection, windowsAuth;

	std::cout << "Wprowadz informacje do polaczenia z serwerem bazodanowym (obslugiwane bazy danych: Microsoft SQL Server)" << std::endl;
	driver = getStringValue("Nazwa sterownika [ODBC Driver 17 for SQL Server]: ", "ODBC Driver 17 for SQL Server");
	databaseName = getStringValue("Nazwa bazy danych [SAWMILL]: ", "SAWMILL");
	host = getStringValue("Adres hosta [127.0.0.1]: ", "127.0.0.1");
	userName = getStringValue("Nazwa użytkownika [SA]: ", "SA");
	do {
		userPassword = getStringValue("Haslo []: ");
		userConfirmPassword = getStringValue("Powtorz haslo []: ");
		if (userPassword != userConfirmPassword) {
			std::cout << "Podane hasla nie sa identyczne, sprobuj jeszcze raz!";
		}
	} while (userPassword != userConfirmPassword);
	trustedConnection = getBooleanValue("Trusted Connection [T\\n]: ", true);
	windowsAuth = getBooleanValue("Windows Authorization [T\\n]: ", true);

	// save configuration into settings file
	QSettings settings;
	settings.beginGroup("database");
	settings.setValue("driver", QString::fromStdString(driver));
	settings.setValue("host", QString::fromStdString(host));
	settings.setValue("name", QString::fromStdString(databaseName));
	settings.setValue("trustedConnection", trustedConnection);
	settings.setValue("userName", QString::fromStdString(userName));
	settings.setValue("userPassword", QString::fromStdString(userPassword));
	settings.setValue("windowsAuthorization", windowsAuth);
	settings.endGroup();
	settings.sync();

	std::cout << "Zapisano:" << std::endl
			  << "+ Driver: " << driver << std::endl
			  << "+ Host: " << host << std::endl
			  << "+ Baza: " << databaseName << std::endl
			  << "+ Uzytkownik: " << userName << std::endl
			  << "+ Haslo: " << userPassword << std::endl
			  << "+ TrustedConnection: " << printBooleanValue(trustedConnection) << std::endl
			  << "+ Windows Authorization: " << printBooleanValue(windowsAuth) << std::endl;

	// get event agent configuration
	std::string eventAgentMulticastAddress;
	int eventAgentPort;
	int servicePort;

	std::cout << "--- USTAWIENIA PRACY SERWERA ---" << std::endl;
	do {
		servicePort = QString::fromStdString(
			getStringValue("Port serwera [27015]: ", "27015")
		).toInt();
		if (servicePort <= 0 || servicePort > 65535) {
			std::cout << "Wprowadzona wartosc jest spoza dozwolonego zakresu [1...65535]" << std::endl;
		}
	} while (servicePort <= 0 || servicePort > 65535);

	do {
		eventAgentPort = QString::fromStdString(
			getStringValue("Port uslugi powiadamiania [5050]: ", "5050")
		).toInt();

		if (eventAgentPort <= 0 || eventAgentPort > 65535) {
			std::cout << "Wprowadzona wartosc jest spoza dozwolonego zakresu [1...65535]" << std::endl;
		}
		if (eventAgentPort == servicePort) {
			std::cout << "Port dzialania serwera oraz uslugi powiadomien sa takie same!" << std::endl;
		}
	} while (eventAgentPort <= 0 || eventAgentPort > 65535 || eventAgentPort == servicePort);

	do {
		eventAgentMulticastAddress = getStringValue("Adres nadawania komunikatow [239.255.43.21]: ", "239.255.43.21");
		QHostAddress address(QString::fromStdString(eventAgentMulticastAddress));
		if (!address.isMulticast()) {
			std::cout << "Wprowadzony adres nie nalezny do grupy adresow multicast!" << std::endl
					  << "Wprowadz adres z przedzialu 239.0.0.0 - 239.255.255.255" << std::endl;
		} else {
			break;
		}
	} while (true);

	// save data into file
	settings.beginGroup("remote");
	settings.setValue("eventAgent", QString::fromStdString(eventAgentMulticastAddress));
	settings.setValue("eventAgentPort", eventAgentPort);
	settings.setValue("serviceHostPort", servicePort);
	settings.endGroup();
	settings.sync();

	std::cout << "Zapisano:" << std::endl
		<< "+ EventAgent adres IP: " << eventAgentMulticastAddress << std::endl
		<< "+ EventAgent port: " << eventAgentPort << std::endl
		<< "+ Port uslugi: " << servicePort << std::endl;


	// try initialize service once again
	machine_.changePhase(service::StateMachine::PhaseId::Initialization);
}

} // namespace service
