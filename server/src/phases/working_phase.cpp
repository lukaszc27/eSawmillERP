#include "state_machine.hpp"
#include "logger.hpp"

namespace service {

WorkingPhase::WorkingPhase(StateMachine& machine)
	: AbstractPhase {machine}
{
}

void WorkingPhase::operator()() {
	if (const auto exitCode = machine_.app_.exec(); exitCode != 0)
		machine_.changePhase(StateMachine::PhaseId::Stopped);
	else
		machine_.changePhase(StateMachine::PhaseId::Finished);
}

} // namespace service
