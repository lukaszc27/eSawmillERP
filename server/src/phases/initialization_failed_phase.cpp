#include "state_machine.hpp"
#include <iostream>

namespace service {

InitializationFailedPhase::InitializationFailedPhase(StateMachine& machine)
	: AbstractPhase {machine}
{
}

void InitializationFailedPhase::operator()() {
	std::cerr << "Podczas uruchamiania usługi produkcyjnej wystąpiły nieoczekiwane błędy!" << std::endl
			  << "Sprawdź konfigurację startową usługi i uruchom usługę ponownie" << std::endl;

	machine_.terminate();
}

}
