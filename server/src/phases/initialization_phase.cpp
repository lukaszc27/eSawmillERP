#include "state_machine.hpp"
#include <iostream>
#include <settings/appsettings.hpp>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>

namespace service {

InitializationPhase::InitializationPhase(StateMachine& machine) noexcept
	: AbstractPhase {machine}
{
}

void InitializationPhase::operator()() {
	std::cout << "Production service startup" << std::endl
			  << " + Compile date: " << __DATE__ << std::endl
			  << " + Compile time: " << __TIME__ << std::endl
			  << "------------------------------------------" << std::endl
			  << std::endl << std::endl;

	try
	{
		QSettings settings;
		core::settings::AuthInformation ai;
		ai.databaseName			= settings.value("database/name").toString();
		ai.userName				= settings.value("database/userName").toString();
		ai.password				= settings.value("database/userPassword").toString();
		ai.driver				= settings.value("database/driver").toString();
		ai.host					= settings.value("database/host").toString();
		ai.trustedConnection	= settings.value("database/trustedConnection").toBool();
		ai.windowsAuthorization = settings.value("database/windowsAuthorization").toBool();

		const int servicePort = settings.value("remote/serviceHostPort").toInt();
		const QString& eventAgentMulticastAddress = settings.value("remote/eventAgent").toString();
		const int eventAgentPort = settings.value("remote/eventAgentPort").toInt();

		QSqlDatabase database = QSqlDatabase::addDatabase("QODBC");
		database.setDatabaseName(ai.connectionString());
		if (!database.open()) {
			std::cout << "eSawmill ControlCenter" << std::endl
					  << "Brak polaczenia z baza danych!" << std::endl
					  << database.lastError().text().toStdString() << std::endl
					  << ai.connectionString().toStdString() << std::endl;

			machine_.changePhase(service::StateMachine::PhaseId::DatabaseConfiguration);
		}
		else
		{
			machine_.logger_ = std::make_unique<Logger>();

			machine_.eventAgent_ = std::make_unique<EventAgent>(
				QHostAddress(eventAgentMulticastAddress),
				eventAgentPort, *machine_.logger_);

			machine_.server_ = std::make_unique<Server>(
				servicePort,
				machine_.eventAgent_.get(),
				*machine_.logger_);

			QObject::connect(&(*machine_.eventAgent_), &QThread::finished,
					&(*machine_.eventAgent_), &EventAgent::deleteLater);

			machine_.changePhase(service::StateMachine::PhaseId::Working);
		}
	}
	catch (const std::exception&)
	{
		// goto initialization failed phase when any exception will be thrown
		machine_.changePhase(service::StateMachine::PhaseId::InitializationFailed);
	}
}

} // namespace service
