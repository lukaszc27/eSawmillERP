#include "logger.hpp"
#include <iostream>
#include <QDateTime>
#include <QReadLocker>
#include <QWriteLocker>
#include <map>

void Logger::log(const QString& message, Type type) {
	bool processed {false};
	{
		QWriteLocker locker (&_logs_locker);
		processed = !_logs.empty();
		_logs.push(std::make_tuple(type, message));
	}
	if (!processed) {
		doLog();
//		QtConcurrent::run(std::bind(&Logger::doLog, this));
	}
}

#ifndef __linux__
void Logger::doLog() const {
	constexpr static auto log = [](std::wostream& out, Type type, const QString& msg)
	{
		static std::map<Type, QString> typeText {
			{ Type::Information,	"Information"	},
			{ Type::Warning,		"Warning"		},
			{ Type::Critical,		"Critical"		},
			{ Type::Debug,			"DEBUG"			},
			{ Type::Trace,			"TRACE"			}
		};
		out << QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdWString() << '\t'
			<< typeText[type].toStdWString() << '\t'
			<< msg.toStdWString() << std::endl;
	};
	
	QReadLocker locker (&_logs_locker);
	while (!_logs.empty()) {
		auto [type, msg] = _logs.front();
		log((type == Type::Critical ? std::wcerr : std::wcout), type, msg);
		_logs.pop();
	}
}
#else
void Logger::doLog() const {
	constexpr static auto log = [](std::ostream& out, Type type, const QString& msg)
	{
		static std::map<Type, QString> typeText {
			{ Type::Information,	"Information"	},
			{ Type::Warning,		"Warning"		},
			{ Type::Critical,		"Critical"		},
			{ Type::Debug,			"DEBUG"			},
			{ Type::Trace,			"TRACE"			}
		};
		out << QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString() << '\t'
			<< typeText[type].toStdString() << '\t'
			<< msg.toStdString() << std::endl;
	};

	QReadLocker locker (&_logs_locker);
	while (!_logs.empty()) {
		auto [type, msg] = _logs.front();
		log ((type == Type::Critical ? std::cerr : std::cout), type, msg);
		_logs.pop();
	}
}
#endif
