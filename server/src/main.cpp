#include <QCoreApplication>
#include <QVersionNumber>
#include <iostream>
#include <string>
#include <string_view>
//#include "server.hpp"
#include "state_machine.hpp"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	auto&& version = QVersionNumber {0, 0, 1};

	app.setApplicationName("ProductionCenter");
	app.setApplicationVersion(version.toString());
	app.setOrganizationName("IDsoft");
	app.setOrganizationDomain("IDsoft.com");

	try
	{
		service::StateMachine stateMachine {app};
		stateMachine.registerPhase<service::InitializationPhase>();
		stateMachine.registerPhase<service::DatabaseConfgurationPhase>();
		stateMachine.registerPhase<service::InitializationFailedPhase>();
		stateMachine.registerPhase<service::WorkingPhase>();

		do
		{
			(*stateMachine.currentPhase())();
		}
		while (!stateMachine.isTerminated());
	}
	catch (std::exception& ex)
	{
		std::cerr << "Uncaught exception in main function!" << std::endl
				  << ex.what();
	}

	return 0;
}
