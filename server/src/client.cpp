#include "client.hpp"
#include <QDataStream>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <QString>
#include <QTime>
#include <iostream>
#include "logger.hpp"
#include "providers/protocol.hpp"
#include "server.hpp"
#include <cstdint>
#include <cstring>
#include <functional>
#include <cstddef>

// use models from eSawmill core library
#include <eloquent/model.hpp>
#include <eloquent/builder.hpp>
#include <dbo/user.h>


Client::Client(Server& server, QTcpSocket* socket, core::logs::AbstractLogger& logger, QObject* parent)
	: QObject {parent}
	, _logger {logger}
	, m_socket {socket}
	, m_server {server}
{
	registerCommands();

	//	_actions[core::providers::Protocol::MsgRegisterDevice]		= &Client::registerDevice;
//	_actions[core::providers::Protocol::MsgUnregisterDevice]	= &Client::unregisterDevice;

//	_actions[core::providers::Protocol::MsgGetAllUser]			= &Client::getAllUsers;
//	_actions[core::providers::Protocol::MsgUserLogin]			= &Client::loginUser;
//	_actions[core::providers::Protocol::MsgUserLogout]			= &Client::logoutUser;
//	_actions[core::providers::Protocol::MsgGetUser]				= &Client::getUser;
//	_actions[core::providers::Protocol::MsgCreateUser]			= &Client::createUser;
//	_actions[core::providers::Protocol::MsgUpdateUser]			= &Client::updateUser;
//	_actions[core::providers::Protocol::MsgDestroyUser]			= &Client::destroyUser;
//	_actions[core::providers::Protocol::MsgGetUserRoles]		= &Client::getUserRoles;

//	_actions[core::providers::Protocol::MsgGetCompany]			= &Client::getCompany;
//	_actions[core::providers::Protocol::MsgUpdateCompany]		= &Client::updateCompany;
//	_actions[core::providers::Protocol::MsgCreateCompany]		= &Client::createCompany;

//	_actions[core::providers::Protocol::MsgGetWarehouses]		= &Client::getAllWarehouses;
//	_actions[core::providers::Protocol::MsgGetWarehouse]		= &Client::getWarehouse;
//	_actions[core::providers::Protocol::MsgCreateWarehouse]		= &Client::createWarehouse;
//	_actions[core::providers::Protocol::MsgUpdateWarehouse]		= &Client::updateWarehouse;
//	_actions[core::providers::Protocol::MsgDestroyWarehouse]	= &Client::destroyWarehouse;

//	_actions[core::providers::Protocol::MsgGetArticleTypes]		= &Client::getArticleTypes;
//	_actions[core::providers::Protocol::MsgGetArticleType]		= &Client::getArticleType;

//	_actions[core::providers::Protocol::MsgGetMeasureUnit]		= &Client::getMeasureUnit;
//	_actions[core::providers::Protocol::MsgGetMeasureUnits]		= &Client::getMeasureUnits;

//	_actions[core::providers::Protocol::MsgGetCities]			= &Client::getCities;
//	_actions[core::providers::Protocol::MsgGetCity]				= &Client::getCity;
//	_actions[core::providers::Protocol::MsgCreateCity]			= &Client::createCity;

//	_actions[core::providers::Protocol::MsgGetAllTaxes]			= &Client::getAllTaxes;
//	_actions[core::providers::Protocol::MsgGetTaxById]			= &Client::getTaxById;
//	_actions[core::providers::Protocol::MsgGetTaxByValue]		= &Client::getTaxByValue;
//	_actions[core::providers::Protocol::MsgDestroyTax]			= &Client::destroyTax;
//	_actions[core::providers::Protocol::MsgUpdateTax]			= &Client::updateTax;
//	_actions[core::providers::Protocol::MsgCreateTax]			= &Client::createTax;

//	_actions[core::providers::Protocol::MsgGetWoodTypes]		= &Client::getAllWoodTypes;
//	_actions[core::providers::Protocol::MsgGetWoodType]			= &Client::getWoodType;
//	_actions[core::providers::Protocol::MsgDestroyWoodType]		= &Client::destroyWoodType;
//	_actions[core::providers::Protocol::MsgCreateWoodType]		= &Client::createWoodType;
//	_actions[core::providers::Protocol::MsgUpdateWoodType]		= &Client::updateWoodType;

//	_actions[core::providers::Protocol::MsgCreateContractor]	= &Client::createContractor;
//	_actions[core::providers::Protocol::MsgUpdateContractor]	= &Client::updateContractor;
//	_actions[core::providers::Protocol::MsgDestroyContractor]	= &Client::destroyContractor;
//	_actions[core::providers::Protocol::MsgGetContractor]		= &Client::getContractor;
//	_actions[core::providers::Protocol::MsgGetContractors]		= &Client::getContractors;

//	_actions[core::providers::Protocol::MsgGetArticle]			= &Client::getArticle;
//	_actions[core::providers::Protocol::MsgGetArticlesForWarehouse]	= &Client::getArticlesForWarehouse;
//	_actions[core::providers::Protocol::MsgCreateArticle]		= &Client::createArticle;
//	_actions[core::providers::Protocol::MsgUpdateArticle]		= &Client::updateArticle;
//	_actions[core::providers::Protocol::MsgDestroyArticle]		= &Client::destroyArticle;

//	_actions[core::providers::Protocol::MsgGetOrders]			= &Client::getOrders;
//	_actions[core::providers::Protocol::MsgGetOrder]			= &Client::getOrder;
//	_actions[core::providers::Protocol::MsgUpdateOrder]			= &Client::updateOrder;
//	_actions[core::providers::Protocol::MsgCreateOrder]			= &Client::createOrder;
//	_actions[core::providers::Protocol::MsgDestroyOrder]		= &Client::destroyOrder;

//	_actions[core::providers::Protocol::MsgGetService]			= &Client::getService;
//	_actions[core::providers::Protocol::MsgGetServices]			= &Client::getServices;
//	_actions[core::providers::Protocol::MsgCreateService]		= &Client::createService;
//	_actions[core::providers::Protocol::MsgUpdateService]		= &Client::updateService;
//	_actions[core::providers::Protocol::MsgDestroyService]		= &Client::destroyService;

	_actions[core::providers::Protocol::MsgGetPurchaseInvoice]	= &Client::getPurchaseInvoice;
//	_actions[core::providers::Protocol::MsgGetAllPurchaseInvoices] = &Client::getAllPurchaseInvoices;
	_actions[core::providers::Protocol::MsgUpdatePurchaseInvoice] = &Client::updatePurchaseInvoice;
	_actions[core::providers::Protocol::MsgPostPurchaseInvoice]	= &Client::postPurchaseInvoice;
	_actions[core::providers::Protocol::MsgGetPurchaseInvoiceCorrection] = &Client::getPurchaseInvoiceCorrectionDocument;
	_actions[core::providers::Protocol::MsgGetItemsForPurchaseInvoice] = &Client::getItemsForPurchaseInvoice;

	_actions[core::providers::Protocol::MsgCreateSaleInvoice]	= &Client::createSaleInvoice;
	_actions[core::providers::Protocol::MsgUpdateSaleInvoice]	= &Client::updateSaleInvoice;
//	_actions[core::providers::Protocol::MsgGetAllSaleInvoices]	= &Client::getAllSaleInvoices;
	_actions[core::providers::Protocol::MsgGetSaleInvoice]		= &Client::getSaleInvoice;
	_actions[core::providers::Protocol::MsgPostSaleInvoice]		= &Client::postSaleInvoice;
	_actions[core::providers::Protocol::MsgGetSaleInvoiceCorrection] = &Client::getSaleInvoiceCorrectionDocument;

	_actions[core::providers::Protocol::MsgLockResource]		= &Client::lock;
	_actions[core::providers::Protocol::MsgUnlockResource]		= &Client::unlock;

	//	m_actions["SALE_INVOICE_GET_ATTRIBUTES"] 		= &Client::getSaleInvoceAttributes;
	//	m_actions["PURCHASE_INVOICE_GET_ATTRIBUTES"] 	= &Client::getPurchaseInvoiceAttributes;
}

void Client::registerCommands()
{
	commands_[proto::msg::RegisterDevice]	= &Client::registerDevice;
	commands_[proto::msg::UnregisterDevice]	= &Client::unregisterDevice;

	commands_[proto::msg::GetAllUsers]		= &Client::getAllUsers;
	commands_[proto::msg::LoginUser]		= &Client::loginUser;
	commands_[proto::msg::LogoutUser]		= &Client::logoutUser;
	commands_[proto::msg::GetUser]			= &Client::getUser;
	commands_[proto::msg::CreateUser]		= &Client::createUser;
	commands_[proto::msg::UpdateUser]		= &Client::updateUser;
	commands_[proto::msg::DestroyUser]		= &Client::destroyUser;
	commands_[proto::msg::GetUserRoles]		= &Client::getUserRoles;

	commands_[proto::msg::GetWoodTypes]		= &Client::getAllWoodTypes;
	commands_[proto::msg::GetWoodType]		= &Client::getWoodType;
	commands_[proto::msg::CreateWoodType]	= &Client::createWoodType;
	commands_[proto::msg::UpdateWoodType]	= &Client::updateWoodType;
	commands_[proto::msg::DestroyWoodType]	= &Client::destroyWoodType;

	commands_[proto::msg::GetCompany]		= &Client::getCompany;
	commands_[proto::msg::CreateCompany]	= &Client::createCompany;
	commands_[proto::msg::UpdateCompany]	= &Client::updateCompany;

	commands_[proto::msg::GetContractor]	= &Client::getContractor;
	commands_[proto::msg::GetAllContractors]= &Client::getAllContractors;
	commands_[proto::msg::CreateContractor]	= &Client::createContractor;
	commands_[proto::msg::UpdateContractor]	= &Client::updateContractor;
	commands_[proto::msg::DestroyContractor]= &Client::destroyContractor;

	commands_[proto::msg::GetWarehouse]		= &Client::getWarehouse;
	commands_[proto::msg::GetAllWarehouses]	= &Client::getAllWarehouses;
	commands_[proto::msg::CreateWarehouse]	= &Client::createWarehouse;
	commands_[proto::msg::UpdateWarehouse]	= &Client::updateWarehouse;
	commands_[proto::msg::DestroyWarehouse]	= &Client::destroyWarehouse;
	commands_[proto::msg::GetArticles]		= &Client::getArticlesForWarehouse;
	commands_[proto::msg::GetArticleTypes]	= &Client::getArticleTypes;
	commands_[proto::msg::GetArticleType]	= &Client::getArticleType;
	commands_[proto::msg::GetArticle]		= &Client::getArticle;
	commands_[proto::msg::CreateArticle]	= &Client::createArticle;
	commands_[proto::msg::UpdateArticle]	= &Client::updateArticle;
	commands_[proto::msg::DestroyArticle]	= &Client::destroyArticle;

	commands_[proto::msg::GetAllCities]		= &Client::getAllCities;

	commands_[proto::msg::GetCity]			= &Client::getCity;
	commands_[proto::msg::CreateCity]		= &Client::createCity;
	commands_[proto::msg::GetMeasureUnit]	= &Client::getMeasureUnit;
	commands_[proto::msg::GetMeasureUnits]	= &Client::getMeasureUnits;

	commands_[proto::msg::GetAllTaxes]		= &Client::getAllTaxes;
	commands_[proto::msg::GetTaxById]		= &Client::getTaxById;
	commands_[proto::msg::GetTaxByValue]	= &Client::getTaxByValue;
	commands_[proto::msg::CreateTax]		= &Client::createTax;
	commands_[proto::msg::UpdateTax]		= &Client::updateTax;
	commands_[proto::msg::DestroyTax]		= &Client::destroyTax;

	commands_[proto::msg::GetAllOrders]		= &Client::getAllOrders;
	commands_[proto::msg::CreateOrder]		= &Client::createOrder;
	commands_[proto::msg::UpdateOrder]		= &Client::updateOrder;
	commands_[proto::msg::DestroyOrder]		= &Client::destroyOrder;

	commands_[proto::msg::GetAllServices]	= &Client::getAllServices;
	commands_[proto::msg::GetService]		= &Client::getService;
	commands_[proto::msg::CreateService]	= &Client::createService;
	commands_[proto::msg::UpdateService]	= &Client::updateService;
	commands_[proto::msg::DestroyService]	= &Client::destroyService;

	commands_[proto::msg::GetAllSaleInvoices]		= &Client::getAllSaleInvoies;
	commands_[proto::msg::GetAllPurchaseInvoices]	= &Client::getAllPurchaseInvoices;
}

Client::~Client()
{
	if (m_socket != nullptr) {
		delete m_socket;
		m_socket = nullptr;
	}
	logInformation("--- Usunięto obiekt klienta z pamięci ---");
}

void Client::read()
{
	QTcpSocket* sock = qobject_cast<QTcpSocket*>(sender());
	if (sock == nullptr)
		return;

	while(sock->bytesAvailable())
	{
		pendingData_.append(sock->readAll());
		if (pendingData_.size() < sizeof(proto::MessageHeader))
			continue;

		QDataStream in{&pendingData_, QIODevice::ReadOnly};
		in.setVersion(QDataStream::Version::Qt_5_0);

		proto::MessageHeader header{};
		in >> header;

		const auto crc = qChecksum(pendingData_.data() + header.headerSize, header.dataSize);
		if (header.crc != crc)
			continue;

		if ((pendingData_.size() - header.headerSize) < header.dataSize)
		{
			qDebug() << "Discard read packet: no enough data to read payload";
			continue;
		}

		auto itr = std::find_if(std::begin(commands_), std::end(commands_),
			[&](const std::pair<proto::MessageType, Command>& row)
			{
				return header.type == row.first;
			});

		if (itr != commands_.end())
		{
			const auto size = pendingData_.size() - header.headerSize;
			QByteArray data{};
			data.resize(size);

			std::memcpy(data.data(), pendingData_.data() + header.headerSize, size);

			const auto [cmd, func] = *itr;
			((*this).*func)(data, *sock);

			sock->flush();
			pendingData_.clear();
		}
		else
		{
			logCritical(QObject::tr("Method identification was not found"));
		}
	}
}

void Client::readError(QAbstractSocket::SocketError socketError) {
	Q_UNUSED(socketError);
	std::cerr << m_socket->errorString().toStdString() << std::endl;
}

void Client::disconnected() {
	logInformation(QString("Klient %1 zakończył połączenie z serwerem").arg(m_socket->socketDescriptor()));

	server().removeClient(this);
}

void Client::registerDevice(QByteArray& data, QAbstractSocket& socket)
{
	logInformation(QObject::tr("Register new device"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray machineUniqueId{};
	QString machineHostName{};

	in >> machineUniqueId >> machineHostName;

	logInformation(QObject::tr("host id: %1\t host name: %2")
				   .arg(machineUniqueId, machineHostName));

	proto::DeviceStatus deviceStatus{};

	if (!server().isDeviceRegistered(QString::fromLocal8Bit(machineUniqueId)))
	{
		server().registerDevice(QString::fromLocal8Bit(machineUniqueId), machineHostName);
		deviceStatus.registered = true;

		logInformation(QObject::tr("Device has been registered successfully"));
	}
	else
	{
		logInformation(QObject::tr("Device rejected - already has been registered"));
		deviceStatus.registered = false;
	}

	const auto payload = as_bytes(deviceStatus);

	proto::MessageHeader header{};
	header.type = proto::msg::DeviceStatus;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	socket.write(as_bytes(header));
	socket.write(payload);
}

void Client::unregisterDevice(QByteArray& data, QAbstractSocket& sock)
{
	logInformation(QObject::tr("Unregister device request"));

	QDataStream in{&data, QIODevice::ReadOnly};
	in.setVersion(QDataStream::Version::Qt_5_0);

	QByteArray deviceId{};
	in >> deviceId;

	proto::DeviceStatus deviceStatus{};
	if (server().isDeviceRegistered(QString::fromUtf8(deviceId)))
	{
		server().unregisterDevice(deviceId);
		logInformation(QObject::tr("Device was unregistered successfully"));

		deviceStatus.registered = false;
	}
	else
	{
		logCritical(QObject::tr("Cannot unregister device in remote service"));
		deviceStatus.registered = true;
	}

	const auto payload = as_bytes(deviceStatus);

	proto::MessageHeader header{};
	header.type = proto::msg::DeviceStatus;
	header.headerSize = sizeof(proto::MessageHeader);
	header.dataSize = payload.size();
	header.crc = qChecksum(payload.constData(), payload.size());

	sock.write(as_bytes(header));
	sock.write(payload);
}

void Client::lock(QDataStream& stream) {
	logInformation(QObject::tr("Blokowanie dostępu do zasobu..."));

	core::eloquent::DBIDKey userId {0};
	core::eloquent::DBIDKey resourceId {0};
	int type;

	stream >> userId >> resourceId >> type;

	agent::Resource resource = static_cast<agent::Resource>(type);

	Q_ASSERT(userId > 0 && resourceId > 0);

	Server::LockHeader header;
	header.userId = userId;

	bool success {false};
	{
		switch (resource) {
		case agent::Resource::User:
			success = server().lockUser(resourceId, header);
			if (!success)
				header = server().getLockHeaderForUser(resourceId);
			break;
		case agent::Resource::Contractor:
			success = server().lockContractor(resourceId, header);
			if (!success)
				header = server().getLockHeaderForContractor(resourceId);
			break;
		case agent::Resource::Article:
			success = server().lockArticle(resourceId, header);
			if (!success)
				header = server().getLockHeaderForArticle(resourceId);
			break;
		case agent::Resource::Warehouse:
			success = server().lockWarehouse(resourceId, header);
			if (!success)
				header = server().getLockHeaderForWarehouse(resourceId);
			break;
		case agent::Resource::PurchaseInvoice:
			success = server().lockPurchaseInvoice(resourceId, header);
			if (!success)
				header = server().getLockHeaderForPurchaseInvoice(resourceId);
			break;
		case agent::Resource::SaleInvoice:
			success = server().lockSaleInvoice(resourceId, header);
			if (!success)
				header = server().getLockHeaderForSaleInvoice(resourceId);
			break;
		case agent::Resource::Order:
			success = server().lockOrder(resourceId, header);
			if (!success)
				header = server().getLockHeaderForOrder(resourceId);
			break;
		case agent::Resource::Service:
			success = server().lockService(resourceId, header);
			if (!success)
				header = server().getLockHeaderForService(resourceId);
			break;
		case agent::Resource::Company:
			success = server().lockCompany(resourceId, header);
			if (!success)
				header = server().getLockHeaderForCompany(resourceId);
			break;
		default:
			Q_ASSERT(false);
		}
	}
	if (!success) {
		stream << core::providers::Protocol::MsgFail
			   << header.userId
			   << header.time;

		logWarning(QObject::tr("Nie udzielono dostępu do zasobu!"));
	} else {
		stream << core::providers::Protocol::MsgSuccess;
		logInformation(QObject::tr("Zablokowano dostęp do zasobu."));
	}
}

void Client::unlock(QDataStream& stream) {
	logInformation(QObject::tr("Zwalnianie blokady na dostęp do zasobu..."));

	core::eloquent::DBIDKey resId {0};
	int type {0};
	stream >> resId >> type;

	agent::Resource resource = static_cast<agent::Resource>(type);
	Q_ASSERT(resId > 0);
	bool success {false};
	{
		switch (resource) {
		case agent::Resource::User:
			success = server().unlockUser(resId);
			break;
		case agent::Resource::Contractor:
			success = server().unlockContractor(resId);
			break;
		case agent::Resource::Article:
			success = server().unlockArticle(resId);
			break;
		case agent::Resource::Warehouse:
			success = server().unlockWarehouse(resId);
			break;
		case agent::Resource::PurchaseInvoice:
			success = server().unlockPurchaseInvoce(resId);
			break;
		case agent::Resource::SaleInvoice:
			success = server().unlockSaleInvoce(resId);
			break;
		case agent::Resource::Order:
			success = server().unlockOrder(resId);
			break;
		case agent::Resource::Service:
			success = server().unlockService(resId);
			break;
		case agent::Resource::Company:
			success = server().unlockCompany(resId);
			break;
		default:
			Q_ASSERT(false);
		}
	}

	if (success) {
		stream << core::providers::Protocol::MsgSuccess;
		logInformation(QObject::tr("Oblokowano dostęp do blokowanego zasobu."));
	} else {
		stream << core::providers::Protocol::MsgFail;
		logWarning(QObject::tr("Nie zwolniono blokady do blokowanego obiektu!"));
	}
}

void Client::logInformation(const QString& message) {
	_logger.information(message);
}

void Client::logWarning(const QString& message) {
	_logger.warning(message);
}

void Client::logCritical(const QString& message) {
	_logger.critical(message);
}

void Client::logTrace(const QString& message) {
	_logger.log(message, Logger::Type::Trace);
}

void Client::sendEvent(agent::ResourceOperation operation,
					   agent::Resource resource,
					   core::eloquent::DBIDKey resourceId)
{
	core::providers::agent::RepositoryEvent event {};
	event.operation = operation;
	event.resourceType = resource;
	event.resourceId = resourceId;
	event.userId = currentUser().id();

	server().eventAgent()->pullEvent(std::move(event));
}

//void Client::sendEvent(ResourceOperation operation, Resource resource, core::eloquent::DBIDKey resourceId) {
//	EventAgent::Event event{};
//	event.operation		= operation;
//	event.resourceType	= resource;
//	event.resourceId	= resourceId;
//	event.userId		= currentUser().id();

//	server().eventAgent()->pullEvent(std::move(event));
//}

