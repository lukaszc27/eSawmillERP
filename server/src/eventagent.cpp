#include "eventagent.hpp"
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkInterface>
#include <QMutexLocker>
#include <QNetworkDatagram>


EventAgent::EventAgent(const QHostAddress& address, quint64 port, core::logs::AbstractLogger& logger, QObject* parent)
	: QThread {parent}
	, m_address {address}
	, m_port{port}
	, m_logger {logger}
{
	m_socket = new QUdpSocket(this);
	m_socket->bind(QHostAddress::AnyIPv4, port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
	if (m_socket->state() == QAbstractSocket::BoundState) {
		logInformation(QString("Utworzono połączenie dla podsystemu obsługi zdarzeń (%1:%2)")
			.arg(m_address.toString())
			.arg(m_socket->localPort()));

		start();	///< run event agent thread
	} else {
		logCritical(QObject::tr("Błąd tworzenia połączenia dla podsystemu obsługi zdarzeń."));
	}
}

EventAgent::~EventAgent() {
	if (m_socket != nullptr && m_socket->isOpen()) {
		m_socket->close();
		delete m_socket;
		m_socket = nullptr;
	}
}

void EventAgent::pullEvent(const core::providers::agent::RepositoryEvent& repoEvent) {
	QByteArray payload{};
	QDataStream out{&payload, QIODevice::WriteOnly};
	out << repoEvent;

	core::providers::agent::Event event{};
	event.type = core::providers::agent::EventType::RepositorySync;
	event.crc = qChecksum(payload.constData(), payload.size());
	event.payloadSize = payload.size();
	event.payload = payload;

	m_eventsToSend.push(event);
}

void EventAgent::pullEvent(const core::providers::agent::ServerStatusEvent& statusEvent) {
	QByteArray payload{};
	QDataStream out {&payload, QIODevice::WriteOnly};
	out << statusEvent;

	core::providers::agent::Event event{};
	event.type = core::providers::agent::EventType::ServerStatus;
	event.payloadSize = payload.size();
	event.payload = payload;
	event.crc = qChecksum(payload.constData(), payload.size());

	m_eventsToSend.push(event);
}

void EventAgent::run() {
	while (!isInterruptionRequested())
	{
		if (!m_eventsToSend.empty())
		{
			auto event = m_eventsToSend.pop();
			if (!event)
				continue;

			QByteArray data{};
			QDataStream out{&data, QIODevice::WriteOnly};
			out << *event;

			if (!m_socket->writeDatagram(data, m_address, m_port))
				logWarning("Błąd podczas wysyłania powiadomienia");
		}
	}
	logInformation("Zamknięto połączenie dla przetwarzania zdarzeń");
}

void EventAgent::logInformation(const QString& message) {
	m_logger.information(message);
}

void EventAgent::logWarning(const QString &message) {
	m_logger.warning(message);
}

void EventAgent::logCritical(const QString &message) {
	m_logger.critical(message);
}
