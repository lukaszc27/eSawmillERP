#ifndef __STATE_MACHINE_HPP__
#define __STATE_MACHINE_HPP__

#include <stdexcept>
#include <unordered_map>
#include <memory>
#include <algorithm>

#include <QCoreApplication>

#include "server.hpp"
#include "eventagent.hpp"
#include "logger.hpp"


namespace service {

class StateMachine;

/// base class for next states
class AbstractPhase {
public:
	explicit AbstractPhase(StateMachine& machine) noexcept
		: machine_{machine}
	{
	}

	virtual void operator()() = 0;

	/// running every time when phase will be changed
	virtual void onPhaseChanged() {};

protected:
	StateMachine& machine_;
};


/// represents all states within can service work
/// (e.g. initialization, working, idle or stopping)
///
class StateMachine final {
public:
	enum class PhaseId
	{
		Initialization,
		DatabaseConfiguration,
		Working,
		InitializationFailed,
		Stopped,
		Finished
	};

	explicit StateMachine(QCoreApplication& app) noexcept
		: phase_{PhaseId::Initialization}
		, app_{app}
	{}
	~StateMachine() = default;

	// no copyable - remove copy constructor
	explicit StateMachine(const StateMachine& other) = delete;
	StateMachine& operator=(const StateMachine& other) = delete;

	/// register new state
	///
	template <typename Phase>
	void registerPhase() {
		const auto id = getPhaseId<Phase>();
		phases_[id] = std::make_shared<Phase>(*this);
	}

	/// get current phase
	///
	PhaseId currentPhase() const { return phase_; }
	bool isTerminated() const { return terminated_; }
	void terminate() { terminated_ = true; }

	std::shared_ptr<AbstractPhase> currentPhase() {
		auto itr = std::find_if(phases_.begin(), phases_.end(),
			[this](const std::pair<PhaseId, std::shared_ptr<AbstractPhase>>& item) {
				return item.first == phase_;
			});
		if (itr == phases_.end())
			throw std::out_of_range("Phase unregistered");

		return itr->second;
	}

	void changePhase(PhaseId next) {
		if (next == phase_)
			return;

		phase_ = next;
		if (auto phase = currentPhase(); phase)
			phase->onPhaseChanged();
	}

public:
	std::unique_ptr<Server> server_;
	std::unique_ptr<EventAgent> eventAgent_;
	std::unique_ptr<Logger> logger_;
	QCoreApplication& app_;

private:
	template <typename Phase>
	PhaseId getPhaseId() {
		return Phase::Id;
	}

private:
	using PhasesMap = std::unordered_map<PhaseId, std::shared_ptr<AbstractPhase>>;
	PhasesMap phases_;
	PhaseId phase_{};
	bool terminated_{};
};


class InitializationPhase final : public AbstractPhase {
public:
	constexpr static StateMachine::PhaseId Id {StateMachine::PhaseId::Initialization};

	explicit InitializationPhase(StateMachine& machine) noexcept;

	void operator()() override;
};

class DatabaseConfgurationPhase final : public AbstractPhase {
public:
	constexpr static StateMachine::PhaseId Id {StateMachine::PhaseId::DatabaseConfiguration};

	explicit DatabaseConfgurationPhase(StateMachine& machine);

	void operator()() override;
};

class InitializationFailedPhase final : public AbstractPhase {
public:
	constexpr static StateMachine::PhaseId Id {StateMachine::PhaseId::InitializationFailed};

	explicit InitializationFailedPhase(StateMachine& machine);

	void operator()() override;
};

class WorkingPhase final : public AbstractPhase {
public:
	constexpr static StateMachine::PhaseId Id {StateMachine::PhaseId::Working};

	explicit WorkingPhase(StateMachine& machine);

	void operator()() override;
};

} // namespace service

#endif //__STATE_MACHINE_HPP__
