#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <QTcpSocket>
#include <QThread>
#include <eloquent/model.hpp>
#include <dbo/user.h>
#include <logs/abstractlogger.hpp>
#include <unordered_map>
#include <providers/protocol.hpp>
//#include "logger.hpp"
//#include <providers/controlcenterclient.hpp>

namespace agent = core::providers::agent;
namespace proto = core::providers::protocol;

class Server;

class Client : public QObject {
	Q_OBJECT

	friend class Server;

public:
	Client(Server& server, QTcpSocket* socket, core::logs::AbstractLogger& logger, QObject* parent = nullptr);
	virtual ~Client();

	Client(const Client&) noexcept = delete;
	Client& operator=(const Client& other) noexcept = delete;

	///
	/// \brief The ErrorCode enum
	/// error codes used to inform other users about problems on server side
	///
	enum class StatusCode {
		// successfull responses
		Ok								= 200,
		Created							= 201,
		Accepted						= 202,
		NoContent						= 204,
		ResetContent					= 205,

		// client error responses
		BadRequest						= 400,
		Unauthorized					= 401,
		Forbidden						= 403,
		NotFound						= 404,
		MethodNowAllowed				= 405,

		// server error responses
		InternalServerError				= 500,
		NotImplemented					= 501,
		ServiceUnavaiable				= 503,
		NetworkAuthenticationRequired	= 511
	};

	QTcpSocket* socket() const	{ return m_socket; }
	Server& server()			{ return m_server; }
	core::User& currentUser()	{ return m_currentUser; }

private:
	core::User	m_currentUser;	///< current logged user
	QTcpSocket* m_socket;		///< TCP socket to communicate with client
	Server&		m_server;		///< reference to server object
	core::logs::AbstractLogger& _logger;
	QByteArray pendingData_{};

	using CommandDescriptor = void(Client::*)(QDataStream&);
	std::unordered_map<int, CommandDescriptor> _actions;

	using Command = void(Client::*)(QByteArray&,QAbstractSocket&);
	std::unordered_map<proto::MessageType, Command> commands_;

	void registerCommands();

private Q_SLOTS:
	///
	/// \brief read data sent by client
	///
	void read();

	///
	/// \brief readError
	///
	void readError(QAbstractSocket::SocketError socketError);
	void disconnected();

private:
	void registerDevice(QByteArray& data, QAbstractSocket&);
	void unregisterDevice(QByteArray& data, QAbstractSocket&);

	void lock(QDataStream&);
	void unlock(QDataStream&);

private:
	void loginUser(QByteArray&, QAbstractSocket&);
	void logoutUser(QByteArray&, QAbstractSocket&);
	void getAllUsers(QByteArray&, QAbstractSocket&);
	void getUser(QByteArray&, QAbstractSocket&);
	void createUser(QByteArray&, QAbstractSocket&);
	void updateUser(QByteArray&, QAbstractSocket&);
	void destroyUser(QByteArray&, QAbstractSocket&);
	void getUserRoles(QByteArray&, QAbstractSocket&);

private:
	void getCompany(QByteArray& data, QAbstractSocket& sock);
	void createCompany(QByteArray& data, QAbstractSocket&);
	void updateCompany(QByteArray& data, QAbstractSocket&);

private:
	void getMeasureUnits(QByteArray&, QAbstractSocket&);
	void getMeasureUnit(QByteArray&, QAbstractSocket&);

private:
	void getAllWarehouses(QByteArray&, QAbstractSocket&);
	void getWarehouse(QByteArray&, QAbstractSocket&);
	void createWarehouse(QByteArray&, QAbstractSocket&);
	void updateWarehouse(QByteArray&, QAbstractSocket&);
	void destroyWarehouse(QByteArray&, QAbstractSocket&);
	void getArticlesForWarehouse(QByteArray&, QAbstractSocket&);

private:
	void getArticleType(QByteArray&, QAbstractSocket&);
	void getArticleTypes(QByteArray&, QAbstractSocket&);
	void getArticle(QByteArray&, QAbstractSocket&);
	void createArticle(QByteArray&, QAbstractSocket&);
	void updateArticle(QByteArray&, QAbstractSocket&);
	void destroyArticle(QByteArray&, QAbstractSocket&);

private:
	void getAllTaxes(QByteArray&, QAbstractSocket&);
	void getTaxById(QByteArray&, QAbstractSocket&);
	void getTaxByValue(QByteArray&, QAbstractSocket&);
	void createTax(QByteArray&, QAbstractSocket&);
	void updateTax(QByteArray&, QAbstractSocket&);
	void destroyTax(QByteArray&, QAbstractSocket&);

private:
	void getAllWoodTypes(QByteArray&, QAbstractSocket&);
	void getWoodType(QByteArray&, QAbstractSocket&);
	void createWoodType(QByteArray&, QAbstractSocket&);
	void updateWoodType(QByteArray&, QAbstractSocket&);
	void destroyWoodType(QByteArray&, QAbstractSocket&);

private:
	void getAllContractors(QByteArray&, QAbstractSocket&);
	void getContractor(QByteArray&, QAbstractSocket&);
	void createContractor(QByteArray&, QAbstractSocket&);
	void updateContractor(QByteArray&, QAbstractSocket&);
	void destroyContractor(QByteArray&, QAbstractSocket&);

private:
	void getAllSaleInvoies(QByteArray&, QAbstractSocket&);
	void getAllPurchaseInvoices(QByteArray&, QAbstractSocket&);
	///////////////////////////////////////////////////////////
	/// invoices group
	///////////////////////////////////////////////////////////
	void getSaleInvoice(QDataStream&);
	void updateSaleInvoice(QDataStream&);
	void createSaleInvoice(QDataStream&);
	void getItemsForSaleInvoice(QDataStream&);
	void markSaleInvoiceAsCorrectionDocument(QDataStream&);
	void getSaleInvoiceCorrectionDocument(QDataStream&);
	void postSaleInvoice(QDataStream&);
	//// subgroup for purchase invoices
	void getPurchaseInvoice(QDataStream&);
	void getPurchaseInvoices(QDataStream&);
	void updatePurchaseInvoice(QDataStream&);
	void createPurchaseInvoice(QDataStream&);
	void getItemsForPurchaseInvoice(QDataStream&);
	void getPurchaseInvoiceCorrectionDocument(QDataStream&);
	void postPurchaseInvoice(QDataStream&);

private:
	///@{
	/// methods to get attributes for invoices
	QJsonObject getHasSaleInvoiceCorrectionAttribute(core::eloquent::DBIDKey saleInvoiceId);
	QJsonObject getHasSaleInvoiceAssignedOrder(core::eloquent::DBIDKey saleInvoiceId);
	/// methods for purchase invoices
	QJsonObject getHasPurchaseInvoiceCorrectionAttribute(core::eloquent::DBIDKey purchaseInvoiceId);
	///@}

private:
	void getAllOrders(QByteArray&, QAbstractSocket&);
	void getOrder(QByteArray&, QAbstractSocket&);
	void createOrder(QByteArray&, QAbstractSocket&);
	void updateOrder(QByteArray&, QAbstractSocket&);
	void destroyOrder(QByteArray&, QAbstractSocket&);

private:
	void getAllServices(QByteArray&, QAbstractSocket&);
	void getService(QByteArray&, QAbstractSocket&);
	void destroyService(QByteArray&, QAbstractSocket&);
	void createService(QByteArray&, QAbstractSocket&);
	void updateService(QByteArray&, QAbstractSocket&);
	///////////////////////////////////////////////////////////
	/// service group
	///////////////////////////////////////////////////////////
//	void getServices(QDataStream&);
//	void getService(QDataStream&);
//	void destroyService(QDataStream&);
//	void createService(QDataStream&);
//	void updateService(QDataStream&);

private:
	void getAllCities(QByteArray&, QAbstractSocket&);
	void getCity(QByteArray&, QAbstractSocket& sock);
	void createCity(QByteArray&, QAbstractSocket&);

private:
	///@{
	/// internal methods
	void logInformation(const QString& message);
	void logWarning(const QString& message);
	void logCritical(const QString& message);
	void logTrace(const QString& message);

	/// @brief send event via internal event agent service
	void sendEvent(agent::ResourceOperation operation,
				   agent::Resource resource,
				   core::eloquent::DBIDKey resourceId);
	///@}
};


template <typename Object>
QByteArray as_bytes(const Object& obj) noexcept
{
	QByteArray arr{};
	QDataStream out{&arr, QIODevice::WriteOnly};
	out.setVersion(QDataStream::Version::Qt_5_0);
	out << obj;

	return arr;
}

#endif // CLIENT_HPP
