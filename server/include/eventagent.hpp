#ifndef EVENTAGENT_HPP
#define EVENTAGENT_HPP

#include <QThread>
#include <QUdpSocket>
#include <QMutex>
#include <queue>
#include <logs/abstractlogger.hpp>
#include "providers/protocol.hpp"

///
/// \brief The Queue class
/// thread safe queue for events uses by event agent thread
///
template <typename Event>
class Queue {
public:
	explicit Queue() = default;

	// no copyable
	Queue(const Queue<Event>&) = delete;
	Queue<Event>& operator=(const Queue<Event>&) = delete;


	bool empty() noexcept {
		std::lock_guard lock{mtx_};
		return queue_.empty();
	}

	void push(const Event& event) noexcept {
		std::lock_guard lock {mtx_};
		queue_.push(event);
	}

	void push(Event&& event) noexcept {
		std::lock_guard lock{mtx_};
		queue_.push(std::move(event));
	}

	std::shared_ptr<Event> pop() noexcept {
		if (empty())
			return nullptr;

		std::lock_guard lock{mtx_};
		Event event = queue_.front();
		queue_.pop();

		return std::make_shared<Event>(event);
	}

private:
	std::queue<Event> queue_{};
	std::mutex mtx_{};
};

///
/// \brief The EventAgent class
/// thread to send notifications from server via UDP protocol
/// the event agent pull all events into multicast group or broadcast address
///
class EventAgent : public QThread {
	Q_OBJECT

public:
	explicit EventAgent(const QHostAddress& address, quint64 port, core::logs::AbstractLogger& logger, QObject* parent = nullptr);
	virtual ~EventAgent();

	EventAgent(const EventAgent& other) = delete;
	EventAgent& operator=(const EventAgent& other) = delete;

	///
	/// \brief pullEvent
	/// add new event do queue
	/// \param event
	///
	void pullEvent(const core::providers::agent::RepositoryEvent& event);
	void pullEvent(const core::providers::agent::ServerStatusEvent& event);

protected:
	virtual void run() override;

private:
	/// queue event prepared to send
	Queue<core::providers::agent::Event> m_eventsToSend;

	///
	/// \brief m_socket
	/// socket instance to send and recvied event messages from clients
	///
	QUdpSocket* m_socket;
	QHostAddress m_address;
	quint64 m_port;


	core::logs::AbstractLogger& m_logger;


private:
	void logInformation(const QString& message);
	void logWarning(const QString& message);
	void logCritical(const QString& message);
};

#endif // EVENTAGENT_HPP
