#ifndef SERVER_HPP
#define SERVER_HPP

#include <QTcpServer>
#include <QList>
#include <QTcpSocket>
#include <QSet>
#include <QTime>
#include <repository/abstractrepository.hpp>
#include <unordered_map>
#include <set>
#include <memory>
#include "client.hpp"
#include "eventagent.hpp"
#include "logger.hpp"
#include <cstdint>

struct ClientInformation {
	explicit ClientInformation()
		: client {nullptr}
	{}

	QByteArray	uniqueDeviceId;
	QString		hostName;
	core::User	user;
	Client*		client;

	operator bool() noexcept;
	bool operator<(const ClientInformation&) noexcept;
};

class Server : public QTcpServer {
	Q_OBJECT

private:
	std::uint16_t port_;

public:
	explicit Server(const std::uint16_t port, EventAgent* eventAgent, Logger& logger, QObject* parent = nullptr);
	virtual ~Server();

	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;

	///
	/// \brief repository return default repository uses to store data in database
	/// this method should return the same object as DatabaseRepository used in main app
	/// (in case when server can operate on database) if future we will need a proxy server
	/// we have to write next repository object and return it in this method
	///
	core::repository::AbstractRepository& repository();

	///
	/// \brief removeClient
	/// check if client exist in m_clients list
	/// if exist interrupt threat and release alocated memory
	/// \param client
	///
	void removeClient(Client* client);

	///
	/// \brief eventAgent
	/// pointer to current  uses event agent subservice
	/// \return
	///
	EventAgent* eventAgent() { return m_eventAgent; }

	bool registerDevice(const QString& deviceId, const QString& hostName);
	void unregisterDevice(const QString& deviceId);
	bool isDeviceRegistered(const QString& deviceId) const;

	////////////////////////////////////////////////////////////////////////////
	/// \brief The LockHeader struct
	/// dostarcza grupę informacji o użytkowniu który tworzy blokadę na zasób
	///
public:
	struct LockHeader {
		explicit LockHeader()
			: userId {0}
			, time{QTime::currentTime()}
		{
		}
		explicit LockHeader(core::eloquent::DBIDKey id, const QTime& time)
			: userId {id}
			, time {time}
		{
		}

		core::eloquent::DBIDKey userId;	///< ib użytkownika który zakłada blokadę
		QTime	time;					///< czas kiedy została założona blokada
	};
	/////////////////////////////////////////////////////////////////////////////

public:
	/// sekcja zarządania blokadą dla użytkowników
	inline bool lockUser(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingUsers); }
	inline bool unlockUser(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingUsers); }
	inline LockHeader getLockHeaderForUser(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingUsers); }

	/// sekcja blokady dla kontrahentów
	inline bool lockContractor(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingContractors); }
	inline bool unlockContractor(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingContractors); }
	inline LockHeader getLockHeaderForContractor(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingContractors); }

	/// sekcja zarządania blokadą na artykułów
	inline bool lockArticle(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingArticles); }
	inline bool unlockArticle(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingArticles); }
	inline LockHeader getLockHeaderForArticle(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingArticles); }

	/// sekcja zarządzania blokadą dla magazynów
	inline bool lockWarehouse(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingWarehouses); }
	inline bool unlockWarehouse(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingWarehouses); }
	inline LockHeader getLockHeaderForWarehouse(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingWarehouses); }

	/// sekcja zarządzania blokadą dla faktór zakupu
	inline bool lockPurchaseInvoice(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingPurchaseInvoices); }
	inline bool unlockPurchaseInvoce(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingPurchaseInvoices); }
	inline LockHeader getLockHeaderForPurchaseInvoice(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingPurchaseInvoices); }

	/// sekcja zarządzania blokadą na faktór sprzedaży
	inline bool lockSaleInvoice(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingSaleInvoices); }
	inline bool unlockSaleInvoce(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingSaleInvoices); }
	inline LockHeader getLockHeaderForSaleInvoice(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingSaleInvoices); }

	/// sekcja zarządzania blokadą zamówień
	inline bool lockOrder(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingOrders); }
	inline bool unlockOrder(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingOrders); }
	inline LockHeader getLockHeaderForOrder(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingOrders); }

	/// sekcja zarządzania blokadą na usługach
	inline bool lockService(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingServices); }
	inline bool unlockService(const core::eloquent::DBIDKey& resourceId)
		{ return unlock(resourceId, &m_lockingServices); }
	inline LockHeader getLockHeaderForService(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingServices); }

	inline bool lockCompany(const core::eloquent::DBIDKey& resourceId, const LockHeader& header)
		{ return lock(resourceId, header, &m_lockingCompanies); }
	inline bool unlockCompany(const core::eloquent::DBIDKey& companyId)
		{ return unlock(companyId, &m_lockingCompanies); }
	inline LockHeader getLockHeaderForCompany(const core::eloquent::DBIDKey& resourceId)
		{ return getLockHeader(resourceId, &m_lockingCompanies); }

public:
	/// @brief releaseAllLocker
	/// release all existing lock object on server for specific user
	/// @param userId
	void releaseAllLocker(const core::eloquent::DBIDKey& userId);

	/// @brief isUSerLogged function
	/// check do user is logged in current time
	bool isUserLogged(const core::eloquent::DBIDKey& userId) const;
	bool attachUserToDevice(const QByteArray& deviceId, const core::User& user, Client* client);
	bool detachUserFromDevice(const core::User& user);

	ClientInformation* getClientInformation(const core::eloquent::DBIDKey& userId);

private:
	/// @brief m_clients - list all clients connected to server
	/// userd to broadcast communication
	std::vector<std::unique_ptr<Client>> m_clients;
	std::vector<ClientInformation> m_clientsInformation;


	/// @brief m_eventAgent
	/// theard uses to send notifications to client via UDP protocol
	EventAgent* m_eventAgent;


	///@{
	/// blocking
	using LockPair = std::pair<core::eloquent::DBIDKey, LockHeader>;
	/// \brief m_lockingUsers
	/// aktualnie zablokowani użytkownicy podczasy wykonywania operacji edycji
	/// mapa wskazuje id blkowanego zasobu => struktura opisująca użytkownika który założył
	/// blokadę oraz czasie kiedy została ona utworzona
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingUsers;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingContractors;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingArticles;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingWarehouses;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingPurchaseInvoices;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingSaleInvoices;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingOrders;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingServices;
	std::unordered_map<core::eloquent::DBIDKey, LockHeader> m_lockingCompanies;


	/// \brief lock - blokuje dostęp do zasobu
	/// \param resId - id blokowanego zasobu
	/// \param lock - obiekt na którym będziemy blokować
	/// \return
	bool lock(
		const core::eloquent::DBIDKey& resId,
		const LockHeader& header,
		std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock
	);
	/// \brief unlock
	/// odblokowuje dostęp do zasobu
	/// \param resId
	/// \param lock
	/// \return
	bool unlock(
		const core::eloquent::DBIDKey& resId,
		std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock
	);
	/// \brief getLockHeader
	/// pobiera nagłówek zawierający informacje który użytkownik zablokowł dany zasób
	/// oraz o czasie założenia blokady
	/// \param resId
	/// \param lock
	/// \return
	LockHeader getLockHeader(
		const core::eloquent::DBIDKey& resId,
		std::unordered_map<core::eloquent::DBIDKey, LockHeader>* lock
	);
	///@}

private:
	Logger& m_logger;
	QTimer m_stateTimer;

private Q_SLOTS:
	///
	/// \brief acceptPendingConnection
	/// answer on newConnection signal emitted every time when a new connection is avaiable
	///
	void acceptPendingConnection();
	void sendServerState();
};

#endif // SERVER_HPP
