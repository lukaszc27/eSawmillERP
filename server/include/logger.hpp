#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <QString>
#include <QtConcurrent>
#include <queue>
#include <logs/abstractlogger.hpp>

///
/// \brief The Logger struct
/// basic logger uses by service to show nessesary information in console window
///
struct Logger : public core::logs::AbstractLogger {
	explicit Logger() noexcept = default;
	virtual ~Logger() = default;

	explicit Logger(const Logger&) = delete;
	Logger& operator=(const Logger&) = delete;

	operator bool() override { return true; }

	///
	/// \brief log
	/// \param type
	/// \param message
	///
	void log(const QString& message, Type type = Type::Information) override;

private:
	///
	/// \brief doLog
	/// log all messages on other thread
	///
	void doLog() const;

	typedef std::tuple<Type, QString> Log;
	mutable std::queue<Log> _logs {};
	mutable QReadWriteLock _logs_locker {};
};

#endif // LOGGER_HPP
